from __future__ import division
import _path
import os, sys, inspect
import logging
from collections import namedtuple
import common.dirs as dirs
import common.emails as emails
import data.database as db
import pandas as pd
import numpy as np
import xlwings as xw
import _config as cfg
cfg.config_path()



def get_raw_gs_norm_fci():
    q = r"""SELECT [AsOfDate],[Security],[Field],[Value] FROM [Fixed_Income].[data].[GS_NORM_FCI]"""
    return q

def pivot_fci(unpivoted_data):
    #print unpivoted_data
    
    pivoted_data = unpivoted_data.pivot(index='AsOfDate', columns = 'Security', values = 'Value')
    renamed_data = pivoted_data.rename(cfg.FCI_NAMES, axis = 'columns').reindex(cfg.DATE_IDX).ffill()
    
    

    result = Result(sov_2=sov_2y, sov_5=sov_5y, sov_10=sov_10y )   
    return result




if __name__ == '__main__':

    _exit_code = 9999

    try:
       
        logging.info("<<<< {0} starts >>>>".format(cfg.APP_NAME))
        dirs.archive_files(cfg.DEBUG_FOLDER, cfg.ARCHIVE_DEBUG_FOLDER, copy_only = False)
        dirs.archive_files(cfg.OUTPUT_FOLDER, cfg.ARCHIVE_OUTPUT_FOLDER, copy_only = False)
        
        _db = db.Database(cfg.FI_PROD_DB_CONN_STRING, cfg.DEBUG_FOLDER)

        fci_data = _db.query(get_raw_gs_norm_fci())
        pvt_fci_data = pivot_fci(fci_data)


        
        '''
        
        wb = xw.books[cfg.WB_NAME]
        wsht = wb.sheets[cfg.WKST_NAME]
        wb_data = wsht.range('A1').options(pd.DataFrame, expand='table').value
        
        for c in cfg.MISSING_COUNTRTIES:
            wb_data[c] = np.NaN
        
        fci_data = wb_data[cfg.AVAILABLE_COUNTRTIES].rename(columns=cfg.RENAMES)
        fci_data = fci_data.reindex(cfg.DATE_IDX)
        fci_data.index.name = 'Date'
        
        
        
        
        #create a subset
        fci_data_subset = fci_data.tail(cfg.SUBSET_SIZE).ffill()        
        fci_data_subset.to_sql(cfg.DB_TABLE, _db.engine, schema=cfg.IMP_SCHEMA,  if_exists='replace', index=True)
        
        
        #merge data
        if(cfg.MERGE_DATA):
            _db.execute_nonquery(cfg.CLEANUP_STATEMENT)
            _db.execute_nonquery(cfg.MERGE_STATEMENT)
        
        '''
        
        logging.info("<<<< {0} ends SUCCESSFULLY >>>>".format(cfg.APP_NAME))
        _exit_code = 0
    except:
        logging.exception("!!! Error detected in {0}".format(cfg.APP_NAME))
        logging.ERROR("<<<< {0} ends UNSUCCESSFULLY >>>>".format(cfg.APP_NAME))
        _exit_code = 9999
    finally:
        _result = dirs.check_log_file_result(cfg.LOG_PATH)
        _title = "{0} process - {1}".format(cfg.APP_NAME, _result)
        _email_body = dirs.htmlfy_log_file(cfg.LOG_PATH)
        emails.send_SMTP_mail(cfg.SENDER, cfg.RECEIVERS, _title, _email_body, cfg.CC, [cfg.LOG_PATH])
        sys.exit(_exit_code)
