import os, sys, inspect
import logging
import datetime
import copy
import pandas as pd
from pandas.tseries.offsets import DateOffset, BDay
import xlwings as xw


### common 
TEST_MODE = True
APP_NAME = r'gs_fci_excel_parser'
CURRENT_PATH = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0])) 
APP_ROOT = r'c:/temp/test/' + APP_NAME
CORE_ROOT = r''
SENDER = 'YIWEI.ZHONG@ampcapital.com'
RECEIVERS = ['YIWEI.ZHONG@ampcapital.com']
CC = []
DTYPE_DATEIME64 = r'datetime64[ns]'
ODBC_STYLE_CSTR_PREFIX = r'mssql+pyodbc:///?odbc_connect=%s'
LOG_FORMAT = r'%(asctime)s - %(name)s - %(levelname)s - %(message)s'
TIMESTAMP_FMT = r'%Y-%m-%d %H:%M:%S.%f'
FI_PROD_DB_CONN_STRING = r'DRIVER={SQL Server Native Client 10.0};SERVER=frontofficesql;DATABASE=Fixed_income;Trusted_Connection=yes;Integrated Security=True; MultipleActiveResultSets=True; Connection Timeout=800'
LOG_PATH = ''
LOG_FOLDER = ''
DEBUG_FOLDER = ''
OUTPUT_FOLDER = ''
ARCHIVE_FOLDER = ''
ARCHIVE_LOG_FOLDER = ''
ARCHIVE_DEBUG_FOLDER = ''
ARCHIVE_OUTPUT_FOLDER = ''
INPUT_FOLDER = ''


### app sepcific
COUNTRY_CONFIGS = None


### 
def config_path():

    _parent_path = os.path.abspath(os.path.join(CURRENT_PATH, os.pardir))
    _gparent_path = os.path.abspath(os.path.join(_parent_path, os.pardir))
    _ggparent_path = os.path.abspath(os.path.join(_gparent_path, os.pardir))

    #adding the path here
    if (TEST_MODE):        
        _core_path = _gparent_path
        if _core_path not in sys.path: sys.path.insert(0, _core_path)
    else:
        pass

    import core.dirs as dirs
    
    global INPUT_FOLDER, LOG_FOLDER, DEBUG_FOLDER, \
           OUTPUT_FOLDER, ARCHIVE_FOLDER, ARCHIVE_LOG_FOLDER, \
           ARCHIVE_DEBUG_FOLDER, ARCHIVE_OUTPUT_FOLDER, \
           LOG_PATH


    INPUT_FOLDER = dirs.get_creat_sub_folder(folder_name='input', parent_dir=APP_ROOT) 
    LOG_FOLDER = dirs.get_creat_sub_folder(folder_name='log', parent_dir=APP_ROOT)
    DEBUG_FOLDER = dirs.get_creat_sub_folder(folder_name='debug', parent_dir=APP_ROOT)
    OUTPUT_FOLDER = dirs.get_creat_sub_folder(folder_name='output', parent_dir=APP_ROOT)
    ARCHIVE_FOLDER  = dirs.get_creat_sub_folder(folder_name='archive', parent_dir=APP_ROOT)
    ARCHIVE_LOG_FOLDER = dirs.get_creat_sub_folder(folder_name="log", parent_dir=ARCHIVE_FOLDER)
    ARCHIVE_DEBUG_FOLDER = dirs.get_creat_sub_folder(folder_name="debug", parent_dir=ARCHIVE_FOLDER)
    ARCHIVE_OUTPUT_FOLDER = dirs.get_creat_sub_folder(folder_name="Output", parent_dir=ARCHIVE_FOLDER)

    ###
    LOG_PATH = os.path.join(LOG_FOLDER + '/' + APP_NAME + '_' + datetime.datetime.now().strftime("%Y%m%d_%H")+'.log')
    logging.basicConfig(filename=LOG_PATH, level=logging.INFO, format = '%(asctime)s - %(levelname)s - %(message)s')
    
    logging.info("sys.path:")
    for p in sys.path: logging.info(p)
    #for p in sys.path: print p
    return

###
#config_path()


###
WB_NAME = 'document (1).xlsx'
WKST_NAME = 'FCI - Nominal (daily)'
MISSING_COUNTRTIES = ['Singapore', 'Norway']
AVAILABLE_COUNTRTIES = ['Hong Kong', 'Taiwan', 'South Korea', 'Singapore', 'Malaysia', 'Indonesia', 
                       'Philippines', 'Thailand', 'India', 'China', 'Euro area', 'Japan', 'Norway',
                       'UK', 'Switzerland', 'Canada', 'Australia', 'USA', 'New Zealand', 'Sweden', 
                       'Global', 'Developed Markets', 'Emerging Markets'] 

RENAMES = {'USA':'US', 
           'South Korea': 'Korea', 
           'Euro area': 'Europe', 
           'New Zealand': 'NZD', 
           'Sweden':'SEK',
           'Developed Markets':'DM', 
           'Emerging Markets':'EM'}


FCI_NAMES = {
    'GSAUFCI Index':'AUD',
    'GSCAFCI Index':'CAD',
    'GSCNFCI Index':'CNY',
    'GSEAFCI Index':'EUR',
    'GSINFCI Index':'INR',
    'GSIDFCI Index':'IDR',
    'GSJPFCI Index':'JPY',
    'GSKRFCI Index':'KRW'
    'GSMYFCI Index':'MYR',
    'GSNOFCI Index':'NOK',
    'GSPHFCI Index':'PHP',
    'GSCHFCI Index':'CHF',
    'GSTHFCI Index':'THB',
    'GSGBFCI Index':'GBP',
    'GSUSFCI Index':'USD',
    'GSNZFCI Index':'NZD',  
    'GSSEFCI Index':'SEK',
    'GSAUFCIE Index':'AUDE',
    'GSCAFCIE Index':'CADE',
    'GSNZFCIE Index':'NZDE',
    'GSNOFCIE Index':'NOKE',
    'GSBRFCI Index': 'BRL',
    'GSCLFCI Index': 'CLP',
    'GSMXFCI Index': 'MXN',
    'GSCZFCI Index': 'CZK',
    'GSHUFCI Index': 'HUF',
    'GSILFCI Index': 'ILS',
    'GSPLFCI Index': 'PLN',
    'GSRUFCI Index': 'RUB',
    'GSZAFCI Index': 'ZAR',
    'GSTRFCI Index': 'TRY',
    'GSGLFCI Index': 'GLOBAL',
    'GSDMFCI Index': 'DM',
    'GSEMFCI Index': 'EM'
}

IMP_SCHEMA = 'IMP'
DATA_SCHEMA = 'DATA'
DB_TABLE = 'GSFCI'
SUBSET_SIZE = 90

MERGE_DATA = True

CLEANUP_STATEMENT = '''Delete FROM [Fixed_Income].[{0}].[{1}]
WHERE [Date] in
(
   select [Date] from [Fixed_Income].[{2}].[{1}]
)
'''.format(DATA_SCHEMA, DB_TABLE, IMP_SCHEMA)

MERGE_STATEMENT =  '''INSERT INTO [Fixed_Income].{0}.{1}
([Date],[Hong Kong],[Taiwan],[Korea],[Singapore],[Malaysia],[Indonesia],[Philippines]
 ,[Thailand],[India],[China],[Europe],[Japan],[Norway],[UK],[Switzerland],[Canada]
 ,[Australia],[US],[NZD],[SEK])
SELECT [Date],[Hong Kong],[Taiwan],[Korea],[Singapore],[Malaysia],[Indonesia],[Philippines]
 ,[Thailand],[India],[China],[Europe],[Japan],[Norway],[UK],[Switzerland],[Canada]
 ,[Australia],[US],[NZD],[SEK]
 FROM [Fixed_Income].{2}.{1} SRC
 WHERE NOT EXISTS
 (
      SELECT null
      FROM [Fixed_Income].{0}.{1} tgt
      WHERE (tgt.[Date] = src.[Date]) 
 )
'''.format(DATA_SCHEMA, DB_TABLE, IMP_SCHEMA)

DATE_IDX = pd.date_range('1999-01-01', datetime.datetime.now().strftime(r'%Y-%m-%d'), freq=BDay())			  


