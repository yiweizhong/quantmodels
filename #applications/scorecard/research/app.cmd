echo off
SET localPath=%~dp0%
echo %localPath%
pushd %localPath%
C:\Anaconda\python.exe scorecard_component.py
if errorlevel 1 (
   echo Process failed with code %errorlevel%
   exit /b %errorlevel%
)

popd
echo on