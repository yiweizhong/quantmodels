import datetime
import os, sys, inspect
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper# -*- coding: utf-8 -*-


#------------------------------------------------------------------------------------------------
def recolumn_df_data(df, cols=None):
#------------------------------------------------------------------------------------------------
    #validate_timeseries_data(df)
    if cols is None:
        return df
    else:
        df_new = df.copy()
        for c in cols:
            if c in df_new.columns.tolist():
                pass
            else:
                df_new[c] =np.nan
        return df_new[list(cols)]


#------------------------------------------------------------------------------------------------
def rolling_apply_pca(df, window_size, min_window_size=None):
    """
    Partition a dataframe into certain window size and apply func to the partitioned dataframe
    :param df dataframe: a dataframe containing the raw data
    :param window_size int: the window size into which the original dataframe will be partitioned in
    :param min_window_size: the number of minimal number of data points must be available before
                            func result is valid
    :return: a series which is the result of pca being applied to each partioned
             dataframe
    """
# ------------------------------------------------------------------------------------------------
    if min_window_size is None:
        min_window_size = window_size
    result = {}
    for i in range(1, len(df)+1):
        sub_df = df.iloc[max(i - window_size, 0):i, :] #I edited here
        if len(sub_df) >= min_window_size:
            idx = sub_df.index[-1]
            print(idx)
            result[idx] = pca_sub_df(sub_df, window_size)
    return result



def pca_sub_df(df, window_size):
    original_cols = list(df.columns)
    drop_list = []
    for oc in original_cols:
        if len(df[oc].dropna()) < window_size:
            drop_list.append(oc)
    #print(drop_list)
    clean_df = df.drop(drop_list, axis=1)
    #got rid off the columns with empty col, then pca it if it is possible
   
    #if there are less than 2 columns then there is no point.
    if len(clean_df.columns) >= 2:
    
        return clean_df
    else:
        #need to return some empty default values
        return None
    
    
    



valuation_data = pd.read_csv('C:/Dev/Models/QuantModels/#applications/scorecard/MarketDataRegimeScore_Valuation.csv')                    
valuation_data['Date'] = pd.to_datetime(valuation_data['Date'], format = '%d/%m/%Y')
valuation_data = valuation_data.set_index('Date').ffill().apply(pd.to_numeric)


valuation_pcas = rolling_apply_pca(valuation_data,75, min_window_size=75)


#test2 = pca_sub_df(test1.tail(99),99)

pca.PCA(test1[1], rates_pca = True)
