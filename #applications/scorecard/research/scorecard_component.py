from __future__ import division
import pandas as pd 
import numpy as np
import _path
import os, sys, inspect
import logging
from collections import namedtuple
import common.dirs as dirs
import common.emails as emails
import data.database as db
import _config as cfg


def get_sov_rates():
    q = r"""SELECT [AsOfDate],[Security],[Value] FROM [Fixed_Income].[data].[SC_SOV]"""
    return q

def get_sov_bei():
    q = r"""SELECT [AsOfDate],[Security],[Value] FROM [Fixed_Income].[data].[SC_SOV_BE]"""
    return q

def get_fwis():
    q = r"""SELECT [AsOfDate],[Security],[Value] FROM [Fixed_Income].[data].[SC_FWIS]"""
    return q

def get_s_pmi_diffusion():
    q = r"""SELECT [Date],[BM1M_CHG] [CHG1M],[BM3M_CHG] [CHG3M],[BM6M_CHG] [CHG6M],[BM12M_CHG] [CHG12M]
            FROM [Fixed_Income].[pmi].[V_pmi_service_breadth_Model]"""
    return q

def get_m_pmi_diffusion():
    q = r"""SELECT [Date],[BM1M_CHG] [CHG1M],[BM3M_CHG] [CHG3M],[BM6M_CHG] [CHG6M],[BM12M_CHG] [CHG12M]
            FROM [Fixed_Income].[pmi].[V_pmi_manufacoring_Breadth_Model]"""
    return q


def get_monthly_epi():
    q = r"""
SELECT * FROM [Fixed_Income].[epi].[V_SC_EPI_Monthly]"""
    return q


def get_quarterly_epi():
    q = r"""
SELECT * FROM [Fixed_Income].[epi].[V_SC_EPI_Quarterly]"""
    return q

def pivot_sov_rates(unpivoted_data):
    #print unpivoted_data
    Result = namedtuple('Result', ['sov_2', 'sov_5', 'sov_10'])
    pivoted_data = unpivoted_data.pivot(index='AsOfDate', columns = 'Security', values = 'Value')
    renamed_data = pivoted_data.rename(cfg.SOV_NAMES, axis = 'columns').reindex(cfg.DATE_IDX).ffill()
    sov_2y = renamed_data[[col for col in renamed_data.columns if '_2Y' in col]] \
                .rename(lambda n: n.replace('_2Y',''), axis='columns') \
                .reindex(cfg.EXPORT_COLS, axis='columns')
    sov_5y = renamed_data[[col for col in renamed_data.columns if '_5Y' in col]] \
                .rename(lambda n: n.replace('_5Y',''), axis='columns') \
                .reindex(cfg.EXPORT_COLS, axis='columns')
    sov_10y = renamed_data[[col for col in renamed_data.columns if '_10Y' in col]] \
                .rename(lambda n: n.replace('_10Y',''), axis='columns') \
                .reindex(cfg.EXPORT_COLS, axis='columns')

    result = Result(sov_2=sov_2y, sov_5=sov_5y, sov_10=sov_10y )   
    return result

def pivot_sov_bei(unpivoted_data):
    #print unpivoted_data
    Result = namedtuple('Result', ['sov_bei_10'])
    pivoted_data = unpivoted_data.pivot(index='AsOfDate', columns = 'Security', values = 'Value')
    renamed_data = pivoted_data.rename(cfg.SOV_BEI_NAMES, axis = 'columns').reindex(cfg.DATE_IDX).ffill()
    sov_bei_10y = renamed_data[[col for col in renamed_data.columns if '_10Y' in col]] \
                    .rename(lambda n: n.replace('_10Y',''), axis='columns') \
                    .reindex(cfg.EXPORT_COLS, axis='columns')
    result = Result(sov_bei_10=sov_bei_10y)   
    return result

def pivot_sov_fwis(unpivoted_data):
    Result = namedtuple('Result', ['fwis_5y_5y'])
    pivoted_data = unpivoted_data.pivot(index='AsOfDate', columns = 'Security', values = 'Value')
    renamed_data = pivoted_data.rename(cfg.SOV_FWIS_NAMES, axis = 'columns').reindex(cfg.DATE_IDX).ffill()
    fwis_5y_5y = renamed_data[[col for col in renamed_data.columns if '_5Y_5Y' in col]] \
                    .rename(lambda n: n.replace('_5Y_5Y',''), axis='columns') \
                    .reindex(cfg.EXPORT_COLS, axis='columns')
    result = Result(fwis_5y_5y=fwis_5y_5y)   
    return result

def refmt_pmi(data):
    Result = namedtuple('Result', ['pmi'])
    data['Date'] = pd.to_datetime(data['Date'])
    data = data.set_index('Date').reindex(cfg.DATE_IDX).ffill()
    result = Result(pmi=data)   
    return result


def refmt_m_epi(data):
    Result = namedtuple('Result', ['epi', 'epi_pulse'])
    data['Date'] = pd.to_datetime(data['Date'])
    data = data.set_index('Date').reindex(cfg.EPI_DATE_IDX).ffill()
    data_8w_diff = (data - data.shift(cfg.EPI_M_PULSE_WINDOW_SIZE))

    result = Result(epi=data, epi_pulse = data_8w_diff)
    return result


def refmt_q_epi(data):

    def zsc(ds, window=None):
        v = ds.dropna()
        w = len(v.index) if isinstance(window, (int, long)) else window
        windows = v.rolling(window=w, min_periods =w)
        windows_mean = windows.mean()
        windows_stdev = windowws.std()
        zsc = ((v - windows_mean)/windows_stdev).replace([np.inf, -np.inf], np.nan).reindex(ds.index)
        return zsc

    def diffusion(row):
        count = row.dropna().count()
        increase = row.map(lambda i: 1 if i > 0 else 0).sum()
        return 0 if count == 0 else increase/count
    
    Result = namedtuple('Result', ['epi','data_3m_diff_zsc', 'global_3m_diffusion' , 'g10_3m_diffusion', 'asia_3m_diffusion'])
    data['Date'] = pd.to_datetime(data['Date'])
    data = data.set_index('Date').reindex(cfg.EPI_DATE_IDX).ffill()
    
    data_3m_diff = (data - data.shift(cfg.EPI_Q_REGIME_WINDOW_SIZE))
    data_3m_diff_zsc = pd.DataFrame({ col: data_3m_diff[col] for col in data_3m_diff.columns})

    global_3m_diffusion = pd.DataFrame(
        {'headline': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Headline' in col and cfg.IsGlobal(col)}).apply(diffusion, axis=1),
         'business': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Business' in col and cfg.IsGlobal(col)}).apply(diffusion, axis=1),
         'consumer': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Consumer' in col and cfg.IsGlobal(col)}).apply(diffusion, axis=1), 
         'growth': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Growth' in col and cfg.IsGlobal(col)}).apply(diffusion, axis=1), 
         'employment': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Employment' in col and cfg.IsGlobal(col)}).apply(diffusion, axis=1),
         'exinflation': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'ExInflation' in col and cfg.IsGlobal(col)}).apply(diffusion, axis=1),
         'inflation': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Inflation' in col and cfg.IsGlobal(col)}).apply(diffusion, axis=1)
        }
        )


    g10_3m_diffusion = pd.DataFrame(
        {'headline': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Headline' in col and cfg.IsG10(col)}).apply(diffusion, axis=1),
         'business': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Business' in col and cfg.IsG10(col)}).apply(diffusion, axis=1),
         'consumer': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Consumer' in col and cfg.IsG10(col)}).apply(diffusion, axis=1), 
         'growth': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Growth' in col and cfg.IsG10(col)}).apply(diffusion, axis=1), 
         'employment': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Employment' in col and cfg.IsG10(col)}).apply(diffusion, axis=1),
         'exinflation': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'ExInflation' in col and cfg.IsG10(col)}).apply(diffusion, axis=1),
         'inflation': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Inflation' in col and cfg.IsG10(col)}).apply(diffusion, axis=1)
        }
        )

    asia_3m_diffusion = pd.DataFrame(
        {'headline': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Headline' in col and cfg.IsAsia(col)}).apply(diffusion, axis=1),
         'business': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Business' in col and cfg.IsAsia(col)}).apply(diffusion, axis=1),
         'consumer': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Consumer' in col and cfg.IsAsia(col)}).apply(diffusion, axis=1), 
         'growth': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Growth' in col and cfg.IsAsia(col)}).apply(diffusion, axis=1), 
         'employment': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Employment' in col and cfg.IsAsia(col)}).apply(diffusion, axis=1),
         'exinflation': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'ExInflation' in col and cfg.IsAsia(col)}).apply(diffusion, axis=1),
         'inflation': pd.DataFrame({ col:data_3m_diff[col] for col in data_3m_diff.columns if 'Inflation' in col and cfg.IsAsia(col)}).apply(diffusion, axis=1)
        }
        )



    result = Result(epi=data, data_3m_diff_zsc = data_3m_diff_zsc, global_3m_diffusion = global_3m_diffusion, asia_3m_diffusion = asia_3m_diffusion, g10_3m_diffusion = g10_3m_diffusion)
    return result



if __name__ == '__main__':

    _exit_code = 9999

    try:
       
        logging.info("<<<< {0} starts >>>>".format(cfg.APP_NAME))
        dirs.archive_files(cfg.DEBUG_FOLDER, cfg.ARCHIVE_DEBUG_FOLDER, copy_only = False)
        dirs.archive_files(cfg.OUTPUT_FOLDER, cfg.ARCHIVE_OUTPUT_FOLDER, copy_only = False)
        _db = db.Database(cfg.FI_PROD_DB_CONN_STRING, cfg.DEBUG_FOLDER)


        #sov rates
        logging.info("=====> {0} data starts".format('sov rates'))
        sov_rates_data = _db.query(get_sov_rates())
        logging.info("=====> {0} reformat starts".format('sov rates'))
        sov_rates = pivot_sov_rates(sov_rates_data)
        
        #sov bei
        logging.info("=====> {0} data starts".format('sov bei'))
        sov_bei_rates_data = _db.query(get_sov_bei())
        logging.info("=====> {0} reformat starts".format('sov bei'))
        sov_bei_rates = pivot_sov_bei(sov_bei_rates_data)
        
        #fwis
        logging.info("=====> {0} data starts".format('fwis'))
        fwis_data = _db.query(get_fwis())
        logging.info("=====> {0} reformat starts".format('fwis'))
        fwis = pivot_sov_fwis(fwis_data)

        #m_pmi 
        logging.info("=====> {0} data starts".format('m pmi'))
        m_pmi_data = _db.query(get_m_pmi_diffusion())
        logging.info("=====> {0} reformat starts".format('m pmi'))
        m_pmi = refmt_pmi(m_pmi_data)

        logging.info("=====> {0} data starts".format('s pmi'))
        s_pmi_data = _db.query(get_s_pmi_diffusion())
        logging.info("=====> {0} reformat starts".format('s pmi'))
        s_pmi = refmt_pmi(s_pmi_data)

        #m epi
        logging.info("=====> {0} data starts".format('m epi'))
        m_epi_data = _db.query(get_monthly_epi())
        logging.info("=====> {0} reformat starts".format('m epi'))
        m_epi_ouput = refmt_m_epi(m_epi_data)

        
        #q epi
        logging.info("=====> {0} data starts".format('q epi'))
        q_epi_data = _db.query(get_quarterly_epi())
        q_epi_ouput = refmt_q_epi(q_epi_data)
        
        
        #output 
        logging.info("=====> {0} output starts".format('sov rates'))
        sov_rates.sov_2.to_csv(os.path.join(cfg.OUTPUT_FOLDER,'sov_2y.csv'), index_label ='Date')
        sov_rates.sov_5.to_csv(os.path.join(cfg.OUTPUT_FOLDER,'sov_5y.csv'), index_label='Date')
        sov_rates.sov_10.to_csv(os.path.join(cfg.OUTPUT_FOLDER,'sov_10y.csv'), index_label='Date')

        logging.info("=====> {0} output starts".format('sov bei'))
        sov_bei_rates.sov_bei_10.to_csv(os.path.join(cfg.OUTPUT_FOLDER,'sov_bei_10y.csv'), index_label ='Date')

        logging.info("=====> {0} output starts".format('fwis'))
        fwis.fwis_5y_5y.to_csv(os.path.join(cfg.OUTPUT_FOLDER,'fwis_5y_5y.csv'), index_label ='Date')

        logging.info("=====> {0} output starts".format('m pmi'))
        m_pmi.pmi.to_csv(os.path.join(cfg.OUTPUT_FOLDER,'m_pmi_breadth.csv'), index_label ='Date')

        logging.info("=====> {0} output starts".format('s pmi'))
        s_pmi.pmi.to_csv(os.path.join(cfg.OUTPUT_FOLDER,'s_pmi_breadth.csv'), index_label ='Date')

        logging.info("=====> {0} output starts".format('m epi'))
        m_epi_ouput.epi.to_csv(os.path.join(cfg.OUTPUT_FOLDER,'epi_monthly.csv'), index_label ='Date')
        m_epi_ouput.epi_pulse.to_csv(os.path.join(cfg.OUTPUT_FOLDER,'epi_monthly_8w_diff.csv'), index_label ='Date')

        logging.info("=====> {0} output starts".format('q epi'))
        m_epi_ouput.epi.to_csv(os.path.join(cfg.OUTPUT_FOLDER,'epi_quarterly.csv'), index_label ='Date')
        q_epi_ouput.data_3m_diff_zsc.to_csv(os.path.join(cfg.OUTPUT_FOLDER,'epi_quarterly_3m_diff_zsc.csv'), index_label ='Date')

        logging.info("=====> {0} output starts".format('q epi diffusion'))
        q_epi_ouput.global_3m_diffusion.to_csv(os.path.join(cfg.OUTPUT_FOLDER,'global_epi_quarterly_3m_diffusion.csv'), index_label ='Date')
        q_epi_ouput.g10_3m_diffusion.to_csv(os.path.join(cfg.OUTPUT_FOLDER,'g10_epi_quarterly_3m_diffusion.csv'), index_label ='Date')
        q_epi_ouput.asia_3m_diffusion.to_csv(os.path.join(cfg.OUTPUT_FOLDER,'asia_epi_quarterly_3m_diffusion.csv'), index_label ='Date')



        
        logging.info("<<<< {0} ends SUCCESSFULLY >>>>".format(cfg.APP_NAME))
        _exit_code = 0
    except:
        logging.exception("!!! Error detected in {0}".format(cfg.APP_NAME))
        logging.ERROR("<<<< {0} ends UNSUCCESSFULLY >>>>".format(cfg.APP_NAME))
        _exit_code = 9999
    finally:
        _result = dirs.check_log_file_result(cfg.LOG_PATH)
        _title = "{0} process - {1}".format(cfg.APP_NAME, _result)
        _email_body = dirs.htmlfy_log_file(cfg.LOG_PATH)
        emails.send_SMTP_mail(cfg.SENDER, cfg.RECEIVERS, _title, _email_body, cfg.CC, [cfg.LOG_PATH])
        sys.exit(_exit_code)

