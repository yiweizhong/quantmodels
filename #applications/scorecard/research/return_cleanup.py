from __future__ import division
import pandas as pd
import numpy as np
import xlwings as xw


return_data_wb = xw.books['DataBaseAllSeriesYw.xlsx']

return_data = return_data_wb.sheets['Sheet1'].range('A1').options(pd.DataFrame, expand='table').value



countries = ['AUD', 'USD', 'EUR', 'GBP', 'CNY', 'JPY', 'KRW']         

return_data_for_countries_col = [col for col in return_data.columns if sum(map(lambda c: 1 if c.upper() in col.upper() else 0, countries)) > 0] 

for r in return_data.columns:
    if 'equity'.upper() in r.upper():
        print r


def get_return(return_data, suffix):
    countries = ['AUD', 'USD', 'EUR', 'GBP', 'CNY', 'JPY', 'KRW']
    return_data_for_countries_col = [col for col in return_data.columns if sum(map(lambda c: 1 if c.upper() in col.upper() else 0, countries)) > 0] 
    return_cols = [col for col in return_data_for_countries_col if suffix.upper() in col.upper()]
    filtered_return_data = return_data[return_cols].rename(lambda n: n.upper().replace(suffix.upper(),''), axis='columns')
    return filtered_return_data 
    
def add_missing_country(return_data):
    for c in countries:
        if c not in return_data.columns:
            return_data[c] = np.NaN
    return return_data

#real yield
frontend_real_yield_returns = add_missing_country(get_return(return_data, '2Real Return (Total)').resample('M').sum(min_count = 1).shift(-1))
mid_curve_real_yield_returns = add_missing_country(get_return(return_data, '5Real Return (Total)').resample('M').sum(min_count = 1).shift(-1))
backend_real_yield_returns = add_missing_country(get_return(return_data, '10Real Return (Total)').resample('M').sum(min_count = 1).shift(-1))


#real yield steepener
real_yield_curve_returns = backend_real_yield_returns - frontend_real_yield_returns

#inflation
frontend_inflation_returns = add_missing_country(get_return(return_data, '2Inflation Returns (Total)').resample('M').sum(min_count = 1).shift(-1))
mid_curve_inflation_returns = add_missing_country(get_return(return_data, '5Inflation Returns (Total)').resample('M').sum(min_count = 1).shift(-1))
backend_inflation_returns = add_missing_country(get_return(return_data, '10Inflation Returns (Total)').resample('M').sum(min_count = 1).shift(-1))


#inflation curve 
inflation_curve_returns = add_missing_country(get_return(return_data, '2s10sInflation Curve (Total)').resample('M').sum(min_count = 1).shift(-1))


# credit short end
credit_short_end_returns = add_missing_country(
        pd.DataFrame({'USD': return_data[['ALLUSTotalAAA2', 'ALLUSTotalAA2', 'ALLUSTotalA2', 'ALLUSTotalBBB2']].apply(np.nanmean, axis=1),
                      'EUR': return_data[['ALLEUTotalAAA2', 'ALLEUTotalAA2', 'ALLEUTotalA2', 'ALLEUTotalBBB2']].apply(np.nanmean, axis=1),
                      'GBP': return_data[['ALLUKTotalAAA2', 'ALLUKTotalAA2', 'ALLUKTotalA2', 'ALLUKTotalBBB2']].apply(np.nanmean, axis=1)}
    ).resample('M').sum(min_count = 1).shift(-1)
)

        

# credit long end
credit_long_end_returns = add_missing_country(
        pd.DataFrame({'USD': return_data[['ALLUSTotalAAA10', 'ALLUSTotalAA10', 'ALLUSTotalA10', 'ALLUSTotalBBB10']].apply(np.nanmean, axis=1),
                      'EUR': return_data[['ALLEUTotalAAA10', 'ALLEUTotalAA10', 'ALLEUTotalA10', 'ALLEUTotalBBB10']].apply(np.nanmean, axis=1),
                      'GBP': return_data[['ALLUKTotalAAA10', 'ALLUKTotalAA10', 'ALLUKTotalA10', 'ALLUKTotalBBB10']].apply(np.nanmean, axis=1)}
    ).resample('M').sum(min_count = 1).shift(-1)
)

        
        

# equity
equity_returns = add_missing_country(get_return(return_data, 'Total ReturnEquity').resample('M').sum(min_count = 1).shift(-1))



#output

output = pd.concat(
        [frontend_real_yield_returns.rename(lambda n: n + '_FE_RR', axis='columns'),
         mid_curve_real_yield_returns.rename(lambda n: n + '_MC_RR', axis='columns'),
         backend_real_yield_returns.rename(lambda n: n + '_LE_RR', axis='columns'),
         real_yield_curve_returns.rename(lambda n: n + '_RR_CRV', axis='columns'),
         frontend_inflation_returns.rename(lambda n: n + '_FE_INFL', axis='columns'),
         mid_curve_inflation_returns.rename(lambda n: n + '_MC_INFL', axis='columns'),
         backend_inflation_returns.rename(lambda n: n + '_LE_INFL', axis='columns'),
         inflation_curve_returns.rename(lambda n: n + '_INFL_CRV', axis='columns'),
         credit_short_end_returns.rename(lambda n: n + '_SE_CR', axis='columns'),
         credit_long_end_returns.rename(lambda n: n + '_LE_CR', axis='columns'),
         equity_returns.rename(lambda n: n + '_EQ', axis='columns')
        ], axis=1, join='outer').ffill()
        
output.transpose().to_csv(r'C:/Temp/test/scorecard_component/output/returns.csv', index_label ='Date')
        