from __future__ import division
import _path
import _config as cfg
import logging
import os, sys, inspect
import time, datetime
from pandas.tseries.offsets import *
from collections import namedtuple
import argparse
import common.dirs as dirs
import common.emails as emails
import component_growth as growth
import component_fc as fc
import data.database as db
import pandas as pd 
from tia.bbg import LocalTerminal as bbgterminal


df = pd.DataFrame(data={'a':[11,22,33], 'b':[4,5,6]}, index=[10,20,30]) 
df.index.name = 'id'

print(df)

_db = db.Database(cfg.FI_PROD_DB_CONN_STRING, cfg.DEBUG_FOLDER)

df.to_sql('test_df',_db.engine, schema='dev', if_exists='replace', index=True, index_label='Date')

print(df)


from collections import namedtuple

Result = namedtuple('Result',['A','B'])
Result2 = namedtuple('Result2',['C','D'])

r1 = Result(**{'A':1, 'B':2}) 
r2 = Result2(**{'C':3, 'D':4}) 

def filterResults(results, filters):
    outputs = {}
    for result in results:
        for prop in result._fields:
            if prop in filters:
                outputs[prop] = getattr(result, prop)
    return outputs

for key in filterResults([r1, r2], ['B','D']).keys():
    print(key)