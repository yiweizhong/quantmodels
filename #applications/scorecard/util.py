from __future__ import division
from collections import namedtuple
import os
import numpy as np
import pandas as pd




def count_countinuous_pos(s):
    result = {}
    runcount = 0
    for (idx, v) in s.iteritems():        
        if (v > 0):
            #print idx
            runcount = runcount + 1
        else:
            runcount = 0
        result[idx] = runcount
    return result



def dumptoCsvMonthEnd(path, named_tuple_dfs):
    for prop in named_tuple_dfs._fields:
        df = getattr(named_tuple_dfs, prop)
        df = df.resample('M').last()
        df.to_csv(os.path.join(path, prop + '.csv'), index_label ='Date')


def dumptoCsv(path, named_tuple_dfs):
    for prop in named_tuple_dfs._fields:
        df = getattr(named_tuple_dfs, prop)
        df.to_csv(os.path.join(path, prop + '.csv'), index_label ='Date')


def dumpNamedTupletoExcel(path, named_tuple_dfs):
    writer = pd.ExcelWriter(path)
    for prop in named_tuple_dfs._fields:
        df = getattr(named_tuple_dfs, prop)
        df.to_excel(writer, prop, index_label ='Date')
    writer.save()

def dumptoExcel(path, dfs_dict):
    writer = pd.ExcelWriter(path, datetime_format='yyyy-MM-dd', date_format='%Y-%m-%d')
    for key in dfs_dict.keys():
        df = dfs_dict[key]
        df.to_excel(writer, key, index_label ='Date')
    writer.save()

def dumptoDatabase(db_conn, schema, dfs_dict, dropblanks=False):
    for key in dfs_dict.keys():
        df = dfs_dict[key].dropna() if dropblanks else dfs_dict[key] 
        df.to_sql(key, db_conn.engine, schema=schema, if_exists='replace', index=True, index_label ='Date')
    


def normToThree(e):

    if np.isnan(e):
        return np.NaN

    if e >= 3:
        return 3
    elif e <= -3:
        return -3
    else: 
        return round(e)

def simpleNorml(e, h, l):

    if np.isnan(e):
        return np.NaN

    if e >= h:
        return h
    elif e <= l:
        return l
    else:
        fraction = abs(e) % 1
        
        if fraction >= 0.5:
            return (abs(int(e)) + 1) * np.sign(e)
        elif fraction < 0.5 and fraction >0:
            return abs(int(e + 0.5)) * np.sign(e)
        else:
            return abs(int(e)) * np.sign(e)

def trim_series(ts):
    fi = ts.first_valid_index()
    li = ts.last_valid_index()
    return ts[fi:li]

def ltrim(pdobj):
    fi = pdobj.first_valid_index()
    return pdobj[fi:]

def rtrim(pdobj):
    li = pdobj.first_valid_index()
    return pdobj[:li]


def momentum(data, window):
    return (data - data.shift(window)).reindex(data.index)


def expanding_standardise_table(df,
                                fill_missing_method=r"ffill",
                                start=None,
                                end=None,
                                bday_only=True,
                                fill_missing_at_the_end =False,
                                zero_mean = False,
                                panel_process = False
                                ):
    """
    :param df:
    :param fill_missing_method:
    :param start:
    :param end:
    :param bday_only:
    :param fill_missing_at_the_end: a flag decides whether to fill the missing value at the end AFTER the calculation
    :return:
    """

    input_df_index_name = df.index.name

    start_date = df.sort_index().index.min() if start is None else start
    end_date = df.sort_index().index.max() if end is None else end
    keys = pd.bdate_range(start=start_date,end=end_date) if bday_only else pd.date_range(start=start_date,end=end_date)
    cols = df.columns.tolist()
    raw_df = df.reindex(keys)
    
    expanding_zscore_df = None

    if(panel_process == False):
        expanding_zscore_list = []
        for col in df.columns.tolist():
            raw_data = raw_df[col].sort_index().dropna()
            raw_data = trim_series (raw_df[col].sort_index()).fillna(method=fill_missing_method).dropna()
            expanding_avg = raw_data.expanding(min_periods=1).mean() if zero_mean == False else 0
            expanding_stdev = raw_data.expanding(min_periods=1).std()
            expanding_zscore_series = ((raw_data - expanding_avg)/expanding_stdev) \
                                    .replace([np.inf, -np.inf], np.nan) \
                                    .reindex(keys) \
                                    .fillna(method=fill_missing_method) 
            expanding_zscore_list.append(pd.DataFrame(expanding_zscore_series,columns=[col]))

        expanding_zscore_df = pd.concat(expanding_zscore_list, join='outer', axis=1)
    else:
        # to be done!!!!!
        raise NotImplementedError("panel zscore not done yet!!!")
    
    
    if fill_missing_at_the_end: expanding_zscore_df = expanding_zscore_df.fillna(method=fill_missing_method)[cols]

    # reapply the index name from the input df
    expanding_zscore_df.index.name = input_df_index_name

    return expanding_zscore_df
