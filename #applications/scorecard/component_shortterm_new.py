from __future__ import division
import pandas as pd 
import numpy as np
import _path
import os, sys, inspect
import logging
from collections import namedtuple
from datetime import datetime
import common.dirs as dirs
import common.emails as emails
import data.datatools as datatools
import data.database as db
from util import *
import _config as cfg


component_name = 'Shortterm'

SOV_NAMES = {
        'GACGB2 INDEX': 'AUD_2Y',
        'GACGB5 INDEX': 'AUD_5Y',
        'GACGB10 INDEX': 'AUD_10Y',
        'USGG2YR INDEX': 'USD_2Y',
        'USGG5YR INDEX': 'USD_5Y',
        'USGG10YR INDEX':'USD_10Y',
        'GCAN2YR INDEX': 'CAD_2Y',
        'GCAN5YR INDEX': 'CAD_5Y',
        'GCAN10YR INDEX': 'CAD_10Y',
        'GCNY2YR INDEX': 'CNY_2Y',
        'GCNY5YR INDEX': 'CNY_5Y',
        'GCNY10YR INDEX': 'CNY_10Y',
        'GDBR2 INDEX': 'EUR_2Y',
        'GDBR5 INDEX': 'EUR_5Y',
        'GDBR10 INDEX': 'EUR_10Y',
        'GJGB2 INDEX': 'JPY_2Y',
        'GJGB5 INDEX': 'JPY_5Y',
        'GJGB5 INDEX': 'JPY_10Y',
        'GNZGB2 INDEX': 'NZD_2Y',
        'GNZGB5 INDEX': 'NZD_5Y',
        'GNZGB10 INDEX': 'NZD_10Y',
        'GUKG2 INDEX': 'GBP_2Y',
        'GUKG5 INDEX': 'GBP_5Y',
        'GUKG10 INDEX': 'GBP_10Y',
        'GVSK2YR INDEX': 'KRW_2Y',
        'GVSK5YR INDEX': 'KRW_5Y',
        'GVSK10YR INDEX': 'KRW_10Y',
    }

SWAP_NAMES = {
    'SD0302FS 1Y1Y BLC Curncy':'AUD_1Y_1Y',
    'SD0023FS 1Y1Y BLC Curncy':'USD_1Y_1Y',
    'SD0004FS 1Y1Y BLC Curncy':'CAD_1Y_1Y',
    'SD0045FS 1Y1Y BLC Curncy':'EUR_1Y_1Y',
    'SD0013FS 1Y1Y BLC Curncy':'JPY_1Y_1Y',
    'SD0015FS 1Y1Y BLC Curncy':'NZD_1Y_1Y',
    'BPSW0101 BLC Curncy':'GBP_1Y_1Y'

}

CASH_NAMES = {
    'FDTR Index': 'USD',
    'RBACTRD Index':'AUD'
}

INFLATION_NAMES = {
    'PCE CMOM Index':'USD',
    'RBCPTRIQ Index':'AUD'
}

INFLATION_BEI10_NAMES = {
    'ADGGBE10 INDEX': 'AUD',
    'USGGBE10 INDEX': 'USD',
}

SOV_BEI_NAMES = {
        'ADGGBE10 INDEX': 'AUD_10Y',
        'USGGBE10 INDEX': 'USD_10Y',
        'CDGGBE10 INDEX': 'CAD_10Y',
        'EUSWSB10 INDEX': 'EUR_10Y',
        'JYGGBE10 INDEX': 'JPY_10Y',
        'NDGGBE10 INDEX': 'NZD_10Y',
        'UKGGBE10 INDEX': 'GBP_10Y',
        'SKGGBE10 INDEX': 'SEK_10Y',
        'DEGGBE10 INDEX': 'DEU_10Y',
        'FRGGBE10 INDEX': 'FRA_10Y',
        'GILGBE10 INDEX': 'ITA_10Y',
        'SPGGBE10 INDEX': 'SPA_10Y'
}

names_2 = ['SK', 'AU', 'CA', 'US', 'CH', 'JN', 'EU', 'UK','NZ','GLOBALGDP']
names_3 = ['KRW', 'AUD', 'CAD', 'USD', 'CNY', 'JPY','EUR','GBP', 'NZD', 'GLOBAL']
names_4 = ['AUD', 'USD', 'CAD','EUR', 'JPY', 'CNY', 'KRW', 'GBP','NZD']

def get_sov_rates():
    q = r"""SELECT [AsOfDate],[Security],[Value] FROM [Fixed_Income].[data].[SC_SOV]"""
    return q

def get_swap_rates():
    q = r"""SELECT [AsOfDate],[Security],[Field],[Value] FROM [Fixed_Income].[data].[SC_SWAP]"""
    return q

def get_real_cash_rates():
    q = r"""SELECT [AsOfDate],[Security],[Field],[Value] FROM [Fixed_Income].[data].[SC_SOV_BE] union all
            SELECT [AsOfDate],[Security],[Field],[Value] FROM [Fixed_Income].[data].[SC_INFLATION] union all 
            SELECT [AsOfDate],[Security],[Field],[Value] FROM [Fixed_Income].[data].[SC_CASH]"""
    return q


def get_sov_bei():
    q = r"""SELECT [AsOfDate],[Security],[Value] FROM [Fixed_Income].[data].[SC_SOV_BE]"""
    return q

def get_fwis():
    q = r"""SELECT [AsOfDate],[Security],[Value] FROM [Fixed_Income].[data].[SC_FWIS]"""
    return q

def get_s_pmi_diffusion():
    q = r"""SELECT [Date],[BM1M_CHG] [CHG1M],[BM3M_CHG] [CHG3M],[BM6M_CHG] [CHG6M],[BM12M_CHG] [CHG12M]
            FROM [Fixed_Income].[pmi].[V_pmi_service_breadth_Model]"""
    return q

def get_m_pmi_diffusion():
    q = r"""SELECT [Date],[BM1M_CHG] [CHG1M],[BM3M_CHG] [CHG3M],[BM6M_CHG] [CHG6M],[BM12M_CHG] [CHG12M]
            FROM [Fixed_Income].[pmi].[V_pmi_manufacoring_Breadth_Model]"""
    return q


def get_monthly_epi():
    q = r"""
SELECT * FROM [Fixed_Income].[Scorecard].[V_EPI_Monthly]"""
    return q

def get_cftc():
    q = r"""
        SELECT [Date],[z_CFTC_3M],[z_CFTC_2Y],[z_CFTC_5Y],[z_CFTC_10Y],[z_CFTC_30Y],[z_CFTC_ULTRA] ,[z_CFTC]
        FROM [Fixed_Income].[Positioning].[v_CFTC]
    """
    return q

def get_esi():
    q = r"""
    SELECT [Date]
      ,[AU] [AUD]
      ,[CA] [CAD]
      ,[US] [USD]
      ,[CH] [CNY]
      ,[JN] [JPY]
      ,[EU] [EUR]
      ,[SK] [KRW]
      ,[UK] [GBP]
      ,[NZ] [NZD] 
      ,[GLOBAL] [GLOBAL]
      --,[GLOBAL-GDP] [GLOBAL]
  FROM [Fixed_Income].[fundamental].[v_ESI_COMBINED]
    """
    return q

def score_1_with_05(x):
    if np.isnan(x): return np.NaN
    elif x == 0: return 0
    elif 0 < x and x <= 0.5: return  0.5
    elif 0.5 < x: return  1
    elif -0.5 <= -x and x < 0: return  -0.5
    elif x < -0.5: return  -1


def score_mean_reversion(x):
    if -1 <= x and x <=1: return 0
    elif x < -1: return -1 - x
    elif x > 1: return - (x - 1) 


def score_cross_over(x, signal = 1):
    if np.isnan(x):
        return np.NaN
    elif x == 0:
        return 0
    elif x > 0 :
        return signal
    else:
        return -1 * signal


def filterResults(results, filters):
    outputs = {}
    for result in results:
        for prop in result._fields:
            if prop in filters:
                outputs[prop] = getattr(result, prop)
    return outputs



def roundToNearest(num, nearest, roundDown = True, optimizeRounding =False):

    if np.isnan(num):
        return np.NaN

    numSign = np.sign(num)
    numAbs = np.abs(num)
    nearestAbs = np.abs(nearest)
    roundDownNum = np.floor_divide(numAbs, nearestAbs) * nearestAbs
    roundUpNum = roundDownNum + nearestAbs

    if optimizeRounding:
        roundDown = True if (numAbs - roundDownNum) < (roundUpNum - numAbs) else False 
        

    if roundDown:
        return numSign * roundDownNum
    else:
        return  numSign * roundUpNum

def barrierDampen(num, barrier, dampener, belowBarrierOverride = None):
    
    if np.isnan(num):
        return np.NaN

    numSign = np.sign(num)
    numAbs = np.abs(num)

    #if there is an override value for num not reaching the barrier return the override value
    #otherwise return the num itself
    if numAbs <= barrier:
        if belowBarrierOverride is None:
            return num
        else:
            return belowBarrierOverride
    else:
        return numSign * (numAbs - dampener)

    





###############################################

def get_macro_risk_sentiment_data():
    logging.info("=====> macro risk sentiment is currently sourced from the saved down to csv version of the Combine Indicators spreadsheet. This needs to be overhauled")
    
    macro_risk_sentiment = pd.read_csv(os.path.join(cfg.INPUT_FOLDER, 'Combine Indicators.csv'),usecols=[
         'Date',
         'Macro Risk Sentiment Global',
         'GLOBAL Risk Aversion Levels 12 mth'
         ]
         ,na_values=['#DIV/0!','#VALUE!']
         ,header=3
    )
    macro_risk_sentiment['Date'] = pd.to_datetime(macro_risk_sentiment['Date'])
    macro_risk_sentiment = macro_risk_sentiment.set_index('Date').reindex(cfg.DATE_IDX).ffill() \
        .rename(columns={'Macro Risk Sentiment Global':'Macro Risk Sentiment Index', 'GLOBAL Risk Aversion Levels 12 mth':'Macro Risk Index'})
    macro_risk_sentiment['Macro Risk Index'] = -1 * macro_risk_sentiment['Macro Risk Index'] 

        
    
    return macro_risk_sentiment

def process_macro_risk_sentiment(macro_risk_sentiment_data):

    Result = namedtuple('Result', ['Macro_Risk_Sentiment', 'Data_Macro_Risk_Sentiment'])

    #1m/3m crossover signal (1m - 3m capped at +/-2)
    mrs_1m_mva = macro_risk_sentiment_data[['Macro Risk Sentiment Index']].rolling(20, min_periods=20).mean()
    mrs_3m_mva = macro_risk_sentiment_data[['Macro Risk Sentiment Index']].rolling(60, min_periods=60).mean()
    mrs_1m3m_mvacrossover_signal = (mrs_1m_mva - mrs_3m_mva).applymap(lambda x: np.sign(x) * 2 if abs(x) > 0 else 0) # cap the crossover signal at +/- 2


    #2m change signal (2m changes exp zscore round down to nearest 0.5 and capped at +/-1)
    mrs_8w_change = (macro_risk_sentiment_data[['Macro Risk Sentiment Index']] - macro_risk_sentiment_data[['Macro Risk Sentiment Index']].shift(40))
    mrs_8w_change_exp_zscr = expanding_standardise_table(mrs_8w_change,fill_missing_at_the_end =True, zero_mean=True)
    mrs_8w_change_signal = mrs_8w_change_exp_zscr.applymap(lambda x: roundToNearest(x, 0.5, roundDown=True)).applymap(lambda x: np.sign(x) * 1 if abs(x) > 1 else x)
    

    #macro risk sentiment momentum indicator
    macro_risk_sentiment_momentum_indicator = mrs_1m3m_mvacrossover_signal + mrs_8w_change_signal.applymap(lambda x: np.nan if np.isnan(x) else np.sign(x))



    #macro risk sentiment mean reversion indicator
    macro_risk_mean_reversion_indicator = macro_risk_sentiment_data[['Macro Risk Index']] \
                                          .applymap(lambda x: barrierDampen(x, 0.5, 0, belowBarrierOverride=0)) \
                                          .applymap(lambda x: roundToNearest(x, 0.5, roundDown=True))

    #global macro risk sentiment indicator
    global_macro_risk_sentiment_component = pd.DataFrame({'global_macro_risk_sentiment_component': 
                                                            macro_risk_sentiment_momentum_indicator[macro_risk_sentiment_momentum_indicator.columns[0]] + 
                                                            macro_risk_mean_reversion_indicator[macro_risk_mean_reversion_indicator.columns[0]]})


    result = Result(
        Macro_Risk_Sentiment=pd.concat(
            [macro_risk_sentiment_data.rename(lambda col:col, axis='columns')
            ,mrs_1m3m_mvacrossover_signal.rename(lambda col: col + '_1m3m_mvacrossover_signal', axis='columns')
            ,mrs_8w_change_signal.rename(lambda col: col + '_8w_change_signal', axis='columns')
            ,macro_risk_sentiment_momentum_indicator.rename(lambda col: col + '_momentum_inidcator', axis='columns')
            ,macro_risk_mean_reversion_indicator.rename(lambda col: col + '_meanreversion_inidcator', axis='columns')
            ,global_macro_risk_sentiment_component.rename(lambda col:  'Global_Macro_Risk_Sentiment', axis='columns')
            ], axis='columns', join='outer').ffill(),
        Data_Macro_Risk_Sentiment=pd.concat(
            [macro_risk_sentiment_data.rename(lambda col:col, axis='columns')
            ,mrs_1m_mva.rename(lambda col:col+'_1m_mva', axis='columns')
            ,mrs_3m_mva.rename(lambda col: col + '_3m_mva', axis='columns')
            ,mrs_1m3m_mvacrossover_signal.rename(lambda col: col + '_1m3m_mvacrossover_signal', axis='columns')
            ,mrs_8w_change.rename(lambda col: col + '_8w_change', axis='columns')
            ,mrs_8w_change_exp_zscr.rename(lambda col: col + '_8w_change_exp_zscr', axis='columns')
            ,mrs_8w_change_signal.rename(lambda col: col + '_change_signal', axis='columns')
            ,macro_risk_sentiment_momentum_indicator.rename(lambda col: col + '_momentum_inidcator', axis='columns')
            ,macro_risk_mean_reversion_indicator.rename(lambda col: col + '_meanreversion_inidcator', axis='columns')
            ,global_macro_risk_sentiment_component.rename(lambda col:  'Global_Macro_Risk_Sentiment', axis='columns')
            ], axis='columns', join='outer').ffill()
        )   
    
    return result

##############################################

def process_esi(esi_data):

    Result = namedtuple('Result', ['Esi', 'Data_Esi'])
        
    #1m/3m crossover signal (1m - 3m signalled at +/-2)
    esi_1m_mva = esi_data.rolling(20, min_periods=20).mean()
    esi_3m_mva = esi_data.rolling(60, min_periods=60).mean()
    esi_1m3m_mvacrossover_signal = (esi_1m_mva - esi_3m_mva).applymap(lambda x: np.sign(x) * 2 if abs(x) > 0 else 0) # crossover signal at +/- 2

    #1m change signal round up to nearest 0.5 and capped +/-1
    esi_1m_change = esi_data - esi_data.shift(20)
    esi_1m_change_signal = esi_1m_change \
                           .applymap(lambda x: barrierDampen(x, 0.1, 0, belowBarrierOverride=0)) \
                           .applymap(lambda x: roundToNearest(x, 0.5, roundDown=False, optimizeRounding=False)) \
                           .applymap(lambda x: np.nan if np.isnan(x) else np.sign(x) * (abs(x) if abs(x) < 1 else 1))


                          
    #esi momentum indicator
    esi_momentum_indicator = esi_1m3m_mvacrossover_signal + esi_1m_change_signal

    #esi mean reversion indicator (0 when below the barrier of 1, capped at -+3, rounding up to nearest 0.5)
    esi_meanreversion_inicator = esi_data \
                                 .applymap(lambda x: barrierDampen(x, 1, 1, belowBarrierOverride=0)) \
                                 .applymap(lambda x: roundToNearest(x, 0.5, roundDown=False)) \
                                 .applymap(lambda x: np.nan if np.isnan(x) else np.sign(x) * (abs(x) if abs(x) < 3 else 3)) \
                                 .applymap(lambda x: -1 * x) #for esi needs to invert the sign here for mean reversion

    #eco sentiment indicator
    eco_sentiment_indicator = esi_momentum_indicator + esi_meanreversion_inicator



    result = Result(
        Esi=pd.concat(
            [esi_data
            ,esi_momentum_indicator.rename(lambda col: col + '_momentum_indicator', axis='columns')
            ,esi_meanreversion_inicator.rename(lambda col: col + '_meanreversion_indicator', axis='columns')
            ,eco_sentiment_indicator.rename(lambda col: col + '_Eco_Sentiment', axis='columns')
            ], axis='columns', join='outer').ffill(),
        Data_Esi=pd.concat(
            [esi_data
            ,esi_1m_mva.rename(lambda col: col + '_1m_mva', axis='columns')
            ,esi_3m_mva.rename(lambda col: col + '_3m_mva', axis='columns')
            ,esi_1m3m_mvacrossover_signal.rename(lambda col: col + '_1m3m_mvacrossover_signal', axis='columns')
            ,esi_1m_change.rename(lambda col: col + '_1m_change', axis='columns')
            ,esi_1m_change_signal.rename(lambda col: col + '_1m_change_signal', axis='columns')
            ,esi_momentum_indicator.rename(lambda col: col + '_momentum_indicator', axis='columns')
            ,esi_meanreversion_inicator.rename(lambda col: col + '_meanreversion_indicator', axis='columns')
            ,eco_sentiment_indicator.rename(lambda col: col + '_Eco_Sentiment', axis='columns')
            ], axis='columns', join='outer').ffill()
        )   
    
    return result

##############################################

def get_price_action_data(unpivoted_sov_data, unpivoted_swap_data, unpivoted_real_cash_rate_data):
    logging.info("=====> price action is currently sourced from the saved down to csv version of the Combine Tech spreadsheet. This needs to be overhauled")

    spreadsheet_data = pd.read_csv(os.path.join(cfg.INPUT_FOLDER, 'Combine Tech.csv'),usecols=[
         'Date',
         'AUD 50 day MVA Slope Trend Signal',
         'USD 50 day MVA Slope Trend Signal', 
         'CAD 50 day MVA Slope Trend Signal', 
         'EUR 50 day MVA Slope Trend Signal',
         'JPY 50 day MVA Slope Trend Signal',
         'CNY 50 day MVA Slope Trend Signal',
         'KRW 50 day MVA Slope Trend Signal',
         'GBP 50 day MVA Slope Trend Signal',
         'NZD 50 day MVA Slope Trend Signal',
         'AUD 30 Day MVA Signal',
         'USD 30 Day MVA Signal', 
         'CAD 30 Day MVA Signal', 
         'EUR 30 Day MVA Signal',
         'JPY 30 Day MVA Signal',
         'CNY 30 Day MVA Signal',
         'KRW 30 Day MVA Signal',
         'GBP 30 Day MVA Signal',
         'NZD 30 Day MVA Signal',
         'AUD MACD Crossover Trend',
         'USD MACD Crossover Trend', 
         'CAD MACD Crossover Trend', 
         'EUR MACD Crossover Trend',
         'JPY MACD Crossover Trend',
         'CNY MACD Crossover Trend',
         'KRW MACD Crossover Trend',
         'GBP MACD Crossover Trend',
         'NZD MACD Crossover Trend',
         'AUD G10YR 200 Z score day count mean reversion',
         'USD G10YR 200 Z score day count mean reversion', 
         'CAD G10YR 200 Z score day count mean reversion', 
         'EUR G10YR 200 Z score day count mean reversion',
         'JPY G10YR 200 Z score day count mean reversion',
         'CNY G10YR 200 Z score day count mean reversion',
         'KRW G10YR 200 Z score day count mean reversion',
         'GBP G10YR 200 Z score day count mean reversion',
         'NZD G10YR 200 Z score day count mean reversion',
         'AUD FI Range Trader',
         'USD FI Range Trader', 
         'CAD FI Range Trader', 
         'EUR FI Range Trader',
         'JPY FI Range Trader',
         'CNY FI Range Trader',
         'KRW FI Range Trader',
         'GBP FI Range Trader',
         'NZD FI Range Trader',
         'AUD 200D MVA Slope Mean Rev Signal',
         'USD 200D MVA Slope Mean Rev Signal', 
         'CAD 200D MVA Slope Mean Rev Signal', 
         'EUR 200D MVA Slope Mean Rev Signal',
         'JPY 200D MVA Slope Mean Rev Signal',
         'CNY 200D MVA Slope Mean Rev Signal',
         'KRW 200D MVA Slope Mean Rev Signal',
         'GBP 200D MVA Slope Mean Rev Signal',
         'NZD 200D MVA Slope Mean Rev Signal'

         ]
         ,na_values=['#DIV/0!','#VALUE!']
         ,header=5
     
    )

    spreadsheet_data['Date'] = pd.to_datetime(spreadsheet_data['Date'])
    spreadsheet_data = spreadsheet_data.set_index('Date').reindex(cfg.DATE_IDX).ffill()

    #### 10y
    pivoted_sov_data = unpivoted_sov_data.pivot(index='AsOfDate', columns = 'Security', values = 'Value')
    renamed_data = pivoted_sov_data.rename(SOV_NAMES, axis = 'columns').reindex(cfg.DATE_IDX).ffill()
    renamed_data.index.name = 'Date'
    
    sov_10y = renamed_data[[col for col in renamed_data.columns if '_10Y' in col]] \
                .rename(lambda n: n.replace('_10Y',''), axis='columns') \
                .reindex(names_4, axis='columns') \
                .rename(lambda n: n + '_10Y', axis='columns') #add back _10y suffix as we have more rates now


    #### swap
    pivoted_swap_data = unpivoted_swap_data.pivot(index='AsOfDate', columns = 'Security', values = 'Value')
    renamed_data = pivoted_swap_data[list(SWAP_NAMES.keys())].rename(SWAP_NAMES, axis = 'columns').reindex(cfg.DATE_IDX).ffill()
    renamed_data.index.name = 'Date'

    swap_1y_1y = renamed_data[[col for col in renamed_data.columns if '_1Y_1Y' in col]] \
                .rename(lambda n: n.replace('_1Y_1Y',''), axis='columns') \
                .reindex(names_4, axis='columns') \
                .rename(lambda n: n + '_1Y_1Y', axis='columns') #add back _10y suffix as we have more rates now

    #### real cash rate data
    pivoted_real_cash_rate_data = unpivoted_real_cash_rate_data.pivot(index='AsOfDate', columns = 'Security', values = 'Value')
    norm_cash_rate_data = pivoted_real_cash_rate_data[list(CASH_NAMES.keys())].rename(CASH_NAMES, axis = 'columns').reindex(cfg.DATE_IDX).ffill()
    norm_cash_rate_data.index.name = 'Date'
    inflation_data = pivoted_real_cash_rate_data[list(INFLATION_NAMES.keys())].rename(INFLATION_NAMES, axis = 'columns').reindex(cfg.DATE_IDX).ffill()
    inflation_data.index.name = 'Date'
    inflation_bei_data = pivoted_real_cash_rate_data[list(INFLATION_BEI10_NAMES.keys())].rename(INFLATION_BEI10_NAMES, axis = 'columns').reindex(cfg.DATE_IDX).ffill()
    inflation_bei_data.index.name = 'Date'
    annual_inflation_data_mva = pd.DataFrame({
        'USD':inflation_data['USD'].rolling(64, min_periods=64).mean() * 12,
        'AUD':inflation_data['AUD'].rolling(64, min_periods=64).mean() * 4
    })

    real_cash_rate_data = {}

    for c in norm_cash_rate_data.columns:
        infl_data = pd.DataFrame({
            'inflation': annual_inflation_data_mva[c],
            'inlfation_bei': inflation_bei_data[c]  
        })
        infl_data = infl_data.apply(lambda row: row.dropna().mean(), axis='columns')
        real_cash_rate_data[c] = norm_cash_rate_data[c] - infl_data

    real_cash_rate_data = pd.DataFrame(real_cash_rate_data) \
                            .reindex(names_4, axis='columns') \
                            .rename(lambda n: n + '_REALCASH', axis='columns') 

    price_action_data=pd.concat([spreadsheet_data, sov_10y,swap_1y_1y, real_cash_rate_data], axis='columns', join='outer').ffill()[[
         'AUD 50 day MVA Slope Trend Signal',
         'USD 50 day MVA Slope Trend Signal', 
         'CAD 50 day MVA Slope Trend Signal', 
         'EUR 50 day MVA Slope Trend Signal',
         'JPY 50 day MVA Slope Trend Signal',
         'CNY 50 day MVA Slope Trend Signal',
         'KRW 50 day MVA Slope Trend Signal',
         'GBP 50 day MVA Slope Trend Signal',
         'NZD 50 day MVA Slope Trend Signal',
         'AUD 30 Day MVA Signal',
         'USD 30 Day MVA Signal', 
         'CAD 30 Day MVA Signal', 
         'EUR 30 Day MVA Signal',
         'JPY 30 Day MVA Signal',
         'CNY 30 Day MVA Signal',
         'KRW 30 Day MVA Signal',
         'GBP 30 Day MVA Signal',
         'NZD 30 Day MVA Signal',
         'AUD MACD Crossover Trend',
         'USD MACD Crossover Trend', 
         'CAD MACD Crossover Trend', 
         'EUR MACD Crossover Trend',
         'JPY MACD Crossover Trend',
         'CNY MACD Crossover Trend',
         'KRW MACD Crossover Trend',
         'GBP MACD Crossover Trend',
         'NZD MACD Crossover Trend',
         'AUD G10YR 200 Z score day count mean reversion',
         'USD G10YR 200 Z score day count mean reversion', 
         'CAD G10YR 200 Z score day count mean reversion', 
         'EUR G10YR 200 Z score day count mean reversion',
         'JPY G10YR 200 Z score day count mean reversion',
         'CNY G10YR 200 Z score day count mean reversion',
         'KRW G10YR 200 Z score day count mean reversion',
         'GBP G10YR 200 Z score day count mean reversion',
         'NZD G10YR 200 Z score day count mean reversion',
         'AUD FI Range Trader',
         'USD FI Range Trader', 
         'CAD FI Range Trader', 
         'EUR FI Range Trader',
         'JPY FI Range Trader',
         'CNY FI Range Trader',
         'KRW FI Range Trader',
         'GBP FI Range Trader',
         'NZD FI Range Trader',
         'AUD 200D MVA Slope Mean Rev Signal',
         'USD 200D MVA Slope Mean Rev Signal', 
         'CAD 200D MVA Slope Mean Rev Signal', 
         'EUR 200D MVA Slope Mean Rev Signal',
         'JPY 200D MVA Slope Mean Rev Signal',
         'CNY 200D MVA Slope Mean Rev Signal',
         'KRW 200D MVA Slope Mean Rev Signal',
         'GBP 200D MVA Slope Mean Rev Signal',
         'NZD 200D MVA Slope Mean Rev Signal',
         'AUD_10Y','USD_10Y','CAD_10Y','EUR_10Y','JPY_10Y','CNY_10Y','KRW_10Y',	'GBP_10Y','NZD_10Y',
         'AUD_REALCASH','USD_REALCASH','CAD_REALCASH','EUR_REALCASH','JPY_REALCASH','CNY_REALCASH','KRW_REALCASH','GBP_REALCASH','NZD_REALCASH',
         'AUD_1Y_1Y','USD_1Y_1Y','CAD_1Y_1Y','EUR_1Y_1Y','JPY_1Y_1Y','CNY_1Y_1Y','KRW_1Y_1Y','GBP_1Y_1Y','NZD_1Y_1Y',
    ]]

    return price_action_data

def process_price_action(price_action_data):

    Result =  namedtuple('Result', ['Price_action', 'Data_Price_action'])


    country_50_day_mva_slope_trend_signal = price_action_data[[
                                                'AUD 50 day MVA Slope Trend Signal',
                                                'USD 50 day MVA Slope Trend Signal', 
                                                'CAD 50 day MVA Slope Trend Signal', 
                                                'EUR 50 day MVA Slope Trend Signal',
                                                'JPY 50 day MVA Slope Trend Signal',
                                                'CNY 50 day MVA Slope Trend Signal',
                                                'KRW 50 day MVA Slope Trend Signal',
                                                'GBP 50 day MVA Slope Trend Signal',
                                                'NZD 50 day MVA Slope Trend Signal',
                                                ]].rename(lambda col: col.replace(' 50 day MVA Slope Trend Signal',''), axis='columns')


    country_30_day_mva_signal = price_action_data[[
                                    'AUD 30 Day MVA Signal',
                                    'USD 30 Day MVA Signal', 
                                    'CAD 30 Day MVA Signal', 
                                    'EUR 30 Day MVA Signal',
                                    'JPY 30 Day MVA Signal',
                                    'CNY 30 Day MVA Signal',
                                    'KRW 30 Day MVA Signal',
                                    'GBP 30 Day MVA Signal',
                                    'NZD 30 Day MVA Signal',
                                    ]].rename(lambda col: col.replace(' 30 Day MVA Signal',''), axis='columns')


    
    country_macd_crossover_trend = price_action_data[[
                                        'AUD MACD Crossover Trend',
                                        'USD MACD Crossover Trend', 
                                        'CAD MACD Crossover Trend', 
                                        'EUR MACD Crossover Trend',
                                        'JPY MACD Crossover Trend',
                                        'CNY MACD Crossover Trend',
                                        'KRW MACD Crossover Trend',
                                        'GBP MACD Crossover Trend',
                                        'NZD MACD Crossover Trend'
                                        ]].rename(lambda col: col.replace(' MACD Crossover Trend',''), axis='columns')
                                


    country_200_day_zscore_day_count_mean_reversion = price_action_data[[
         'AUD G10YR 200 Z score day count mean reversion',
         'USD G10YR 200 Z score day count mean reversion', 
         'CAD G10YR 200 Z score day count mean reversion', 
         'EUR G10YR 200 Z score day count mean reversion',
         'JPY G10YR 200 Z score day count mean reversion',
         'CNY G10YR 200 Z score day count mean reversion',
         'KRW G10YR 200 Z score day count mean reversion',
         'GBP G10YR 200 Z score day count mean reversion',
         'NZD G10YR 200 Z score day count mean reversion',
         ]].rename(lambda col: col.replace(' G10YR 200 Z score day count mean reversion',''), axis='columns')



    country_FI_range_trader = price_action_data[[
                                'AUD FI Range Trader',
                                'USD FI Range Trader', 
                                'CAD FI Range Trader', 
                                'EUR FI Range Trader',
                                'JPY FI Range Trader',
                                'CNY FI Range Trader',
                                'KRW FI Range Trader',
                                'GBP FI Range Trader',
                                'NZD FI Range Trader',
                                ]].rename(lambda col: col.replace(' FI Range Trader',''), axis='columns')


    #
    country_200_day_mva_slope_mean_rev_signal =  price_action_data[[
                                                    'AUD 200D MVA Slope Mean Rev Signal',
                                                    'USD 200D MVA Slope Mean Rev Signal', 
                                                    'CAD 200D MVA Slope Mean Rev Signal', 
                                                    'EUR 200D MVA Slope Mean Rev Signal',
                                                    'JPY 200D MVA Slope Mean Rev Signal',
                                                    'CNY 200D MVA Slope Mean Rev Signal',
                                                    'KRW 200D MVA Slope Mean Rev Signal',
                                                    'GBP 200D MVA Slope Mean Rev Signal',
                                                    'NZD 200D MVA Slope Mean Rev Signal',
                                                    ]].rename(lambda col: col.replace(' 200D MVA Slope Mean Rev Signal',''), axis='columns')
 

    sov_10y = price_action_data[[
                'AUD_10Y','USD_10Y','CAD_10Y','EUR_10Y','JPY_10Y','CNY_10Y','KRW_10Y',	'GBP_10Y','NZD_10Y'
                ]].rename(lambda col: col.replace('_10Y',''), axis='columns')


    #sov10 1m/3m crossover signal (1m - 3m signalled at +/-1)
    sov10_1m_mva = sov_10y.rolling(20, min_periods=20).mean()
    sov10_3m_mva = sov_10y.rolling(60, min_periods=60).mean()
    sov10_1m3m_mvacrossover_signal = (sov10_1m_mva - sov10_3m_mva).applymap(lambda x: np.sign(x) * 1 if abs(x) > 0 else 0) # crossover signal at +/- 1


    #sov10 1m change rolling 200 days zscore signal
    sov_10y_1m_change_200d_zscr = (sov_10y - sov_10y.shift(20)).rolling(200, min_periods=200).apply(lambda s: (s[-1] - np.mean(s))/np.std(s, ddof=1), raw=True) \
                                        .applymap(lambda x: np.nan if np.isnan(x) else np.sign(x) * 1 if abs(x) > 0.5 else 0)    


    price_action_momentum_signal = ((country_50_day_mva_slope_trend_signal * 2 \
                                      + country_30_day_mva_signal \
                                      + country_macd_crossover_trend / 3 \
                                      + sov10_1m3m_mvacrossover_signal \
                                      + sov_10y_1m_change_200d_zscr) \
                                      / 2) \
                                      .applymap(lambda x: roundToNearest(x, 0.5, roundDown=True)) \
                                      .applymap(lambda x: np.nan if np.isnan(x) else np.sign(x) * (abs(x) if abs(x) < 3 else 3)) 

    
     

    price_action_momentum_signal ['GLOBAL'] = price_action_momentum_signal.apply(lambda row: row.dropna().mean(), axis='columns') 
    

    price_action_meanreversion_signal = (country_200_day_zscore_day_count_mean_reversion / 2 \
                                            + country_FI_range_trader \
                                            +country_200_day_mva_slope_mean_rev_signal \
                                           ) \
                                          .applymap(lambda x: roundToNearest(x, 0.5, roundDown=True)) \
                                          .applymap(lambda x: np.nan if np.isnan(x) else np.sign(x) * (abs(x) if abs(x) < 3 else 3)) 

    price_action_meanreversion_signal['GLOBAL'] = price_action_meanreversion_signal.apply(lambda row: row.dropna().mean(), axis='columns') 
                                    
 
    price_action_technical = price_action_momentum_signal + price_action_meanreversion_signal


        
    result = Result(
            Price_action=pd.concat(
                [price_action_data
                ,price_action_momentum_signal.rename(lambda col: col + '_pa_momentum_signal', axis='columns')
                ,price_action_meanreversion_signal.rename(lambda col: col + '_pa_meanreversion_signal', axis='columns')
                ,price_action_technical.rename(lambda col: col + '_Price_Technical', axis='columns')  
                ], axis='columns', join='outer').ffill(),
            Data_Price_action=pd.concat(
                [price_action_data
                ,price_action_momentum_signal.rename(lambda col: col + '_pa_momentum_signal', axis='columns')
                ,price_action_meanreversion_signal.rename(lambda col: col + '_pa_meanreversion_signal', axis='columns')
                ,price_action_technical.rename(lambda col: col + '_Price_Technical', axis='columns')  
                ], axis='columns', join='outer').ffill()
        )  
    
    return result

##############################################


def get_econ_impulse_data(data):

    def renames(keys, values):
        a = [ name + '_Headline' for name in keys] + \
        [ name + '_Business' for name in keys] +  \
        [ name + '_Consumer' for name in keys] + \
        [ name + '_Employment' for name in keys] + \
        [ name + '_ExInflation' for name in keys] + \
        [ name + '_Growth' for name in keys] + \
        [ name + '_Inflation' for name in keys] 

        b = [ name + '_Headline' for name in values] + \
        [ name + '_Business' for name in values] + \
        [ name + '_Consumer' for name in values] + \
        [ name + '_Employment' for name in values] + \
        [ name + '_ExInflation' for name in values] + \
        [ name + '_Growth' for name in values] + \
        [ name + '_Inflation' for name in values] 
        return dict(zip(a, b))

    renames_dict = renames(names_2, names_3)
    
    sortedkeys = renames_dict.keys()
    sortedkeys.sort()

    data['Date'] = pd.to_datetime(data['Date'])
    data = data.set_index('Date').reindex(cfg.EPI_DATE_IDX).ffill() \
    .reindex(sortedkeys, axis='columns') \
    .rename(renames_dict, axis='columns')

    data = data[[
        'AUD_Business','AUD_Consumer','AUD_Employment','AUD_ExInflation','AUD_Growth','AUD_Inflation',
        'CAD_Business','CAD_Consumer','CAD_Employment','CAD_ExInflation','CAD_Growth','CAD_Inflation',
        'CNY_Business','CNY_Consumer','CNY_Employment','CNY_ExInflation','CNY_Growth','CNY_Inflation',
        'EUR_Business','EUR_Consumer','EUR_Employment','EUR_ExInflation','EUR_Growth','EUR_Inflation',        
        'JPY_Business','JPY_Consumer','JPY_Employment','JPY_ExInflation','JPY_Growth','JPY_Inflation',
        'KRW_Business','KRW_Consumer','KRW_Employment','KRW_ExInflation','KRW_Growth','KRW_Inflation',
        'GBP_Business','GBP_Consumer','GBP_Employment','GBP_ExInflation','GBP_Growth','GBP_Inflation',
        'USD_Business','USD_Consumer','USD_Employment','USD_ExInflation','USD_Growth','USD_Inflation',
        'NZD_Business','NZD_Consumer','NZD_Employment','NZD_ExInflation','NZD_Growth','NZD_Inflation',
        'GLOBAL_Business','GLOBAL_Consumer','GLOBAL_Employment','GLOBAL_ExInflation','GLOBAL_Growth','GLOBAL_Inflation',
        'AUD_Headline','USD_Headline','CAD_Headline','EUR_Headline','JPY_Headline','CNY_Headline','KRW_Headline','GBP_Headline','NZD_Headline','GLOBAL_Headline'
    ]]

    return data


def process_econ_impulse(ecom_impulse_data):

    Result = namedtuple('Result', ['Eco_pulse', 'Data_Eco_pulse'])

    headline_ecom_impulse_data = ecom_impulse_data[[
        'AUD_Headline','USD_Headline','CAD_Headline','EUR_Headline',
        'JPY_Headline','CNY_Headline','KRW_Headline','GBP_Headline',
        'NZD_Headline','GLOBAL_Headline'
        ]].rename(lambda col: col.replace('_Headline',''), axis='columns')

    #crossover signal
    epi_1m_mva = headline_ecom_impulse_data.rolling(20, min_periods=20).mean()
    epi_3m_mva = headline_ecom_impulse_data.rolling(60, min_periods=60).mean()
    epi_1m3m_mvacrossover_signal = (epi_1m_mva - epi_3m_mva).applymap(lambda x: np.sign(x) * 2 if abs(x) > 0 else 0) # crossover signal at +/- 2



    #2m changes exp zscr
    epi_m_8w_change = (headline_ecom_impulse_data - headline_ecom_impulse_data.shift(40))
    epi_m_8w_change_exp_zscr = expanding_standardise_table(epi_m_8w_change,fill_missing_at_the_end =True, zero_mean=True)
    
    
    #2m changes exp zscr round down to 0.5 and capped at 1
    epi_m_8w_change_signal = epi_m_8w_change_exp_zscr \
                             .applymap(lambda x: np.nan if np.isnan(x) else  roundToNearest(x, 0.5, roundDown=True)) \
                             .applymap(lambda x: np.nan if np.isnan(x) else (np.sign(x) * 1 if abs(x) > 1 else x)) 

    #momentum signal
    epi_momentum_signal = epi_1m3m_mvacrossover_signal + epi_m_8w_change_signal

    #pulse reversion
    epi_pulse_reversion = epi_m_8w_change_exp_zscr \
                          .applymap(lambda x: np.nan if np.isnan(x) else (0 if abs(x) < 1.25 else x)) \
                          .applymap(lambda x: np.nan if np.isnan(x) else (-1 * np.sign(x) * 3 if abs(x) >= 3 else -1 * x)) \
                          .applymap(lambda x: np.nan if np.isnan(x) else (x if (abs(x) == 3 or x == 0) else roundToNearest(x - 1.25 * np.sign(x), 0.5, roundDown=False))) 
                            
    eco_pulse_indicator = epi_pulse_reversion + epi_momentum_signal


    result = Result(
                Eco_pulse=pd.concat(
                    [ecom_impulse_data
                    ,epi_1m3m_mvacrossover_signal.rename(lambda col: col + '_epi_1m3m_mvacrossover_signal', axis='columns')
                    ,epi_m_8w_change_exp_zscr.rename(lambda col: col + '_epi_m_8w_change_exp_zscr', axis='columns')
                    ,epi_m_8w_change_signal.rename(lambda col: col + '_epi_m_8w_change_signal', axis='columns')  
                    ,epi_momentum_signal.rename(lambda col: col + '_epi_momentum_signal', axis='columns')
                    ,epi_pulse_reversion.rename(lambda col: col + '_epi_pulse_reversion', axis='columns')
                    ,eco_pulse_indicator.rename(lambda col: col + '_Eco_Pulse', axis='columns')
                    ], axis='columns', join='outer').ffill(),
                Data_Eco_pulse=pd.concat(
                    [ecom_impulse_data
                    ,epi_1m3m_mvacrossover_signal.rename(lambda col: col + '_epi_1m3m_mvacrossover_signal', axis='columns')
                    ,epi_m_8w_change_exp_zscr.rename(lambda col: col + '_epi_m_8w_change_exp_zscr', axis='columns')
                    ,epi_m_8w_change_signal.rename(lambda col: col + '_epi_m_8w_change_signal', axis='columns')
                    ,epi_momentum_signal.rename(lambda col: col + '_epi_momentum_signal', axis='columns')
                    ,epi_pulse_reversion.rename(lambda col: col + '_epi_pulse_reversion', axis='columns')
                    ,eco_pulse_indicator.rename(lambda col: col + '_Eco_Pulse', axis='columns')  
                    ], axis='columns', join='outer').ffill()
            )  
    
    return result



#############################################


def process_cftc(cftc_data):

    Result = namedtuple('Result', ['Cftc', 'Data_Cftc'])
    
    positioning_score = cftc_data[['z_CFTC']] \
                        .applymap(lambda x: barrierDampen(x, 0, 0, belowBarrierOverride=None)) \
                        .applymap(lambda x: roundToNearest(x, 0.5, roundDown=True)) \
                        .applymap(lambda x: np.nan if np.isnan(x) else  np.sign(x) * (abs(x) if abs(x) < 3 else 3))  
    
    #1m/3m crossover signal (1m - 3m signalled at +/-2)
    pos_1m_mva = cftc_data[['z_CFTC']].rolling(20, min_periods=20).mean()
    pos_3m_mva = cftc_data[['z_CFTC']].rolling(60, min_periods=60).mean()
    pos_1m3m_mvacrossover_signal = (pos_1m_mva - pos_3m_mva).applymap(lambda x: np.sign(x) * 2 if abs(x) > 0 else 0) # crossover signal at +/- 2


    #1m change signal
    pos_1m_change_signal = (cftc_data[['z_CFTC']] - cftc_data[['z_CFTC']].shift(20)) \
                           .applymap(lambda x: np.nan if np.isnan(x) else np.sign(x))         

    #supply and demand indicator 
    supply_demand_indicator = -1 * (pos_1m3m_mvacrossover_signal + pos_1m_change_signal)

    
    result = Result(
            Cftc=pd.concat(
                [cftc_data
                ,positioning_score.rename(lambda col: 'Positioning', axis='columns')
                ,supply_demand_indicator.rename(lambda col: 'Supply_demand', axis='columns')  
                ], axis='columns', join='outer').ffill(),
            Data_Cftc=pd.concat(
                [cftc_data
                ,positioning_score.rename(lambda col: 'Positioning', axis='columns')
                ,pos_1m3m_mvacrossover_signal.rename(lambda col: col + '_1m3m_mvacrossover_signal', axis='columns')
                ,pos_1m_change_signal.rename(lambda col: col + '_1m_change_signal', axis='columns')
                ,supply_demand_indicator.rename(lambda col: 'Supply_demand', axis='columns')  
                ], axis='columns', join='outer').ffill()
        )  
    
    return result



def run():
    _exit_code = 9999
    
    try:
        logging.info("<<<< {0} starts >>>>".format(component_name))
        #dirs.archive_files(cfg.DEBUG_FOLDER, cfg.ARCHIVE_DEBUG_FOLDER, copy_only = False)
        #dirs.archive_files(cfg.OUTPUT_FOLDER, cfg.ARCHIVE_OUTPUT_FOLDER, copy_only = False)
        _db = db.Database(cfg.FI_PROD_DB_CONN_STRING, cfg.DEBUG_FOLDER)

####
        print("start testing")

        print("end of testing")
        #exit(0)
###



########################################################################################################

        logging.info("=====> {0} start".format('macro risk sentiment'))

        logging.info("=====> {0} ".format('get data'))
        macro_risk_sentiment_data = get_macro_risk_sentiment_data()

        logging.info("=====> {0} ".format('process data'))
        macro_risk_sentiment_output = process_macro_risk_sentiment(macro_risk_sentiment_data)

        logging.info("=====> {0} end".format('macro risk sentiment'))

########################################################################################################

        logging.info("=====> {0} start".format('esi'))

        logging.info("=====> {0} ".format('get data'))
        esi_data = _db.query(get_esi()).set_index('Date').reindex(cfg.DATE_IDX).ffill()
        esi_data = esi_data.reindex(names_3, axis='columns')

        logging.info("=====> {0} ".format('process data'))
        esi_output = process_esi(esi_data)

        logging.info("=====> {0} end".format('esi'))

########################################################################################################


        logging.info("=====> {0} start".format('cftc'))

        logging.info("=====> {0} ".format('get data'))
        cftc_data = _db.query(get_cftc())
        cftc_data['Date'] = pd.to_datetime(cftc_data['Date'])
        cftc_data = cftc_data.set_index('Date').reindex(cfg.EPI_DATE_IDX).ffill()

        logging.info("=====> {0} ".format('process data'))
        cftc_output = process_cftc(cftc_data)

        logging.info("=====> {0} end".format('cftc'))

########################################################################################################

        logging.info("=====> {0} start".format('price action'))

        logging.info("=====> {0} ".format('get sov data'))
        sov_rates_data = _db.query(get_sov_rates())

        logging.info("=====> {0} ".format('get swap data'))
        swap_rates_data = _db.query(get_swap_rates())

        logging.info("=====> {0} ".format('get real cash rate data'))
        real_cash_data = _db.query(get_real_cash_rates())

        logging.info("=====> {0} ".format('merge spreadsheet data, sov10 data, swap data, real cash rate data'))
        price_action_data = get_price_action_data(sov_rates_data, swap_rates_data, real_cash_data)

        logging.info("=====> {0} ".format('process data'))
        price_action_output = process_price_action(price_action_data)

        logging.info("=====> {0} end".format('price action'))

########################################################################################################

        logging.info("=====> {0} start".format('econ impulse'))

        logging.info("=====> {0} ".format('get data'))
        ecom_impulse_data = get_econ_impulse_data( _db.query(get_monthly_epi()))

        logging.info("=====> {0} ".format('process data'))
        econ_impulse_output = process_econ_impulse(ecom_impulse_data)

        logging.info("=====> {0} end".format('econ impulse'))

########################################################################################################






        
        #output_obj = {
        #    'esi':esi_output.Esi,
        #    'price_action': price_action_output.Price_action,
        #    'cftc':cftc_output.Cftc,
        #    'macro_risk_sentiment': macro_risk_sentiment_output.Macro_Risk_Sentiment,
        #    'econ_impulse_output': econ_impulse_output
        #}
        #dumptoExcel(os.path.join(cfg.OUTPUT_FOLDER, 'shortterm_components_output.xlsx'), output_obj) 


        debug_obj = {
            'esi':esi_output.Data_Esi,
            'price_action': price_action_output.Data_Price_action,
            'cftc':cftc_output.Data_Cftc,
            'macro_risk_sentiment': macro_risk_sentiment_output.Data_Macro_Risk_Sentiment,
            'econ_impulse': econ_impulse_output.Data_Eco_pulse
        }
        dumptoExcel(os.path.join(cfg.OUTPUT_FOLDER, 'shortterm_components_output.xlsx'), debug_obj) 


        # stop out here
        # return 

        # ESI - XXX_Eco_Sentiment
        # price_action - XXX_Price_TECHNICALS
        # cftc - Positioning
        # cftc - Supply_Demand
        # macro_risk_sentiment - Global_Macro_Risk_Sentiment
        # econ_impulse: XXX_Eco_Pulse
      


        

        data_obj = {
            'macro_risk_sentiment_data':macro_risk_sentiment_data,
            'esi_data':esi_data,
            'price_action_data':price_action_data,
            'ecom_impulse_data':ecom_impulse_data,
            'cftc_data':cftc_data
        }


        dumptoExcel(os.path.join(cfg.OUTPUT_FOLDER, 'all_data_debug.xlsx'),data_obj) 


        if cfg.UPDATE_DB:
            logging.info("=====> {0} start".format('dump to db'))
            filtered_results = filterResults([esi_output, price_action_output, cftc_output, macro_risk_sentiment_output, econ_impulse_output], 
                                ['Data_Esi','Data_Price_action','Data_Cftc', 'Data_Macro_Risk_Sentiment','Data_Eco_pulse'])
                                
            _db = db.Database(cfg.FI_PROD_DB_CONN_STRING, cfg.DEBUG_FOLDER)
            dumptoDatabase(_db, 'Tech_Sentiment', filtered_results, dropblanks=False) 
            logging.info("=====> {0} end".format('dump to db'))  
        else: 
            logging.info("=====> {0} skipped due to the config".format('dump to db'))


        logging.info("<<<< {0} ends SUCCESSFULLY >>>>".format(component_name))
        print("<<<< {0} ends SUCCESSFULLY >>>>".format(component_name))
        _exit_code = 0
    except:
        logging.exception("!!! Error detected in {0}".format(component_name))
        print("!!! Error detected in {0}".format(component_name))
        logging.ERROR("<<<< {0} ends UNSUCCESSFULLY >>>>".format(component_name))
        _exit_code = -1
    finally:
        logging.info("about to update job status")
        _db.update_job_status(cfg.APP_ID, cfg.APP_NAME, datetime.now().strftime(r"%Y-%m-%d"),_exit_code,cfg.LOG_PATH)
        #_result = dirs.check_log_file_result(cfg.LOG_PATH)
        #_title = "{0} process - {1}".format(cfg.APP_NAME, _result)
        #_email_body = dirs.htmlfy_log_file(cfg.LOG_PATH)
        #emails.send_SMTP_mail(cfg.SENDER, cfg.RECEIVERS, _title, _email_body, cfg.CC, [cfg.LOG_PATH])
        #sys.exit(_exit_code)
        return _exit_code