from __future__ import division
import pandas as pd 
import numpy as np
import _path
import os, sys, inspect
import logging
from collections import namedtuple
from datetime import datetime
import common.dirs as dirs
import common.emails as emails
import data.datatools as datatools
import data.database as db
from util import *
import _config_component_ftv as cfg


def run():
    _exit_code = 9999
    
    try:
        logging.info("<<<< {0} starts >>>>".format(cfg.APP_NAME))
        #dirs.archive_files(cfg.DEBUG_FOLDER, cfg.ARCHIVE_DEBUG_FOLDER, copy_only = False)
        #dirs.archive_files(cfg.OUTPUT_FOLDER, cfg.ARCHIVE_OUTPUT_FOLDER, copy_only = False)
        _db = db.Database(cfg.FI_PROD_DB_CONN_STRING, cfg.DEBUG_FOLDER)

####
        print("start testing")

        print("end of testing")
        #exit(0)
###


        if cfg.UPDATE_DB:
            logging.info("=====> {0} start".format('dump to db'))

            logging.info("=====> {0} end".format('dump to db'))  
        else: 
            logging.info("=====> {0} skipped due to the config".format('dump to db'))


        logging.info("<<<< {0} ends SUCCESSFULLY >>>>".format(cfg.APP_NAME))
        print("<<<< {0} ends SUCCESSFULLY >>>>".format(cfg.APP_NAME))
        _exit_code = 0
    except:
        logging.exception("!!! Error detected in {0}".format(cfg.APP_NAME))
        print("!!! Error detected in {0}".format(cfg.APP_NAME))
        logging.ERROR("<<<< {0} ends UNSUCCESSFULLY >>>>".format(cfg.APP_NAME))
        _exit_code = -1
    finally:
        logging.info("about to update job status")
        _db.update_job_status(cfg.APP_ID, cfg.APP_NAME, datetime.now().strftime(r"%Y-%m-%d"),_exit_code,cfg.LOG_PATH)

        return _exit_code