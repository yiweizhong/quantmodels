import os, sys, inspect
_tools_path = r"""\\capricorn\ausfi\Macro_Dev\Data\macro_tools\bin\tools"""
if _tools_path not in sys.path: sys.path.insert(0, _tools_path)
print("sys.path:")
for p in sys.path: print(p)