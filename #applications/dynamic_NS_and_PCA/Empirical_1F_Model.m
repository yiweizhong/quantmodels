%% Empirical one-factor short-rate model
%
F     = [ones(nObs,1) RDNS.yields(:,1)];
H     = F\RDNS.yields;
Y_fit = F*H;
RMSE = (mean((RDNS.yields - Y_fit).^2)).^(0.5);
