%%  Section 1: Empirical exploration of yield curve data
%
clear all;                             % clear all variables
close all;                             % close all figures
clc;                                   % clear command window
disp('Please wait...')
load('Data_YCM.mat');

% adjusting the minimum maturity for the German data:
%   the 6 month maturity is the shortest observed maturity in the 
%   downloaded german data. Using a principal component analysis
%   (more about this in the text) a three-month yield observation is
%   is fitted. This makes US and German yields available for an 
%   identical set of maturities.
%
x_      = [1:1:11]';
tau_in  = [6 12:12:120]';
tau_out = [3 12:12:120]';
Dates   = DE_data(:,1);
Y_tmp   = DE_data(:,2:end);
[G, F ] = pca(DE_data(:,2:end),'Centered','off');
G_ = nan(11,11);
for (j=1:11)
    G_(:,j) = interp1(tau_in,G(:,j),tau_out,'spline','extrap');
end
DE_data = [];
DE_data = [Dates (G_*F')'];

start_ = datenum('31-Jan-1975');       % defines the start date of the data samples.
                                       %    can be changed to test whether the results 
                                       %    below are robust to other starting points.  

indx_s   = find(US_data(:,1)==start_,1,'first'); 
indx_tau = [1 2 3 6 8 11];                    % selected maturities
tauDE    = [3 12 24 60 84 120]';              % defines the maturities
tauUS    = [3 12 24 60 84 120]';
Y_US     = US_data(indx_s:end,indx_tau+1);    % ... first column holds the date ...
Y_DE     = DE_data(indx_s:end,indx_tau+1);    % contains the yield curve 
dates    = US_data(indx_s:end,1);             %   observations nObs-by-nTau
[nObs,nTau] = size(Y_US);                     % number of time series observations and
                                              %    number of maturities. 

figure('units','normalized','outerposition',[0 0 1 1])
    subplot(2,1,1), plot( dates, Y_US )
    date_ticks = datenum(1975:5:2020,1,1);
    set(gca, 'xtick', date_ticks), ylabel('(pct)')
    datetick('x','mmm-yy','keepticks'), title('US') 
    set(gca, 'FontSize', 20)  
   
   subplot(2,1,2), plot(dates, Y_DE), 
   date_ticks = datenum(1975:5:2020,1,1);
   set(gca, 'xtick', date_ticks), ylabel('(pct)')
   datetick('x','mmm-yy','keepticks'), title('German') 
   set(gca, 'FontSize', 20)  

print -depsc Empirical_YieldCurves_US_DE_EA

%% Cross sectional plots
figure('units','normalized','outerposition',[0 0 1 1])
    plot(tauDE,[mean(Y_DE)' median(Y_DE)' min(Y_DE)' max(Y_DE)' ], ...
        'o-','LineWidth',2), ...
    xticks(tauDE'), grid, 'on';
    xticklabels(tauDE' ), ...
    ylabel('Pct'), legend('Mean', 'Median', 'Min', ...
    'Max', 'Location','northeast')
    ylim([-2 15])
    set(gca, 'FontSize', 20)  
print -depsc AverageYieldsDE
    
diff_S = Y_DE(:,end)-Y_DE(:,1);       % difference between the 10y and 
                                      % 6m yields (a measure for the slope)

[~, indxS_med] = min(abs(diff_S-median(diff_S)));  % finds the index of 
[~, indxS_min] = min(abs(diff_S-min(diff_S)));     % the curve having the 
[~, indxS_max] = min(abs(diff_S-max(diff_S)));     % median, min, and max 
                                                   % slope in the sample

figure('units','normalized','outerposition',[0 0 1 1])
    plot(tauDE,[Y_DE(indxS_med,:)' Y_DE(indxS_min,:)'...
              Y_DE(indxS_max,:)'],'o-', 'LineWidth',2 ), ...
    %title('Generic Slope-Based Shapes of the Yield Curve - Germany'), ...
    legend( datestr(dates(indxS_med,1)), datestr(dates(indxS_min,1)), ...
            datestr(dates(indxS_max,1)), 'Location', 'SouthEast' ), ...
    xticks(tauDE), xticklabels(tauDE'), ...
    ylabel('Pct'), grid, 'on'; ...
    ylim([0 15])
    set(gca, 'FontSize', 20)  
print -depsc GenericYieldCurveShapesDE

 
%% Correlation analysis
subData = array2table([Y_DE(:,1), Y_DE(:,4), Y_DE(:,6), Y_US(:,1), Y_US(:,4), Y_US(:,6)]);     
subData.Properties.VariableNames = { 'DEm6', 'DEy5', 'DEy10', ...
                                     'USm3', 'USy5', 'USy10' };
corrplot(subData,'type','Pearson','testR','on','alpha',0.01)

print -depsc YieldCorrPlot

%% Generating projections from a yields-only model
%
rng(42+42+42);  % fixing the starting point for the random number generator
                %    to ensure replicability
nHist    = 12;  % number of historical observations to inlude in the plot
nSim     = 42;  % number of periods to be simulated
VAR_y    = varm(nTau, 1);  % sets up a VAR1 model: 11 variables and 1 lag
est_DE   = estimate(VAR_y, Y_DE);  % estimate VAR1 model on all obs. 
sim_DE   = simulate(est_DE, nSim, 'Y0', Y_DE(end,:)); % simulate the model
                                                      %  star at last obs
simDates = [ dates(end-11:end,1); ...
                dates(end,1)+(31:31:nSim*31)' ];     
              % concatenating the dates for the last 12 data observations
              %    with the dates spanning the forecasts
data2plot = [ Y_DE(end-nHist+1:end,:); sim_DE];  %  hist. + sim. data

figure('units','normalized','outerposition',[0 0 1 1])
    plot(simDates, data2plot, '--', 'LineWidth',2), ...
    hold on, grid, 'on';
    plot(simDates(1:nHist,1), Y_DE(end-nHist+1:end,:), '-', ...
                                          'LineWidth',2)
    %title('Forecasting German Yields the Incorrect way'), ...
    set(gca, 'FontSize', 20)  
    datetick('x','mmm-yy')
    print -depsc WrongProjections

figure('units','normalized','outerposition',[0 0 1 1])
    subplot(3,1,1), plot(tauDE,sim_DE(17,:),'o-'), ...
        ylim([-4 0]), grid,'on'; ...
                               title(datestr(simDates(17+nHist),'mmm-yy'))
    xticks(tauDE), xticklabels({tauDE}), 
    set(gca, 'FontSize', 18)
    subplot(3,1,2), plot(tauDE,sim_DE(36,:),'o-'), ...
         ylim([-4 0]), grid,'on'; ...
                               title(datestr(simDates(36+nHist),'mmm-yy'))
    xticks(tauDE), xticklabels({tauDE}),
    set(gca, 'FontSize', 18)
    subplot(3,1,3), plot(tauDE,sim_DE(42,:),'o-'), ...
         ylim([-4 0]), grid,'on'; ...
                               title(datestr(simDates(42+nHist),'mmm-yy'))
    xticks(tauDE), xticklabels({tauDE})
    set(gca, 'FontSize', 18)
    print -depsc FunnySimYields

    
%% A first look at factor models
%
[G_US, F_US, eig_US]    = pca(Y_US);      % run factor analysis on US data
[G_DE, F_DE, eig_DE]    = pca(Y_DE);      % run factor analysis on DE data

[ cumsum(eig_US./sum(eig_US)) cumsum(eig_DE./sum(eig_DE)) ]

nF     = 3;
Spread = Y_DE-Y_US;                         % the pure spread in percentage points
[G_Sprd, F_Sprd, eig_Sprd] = pca(Spread);   % run factor analysis on US data

figure('units','normalized','outerposition',[0 0 1 1])
    subplot(2,1,1), plot(tauUS,G_US(:,1:nF),'o-', ...
        'LineWidth',2),  ylim([-1 1]),  title('US loading structure'), 
    xticks(tauUS),xticklabels(tauUS'), 
    ylabel('Value'), grid, 'on'; set(gca, 'FontSize', 20)
    subplot(2,1,2), plot(tauDE,G_Sprd(:,1:nF),'o-', ...
        'LineWidth',2), ylim([-1 1]), title('Spread loading structure (US-DE)'), 
    xticks(tauDE), xticklabels(tauDE'), 
    ylabel('Value'), grid, 'on'; set(gca, 'FontSize', 20)
    print -depsc EmpiricalLoadingStructures
    
%% Joint model for DE and US yields 
% 
Y2     = [ Y_US Y_DE ];                  % collecting the relevant yield segments 
G_mdl  = [ G_US(:,1:nF) zeros(nTau,nF);  % loading structure for the joint model
           G_US(:,1:nF) G_Sprd(:,1:nF)] ;
G_mdl2  = [ zeros(nTau,nF)
            G_US(:,1:nF)% loading structure for the joint model
          ] ;
F_mdl2  = G_mdl2\Y2';        
       
F_mdl  = G_mdl\Y2';
Y2_hat = (G_mdl*F_mdl)';                 % fitted yield curves
err    = Y2-Y2_hat;                      % fitting errors

RMSE_bps = 100*(mean(err.^2)).^(1/2)    % RMSE in basis points

Tab_rmse = array2table(round(RMSE_bps)); % just for the display of output
for (j=1:nTau)
    Tab_rmse.Properties.VariableNames(1,j)      = {strcat(['US',num2str(tauDE(j,1))])};   
    Tab_rmse.Properties.VariableNames(1,j+nTau) = {strcat(['DE',num2str(tauUS(j,1))])};
end
disp(Tab_rmse)
disp('Min and Max RMSE')
disp(round([ min(RMSE_bps) max(RMSE_bps) ]))
%% Comparing observed and fitted yields 
figure('units','normalized','outerposition',[0 0 1 1])
subplot(2,2,1), plot(dates,[Y2(:,1) Y2_hat(:,1)], 'LineWidth',2), ...
                set(gca, 'FontSize', 20)                
                title(Tab_rmse.Properties.VariableNames(1,1)), ...
                datetick('x','yyyy'), 
                legend('Obs','Fitted','Location','northeast')
                
subplot(2,2,2), plot(dates,[Y2(:,6) Y2_hat(:,6)], 'LineWidth',2), ...
                set(gca, 'FontSize', 20)                
                title(Tab_rmse.Properties.VariableNames(1,6)), ...
                datetick('x','yyyy'), 
                legend('Obs','Fitted','Location','northeast')
                
subplot(2,2,3), plot(dates,[Y2(:,7) Y2_hat(:,7)], 'LineWidth',2), ...
                set(gca, 'FontSize', 20)                
                title(Tab_rmse.Properties.VariableNames(1,7)), ...
                datetick('x','yyyy'), 
                legend('Obs','Fitted','Location','northeast')
                
subplot(2,2,4), plot(dates,[Y2(:,12) Y2_hat(:,12)], 'LineWidth',2), ...
                set(gca, 'FontSize', 20)
                title(Tab_rmse.Properties.VariableNames(1,12)), ...
                datetick('x','yyyy'), 
                legend('Obs','Fitted','Location','northeast') 
                print -depsc EvaluatingJointModel

                
%% Adding dynamics to the model 
%
maxLags = 6;
aic_bic = zeros(maxLags,2);
for ( j=1:maxLags )
    Mdl_    = varm(nF*2,j);
    Mdl_est = estimate(Mdl_,F_mdl');
    Info_   = summarize(Mdl_est);
    aic_bic(j,:) = [ Info_.AIC Info_.BIC ];
end
disp('Optimal lag-order according to:')
disp('    AIC   BIC ')
disp( [find(min(aic_bic(:,1))==aic_bic(:,1)) ...
       find(min(aic_bic(:,2))==aic_bic(:,2))])

Mdl_dynamics          = varm(nF*2,1);
Est_dynamics          = estimate(Mdl_dynamics, F_mdl');
sort(real(eig(Est_dynamics.AR{:,:})))

end_     = datenum('31-May-2013');   % end-date of the est data sample
indx_e   = find(dates==end_,1,'first')-1;
horizon_ = 6;                        % projection horizon
nCast    = nObs-indx_e-horizon_;     % number of times to re-estimate
err_mdl  = NaN(horizon_+1,nTau*2,nCast);  % container for the output
err_rw   = NaN(horizon_+1,nTau*2,nCast);  % container for the random-walk 
Mdl_cast = varm(nF*2,1);
for ( j=1:nCast )
    % estimate on expanding data window
    est_tmp   = estimate(Mdl_cast, F_mdl(:,1:indx_e+j)');  
    % forecast VAR model
    F_cast    = forecast(est_tmp,horizon_,F_mdl(:,1:indx_e+j)');   
    % forecast random-walk
    F_rw      = repmat(F_mdl(:,indx_e+j-1)',horizon_+1,1);         
    Y_obs     = Y2(indx_e+j:indx_e+j+horizon_,:);
    F_cast    = [ F_mdl(:,indx_e+j)'; F_cast ];
    Y_cast    = (G_mdl*F_cast')';  % convert forecasted factors into yields 
    Y_rw      = (G_mdl*F_rw')';    % convert random projections into yields
    err_mdl(:,:,j) = (Y_obs-Y_cast)*100;
    err_rw(:,:,j)  = (Y_obs-Y_rw)*100;
end
tab_Fcast_mdl_rmse = array2table( round( (mean(err_mdl.^2,3)).^(1/2)) );
tab_Fcast_mdl_rmse.Properties.VariableNames = Tab_rmse.Properties.VariableNames;
tab_Fcast_mdl_rmse.Properties.RowNames      = {'Fitted', 'Forecast 1m ahead', ...
                                               'Forecast 2m ahead', 'Forecast 3m ahead', ...
                                               'Forecast 4m ahead', 'Forecast 5m ahead', ...
                                               'Forecast 6m ahead'};
                                           
tab_Fcast_rw_rmse  = array2table( round( (mean(err_rw.^2,3)).^(1/2)) );
tab_Fcast_rw_rmse.Properties.VariableNames = tab_Fcast_mdl_rmse.Properties.VariableNames;
tab_Fcast_rw_rmse.Properties.RowNames      = tab_Fcast_mdl_rmse.Properties.RowNames;
disp('Projections from the estimated model')
disp('------------------------------------')
disp(tab_Fcast_mdl_rmse)
disp('Projections assuming an Random-Walk model')
disp(tab_Fcast_rw_rmse)
disp('-------------------------------------')

%% Creating the expanded loading matrix
%  to facilitate monthly return calculations
%  for the 1, 60, 120 months maturity points
%
tau_new    = sort([tauUS;[0;48;108]]);
nTau_new   = length(tau_new);
% inter- and extra-polation of the Base loadings
G_ext      = interp1(tauUS,G_mdl(1:nTau,1:3),tau_new,'pchip');         
% inter- and extra-polation of the Spread loadings
G_Sprd     = interp1(tauUS,G_mdl(nTau+1:end,4:6),tau_new,'pchip');     
% expanded loading matrix
G_sim      = [ G_ext zeros(nTau_new,3); G_ext G_Sprd ];  

figure('units','normalized','outerposition',[0 0 1 1])
    subplot(2,1,1), plot(tau_new, G_sim(1:nTau_new,1:3),'-b*','LineWidth',2)
    hold on, ylim([-1 1])
    subplot(2,1,1), plot(tauUS, G_mdl(1:nTau,1:3),'r*','LineWidth',5), 
    title('Expanded loadings: US')
    set(gca, 'FontSize', 20), xticks(tau_new), xticklabels({tau_new})
    grid, 'on';
    subplot(2,1,2), plot(tau_new, G_sim(nTau_new+1:end,4:6),'-b*','LineWidth',2)
    hold on, ylim([-1 1])
    subplot(2,1,2), plot(tauUS, G_mdl(nTau+1:end,4:6),'r*','LineWidth',5), 
    title('Expanded loadings: Spread')
    set(gca, 'FontSize', 20), xticks(tau_new), xticklabels({tau_new})
    grid, 'on';
    print -depsc ExpandedLoadingMatrix  
    
%% Calculates return distributions
% defines the end-date of the first data sample
end_      = datenum('31-Dec-2018');                 
indx_e    = find(dates==end_,1,'first');
horizon_  = 12;                             % simulation horizon
nSim      = 1e4;                            % number of simulation paths
nAssets   = length(tau_new)-length(tauDE);  % number of points on the curve  
                                      %    for which returns are generated
Sim_Ret   = NaN(nSim, nAssets);       % container for the simulated returns
Mdl_      = varm(nF*2,1);
% estimate the VAR model on the selected data
est_Mdl   = estimate(Mdl_, F_mdl(:,1:indx_e)');     
Y0        = repmat(F_mdl(:,indx_e)',nSim,1);
F_sim1    = repmat(F_mdl(:,indx_e)',1,1,nSim);
% Simulated paths for the factors
F_sim2    = simulate(est_Mdl, horizon_, 'Y0', Y0, 'NumPaths', nSim);   
% combining obs and simulated data
F_sim3    = cat(1,F_sim1,F_sim2); 
% transposing first two dimensions
F_sim     = permute(F_sim3,[2 1 3]);
% container for simulated yields
Y_sim     = NaN(2*nTau_new,horizon_+1,nSim);% dim: Tau x horizon x sim_path                         
% container for the simulated annual returns                                                                       
R_sim     = NaN(nSim,nAssets*2);    
e1        = [3;6;9;12;15;18];    % indicator for relevant maturity points
e2        = [1;5;8;10;14;17];    %    at time t, and t+1
tau_ret   = [tau_new;tau_new]./12;

for ( j=1:nSim )                                  % calculating returns
    Y_sim(:,:,j) = G_sim*squeeze(F_sim(:,:,j));
    R_sim(j,:)   = (tau_ret(e1,1).*squeeze(Y_sim(e1,1,j)) - ... 
                    tau_ret(e2,1).*squeeze(Y_sim(e2,horizon_+1,j)))'; 
end
ret_Tab = array2table([ round(mean(R_sim).*100)./100; ... 
                     round(std(R_sim)*100)./100 ]);  % organising results
ret_Tab.Properties.VariableNames = ...
                          Tab_rmse.Properties.VariableNames(1,(2:2:12));
ret_Tab.Properties.RowNames = [{'Mean'};{'Std.'}];
disp('Summary of the simulated return distributions')
disp(ret_Tab)

figure('units','normalized','outerposition',[0 0 1 1])
    subplot(2,2,1), histfit(R_sim(:,2),50,'Normal'), ...
       set(gca, 'FontSize', 20), title(ret_Tab.Properties.VariableNames(2))                       
    subplot(2,2,2), histfit(R_sim(:,3),50,'Normal'), ...
       set(gca, 'FontSize', 20), title(ret_Tab.Properties.VariableNames(3))
    subplot(2,2,3), histfit(R_sim(:,5),50,'Normal'), ...
       set(gca, 'FontSize', 20), title(ret_Tab.Properties.VariableNames(5))
    subplot(2,2,4), histfit(R_sim(:,6),50,'Normal'), ...
       set(gca, 'FontSize', 20), title(ret_Tab.Properties.VariableNames(6))
     print -depsc ReturnDistributions 
disp('Done executing the code')                         
                          
                          