%% Scenario generation and forecasting
%
% preparing the data
%
warning('off','all')
path_=[pwd,'\MATLAB_classes'];
addpath(path_);
load('Data_GSW.mat'); 
GSW_         = GSW;                  % creates an instance of the GSW class
GSW_.tau     = [3 12:12:120]';       % vector of maturities
GSW_.beta    = GSW_factors(:,2:5);   % yield curve factors
GSW_.lambda  = GSW_factors(:,6:7);   % lambdas
GSW_         = GSW_.getYields;       % getting yields

dates = GSW_factors(:,1);
Y     = GSW_.yields;
tau   = GSW_.tau;
nTau  = size(tau,1);

figure
    plot(dates,Y(:,11))
    date_ticks = datenum(1960:4:2020,1,1);
    set(gca, 'xtick', date_ticks);
    datetick('x','mmm-yy','keepticks')

figure('units','normalized','outerposition',[0 0 1 1])
    plot(US_MacroVariables(:,1),[US_MacroVariables(:,2)./25 ...
                     US_MacroVariables(:,3) ],'LineWidth',2)
    date_ticks = datenum(1972:4:2020,1,1);
    set(gca, 'xtick', date_ticks);
    datetick('x','mmm-yy','keepticks')
    set(gca, 'FontSize', 18), 
    legend('Capacity Utilisation','Inflation rate')
  %  print -depsc MacroVariables
    
    
%% The horse-race
% The following models are included in the horce-race
% ---------------------------------------------------
%  DNS         -> Dynamic Nelson-Siegel model 
%  DNS_bc      -> Dynamic Nelson-Siegel model, bias corrected
%  DSS         -> Dynamic Svensson-Soderlind model
%  DSS_bc      -> Dynamic Svensson-Soderlind model, bias corrected
%  SRB3        -> Short-Rate based 3-factor model 
%  SRB3_bc     -> Short-Rate based 3-factor model, bias corrected 
%  SRB4        -> Short-Rate based 4-factor model 
%  SRB4_bc     -> Short-Rate based 4-factor model, bias corrected
%  JSZ         -> Joslin, Singleton, Zhu (2011)
%  JSZ_bc      -> Joslin, Singleton, Zhu (2011), bias corrected
%  AFSRB       -> Arbitrage-free SRB model with 2,3, or 4 factors
%  AFSRB_bc    -> Arbitrage-free SRB model with 2,3, or 4 factors, bias c.
%  SRTPC1C2    -> Model with Short rate, 10-year term premium, 
%                      and 2 additional empirical factors 
%  SRTPC1C2_bc -> Model with Short rate, 10-year term premium, 
%                      and 2 additional empirical factors, bias corrected 
%
% Note that program execution could possibly be improved by combining 
%      the pseudo out-of-sample forecasts, performed for each model,
%      inside one loop. However, with an eye to clarity of the code,
%      a slower model-by-model implementation is used.
%
fDate     = datenum('31-Jan-2017'); % start date for the horse-race
horizon   = 12;                     % forecast horizon
startIndx = find(fDate==dates,1,'first');
nIter     = GSW_.nObs - startIndx - horizon; 

%
% ... DNS 
%
DNS_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 3;
    A_TSM.biasCorrect = 0;
    A_TSM          = A_TSM.getDNS;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    DNS_fErr(:,:,j)    = oYields-castY(:,1:11);
end
DNS_fRMSE = 100.*sqrt(mean((DNS_fErr.^2),3));

%
% ... DNSbc 
%
DNSbc_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 3;
    A_TSM.biasCorrect = 1;
    A_TSM          = A_TSM.getDNS;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    DNSbc_fErr(:,:,j)    = oYields-castY(:,1:11);
end
DNSbc_fRMSE = 100.*sqrt(mean((DNSbc_fErr.^2),3));

%
% ... DSS 
%
DSS_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 3;
    A_TSM.biasCorrect = 0;
    A_TSM          = A_TSM.getDSS;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    DSS_fErr(:,:,j) = oYields-castY(:,1:11);
end
DSS_fRMSE = 100.*sqrt(mean((DSS_fErr.^2),3));

%
% ... DSSbc 
%
DSSbc_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 3;
    A_TSM.biasCorrect = 1;    
    A_TSM          = A_TSM.getDSS;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    DSSbc_fErr(:,:,j) = oYields-castY(:,1:11);
end
DSSbc_fRMSE = 100.*sqrt(mean((DSSbc_fErr.^2),3));


%
% ... SRB3 
%
SRB3_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 3;
    A_TSM.biasCorrect = 0;
    A_TSM          = A_TSM.getSRB3;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    SRB3_fErr(:,:,j) = oYields-castY(:,1:11);
end
SRB3_fRMSE = 100.*sqrt(mean((SRB3_fErr.^2),3));


%
% ... SRB3bc 
%
SRB3bc_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 3;
    A_TSM.biasCorrect = 1;
    A_TSM          = A_TSM.getSRB3;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    SRB3bc_fErr(:,:,j) = oYields-castY(:,1:11);
end
SRB3bc_fRMSE = 100.*sqrt(mean((SRB3bc_fErr.^2),3));


%
% ... SRB4 
%
SRB4_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 4;
    A_TSM.biasCorrect = 0;
    A_TSM          = A_TSM.getSRB4;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    SRB4_fErr(:,:,j) = oYields-castY(:,1:11);
end
SRB4_fRMSE = 100.*sqrt(mean((SRB4_fErr.^2),3));


%
% ... SRB4bc 
%
SRB4bc_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 4;
    A_TSM.biasCorrect = 1;
    A_TSM          = A_TSM.getSRB4;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    SRB4bc_fErr(:,:,j) = oYields-castY(:,1:11);
end
SRB4bc_fRMSE = 100.*sqrt(mean((SRB4bc_fErr.^2),3));


%
% ... JSZ
%
JSZ_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 3;
    A_TSM.biasCorrect = 0;
    A_TSM          = A_TSM.getJSZ;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    JSZ_fErr(:,:,j) = oYields-castY(:,1:11);
end
JSZ_fRMSE = 100.*sqrt(mean((JSZ_fErr.^2),3));


%
% ... JSZ_bc
%
JSZbc_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 3;
    A_TSM.biasCorrect = 1;
    A_TSM          = A_TSM.getJSZ;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    JSZbc_fErr(:,:,j) = oYields-castY(:,1:11);
end
JSZbc_fRMSE = 100.*sqrt(mean((JSZbc_fErr.^2),3));


%
% ... AFSRB2
%
AF2_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 2;
    A_TSM.biasCorrect = 0;
    A_TSM          = A_TSM.getAFSRB;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    AF2_fErr(:,:,j) = oYields-castY(:,1:11);
end
AF2_fRMSE = 100.*sqrt(mean((AF2_fErr.^2),3));


%
% ... AFSRB2
%
AF2bc_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 2;
    A_TSM.biasCorrect = 1;
    A_TSM          = A_TSM.getAFSRB;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    AF2bc_fErr(:,:,j) = oYields-castY(:,1:11);
end
AF2bc_fRMSE = 100.*sqrt(mean((AF2bc_fErr.^2),3));

%
% ... AFSRB3
%
AF3_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 3;
    A_TSM.biasCorrect = 0;
    A_TSM          = A_TSM.getAFSRB;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    AF3_fErr(:,:,j) = oYields-castY(:,1:11);
end
AF3_fRMSE = 100.*sqrt(mean((AF3_fErr.^2),3));


%
% ... AFSRB3_bc
%
AF3bc_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 3;
    A_TSM.biasCorrect = 1;
    A_TSM          = A_TSM.getAFSRB;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    AF3bc_fErr(:,:,j) = oYields-castY(:,1:11);
end
AF3bc_fRMSE = 100.*sqrt(mean((AF3bc_fErr.^2),3));


%
% ... AFSRB4
%
AF4_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 4;
    A_TSM.biasCorrect = 0;
    A_TSM          = A_TSM.getAFSRB;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    AF4_fErr(:,:,j) = oYields-castY(:,1:11);
end
AF4_fRMSE = 100.*sqrt(mean((AF4_fErr.^2),3));


%
% ... AFSRB4_bc
%
AF4bc_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 4;
    A_TSM.biasCorrect = 1;
    A_TSM          = A_TSM.getAFSRB;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    AF4bc_fErr(:,:,j) = oYields-castY(:,1:11);
end
AF4bc_fRMSE = 100.*sqrt(mean((AF4bc_fErr.^2),3));


%
% ... SRTPC1C2
%
SRTPC1C2_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 4;
    A_TSM.biasCorrect = 0;
    A_TSM          = A_TSM.getSRTPC1C2;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    SRTPC1C2_fErr(:,:,j) = oYields-castY(:,1:11);
end
SRTPC1C2_fRMSE = 100.*sqrt(mean((SRTPC1C2_fErr.^2),3));


%
% ... SRTPC1C2bc
%
SRTPC1C2bc_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    estYields      = Y(1:startIndx+j,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    A_TSM          = [];
    A_TSM          = TSM;
    A_TSM.yields   = estYields;
    A_TSM.tau      = tau;
    A_TSM.DataFreq = 12;
    A_TSM.nF       = 4;
    A_TSM.biasCorrect = 1;
    biasCorrect    = 1;
    A_TSM          = A_TSM.getSRTPC1C2;
    castY          = [];
    A_SSM          = TSM2SSM;
    A_SSM.TSM      = A_TSM;
    A_SSM          = A_SSM.getMdl;
    castY          = [ A_SSM.Data(startIndx+j-1,:); forecast(A_SSM.Mdl, ...
                                  horizon, A_SSM.Data(startIndx+j-1,:) )];
    SRTPC1C2bc_fErr(:,:,j) = oYields-castY(:,1:11);
end
SRTPC1C2bc_fRMSE = 100.*sqrt(mean((SRTPC1C2bc_fErr.^2),3));

%% preparing output tables
%
% RMSE of all models 
%       For maturities      : 3m 1Y 5Y 10Y 
%       and forecasts adead : 1m 2m 3m 6m 12m
%

ahead = [2;3;4;7;13];
%
% ... for the 3m maturity segment
%
ZZmat_3m = [  DNS_fRMSE(ahead,1)';   
              DNSbc_fRMSE(ahead,1)';       
              DSS_fRMSE(ahead,1)';           
              DSSbc_fRMSE(ahead,1)';        
              SRB3_fRMSE(ahead,1)';           
              SRB3bc_fRMSE(ahead,1)';        
              SRB4_fRMSE(ahead,1)';           
              SRB4bc_fRMSE(ahead,1)';      
              JSZ_fRMSE(ahead,1)';           
              JSZbc_fRMSE(ahead,1)';           
              AF2_fRMSE(ahead,1)';         
              AF2bc_fRMSE(ahead,1)';      
              AF3_fRMSE(ahead,1)';         
              AF3bc_fRMSE(ahead,1)';     
              AF4_fRMSE(ahead,1)';         
              AF4bc_fRMSE(ahead,1)';      
              SRTPC1C2_fRMSE(ahead,1)';
              SRTPC1C2bc_fRMSE(ahead,1)'];
        
%
% ... for the 1Y maturity segment
%
ZZmat_1Y = [  DNS_fRMSE(ahead,2)';   
              DNSbc_fRMSE(ahead,2)';       
              DSS_fRMSE(ahead,2)';           
              DSSbc_fRMSE(ahead,2)';        
              SRB3_fRMSE(ahead,2)';           
              SRB3bc_fRMSE(ahead,2)';        
              SRB4_fRMSE(ahead,2)';           
              SRB4bc_fRMSE(ahead,2)';      
              JSZ_fRMSE(ahead,2)';           
              JSZbc_fRMSE(ahead,2)';           
              AF2_fRMSE(ahead,2)';         
              AF2bc_fRMSE(ahead,2)';      
              AF3_fRMSE(ahead,2)';         
              AF3bc_fRMSE(ahead,2)';     
              AF4_fRMSE(ahead,2)';         
              AF4bc_fRMSE(ahead,2)';      
              SRTPC1C2_fRMSE(ahead,2)';
              SRTPC1C2bc_fRMSE(ahead,2)'];        
        
        
%
% ... for the 5Y maturity segment
%
ZZmat_5Y = [  DNS_fRMSE(ahead,6)';   
              DNSbc_fRMSE(ahead,6)';       
              DSS_fRMSE(ahead,6)';           
              DSSbc_fRMSE(ahead,6)';        
              SRB3_fRMSE(ahead,6)';           
              SRB3bc_fRMSE(ahead,6)';        
              SRB4_fRMSE(ahead,6)';           
              SRB4bc_fRMSE(ahead,6)';      
              JSZ_fRMSE(ahead,6)';           
              JSZbc_fRMSE(ahead,6)';           
              AF2_fRMSE(ahead,6)';         
              AF2bc_fRMSE(ahead,6)';      
              AF3_fRMSE(ahead,6)';         
              AF3bc_fRMSE(ahead,6)';     
              AF4_fRMSE(ahead,6)';         
              AF4bc_fRMSE(ahead,6)';      
              SRTPC1C2_fRMSE(ahead,6)';
              SRTPC1C2bc_fRMSE(ahead,6)'];        
        
%
% ... for the 10Y maturity segment
%
ZZmat_10Y = [ DNS_fRMSE(ahead,11)';   
              DNSbc_fRMSE(ahead,11)';       
              DSS_fRMSE(ahead,11)';           
              DSSbc_fRMSE(ahead,11)';        
              SRB3_fRMSE(ahead,11)';           
              SRB3bc_fRMSE(ahead,11)';        
              SRB4_fRMSE(ahead,11)';           
              SRB4bc_fRMSE(ahead,11)';      
              JSZ_fRMSE(ahead,11)';           
              JSZbc_fRMSE(ahead,11)';           
              AF2_fRMSE(ahead,11)';         
              AF2bc_fRMSE(ahead,11)';      
              AF3_fRMSE(ahead,11)';         
              AF3bc_fRMSE(ahead,11)';     
              AF4_fRMSE(ahead,11)';         
              AF4bc_fRMSE(ahead,11)';      
              SRTPC1C2_fRMSE(ahead,11)';
              SRTPC1C2bc_fRMSE(ahead,11)'];         
       
ZZZ_tex_3msegment = latex(vpa(sym(ZZmat_3m),2));
ZZZ_tex_1Ysegment = latex(vpa(sym(ZZmat_1Y),2));
ZZZ_tex_5Ysegment = latex(vpa(sym(ZZmat_5Y),2));
ZZZ_tex_10Ysegment = latex(vpa(sym(ZZmat_10Y),2));

%% RW forecasts
%
RW_fErr = NaN(horizon+1,GSW_.nTau,nIter);
for ( j=1:nIter )
    RW_cast        = Y(startIndx+j-1,:);
    oYields        = Y(startIndx+j-1:startIndx+j-1+horizon,:);
    RW_fErr(:,:,j) = oYields-RW_cast;
end
RW_fRMSE = 100.*sqrt(mean((SRTPC1C2bc_fErr.^2),3));

ZZ_RW = RW_fRMSE(ahead,[1 2 6 11])';

ZZZ_tex_RW = latex(vpa(sym(ZZ_RW),2));

%%  Plots 
%

%
% ... 1Y maturity, 12 months ahead
%
hori = 13;
matu = 11;
figure
    subplot(2,1,1), plot(dates(startIndx+1:end-12,1),Y(startIndx+1:end-12,matu))
    datetick('x','mmm-yy')
    subplot(2,1,2), plot(dates(startIndx+1:end-12,1),squeeze(DNS_fErr(hori,matu,:).^2))
    hold on
    subplot(2,1,2), plot(dates(startIndx+1:end-12,1),squeeze(JSZbc_fErr(hori,matu,:).^2))
    hold on
    subplot(2,1,2), plot(dates(startIndx+1:end-12,1),squeeze(AF2_fErr(hori,matu,:).^2))
    datetick('x','mmm-yy'), legend('DNS','JSZ','AFSRB2')

%% Conditional forecasting exercise
%
nCast         = 60;
indxStart     = find(dates==US_MacroVariables(1,1),1,'first');  
                                     % index to match yield and macro data
datesX        = dates(indxStart:end,1);
datesCast     = (dates(end,1):31:dates(end,1)+(nCast)*31)';
SRB3          = TSM;
SRB3.yields   = Y(indxStart:end,:);
SRB3.tau      = tau;
SRB3.DataFreq = 12;
SRB3.nF       = 3;
SRB3.eXo      = [US_MacroVariables(:,2)./25 US_MacroVariables(:,3)];
SRB3          = SRB3.getSRB3;
%
% ... convert the VAR part of the model into SSM format
%
SRB3_SSM     = TSM2SSM; 
SRB3_SSM.TSM = SRB3;
SRB3_SSM     = SRB3_SSM.getMdl;
nX           = SRB3_SSM.TSM.nF+SRB3_SSM.TSM.nVarExo;  % number of factors and exogenous variables
AA           = [ SRB3_SSM.Mdl.A(1:nX,1:nX*2);
                 zeros(nX,nX) eye(nX) ]; 
BB           = [ SRB3_SSM.Mdl.B(1:nX,1:nX); zeros(nX,nX)];
CC           = eye(nX*2);
stateType    = [ zeros(1,nX), ones(1,nX) ];
castMdl      = ssm(AA,BB,CC,'statetype',stateType);  % VAR model as SSM model

beta_Cast      = [ NaN( size(SRB3_SSM.Mdl.B,2), nCast); ones(nX,nCast)];
beta_Cast(1:nX,1) = SRB3_SSM.TSM.beta(:,end);    
                                 % start projections at last obs of factors  

% .......................................
% ... Conditional forecasting examples
% .......................................

%
% 0: unconditional forecast
%
beta_0   = beta_Cast;

filter_0 = [ [SRB3_SSM.TSM.beta(:,end)' ones(1,size(BB,2))] ; filter(castMdl,beta_0') ];
Y_0      = [SRB3_SSM.Mdl.C(1:nTau,1:nX*2)*filter_0']';

figure('units','normalized','outerposition',[0 0 1 1])
    surf(tau./12,datesCast,Y_0)
    date_ticks = datenum(2018:1:2024,1,1);
    set(gca, 'ytick', date_ticks);
    datetick('y','mmm-yy','keepticks')
    xticks(0:1:11), xticklabels({tau}),
    xlabel('Maturity (months)'), zlabel('Yield (pct)'), 
    zlim([0 5])
    view([-53 16]),
    ytickangle(25),
    set(gca, 'FontSize', 18)
    %print -depsc Forecast_Y0

figure('units','normalized','outerposition',[0 0 1 1])
    plot(datesCast,filter_0(:,4:5),'LineWidth',2) 
    date_ticks = datenum(2018:1:2024,1,1);
    set(gca, 'xtick', date_ticks);
    datetick('x','mmm-yy','keepticks')
    ylabel(' (pct)'), legend('CU (scaled)', 'INFL')
    ylim([0 4])
    set(gca, 'FontSize', 18)
    %print -depsc Forecast_X0
    
%
% 1: random walk assumption on macro variables
%
beta_1              = beta_Cast;
beta_1(4:5,2:nCast) = repmat(beta_1(4:5,1),1,nCast-1); 

filter_1 = [ [SRB3_SSM.TSM.beta(:,end)' ones(1,size(BB,2))] ; filter(castMdl,beta_1') ];
Y_1      = [SRB3_SSM.Mdl.C(1:nTau,1:nX*2)*filter_1']';

figure('units','normalized','outerposition',[0 0 1 1])
    surf(tau./12,datesCast,Y_1)
    date_ticks = datenum(2018:1:2024,1,1);
    set(gca, 'ytick', date_ticks);
    datetick('y','mmm-yy','keepticks')
    xticks(0:1:11), xticklabels({tau}),
    xlabel('Maturity (months)'), zlabel('Yield (pct)'), 
    zlim([0 6])
    view([-53 16]),
    ytickangle(25),
    set(gca, 'FontSize', 18)
    %print -depsc Forecast_Y1
    
figure('units','normalized','outerposition',[0 0 1 1])
    plot(datesCast,filter_1(:,4:5),'LineWidth',2) 
    date_ticks = datenum(2018:1:2024,1,1);
    set(gca, 'xtick', date_ticks);
    datetick('x','mmm-yy','keepticks')
    legend('CU (scaled)', 'INFL')
    ylim([0 4])
    set(gca, 'FontSize', 18)
    %print -depsc Forecast_X1
   
%
% 2: Inflation overshooting, and increased CU 
%
nn     = 12;
beta_2 = beta_Cast;
a      = beta_Cast(5,1);
b      = 2*a;
a1     = 2.5;
g1     = (b/a)^(1/nn);
g2     = (a1/b)^(1/nn); 
infl_  = [a*g1.^(0:nn) b*g2.^(0:nn) ];
cu_    = linspace(beta_Cast(4,1),beta_Cast(4,1)+0.25,nn+1);

beta_2(4,1:length(cu_))   = cu_;
beta_2(5,1:length(infl_)) = infl_; 
beta_2(5,length(infl_):end) = 2.5;

filter_2 = [ [SRB3_SSM.TSM.beta(:,end)' ones(1,size(BB,2))] ; filter(castMdl,beta_2') ];
Y_2      = [SRB3_SSM.Mdl.C(1:nTau,1:nX*2)*filter_2']';

figure('units','normalized','outerposition',[0 0 1 1])
    surf(tau./12,datesCast,Y_2)
    date_ticks = datenum(2018:1:2024,1,1);
    set(gca, 'ytick', date_ticks);
    datetick('y','mmm-yy','keepticks')
    xticks(0:1:11), xticklabels({tau}),
    xlabel('Maturity (months)'), zlabel('Yield (pct)'), 
    zlim([0 25])
    view([-64 25]),
    ytickangle(25),
    set(gca, 'FontSize', 18)
    %print -depsc Forecast_Y2

figure('units','normalized','outerposition',[0 0 1 1])
    plot(datesCast,filter_2(:,4:5),'LineWidth',2) 
    date_ticks = datenum(2018:1:2024,1,1);
    set(gca, 'xtick', date_ticks);
    datetick('x','mmm-yy','keepticks')
    legend('CU (scaled)', 'INFL')
    ylim([0 5])
    set(gca, 'FontSize', 18)
    %print -depsc Forecast_X2


%
% 3: New drop in inflation  
%
nn     = 12;
beta_3 = beta_Cast;
a      = beta_Cast(5,1);
b      = a-0.25;
a1     = 2;
g1     = (b/a)^(1/nn);
g2     = (a1/b)^(1/nn); 
infl_  = [a*g1.^(0:nn) b*g2.^(0:nn) ];
cu_    = linspace(beta_Cast(4,1),beta_Cast(4,1)-0.05,nn+1);

beta_3(4,1:length(cu_))   = cu_;
beta_3(5,1:length(infl_)) = infl_; 
%beta_3(5,length(infl_):end) = 2.5;

filter_3 = [ [SRB3_SSM.TSM.beta(:,end)' ones(1,size(BB,2))] ; filter(castMdl,beta_3') ];
Y_3      = [SRB3_SSM.Mdl.C(1:nTau,1:nX*2)*filter_3']';

figure('units','normalized','outerposition',[0 0 1 1])
    surf(tau./12,datesCast,Y_3)
    date_ticks = datenum(2018:1:2024,1,1);
    set(gca, 'ytick', date_ticks);
    datetick('y','mmm-yy','keepticks')
    xticks(0:1:11), xticklabels({tau}),
    xlabel('Maturity (months)'), zlabel('Yield (pct)'), 
    zlim([0 4])
    view([-53 16]),
    ytickangle(25),
    set(gca, 'FontSize', 18)
    %print -depsc Forecast_Y3

figure('units','normalized','outerposition',[0 0 1 1])
    plot(datesCast,filter_3(:,4:5),'LineWidth',2) 
    date_ticks = datenum(2018:1:2024,1,1);
    set(gca, 'xtick', date_ticks);
    datetick('x','mmm-yy','keepticks')
    legend('CU (scaled)', 'INFL','location','NW')
    ylim([0 4])
    set(gca, 'FontSize', 18)
    %print -depsc Forecast_X3


%
% 4: high growth, inflation under control  
%
nn     = 36;
beta_4 = beta_Cast;
a      = beta_Cast(5,1);
b      = a;
a1     = 2;
g1     = (b/a)^(1/nn);
g2     = (a1/b)^(1/nn); 
infl_  = [a*g1.^(0:nn/2) b*g2.^(0:nn/2) ];
cu_    = linspace(beta_Cast(4,1),beta_Cast(4,1)+0.15,nn+1);

beta_4(4,1:length(cu_))   = cu_;
beta_4(5,1:length(infl_)) = infl_; 
%beta_4(5,length(infl_):end) = 2.5;

filter_4 = [ [SRB3_SSM.TSM.beta(:,end)' ones(1,size(BB,2))] ; filter(castMdl,beta_4') ];
Y_4      = [SRB3_SSM.Mdl.C(1:nTau,1:nX*2)*filter_4']';

figure('units','normalized','outerposition',[0 0 1 1])
    surf(tau./12,datesCast,Y_4)
    date_ticks = datenum(2018:1:2024,1,1);
    set(gca, 'ytick', date_ticks);
    datetick('y','mmm-yy','keepticks')
    xticks(0:1:11), xticklabels({tau}),
    xlabel('Maturity (months)'), zlabel('Yield (pct)'), 
    zlim([0 6])
    view([-53 16]),
    ytickangle(25),
    set(gca, 'FontSize', 18)
    %print -depsc Forecast_Y4

figure('units','normalized','outerposition',[0 0 1 1])
    plot(datesCast,filter_4(:,4:5),'LineWidth',2) 
    date_ticks = datenum(2018:1:2024,1,1);
    set(gca, 'xtick', date_ticks);
    datetick('x','mmm-yy','keepticks')
    legend('CU (scaled)', 'INFL','location','NW')
    ylim([0 4])
    set(gca, 'FontSize', 18)
    %print -depsc Forecast_X4

%% Fix-point projections
%
% h_target:  is the number of periods ahead at which the target is met 
% X_target:  is the fix-point forecast for the yield curve factor
% V       :  id the eigenvector of Phi

% % Function that calculate the adjusted mean
% m_target    = @(X_t, Phi, X_target,h_target) ...
%                1/h_target*((eye(length(X_t))-Phi)^(-1)*(X_target-Phi*X_t));

% Function that calculates  
D_target = @(X_t, V, m, X_target, h_target) ...
                    diag(((V^(-1)*(X_target-m))./(V\(X_t-m))).^(1/h_target));

% ... Using the model with factors equal to the: short rate, term premium, 
%           and C1 and C2
%
nCast     = 60;
datesCast = (dates(end,1):31:dates(end,1)+(nCast-1)*31)';

SR_TP = TSM;
SR_TP.yields   = GSW_.yields;
SR_TP.tau      = GSW_.tau;
SR_TP.nF       = 3;
SR_TP.DataFreq = 12;
SR_TP          = SR_TP.getSRTPC1C2;  % est model with SR,TP,C1,C2

% ... Generating scenarios
%
% ... Scenario 1: (a) TP goes to 0% in 6 months,  
%                 (b) Thereafter TP goes to 2% after additional 12 months
%                        while short rate stays low
%                 (c) At the end of the 60 months projection horizon,
%                        the short rate converges to 4% and the TP to 3%

X_t1           = SR_TP.beta(:,end);
X_t1(2,1)      = 0;
X_t2           = SR_TP.beta(:,end);
X_t2(2,1)      = 2;
X_t3           = SR_TP.beta(:,end);
X_t3(1,1)      = 4.00;
X_t3(2,1)      = 1.50;
h1             = 6;
h2             = 12;
h3             = 42;
beta_proj      = NaN(SR_TP.nF+1,h1+h2+h3);
beta_proj(:,1) = SR_TP.beta(:,end);

[V,D] = eig(SR_TP.PhiP);
D_1   = D_target( beta_proj(:,1), V, SR_TP.mP, X_t1, h1-1 );
for ( j=2:h1+1 )
    beta_proj(:,j) = SR_TP.mP + (V*(D_1)*V^(-1)) * ...
                                           (beta_proj(:,j-1) - SR_TP.mP);
end
D_2   = D_target( beta_proj(:,h1), V, SR_TP.mP, X_t2, h2 );
for ( j=h1+1:h1+h2+1 )
    beta_proj(:,j) = SR_TP.mP + (V*(D_2)*V^(-1)) * ...
                                           (beta_proj(:,j-1) - SR_TP.mP);
end
D_3   = D_target( beta_proj(:,h1+h2), V, SR_TP.mP, X_t3, h3 );
for ( j=h1+h2+1:h1+h2+h3 )
    beta_proj(:,j) = SR_TP.mP + (V*(D_3)*V^(-1)) * ...
                                           (beta_proj(:,j-1) - SR_TP.mP);
end
beta_proj = real(beta_proj);
Y_proj    = (SR_TP.B*real(beta_proj))';
fDates    = cumsum([h1;h2;h3]);

figure('units','normalized','outerposition',[0 0 1 1])
    surf(tau./12,datesCast,Y_proj)
    date_ticks = datenum(2018:1:2024,1,1);
    set(gca, 'ytick', date_ticks);
    datetick('y','mmm-yy','keepticks')
    xticks(0:1:11), xticklabels({tau}),
    xlabel('Maturity (months)'), zlabel('Yield (pct)'), 
    zlim([0 8])
    view([-53 16]),
    ytickangle(25),
    set(gca, 'FontSize', 18)
    %print -depsc Y_fixed_point_1         
          
figure('units','normalized','outerposition',[0 0 1 1])
    plot(datesCast,beta_proj,'LineWidth',2), 
    hold on
    plot(datesCast(fDates,1),beta_proj(:,fDates')','*b','LineWidth',5), ...
         legend('Short rate','10-year term premium','Curvature 1','Curvature 2','Fix-points','Location','NW')
    hold off
    date_ticks = datenum(2018:1:2024,1,1);
    set(gca, 'xtick', date_ticks);
    datetick('x','mmm-yy','keepticks')
    set(gca, 'FontSize', 18)    
    %print -depsc beta_fixed_point_1


