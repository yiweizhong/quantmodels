classdef GSW
    %
    % Usage:  .getYields   ->  converts factors and lambdas into zero yieds
    %         .getForwards ->  converts factors and lambdas into forwards
    %
    % GSW: converts Gurkaynak, Sack and Wright yield curve factors into 
    %      yield and forward curves for a user-selected set of maturities.
    %
    %      The conversion is based on the Svensson-Soderlind model as 
    %      applied by GSW in their Fed working paper: 
    %             "The U.S. Treasury Yield Curve: 1961 to the Present"
    %              Refet S. Gurkaynak, Brian Sack, and Jonathan H. Wright
    %              2006-28 
    %
    %      Gurkaynak, Sack and Wright data can be downloaded from the 
    %      Fed homepage (http://www.federalreserve.gov/pubs/feds/2006/200628/200628abs.html). 
    %
    % Input:
    %    beta     :   GSW yield curve factors         (nObs-by-4)
    %    lambda   :   holds the time-decay parameters (nObs-by-2) [called tau by GSW]
    %    tau      :   vector of maturities (in months)(nTau-by-1) 
    %                 e.g. 24 is provided for a 2year maturity
    % Output:
    %    yields   :   zero-coupon yield curves
    %    forwards :   forward curves
    %
    %    Activate calculations: 
    %                           getYields   -> obtains zero coupon curves
    %                           getForwards -> obtains instantaneous forward curves
    %
    % Ken Nyholm
    % June 2018
    % Version 1.0
    %

    properties (Access='public')
        beta, lambda double
        tau double = [3 12:12:120]';
        yields double
        forwards double
        flagg double = 0
        nObs double 
        nTau double 
    end
    properties (Access='private')
        yLoadings, fLoadings, l_t double     
    end
    
    methods (Access = 'private')
        function [GSW] = chkInput(GSW)
            % checks input and generates aux-variables
            %
            if ( size(GSW.beta,2)==4 && size(GSW.lambda,2)==2 && ...
                 size(GSW.beta,1)==size(GSW.lambda,1) && ...
                 size(GSW.tau,1)>=size(GSW.tau,2) )
                GSW.flagg = 1;
                GSW.nObs  = size(GSW.beta,1);
                GSW.nTau  = size(GSW.tau,1);
            else
                GSW.flagg = 0;
                disp('Something is wrong: Please check the dimensions of the supplied variables...')    
                return
            end
        end
        function [GSW] = Loadings(GSW)
            % calculates the Svensson-Soderlind yield and forward loadings
            %
            GSW.fLoadings = [ ones(GSW.nTau,1) exp(-(1/12).*GSW.tau./GSW.l_t(1,1) ) ...
                              ((1/12).*GSW.tau./GSW.l_t(1,1)).*exp(-(1/12).*GSW.tau./GSW.l_t(1,1)) ... 
                              ((1/12).*GSW.tau./GSW.l_t(1,2)).*exp(-(1/12).*GSW.tau./GSW.l_t(1,2)) ];
                          
            GSW.yLoadings = [ ones(GSW.nTau,1) (1-exp(-(1/12).*GSW.tau./GSW.l_t(1,1)))./((1/12).*GSW.tau./GSW.l_t(1,1)) ... 
                      (1-exp(-(1/12).*GSW.tau./GSW.l_t(1,1)))./((1/12).*GSW.tau./GSW.l_t(1,1))-exp(-(1/12).*GSW.tau./GSW.l_t(1,1)) ...
                      (1-exp(-(1/12).*GSW.tau./GSW.l_t(1,2)))./((1/12).*GSW.tau./GSW.l_t(1,2))-exp(-(1/12).*GSW.tau./GSW.l_t(1,2)) ];
        end
    end
    
    methods ( Access='public' )
        function [GSW] = getYields(GSW)
            [GSW] = chkInput(GSW);
            for ( j=1:GSW.nObs )
                GSW.l_t         = GSW.lambda(j,:);
                GSW             = Loadings(GSW);
                GSW.yields(j,:) = GSW.beta(j,:)*GSW.yLoadings';
            end
        end
        function [GSW] = getForwards(GSW)
            [GSW] = chkInput(GSW);
            for ( j=1:GSW.nObs )
                GSW.l_t           = GSW.lambda(j,:);
                GSW               = Loadings(GSW);
                GSW.forwards(j,:) = GSW.beta(j,:)*GSW.fLoadings';
            end      
        end
    end  
end