classdef TSM2SSM 
    %
    % Requirement: (1) an estimated TSM instance
    %              (2) access to MATLAB's Econometric Toolbox
    %                     the TSM2SSM class uses the state-space modelling
    %                     capabilities of this toolbox
    %
    % Usage   : 
    %              .getMdl  -> converts the supplied TSM into the variables
    %                             reguired for using the SSM toolbox
    %
    %
    % TSM2SSM : converts an estimated TSM model into MATLAB's state-space 
    %              format to allow easier unconditional and conditional
    %              forecasting and scenario generation of the estimated
    %              TSM.
    %              
    % Input   : 
    % ---------
    %           .TSM        <-    estimated TSM class
    %
    % Output  :
    % ---------
    %           TSM2SSM.Mdl contains a MATLAB state-space model ready to  
    %              be used by simulate, filter, smooth, and other 
    %              functionalities embedded in the SSM module 
    %
    %              The following setup is dictated by the SSM module:
    %              
    %              state eqn:  X_{t} = A * X_{t-1} + B * e_{t}
    %              obs eqn  :  y_{t} = C * X_{t}   + D * u_{t}
    %
    % Ken Nyholm
    % October 2018  
    % Version 1.0
    %
    properties ( Access=private )
       C1, C2, I1, I2  double
       A1, A2 double
       nX, nZ, nF, nObs, nTau double
    end
    properties ( Access=public )
       Data double
       TSM struct
       Mdl ssm
       Params = struct('A',0,'B',0,'C',0,'D',0,'Mean0',[],'Cov0',[],'StateType',[]);
    end
    methods ( Access=private )
        function [TSM2SSM] = convert(TSM2SSM)  
            [nF,nObs] = size(TSM2SSM.TSM.beta);
            nTau      = TSM2SSM.TSM.nTau;        % number of maturities
            nZ        = TSM2SSM.TSM.nVarExo;     % number of exo variables
            nX        = nF-nZ;                   % number of yield factors
            if (TSM2SSM.TSM.biasCorrect==1)
                A1 = TSM2SSM.TSM.PhiP_bc;
            else
                A1 = TSM2SSM.TSM.PhiP;
            end
            A2 = diag( (eye(nF)-A1)*TSM2SSM.TSM.mP);
            TSM2SSM.Data     = [ TSM2SSM.TSM.yields TSM2SSM.TSM.beta' ones(nObs,nZ+nX) ones(nObs,nTau) ];
            TSM2SSM.Params.A = [A1 A2 zeros(nF,nTau); zeros(nF+nTau,nF) eye(nF+nTau)];
            TSM2SSM.Params.B = [ TSM2SSM.TSM.Omega_beta; zeros(nF+nTau,nF) ];
            C1               = [ TSM2SSM.TSM.B zeros(nTau,nZ) zeros(nTau,nF) diag(TSM2SSM.TSM.A) ];
            C2               = [ eye(nF+nF+nTau) ]; 
            TSM2SSM.Params.C = [ C1; C2 ];
            TSM2SSM.Params.D = [ TSM2SSM.TSM.Omega_yields; zeros(nF+nF+nTau,nTau)];
            TSM2SSM.Params.StateType = [zeros(nF,1); ones(nF+nTau,1)]; 
            TSM2SSM.Mdl              = ssm(TSM2SSM.Params.A,TSM2SSM.Params.B, ...
                                           TSM2SSM.Params.C,TSM2SSM.Params.D, ...
                                           'Mean0',TSM2SSM.Params.Mean0, ...
                                           'Cov0', TSM2SSM.Params.Cov0, ...
                                           'StateType', TSM2SSM.Params.StateType );
        end
    end
    methods ( Access=public )
        function [TSM2SSM] = getMdl(TSM2SSM)
            [TSM2SSM] = convert(TSM2SSM);
        end
    end
end



