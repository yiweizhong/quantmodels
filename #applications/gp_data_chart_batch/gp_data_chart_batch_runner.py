from __future__ import division
import _path
import os, sys, inspect
import logging
import common.dirs as dirs
import common.emails as emails
#import chart.PlotWrapper as plotwrapper
import data.database as db
import data.datatools as tools
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mpd
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
import numpy as np
import pandas as pd
from datetime import date
from dateutil.relativedelta import relativedelta
import gp_data_chart as gdc
import gp_data_chart_batch_config as cfg


__author__ = "Yiwei Zhong"
__copyright__ = "AMP Capital Fixed Income Macro Market"
__version__ = "0.0.1"
__maintainer__ = "Yiwei Zhong"
__email__ = "Yiwei.Zhong@ampcapital.com"
__status__ = "Testing"


#'SMPLIFAB',
gp_funds_credit = ['SMPICOFI', 'SMPSUNAF','SMPSUNSF','SMPSUNRF','ACIWSABF','SMPAVSU','SMPGEFI','SMPADFI','SMPRABF','SMPDKAFI','SMPDAIFI','SMPNABMF','NMFAUCP',
            'SMPACSC','ACIWASFI','SMPMAHIB','SMPSSFCB','SMPWRKCV','NMFAUAC','SMPAVANT','IWIGACI','SMPAAIB',
            'ACIMACSF', 'ABDFDAMP', 'IWI2ACI', 'NMFAUWGF', 'WEIBACI', 'SMPLGSFI','ACIWALFO', 'ACIACBND','ACZIMAC', 'SMPWFI1','SMPMGIBF']
#removed 'ACIFRIFD',
gp_funds = ['SMPICOFI', 'SMPSUNAF','SMPSUNSF','SMPSUNRF','ACIWSABF','SMPAVSU','SMPGEFI','SMPADFI','SMPRABF','SMPDKAFI','SMPDAIFI','SMPNABMF','NMFAUCP',
            'SMPACSC','ACIWASFI','SMPMAHIB','SMPSSFCB','SMPWRKCV','NMFAUAC','SMPAVANT','IWIGACI','SMPAAIB',
            'ACIMACSF', 'ABDFDAMP', 'IWI2ACI', 'NMFAUWGF', 'WEIBACI', 'SMPLGSFI','ACIWALFO','ACZIMAC', 'SMPWFI1','SMPMGIBF']
#removed 'ACIFRIFD',
gp_funds1 = ['SMPICOFI', 'ABDFDAMP', 'IWI2ACI','WEIBACI','ACIMACSF','SMPACSC','SMPRABF', 'SMPADFI', 'SMPSUNAF','SMPSUNSF','SMPSUNRF','ACZIMAC', 'SMPWFI1','SMPMGIBF']

gp_funds_port_duration = ['ACIACBND'] 

#overrides
#gp_funds = ['SMPSUNRF']
#gp_funds1 = ['SMPSUNRF']
#gp_funds_port_duration = ['ACIACBND']
#gp_funds_credit = ['SMPSUNRF']



#------------------------------------------------------------------------------------------------
def get_AU_TOTVSTWI_chart_data(runDate):
#------------------------------------------------------------------------------------------------
    query = r"""SELECT [DATE], [AUD] FROM [Fixed_Income].[calc].[z_TOTVSTWI]
                WHERE DATEDIFF(Year,[DATE],'{0}') <= 20 
                and [DATE] >= '2000-01-01' 
                Order by [DATE] asc""".format(runDate)
    return query

#------------------------------------------------------------------------------------------------
def get_GP_RATE_chart_data(length, funds, runDate):
#------------------------------------------------------------------------------------------------
    #,rnc.Duration_Active [Active_Duration]
    query = r"""SELECT rnc.[DATE] ,rnc.[Portfolio]
              ,rnc.Duration_Active [Active_Duration]
              ,te.[1 SD Standalone TE % Market Value_AUD IR] [TrackingError]
              ,krd.[Parametric Steepeners_3/10]/100 [Steepeners_3vs10]
              FROM [dbo].[TS_RnC_Report_Holistic] rnc
              left outer join [dbo].[TS_Standalone_TE] te on rnc.DATE = te.DATE
              left outer join [dbo].[TS_KRD_Active_Holistic] krd on rnc.DATE = krd.DATE
              Where te.Portfolio = rnc.Portfolio
                and te.Portfolio = krd.Portfolio
                and te.Portfolio in ({1})				
                and DATEDIFF(Month,rnc.[DATE],'{2}') <= {0}
                and rnc.[DATE] <='{2}'
				and rnc.[DATE] not in ('2017-01-09','2017-03-17','2017-04-03','2017-05-30','2017-05-31','2017-06-01',
                '2017-11-27','2017-11-28','2017-11-29', '2017-12-03', '2017-12-14', '2018-02-23','2018-11-12','2018-12-18' , '2019-01-02', '2019-01-30',
                '2019-04-24', '2019-04-30', '2019-05-30', '2019-09-16', '2019-09-30', '2019-10-17', '2020-02-21', '2020-03-30', '2020-03-31', '2020-04-30'
                ,'2020-07-24','2020-08-07','2020-08-31','2020-10-21','2020-11-30', '2020-12-01', '2020-12-23' ,'2020-12-31', '2021-03-31', '2021-02-12' )			
            order by rnc.DATE asc""".format(length, funds, runDate)
    return query


#------------------------------------------------------------------------------------------------
def get_GP_RATE_chart_data2(length, funds, runDate, curr):
#------------------------------------------------------------------------------------------------
    query = r"""SELECT rnc.[DATE] ,rnc.[Portfolio]
              ,rnc.Duration_Active [Active_Duration]
              ,te.[1 SD Standalone TE % Market Value_{3} IR] [TrackingError]
              ,krd.[Parametric Steepeners_3/10]/100 [Steepeners_3vs10]
              FROM [dbo].[TS_RnC_Report_Holistic] rnc
              left outer join [dbo].[TS_Standalone_TE] te on rnc.DATE = te.DATE
              left outer join [dbo].[TS_KRD_Active_Holistic] krd on rnc.DATE = krd.DATE
              Where te.Portfolio = rnc.Portfolio
                and te.Portfolio = krd.Portfolio
                and te.Portfolio in ({1})
                and DATEDIFF(Month,rnc.[DATE],'{2}') <= {0}
                and rnc.[DATE] <= '{2}'
            order by rnc.DATE asc""".format(length, funds, runDate, curr)
    return query


#------------------------------------------------------------------------------------------------
def get_GP_RATE_chart_data3(length, funds, runDate):
#------------------------------------------------------------------------------------------------
    # Using Duration_fund instead of Port_duration
    query = r"""SELECT rnc.[DATE] ,rnc.[Portfolio]
              ,rnc.Duration_Fund [Active_Duration]
              ,te.[1 SD Standalone TE % Market Value_AUD IR] [TrackingError]
              ,krd.[Parametric Steepeners_3/10]/100 [Steepeners_3vs10]
              FROM [dbo].[TS_RnC_Report_Holistic] rnc
              left outer join [dbo].[TS_Standalone_TE] te on rnc.DATE = te.DATE
              left outer join [dbo].[TS_KRD_Active_Holistic] krd on rnc.DATE = krd.DATE
              Where te.Portfolio = rnc.Portfolio
                and te.Portfolio = krd.Portfolio
                and te.Portfolio in ({1})				
                and DATEDIFF(Month,rnc.[DATE],'{2}') <= {0}
                and rnc.[DATE] <='{2}'
				and rnc.[DATE] not in ('2017-01-09','2017-03-17','2017-04-03','2017-05-30','2017-05-31','2017-06-01',
                '2017-11-27','2017-11-28','2017-11-29', '2017-12-03', '2017-12-14', '2018-02-23', '2018-12-18', '2019-01-02', '2019-01-30',
                '2019-04-24', '2019-04-30', '2019-05-30', '2019-09-16', '2019-09-30', '2019-10-17', '2020-02-21', '2020-03-30', '2020-03-31', '2020-04-30'
                ,'2020-07-24','2020-08-07','2020-08-31','2020-10-21','2020-11-30', '2020-12-01','2020-12-23' ,'2020-12-31', '2021-02-12' )			
            order by rnc.DATE asc""".format(length, funds, runDate)
    return query

#------------------------------------------------------------------------------------------------
def get_GP_CREDIT_chart_data(length, funds, runDate):
#------------------------------------------------------------------------------------------------
    query = r"""SELECT rnc.[DATE], rnc.[Portfolio]
,ROUND(isnull([Duration Contribution_Local Govt],0) + isnull([Duration Contribution_Supra Govt Guara],0) + isnull([Duration Contribution_Credit],0) + isnull([Duration Contribution_Swap],0),2) [SwapBeta]
,ROUND([Spread Duration Contribution_Credit],2) [CreditSpreadDurationContribution]
,dah.[DxS] [DxS]
FROM [dbo].[TS_RnC_Report_Active_Holistic] rnc
inner join [dbo].[TS_DxS_Active_Holistic] dah on rnc.[DATE] = dah.[DATE] and rnc.[Portfolio] = dah.[Portfolio]
where rnc.Portfolio in ({1})
and DATEDIFF(Month,rnc.[DATE],'{2}') <=  {0}
and rnc.[DATE] <= '{2}'
and rnc.[DATE] not in ('2016-04-06', '2016-04-07','2016-04-08','2016-04-09', '2016-04-11', '2016-12-22', '2016-12-21', 
'2016-12-06', '2017-03-13', '2017-03-09','2017-03-17','2017-06-01','2017-11-28','2017-11-29', '2017-12-03', '2018-12-12', '2019-01-02',
'2019-04-24', '2019-07-29', '2019-08-05', '2019-08-06','2019-09-16', '2019-09-30', '2019-10-17' , '2020-02-21', '2020-12-23')
order by rnc.DATE asc""".format(length, funds, runDate)
    return query


#------------------------------------------------------------------------------------------------
def get_GP_TE_chart_data(length, funds, runDate):
#------------------------------------------------------------------------------------------------
    query = r"""SELECT distinct [DATE], [Portfolio]
, [Analytical VaR_1 SD TE][TotalLongTermTE]
, convert(decimal(4, 2), (isnull([1 SD Marginal TE_USD IR], 0)
                          + isnull([1 SD Marginal TE_EUR IR], 0)
                          + isnull([1 SD Marginal TE_GBP IR], 0)
                          + isnull([1 SD Marginal TE_Other IR], 0)) / [Analytical VaR_1 SD TE])[OtherRates]
, convert(decimal(4, 2), isnull([1 SD Marginal TE_AUD IR], 0) / [Analytical VaR_1 SD TE])[AUDRates]
, convert(decimal(4, 2), isnull([1 SD Marginal TE_Other Spread], 0) / [Analytical VaR_1 SD TE])[OtherSpreads]
, convert(decimal(4, 2), isnull([1 SD Marginal TE_Corporate Spread], 0) / [Analytical VaR_1 SD TE])[CorpSpreads]
, convert(decimal(4, 2), isnull([1 SD Marginal TE_FX], 0) / [Analytical VaR_1 SD TE])[FX]
, convert(decimal(4, 2), isnull([1 SD Marginal TE_Idiosyncratic], 0) / [Analytical VaR_1 SD TE])[Idiosyncratic]
, (1 - convert(decimal(4, 2), (
isnull([1 SD Marginal TE_USD IR], 0) + isnull([1 SD Marginal TE_EUR IR], 0) + isnull([1 SD Marginal TE_GBP IR],
                                                                                     0) + isnull(
    [1 SD Marginal TE_Other IR], 0)) / [Analytical VaR_1 SD TE])
   - convert(decimal(4, 2), isnull([1 SD Marginal TE_AUD IR], 0) / [Analytical VaR_1 SD TE])
   - convert(decimal(4, 2), isnull([1 SD Marginal TE_Other Spread], 0) / [Analytical VaR_1 SD TE])
   - convert(decimal(4, 2), isnull([1 SD Marginal TE_Corporate Spread], 0) / [Analytical VaR_1 SD TE])
   - convert(decimal(4, 2), isnull([1 SD Marginal TE_FX], 0) / [Analytical VaR_1 SD TE])
   - convert(decimal(4, 2), isnull([1 SD Marginal TE_Idiosyncratic], 0) / [Analytical VaR_1 SD TE])
   )[Others]
FROM[dbo].[TS_Standalone_TE] te
where te.portfolio in ({1})
and DATEDIFF(Month, [DATE], '{2}') <= {0}
and [DATE] <= '{2}'
and [DATE] not in ('2016-09-02', '2016-09-03','2016-09-04','2016-11-30', '2016-12-01', '2016-10-10', '2016-10-11', '2016-10-12', 
'2016-10-13', '2016-10-14', '2016-10-15', '2016-10-16', '2016-12-20', 
'2016-12-21','2017-03-17','2017-03-18','2017-03-19','2017-04-03','2017-11-27','2017-11-28','2017-11-29','2017-11-30', '2017-12-01', 
'2017-12-02', '2017-12-03', '2017-12-04', '2018-02-22', '2018-02-23', '2018-02-24', '2018-06-14', '2018-11-12', '2018-12-17', 
'2018-12-18', '2018-12-19', '2018-12-31', '2019-01-02','2019-02-05','2019-04-24','2019-04-30','2019-05-29','2019-05-30','2019-06-07','2019-06-08','2019-06-09','2019-06-10',
'2019-06-12', '2019-07-31', '2019-08-30', '2019-08-31', '2019-09-16', '2019-09-30', '2019-10-14', '2019-10-15','2019-10-16','2019-10-17','2019-12-13', 
'2020-02-21', '2020-03-30', '2020-03-31', '2020-04-06', '2020-04-30', '2020-05-18', '2020-07-24','2020-08-07', '2020-08-31','2020-10-06','2020-10-21','2020-11-30', '2020-12-01',
'2020-12-31', '2021-03-31')
ORDER BY[DATE] ASC
""".format(length, funds, runDate)
    return query


def setup():
    """Log settings"""
    _log_folder = dirs.get_creat_sub_folder(folder_name="Log", parent_dir=cfg.APP_ROOT)
    _debug_folder = dirs.get_creat_sub_folder(folder_name="Debug", parent_dir=cfg.APP_ROOT)
    _output_folder = dirs.get_creat_sub_folder(folder_name="Output", parent_dir=cfg.APP_ROOT)
    _archive_folder = dirs.get_creat_sub_folder(folder_name="Archive", parent_dir=cfg.APP_ROOT)

    _archive_log_folder = dirs.get_creat_sub_folder(folder_name="Log", parent_dir=_archive_folder)
    _archive_debug_folder = dirs.get_creat_sub_folder(folder_name="Debug", parent_dir=_archive_folder)
    _archive_output_folder = dirs.get_creat_sub_folder(folder_name="Output", parent_dir=_archive_folder)

    _log_file_name = dirs.get_log_file_name(logfile_name_prefix=cfg.APP_NAME,log_folder=_log_folder)

    return _log_folder, _debug_folder, _output_folder, _archive_folder, _log_file_name, _archive_log_folder, _archive_debug_folder, _archive_output_folder


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-- Main
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':

    _log_folder, _debug_folder, _output_folder, _archive_folder, _log_file_name, _archive_log_folder, _archive_debug_folder, _archive_output_folder = setup()
    _exit_code = 9999

    try:


        logging.basicConfig(filename= _log_file_name, format=cfg.LOG_FORMAT, level=logging.DEBUG)
        logging.info("<<<< {0} starts >>>>".format(cfg.APP_NAME))

        dirs.archive_files(_debug_folder, _archive_debug_folder, copy_only = True)
        dirs.archive_files(_output_folder, _archive_output_folder, copy_only = True)

        _db = db.Database(cfg.FI_PROD_DB_CONN_STRING,_debug_folder)

        chartpack = gdc.gp_data_chart_pack(_debug_folder, debug=False)

        length_in_months = 6
        #length_in_months = 11
        #length_in_months = 5

        gp_funds = ['ACIACBND', 'ACIWSABF', 'IWIGACI', 'SMPNABMF','SMPNABMF','SMPDKAFI'] if length_in_months >6 else gp_funds
        gp_funds1 = ['ACIACBND', 'ACIWSABF', 'IWIGACI', 'SMPNABMF','SMPDKAFI'] if length_in_months >6 else gp_funds1
        gp_funds_credit = ['ACIACBND', 'ACIWSABF', 'IWIGACI', 'SMPNABMF','SMPDKAFI'] if length_in_months >6 else  gp_funds_credit 

        #gp_funds = ['SMPWFI1'] if length_in_months == 5 else gp_funds
        #gp_funds1 = ['SMPWFI1'] if length_in_months == 5 else gp_funds1
        #gp_funds_credit = ['SMPWFI1'] if length_in_months ==5 else  gp_funds_credit 



		#----------------------------
        # generate AU TOTvsTWI chart 
        #----------------------------

        #this chart doesnt need RUN_DATE_M1
        tot_twi_data = _db.load_single_table_from_db(get_AU_TOTVSTWI_chart_data(cfg.RUN_DATE), 'AU_TOT_VS_TWI_CHART_DATA', 
                                                        debug=True)
        
        if(len(tot_twi_data.index)>0):
            chartpack.plot_au_tot_twi_chart(tot_twi_data,
                                            output_file_name= "au_tot_vs_twi_zscore",
                                            width=12, height=8,
                                            output_dir=_output_folder,
                                            dpi=100)
        
        #----------------------------
        # generate GP RATES Chart without AU 3s10s 
        #----------------------------

        gp_RATES_data = _db.load_single_table_from_db(get_GP_RATE_chart_data(length_in_months, ",".join(map(lambda x: "'{0}'".format(x), gp_funds1)), cfg.RUN_DATE),
                                                      "GP_RATES_CHART_DATA")

        #for curr in ['AUD', 'USD', 'EUR', 'GBP', 'Other' ]:
        #    gp_RATES_data = _db.load_single_table_from_db(
        #        get_GP_RATE_chart_data2(length_in_months, ",".join(map(lambda x: "'{0}'".format(x), gp_funds)), cfg.RUN_DATE, curr),
        #        "GP_RATES_CHART_DATA")

        for fund in gp_funds1:
			fund_data = gp_RATES_data[gp_RATES_data["Portfolio"] == fund]
			if (len(fund_data.index) > 0): 
				chartpack.plot_gp_RATES_chart2(fund_data,
																  fund_name=fund,
																  output_file_name = "gp_RATES_chart_without_AU_Curve_{0}_months".format(length_in_months + 1),
																  width=7,
																  height=5,
																  output_dir=_output_folder,
																  dpi = 100)
	
		

        #----------------------------
        # generate GP RATES Chart
        #----------------------------

        gp_RATES_data = _db.load_single_table_from_db(get_GP_RATE_chart_data(length_in_months, ",".join(map(lambda x: "'{0}'".format(x), gp_funds)), cfg.RUN_DATE),
                                                      "GP_RATES_CHART_DATA")

        #for curr in ['AUD', 'USD', 'EUR', 'GBP', 'Other' ]:
        #    gp_RATES_data = _db.load_single_table_from_db(
        #        get_GP_RATE_chart_data2(length_in_months, ",".join(map(lambda x: "'{0}'".format(x), gp_funds)), cfg.RUN_DATE, curr),
        #        "GP_RATES_CHART_DATA")

        for fund in gp_funds:
			fund_data = gp_RATES_data[gp_RATES_data["Portfolio"] == fund]
			if len(fund_data.index) > 0:
				chartpack.plot_gp_RATES_chart(fund_data,
											  fund_name=fund,
											  output_file_name = "gp_RATES_chart_{0}_months".format(length_in_months + 1),
											  width=7,
											  height=5,
											  output_dir=_output_folder,
											  dpi = 100)

        
        #----------------------------
        # generate GP RATES Chart for absolute port duration
        #----------------------------

        gp_RATES_data_port_duration = _db.load_single_table_from_db(get_GP_RATE_chart_data3(length_in_months, ",".join(map(lambda x: "'{0}'".format(x), gp_funds_port_duration)), 
                                                      cfg.RUN_DATE),
                                                      "GP_RATES_CHART_DATA_PORT_DURATION")

        #for curr in ['AUD', 'USD', 'EUR', 'GBP', 'Other' ]:
        #    gp_RATES_data = _db.load_single_table_from_db(
        #        get_GP_RATE_chart_data2(length_in_months, ",".join(map(lambda x: "'{0}'".format(x), gp_funds)), cfg.RUN_DATE, curr),
        #        "GP_RATES_CHART_DATA")

        for fund in gp_funds_port_duration:
			fund_data = gp_RATES_data_port_duration[gp_RATES_data_port_duration["Portfolio"] == fund]
			if len(fund_data.index) > 0:
				chartpack.plot_gp_RATES_chart(fund_data,
											  fund_name=fund,
											  output_file_name = "gp_RATES_chart_{0}_months_port_duration".format(length_in_months + 1),
											  width=7,
											  height=5,
											  output_dir=_output_folder,
											  dpi = 100)

        #----------------------------
        # generate GP CREDIT Chart
        #----------------------------
        gp_CREDIT_data = _db.load_single_table_from_db(get_GP_CREDIT_chart_data(length_in_months, ",".join(map(lambda x: "'{0}'".format(x), gp_funds_credit)), 
                                                        cfg.RUN_DATE),
                                                        "GP_CREDIT_CHART_DATA")

        for fund in gp_funds_credit:
            fund_data = gp_CREDIT_data[gp_CREDIT_data["Portfolio"] == fund]
            if (len(fund_data.index) > 0):
			    chartpack.plot_gp_CREDIT_chart(fund_data,
										   fund_name=fund,
										   output_file_name="gp_CREDIT_chart_{0}_months".format(length_in_months + 1),
										   width=7,
										   height=5,
										   output_dir=_output_folder,
										   dpi=100)


        #----------------------------
        # generate GP CREDITRATE Chart
        #----------------------------
        for fund in gp_funds:
            credit_fund_data = gp_CREDIT_data[gp_CREDIT_data["Portfolio"] == fund]
            rate_fund_data = gp_RATES_data[gp_RATES_data["Portfolio"] == fund]
            if (len(credit_fund_data) and len(rate_fund_data))  > 0:
				chartpack.plot_gp_RATECREDIT_chart((credit_fund_data,rate_fund_data),
												   fund_name=fund,
												   output_file_name="gp_CREDITRATE_chart_{0}_months".format(length_in_months + 1),
												   width=14,
												   height=5,
												   output_dir=_output_folder,
												   dpi=100)

        for fund in gp_funds_port_duration:
            credit_fund_data = gp_CREDIT_data[gp_CREDIT_data["Portfolio"] == fund]
            rate_fund_data = gp_RATES_data_port_duration[gp_RATES_data_port_duration["Portfolio"] == fund]
            if (len(credit_fund_data) and len(rate_fund_data))  > 0:
				chartpack.plot_gp_RATECREDIT_chart((credit_fund_data,rate_fund_data),
												   fund_name=fund,
												   output_file_name="gp_CREDITRATE_chart_{0}_months_port_duration".format(length_in_months + 1),
												   width=14,
												   height=5,
												   output_dir=_output_folder,
												   dpi=100)


        # ----------------------------
        # generate GP TE Chart
        # ----------------------------
        gp_TE_data = _db.load_single_table_from_db(
            get_GP_TE_chart_data(length_in_months, ",".join(map(lambda x: "'{0}'".format(x), gp_funds)), cfg.RUN_DATE),
            "GP_TE_CHART_DATA")

        for fund in gp_funds:
            fund_data = gp_TE_data[gp_TE_data["Portfolio"] == fund]
            if (len(fund_data.index) > 0):
				chartpack.plot_gp_TE_chart(fund_data,
											   fund_name=fund,
											   output_file_name="gp_TE_chart_{0}_months".format(length_in_months),
											   width=10,
											   height=7,
											   output_dir=_output_folder,
											   dpi=100)


        logging.info("<<<< {0} ends SUCCESSFULLY >>>>".format(cfg.APP_NAME))
        _exit_code = 0
    except:
        logging.exception("!!! Error detected in {0}".format(cfg.APP_NAME))
        logging.ERROR("<<<< {0} ends UNSUCCESSFULLY >>>>".format(cfg.APP_NAME))
        _exit_code = 9999
    finally:
        _result = dirs.check_log_file_result(_log_file_name)
        _title = "{0} process - {1}".format(cfg.APP_NAME, _result)
        _email_body = dirs.htmlfy_log_file(_log_file_name)
        emails.send_SMTP_mail(cfg.SENDER, cfg.RECEIVERS, _title, _email_body, cfg.CC, [_log_file_name])
        sys.exit(_exit_code)
