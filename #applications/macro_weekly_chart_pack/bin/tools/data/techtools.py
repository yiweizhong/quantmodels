from __future__ import division
import pandas as pd
import numpy as np
import logging
import datetime as dt
import collections
from func import each as f_each
from pandas.tseries.offsets import *
from datatools import *


def calc_MACD_signal():
    pass

def calc_mean_reversion_signal():

    # This is slightly different than MaxMin2.0 FI sheet as the formulas have lots of shot circuited conditions
    # 1 is down signal -1 is up signal
    # 1) n day slope of the level
    # 2) exp zscore of 1)
    # 3) mathc 2) with
    #    2) > 1.5 => -1
    #    2) < -1.5 => 1
    #    previous 3) = 1 and  0 < 2) < 1.5 and 2) < 20Bday MVA of 2)  => -1
    #    previous 3) = -1 and  -1.5 < 2) < 0 and 2) < 20Bday MVA of 2)  => 1
    #    previous 3) = -1 and  0 < 2) < 1.5  => -1
    #    previous 3) = 1 and  -1.5 < 2) < 0 => 1


    pass


def calc_mva_day_count_based_MR_signal(level):

    """
    calculate mva day count based mean reversion signal

    1) n day slope of the level
    2) 200 days of rolling zscore of level
    3) cumulative sum of days above or below 200 mean
    4) exanding zscore of 3)
    5)
       1) < 0 and 4) > 1 => -2
       1) > 0 and 4) < -1 => 2
       prev 5) < 0 and 1) < 0 and 4) > -0.9 => -1
       prev 5) > 0 and 1) > 0 and 4) < 0.9 => 1
    Args:
        level (series): A pandas time series
    Returns:
              (series): A series of signal
    """

    validate_timeseries_data(level,is_df=False)

    slope = level - level.shift










    pass

def calc_mva_signal():
    pass


def calc_rsi():
    pass



def calc_range_trade_signal():
    pass

