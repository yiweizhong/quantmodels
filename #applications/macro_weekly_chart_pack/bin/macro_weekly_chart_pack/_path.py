import os, sys, inspect
_path = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0]))
_parent_path = os.path.abspath(os.path.join(_path, os.pardir))
_tools_path = os.path.abspath(os.path.join(_parent_path, "tools"))
if _tools_path not in sys.path: sys.path.insert(0, _tools_path)
print "sys.path:"
for p in sys.path: print p