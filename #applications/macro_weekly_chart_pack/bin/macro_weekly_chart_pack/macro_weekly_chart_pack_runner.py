from __future__ import division
import _path
import os, sys, inspect
import logging
import common.dirs as dirs
import common.emails as emails
#import chart.PlotWrapper as plotwrapper
import data.database as db
import data.datatools as tools
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mpd
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
import numpy as np
import pandas as pd
from datetime import datetime
from datetime import date
from dateutil.relativedelta import relativedelta
import macro_weekly_chart_pack as mwcp
import macro_weekly_chart_pack_config as cfg

__author__ = "Yiwei Zhong"
__copyright__ = "AMP Capital Fixed Income Macro Market"
__version__ = "0.0.1"
__maintainer__ = "Yiwei Zhong"
__email__ = "Yiwei.Zhong@ampcapital.com"
__status__ = "Testing"


#------------------------------------------------------------------------------------------------
def get_latest_pci_and_component_data(item):
#------------------------------------------------------------------------------------------------
    table = ""

    if item == "ToT vs TWI":
        table = "z_TOTVSTWI"
    if item == "Budget balance as % of GDP":
        table = "z_BUDGET_VS_GDP"
    if item == "Policy rate":
        table = "z_POLICY_RATES"
    if item == "Reserve requirement":
        table = "z_CHINA_RESERVE_RATIO"
    if item == "Balance sheet":
        table = "z_RESERVE_BANK_BALANCE_SHEETS"
    if item == "PCI":
        table = "AMP_PCI_SIMPLE"
    if item == "Govt 10yr":
        table = "z_10YEAR_YIELD"

    view = 'Fundamental' if table == 'AMP_PCI_SIMPLE' else 'Calc'
    query = r"""SELECT [Date] [DATE], [AUD],[CAD],[EUR],[JPY],[NOK],[NZD],[SEK],[CHF],[GBP],
                [USD],[CNY],[HKD],[INR],[IDR],[MYR],[PHP],[SGD],[KRW],[TWD],[THB]
                FROM [{0}].[{1}] WHERE [DATE] = (SELECT MAX(DATE) FROM [{0}].[{1}])""".format(view, table)
    return query


#------------------------------------------------------------------------------------------------
def get_epi_data(country):
#------------------------------------------------------------------------------------------------
    query = r"""SELECT [Date] [DATE]
          ,[All_{0}_All_All] [Headline]
          ,[All_{0}_Business_All] [Business]
          ,[All_{0}_Consumer_All] [Consumer]
          ,[All_{0}_Employment_All] [Employment]
          ,[All_{0}_Growth_All] [Growth]
          ,[All_{0}_Inflation_All] [Inflation]
      FROM [Fixed_Income].[epi].[Epi_Historical_Q_Pivoted]
      WHERE DATEDIFF(Month, [Date],GETDATE() ) <= 50
      ORDER BY [DATE] ASC""".format(country)
    return query

#------------------------------------------------------------------------------------------------
def get_epi_monthly_data(country):
#------------------------------------------------------------------------------------------------
    query = r"""SELECT [Date] [DATE]
          ,[All_{0}_All_All] [Headline]
          ,[All_{0}_Business_All] [Business]
          ,[All_{0}_Consumer_All] [Consumer]
          ,[All_{0}_Employment_All] [Employment]
          ,[All_{0}_Growth_All] [Growth]
          ,[All_{0}_Inflation_All] [Inflation]
      FROM [Fixed_Income].[epi].[Epi_Historical_M_Pivoted]
      WHERE DATEDIFF(Month, [Date],GETDATE() ) <= 50
      ORDER BY [DATE] ASC""".format(country)
    return query


#------------------------------------------------------------------------------------------------
def get_epi_data_global_M():
#------------------------------------------------------------------------------------------------
    query = r"""SELECT ta.[Date] [DATE],
       tb.[Global_Gdp_weighted] [Headline],
       [Business], [Consumer], [Inflation], [Growth], [Employment]
        FROM [epi].[EPI_GLobal_Components_GDP_WEIGHTED_Monthly_W_DP] ta
        left outer join [Fixed_Income].[epi].[Headline_EPI_By_Region_M] tb on ta.Date = tb.Date
        WHERE DATEDIFF(Month, ta.[Date],GETDATE() ) <= 50
        ORDER BY ta.[Date] asc"""
    return query


#------------------------------------------------------------------------------------------------
def get_epi_data_global():
#------------------------------------------------------------------------------------------------
    query = r"""SELECT ta.[Date] [DATE],
       tb.[Global_Gdp_weighted] [Headline],
       [Business], [Consumer], [Inflation], [Growth], [Employment]
        FROM [epi].[EPI_GLobal_Components_GDP_WEIGHTED_Quarterly_W_DP] ta
        left outer join [Fixed_Income].[epi].[Headline_EPI_By_Region_Q] tb on ta.Date = tb.Date
        WHERE DATEDIFF(Month, ta.[Date],GETDATE() ) <= 50
        ORDER BY ta.[Date] asc"""
    return query



#------------------------------------------------------------------------------------------------
def get_fundamental_data(country):
#------------------------------------------------------------------------------------------------
    query = r"""select epiIG.[Date] [DATE]
                   ,epiIG.[{0}] [Growth_ex_Inflation_EPI]
                   ,esi.[{0}] [ESI]
                   ,mp.[{0}] [Macro_Pulse]
                   ,pci.[{0}] [PCI_Lookback]
                   ,fci.[{0}] [FCI]
                   ,epi.[{0}] [Headline_EPI]
                   ,dbo.fn_rowAvg6(epiIG.[{0}],esi.[{0}],mp.[{0}],pci.[{0}],fci.[{0}],epi.[{0}]) [Fundamental]
    from [EPI].[v_Epi_All_XX_Inflation_Growth_All_Q] epiIG
    left outer join [fundamental].[v_ESI_COMBINED] esi on epiIG.[Date] = esi.[Date]
    left outer join [fundamental].[v_z_MACRO_PULSE] mp on epiIG.[Date] = mp.[Date]
    left outer join [fundamental].[v_AMP_PCI_6MTH_MAX_ABS_FROM_6MTH_WINDOW_6MTH_LOOKBACK] pci on epiIG.[Date] = pci.[Date]
    left outer join [fundamental].[v_FCI_3mth_lookback] fci on epiIG.[Date] = fci.[Date]
    left outer join [epi].[v_Epi_All_XX_All_All_Q] epi on  epiIG.[Date] = epi.[Date]
    WHERE DATEDIFF(MONTH,epiIG.[Date],GETDATE()) <= 50
    ORDER BY epiIG.[Date] ASC""".format(country)

    return query


#------------------------------------------------------------------------------------------------
def get_fundamental_data_global():
#------------------------------------------------------------------------------------------------
    query = r"""SELECT [Date] [DATE],
          [GrowthExInflationDp] [Growth_ex_Inflation_EPI],
          [EsiDp] [ESI],
          [MacroPulseDp] [Macro_Pulse],
          [PciDp] [PCI_Lookback],
          [FciDp] [FCI],
          [EpiDp] [Headline_EPI],
          [FundamentalDp] [Fundamental]
          FROM [Fixed_Income].[fundamental].[v_Global_Fundamental_And_components]
          order by [Date] asc"""

    return query




#------------------------------------------------------------------------------------------------
def get_epi_snail_trail_data(view, global_name):
#------------------------------------------------------------------------------------------------
    query = r"""SELECT
       [Date] [DATE]
      ,[AU] ,[AUM] ,[CA] ,[CAM]  ,[CH] ,[CHM] ,[EU],[EUM]
      ,[ID] ,[IDM] ,[IN] ,[INM]  ,[JN] ,[JNM] ,[MA],[MAM]
      ,[NZ] ,[NZM] ,[PH] ,[PHM]  ,[SI] ,[SIM] ,[SK],[SKM]
      ,[TA] ,[TAM] ,[TH] ,[THM]  ,[UK]  ,[UKM],[US],[USM]
      ,[{1}_GDP] [GLOBAL],[{1}_GDPM] [GLOBALM]
    FROM [epi].[{0}] ORDER BY [DATE] ASC """.format(view,  global_name)
    return query


#------------------------------------------------------------------------------------------------
def get_epi_heatmap_data(view):
#------------------------------------------------------------------------------------------------
    query = r"""
    SELECT [Date] [DATE],[AUD],[CAD],[EUR],[JPY],[NZD],[USD],[GBP],[NOK],[SEK],[CHF],[CNY],[HKD],[INR],[MYR]
      ,[SGD],[KRD] [KRW],[TWD],[THB],[G10],[Asia],[LATAM],[EEur], [Global_Gdp_weighted] [GLOBAL]
    FROM [Fixed_Income].[epi].[{0}_EPI_By_20_REGIONS_Countries_Q]
    WHERE DATEDIFF(MONTH,[DATE], GETDATE()) <= 12
    """.format(view)
    return query

#------------------------------------------------------------------------------------------------
def get_epi_cross_country_data(epi_type):
#------------------------------------------------------------------------------------------------
    query = r"""SELECT TOP 1 [Date] [DATE]
        ,[All_AU_{0}_All] [AUD],[All_CA_{0}_All] [CAD],[All_EU_{0}_All] [EUR],[All_JN_{0}_All] [JPY]
        ,[All_NO_{0}_All] [NOK],[All_NZ_{0}_All] [NZD],[All_SW_{0}_All] [SEK],[All_SZ_{0}_All] [CHF]
        ,[All_UK_{0}_All] [GBP],[All_US_{0}_All] [USD],[All_CH_{0}_All] [CNY],[All_HK_{0}_All] [HKD]
        ,[All_IN_{0}_All] [INR],[All_ID_{0}_All] [IDR],[All_MA_{0}_All] [MYR],[All_PH_{0}_All] [PHP]
        ,[All_SI_{0}_All] [SGD],[All_SK_{0}_All] [KRW],[All_TA_{0}_All] [TWD],[All_TH_{0}_All] [THB]
    FROM [Fixed_Income].[epi].[Epi_Historical_Q_Pivoted]
    ORDER BY [DATE] DESC """.format(epi_type)
    return query


#------------------------------------------------------------------------------------------------
def get_epi_by_region_data(epi_type):
#------------------------------------------------------------------------------------------------
    query = r"""SELECT [Date] [DATE],[Asia],[EEur] [EEurope],[G10],[Latam],[Global],[Global_Gdp_weighted]
            FROM [Fixed_Income].[epi].[{0}_EPI_By_Region_Q] order by [Date] asc""".format(epi_type)
    return query


#------------------------------------------------------------------------------------------------
def get_epi_by_G10_Countries_data(epi_type):
#------------------------------------------------------------------------------------------------
    query = r"""SELECT [Date] [DATE],[AUD],[CAD],[EUR],[JPY],[NZD],[USD],[GBP]
                FROM [Fixed_Income].[epi].[{0}_EPI_By_G10_Countries_Q] order by [Date] asc""".format(epi_type)
    return query

#------------------------------------------------------------------------------------------------
def get_epi_by_G10_and_Regions_Countries_data(epi_type):
#------------------------------------------------------------------------------------------------
    query = r"""SELECT ta.[Date] [DATE],[AUD],[CAD],[EUR],[JPY],[NZD],[USD],[GBP]
                ,[Asia],[EEur] [EEurope],[G10],[Latam] [LATAM],[Global],[Global_Gdp_weighted] [GLOBAL_GDP]
                FROM [Fixed_Income].[epi].[{0}_EPI_By_Region_Q] ta
                inner join [Fixed_Income].[epi].[{0}_EPI_By_G10_Countries_Q] tb on ta.[Date] = tb.[Date]
                order by [Date] asc""".format(epi_type)
    return query

#------------------------------------------------------------------------------------------------
def get_esi_data(length):
#------------------------------------------------------------------------------------------------
    query = r"""SELECT[Date] [DATE],[AUD],[CAD],[EUR],[JPY],[NZD],[SEK],[CHF],[GBP],[USD],[CNY],[HKD],[INR],[IDR],[MYR]
      ,[PHP],[SGD],[KRW],[TWD],[THB]
      ,[DM],[ASIA],[GLOBAL]
        FROM [Fixed_Income].[fundamental].[ESI_COMBINED]
        WHERE DATEDIFF(Month,[Date],GETDATE()) <= {0}""".format(length)
    return query


#------------------------------------------------------------------------------------------------
def get_fci_data(length):
#------------------------------------------------------------------------------------------------
    query = r"""SELECT[Date] [DATE],[AUD],[CAD],[EUR],[JPY],[NZD],[SEK],[CHF],[GBP],[USD],[CNY],[HKD],[INR],[IDR],[MYR]
      ,[PHP],[SGD],[KRW],[TWD],[THB]
              , [GLOBAL_GDPW] [GLOBAL]
      FROM [calc].[z_GS_FCI_INTERPOLATED]
      WHERE DATEDIFF(Month,[Date],GETDATE()) <= {0}""".format(length)
    return query


#------------------------------------------------------------------------------------------------
def get_pci_data(length):
#------------------------------------------------------------------------------------------------
    query = r"""SELECT[Date] [DATE],[AUD],[CAD],[EUR],[JPY],[NZD],[SEK],[CHF],[GBP],[USD],[CNY],[HKD],[INR],[IDR],[MYR]
      ,[PHP],[SGD],[KRW],[TWD],[THB]
      ,[GLOBAL]
    FROM [fundamental].[AMP_PCI_SIMPLE]
    WHERE DATEDIFF(Month,[Date],GETDATE()) <= {0}""".format(length)
    return query

#------------------------------------------------------------------------------------------------
def get_macro_pulse_by_region_data():
#------------------------------------------------------------------------------------------------
    query = r"""SELECT [Date] [DATE],[ASIA],[BRIC],[EUR],[EM],[DM],[GLOBAL]
        FROM [fundamental].[z_MACRO_PULSE]
        Where DATEDIFF(MONTH,[Date],GETDATE()) <= 24
        order by [Date] asc
        """
    return query

#------------------------------------------------------------------------------------------------
def get_macro_pulse_by_g10_data():
#------------------------------------------------------------------------------------------------
    query = r"""SELECT [Date] [DATE],[AUD],[CAD],[JPY],[NZD],[EUR],[GBP],[USD]
        FROM [fundamental].[z_MACRO_PULSE]
        Where DATEDIFF(MONTH,[Date],GETDATE()) <= 24
        order by [Date] asc
        """
    return query

#------------------------------------------------------------------------------------------------
def get_macro_pulse_by_ASIA_data():
#------------------------------------------------------------------------------------------------
    query = r"""SELECT [Date] [DATE],[CNY],[HKD],[INR],[IDR],[MYR],[PHP],[SGD],[KRW],[TWD],[THB]
        FROM [fundamental].[z_MACRO_PULSE]
        Where DATEDIFF(MONTH,[Date],GETDATE()) <= 24
        order by [Date] asc
        """
    return query



#------------------------------------------------------------------------------------------------
def get_regime_based_blended_credit_spread_data():
#------------------------------------------------------------------------------------------------
    query = r"""select bl.[Date] [DATE], 
                90 [Base],
                10 [RockBottom],
                100 [MidLateCycle],
                100 [Transition],
                100 [ReflationRecession],
                bl.BlendedCreditSpread
                from [CreditAnalysis].[V_Blended_Credit_Spread] bl
                WHERE [Date] > '2000-01-01'
                order by bl.[date] asc
        """
    return query


#------------------------------------------------------------------------------------------------
def get_country_credit_spread_data():
#------------------------------------------------------------------------------------------------

    query = r"""SELECT [Date] [DATE], [AU], [EU], [US] 
    from  [CreditAnalysis].[V_Credit_Spread_Proxy_AU_US_EU] 
    WHERE  [Date] > '2000-01-01' order by [Date] asc
    """
    return query



def setup():
    """Log settings"""
    _log_folder = dirs.get_creat_sub_folder(folder_name="Log", parent_dir=cfg.APP_ROOT)
    _debug_folder = dirs.get_creat_sub_folder(folder_name="Debug", parent_dir=cfg.APP_ROOT)
    _output_folder = dirs.get_creat_sub_folder(folder_name="Output", parent_dir=cfg.APP_ROOT)
    _archive_folder = dirs.get_creat_sub_folder(folder_name="Archive", parent_dir=cfg.APP_ROOT)

    _archive_log_folder = dirs.get_creat_sub_folder(folder_name="Log", parent_dir=_archive_folder)
    _archive_debug_folder = dirs.get_creat_sub_folder(folder_name="Debug", parent_dir=_archive_folder)
    _archive_output_folder = dirs.get_creat_sub_folder(folder_name="Output", parent_dir=_archive_folder)

    _log_file_name = dirs.get_log_file_name(logfile_name_prefix=cfg.APP_NAME,log_folder=_log_folder)

    return _log_folder, _debug_folder, _output_folder, _archive_folder, _log_file_name, _archive_log_folder, _archive_debug_folder, _archive_output_folder


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-- Main
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':

    _log_folder, _debug_folder, _output_folder, _archive_folder, _log_file_name, _archive_log_folder, _archive_debug_folder, _archive_output_folder = setup()
    _exit_code = 9999

    try:


        logging.basicConfig(filename= _log_file_name, format=cfg.LOG_FORMAT, level=logging.DEBUG)
        logging.info("<<<< {0} starts >>>>".format(cfg.APP_NAME))

        #dirs.archive_files(_debug_folder, _archive_debug_folder)
        #dirs.archive_files(_output_folder, _archive_output_folder)


        _db = db.Database(cfg.FI_PROD_DB_CONN_STRING,_debug_folder)
        chartpack = mwcp.macro_weekly_chart_pack(_debug_folder, debug=False)
        epi_country_list = ["AU","CA","CH", "EU", "ID", "IN", "JN", "MA", "NZ", "PH", "SI", "SK", "TA", "TH", "UK", "US"]
        #pci_fci_country_list = ["AUD","CAD","EUR", "JPY","NZD", "SEK", "CHF", "GBP", "USD", "CNY", "HKD", "INR", "IDR", "MYR", "PHP", "SGD", "KRW", "TWD", "THB", "GLOBAL"]
        pci_fci_country_list = ["GLOBAL"]


        #----------------------------
        # generate country credit spread timeseries charts
        #----------------------------

        country_credit_spread_data = _db.load_single_table_from_db(get_country_credit_spread_data(), "Country_credit_spread_DATA")

        chartpack.plot_country_credit_spread_chart(country_credit_spread_data,output_dir=_output_folder, width=12, height=6, dpi = 100)        


        #----------------------------
        # generate regime base blended credit spread timeseries charts
        #----------------------------

        regimes_blended_credit_spread_data = _db.load_single_table_from_db(get_regime_based_blended_credit_spread_data(), "Regimes_blended_credit_spread_DATA")

        chartpack.plot_regime_blended_credit_spread_chart(regimes_blended_credit_spread_data,output_dir=_output_folder, width=12, height=6, dpi = 100)



        #----------------------------
        # generate Macro PCI and FCI timeseries charts
        #----------------------------

        pci_fci_data = {"FCI" : _db.load_single_table_from_db(get_fci_data("144"), "FCI").fillna(0),
                        "PCI" : _db.load_single_table_from_db(get_pci_data("144"), "PCI").fillna(0)}

        for country in pci_fci_country_list:
            chartpack.plot_fci_pci_timeseries_line_chart(pci_fci_data, country_name= country, output_dir=_output_folder, width=12, height=6, dpi = 100)



        #----------------------------
        # generate Macro PCI and component charts
        #----------------------------

        pci_component_data = {table : _db.load_single_table_from_db(get_latest_pci_and_component_data(table), table).fillna(0) for table in ["ToT vs TWI", "Govt 10yr", "Budget balance as % of GDP", "Policy rate", "Reserve requirement", "Balance sheet", "PCI"] }
        chartpack.plot_pci_component_cross_countries_chart(pci_component_data, output_file_name = "Policy Condition Index Summary", output_dir=_output_folder,  width=12, height=6, dpi = 100)

        #----------------------------
        # generate Macro Pulse line charts
        #----------------------------

        mp_data = _db.load_single_table_from_db(get_macro_pulse_by_region_data(), "Macro_Pulse_DATA")
        chartpack.plot_Macro_Pulse_by_region_chart(mp_data, output_file_name = " Macro Pulse ", output_dir=_output_folder, width=12, height=6, dpi = 100)

        mp_data = _db.load_single_table_from_db(get_macro_pulse_by_g10_data(), "Macro_Pulse_DATA")
        chartpack.plot_Macro_Pulse_by_G10_chart(mp_data, output_file_name = " Macro Pulse ", output_dir=_output_folder, width=12, height=6, dpi = 100)

        mp_data = _db.load_single_table_from_db(get_macro_pulse_by_ASIA_data(), "Macro_Pulse_DATA")
        chartpack.plot_Macro_Pulse_by_ASIA_chart(mp_data, output_file_name = " Macro Pulse ", output_dir=_output_folder, width=12, height=6, dpi = 100)

        #----------------------------
        # generate esi line charts
        #----------------------------

        esi_data = _db.load_single_table_from_db(get_esi_data("24"), "ESI_DATA")
        chartpack.plot_ESI_line_chart(esi_data, output_file_name = " ESI ", output_dir=_output_folder, width=12, height=6, dpi = 100)
        esi_data = _db.load_single_table_from_db(get_esi_data("48"), "ESI_DATA")
        chartpack.plot_Global_ESI_line_chart(esi_data, output_file_name = "ESI by region", output_dir=_output_folder, width=12, height=6, dpi = 100)

        #esi_data = _db.load_single_table_from_db(get_esi_data("48"), "ESI_DATA")
        #chartpack.plot_Global_ESI_line_chart(esi_data, output_file_name = "ESI by region", output_dir=_output_folder, width=12, height=6, dpi = 100)

        #----------------------------
        # generate epi line charts
        #----------------------------


        for country in epi_country_list:
            epi_data = _db.load_single_table_from_db(get_epi_data(country), country)
            chartpack.plot_epi_timeseries_line_chart(epi_data, country_name= country, output_dir=_output_folder, width=12, height=6, dpi = 100)


        for country in epi_country_list:
            epi_data = _db.load_single_table_from_db(get_epi_monthly_data(country), country)
            chartpack.plot_epi_timeseries_line_chart(epi_data, country_name= country + " Monthly", output_dir=_output_folder, width=12, height=6, dpi = 100)



        global_epi_data = _db.load_single_table_from_db(get_epi_data_global(), "GLOBAL")
        chartpack.plot_epi_timeseries_line_chart(global_epi_data, country_name="GLOBAL", output_dir=_output_folder, width=12, height=6, dpi = 100)
		
	global_epi_data_m = _db.load_single_table_from_db(get_epi_data_global_M(), "GLOBAL")
	chartpack.plot_epi_timeseries_line_chart(global_epi_data_m, country_name="GLOBAL Monthly", output_dir=_output_folder, width=12, height=6, dpi = 100)

        #----------------------------
        # generate plot_EPI_region_and_countries_history_column_chart
        #----------------------------

        for epi_type in ["Headline","Business", "Inflation", "Consumer", "Growth", "Employment"]:
            epi_data = _db.load_single_table_from_db(get_epi_by_G10_and_Regions_Countries_data(epi_type), epi_type)
            chartpack.plot_EPI_region_and_countries_history_column_chart(epi_data, output_file_name = epi_type  + " EPI cross countries and regions level history", output_dir=_output_folder, width=12, height=6, dpi = 100)

        # ----------------------------
        # generate EPI component cross country charts
        # ----------------------------

        epi_component_cross_country_data = {epi_type : _db.load_single_table_from_db(get_epi_cross_country_data(epi_type), epi_type).fillna(0) for epi_type in ["Business", "Consumer", "Inflation", "Growth", "Employment" ] }
        chartpack.plot_epi_component_cross_countries_chart(epi_component_cross_country_data, output_file_name = "EPI components cross countries", output_dir=_output_folder,  width=12, height=6, dpi = 100)

        #----------------------------
        # generate EPI by region charts
        #----------------------------

        for epi_type in ["Headline","Business","Inflation","Consumer","Employment","Growth"]:
            epi_by_region_data = _db.load_single_table_from_db(get_epi_by_region_data(epi_type), epi_type)
            chartpack.plot_epi_by_region_timeseries_line_chart(epi_by_region_data, epi_type = epi_type, output_dir=_output_folder, width=12, height=6, dpi = 100)


        #----------------------------
        # generate EPI by G10 countries charts
        #----------------------------

        for epi_type in ["Headline","Business","Inflation","Consumer","Employment","Growth"]:
            epi_by_region_data = _db.load_single_table_from_db(get_epi_by_G10_Countries_data(epi_type), epi_type)
            chartpack.plot_epi_by_G10_countries_timeseries_line_chart(epi_by_region_data, epi_type = epi_type, output_dir=_output_folder, width=12, height=6, dpi = 100)


        #----------------------------
        # generate Fundamental charts
        #----------------------------



        for country in epi_country_list:
            fundamental_data = _db.load_single_table_from_db(get_fundamental_data(country), country)
            chartpack.plot_fundamental_area_chart(fundamental_data, country_name= country, width=12, height=6, output_dir=_output_folder, dpi = 100)

        global_fundamental_data = _db.load_single_table_from_db(get_fundamental_data_global(), "GLOBAL")
        chartpack.plot_fundamental_area_chart(global_fundamental_data, country_name="GLOBAL", output_dir=_output_folder, width=12, height=6, dpi = 100)
        chartpack.plot_fundamental_movement_chart(global_fundamental_data, output_file_name = "Global fundamental changes", output_dir=_output_folder, width=12, height=6, dpi = 100)



        #----------------------------
        # generate EPI Quarand Snail charts
        #----------------------------


        h_epi_snail = _db.load_single_table_from_db(get_epi_snail_trail_data('v_Epi_Momentum', 'WORLD'), 'headline')
        chartpack.plot_epi_quardrant_snail_chart(h_epi_snail, epi_country_list + ['GLOBAL'], output_file_name = "Headline EPI", output_dir= _output_folder,  width=14, height=6, dpi = 100)

        ### for EPI_type in ['Inflation', 'Growth', 'Business','Employment','Consumer']:
        ###     epi_snail = _db.load_single_table_from_db(get_epi_snail_trail_data('v_Epi_Momentum_24M_'+ EPI_type, 'GLOBAL'), EPI_type)
        ###     chartpack.plot_epi_quardrant_snail_chart(epi_snail, epi_country_list + ['GLOBAL'], output_file_name = EPI_type + ' EPI', output_dir= _output_folder,  width=12, height=6, dpi = 100)


        #----------------------------
        # generate EPI Heatmap charts
        #----------------------------
        for EPI_type in ['Headline', 'Inflation', 'Growth', 'Business','Employment','Consumer']:
            heatmap_data = _db.load_single_table_from_db(get_epi_heatmap_data(EPI_type), EPI_type)
            chartpack.plot_epi_heatmap(heatmap_data, chartpack.resample_df_back_monthly_from_latest, output_file_name = EPI_type + ' EPI', width=12, height=6, output_dir=_output_folder, dpi = 100 )



        logging.info("<<<< {0} ends SUCCESSFULLY >>>>".format(cfg.APP_NAME))
        _exit_code = 0
    except:
        logging.exception("!!! Error detected in {0}".format(cfg.APP_NAME))
        logging.info("<<<< {0} ends SUCCESSFULLY >>>>".format(cfg.APP_NAME))
        _exit_code = -1
    finally:
        _db.update_job_status(cfg.APP_ID, cfg.APP_NAME, datetime.now().strftime(r"%Y-%m-%d"),_exit_code,_log_file_name)
        _result = dirs.check_log_file_result(_log_file_name)
        _title = "{0} process - {1}".format(cfg.APP_NAME, _result)
        _email_body = dirs.htmlfy_log_file(_log_file_name)
        emails.send_SMTP_mail(cfg.SENDER, cfg.RECEIVERS, _title, _email_body, cfg.CC, [_log_file_name])
        sys.exit(_exit_code)
