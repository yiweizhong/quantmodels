APP_ROOT = r"\\capricorn\ausfi\Macro_Dev\Data\Macro_weekly_chart_pack"
APP_NAME = r"Macro_weekly_chart_pack"
APP_ID = 'RPT_001'
DATE = r"DATE"
SENDER = "YIWEI.ZHONG@ampcapital.com"
RECEIVERS = ["YIWEI.ZHONG@ampcapital.com"]
#CC = []
CC = ["YIWEI.ZHONG@ampcapital.com", "MacroStatusNotification@gmail.com", "bhavjot.deol@ampcapital.com"]
DTYPE_DATEIME64 = r"datetime64[ns]"
ODBC_STYLE_CSTR_PREFIX = r"mssql+pyodbc:///?odbc_connect=%s"
LOG_FORMAT = r"%(asctime)s - %(name)s - %(levelname)s - %(message)s"
TIMESTAMP_FMT = r"%Y-%m-%d %H:%M:%S.%f"

### DB
FI_DATA_PROD_DB_CONN_STRING = r"DRIVER={SQL Server Native Client 11.0};SERVER=frontofficesql\FO1;DATABASE=Fixed_income_Data;Trusted_Connection=yes;Integrated Security=True; MultipleActiveResultSets=True; Connection Timeout=800;"
FI_PROD_DB_CONN_STRING = r"DRIVER={SQL Server Native Client 11.0};SERVER=frontofficesql\FO1;DATABASE=Fixed_income;Trusted_Connection=yes;Integrated Security=True; MultipleActiveResultSets=True; Connection Timeout=800;"