
import numpy as np
import matplotlib.pyplot as plt
from cvxopt import matrix
import cvxopt as opt
from cvxopt import blas, solvers
from cvxopt import printing
printing.options['dformat'] = '%.8f'
printing.options['width'] = -1

import pandas as pd
import os, sys, inspect
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations
import pylab


_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper

pd.set_option('display.expand_frame_repr', False)




def plot_stack_area(x, ys, ax, reset_color_cycle = False, series_names = None, legend_pos=None, legend_ncol=1, legend_font_size= 12, **kwargs):

        min_x = x.min()
        max_x = x.max()

        sp = ax.stackplot(x, ys,linewidth=0.0, baseline ='zero', **kwargs)

        ax.set_xlim([min_x, max_x])

        ax.xaxis.grid(True)
        ax.yaxis.grid(True)

        if reset_color_cycle:
            #ax.set_color_cycle(None)
            ax.set_prop_cycle(None)

        if series_names is not None:
            #ax.set_color_cycle(None)
            legend_proxy = [mpl.patches.Rectangle((0,0), 0,0, facecolor=pol.get_facecolor()[0]) for pol in sp]
            ax.legend(legend_proxy, series_names, loc=legend_pos, fancybox=True, framealpha=0,  ncol=legend_ncol, prop={'size':legend_font_size})




def plot_stacked_bar(ax, series, left =None, width=0, color=None, edgecolor="none", bottom=None, **kwargs):
    width = width
    left = np.arange(len(series)) + width/2.0 if left is None else left    
    s = ax.bar(left, series, width, color= color,edgecolor=edgecolor, bottom=bottom, **kwargs)
    return s

def plot_heatmap(tbl, ax):
    #mask = np.triu(np.ones_like(cov, dtype=np.bool))

    
    # Generate a custom diverging colormap
    cmap = sns.diverging_palette(10, 240, as_cmap=True)
    
    # Draw the heatmap with the mask and correct aspect ratio
    #sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
    _ax = sns.heatmap(tbl, cmap=cmap, center=0,
                square=True, annot=True, linewidths=.5, cbar_kws={"shrink": .5},
                annot_kws={"size": 9}, ax=ax)
    _ax.set_yticklabels(ax.get_yticklabels(), rotation=90, horizontalalignment='right')
    
    return  _ax


def get_flattened_cov(df):
    covmatx = df.cov()
    nodes = set([comb for comb in combinations(covmatx.columns.tolist() + covmatx.columns.tolist(), 2)])
    result = pd.Series({col + '_' + row : covmatx[col][row]  for (col, row) in nodes})
    return result

def get_mean(df):
    return df.mean()

    

def rolling_apply(df, window_size, func, min_window_size=None):
    if min_window_size is None:
        min_window_size = window_size
    result = {}
    for i in range(1, len(df)+1):
        sub_df = df.iloc[max(i - window_size, 0):i, :] #I edited here

        if len(sub_df) >= min_window_size:            
            idx = sub_df.index[-1]
            result[idx] = func(sub_df)
    return result

def partition_by_sign( s, pos=True):
    s = s.fillna(0)
    if pos:
        return s.apply(lambda v: v if v >= 0 else 0)
    else:
        return s.apply(lambda v: v if v <= 0 else 0)
    
    
def get_weighted_return(return_df, weight_mtx, name=None):
    wr = matrix(return_df.values) * weight_mtx
    return pd.Series(data=wr, index=return_df.index, name=name)
    
    
    
    
    
    

#DM Sov	8.7
#EM Sov	6.2
#US IG	8.3
#US HY	4.3
#AU IG	3.7
risk_scaler = pd.Series({'DM_Sov':1, 'EM_Sov':0.712, 'US_IG':0.954, 'US_HY':0.494, 'AU_IG':0.425})


filedate = '2020-04-28' 

data = pd.read_csv('C:/Temp/test/market_watch/staging/' + filedate + '_MVO_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill().dropna(axis=0)
data = data.drop(['AU_IG2'], axis=1)
#data_risk_scaled = data/risk_scaler

data_rtns_d = (data/data.shift(1) - 1).dropna()
data_rtns_d = data_rtns_d / risk_scaler ############## adhoc scale the risk ############
assets = data_rtns_d.columns.tolist()




######################################################################################## 
########################################################################################
# output data objects
########################################################################################
########################################################################################




#rolling vol
data_rtns_d_1y_rolling_covs = pd.DataFrame(rolling_apply(data_rtns_d, 250, get_flattened_cov)).transpose()
data_rtns_d_1y_rolling_vol = data_rtns_d_1y_rolling_covs[[ a + '_' + b for a,b in set([comb for comb in combinations(assets + assets, 2)]) if a == b]]
data_rtns_d_1y_rolling_vol = data_rtns_d_1y_rolling_vol.applymap(np.sqrt)
                    



#overall vol
#data_rtns_d.plot()
data_rtns_d_cov = data_rtns_d.cov()



# PCA look on overall history
data_rtns_d_pca = pca.PCA(data_rtns_d.cumsum(), rates_pca = True, momentum_size = None)
data_rtns_d_pca.plot_loadings(n=3, m=5)



#vol and return matrix object on overall history  
data_rtns_d_mean_mtx = matrix(data_rtns_d.mean())
data_rtns_d_var_mtx = matrix(data_rtns_d_cov.to_numpy())



#inequality



#g1 is numpy array. covexp wont flip the numpy array when creating matrix but will flip the same dimension list of list 

# weights > -0.3
g1 = np.array([
        [-1.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, -1.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, -1.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, -1.0, 0.0],
        [0.0, 0.0, 0.0, 0.0, -1.0]
    ])
h1 = [ 0.3,  0.3,  0.3,  0.3,  0.3]

# max weights 
g2 = np.array([
        [1.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, 1.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 1.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, 1.0, 0.0],
        [0.0, 0.0, 0.0, 0.0, 1.0]
    ])
h2 = [ 0.3,  0.3,  0.3,  0.3, 0.3]



gs = np.concatenate((g1, g2),axis=0)
hs = h1 + h2

G = matrix(gs)

h = matrix(hs)

#equality

A = matrix(1.0, (1,5))
b = matrix(1.0)

#build portfolios
_lambdas = [ 10**(5.0*t/100-1.0) for t in range(100)]


weights = [ solvers.qp((_lambda)*data_rtns_d_var_mtx, -data_rtns_d_mean_mtx, G, h, A, b)['x'] for _lambda in _lambdas]
expected_annual_returns = [ blas.dot(data_rtns_d_mean_mtx,x) * 250 for x in weights]
expected_annual_risks = [ np.sqrt(blas.dot(x, data_rtns_d_var_mtx*x)) * np.sqrt(250) for x in weights]
expected_annual_sharpes = [ rt/rs for rt, rs in zip(expected_annual_returns, expected_annual_risks)] 

unconstrainted_backtest_d_rtns = [get_weighted_return(data_rtns_d, x, name=s) for x, s in zip(weights, expected_annual_sharpes)]















# portfolios = {{'weights':x, 'expected_return': blas.dot(data_rtns_d_scaled_mean_mtx,x), 'risk': np.sqrt(blas.dot(x, data_rtns_d_scaled_var_mtx*x))} for x in weights}



# portfolios = [ solvers.qp((_lambda)*data_rtns_d_scaled_var_mtx, -data_rtns_d_scaled_mean_mtx, G, h, A, b)['x'] for _lambda in _lambdas]
# #portfolios = [ solvers.qp((_lambda)*data_rtns_d_var_mtx, -data_rtns_d_mean_mtx, G, h, A, b)['x'] for _lambda in _lambdas]
# returns = [ blas.dot(data_rtns_d_scaled_mean_mtx,x) for x in portfolios ]
# risks = [ np.sqrt(blas.dot(x, data_rtns_d_scaled_var_mtx*x)) for x in portfolios ]

# returns = [ r * 250 for r in returns ]
# risks = [ r * np.sqrt(250) for r in risks ]



# Plot trade-off curve and optimal allocations.

fig = plt.figure(figsize=(20,20))
ax0 = fig.add_subplot(221)
ax00 = fig.add_subplot(222)
ax1 = fig.add_subplot(223)
ax2 = fig.add_subplot(224)


data_rtns_d.cumsum().plot(ax=ax0)

plot_heatmap(data_rtns_d_cov * np.sqrt(250), ax00)

ax1.plot(expected_annual_risks, expected_annual_returns)
ax1.set_xlabel('standard deviation')
ax1.set_ylabel('expected return')


c1 = pd.Series([ x[0] for x in weights ], name='DM_Sov')
c2 = pd.Series([ x[1] for x in weights ], name='EM_Sov')
c3 = pd.Series([ x[2] for x in weights ], name='US_IG')
c4 = pd.Series([ x[3] for x in weights ], name='US_HY')
c5 = pd.Series([ x[4] for x in weights ], name='AU_IG')

cc = plt.rcParams['axes.prop_cycle'].by_key()['color']

s1 = plot_stacked_bar(ax2, partition_by_sign(c1, pos=True), left=expected_annual_risks, width = 0.001, color=cc[0], edgecolor ="none",  align="center")
s2 = plot_stacked_bar(ax2, partition_by_sign(c2, pos=True), left=expected_annual_risks, width = 0.001, color=cc[1], edgecolor ="none",  align="center", bottom=partition_by_sign(c1, pos=True).values)
s3 = plot_stacked_bar(ax2, partition_by_sign(c3, pos=True), left=expected_annual_risks, width = 0.001, color=cc[2], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=True).values + partition_by_sign(c2, pos=True).values)
s4 = plot_stacked_bar(ax2, partition_by_sign(c4, pos=True), left=expected_annual_risks, width = 0.001, color=cc[3], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=True).values + partition_by_sign(c2, pos=True).values + partition_by_sign(c3, pos=True).values)
s5 = plot_stacked_bar(ax2, partition_by_sign(c5, pos=True), left=expected_annual_risks, width = 0.001, color=cc[4], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=True).values + partition_by_sign(c2, pos=True).values + partition_by_sign(c3, pos=True).values + partition_by_sign(c4, pos=True).values)

s1 = plot_stacked_bar(ax2, partition_by_sign(c1, pos=False), left=expected_annual_risks, width = 0.001, color=cc[0], edgecolor ="none",  align="center")
s2 = plot_stacked_bar(ax2, partition_by_sign(c2, pos=False), left=expected_annual_risks, width = 0.001, color=cc[1], edgecolor ="none",  align="center", bottom=partition_by_sign(c1, pos=False).values)
s3 = plot_stacked_bar(ax2, partition_by_sign(c3, pos=False), left=expected_annual_risks, width = 0.001, color=cc[2], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=False).values + partition_by_sign(c2, pos=False).values)
s4 = plot_stacked_bar(ax2, partition_by_sign(c4, pos=False), left=expected_annual_risks, width = 0.001, color=cc[3], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=False).values + partition_by_sign(c2, pos=False).values + partition_by_sign(c3, pos=False).values)
s5 = plot_stacked_bar(ax2, partition_by_sign(c5, pos=False), left=expected_annual_risks, width = 0.001, color=cc[4], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=False).values + partition_by_sign(c2, pos=False).values + partition_by_sign(c3, pos=False).values + partition_by_sign(c4, pos=False).values)


ax2.set_xlabel('standard deviation')
ax2.set_ylabel('allocation')
ax2.legend([s1, s2, s3, s4, s5], ['DM_Sov','EM_Sov','US_IG','US_HY','AU_IG'], fancybox=True, framealpha=0, loc=9, ncol=3, prop={'size':10}, bbox_to_anchor=(0.5, 1.12))






######################################################################################## 
########################################################################################
# rolling optimisation experiment
########################################################################################
########################################################################################

#
solvers.options['show_progress'] = False


#data = pd.read_csv(r'C:\Dev\Models\QuantModels\#applications\MVO\RatesCredits_20200511.csv')
data = pd.read_csv(r'C:\Dev\Models\QuantModels\#applications\MVO\RatesCreditsYTW_20200511.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill().dropna(axis=0)


data['US10'] = data['US10'] *100
data['AU10'] = data['AU10'] *100
data['EU10'] = data['EU10'] *100


data = data[[
    'US10','AU10','EU10',
    'All_Main_Sectors_ALL_ALL',
    'Corporates_All_All',
    'JPMorgan_Global_HY_Summary',
    'AUC0'
    ]]

data = data.rename(columns={
    'All_Main_Sectors_ALL_ALL':'US_IG',
    'Corporates_All_All':'EU_IG',
    'JPMorgan_Global_HY_Summary':'US_HY',
    'AUC0':'AU_IG'}
    )

#only needed for YTW not spreads
data['US_IG'] = data['US_IG'] *100
data['EU_IG'] = data['EU_IG'] *100
data['US_HY'] = data['US_HY'] *10000
data['AU_IG'] = data['AU_IG'] *100




data_cap_rtns_d = -1 *(data - data.shift(1)).dropna()
data_carry_rtns_d = (data/250).reindex(data_cap_rtns_d.index,axis='index')

data_rtns_d = data_cap_rtns_d + data_carry_rtns_d

assets = data_rtns_d.columns.tolist()


#rolling 1y covs and mean
data_rtns_d_1y_rolling_covs = rolling_apply(data_rtns_d, 250, lambda df: df.cov())
data_rtns_d_1y_rolling_mean = rolling_apply(data_rtns_d, 250, lambda df: df.mean())
eom_dates = data_rtns_d.resample('M').last().index



#re optimise for the month end

#g1 is numpy array. covexp wont flip the numpy array when creating matrix but will flip the same dimension list of list 

# # weights > -0.3
# g1 = np.array([
#         [-1.0, 0.0, 0.0, 0.0, 0.0],
#         [0.0, -1.0, 0.0, 0.0, 0.0],
#         [0.0, 0.0, -1.0, 0.0, 0.0],
#         [0.0, 0.0, 0.0, -1.0, 0.0],
#         [0.0, 0.0, 0.0, 0.0, -1.0]
#     ])
# h1 = [ 0.3,  0.3,  0.3,  0.3,  0.3]

# # max weights 
# g2 = np.array([
#         [1.0, 0.0, 0.0, 0.0, 0.0],
#         [0.0, 1.0, 0.0, 0.0, 0.0],
#         [0.0, 0.0, 1.0, 0.0, 0.0],
#         [0.0, 0.0, 0.0, 1.0, 0.0],
#         [0.0, 0.0, 0.0, 0.0, 1.0]
#     ])
# h2 = [ 0.3,  0.3,  0.3,  0.3, 0.3]


# gs = np.concatenate((g1, g2),axis=0)
# hs = h1 + h2

# G = matrix(gs)

# h = matrix(hs)

# #equality

# A = matrix(1.0, (1,5))
# b = matrix(1.0)

# #build portfolios
# _lambdas = [ 10**(5.0*t/1000-1.0) for t in range(1000)]




# weights > -0.3
g1 = np.array([
        [-1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0],
        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.0]
    ])
h1 = [ 0.3,  0.3,  0.3,  0.3,  0.3,  0.3,  0.3]

# max weights 
g2 = np.array([
        [1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]
    ])
h2 = [ 0.3,  0.3,  0.3,  0.3,  0.3,  0.3,  0.3]


gs = np.concatenate((g1, g2),axis=0)
hs = h1 + h2

G = matrix(gs)

h = matrix(hs)


#equality

A = matrix(1.0, (1,7))
b = matrix(1.0)



#build portfolios
_lambdas = [ 10**(5.0*t/1000-1.0) for t in range(1000)]




optimizations = {}


# cpu heavy optimizations

for dt in eom_dates:
    if dt in data_rtns_d_1y_rolling_mean:
        data_rtns_d_var_mtx = matrix(data_rtns_d_1y_rolling_covs[dt].to_numpy())
        data_rtns_d_mean_mtx = matrix(data_rtns_d_1y_rolling_mean[dt])
        
        weights = [ solvers.qp((_lambda)*data_rtns_d_var_mtx, -data_rtns_d_mean_mtx, G, h, A, b)['x'] for _lambda in _lambdas]
        expected_annual_returns = [ blas.dot(data_rtns_d_mean_mtx,x) * 250 for x in weights]
        expected_annual_risks = [ np.sqrt(blas.dot(x, data_rtns_d_var_mtx*x)) * np.sqrt(250) for x in weights]
        expected_annual_sharpes = [ rt/rs for rt, rs in zip(expected_annual_returns, expected_annual_risks)] 
        optimizations[dt] = {'weights':weights, 'expected_annual_returns':expected_annual_returns, 'expected_annual_risks':expected_annual_risks, 'expected_annual_sharpes':expected_annual_sharpes}
        



# find threshold portfolio

return_threshhold = 0.04        
selected_optimization = {}
for dt in optimizations.keys():
        weights = optimizations[dt]['weights']
        expected_annual_returns = optimizations[dt]['expected_annual_returns']
        expected_annual_risks = optimizations[dt]['expected_annual_risks']
        expected_annual_sharpes = optimizations[dt]['expected_annual_sharpes'] 
     
        selected_returns = [x for x in expected_annual_returns if x >= return_threshhold]
        selected_return = max(expected_annual_returns) if len(selected_returns) == 0 else min(selected_returns)
        seleted_index = expected_annual_returns.index(selected_return)
        
        #print(dt)
        #print(seleted_index)
        #print(len(expected_annual_returns))
        #print(selected_return)
        
        selected_optimization[dt] = {'weights':weights[seleted_index], 
                                     'expected_annual_returns':expected_annual_returns[seleted_index], 
                                     'expected_annual_risks':expected_annual_risks[seleted_index], 
                                     'expected_annual_sharpes':expected_annual_sharpes[seleted_index]}
        

# chart

threshhold_returns ={}
threshhold_risks ={}
threshhold = {}

US10_wt = {}
AU10_wt = {}
EU10_wt = {}
US_IG_wt = {}
EU_IG_wt = {}
US_HY_wt ={}
AU_IG_wt = {}



 

for dt in selected_optimization.keys():
    threshhold_returns[dt] = selected_optimization[dt]['expected_annual_returns']
    threshhold_risks[dt] = selected_optimization[dt]['expected_annual_risks']
    threshhold[dt] = return_threshhold 

    
    US10_wt[dt] = selected_optimization[dt]['weights'][0]
    AU10_wt[dt] = selected_optimization[dt]['weights'][1]
    EU10_wt[dt] = selected_optimization[dt]['weights'][2]
    US_IG_wt[dt] = selected_optimization[dt]['weights'][3]
    EU_IG_wt[dt] = selected_optimization[dt]['weights'][4]
    US_HY_wt[dt] = selected_optimization[dt]['weights'][5]
    AU_IG_wt[dt] = selected_optimization[dt]['weights'][6]
    
    
    
threshhold_risk_rewards = pd.DataFrame({'return':pd.Series(threshhold_returns, name='return'), 
                                        'risk': pd.Series(threshhold_risks, name='rik'), 
                                        'return_threshhold':pd.Series(threshhold, name='rik') })



US10_wt = pd.Series(US10_wt, name='US10')
AU10_wt = pd.Series(AU10_wt, name='AU10')
EU10_wt = pd.Series(EU10_wt, name='EU10')
US_IG_wt = pd.Series(US_IG_wt, name='US_IG')
EU_IG_wt = pd.Series(EU_IG_wt, name='EU_IG')
US_HY_wt = pd.Series(US_HY_wt, name='US_HY')
AU_IG_wt = pd.Series(AU_IG_wt, name='AU_IG')





portfolio_wt = pd.DataFrame([US10_wt, AU10_wt, EU10_wt,  US_IG_wt, EU_IG_wt, US_HY_wt,AU_IG_wt]).transpose().reindex(data_rtns_d.index, axis='index').ffill().dropna()

porfolio_d_rtn = (portfolio_wt * data_rtns_d).dropna() 

porfolio_backtest = porfolio_d_rtn.sum(axis=1)


porfolio_backtest_2001 = porfolio_backtest[(porfolio_backtest.index > '2000-12-31') & (porfolio_backtest.index <= '2001-12-31')].cumsum()
porfolio_backtest_2002 = porfolio_backtest[(porfolio_backtest.index > '2001-12-31') & (porfolio_backtest.index <= '2002-12-31')].cumsum()
porfolio_backtest_2003 = porfolio_backtest[(porfolio_backtest.index > '2002-12-31') & (porfolio_backtest.index <= '2003-12-31')].cumsum()
porfolio_backtest_2004 = porfolio_backtest[(porfolio_backtest.index > '2003-12-31') & (porfolio_backtest.index <= '2004-12-31')].cumsum()
porfolio_backtest_2005 = porfolio_backtest[(porfolio_backtest.index > '2004-12-31') & (porfolio_backtest.index <= '2005-12-31')].cumsum()
porfolio_backtest_2006 = porfolio_backtest[(porfolio_backtest.index > '2005-12-31') & (porfolio_backtest.index <= '2006-12-31')].cumsum()
porfolio_backtest_2007 = porfolio_backtest[(porfolio_backtest.index > '2006-12-31') & (porfolio_backtest.index <= '2007-12-31')].cumsum()
porfolio_backtest_2008 = porfolio_backtest[(porfolio_backtest.index > '2007-12-31') & (porfolio_backtest.index <= '2008-12-31')].cumsum()
porfolio_backtest_2009 = porfolio_backtest[(porfolio_backtest.index > '2008-12-31') & (porfolio_backtest.index <= '2009-12-31')].cumsum()
porfolio_backtest_2010 = porfolio_backtest[(porfolio_backtest.index > '2009-12-31') & (porfolio_backtest.index <= '2010-12-31')].cumsum()
porfolio_backtest_2011 = porfolio_backtest[(porfolio_backtest.index > '2010-12-31') & (porfolio_backtest.index <= '2011-12-31')].cumsum()
porfolio_backtest_2012 = porfolio_backtest[(porfolio_backtest.index > '2011-12-31') & (porfolio_backtest.index <= '2012-12-31')].cumsum()
porfolio_backtest_2013 = porfolio_backtest[(porfolio_backtest.index > '2012-12-31') & (porfolio_backtest.index <= '2013-12-31')].cumsum()
porfolio_backtest_2014 = porfolio_backtest[(porfolio_backtest.index > '2013-12-31') & (porfolio_backtest.index <= '2014-12-31')].cumsum()
porfolio_backtest_2015 = porfolio_backtest[(porfolio_backtest.index > '2014-12-31') & (porfolio_backtest.index <= '2015-12-31')].cumsum()
porfolio_backtest_2016 = porfolio_backtest[(porfolio_backtest.index > '2015-12-31') & (porfolio_backtest.index <= '2016-12-31')].cumsum()
porfolio_backtest_2017 = porfolio_backtest[(porfolio_backtest.index > '2016-12-31') & (porfolio_backtest.index <= '2017-12-31')].cumsum()
porfolio_backtest_2018 = porfolio_backtest[(porfolio_backtest.index > '2017-12-31') & (porfolio_backtest.index <= '2018-12-31')].cumsum()
porfolio_backtest_2019 = porfolio_backtest[(porfolio_backtest.index > '2018-12-31') & (porfolio_backtest.index <= '2019-12-31')].cumsum()
porfolio_backtest_2020 = porfolio_backtest[(porfolio_backtest.index > '2019-12-31') & (porfolio_backtest.index <= '2020-12-31')].cumsum()

porfolio_backtest_anchor = pd.Series({'2002-01-01':0,'2003-01-01':0, '2004-01-01':0, '2005-01-01':0, '2006-01-01':0, '2007-01-01':0,
                                      '2008-01-01':0, '2009-01-01':0, '2010-01-01':0, '2011-01-01':0, '2012-01-01':0,
                                      '2013-01-01':0, '2014-01-01':0, '2015-01-01':0, '2016-01-01':0, '2017-01-01':0,
                                      '2018-01-01':0, '2019-01-01':0, '2020-01-01':0})

porfolio_backtest_anchor.index = pd.to_datetime(porfolio_backtest_anchor.index)


porfolio_rolling_cumulative_performance = pd.concat([ 
    porfolio_backtest_2001,
    porfolio_backtest_2002,
    porfolio_backtest_2003,
    porfolio_backtest_2004,
    porfolio_backtest_2005,
    porfolio_backtest_2006,
    porfolio_backtest_2007,
    porfolio_backtest_2008,
    porfolio_backtest_2009,
    porfolio_backtest_2010,
    porfolio_backtest_2011,
    porfolio_backtest_2012,
    porfolio_backtest_2013,
    porfolio_backtest_2014,
    porfolio_backtest_2015,
    porfolio_backtest_2016,
    porfolio_backtest_2017,
    porfolio_backtest_2018,
    porfolio_backtest_2019,
    porfolio_backtest_2020])

    
#porfolio_rolling_cumulative_performance = porfolio_rolling_cumulative_performance.reindex(US10_wt.index, axis='index')


fig = plt.figure(figsize=(60,20))

ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212)
#ax3 = fig.add_subplot(313)


#threshhold_risk_rewards.plot(ax=ax1)



plot_stack_area(US10_wt.index,[partition_by_sign(US10_wt,True), 
                                 partition_by_sign(AU10_wt,True), 
                                 partition_by_sign(EU10_wt,True), 
                                 partition_by_sign(US_IG_wt,True), 
                                 partition_by_sign(EU_IG_wt,True),
                                 partition_by_sign(US_HY_wt,True),
                                 partition_by_sign(AU_IG_wt,True)
                                 ], 
                                 ax1, 
                reset_color_cycle= True, series_names=data_rtns_d.columns.tolist(), colors=plt.rcParams['axes.prop_cycle'].by_key()['color'][:7],
                            legend_font_size= 10, legend_pos=9, legend_ncol=7)


plot_stack_area(US10_wt.index,[partition_by_sign(US10_wt,False), 
                                 partition_by_sign(AU10_wt,False), 
                                 partition_by_sign(EU10_wt,False), 
                                 partition_by_sign(US_IG_wt,False), 
                                 partition_by_sign(EU_IG_wt,False),
                                 partition_by_sign(US_HY_wt,False),
                                 partition_by_sign(AU_IG_wt,False)
                                 ], 
                                ax1, 
                reset_color_cycle= True, series_names=data_rtns_d.columns.tolist(), colors=plt.rcParams['axes.prop_cycle'].by_key()['color'][:7],
                            legend_font_size= 10, legend_pos=9, legend_ncol=7)





plot_stack_area(porfolio_rolling_cumulative_performance.index,[partition_by_sign(porfolio_rolling_cumulative_performance.fillna(0),True)], 
                                 ax2, 
                reset_color_cycle= True, series_names=['Portfolio returns'], colors=plt.rcParams['axes.prop_cycle'].by_key()['color'][:1],
                            legend_font_size= 10, legend_pos=9, legend_ncol=1)


plot_stack_area(porfolio_rolling_cumulative_performance.index,[partition_by_sign(porfolio_rolling_cumulative_performance.fillna(0),False)], 
                                 ax2, 
                reset_color_cycle= True, series_names=['Portfolio returns'], colors=plt.rcParams['axes.prop_cycle'].by_key()['color'][:1],
                            legend_font_size= 10, legend_pos=9, legend_ncol=1)



ax3.yaxis.grid()

#porfolio_rolling_cumulative_performance.plot.area(ax=ax3, title='back test', stacked=False)

#ax3.xaxis.set_major_locator(mdates.MonthLocator(bymonth=(1)))

#ax3.xaxis.grid(True, which='major')

ax2.xaxis.set_major_locator(mdates.YearLocator())



##############################################################################################
##############################################################################################
##############################################################################################
##############################################################################################
##############################################################################################
##############################################################################################
##
## USING generic yield and credit spreads
##
##############################################################################################
##############################################################################################
##############################################################################################
##############################################################################################
##############################################################################################
##############################################################################################




#data = pd.read_csv(r'C:\Dev\Models\QuantModels\#applications\MVO\RatesCredits_20200511.csv')
data = pd.read_csv(r'C:\Dev\Models\QuantModels\#applications\MVO\RatesCreditsYTW_20200511.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill().dropna(axis=0)


data['US10'] = data['US10'] *100
data['AU10'] = data['AU10'] *100
data['EU10'] = data['EU10'] *100


data = data[[
    'US10','AU10','EU10',
    'All_Main_Sectors_ALL_ALL',
    'Corporates_All_All',
    'JPMorgan_Global_HY_Summary',
    'AUC0'
    ]]

data = data.rename(columns={
    'All_Main_Sectors_ALL_ALL':'US_IG',
    'Corporates_All_All':'EU_IG',
    'JPMorgan_Global_HY_Summary':'US_HY',
    'AUC0':'AU_IG'}
    )

#only needed for YTW not spreads
data['US_IG'] = data['US_IG'] *100
data['EU_IG'] = data['EU_IG'] *100
data['US_HY'] = data['US_HY'] *10000
data['AU_IG'] = data['AU_IG'] *100




data_cap_rtns_d = -1 *(data - data.shift(1)).dropna()
data_carry_rtns_d = (data/250).reindex(data_cap_rtns_d.index,axis='index')

data_rtns_d = data_cap_rtns_d + data_carry_rtns_d

assets = data_rtns_d.columns.tolist()



######### return profiles and chart

US10_rtns = pd.DataFrame({'Capital':data_cap_rtns_d['US10'], 'Carry':data_carry_rtns_d['US10']}).resample('Y').sum()
AU10_rtns = pd.DataFrame({'Capital':data_cap_rtns_d['AU10'], 'Carry':data_carry_rtns_d['AU10']}).resample('Y').sum()
EU10_rtns = pd.DataFrame({'Capital':data_cap_rtns_d['EU10'], 'Carry':data_carry_rtns_d['EU10']}).resample('Y').sum()
US_IG_rtns = pd.DataFrame({'Capital':data_cap_rtns_d['US_IG'], 'Carry':data_carry_rtns_d['US_IG']}).resample('Y').sum()
EU_IG_rtns = pd.DataFrame({'Capital':data_cap_rtns_d['EU_IG'], 'Carry':data_carry_rtns_d['EU_IG']}).resample('Y').sum()
US_HY_rtns = pd.DataFrame({'Capital':data_cap_rtns_d['US_HY'], 'Carry':data_carry_rtns_d['US_HY']}).resample('Y').sum()
AU_IG_rtns = pd.DataFrame({'Capital':data_cap_rtns_d['AU_IG'], 'Carry':data_carry_rtns_d['AU_IG']}).resample('Y').sum()
US10_rtns.index = US10_rtns.index.date
AU10_rtns.index = AU10_rtns.index.date
EU10_rtns.index = EU10_rtns.index.date
US_IG_rtns.index = US_IG_rtns.index.date
EU_IG_rtns.index = EU_IG_rtns.index.date
US_HY_rtns.index = US_HY_rtns.index.date
AU_IG_rtns.index = AU_IG_rtns.index.date


fig = plt.figure(figsize=(60,20))

_ax8 = fig.add_subplot(241)
_ax1 = fig.add_subplot(242)
_ax2 = fig.add_subplot(243)
_ax3 = fig.add_subplot(244)
_ax4 = fig.add_subplot(245)
_ax5 = fig.add_subplot(246)
_ax6 = fig.add_subplot(247)
_ax7 = fig.add_subplot(248)

US10_rtns.plot(kind='bar', stacked=True, title='US10',ax=_ax1)
AU10_rtns.plot(kind='bar', stacked=True, title='AU10',ax=_ax2)
EU10_rtns.plot(kind='bar', stacked=True, title='EU10',ax=_ax3)
US_IG_rtns.plot(kind='bar', stacked=True, title='US_IG',ax=_ax4)
EU_IG_rtns.plot(kind='bar', stacked=True, title='EU_IG',ax=_ax5)
US_HY_rtns.plot(kind='bar', stacked=True, title='US_HY',ax=_ax6)
AU_IG_rtns.plot(kind='bar', stacked=True, title='AU_IG',ax=_ax7)
plot_heatmap(data_rtns_d.cov(), _ax8)





#rolling vol and coVol
data_rtns_d_1y_rolling_covs = pd.DataFrame(rolling_apply(data_rtns_d, 250, get_flattened_cov)).transpose()
data_rtns_d_1y_rolling_vol = data_rtns_d_1y_rolling_covs[[ a + '_' + b for a,b in set([comb for comb in combinations(assets + assets, 2)]) if a == b]]
data_rtns_d_1y_rolling_vol = data_rtns_d_1y_rolling_vol.applymap(np.sqrt)
data_rtns_d_1y_rolling_covol = data_rtns_d_1y_rolling_covs[[ a + '_' + b for a,b in set([comb for comb in combinations(assets + assets, 2)]) if a != b]]


fig = plt.figure(figsize=(30,20))
__ax3 = fig.add_subplot(311)
__ax1 = fig.add_subplot(312)
__ax2 = fig.add_subplot(313)

data.plot(ax=__ax3, title='Data')
__ax3.legend(frameon=False, loc='lower center', ncol=10)
data_rtns_d_1y_rolling_vol.plot(ax=__ax1, title='1yr rolling Vol')
__ax1.legend(frameon=False, loc='upper center', ncol=10)
data_rtns_d_1y_rolling_covol.plot(ax=__ax2, title='1yr rolling pairwise CoVol')
__ax2.legend(frameon=False, loc='lower center', ncol=10)





######################################################################################## 
########################################################################################
# full history optimization
########################################################################################
########################################################################################



#overall vol
#data_rtns_d.plot()
data_rtns_d_cov = data_rtns_d.cov()



# PCA look on overall history
data_rtns_d_pca = pca.PCA(data_rtns_d.cumsum(), rates_pca = True, momentum_size = None)
data_rtns_d_pca.plot_loadings(n=3, m=5)



#vol and return matrix object on overall history  
data_rtns_d_mean_mtx = matrix(data_rtns_d.mean())
data_rtns_d_var_mtx = matrix(data_rtns_d_cov.to_numpy())




# weights > -0.3
g1 = np.array([
        [-1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0],
        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.0]
    ])
h1 = [ 0.3,  0.3,  0.3,  0.3,  0.3,  0.3,  0.3]

# max weights 
g2 = np.array([
        [1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]
    ])
h2 = [ 0.3,  0.3,  0.3,  0.3,  0.3,  0.3,  0.3]


gs = np.concatenate((g1, g2),axis=0)
hs = h1 + h2

G = matrix(gs)

h = matrix(hs)


#equality

A = matrix(1.0, (1,7))
b = matrix(1.0)

#build portfolios
_lambdas = [ 10**(5.0*t/5000-1.0) for t in range(5000)]


weights = [solvers.qp((_lambda)*data_rtns_d_var_mtx, -data_rtns_d_mean_mtx, G, h, A, b)['x'] for _lambda in _lambdas]
expected_annual_returns = [blas.dot(data_rtns_d_mean_mtx,x) * 250 for x in weights]
expected_annual_risks = [ np.sqrt(blas.dot(x, data_rtns_d_var_mtx*x)) * np.sqrt(250) for x in weights]
expected_annual_sharpes = [ rt/rs for rt, rs in zip(expected_annual_returns, expected_annual_risks)] 

unconstrainted_backtest_d_rtns = [get_weighted_return(data_rtns_d, x, name=s) for x, s in zip(weights, expected_annual_sharpes)]




# Plot trade-off curve and optimal allocations.

fig = plt.figure(figsize=(20,20))
ax0 = fig.add_subplot(221)
ax00 = fig.add_subplot(222)
ax1 = fig.add_subplot(223)
ax2 = fig.add_subplot(224)


data_rtns_d.cumsum().plot(ax=ax0)

plot_heatmap(data_rtns_d_cov, ax00)

ax1.plot(expected_annual_risks, expected_annual_returns)
ax1.set_xlabel('standard deviation')
ax1.set_ylabel('expected return')


c1 = pd.Series([ x[0] for x in weights ], name='US10')
c2 = pd.Series([ x[1] for x in weights ], name='AU10')
c3 = pd.Series([ x[2] for x in weights ], name='EU10')
c4 = pd.Series([ x[3] for x in weights ], name='US_IG')
c5 = pd.Series([ x[4] for x in weights ], name='EU_IG')
c6 = pd.Series([ x[5] for x in weights ], name='US_HY')
c7 = pd.Series([ x[6] for x in weights ], name='AU_IG')


cc = plt.rcParams['axes.prop_cycle'].by_key()['color']

s1 = plot_stacked_bar(ax2, partition_by_sign(c1, pos=True), left=expected_annual_risks, width = 0.1, color=cc[0], edgecolor ="none",  align="center")
s2 = plot_stacked_bar(ax2, partition_by_sign(c2, pos=True), left=expected_annual_risks, width = 0.1, color=cc[1], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=True).values)
s3 = plot_stacked_bar(ax2, partition_by_sign(c3, pos=True), left=expected_annual_risks, width = 0.1, color=cc[2], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=True).values + partition_by_sign(c2, pos=True).values)
s4 = plot_stacked_bar(ax2, partition_by_sign(c4, pos=True), left=expected_annual_risks, width = 0.1, color=cc[3], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=True).values + partition_by_sign(c2, pos=True).values + partition_by_sign(c3, pos=True).values)
s5 = plot_stacked_bar(ax2, partition_by_sign(c5, pos=True), left=expected_annual_risks, width = 0.1, color=cc[4], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=True).values + partition_by_sign(c2, pos=True).values + partition_by_sign(c3, pos=True).values + partition_by_sign(c4, pos=True).values)
s6 = plot_stacked_bar(ax2, partition_by_sign(c6, pos=True), left=expected_annual_risks, width = 0.1, color=cc[5], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=True).values + partition_by_sign(c2, pos=True).values + partition_by_sign(c3, pos=True).values 
                      + partition_by_sign(c4, pos=True).values+ partition_by_sign(c5, pos=True).values)
    
s7 = plot_stacked_bar(ax2, partition_by_sign(c7, pos=True), left=expected_annual_risks, width = 0.1, color=cc[6], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=True).values + partition_by_sign(c2, pos=True).values + partition_by_sign(c3, pos=True).values 
                      + partition_by_sign(c4, pos=True).values+ partition_by_sign(c5, pos=True).values+ partition_by_sign(c6, pos=True).values)



s1 = plot_stacked_bar(ax2, partition_by_sign(c1, pos=False), left=expected_annual_risks, width = 0.1, color=cc[0], edgecolor ="none",  align="center")
s2 = plot_stacked_bar(ax2, partition_by_sign(c2, pos=False), left=expected_annual_risks, width = 0.1, color=cc[1], edgecolor ="none",  align="center", bottom=partition_by_sign(c1, pos=False).values)
s3 = plot_stacked_bar(ax2, partition_by_sign(c3, pos=False), left=expected_annual_risks, width = 0.1, color=cc[2], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=False).values + partition_by_sign(c2, pos=False).values)
s4 = plot_stacked_bar(ax2, partition_by_sign(c4, pos=False), left=expected_annual_risks, width = 0.1, color=cc[3], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=False).values + partition_by_sign(c2, pos=False).values + partition_by_sign(c3, pos=False).values)
s5 = plot_stacked_bar(ax2, partition_by_sign(c5, pos=False), left=expected_annual_risks, width = 0.1, color=cc[4], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=False).values + partition_by_sign(c2, pos=False).values + partition_by_sign(c3, pos=False).values + partition_by_sign(c4, pos=False).values)
s6 = plot_stacked_bar(ax2, partition_by_sign(c6, pos=False), left=expected_annual_risks, width = 0.1, color=cc[5], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=False).values + partition_by_sign(c2, pos=False).values + partition_by_sign(c3, pos=False).values 
                      + partition_by_sign(c4, pos=False).values + partition_by_sign(c5, pos=False).values)
    
s7 = plot_stacked_bar(ax2, partition_by_sign(c7, pos=False), left=expected_annual_risks, width = 0.1, color=cc[6], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=False).values + partition_by_sign(c2, pos=False).values + partition_by_sign(c3, pos=False).values 
                      + partition_by_sign(c4, pos=False).values + partition_by_sign(c5, pos=False).values + partition_by_sign(c6, pos=False).values)



ax2.set_xlabel('standard deviation')
ax2.set_ylabel('allocation')
ax2.legend([s1, s2, s3, s4, s5, s6, s7],  ['US10', 'AU10', 'EU10', 'US_IG', 'EU_IG', 'US_HY', 'AU_IG'], fancybox=True, framealpha=0, loc=9, ncol=3, prop={'size':10}, bbox_to_anchor=(0.5, 1.12))













