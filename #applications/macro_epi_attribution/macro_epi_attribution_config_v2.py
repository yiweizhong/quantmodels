APP_ROOT = r"\\capricorn\ausfi\Macro_Dev\Data\macro_epi_attribution"
APP_NAME = r"macro_epi_attribution_v2"
DATE = r"DATE"
SENDER = "YIWEI.ZHONG@ampcapital.com"
RECEIVERS = ["YIWEI.ZHONG@ampcapital.com"]
CC = []
DTYPE_DATEIME64 = r"datetime64[ns]"
ODBC_STYLE_CSTR_PREFIX = r"mssql+pyodbc:///?odbc_connect=%s"
LOG_FORMAT = r"%(asctime)s - %(name)s - %(levelname)s - %(message)s"
TIMESTAMP_FMT = r"%Y-%m-%d %H:%M:%S.%f"
#CURRENT_EPI_DATA = r"\\capricorn\ausfi\Macro_Dev\Data\EPIBatch\Archive\EPIINDICE_RESULT_20160808_SPLICED.CSV"
#EPI_DATA_BASE1 = r"\\capricorn\ausfi\Macro_Dev\Data\EPIBatch\Archive\EPIINDICE_RESULT_20160801_SPLICED.CSV"
#EPI_DATA_BASE2 = r"\\capricorn\ausfi\Macro_Dev\Data\EPIBatch\Archive\EPIINDICE_RESULT_20160708_20160708083110.CSV"
#EPI_DATA_BASE3 = r"\\capricorn\ausfi\Macro_Dev\Data\EPIBatch\Archive\EPIINDICE_RESULT_20160508_20160508183532.CSV"

EPI_DIR = r"//capricorn/ausfi/Macro_Dev/Data/EPIBatch/BBGDataPatch/"
EPI_FILES = [
             "EPIINDICE_PATCH_20180818122511.csv",
             "EPIINDICE_PATCH_20180811204400.csv",
             "EPIINDICE_PATCH_20180804192611.csv",
             "EPIINDICE_PATCH_20180728183540.csv",
             "EPIINDICE_PATCH_20180721213520.csv",
             "EPIINDICE_PATCH_20180714222831.csv",
             "EPIINDICE_PATCH_20180708001308.csv",
             "EPIINDICE_PATCH_20180630210449.csv",
             "EPIINDICE_PATCH_20180623232723.CSV", 
             "EPIINDICE_PATCH_20180617190535.CSV",
             "EPIINDICE_PATCH_20180610211233.CSV",
             "EPIINDICE_PATCH_20180603202631.CSV",
             "EPIINDICE_PATCH_20180527081812.CSV",
             "EPIINDICE_PATCH_20180524092403.csv"
             ]

COUNTRIES = ['AU', 'US', 'UK', 'NZ', 'EU', 'CH', 'JN', 'SK', 'TA', 'TH', 'HK', 'MA','IN', 'ID','CA']
COUNTRIES = ['AU']

