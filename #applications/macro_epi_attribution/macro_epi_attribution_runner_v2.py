from __future__ import division
import _path
import os, sys, inspect, time
import logging
import common.dirs as dirs
import common.emails as emails
from stat import S_ISREG, ST_CTIME, ST_MODE
from lxml import html
import pandas as pd
import numpy as np
from datetime import date, datetime, timedelta #as td
import calendar
import matplotlib as mpl
import matplotlib.pyplot as plt
import sys
import traceback
import os
import matplotlib.dates as mpd
##from scipy import stats
##from scipy.optimize import curve_fit
import statsmodels.api as sm
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MultipleLocator, FormatStrFormatter, NullFormatter
import macro_epi_attribution_config_v2 as cfg

__author__ = "Yiwei Zhong"
__copyright__ = "AMP Capital Fixed Income Macro Market"
__version__ = "0.0.1"
__maintainer__ = "Yiwei Zhong"
__email__ = "Yiwei.Zhong@ampcapital.com"
__status__ = "Prod"

def setup():
    """Log settings"""
    _input_folder = dirs.get_creat_sub_folder(folder_name="Input", parent_dir=cfg.APP_ROOT)
    _log_folder = dirs.get_creat_sub_folder(folder_name="Log", parent_dir=cfg.APP_ROOT)
    _debug_folder = dirs.get_creat_sub_folder(folder_name="Debug", parent_dir=cfg.APP_ROOT)
    _output_folder = dirs.get_creat_sub_folder(folder_name="Output", parent_dir=cfg.APP_ROOT)
    _archive_folder = dirs.get_creat_sub_folder(folder_name="Archive", parent_dir=cfg.APP_ROOT)

    _archive_input_folder = dirs.get_creat_sub_folder(folder_name="Input", parent_dir=_archive_folder)
    _archive_log_folder = dirs.get_creat_sub_folder(folder_name="Log", parent_dir=_archive_folder)
    _archive_debug_folder = dirs.get_creat_sub_folder(folder_name="Debug", parent_dir=_archive_folder)
    _archive_output_folder = dirs.get_creat_sub_folder(folder_name="Output", parent_dir=_archive_folder)

    _log_file_name = dirs.get_log_file_name(logfile_name_prefix=cfg.APP_NAME,log_folder=_log_folder)

    return _log_folder, _debug_folder, _output_folder, _archive_folder, \
           _log_file_name, _archive_log_folder, _archive_debug_folder, \
           _archive_output_folder, _input_folder, _archive_input_folder


def calc_exp_zscore(x):
    """
    :param x: a series of dp values
    :return: a tuple of exp_mean ts,exp_std ts and exp_z ts
    """

    # there is a chance there will be leading blanks in the time series. the expanding window needs to ignore them

    row_keys = x.sort_index().index
    x1 = x.sort_index().dropna()
    partial_row_keys = [i for i in row_keys if (i >= x1.index.min())]

    exp_mean = pd.expanding_mean(x.reindex(partial_row_keys))
    exp_std = pd.expanding_std(x.reindex(partial_row_keys))
    exp_z = (x.reindex(partial_row_keys) - exp_mean)/exp_std

    return exp_mean.reindex(row_keys),exp_std.reindex(row_keys),exp_z.reindex(row_keys)


def plot_pd_non_ts(x, y, c, s, ax, **kwargs):
    """
    :param x: x
    :param y: y
    :param c: sequence of color
    :param s: seuqence of sizes
    :param ax: ax
    :param kwargs:
    :return:
    """
    ax.scatter(x, y, c=c, s=s, alpha=0.8, edgecolors='none')
    ax.xaxis.grid(True, which='major')
    ax.xaxis.grid(True, which='minor')
    ax.yaxis.grid(True)
    ax.set_xlim(1, 240)
    #ax.xaxis.set_minor_locator(mpd.DayLocator(bymonthday=(1,15)))
    #ax.xaxis.set_minor_formatter(mpd.DateFormatter('%d'))
    ax.xaxis.set_minor_locator(MultipleLocator(5))
    ax.xaxis.set_minor_formatter(NullFormatter())
    ax.xaxis.set_major_locator(MultipleLocator(10))
    #ax.xaxis.set_major_formatter(mpd.DateFormatter('\n%b \n%Y'))
    ax.xaxis.set_tick_params(which='major', labelsize=8)
    ax.xaxis.set_tick_params(which='minor', labelsize=8)
    ax.yaxis.set_tick_params(which='major', labelsize=8)
    ax.yaxis.set_tick_params(which='minor', labelsize=8)



def get_date_from_data_file_name(file_name):
    file_name = os.path.basename(file_name)
    datestring = file_name.split("_")[2][0:8]
    result = datetime.strptime(datestring, "%Y%m%d")
    return result

def load_csv_file(f, dir):
    logging.info(">>>" + inspect.stack()[0][3])
    logging.info("Start to load " + f)
    f = os.path.join(dir, f)
    df = pd.DataFrame.from_csv(f)
    logging.info("Finish loading " + f)
    return df

def load_csv_file(f):
    logging.info(">>>" + inspect.stack()[0][3])
    logging.info("Start to load " + f)
    df = pd.DataFrame.from_csv(f)
    logging.info("Finish loading " + f)
    return df

def get_top_n_file_names(dir, n):
    files = (os.path.join(dir, fn) for fn in os.listdir(dir))
    files = ((os.stat(path), path) for path in files)
    files = ((stat[ST_CTIME], path) for stat, path in files if S_ISREG(stat[ST_MODE]))
    top_n_files = sorted(files, reverse=True)[0: n]
    for cdate, path in top_n_files:
        logging.info( "{0} {1}".format(time.ctime(cdate), os.path.basename(path)))
    #result = [os.path.basename(path) for cdate, path in top_n_files]
    result = [path for cdate, path in top_n_files]
    return result

def get_file_names_by_idx(dir, l):
    files = (os.path.join(dir, fn) for fn in os.listdir(dir))
    files = ((os.stat(path), path) for path in files)
    files = ((stat[ST_CTIME], path) for stat, path in files if S_ISREG(stat[ST_MODE]))
    top_n_files = np.array(sorted(files, reverse=True))[l]
    for cdate, path in top_n_files:
        logging.info( "{0} {1}".format(time.ctime(long(cdate)), os.path.basename(path)))
    #result = [os.path.basename(path) for cdate, path in top_n_files]
    result = [path for cdate, path in top_n_files]
    return result


def add_months(source_date,months):
    """
    :param source_date: the date to anchor on
    :param months: months to be added
    :return: a new date
    """
    #logging.info(">>>" + inspect.stack()[0][3])
    month = source_date.month - 1 + months
    year = source_date.year + month // 12
    month = month % 12 + 1
    day = min(source_date.day,calendar.monthrange(year,month)[1])
    return date(year,month,day)


def get_n_EOM_date_range(input_date, n=240):
    """
    :param input_date:
    :return: date range
    """
    logging.info(">>>" + inspect.stack()[0][3])

    # this is a urgent fix -start
    if input_date.month <= 10:
        range_end_date = date(input_date.year,input_date.month + 2, 1)
    else:
        range_end_date = date(input_date.year + 1 ,input_date.month + 2 - 12, 1)
    # this is a urgent fix -end

    #date_range = [add_months(range_end_date,-i) - datetime.timedelta (days = 1) for i in range(0, 240, 1)]
    date_range = [add_months(range_end_date,-i) - timedelta (days = 1) for i in range(0, 240, 1)]
    return date_range


def get_country_epi_constituents(df, c, t, fl="Lead"):
    """
    Parameter:
    -----------
    df - categorisation df
    c - country
    t - epi type
    fl = Lead/Lag

    Return:
    ---------
    A df of BBG ticker code and Grow Correlation factors

    """
    logging.info(">>>" + inspect.stack()[0][3])
    #return df[(df["Country"] == c) & (df["DataType"] == t) & (df["FwdLagging"] == fl)][["BBGTickerCode","GrowthCorrelation"]]
    return df[(df["Country"] == c) & (df["DataType"] == t)][["BBGTickerCode","GrowthCorrelation"]]



def get_epi_bbg_data(df, s):
    """
    :param df: bbg data df
    :param s: security
    :return: a df of sec eod prices
    """
    logging.info(">>>" + inspect.stack()[0][3])
    df1 = df[(df["Security"] == s)][["PX_LAST_Date","PX_LAST"]]
    df1["DATE"] = pd.to_datetime(df1.PX_LAST_Date, format= '%Y%m%d')
    return df1[["DATE", "PX_LAST"]].set_index('DATE')



def check_new_data_points(ts_old, ts_new):
    """
    :param ts_old: the older EPI bbg data
    :param ts_new: the newer EPI bbg EPI
    :return: a ts of data points appear in the new series since the end of old time series
    """
    logging.info(">>>" + inspect.stack()[0][3])
    logging.debug("Last data date in the old data series is " + ts_old.index.max().strftime("%Y_%m_%d"))
    logging.debug("Last data date in the new data series is " + ts_old.index.max().strftime("%Y_%m_%d"))
    df1 = ts_new.loc[ts_new.dropna().index > ts_old.dropna().index.max()]
    has_new_data = (True if len(df1) > 0 else False)
    if has_new_data:
        logging.info("Found " + str(len(df1)) + " new dps")
    else:
        logging.info("No new dp")
    return has_new_data, df1

def check_revised_data_points(ts_old, ts_new):
    """
    :param ts_old: the older EPI bbg data
    :param ts_new: the newer EPI bbg EPI
    :return: a ts of data points appear in the new series whose equivalent in old series has a different value
    """
    logging.info(">>>" + inspect.stack()[0][3])

    df1 = pd.concat([ts_old.dropna(), ts_new.dropna()], axis=1, join='inner')

    df1["REVISED"] = (df1[[0]] != df1[[1]])

    df1.columns = ["PX_LAST_OLD", "PX_LAST_NEW", "REVISED"]
    has_new_data = (True if len(df1[df1["REVISED"] == True]) > 0 else False)

    if has_new_data:

        logging.info("Found " + str(len(df1[df1["REVISED"] == True])) + " revised dps")
    else:
        logging.info("No revised dp")
    return has_new_data, df1[df1["REVISED"] == True]


def resample_bbg_data_into_monthly_dp(df):
    """
    :param df: the raw PX_LAST data df
    :return: resampled PX_LAST data df
    """
    logging.info(">>>" + inspect.stack()[0][3])

    #print df.head()

    if(len(df) > 0):
        df1 = df.copy(deep=True)
        df2 = df1.resample('M').mean()
        #if(len(df1) == len(df2)
        #   and (df1["PX_LAST"].isnull() == df2["PX_LAST"].isnull()).all()
        #   and (df1["PX_LAST"][df1["PX_LAST"].notnull()] == df2["PX_LAST"][df2["PX_LAST"].notnull()]).all()
        #   ):
        #    logging.info("Monthly resample is applied")
        #    return df
        #else:
        #    logging.info("Monthly resample is skipped")
        #    return df2
        return df2
    else:
        logging.info("Monthly resample is skipped. Blank df")
        return df


def create_epi_input_from_bbg_indice(df, indice_name, row_keys, grow_correlation = 1):
    """
    :param df: the df contains the total market prices loaded from csv
    :param indice_name: the indice name, eg. 'ACNFCOMF Index'
    :param row_keys: the 240 month end dates for a particular run date
    :return: a df with 240 dps of prices. Some of them will Nan. They will be taken care of by ffill
    """
    df1 = get_epi_bbg_data(df, indice_name)
    df2 = resample_bbg_data_into_monthly_dp(df1)
    df3 = df2.reindex(row_keys)
    df3["PX_LAST"] = df3["PX_LAST"] * grow_correlation
    return df3




def calc_exp_zsc_on_mva_with_min_n_dps(df,n=1):
    """
    :param df: a df with 240 rows of data grid and holding the px_last for a indice
    :return: a df of expanding zscore
    :Note: Because this is a cut down version of the epi process only the monthly dp is used. in real epi there will also be a quaterly and yearly mva data
           Because the minimal length to create yearly mva is 12 dps the first 12 dps are choped off
           Because the zscore needs a minimal 12 dp, any exp window with less than 12 dp should have the zscore Nan
           When calculating the zscore, only the window position where MVA is available. The ffill is only applied after the valid zscore is calculated.
    """
    row_keys = df.sort_index().index

    df1 = df.sort_index().dropna()  #.ffill()
    partial_row_keys = [i for i in row_keys if (i >= df1.index.min() and i <= df1.index.max())]

    #debug
    #display(partial_row_keys)

    #chop off the first 24 dps
    #and ffill any missing dp in between the first and last available dates
    df2 = df1.reindex(partial_row_keys).sort_index().ffill()[12:]

    exp_mean = pd.expanding_mean(df2,min_periods=n)
    exp_std = pd.expanding_std(df2,min_periods=n)

    exp_z = (df2 - exp_mean)/exp_std
    exp_z = exp_z.reindex(row_keys).sort_index().ffill()


    #debug
    #display("raw")
    #display(df2.dropna().head(10))
    #display(df2.tail(10))

    #display("mean")
    #display(exp_mean.dropna().head(10))
    #display(exp_mean.tail(10))

    #display("stdev")
    #display(exp_std.dropna().head(10))
    #display(exp_std.tail(10))

    #display(exp_z.tail(10))

    return exp_z

def calc_exp_mean_on_mva_with_min_n_dps(df,n=1):
    """
    :param df: a df with 240 rows of data grid and holding the px_last for a indice
    :return: a df of expanding zscore
    :Note: Because this is a cut down version of the epi process only the monthly dp is used. in real epi there will also be a quaterly and yearly mva data
           Because the minimal length to create yearly mva is 12 dps the first 12 dps are choped off
           Because the zscore needs a minimal 12 dp, any exp window with less than 12 dp should have the zscore Nan
    """
    row_keys = df.sort_index().index
    df1 = df.sort_index().ffill()
    df2 = df1[13:241] #chop off the first 24 dps
    exp_mean = pd.expanding_mean(df2,min_periods=n)
    exp_mean = exp_mean.reindex(row_keys).sort_index().ffill()
    return exp_mean

def calc_exp_std_on_mva_with_min_n_dps(df,n=1):
    """
    :param df: a df with 240 rows of data grid and holding the px_last for a indice
    :return: a df of expanding zscore
    :Note: Because this is a cut down version of the epi process only the monthly dp is used. in real epi there will also be a quaterly and yearly mva data
           Because the minimal length to create yearly mva is 12 dps the first 12 dps are choped off
           Because the zscore needs a minimal 12 dp, any exp window with less than 12 dp should have the zscore Nan
    """
    row_keys = df.sort_index().index
    df1 = df.sort_index().ffill()
    df2 = df1[13:241] #chop off the first 24 dps
    exp_std = pd.expanding_std(df2,min_periods=n)
    exp_std = exp_std.reindex(row_keys).sort_index().ffill()
    return exp_std



def create_epi_data_series_dict(data_file_new,
                                date_new,
                                date_keys_new,
                                bbg_ticker_specs,
                                country,
                                epi_type):
    """
    :param data_file_old: df
    :param data_file_new: df
    :param date_new: date
    :param date_old: date
    :param date_keys_new: df
    :param date_keys_old: df
    :param bbg_ticker_specs: df with indice name and growth correlations
    :return: dict of dict. inner dict holds details about a single epi inputs for a date
    """

    filtered_bbg_ticker_specs = get_country_epi_constituents(bbg_ticker_specs,country, epi_type)
    # create an empty dict

    epi_series_dcit = {}

    # loop through the bbg_ticker_specs
    for (idx, row) in filtered_bbg_ticker_specs.iterrows():

        epi_item_old_dcit = {}
        epi_item_new_dcit = {}

        indice = row["BBGTickerCode"]
        gc = row["GrowthCorrelation"]
        logging.info("process " + indice + " gc: " + str(gc))

        if "UKHBSAMM Index" == str.strip(indice):
            logging.debug("break for " + indice)

        bbg_input_new = create_epi_input_from_bbg_indice(data_file_new, indice, date_keys_new, grow_correlation=gc).sort_index()

        bbg_input_new_ffill = bbg_input_new.sort_index().ffill()

        #debug
        #display("new raw bbg " + indice)

        bbg_input_new_exp_z = calc_exp_zsc_on_mva_with_min_n_dps(bbg_input_new,12)

        bbg_input_new_exp_mean = calc_exp_mean_on_mva_with_min_n_dps(bbg_input_new,12)

        bbg_input_new_exp_std = calc_exp_std_on_mva_with_min_n_dps(bbg_input_new,12)

        epi_item_new_dcit["TYPE"] = "NEW"
        epi_item_new_dcit["EPI_DATE"] = date_new.strftime("%Y-%m-%d")
        epi_item_new_dcit["EPI_WINDOW"] = min(date_keys_new).strftime("%Y-%m-%d") + "=>" + max(date_keys_new).strftime("%Y-%m-%d")
        epi_item_new_dcit["LATEST_DP_DATE"] = bbg_input_new.dropna().index.max()
        epi_item_new_dcit["LATEST_DP"] = bbg_input_new.ix[bbg_input_new.dropna().index.max()]["PX_LAST"]
        epi_item_new_dcit["BBG_INPUT"] = bbg_input_new
        epi_item_new_dcit["BBG_INPUT_FF"] = bbg_input_new_ffill
        epi_item_new_dcit["BBG_INPUT_Z"] = bbg_input_new_exp_z
        epi_item_new_dcit["BBG_INPUT_STD"] = bbg_input_new_exp_std
        epi_item_new_dcit["BBG_INPUT_MEAN"] = bbg_input_new_exp_mean
        epi_item_new_dcit["LATEST_Z_DP"] = bbg_input_new_exp_z.ix[bbg_input_new_exp_z.dropna().index.max()]["PX_LAST"]
        epi_item_new_dcit["LATEST_STD_DP"] = bbg_input_new_exp_std.ix[bbg_input_new_exp_std.dropna().index.max()]["PX_LAST"]
        epi_item_new_dcit["LATEST_MEAN_DP"] = bbg_input_new_exp_mean.ix[bbg_input_new_exp_mean.dropna().index.max()]["PX_LAST"]

        epi_series_dcit[indice] = epi_item_new_dcit

    return epi_series_dcit



def chart_epi_summary_info_vertical_3series(df1, df2, df3, from_date1, from_date2, from_date3, w = 10, h = 4,
                                            padding_top = 0, padding_bottom = 0, height_divisor = 2, ylim_bottom=None, ylim_top=None
                                            ,xlim_left = -3, xlim_right = 3,
                                            output_file_name=None,
                                            output_dir=None,
                                            EPI_date = None,
                                            dpi=100):
    """
    :param item_pair_dict: a dict containing the epi info
    :return: None
    """

    def padding_helper(i):
        return (padding_bottom  + i * 1)



    def padding_top_helper(row):
        if row <=3:
            return 5
        elif row <= 10:
            return 3
        else:
            return 1

    def hspace_helper(row):
        if row <= 3:
            return 0.2
        else:
            return 0.1

    def height_helper(row):
        if row <= 3:
            return 0.5
        elif row <= 6:
            return 2.5
        elif row <= 10:
            return 4
        elif row <= 12:
            return 5
        elif row <= 14:
            return 6
        else:
            return 8

    y_loc1 = np.array([padding_helper(i) for i, item in enumerate(df1.columns)])
    y_loc2 = np.array([padding_helper(i) for i, item in enumerate(df2.columns)])
    y_loc3 = np.array([padding_helper(i) for i, item in enumerate(df3.columns)])

    def color_helper(x):
        if np.sign(x) >= 0:
            return 'g'
        else:
            return 'r'

    color1 = [ color_helper(x) for x in df1.ix[0]]
    color2 = [ color_helper(x) for x in df2.ix[0]]
    color3 = [ color_helper(x) for x in df3.ix[0]]

    bar_width = 0.2

    plt.close()


    #fig1 = plt.figure(figsize=(w, np.max(y_loc1)/height_divisor + padding_top))
    #fig1 = plt.figure(figsize=(w, np.max(y_loc1) / height_divisor + padding_top_helper(np.max(y_loc1))))
    fig1 = plt.figure(figsize=(w, height_helper(np.max(y_loc1))  + padding_top_helper(np.max(y_loc1))))

    #gs = gridspec.GridSpec(1, 3, width_ratios=[1,1,1],hspace= 0.1, wspace=0.05)
    gs = gridspec.GridSpec(1, 3, width_ratios=[1, 1, 1], hspace=hspace_helper(np.max(y_loc1)), wspace=0.05)


    ax1 = plt.subplot(gs[0])
    ax2 = plt.subplot(gs[1])
    ax3 = plt.subplot(gs[2])

    ax1.set_title("since " + from_date1,  fontsize=10)
    ax2.set_title("since " + from_date2,  fontsize=10)
    ax3.set_title("since " + from_date3,  fontsize=10)

    rects1 = ax1.barh(y_loc1 + 0.5 * bar_width, df1.ix[0], bar_width, color=color1)
    rects2 = ax2.barh(y_loc2 + 0.5 * bar_width, df2.ix[0], bar_width, color=color2)
    rects3 = ax3.barh(y_loc2 + 0.5 * bar_width, df3.ix[0], bar_width, color=color3)

    ax1.set_yticks(y_loc1 + 1 * bar_width)
    ax2.set_yticks(y_loc2 + 1 * bar_width)
    ax3.set_yticks(y_loc3 + 1 * bar_width)

    ax1.set_yticklabels(df1.columns)
    ax2.set_yticklabels(df2.columns)
    ax3.set_yticklabels(df3.columns)

    ax1.set_xlim([xlim_left,xlim_right])
    ax2.set_xlim([xlim_left,xlim_right])
    ax3.set_xlim([xlim_left,xlim_right])

    ax1.axvline(linewidth=1, color="k")
    ax2.axvline(linewidth=1, color="k")
    ax3.axvline(linewidth=1, color="k")

    ax1.grid(True)
    ax2.grid(True)
    ax3.grid(True)

    if(ylim_bottom is not None and ylim_top is not None):
        ax1.set_ylim([ylim_bottom,ylim_top])
        ax2.set_ylim([ylim_bottom,ylim_top])
        ax3.set_ylim([ylim_bottom,ylim_top])

    ax1.tick_params(axis='y', which='major', pad=30)

    for idx, rect in enumerate(rects1):
        rec_height = rect.get_height() * np.sign(df1.ix[0][idx])
        rec_width = rect.get_width() * np.sign(df1.ix[0][idx])
        if(rec_height == 0):
            label_x = -0.08
        else :
            label_x = rec_width + np.sign(rec_width) * 0.1
        ax1.xaxis.set_tick_params(which='major', labelsize= 8)
        ax1.text(label_x, rect.get_y() + rect.get_height() * 0.8, ('%.4f'% rec_width).rstrip('0').rstrip('.') , ha='center', va='bottom', size=8)

    for idx, rect in enumerate(rects2):
        rec_height = rect.get_height() * np.sign(df2.ix[0][idx])
        rec_width = rect.get_width() * np.sign(df2.ix[0][idx])
        if(rec_height == 0):
            label_x = -0.08
        else :
            label_x = rec_width + np.sign(rec_width) * 0.1
        ax2.xaxis.set_tick_params(which='major', labelsize= 8)
        ax2.text(label_x, rect.get_y() + rect.get_height() * 0.8, ('%.4f'% rec_width).rstrip('0').rstrip('.') , ha='center', va='bottom', size=8)

    for idx, rect in enumerate(rects3):
        rec_height = rect.get_height() * np.sign(df3.ix[0][idx])
        rec_width = rect.get_width() * np.sign(df3.ix[0][idx])
        if(rec_height == 0):
            label_x = -0.08
        else :
            label_x = rec_width + np.sign(rec_width) * 0.1
        ax3.xaxis.set_tick_params(which='major', labelsize= 8)
        ax3.text(label_x, rect.get_y() + rect.get_height() * 0.8, ('%.4f'% rec_width).rstrip('0').rstrip('.') , ha='center', va='bottom', size=8)

    plt.setp(ax2.get_yticklabels(), visible=False)
    plt.setp(ax3.get_yticklabels(), visible=False)

    plt.suptitle(output_file_name + ", as of EPI date [" + EPI_date +"]", fontsize=12)

    #plt.show()
    if output_file_name is not None and output_dir is not None:
        f = os.path.join(output_dir, output_file_name + ".png")
        logging.info("Output chart as " + f)
        #fig1.tight_layout()
        plt.savefig(f, dpi=dpi, bbox_inches='tight')
        #plt.savefig(f, dpi=dpi)
        plt.close()


def get_epi_component_zscores(item_pair_dict1,
                              epi_type,
                              country,
                              #ind_name_df,
                              output_dir=None
                              ):
    """
    :param item_pair_dict: a dict of tuples of 2 epi_item
    :return:
    """
    latest_zscores_difference_summary1 = {}

    latest_dp_difference_summary_d1 = {}


    EPI_start_date = ""
    EPI_end_date = ""

    temp_new_epi_expz_dict = {}

    if len(item_pair_dict1) > 0:

        for indice, item_pair in item_pair_dict1.iteritems():
            #ind_name = ind_name_df[ind_name_df.index == indice]["Name"][0] if len(ind_name_df[ind_name_df.index == indice]["Name"]) > 0 else indice
            #latest_zscores_difference_summary1[ind_name] = item_pair[1]["LATEST_Z_DP"] - item_pair[0]["LATEST_Z_DP"]
            #latest_dp_difference_summary_d1[ind_name] = item_pair[1]["LATEST_DP"] - item_pair[0]["LATEST_DP"]
            #EPI_start_date1 = item_pair[0]["EPI_DATE"]
            #EPI_end_date = item_pair[1]["EPI_DATE"]
            temp_new_epi_expz_dict[indice] = item_pair["BBG_INPUT_Z"]["PX_LAST"]

        


    else:
        logging.warn("[ALL_" + country + "_" + epi_type + "_ALL] EPI constituents change summary unavailable")
         #display(HTML("</p></p></p></p><h4>[ALL_" + country + "_" + epi_type + "_ALL] EPI constituents change summary unavailable</h4></p></p></p></p>"))

    return temp_new_epi_expz_dict


class epi(object):
    """An epi class object with attribution
    """
    def __init__(self,
                 bbg_ticker_file,
                 bbg_indice_translation_files,
                 curr_data_file):

        self._bbg_tickers = load_csv_file(bbg_ticker_file)
        self._bbg_indice_translations = load_csv_file(bbg_indice_translation_files)
        self._current_date = get_date_from_data_file_name(curr_data_file)
        self._current_data = load_csv_file(curr_data_file)
        self._date_keys_new = get_n_EOM_date_range(self._current_date)
    @property
    def current_date_keys(self):
        return self._date_keys_new


    @property
    def bbg_tickers(self):
        return self._bbg_tickers

    @property
    def current_date(self):
        return self._current_date

    @property
    def date_keys_current(self):
        return self._date_keys_new


    @property
    def current_bbg_data(self):
        return self._current_data


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-- Main
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':

    _log_folder, _debug_folder, _output_folder, _archive_folder, \
    _log_file_name, _archive_log_folder, _archive_debug_folder, \
    _archive_output_folder, _input_folder, _archive_input_folder = setup()
    _exit_code = 9999

    try:

        logging.basicConfig(filename= _log_file_name, format=cfg.LOG_FORMAT, level=logging.DEBUG)
        logging.info("<<<< {0} starts >>>>".format(cfg.APP_NAME))

        # Archive
        #dirs.archive_files(_output_folder, _archive_output_folder)

        # process
		# dir = r"\\capricorn\ausfi\Macro_Dev\Data\EPIBatch\Archive"
		# ticker_file = r"\\capricorn\AUSFI\Macro_Dev\Data\macro_epi_attribution\Config\G10_Business_EPI_Indice_Categorisation_full_705.csv"
		# ticker_file = r"\\capricorn\AUSFI\Macro_Dev\Data\macro_epi_attribution\Config\G10_Consumer_EPI_Indice_Categorisation_full_705.csv"
        ticker_file = r'//capricorn/ausfi/Macro_Dev/Data/EPIBatch/Category/EPI_Indice_Categorisation_full_705.csv'
        indice_translation_file = r'\\capricorn\ausfi\Macro_Dev\Data\EPIBatch\Category\EPINameTranslation.CSV'

        history = {}
        
        for epi_file in cfg.EPI_FILES:
            epi_obj = epi(ticker_file, indice_translation_file, cfg.EPI_DIR + epi_file)


            for country in cfg.COUNTRIES:

                if country not in history: 
                    history[country] = {}
                    history[country]['Business'] = []
                    history[country]['Inflation'] = []
                    history[country]['Growth'] = []
                    history[country]['Consumer'] = []
                    history[country]['Employment'] = []


                for epi_type in ["Business", "Inflation", "Growth", "Consumer", "Employment"]:
                
                    
                    epi_result1 = create_epi_data_series_dict(epi_obj.current_bbg_data,
                                                              epi_obj.current_date,
                                                              epi_obj.current_date_keys,
                                                              epi_obj.bbg_tickers,
                                                              country,
                                                              epi_type)

                    #epi_component_zscrs = get_epi_component_zscores(epi_result1, epi_type,country, epi_result1._bbg_indice_translations, output_dir=_output_folder)
                    epi_component_zscrs = get_epi_component_zscores(epi_result1, epi_type,country, output_dir=_output_folder)
                    
                    print epi_type
                
                    if (len(epi_component_zscrs)>0):
                        print "has data"
                        epi_component_zscrs_df = pd.DataFrame(epi_component_zscrs)
                        epi_component_zscrs_df_last = epi_component_zscrs_df.tail(1)
                    
                        epi_component_zscrs_df_last.index = [epi_obj.current_date]

                        history[country][epi_type].append(epi_component_zscrs_df_last)
        
        print "output data"
        
        for c, c_his in history.iteritems():
            print c
            for epi_type, his in c_his.iteritems():
                print epi_type
                if(len(his) > 0):
                    print _output_folder, c + "_" + epi_type + "_components_expz.csv"
                    output = pd.concat(his).sort_index()
                    output.to_csv(os.path.join(_output_folder, c + "_" + epi_type + "_components_expz.csv"))
            
                #print result 

        logging.info("<<<< {0} ends SUCCESSFULLY >>>>".format(cfg.APP_NAME))
        _exit_code = 0
    except:
        logging.exception("!!! Error detected in {0}".format(cfg.APP_NAME))
        logging.info("<<<< {0} ends SUCCESSFULLY >>>>".format(cfg.APP_NAME))
        _exit_code = 9999
    finally:
        _result = dirs.check_log_file_result(_log_file_name)
        _title = "{0} process - {1}".format(cfg.APP_NAME, _result)
        _email_body = dirs.htmlfy_log_file(_log_file_name)
        emails.send_SMTP_mail(cfg.SENDER, cfg.RECEIVERS, _title, _email_body, cfg.CC, [_log_file_name])


