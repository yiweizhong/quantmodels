import os, sys, inspect
import logging
import datetime
import copy
import pandas as pd
from pandas.tseries.offsets import DateOffset, BDay


### common 
TEST_MODE = False
ARCHIVE_FILES = True
APP_NAME = r'Valuation'
CURRENT_PATH = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0])) 
APP_ROOT = r'c:/temp/test/' + APP_NAME
PROD_Lib = r'//capricorn/ausfi/macro_dev/data/macro_tools'
CORE_ROOT = r''
SENDER = 'YIWEI.ZHONG@ampcapital.com'
RECEIVERS = ['YIWEI.ZHONG@ampcapital.com']
CC = []
DTYPE_DATEIME64 = r'datetime64[ns]'
ODBC_STYLE_CSTR_PREFIX = r'mssql+pyodbc:///?odbc_connect=%s'
LOG_FORMAT = r'%(asctime)s - %(name)s - %(levelname)s - %(message)s'
TIMESTAMP_FMT = r'%Y-%m-%d %H:%M:%S.%f'
FI_PROD_DB_CONN_STRING = r'DRIVER={SQL Server Native Client 11.0};SERVER=frontofficesql\FO1;DATABASE=Fixed_income;Trusted_Connection=yes;Integrated Security=True; MultipleActiveResultSets=True; Connection Timeout=800'
LOG_PATH = ''
LOG_FOLDER = ''
DEBUG_FOLDER = ''
OUTPUT_FOLDER = ''
ARCHIVE_FOLDER = ''
ARCHIVE_LOG_FOLDER = ''
ARCHIVE_DEBUG_FOLDER = ''
ARCHIVE_OUTPUT_FOLDER = ''
INPUT_FOLDER = ''


### app sepcific
COUNTRY_CONFIGS = None


### 
def config_path():

    _parent_path = os.path.abspath(os.path.join(CURRENT_PATH, os.pardir))
    _gparent_path = os.path.abspath(os.path.join(_parent_path, os.pardir))
    _ggparent_path = os.path.abspath(os.path.join(_gparent_path, os.pardir))

    #adding the path here
    if (TEST_MODE):        
        _core_path = _gparent_path
        if _core_path not in sys.path: sys.path.insert(0, _core_path)
    else:
        if  PROD_Lib not in sys.path: sys.path.insert(0, PROD_Lib)


    import core.dirs as dirs
    
    global INPUT_FOLDER, LOG_FOLDER, DEBUG_FOLDER, \
           OUTPUT_FOLDER, ARCHIVE_FOLDER, ARCHIVE_LOG_FOLDER, \
           ARCHIVE_DEBUG_FOLDER, ARCHIVE_OUTPUT_FOLDER, \
           LOG_PATH


    INPUT_FOLDER = dirs.get_creat_sub_folder(folder_name='input', parent_dir=APP_ROOT) 
    LOG_FOLDER = dirs.get_creat_sub_folder(folder_name='log', parent_dir=APP_ROOT)
    DEBUG_FOLDER = dirs.get_creat_sub_folder(folder_name='debug', parent_dir=APP_ROOT)
    OUTPUT_FOLDER = dirs.get_creat_sub_folder(folder_name='output', parent_dir=APP_ROOT)
    ARCHIVE_FOLDER  = dirs.get_creat_sub_folder(folder_name='archive', parent_dir=APP_ROOT)
    ARCHIVE_LOG_FOLDER = dirs.get_creat_sub_folder(folder_name="log", parent_dir=ARCHIVE_FOLDER)
    ARCHIVE_DEBUG_FOLDER = dirs.get_creat_sub_folder(folder_name="debug", parent_dir=ARCHIVE_FOLDER)
    ARCHIVE_OUTPUT_FOLDER = dirs.get_creat_sub_folder(folder_name="Output", parent_dir=ARCHIVE_FOLDER)

    ###
    LOG_PATH = os.path.join(LOG_FOLDER + '/' + APP_NAME + '_' + datetime.datetime.now().strftime("%Y%m%d_%H%M%S")+'.log')
    logging.basicConfig(filename=LOG_PATH, level=logging.INFO, format = '%(asctime)s - %(levelname)s - %(message)s')
    
    logging.info("sys.path:")
    for p in sys.path: logging.info(p)
    #for p in sys.path: print p
    return

###
config_path()


###
SCORECARD_COUNTRIES = ['AUD', 'USD', 'EUR',  'GBP', 'CNY', 'JPY', 'KRW',]

###
DATE_IDX = pd.date_range('1997-01-01', datetime.datetime.now().strftime(r'%Y-%m-%d'), freq=BDay())
EPI_DATE_IDX = pd.date_range('1997-01-01', datetime.datetime.now().strftime(r'%Y-%m-%d'), freq=BDay())

###
EPI_M_PULSE_WINDOW_SIZE = 40
EPI_Q_REGIME_WINDOW_SIZE = 60

EXPORT_COLS = ['AUD', 'USD', 'CAD', 'CNY', 'EUR', 'JPY', 'NZD', 'GBP']

def IsGlobal(c):
    for tag in ['W-EUROPE', 'E-EUROPE', 'LATAM', 'GLOBAL', 'G10', 'ASIA']:
        if tag in c:
            return False
    return True


def IsG10(c):
    for tag in ['AU', 'CA', 'EU', 'GE', 'JN', 'NZ', 'SW', 'SZ', 'US', 'UK']:
        if tag in c:
            return True
    return False


def IsAsia(c):
    for tag in ['CH', 'IN', 'MA', 'SI', 'SK', 'TA', 'TH', 'VN', 'HK']:
        if tag in c:
            return True
    return False



