from __future__ import division
import _path
import _config as cfg
import logging
import os, sys, inspect
import time, datetime
from pandas.tseries.offsets import *
from collections import namedtuple
import argparse
import common.dirs as dirs
import common.emails as emails
import component_sudoku as sudoku



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-- Main
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='---- Valuation Data Processor ----')
    parser.add_argument('components', metavar='components', type=str, nargs='+', help='Separate component by a space')
    args = parser.parse_args()
    print(args.components)

    if (cfg.ARCHIVE_FILES):
        dirs.archive_files(cfg.OUTPUT_FOLDER, cfg.ARCHIVE_OUTPUT_FOLDER) 
        dirs.archive_files(cfg.DEBUG_FOLDER, cfg.ARCHIVE_DEBUG_FOLDER, copy_only = False)       
        #dirs.archive_files(cfg.LOG_FOLDER, cfg.ARCHIVE_LOG_FOLDER)

    Result = namedtuple('Result', 
    ['Sudoku'])

    process_result = Result(Sudoku=0) 

    if 'Sudoku'.upper() in map(lambda a: a.upper(), args.components):
        print('Start to process Sudoku logic')
        process_result = process_result._replace(Sudoku=sudoku.run())

    #check the result for error

    _result = dirs.check_log_file_result(cfg.LOG_PATH)
    _result = _result + ': ' + ';'.join(['%s=>%s' % (key, value) for (key, value) in process_result._asdict().items()])
    _title = "{0} process - {1}".format(cfg.APP_NAME, _result)
    print(_title)
    _email_body = dirs.htmlfy_log_file(cfg.LOG_PATH)
    emails.send_SMTP_mail(cfg.SENDER, cfg.RECEIVERS, _title, _email_body, cfg.CC, [cfg.LOG_PATH])
    
        
       

    
        
