echo off
SET localPath=%~dp0%
echo %localPath%
pushd %localPath%
C:\Anaconda\python.exe valuation.py sudoku
if errorlevel 1 (
   echo Process failed with code %errorlevel%
   exit /b %errorlevel%
)

popd
echo on