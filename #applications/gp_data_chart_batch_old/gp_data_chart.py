from __future__ import division
import os, sys, inspect
import logging
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mpd
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
import matplotlib.ticker as ticker
import matplotlib.cm as cm
import math as math
import numpy as np
import pandas as pd
from datetime import date
import data.datatools as tools
from dateutil.relativedelta import relativedelta
#need to register the converter in order to fix the breaking change from pandas 0.21
#import pandas.plotting._converter as pandacnv
#pandacnv.register()

class gp_data_chart_pack(object):


    def __init__(self,debug_folder, debug=False):
        self._debug_folder = debug_folder
        self._debug = debug
        mpl.rcParams['font.family'] =  'sans-serif'
        logging.debug("Done creating gp_data_chart_pack object")


    ##########################################################################################################################
    ##### bottolm level API
    ##########################################################################################################################

    def divid_by_sign(cls, s, pos=True):
        s = s.fillna(0)
        if pos:
            return s.apply(lambda v: v if v >= 0 else 0)
        else:
            return s.apply(lambda v: v if v <= 0 else 0)


    ##########################################################################################################################
    ##### bottolm level API
    ##########################################################################################################################


    def hide_top_left(cls,ax):
        # hide the top left borders
        ax.spines['left'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('right')
        ax.yaxis.tick_right()

    def hide_top_right(cls, ax):
        # hide the top left borders
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')


    def hide_top(cls, ax):
        # hide the top left borders
        ax.spines['top'].set_visible(False)
        ax.xaxis.set_ticks_position('bottom')


    def plot_ts_lines(cls,
                      ax,
                      idx,
                      data,
                      label,
                      fmt,
                      legend_pos=None,
                      legend_ncol=1,
                      legend_font_size=12,
                      annotate_value=True,
                      annotate_font_size=18,
                      **kwargs):

        """
        A base function plots time series in lines!
        plot_pd_ts(df.index.values, df.col,'b-o')
        """

        min_x = idx.min()
        max_x = idx.max()

        ax.plot_date(idx, data, fmt, label=label, **kwargs)
        ax.set_xlim([min_x, max_x])

        x = max_x
        y = data[x]

        if annotate_value:
            ax.annotate(str(round(y, 2)),
                        (mdates.date2num(x), y),
                        xytext=(12, np.sign(y) * 5.0 + 0.5 * y),
                        color=kwargs.get('color', 'black'),
                        textcoords='offset points',
                        arrowprops=dict(arrowstyle='-|>', color=kwargs.get('color', 'black')),
                        fontsize=annotate_font_size)

        # ax.text(x, y * (1 + 0.05 * y) , round(y,2), color=kwargs.get('color','black'))

        if legend_pos is None:
            pass
        else:
            if len(data.dropna()) > 0:
                ax.legend(loc=legend_pos, fancybox=True, framealpha=0, ncol=legend_ncol,
                          prop={'size': legend_font_size})
            else:
                r = mpl.patches.Rectangle((0, 0), 1, 1, fill=False, edgecolor='none', visible=False)
                ax.legend([r], [label], loc=legend_pos, fancybox=True, framealpha=0, ncol=legend_ncol,
                          prop={'size': legend_font_size})

    def plot_stack_area(cls, x, ys, ax, reset_color_cycle=False, series_names=None, legend_pos=None, legend_ncol=1,
                        legend_font_size=12, **kwargs):

        min_x = x.min()
        max_x = x.max()

        sp = ax.stackplot(x, ys, linewidth=0.0, baseline='zero', **kwargs)

        ax.set_xlim([min_x, max_x])

        ax.xaxis.grid(True)
        ax.yaxis.grid(True)

        if reset_color_cycle:
            # ax.set_color_cycle(None)
            ax.set_prop_cycle(None)

        if series_names is not None:
            # ax.set_color_cycle(None)
            legend_proxy = [mpl.patches.Rectangle((0, 0), 0, 0, facecolor=pol.get_facecolor()[0], lw=0) for pol in sp]
            ax.legend(legend_proxy, series_names, loc=legend_pos, fancybox=True, framealpha=0, ncol=legend_ncol,
                      prop={'size': legend_font_size})

    ##########################################################################################################################
    ##### AX level API
    ##########################################################################################################################

    def plot_gp_TE_chart_footer_ax(cls,
                                      ax,
                                      **kwargs
                                      ):

        # axes coordinates
        # (0, 0) bottom left
        # (0.5, 0.5) center,
        # (1.0, 1.0) top right
        # clean up

        ax.axis('off')

        date = kwargs.get('date', '')

        ax.text(0.01,
                0.7,
                'Source: AMPCI Global Fixed Income',
                verticalalignment='top',
                horizontalalignment='left',
                transform=ax.transAxes,
                color='black',
                fontsize=8,
                fontstyle='italic'
                )

        ax.text(0.01,
                0.3,
                'As of: ' + date,
                verticalalignment='top',
                horizontalalignment='left',
                transform=ax.transAxes,
                color='black',
                fontsize=8,
                fontstyle='italic'
                )


    def plot_gp_RATES_chart_footer_ax(cls,
                                      ax,
                                      **kwargs
                                      ):
        # axes coordinates
        #(0, 0) bottom left
        #(0.5, 0.5) center,
        #(1.0, 1.0) top right
        #clean up

        ax.axis('off')

        date = kwargs.get('date','')

        ax.text(0.01,
                0.7,
                'Source: AMPCI Global Fixed Income',
                verticalalignment='top',
                horizontalalignment='left',
                transform=ax.transAxes,
                color='black',
                fontsize=8,
                fontstyle='italic'
                )

        ax.text(0.01,
                0.3,
                'As of: ' + date,
                verticalalignment='top',
                horizontalalignment='left',
                transform=ax.transAxes,
                color='black',
                fontsize=8,
                fontstyle='italic'
                )


    def plot_gp_CREDIT_chart_footer_ax(cls,
                                      ax,
                                      **kwargs
                                      ):

        # axes coordinates
        # (0, 0) bottom left
        # (0.5, 0.5) center,
        # (1.0, 1.0) top right
        # clean up

        ax.axis('off')

        date = kwargs.get('date', '')

        ax.text(0.01,
                0.7,
                'Source: AMPCI Global Fixed Income',
                verticalalignment='top',
                horizontalalignment='left',
                transform=ax.transAxes,
                color='black',
                fontsize=8,
                fontstyle='italic'
                )

        ax.text(0.01,
                0.3,
                'As of: ' + date,
                verticalalignment='top',
                horizontalalignment='left',
                transform=ax.transAxes,
                color='black',
                fontsize=8,
                fontstyle='italic'
                )


    def plot_gp_RATES_chart_content_ax(cls,
                                       ax,
                                       df,
                                       fund_name,
									   plot_aucurve = True
                                       ):

        def trans_ylim(fund_name):


            dict = {
				"SMPSUNAF": [(-0.5, 1),(-0.5, 1)],
				"SMPSUNSF": [(-0.5, 1),(-1, 0.5)],
				"SMPSUNRF": [(-0.5, 1),(-0.5, 1)],
                "ABDFDAMP": [(-2, 3),(-2, 2)],
                "ACIACBND": [(0, 1),(0, 1)],
                "ACIFRIFD": [(-0.2, 0.5),(-0.2, 0.5)],
                "ACIMACSF": [(-6, 10),(-8, 8)],
                "ACIWASFI": [(-1, 1),(-1, 1)],
                "ACIWSABF": [(-0.5, 1),(-0.5, 1)],
                "IWI2ACI": [(-1, 1.5),(-0.7, 1.1)],
                "IWIGACI": [(0.8, 1.4),(0.2, 0.4)],
                "NMFAUAC": [(-0.5, 0.5),(-0.5, 0.5)],
                "NMFAUCP": [(-1, 2),(-1, 2)],
                "NMFAUWGF": [(-0.5, 1),(-0.5, 1)],
                "SMPAAIB": [(-1, 0.5),(-1, 0.5)],
                "SMPACSC": [(-1, 1),(-1, 1)],
                "SMPADFI": [(-0.8, 0.8),(-1.5, 0.5)],
                "SMPAVANT": [(-0.25, 0.25),(-0.25, 0.25)],
                "SMPAVSU": [(-1, 1),(-1, 1)],
                "SMPDAIFI": [(-0.5, 0.5),(-0.5, 0.5)],
                "SMPDKAFI": [(-0.5, 0.5),(-0.5, 0.5)],
                "SMPGEFI": [(-1, 1),(-1, 1)],
                "SMPLGSFI": [(-1, 1), (-1, 1)],
                "SMPMAHIB": [(-0.4, 0.2),(-0.7, -0.2)],
                "SMPNABMF": [(-0.5, 1),(-1, 1)],
                "SMPRABF": [(-0.8, 0.8),(-0.6, 0.4)],
                "SMPSSFAB": [(-0.5, 1),(-0.5, 1)],
                "SMPSSFCB": [(-0.5, 0.5),(-0.5, 0.5)],
                "SMPWRKCV": [(-2, 1),(-2, 1)],
                "WEIBACI": [(-2, 2), (-2, 2)],
                "ACZIMAC": [(-4, 4), (-4, 4)],
                "SMPICOFI":[(-4, 4), (-2, 2)],
                "SMPWFI1":[(-1, 1),(-1, 1)]
            }
            if fund_name in dict:
                return dict[fund_name]
            else:
                return [(-2,2),(-2,2)]

        def trans_tick_space(limits):

            span = str(float(limits[1] - limits[0]))
            dict = {
			    '0.2': 0.05,
                '0.4': 0.1,
                '0.5': 0.1,
                '0.6': 0.15,
                '0.7': 0.1,
				'0.75': 0.15,
                '0.8': 0.2,
                '1.0': 0.25,
                '1.5': 0.25,
                '1.6': 0.4,
                '1.8': 0.3,
                '2.0': 0.5,
                '2.5': 0.5,
                '3.0': 0.5,
                '4.0': 1,
                '5.0': 1,
                '6.0': 2,
                '7.0': 1,
                '8.0': 2,
				'10.0':2,
                '12.0':2,
                '16.0':4
            }

            if span in dict:
                return dict[span]
            else:
                return 0.5

        ax_twin = ax.twinx()

        cls.hide_top_right(ax)
        cls.hide_top_left(ax_twin)

        ax.set_ylim(np.array(trans_ylim(fund_name)[0]) + np.array([-0.000001,0.000001]))
        ax.yaxis.grid(False)

        ax_twin.set_ylim(np.array(trans_ylim(fund_name)[1]) + np.array([-0.000001,0.000001]))
        ax_twin.yaxis.grid(False)

        space = trans_tick_space(trans_ylim(fund_name)[0])
        ax.yaxis.set_major_locator(MultipleLocator(space))
        space = trans_tick_space(trans_ylim(fund_name)[1])
        ax_twin.yaxis.set_major_locator(MultipleLocator(space))

        if (fund_name == 'SMPSSFAB'):
            ax.axhline(y=0, color="black", lw=0.8)
        
        rates_legend = 'Total Portfolio Duration' if fund_name == 'ACIACBND' else 'Total Active Duration'

        #ax.spines['bottom'].set_linestyle("dotted")
        #ax.spines['bottom'].set_linewidth(0.8)

        ax.spines['bottom'].set_smart_bounds(True)

        #ax.spines['bottom'].set_color("grey")
        ax.spines['bottom'].set_linewidth(0)
        ax_twin.spines['bottom'].set_linewidth(0)

        ax.spines['left'].set_linewidth(0.5)
        ax_twin.spines['right'].set_linewidth(0.5)


        ax.xaxis.set_major_locator(mpd.MonthLocator(range(1, 13), bymonthday=1, interval=1))
        ax.xaxis.set_major_formatter(mpd.DateFormatter('%b\n%Y'))

        cls.plot_ts_lines(ax,
                          tools.ltrim(df['Active_Duration']).index,
                          tools.ltrim(df['Active_Duration']).fillna(0),
                          rates_legend,
                          #'Duration',
                          "",
                          legend_pos="upper left",
                          legend_ncol=3,
                          annotate_value=False,
                          #annotate_font_size=8,
                          color="#182B49",
                          legend_font_size=10,
                          linewidth=1,
                          ls='-',
                          zorder=10)
						  
        if plot_aucurve:
            cls.plot_ts_lines(ax_twin,
							  tools.ltrim(df['Steepeners_3vs10']).index,
							  tools.ltrim(df['Steepeners_3vs10']).fillna(0),
							  'AU 3s vs 10s Curve',
							  "",
							  legend_pos="upper right",
							  legend_ncol=3,
							  annotate_value=False,
							  # annotate_font_size=8,
							  color="#7ED0E0",
							  legend_font_size=10,
							  linewidth=1,
							  ls='-',
							  zorder=10)

        # disabled as per DC's request
        # cls.plot_ts_lines(ax,
        #                   tools.ltrim(df['TrackingError']).index,
        #                   tools.ltrim(df['TrackingError']).fillna(0),
        #                   'Tracking Error',
        #                   "",
        #                   legend_pos=9,
        #                   legend_ncol=3,
        #                   annotate_value=False,
        #                   # annotate_font_size=8,
        #                   color="#CA6E46",
        #                   legend_font_size=10,
        #                   linewidth=1,
        #                   ls='-',
        #                   zorder=10)




        for label in ax.get_xticklabels():
            label.set_fontsize(8)

        for label in ax_twin.get_xticklabels():
            label.set_fontsize(8)

        for label in ax.get_yticklabels():
            label.set_fontsize(8)

        for label in ax_twin.get_yticklabels():
            label.set_fontsize(8)

        for tick in ax.get_xaxis().get_major_ticks():
            tick.set_pad(4)

        for tick in ax.get_yaxis().get_major_ticks():
            tick.set_pad(8)

        ax.xaxis.set_tick_params(length=0)
        ax_twin.xaxis.set_tick_params(length=0)
        ax.yaxis.set_tick_params(length=2)
        ax_twin.yaxis.set_tick_params(length=2)


        #rotate the axis label for longer period 
        plt.setp(ax.get_xticklabels(), rotation=30, horizontalalignment='left')


        return (ax, ax_twin)



    def plot_gp_CREDIT_chart_content_ax(cls,
                                       ax,
                                       df,
                                       fund_name
                                       ):


        def trans_ylim(fund_name):
            dict = {
			    "SMPICOFI": [(-1, 1.5),(-0.5, 1.5)],
				"SMPSUNAF": [(-0.5, 1),(-0.5, 1)],
				"SMPSUNSF": [(-0.5, 1),(-0.5, 1)],
				"SMPSUNRF": [(-0.5, 1),(-0.5, 1)],
                "ABDFDAMP": [(-0.2, 0.4), (-0.2, 0.8)],
                "ACIACBND": [(0, 4), (0,  4)],
                "ACICRDSF": [(-2, 6), (-4,12)],
                "ACIFRIFD": [(1, 3), (1.5, 4.5)],
                "ACIMACSF": [(-1, 1), (-2, 2)],
                "ACIWASFI": [(0.5, 2.5), (0.5, 2.5)],
                "ACIWSABF": [(-0.5, 1.5), (-0.5, 2)],
                "IWI2ACI": [(-0.1, 0.1), (-0.1, 0.3)],
                "IWIGACI": [(0.5, 3.5), (0.5, 3.5)],
                "NMFAUAC": [(1, 2.5), (1, 2.5)],
                "NMFAUCP": [(0.5, 2.5), (0.5, 2.5)],
                "NMFAUWGF": [(-0.1, 0.1), (-0.1, 0.1)],
                "SMPAAIB": [(-2, 2), (-2, 2)],
                "SMPACSC": [(-0.5, 1.5), (-0.5, 1.5)],
                "SMPADFI": [(0.4, 1.6), (0.4, 1.6)],
                "SMPAVANT": [(1, 2.5), (1, 2.5)],
                "SMPAVSU": [(0, 1.5), (0, 1.5)],
                "SMPDAIFI": [(0.5, 1.5), (0.5, 1.5)],
                "SMPDKAFI": [(0, 1.5), (0, 1.5)],
                "SMPGEFI": [(0, 1.5), (0, 1.5)],
                "SMPLGSFI": [(0, 1.75), (0.5, 1.75)],
                "SMPLIFAB": [(0, 2), (0, 2)],
                "SMPMAHIB": [(-1, 1), (-1, 1)],
                "SMPNABMF": [(0.5, 2), (0.5, 2)],
                "SMPRABF": [(0.5, 1.5), (0.5, 1.5)],
                "SMPSSFAB": [(0, 0.2), (0, 0.2)],
                "SMPSSFCB": [(-1, 1), (-1, 1)],
                "SMPWRKCV": [(-1.5, 1.5), (-1.5, 1.5)],
                "WEIBACI": [(-1, 1),(-1, 1)],
                "ACIWACSF": [(-1, 4), (-1, 4)],
                "ACIWALFO": [(0, 1.5), (0, 1.5)],
                "SMPWFI1": [(0, 2), (0, 2)],
                
            }

            if fund_name in dict:
                return dict[fund_name]
            else:
                return [(-1, 3), (-1, 3)]

        def trans_tick_space(limits):

            span = str(float(limits[1] - limits[0]))
            dict = {
                '0.2': 0.05,
                '0.3': 0.1,
                '0.4': 0.1,
                '0.5': 0.1,
                '0.6': 0.1,
                '0.7': 0.1,
                '0.8': 0.2,
                '0.9': 0.3,
                '1.0': 0.25,
                '1.2': 0.2,
                '1.25': 0.25,
                '1.75': 0.25,
                '1.5': 0.5,
                '2.0': 0.5,
                '2.5': 0.5,
                '3.0': 0.5,
                '4.0': 1,
                '5.0': 2
            }

            if span in dict:
                return dict[span]
            else:
                return 0.5

        ax_twin = ax.twinx()

        cls.hide_top_right(ax)
        cls.hide_top_left(ax_twin)

        ax.set_ylim(np.array(trans_ylim(fund_name)[0]) + np.array([-0.000001,0.000001]))
        ax.yaxis.grid(False)

        ax_twin.set_ylim(np.array(trans_ylim(fund_name)[1]) + np.array([-0.000001,0.000001]))
        ax_twin.yaxis.grid(False)

        space = trans_tick_space(trans_ylim(fund_name)[0])
        ax.yaxis.set_major_locator(MultipleLocator(space))

        space = trans_tick_space(trans_ylim(fund_name)[1])
        ax_twin.yaxis.set_major_locator(MultipleLocator(space))


        #ax.yaxis.set_major_locator(MultipleLocator(1))
        #ax_twin.yaxis.set_major_locator(MultipleLocator(2))

        #ax.axhline(y=0, color="grey", linewidth=0.5, ls=':')

        #ax.spines['bottom'].set_linestyle("dotted")
        #ax.spines['bottom'].set_linewidth(0.8)

        ax.spines['bottom'].set_smart_bounds(True)
        #ax.spines['bottom'].set_color("grey")

        ax.spines['bottom'].set_linewidth(0)
        ax_twin.spines['bottom'].set_linewidth(0)

        ax.spines['left'].set_linewidth(0.5)
        ax_twin.spines['right'].set_linewidth(0.5)

        ax.xaxis.set_major_locator(mpd.MonthLocator(range(1, 13), bymonthday=1, interval=1))
        ax.xaxis.set_major_formatter(mpd.DateFormatter('%b\n%Y'))


        cls.plot_ts_lines(ax,
                          tools.ltrim(df['CreditSpreadDurationContribution'].fillna(0)).index,
                          tools.ltrim(df['CreditSpreadDurationContribution'].fillna(0)),
                          'Credit Spread Duration',
                          "",
                          legend_pos="upper left" ,
                          legend_ncol=3,
                          annotate_value=False,
                          # annotate_font_size=8,
                          color="#182B49",
                          legend_font_size=10,
                          linewidth=1,
                          ls='-',
                          zorder=10)

        cls.plot_ts_lines(ax_twin,
                          tools.ltrim(df['DxS']).index,
                          tools.ltrim(df['DxS']).fillna(0),
                          'DxS',
                          "",
                          legend_pos="upper right",
                          legend_ncol=3,
                          annotate_value=False,
                          # annotate_font_size=8,
                          color="#7ED0E0",
                          legend_font_size=10,
                          linewidth=1,
                          ls='-',
                          zorder=10)

        ##disabled as per DC's request
        # cls.plot_ts_lines(ax,
        #                   tools.ltrim(df['SwapBeta']).index,
        #                   tools.ltrim(df['SwapBeta']).fillna(0),
        #                   'Swap Beta',
        #                   "",
        #                   legend_pos=9,
        #                   legend_ncol=3,
        #                   annotate_value=False,
        #                   # annotate_font_size=8,
        #                   color="#CA6E46",
        #                   legend_font_size=10,
        #                   linewidth=1,
        #                   ls='-',
        #                   zorder=10)




        for label in ax.get_xticklabels():
            label.set_fontsize(8)

        for label in ax_twin.get_xticklabels():
            label.set_fontsize(8)

        for label in ax.get_yticklabels():
            label.set_fontsize(8)

        for label in ax_twin.get_yticklabels():
            label.set_fontsize(8)

        for tick in ax.get_xaxis().get_major_ticks():
            tick.set_pad(4)

        for tick in ax.get_yaxis().get_major_ticks():
            tick.set_pad(8)

        for tick in ax_twin.get_yaxis().get_major_ticks():
            tick.set_pad(8)

        ax.xaxis.set_tick_params(length=0)
        ax_twin.xaxis.set_tick_params(length=0)
        ax.yaxis.set_tick_params(length=2)
        ax_twin.yaxis.set_tick_params(length=2)

        #rotate the axis label for longer period 
        plt.setp(ax.get_xticklabels(), rotation=30, horizontalalignment='left')

        return (ax, ax_twin)



    def plot_gp_TE_chart_content_ax(cls,
                                     ax,
                                     df,
                                     fund_name
                                   ):

        def trans_ylim(fund_name):
            dict = {
				"SMPSUNAF": [(-0.5, 2),(-0.5, 2)],
				"SMPSUNSF": [(-0.5, 2),(-0.5, 2)],
				"SMPSUNRF": [(-0.5, 2),(-0.5, 2)],
                "ACICRDSF": [(-0.5, 2), (-0.25, 1.0)],
                "SMPSSFAB": [(-0.5, 2), (-0.25, 1.0)],
                "WEIBACI": [(-0.5, 2), (0, 2)],
                "ABDFDAMP": [(-0.5, 2), (0, 1.5)],
                "IWI2ACI": [(-0.5, 2), (0, 0.8)],
                "ACIMACSF": [(-0.5, 2), (0.5, 4.0)],
                "SMPAVANT": [(-0.5, 2), (0, 0.5)],
                "SMPGEFI":[(-0.5, 2), (0, 0.5)],
                "ACIACBND": [(-0.5, 2), (0, 0.8)],
                "ACIFRIFD": [(-0.5, 2), (0, 0.5)],
                "ACIWASFI": [(-0.5, 2), (0, 1)],
                "ACIWSABF": [(-0.5, 2), (0, 0.5)],
                "IWIGACI": [(-0.5, 2), (0, 1)],
                "NMFAUAC": [(-0.5, 2), (0, 0.5)],
                "NMFAUCP": [(-0.5, 2), (0.5, 1.5)],
                "NMFAUWGF": [(-0.5, 2), (0, 0.5)],
                "SMPAAIB": [(-0.5, 2), (0, 0.5)],
                "SMPACSC": [(-0.5, 2), (0, 1)],
                "SMPADFI": [(-0.5, 2), (0, 1)],
                "SMPAVSU": [(-0.5, 2), (0, 0.5)],
                "SMPDAIFI": [(-0.5, 2), (0, 0.5)],
                "SMPDKAFI": [(-0.5, 2), (0, 0.5)],
                "SMPLGSFI": [(-0.5, 2), (0, 0.5)],
                "SMPMAHIB": [(-0.5, 2), (0, 0.5)],
                "SMPNABMF": [(-0.5, 2), (0, 1.25)],
                "SMPRABF": [(-0.5, 2), (0, 0.5)],
                "SMPSSFCB": [(-0.5, 2), (0, 0.5)],
                "SMPWRKCV": [(-0.5, 2), (0, 1.5)],
                "SMPWFI1": [(-0.5, 2), (0, 1)],

            }

            if fund_name in dict:
                return dict[fund_name]
            else:
                return [(-0.5, 2), (-1, 4)]

        def trans_tick_space(limits):

            span = str(float(limits[1] - limits[0]))
            dict = {
                '0.2': 0.05,
                '0.3': 0.1,
                '0.4': 0.1,
                '0.5': 0.1,
                '0.6': 0.1,
                '0.8': 0.2,
                '1.0': 0.25,
                '1.25': 0.25,
                '1.5': 0.5,
                '2.5': 0.5,
				'3.5': 0.7,
                '2.0': 0.5,
                '3.0': 0.5,
                '3.5': 0.5,
                '4.0': 1,
                '5.0': 1,
                '8.0': 2
            }

            if span in dict:
                return dict[span]
            else:
                return 0.5


        ax_twin = ax.twinx()

        cls.hide_top_right(ax)
        cls.hide_top_left(ax_twin)


        ax.spines['bottom'].set_smart_bounds(True)

        ax.spines['bottom'].set_linewidth(0)
        ax_twin.spines['bottom'].set_linewidth(0)

        ax.spines['left'].set_linewidth(0.5)
        ax_twin.spines['right'].set_linewidth(0.5)

        ##################### old

        #ax.set_ylim(trans_ylim(fund_name)[0])
        #ax_twin.set_ylim(trans_ylim(fund_name)[1])

        #ax.yaxis.set_major_locator(MultipleLocator(trans_ylim(fund_name)[2]))
        #ax_twin.yaxis.set_major_locator(MultipleLocator(base=trans_ylim(fund_name)[3]))

        ##################### old

        ##################### new

        ax.set_ylim(np.array(trans_ylim(fund_name)[0]) + np.array([-0.000001, 0.000001]))
        ax.yaxis.grid(False)

        ax_twin.set_ylim(np.array(trans_ylim(fund_name)[1]) + np.array([-0.000001, 0.000001]))
        ax_twin.yaxis.grid(False)

        space = trans_tick_space(trans_ylim(fund_name)[0])
        ax.yaxis.set_major_locator(MultipleLocator(space))

        space = trans_tick_space(trans_ylim(fund_name)[1])
        ax_twin.yaxis.set_major_locator(MultipleLocator(space))

        ##################### new


        ax.xaxis.set_major_locator(mpd.MonthLocator(range(1, 13), bymonthday=1, interval=1))
        ax.xaxis.set_major_formatter(mpd.DateFormatter('%b\n%Y'))

        colors = ['#0075AD', '#7ED0E0', '#BBAE78', '#DCDEC3', '#CD661D', '#000026', '#91B9D0']

        idx = df.index

        Rates_exAUD = cls.divid_by_sign(df.OtherRates, True)
        Rates_AUD = cls.divid_by_sign(df.AUDRates, True)
        Other_Spreads = cls.divid_by_sign(df.OtherSpreads, True)
        Corp_Spreads = cls.divid_by_sign(df.CorpSpreads, True)
        FX = cls.divid_by_sign(df.FX, True)
        Idiosyncratic = cls.divid_by_sign(df.Idiosyncratic, True)
        Others = cls.divid_by_sign(df.Others, True)

        vals = [Rates_AUD, Rates_exAUD, Other_Spreads, Corp_Spreads, FX, Idiosyncratic, Others]

        names = ['AUD Rates', 'ex AUD Rates', 'Other Spread', 'Credit Spread', 'FX', 'Idiosyncratic', 'Others']

        cls.plot_stack_area(idx, vals, ax, reset_color_cycle=True, series_names=names, colors=colors,
                            legend_font_size=10, legend_pos="upper left", legend_ncol=3)

        logging.info("Finished drawing the postive half of the TE Chart")

        Rates_exAUD = cls.divid_by_sign(df.OtherRates, False)
        Rates_AUD = cls.divid_by_sign(df.AUDRates, False)
        Other_Spreads = cls.divid_by_sign(df.OtherSpreads, False)
        Corp_Spreads = cls.divid_by_sign(df.CorpSpreads, False)
        FX = cls.divid_by_sign(df.FX, False)
        Idiosyncratic = cls.divid_by_sign(df.Idiosyncratic, False)
        Others = cls.divid_by_sign(df.Others, False)

        vals = [Rates_AUD, Rates_exAUD, Other_Spreads, Corp_Spreads, FX, Idiosyncratic, Others]

        names = ['AUD Rates', 'ex AUD Rates', 'Other Spread', 'Credit Spread', 'FX', 'Idiosyncratic', 'Others']

        cls.plot_stack_area(idx, vals, ax, reset_color_cycle=True, series_names=names, colors=colors,
                            legend_font_size=10, legend_pos="upper left", legend_ncol=3)

        logging.info("Finished drawing the negative half of the TE Chart")

        cls.plot_ts_lines(ax_twin,
                          idx,
                          df.TotalLongTermTE,
                          "Total Long term TE (RHS)",
                          "",
                          legend_pos="upper right",
                          legend_ncol=2,
                          color="#FF0000",
                          legend_font_size=10,
                          linewidth=1,
                          ls='-',
                          annotate_value=False)


        ax.yaxis.grid(False)
        ax_twin.yaxis.grid(False)
        ax.xaxis.grid(False)
        ax_twin.xaxis.grid(False)


        for label in ax.get_xticklabels():
            label.set_fontsize(10)

        for label in ax_twin.get_xticklabels():
            label.set_fontsize(10)

        for label in ax.get_yticklabels():
            label.set_fontsize(10)

        for label in ax_twin.get_yticklabels():
            label.set_fontsize(10)

        for tick in ax.get_xaxis().get_major_ticks():
            tick.set_pad(4)

        for tick in ax.get_yaxis().get_major_ticks():
            tick.set_pad(8)

        #rotate the axis label for longer period 
        plt.setp(ax.get_xticklabels(), rotation=30, horizontalalignment='left')


        return (ax, ax_twin)
    ##########################################################################################################################
    ##### Plot level API
    ##########################################################################################################################

    def plot_gp_RATES_chart(cls,
                            df,
                            fund_name=None,
                            output_file_name=None,
                            width=12,
                            height=8,
                            output_dir=None,
                            dpi=100):

        logging.info("===> start plot_gp_RATES_chart <===")

        #main title: fig.suptitle
        #subplot's titles: ax.set_title

        plt.close()

        fig1 = plt.figure(figsize=(width, height))
        gs = gridspec.GridSpec(2, 1, height_ratios=[12,1], width_ratios=[1])
        ax_plot0 = plt.subplot(gs[0])
        ax_footer = plt.subplot(gs[1])

        date = df.index.max().strftime('%Y-%m-%d')

        logging.info("Generating RATES chart for {0}".format(fund_name))

        cls.plot_gp_RATES_chart_content_ax(ax_plot0, df, fund_name)
        cls.plot_gp_RATES_chart_footer_ax(ax_footer, date=date)



        if output_file_name is not None and output_dir is not None:
            f = os.path.join(output_dir, output_file_name + "_" + fund_name + ".png")
            logging.info("Output chart as " + f)
            fig1.suptitle("{0} Rates Position".format(fund_name), fontsize = 14, fontweight='bold' )

            #fig1.subplots_adjust(left=0.05, right=0.91, top=0.88, bottom=0.05)
            # left : 0.125 The left side of the subplots of the figure
            # right : 0.9  The right side of the subplots of the figure
            # bottom : 0.1 The bottom of the subplots of the figure
            # top : 0.9 The top of the subplots of the figure
            # wspace : 0.2 The amount of width reserved for blank space between subplots
            # hspace : 0.2 The amount of height reserved for white space between subplots

            #fig1.tight_layout()
            # only set the fig.tight_layout if there is no ax level title

            plt.savefig(f, dpi=dpi)#, bbox_inches='tight')
            plt.close()

        logging.info("===> end plot_gp_RATES_chart <===")

		
	##########################################################################################################################
    ##### Plot level API
    ##########################################################################################################################

    def plot_gp_RATES_chart2(cls,
                            df,
                            fund_name=None,
                            output_file_name=None,
                            width=12,
                            height=8,
                            output_dir=None,
                            dpi=100):

        logging.info("===> start plot_gp_RATES_chart <===")

        #main title: fig.suptitle
        #subplot's titles: ax.set_title

        plt.close()

        fig1 = plt.figure(figsize=(width, height))
        gs = gridspec.GridSpec(2, 1, height_ratios=[12,1], width_ratios=[1])
        ax_plot0 = plt.subplot(gs[0])
        ax_footer = plt.subplot(gs[1])

        date = df.index.max().strftime('%Y-%m-%d')

        logging.info("Generating RATES chart for {0}".format(fund_name))

        cls.plot_gp_RATES_chart_content_ax(ax_plot0, df, fund_name,  plot_aucurve = False)
        cls.plot_gp_RATES_chart_footer_ax(ax_footer, date=date)



        if output_file_name is not None and output_dir is not None:
            f = os.path.join(output_dir, output_file_name + "_" + fund_name + ".png")
            logging.info("Output chart as " + f)
            fig1.suptitle("{0} Rates Position".format(fund_name), fontsize = 14, fontweight='bold' )

            #fig1.subplots_adjust(left=0.05, right=0.91, top=0.88, bottom=0.05)
            # left : 0.125 The left side of the subplots of the figure
            # right : 0.9  The right side of the subplots of the figure
            # bottom : 0.1 The bottom of the subplots of the figure
            # top : 0.9 The top of the subplots of the figure
            # wspace : 0.2 The amount of width reserved for blank space between subplots
            # hspace : 0.2 The amount of height reserved for white space between subplots

            #fig1.tight_layout()
            # only set the fig.tight_layout if there is no ax level title

            plt.savefig(f, dpi=dpi)#, bbox_inches='tight')
            plt.close()

        logging.info("===> end plot_gp_RATES_chart <===")

    def plot_gp_CREDIT_chart(cls,
                            df,
                            fund_name=None,
                            output_file_name=None,
                            width=12,
                            height=10,
                            output_dir=None,
                            dpi=120):

        logging.info("===> start plot_gp_CREDIT_chart <===")

        # main title: fig.suptitle
        # subplot's titles: ax.set_title

        plt.close()

        fig1 = plt.figure(figsize=(width, height))
        gs = gridspec.GridSpec(2, 1, height_ratios=[12, 1], width_ratios=[1])
        ax_plot0 = plt.subplot(gs[0])
        ax_footer = plt.subplot(gs[1])

        date = df.index.max().strftime('%Y-%m-%d')

        logging.info("Generating Credit chart for {0}".format(fund_name))

        cls.plot_gp_CREDIT_chart_content_ax(ax_plot0, df, fund_name)
        cls.plot_gp_CREDIT_chart_footer_ax(ax_footer, date=date)

        if output_file_name is not None and output_dir is not None:
            f = os.path.join(output_dir, output_file_name + "_" + fund_name + ".png")
            logging.info("Output chart as " + f)
            fig1.suptitle("{0} Credit Position".format(fund_name), fontsize=14, fontweight='bold')

            #fig1.subplots_adjust(left=0.05, right=0.91, top = 0.88, bottom = 0.05)
            # left : 0.125 The left side of the subplots of the figure
            # right : 0.9  The right side of the subplots of the figure
            # bottom : 0.1 The bottom of the subplots of the figure
            # top : 0.9 The top of the subplots of the figure
            # wspace : 0.2 The amount of width reserved for blank space between subplots
            # hspace : 0.2 The amount of height reserved for white space between subplots

            #fig1.tight_layout()
            # only set the fig.tight_layout if there is no ax level title

            plt.savefig(f, dpi=dpi, bbox_inches='tight')
            plt.close()

        logging.info("===> end plot_gp_CREDIT_chart <===")


    def plot_gp_RATECREDIT_chart(cls,
                             dfs,
                             fund_name=None,
                             output_file_name=None,
                             width=18,
                             height=8,
                             output_dir=None,
                             dpi=100):

        logging.info("===> start plot_gp_RATECREDIT_chart <===")

        # main title: fig.suptitle
        # subplot's titles: ax.set_title

        plt.close()

        fig1 = plt.figure(figsize=(width, height))
        gs = gridspec.GridSpec(2, 2, height_ratios=[12, 1], width_ratios=[1,1], wspace= 0.18)
        ax_plot1 = plt.subplot(gs[0,0])
        ax_plot2 = plt.subplot(gs[0,1])

        # increase the space between the subplots
        #fig1.subplots_adjust(wspace=6)

        ax_footer = plt.subplot(gs[1,:])

        date = dfs[0].index.max().strftime('%Y-%m-%d')

        logging.info("Generating Credit chart for {0}".format(fund_name))

        ax_pairs1 = cls.plot_gp_CREDIT_chart_content_ax(ax_plot1, dfs[0], fund_name)
        ax_pairs2 = cls.plot_gp_RATES_chart_content_ax(ax_plot2, dfs[1], fund_name)
        cls.plot_gp_CREDIT_chart_footer_ax(ax_footer, date=date)

        ax_plot1.set_title("Credit", fontsize=10)
        ax_plot2.set_title("Rate", fontsize=10)

        for label in ax_pairs1[0].get_xticklabels():
            label.set_fontsize(8)

        for label in ax_pairs1[1].get_xticklabels():
            label.set_fontsize(8)

        for label in ax_pairs1[0].get_yticklabels():
            label.set_fontsize(8)

        for label in ax_pairs1[1].get_yticklabels():
            label.set_fontsize(8)

        for label in ax_pairs2[0].get_xticklabels():
            label.set_fontsize(8)

        for label in ax_pairs2[1].get_xticklabels():
            label.set_fontsize(8)

        for label in ax_pairs2[0].get_yticklabels():
            label.set_fontsize(8)

        for label in ax_pairs2[1].get_yticklabels():
            label.set_fontsize(8)


        if output_file_name is not None and output_dir is not None:
            f = os.path.join(output_dir, output_file_name + "_" + fund_name + ".png")
            logging.info("Output chart as " + f)
            fig1.suptitle("{0} Position".format(fund_name), fontsize=14, fontweight='bold')

            # fig1.subplots_adjust(left=0.05, right=0.91, top = 0.88, bottom = 0.05)
            # left : 0.125 The left side of the subplots of the figure
            # right : 0.9  The right side of the subplots of the figure
            # bottom : 0.1 The bottom of the subplots of the figure
            # top : 0.9 The top of the subplots of the figure
            # wspace : 0.2 The amount of width reserved for blank space between subplots
            # hspace : 0.2 The amount of height reserved for white space between subplots

            # fig1.tight_layout()
            # only set the fig.tight_layout if there is no ax level title

            plt.savefig(f, dpi=dpi, bbox_inches='tight')
            plt.close()

        logging.info("===> end plot_gp_RATECREDIT_chart <===")



    def plot_gp_TE_chart(cls,
                             df,
                             fund_name=None,
                             output_file_name=None,
                             width=12,
                             height=8,
                             output_dir=None,
                             dpi=100):


        logging.info("===> start plot_gp_TE_chart <===")

        # main title: fig.suptitle
        # subplot's titles: ax.set_title

        plt.close()

        fig1 = plt.figure(figsize=(width, height))
        gs = gridspec.GridSpec(2, 1, height_ratios=[12, 1], width_ratios=[1])
        ax_plot0 = plt.subplot(gs[0])
        ax_footer = plt.subplot(gs[1])

        date = df.index.max().strftime('%Y-%m-%d')

        logging.info("Generating TE chart for {0}".format(fund_name))

        cls.plot_gp_TE_chart_content_ax(ax_plot0, df, fund_name)
        cls.plot_gp_TE_chart_footer_ax(ax_footer, date=date)

        if output_file_name is not None and output_dir is not None:
            f = os.path.join(output_dir, output_file_name + "_" + fund_name + ".png")
            logging.info("Output chart as " + f)
            fig1.suptitle("{0} Tracking Error Summary".format(fund_name), fontsize=14, fontweight='bold')

            # fig1.subplots_adjust(left=0.05, right=0.91, top = 0.88, bottom = 0.05)
            # left : 0.125 The left side of the subplots of the figure
            # right : 0.9  The right side of the subplots of the figure
            # bottom : 0.1 The bottom of the subplots of the figure
            # top : 0.9 The top of the subplots of the figure
            # wspace : 0.2 The amount of width reserved for blank space between subplots
            # hspace : 0.2 The amount of height reserved for white space between subplots

            # fig1.tight_layout()
            # only set the fig.tight_layout if there is no ax level title

            plt.savefig(f, dpi=dpi, bbox_inches='tight')
            plt.close()

        logging.info("===> end plot_gp_TE_chart <===")

    def plot_au_tot_twi_chart(cls, 
                              df, 
                              output_file_name=None,
                              width=12,
                              height=8,
                              output_dir=None,
                              dpi=100):

        logging.info("===> start plot_au_tot_twi_chart <===")
        plt.close()

        fig1 = plt.figure(figsize=(width, height))
        gs = gridspec.GridSpec(2, 1, height_ratios=[12, 1], width_ratios=[1])
        ax_plot = plt.subplot(gs[0,0])
        ax_footer = plt.subplot(gs[1,0])

        min_x = df.index.min()
        max_x = df.index.max()

        cls.plot_gp_RATES_chart_footer_ax(ax_footer, date=max_x.strftime(r'%Y-%m-%d'))

        ax_plot.plot(df.index, df, ls = '-', linewidth = 1.5, marker =None)
        all_years = mdates.YearLocator() 
        ax_plot.xaxis.set_major_locator(all_years)
        ax_plot.xaxis.set_major_formatter(mdates.DateFormatter('%Y')) 
        ax_plot.set_xlim([min_x, max_x])
        ax_plot.set_ylim([-2.5, 2.5])
        ax_plot.xaxis.grid(True, which ='major', linewidth=0.2)
        ax_plot.yaxis.grid(True, which ='major', linewidth=0.2)
        ax_plot.spines['top'].set_linewidth(0)
        ax_plot.spines['right'].set_linewidth(0)
        ax_plot.spines['bottom'].set_linewidth(0)
        ax_plot.axhline(y=0, color='black', linewidth = 0.5)
        ax_plot.xaxis.set_ticks_position('none') 
        ax_plot.yaxis.set_major_locator(ticker.MultipleLocator(0.5))
        ax_plot.set_ylabel('AU ToT vs TWI (zscored)')
        ax_plot.yaxis.label.set_size(10)
        
        for label in ax_plot.get_xticklabels():
            label.set_fontsize(8)
            
        for label in ax_plot.get_yticklabels():
            label.set_fontsize(8)
            
        ax_plot.text(0.95, 0.1, 'Currency expensive/tighter financial condition', 
                    verticalalignment='bottom', 
                    horizontalalignment='right',
                    transform=ax_plot.transAxes,
                    fontsize=10)

            
        ax_plot.text(0.95, 0.9, 'Currency cheap/easier financial condition', 
                    verticalalignment='top', 
                    horizontalalignment='right',
                    transform=ax_plot.transAxes,
                    fontsize=10)


        if output_file_name is not None and output_dir is not None:
            f = os.path.join(output_dir, output_file_name + ".png")
            logging.info("Output chart as " + f)
            #fig1.suptitle("{0} Tracking Error Summary".format(fund_name), fontsize=14, fontweight='bold')

            # fig1.subplots_adjust(left=0.05, right=0.91, top = 0.88, bottom = 0.05)
            # left : 0.125 The left side of the subplots of the figure
            # right : 0.9  The right side of the subplots of the figure
            # bottom : 0.1 The bottom of the subplots of the figure
            # top : 0.9 The top of the subplots of the figure
            # wspace : 0.2 The amount of width reserved for blank space between subplots
            # hspace : 0.2 The amount of height reserved for white space between subplots

            # fig1.tight_layout()
            # only set the fig.tight_layout if there is no ax level title

            plt.savefig(f, dpi=dpi, bbox_inches='tight')
            plt.close()

        logging.info("===> end plot_au_tot_twi_chart <===")







