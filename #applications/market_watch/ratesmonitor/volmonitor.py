import pandas as pd
import numpy as np
import os, sys, inspect
from operator import itemgetter
import collections
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from cycler import cycler
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations, chain
from pykalman import KalmanFilter
import xlwings as xw
from pandas.tseries.offsets import DateOffset

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\derivative\pysabr'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\core'
if _p not in sys.path: sys.path.insert(0, _p)



from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper
from statistics.derivative import bs as bs
from statistics.derivative import quantlibutils as qlutils
from statistics.derivative.pysabr import hagan_2002_normal_sabr as nsabr
from statistics.mr import tools as mrt
from core import dirs




from scipy import stats
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, fcluster
import scipy.cluster.hierarchy as cl
from scipy.spatial.distance import pdist, squareform

import QuantLib as ql

pd.set_option('display.expand_frame_repr', False)




########################################################################################
### function
########################################################################################


def read_csv_into_timeframe(file_path, index_col_name='date'):
    data = pd.read_csv(file_path)
    data[index_col_name] = pd.to_datetime(data[index_col_name])
    data = data.set_index('date').ffill()
    return data


def filter_data(data, exps=[], tails=[], strikes=[], cin_f = lambda e, t, s: None, cout_f = lambda e, t, s: None, coutexp_f = lambda e, t, s: f'{e}_{t}_{s}'):
    output = {}    
    for exp in exps:
        for tail in tails:
            for strike in strikes:
                # cin_func turns e, t, s into the input col format
                c_in = cin_f(exp, tail, strike)
                # cout_func turns e, t, s into the input col format
                c_out = cout_f(exp, tail, strike)
                c_out_expected = coutexp_f(exp, tail, strike)

                #make empty column if there is no equivalent data from import
                output[c_out_expected] = data[c_in] if (c_in in data.columns and c_out == c_out_expected) else pd.Series(data= np.nan, index=data.index)
    return pd.DataFrame(output)



def clean_citi_data(df, col_rename_func, col_name_filter_func=None):
    #col 0 is the date
    _df = df[(~ df.iloc[:,0].str.contains('---')) & \
             (~ df.iloc[:,0].str.contains('confidential'))]
    #name the date column
    _df = _df.rename(columns={_df.columns[0]: 'date'})  
    _df['date'] = pd.to_datetime(_df['date'])
    _df = _df.set_index('date')
    # filter col  
    _dfc = _df.columns if col_name_filter_func is None else [ c for c in _df.columns if col_name_filter_func(c)]
    _df = _df[_dfc]
    # rename col    
    _df = _df.rename(col_rename_func, axis=1)    
    return _df


def otm_vol_col_name_cleaner(c):    
    cs = c.split(' ')
    keys = [c.upper() for c in itemgetter(*[5,6,7])(cs)]
    new_c = '{}_{}_{}'.format(*keys)
    return new_c
    

def atm_vol_col_name_cleaner(c):    
    cs = c.split(' ')
    keys = [c.upper() for c in itemgetter(*[0,2])(cs)]
    new_c = '{}_{}'.format(*keys)
    return new_c



def swap_col_name_cleaner(c):
    if 'PAR' in c.upper():
        cs = c.split(' ')        
        new_c = '0Y_{}'.format(cs[1])
        return new_c
    else:            
        cs = c.split(' ')
        keys = [c.upper() for c in itemgetter(*[1,3])(cs)]
        new_c = '{}_{}'.format(*keys)
        return new_c


def calc_premium(vol_df, fwd_df):
    #vol col name is expected to be like '5Y_10Y_+10' indicating 5y exp 10y tail and atm +10bps
    def call_pricer(row):
        #not going to price when vol or fwd is not available or vol is less equal than 0 or strike is negative        
        if (np.isnan(row['vol']) or np.isnan(row['fwd']) or np.isnan(row['otm_spread']) \
            or row['fwd'] + row['otm_spread'] <= 0):
            return np.nan
        else:
            _premium = bs.price_swaption(row['fwd'],
                                        row['fwd'] + row['otm_spread'], #strike 
                                        exp, # t in years
                                        row['vol'] * 100, # vol in bps
                                        payer=True if otm_spread >= 0 else False, 
                                        normal=True)
            return _premium * 100
                                  
    premium_df = {}
    
    for c in vol_df.columns:
        exp, tail, otm_spread = itemgetter(*[0,1,2])(c.split('_'))
        fwd_col = f'{exp}_{tail}'
        exp = bs.convert_to_year(exp)
        tail = bs.convert_to_year(tail)
        otm_spread = float(otm_spread)/100 # in %
        
        #get vol history and fwd history
        
        vol = vol_df[c]
        fwd = fwd_df[fwd_col].reindex(vol.index, axis =0).ffill()
        #strike = fwd + otm_spread
        vol_fwd_df = pd.DataFrame({
            'vol': vol,
            'fwd': fwd,
            })
        vol_fwd_df['otm_spread'] = otm_spread
        vol_fwd_df['exp'] = exp 
                
        premium = vol_fwd_df.apply(call_pricer, axis = 1)        
        
        premium_df[c] = pd.Series(premium.values, vol.index)
               
    return pd.DataFrame(premium_df)

def calc_swaption(vol_df, fwd_df, calc_type_f):
    '''
    Parameters
    ----------
    vol_df : dataframe
        a dataframe of ts of vol. The vol col name is expected to be like 
        '5Y_10Y_+10' indicating 5y exp 10y tail and atm +10bps
    fwd_df : dataframe
        a dataframe of ts of fwd rates. The spread from the vol col name is 
        used with the fwd rate here to calc the strikes 
    calc_type_f : function
        bs.price_swaption(forward, strike, expiry, vol, payer=True, normal=True)
        bs.swaption_detla
        bs.swaption_gamma
        bs.swaption_vega
        bs.swaption_theta
        
    Returns
    -------
    dataframe
        a dataframe ts of swaption price or greeks.

    '''
    #vol col name is expected to be like '5Y_10Y_+10' indicating 5y exp 10y tail and atm +10bps
    def call_bs(row, calc_type_f, normal_vol=True):
        #not going to price when vol or fwd is not available or vol is less equal than 0 or strike is negative        
        if (np.isnan(row['vol']) or np.isnan(row['fwd']) or np.isnan(row['otm_spread']) \
            or row['fwd'] + row['otm_spread'] <= 0):
            return np.nan
        else:
            _bs_out = calc_type_f(row['fwd'],
                                   row['fwd'] + row['otm_spread'], #strike
                                   exp, # t in years 
                                   row['vol'], # vol in bps 
                                   payer=True if otm_spread >= 0 else False,
                                   normal=normal_vol)
            return _bs_out
                                  
    bs_df = {}
    
    for c in vol_df.columns:
        exp, tail, otm_spread = itemgetter(*[0,1,2])(c.split('_'))
        fwd_col = f'{exp}_{tail}'
        exp = bs.convert_to_year(exp)
        tail = bs.convert_to_year(tail)
        otm_spread = float(otm_spread)/100 # in %
        
        #get vol history and fwd history
        
        vol = vol_df[c]
        fwd = fwd_df[fwd_col].reindex(vol.index, axis =0).ffill()
        #strike = fwd + otm_spread
        vol_fwd_df = pd.DataFrame({
            'vol': vol,
            'fwd': fwd,
            })
        vol_fwd_df['otm_spread'] = otm_spread
        vol_fwd_df['exp'] = exp 
        
        # vol_fwd_df has vol, fwd, strike spread and exp columns        
        bs_output = vol_fwd_df.apply(lambda r: call_bs(r, calc_type_f, normal_vol=True), axis = 1)        
        
        bs_df[c] = pd.Series(bs_output.values, vol.index)
               
    return pd.DataFrame(bs_df)
         



def interpolate_strike_space(vol_surface, rates, strikes_in=[], strikes_fit=[], 
                             strikes_fit_in_spread = True):
    structures = set(['_'.join(c.split('_')[0:2]) for c in vol_surface.columns])
    
    _swap = rates.reindex(vol_surface.index, axis=0).ffill()
    _beta = 0 
    _shift = 0
    output = []
    i = 0
    for structure in structures:
        #vol surface history        
        structure_names_with_fitted_strikes = [ f'{structure}_{s}' for s in strikes_fit] 
                
        fitted_structures = {}
        
        _structure_vol_surface = vol_surface[[f'{structure}_{s}' for s in strikes]]   
        _t = bs.convert_to_year(structure.split('_')[0])
        for day in _structure_vol_surface.index:
            _fwd = _swap.loc[day][structure]/100 #in %
            vols = _structure_vol_surface.loc[day]/10000 #in %
            vol_vals = vols.values
            vol_strikes = [float(s)/10000 + _fwd for s in strikes_in]
            vol_strikes_to_fit = [float(s)/10000 + _fwd for s in strikes_fit] \
                if strikes_fit_in_spread else strikes_fit
            #print(f'{vols}: vols,{vol_vals}: vol_vals, {vol_strikes}: vol_strikes, {_t}:_t, {_fwd}:_fwd')
            sabr_model = nsabr.Hagan2002NormalSABR(_fwd, _shift, _t, beta=_beta)            
            (_alpha, _rho, _volvol) = sabr_model.fit(vol_strikes, vol_vals)
            vol_vals_fit = [ nsabr.normal_vol(vsf, _fwd, _t, _alpha, _beta, _rho, _volvol) for vsf in vol_strikes_to_fit]
            fitted_structures[day] = pd.Series(data=vol_vals_fit, index=structure_names_with_fitted_strikes)
                        
        output.append(pd.DataFrame(fitted_structures).transpose())  
        
    return pd.concat(output, axis=1) * 100



def plot_fit(strikes, vols, fitted_strikes, fitted_vols, _ax):
    s = pd.Series(data=vols, index=strikes)
    s.name = 'original'
    fs = pd.Series(data=fitted_vols, index=fitted_strikes)
    fs.name = 'fit'
    _ax = s.plot(legend = True)
    return _ax
    


        

# There are two data sources: BBG and Citi.
# BBG data source has more country but no OTM
# Citi has 2 countries (so far) but it has OTM

#step 1 - load data from both source and convert them into the same formats : ATM and OTM
#step 2 - load rates data from both sources and convert them into the same formats.
#step 3 - select source. from step 4 onwards all the function should be generic for both source
#step 4 - derive data: build the risk engine to calc carry roll, dv01 
#step 5 - derive mid curve vol (applies to both ATM and OTM)
#step 6 - convert vol into premium % of notional and bps (applies to both ATM and OTM)
#step 6 - derive data: Rvol and Ivol/RVol ratio (applies to ATM)
#step 7 - derive data: Exp/Exp ratio for tail (applies to ATM and OTM)
#step 8 - derive data: tail/tail ratio for exp (applies to ATM and OTM)
#step 9 - derive data: risk reversal and BF (applies to OTM)
#
# what are other key metrics? especially if they dont fit in table format
#
#step 10 - derive data: transform data from previous 8 steps into momentum or zscore
#step 11 - convert all data for heatmap or chart
#step 12 - display 

#so far there is no creating strategies and key metrics
#what should the key metrics be? vol vs curve steepness, vol relative movement? implied vs realised?




#exp = ['3M', '6M', '1Y', '2Y', '3Y', '5Y', '10Y'] 


exp = ['1M', '3M', '6M', '1Y',  '2Y', '3Y', '4Y', '5Y', '10Y'] # not all exp are available in citi
tails = ['1Y','2Y', '3Y', '5Y','7Y', '10Y', '20Y', '30Y']  
strikes = ['+50', '+25','+10','+0','-10', '-25','-50']
interpolated_strikes = [ f'+{c}' if c >=0 else f'{c}' for c in  np.arange(-50, 55, 5)[::-1]] # 50 to -50 with 5bps a stop


class Qlcfg(object):pass;
ql_cfg = Qlcfg() 
 

########################################################################################
### load data from both sources
########################################################################################

#################
### BBG
#################

filedate = '2021-04-28'

usd_bbg_data = pd.read_csv('C:/Temp/test/market_watch/staging/'+ filedate + '_USD_historical.csv')  
usd_bbg_data['date'] = pd.to_datetime(usd_bbg_data['date'])
usd_bbg_data = usd_bbg_data.set_index('date').ffill()

swap_bbg_data = pd.read_csv('C:/Temp/test/market_watch/staging/'+ filedate + '_SWAP_ALL_V2_historical.csv')  
swap_bbg_data['date'] = pd.to_datetime(swap_bbg_data['date'])
swap_bbg_data = swap_bbg_data.set_index('date').ffill()

vol_bbg_data = pd.read_csv('C:/Temp/test/market_watch/staging/'+ filedate + '_VOL_historical.csv')  
vol_bbg_data['date'] = pd.to_datetime(vol_bbg_data['date'])
vol_bbg_data = vol_bbg_data.set_index('date').ffill()



# ivol                
usd_bbg_vol_cin_f = lambda e, t, s: f'IVOL_{e}_{t}'
usd_bbg_vol_cout_f = lambda e, t, s: f'{e}_{t}_+0'

aud_bbg_vol_cin_f = lambda e, t, s: f'AUD_{e}_{t}'
aud_bbg_vol_cout_f = lambda e, t, s: f'{e}_{t}_+0'


bbg_ivol_data = {
    'usd': filter_data(usd_bbg_data, exps = exp, tails = tails, strikes = ['+0'], 
                       cin_f = usd_bbg_vol_cin_f, cout_f =usd_bbg_vol_cout_f),
    'aud': filter_data(vol_bbg_data, exps = exp, tails = tails, strikes = ['+0'], 
                       cin_f = aud_bbg_vol_cin_f, cout_f =aud_bbg_vol_cout_f)
    }

# =============================================================================
# # no otm hence 72 columns each
# assert len(bbg_ivol_data['usd'].dropna(axis=1,how='all').columns) == 72
# assert len(bbg_ivol_data['aud'].dropna(axis=1,how='all').columns) == 72
# 
# =============================================================================

# swap

ivol_swap_coutexp_f =  lambda e, t, s: f'{e}_{t}'
usd_bbg_ivol_swap_cin_f = lambda e, t, s: f'USD_{e}_{t}'
usd_bbg_ivol_swap_cout_f = lambda e, t, s: f'{e}_{t}'
aud_bbg_ivol_swap_cin_f = lambda e, t, s: f'AUD_{e}_{t}'
aud_bbg_ivol_swap_cout_f = lambda e, t, s: f'{e}_{t}'


bbg_ivol_swap_data = {
    'usd': filter_data(usd_bbg_data, exps = exp, tails = tails, strikes = ['+0'], 
                       cin_f = usd_bbg_ivol_swap_cin_f, cout_f =usd_bbg_ivol_swap_cout_f, 
                       coutexp_f=ivol_swap_coutexp_f),
    'aud': filter_data(swap_bbg_data, exps = exp, tails = tails, strikes = ['+0'], 
                       cin_f = aud_bbg_ivol_swap_cin_f, cout_f =aud_bbg_ivol_swap_cout_f, 
                       coutexp_f=ivol_swap_coutexp_f)
}

# =============================================================================
# 
# # 72 columns each
# assert len(bbg_ivol_swap_data['usd'].dropna(axis=1,how='all').columns) == 72
# assert len(bbg_ivol_swap_data['aud'].dropna(axis=1,how='all').columns) == 72
# 
# 
# =============================================================================

#tis needs to be stored 

bbg_swaption_premium_data = {
    'usd': calc_premium(bbg_ivol_data['usd'], bbg_ivol_swap_data['usd']),  #make saure the vol is converted to %
    'aud': calc_premium(bbg_ivol_data['aud'], bbg_ivol_swap_data['aud'])
    }


bbg_swaption_data_premium = {
    'usd': calc_swaption(bbg_ivol_data['usd'], bbg_ivol_swap_data['usd'], bs.price_swaption),  #make saure the vol is converted to %
    'aud': calc_swaption(bbg_ivol_data['aud'], bbg_ivol_swap_data['aud'], bs.price_swaption)
    }

bbg_swaption_data_gamma = {
    'usd': calc_swaption(bbg_ivol_data['usd'], bbg_ivol_swap_data['usd'], bs.swaption_gamma),  #make saure the vol is converted to %
    'aud': calc_swaption(bbg_ivol_data['aud'], bbg_ivol_swap_data['aud'], bs.swaption_gamma)
    }

bbg_swaption_data_vega = {
    'usd': calc_swaption(bbg_ivol_data['usd'], bbg_ivol_swap_data['usd'], bs.swaption_vega),  #make saure the vol is converted to %
    'aud': calc_swaption(bbg_ivol_data['aud'], bbg_ivol_swap_data['aud'], bs.swaption_vega)
    }

bbg_swaption_data_theta = {
    'usd': calc_swaption(bbg_ivol_data['usd'], bbg_ivol_swap_data['usd'], bs.swaption_theta),  #make saure the vol is converted to %
    'aud': calc_swaption(bbg_ivol_data['aud'], bbg_ivol_swap_data['aud'], bs.swaption_theta)
    }






##############
# create risk engines
##############


#OIS CURVE inputs

bbg_ois_curve_data = {
    'aud': pd.concat([swap_bbg_data[['AUD_0Y_1D']].rename(lambda c: '1D', axis=1),
                   swap_bbg_data[[c for c in swap_bbg_data.columns if 'AUDOIS' in c]].rename(lambda c: c.replace('AUDOIS_0Y_',''), axis=1)],
                  axis=1),
    'usd': usd_bbg_data[[c for c in usd_bbg_data.columns if 'SOFR_' in c]].rename(lambda c: c.replace('SOFR_',''), axis=1)

}

#swap curve inputs

bbg_swap_curve_data = {
    'aud': swap_bbg_data[[c for c in swap_bbg_data.columns if 'AUD_0Y' in c]].rename(lambda c: c.replace('AUD_0Y_',''), axis=1),
    'usd': usd_bbg_data[[c for c in usd_bbg_data.columns if 'LIBOR_' in c]].rename(lambda c: c.replace('LIBOR_',''), axis=1)
}


usd_3m_libor_engine = qlutils.CurveRiskEngine(bbg_swap_curve_data['usd'].iloc[-60:], 
                                              bbg_ois_curve_data['usd'].iloc[-60:], 
                                              ql.UnitedStates())


aud_bbsw_engine = qlutils.CurveRiskEngine(bbg_swap_curve_data['aud'].iloc[-60:], 
                                              bbg_ois_curve_data['aud'].iloc[-60:], 
                                              ql.Australia())






################
### Citi
################

#load new citi data

usd_citi_otm_data_new = pd.read_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/raw/USD_OTM.csv', skiprows=2)  
usd_citi_otm_data_new = clean_citi_data(usd_citi_otm_data_new, otm_vol_col_name_cleaner).ffill()

usd_citi_swap_data_new = pd.read_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/raw/USD_SWAP.csv', skiprows=2)  
usd_citi_swap_data_new = clean_citi_data(usd_citi_swap_data_new, swap_col_name_cleaner, col_name_filter_func=lambda c: 'Swap Rate'.upper() in c.upper()).ffill()

aud_citi_otm_data_new = pd.read_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/raw/AUD_OTM.csv', skiprows=2)  
aud_citi_otm_data_new = clean_citi_data(aud_citi_otm_data_new, otm_vol_col_name_cleaner).ffill()

aud_citi_otm_data_prt2_new = pd.read_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/raw/AUD_OTM_PRT2.csv', skiprows=2)  
aud_citi_otm_data_prt2_new = clean_citi_data(aud_citi_otm_data_prt2_new, otm_vol_col_name_cleaner).ffill()

aud_citi_otm_data_new = mrt.merge_dataframess(aud_citi_otm_data_new, aud_citi_otm_data_prt2_new, axis=1)

aud_citi_swap_data_new = pd.read_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/raw/AUD_SWAP.csv', skiprows=2)  
aud_citi_swap_data_new = clean_citi_data(aud_citi_swap_data_new, 
                                     swap_col_name_cleaner, 
                                     col_name_filter_func=lambda c: 'Swap Rate'.upper() in c.upper()).ffill()

usd_citi_atm_data_new = pd.read_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/raw/USD_ATM.csv', skiprows=2)  
usd_citi_atm_data_new = clean_citi_data(usd_citi_atm_data_new, atm_vol_col_name_cleaner).ffill()

aud_citi_atm_data_new = pd.read_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/raw/AUD_ATM.csv', skiprows=2)  
aud_citi_atm_data_new = clean_citi_data(aud_citi_atm_data_new, atm_vol_col_name_cleaner).ffill()

gbp_citi_atm_data_new = pd.read_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/raw/GBP_ATM.csv', skiprows=2)  
gbp_citi_atm_data_new = clean_citi_data(gbp_citi_atm_data_new, atm_vol_col_name_cleaner).ffill()

eur_citi_atm_data_new = pd.read_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/raw/EUR_ATM.csv', skiprows=2)  
eur_citi_atm_data_new = clean_citi_data(eur_citi_atm_data_new, atm_vol_col_name_cleaner).ffill()

gbp_citi_swap_data_new = pd.read_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/raw/GBP_SWAP.csv', skiprows=2)  
gbp_citi_swap_data_new = clean_citi_data(gbp_citi_swap_data_new, swap_col_name_cleaner, col_name_filter_func=lambda c: 'Swap Rate'.upper() in c.upper()).ffill()

eur_citi_swap_data_new = pd.read_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/raw/EUR_SWAP.csv', skiprows=2)  
eur_citi_swap_data_new = clean_citi_data(eur_citi_swap_data_new, swap_col_name_cleaner, col_name_filter_func=lambda c: 'Swap Rate'.upper() in c.upper()).ffill()



#archive main before merging
dirs.archive_files("C:/Users/zngyiq/OneDrive/Library/Data/citi/main", 
                   "C:/Users/zngyiq/OneDrive/Library/Data/citi/archive",
                   copy_only=True)


#merge new with main and save main


usd_citi_otm_data = read_csv_into_timeframe('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/USD_OTM.csv')  
usd_citi_swap_data = read_csv_into_timeframe('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/USD_SWAP.csv')  
aud_citi_otm_data = read_csv_into_timeframe('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/AUD_OTM.csv')  
aud_citi_swap_data = read_csv_into_timeframe('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/AUD_SWAP.csv')  
aud_citi_atm_data = read_csv_into_timeframe('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/AUD_ATM.csv')
usd_citi_atm_data = read_csv_into_timeframe('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/USD_ATM.csv')
gbp_citi_atm_data = read_csv_into_timeframe('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/GBP_ATM.csv')
eur_citi_atm_data = read_csv_into_timeframe('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/EUR_ATM.csv')
gbp_citi_swap_data = read_csv_into_timeframe('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/GBP_SWAP.csv')
eur_citi_swap_data = read_csv_into_timeframe('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/EUR_SWAP.csv')






#merge with main

usd_citi_otm_data = mrt.merge_dataframess(usd_citi_otm_data, usd_citi_otm_data_new, axis=0)
usd_citi_swap_data = mrt.merge_dataframess(usd_citi_swap_data, usd_citi_swap_data_new, axis=0)
aud_citi_otm_data = mrt.merge_dataframess(aud_citi_otm_data, aud_citi_otm_data_new, axis=0)
aud_citi_swap_data = mrt.merge_dataframess(aud_citi_swap_data, aud_citi_swap_data_new, axis=0)
usd_citi_atm_data = mrt.merge_dataframess(usd_citi_atm_data, usd_citi_atm_data_new, axis=0)
aud_citi_atm_data = mrt.merge_dataframess(aud_citi_atm_data, aud_citi_atm_data_new, axis=0)
gbp_citi_atm_data = mrt.merge_dataframess(gbp_citi_atm_data, gbp_citi_atm_data_new, axis=0)
eur_citi_atm_data = mrt.merge_dataframess(eur_citi_atm_data, eur_citi_atm_data_new, axis=0)
gbp_citi_swap_data = mrt.merge_dataframess(gbp_citi_swap_data, gbp_citi_swap_data_new, axis=0)
eur_citi_swap_data = mrt.merge_dataframess(eur_citi_swap_data, eur_citi_swap_data_new, axis=0)




#arcchive main
usd_citi_otm_data.to_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/USD_OTM.csv')
usd_citi_swap_data.to_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/USD_SWAP.csv')  
aud_citi_otm_data.to_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/AUD_OTM.csv')  
aud_citi_swap_data.to_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/AUD_SWAP.csv')  
aud_citi_atm_data.to_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/AUD_ATM.csv')
usd_citi_atm_data.to_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/USD_ATM.csv')
gbp_citi_atm_data.to_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/GBP_ATM.csv')
eur_citi_atm_data.to_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/EUR_ATM.csv')
gbp_citi_swap_data.to_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/GBP_SWAP.csv')
eur_citi_swap_data.to_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/main/EUR_SWAP.csv')





# interpoation


usd_citi_otm_interpolated_data = interpolate_strike_space(usd_citi_otm_data.tail(90), 
                                                          usd_citi_swap_data, 
                                                           strikes_in = strikes, 
                                                           strikes_fit=interpolated_strikes)

aud_citi_otm_interpolated_data = interpolate_strike_space(aud_citi_otm_data.tail(90), 
                                                          aud_citi_swap_data, 
                                                           strikes_in = strikes, 
                                                           strikes_fit=interpolated_strikes)




citi_vol_date = usd_citi_otm_data.index[-1].strftime('%Y-%m-%d')
#aud_citi_otm_data.index[-1].strftime('%Y-%m-%d')   


# ivol                

# =============================================================================
# citi_ivol_data = {
#     'usd': filter_data(usd_citi_otm_data, exps = exp, tails = tails, strikes = strikes, cin_f = citi_ivol_cin_f, cout_f =citi_ivol_cout_f),
#     'aud': filter_data(aud_citi_otm_data, exps = exp, tails = tails, strikes = strikes, cin_f = citi_ivol_cin_f, cout_f =citi_ivol_cout_f)    
#     }
# 
# # 
# assert len(citi_ivol_data['usd'].dropna(axis=1,how='all').columns) == 350
# assert len(citi_ivol_data['aud'].dropna(axis=1,how='all').columns) == 392
# 
# =============================================================================

citi_ivol_cin_f = lambda e, t, s: f'{e}_{t}_{s}'
citi_ivol_cout_f = lambda e, t, s: f'{e}_{t}_{s}'


citi_ivol_interpolated_data = {
    'usd': filter_data(usd_citi_otm_interpolated_data, exps = exp, tails = tails, strikes = interpolated_strikes, 
                       cin_f = citi_ivol_cin_f, cout_f =citi_ivol_cout_f).dropna(axis=1,how='all'),
    'aud': filter_data(aud_citi_otm_interpolated_data, exps = exp, tails = tails, strikes = interpolated_strikes, 
                       cin_f = citi_ivol_cin_f, cout_f =citi_ivol_cout_f).dropna(axis=1,how='all')    
    }




#the atm vol seems to be in %
citi_ivol_atm_data = {
    'usd': filter_data(usd_citi_atm_data.rename(lambda c: f'{c}_+0', axis=1)/100, exps = exp, tails = tails, strikes = ['+0'], 
                       cin_f = citi_ivol_cin_f, cout_f =citi_ivol_cout_f).dropna(axis=1,how='all'),
    'aud': filter_data(aud_citi_atm_data.rename(lambda c: f'{c}_+0', axis=1)/100, exps = exp, tails = tails, strikes = ['+0'], 
                       cin_f = citi_ivol_cin_f, cout_f =citi_ivol_cout_f).dropna(axis=1,how='all'),
    'gbp': filter_data(gbp_citi_atm_data.rename(lambda c: f'{c}_+0', axis=1)/100, exps = exp, tails = tails, strikes = ['+0'], 
                       cin_f = citi_ivol_cin_f, cout_f =citi_ivol_cout_f).dropna(axis=1,how='all'),
    'eur': filter_data(eur_citi_atm_data.rename(lambda c: f'{c}_+0', axis=1)/100, exps = exp, tails = tails, strikes = ['+0'], 
                       cin_f = citi_ivol_cin_f, cout_f =citi_ivol_cout_f).dropna(axis=1,how='all'),
    }







# 

assert len(citi_ivol_interpolated_data['usd'].columns) == 1050
assert len(citi_ivol_interpolated_data['aud'].columns) == 1134



# swap

ivol_swap_coutexp_f =  lambda e, t, s: f'{e}_{t}'
citi_ivol_swap_cin_f = lambda e, t, s: f'{e}_{t}'
citi_ivol_swap_cout_f = lambda e, t, s: f'{e}_{t}'


citi_ivol_swap_data = {
    'usd': filter_data(usd_citi_swap_data, exps = exp, tails = tails, strikes = strikes, 
                       cin_f = citi_ivol_swap_cin_f, cout_f =citi_ivol_swap_cout_f, coutexp_f=ivol_swap_coutexp_f),
    'aud': filter_data(aud_citi_swap_data, exps = exp, tails = tails, strikes = strikes, 
                       cin_f = citi_ivol_swap_cin_f, cout_f =citi_ivol_swap_cout_f, coutexp_f=ivol_swap_coutexp_f),
    'gbp': filter_data(gbp_citi_swap_data, exps = exp, tails = tails, strikes = strikes, 
                       cin_f = citi_ivol_swap_cin_f, cout_f =citi_ivol_swap_cout_f, coutexp_f=ivol_swap_coutexp_f),
    'eur': filter_data(eur_citi_swap_data, exps = exp, tails = tails, strikes = strikes, 
                       cin_f = citi_ivol_swap_cin_f, cout_f =citi_ivol_swap_cout_f, coutexp_f=ivol_swap_coutexp_f),       
}

# 72 columns each
assert len(citi_ivol_swap_data['usd'].dropna(axis=1,how='all').columns) == 72
assert len(citi_ivol_swap_data['aud'].dropna(axis=1,how='all').columns) == 72



#premium

citi_swaption_premium_data = {
    'usd': calc_premium(citi_ivol_interpolated_data['usd'], citi_ivol_swap_data['usd']),
    'aud': calc_premium(citi_ivol_interpolated_data['aud'], citi_ivol_swap_data['aud'])
    }
    

citi_atm_swaption_premium_data = {
    'usd': calc_premium(citi_ivol_atm_data['usd'], citi_ivol_swap_data['usd']),
    'aud': calc_premium(citi_ivol_atm_data['aud'], citi_ivol_swap_data['aud']),
    'gbp': calc_premium(citi_ivol_atm_data['gbp'], citi_ivol_swap_data['gbp']),
    'eur': calc_premium(citi_ivol_atm_data['eur'], citi_ivol_swap_data['eur']),
    }



# =============================================================================
# pca.PCA((lambda t: pd.DataFrame({c:d[t] 
#                                  for c, d 
#                                  in citi_atm_swaption_premium_data.items()}))('3M_10Y_+0').ffill().iloc[-600:],
#         rates_pca=True).plot_loadings(n=4)
# 
# =============================================================================


###########################################################################
## Analysis here 
###########################################################################

def zsc_df(df, transform_f = lambda d: d):
    output = (df.ffill() - df.ffill().apply(lambda s: np.average(s.dropna()))) \
             / df.ffill().apply(lambda s: np.std(s.dropna(), ddof=1))
    output = output.replace([np.inf, -np.inf], np.nan)    
    return transform_f(output)



def combine_same_fmt_tables(table1, table2, combine_func=lambda x, y: '{} / {}'.format(x,y) ):
    c_s = {}
    for i in table1.index:
        r_s = {}        
        for j in table1.columns:            
            r_s[j] = combine_func(table1.loc[i,j], table2.loc[i,j])      
        c_s[i] = pd.Series(r_s)
    combined_table = pd.DataFrame(c_s).transpose()
    if table1.index.name is not None:
        combined_table.index.name = table1.index.name
    return combined_table
    

def plot_heatmap(data, annot_data=None, ax=None, title=None, fsize=(10,4), 
                 color_min_v = -3, font_size = 7, 
                 min_val = None, max_val = None):    
    _annot_data = annot_data if annot_data is not None else data     
    _ax = None
    if ax is not None:
        _ax = ax
    else:
        
        fig1 = plt.figure(figsize=fsize)
        gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios =[1])
        _ax = plt.subplot(gs[0])             
    sns.heatmap(data, annot=_annot_data, fmt='', square =False, cbar =False, 
                linewidths=.5, cmap="coolwarm", annot_kws={"size": font_size}, mask=data.isnull(),vmin=None, vmax=None)
    _ax.set_yticklabels(_ax.get_yticklabels(), rotation=0, fontsize=font_size)
    _ax.set_xticklabels(_ax.get_xticklabels(), rotation=0, fontsize=font_size)
    _ax.set_xlabel(_ax.get_xlabel(), fontsize=font_size)
    _ax.set_ylabel(_ax.get_ylabel(), fontsize=font_size)
    
    _ax.xaxis.set_ticks_position("top")
                   
    if title is not None:
        _ax.set_title(title, fontsize = font_size)
    
    #_ax.tick_params(labelsize=8)
    plt.tight_layout()
    return _ax


def add_fig_to_excel(f, sht, fname, cell):
    sht.pictures.add(f, name=fname, left=sht1.range(cell).left, top=sht1.range(cell).top, update=True)
    plt.close(_f)
        


def construct_vol_premium_surface_skew(vol_premium_dict, skew_type =1):
    '''
    Parameters
    ----------
    vol_premium_dict : dict of dataframe
        k is the structure like 2Y_1Y and v is the dataframe of timeseries of the vol surface.
    skew_type : int, optional
        1 for comparing L/R wings, 2 for comparing wings to the belly. The default is 1.

    Returns
    -------
    output : TYPE
        DESCRIPTION.

    '''
    output = {}
    for k, v in vol_premium_dict.items():
        k_output = {}
        cols = v.columns
        half_ncol = len(cols)//2
        if half_ncol > 1: # only when there are more than 1 columns before skew can be constructed
            if skew_type == 1: #L/R wing comparison
                for i in np.arange(half_ncol):
                    k_output['{}/{}'.format(cols[i], cols[-i-1])] = v[cols[i]] - v[cols[-i-1]]
                    k_output = pd.DataFrame(k_output).replace([np.inf, -np.inf], np.nan)
            elif skew_type == 2: #wing to the belly
                if len(cols) % 2 ==  1: #only odd number of col can compare wings to the belly
                    for i in np.arange(half_ncol):
                        k_output['{}/{}/{}'.format(cols[i],cols[half_ncol], cols[-i-1])] = v[cols[i]] + v[cols[-i-1]] - v[cols[half_ncol]]
                        k_output = pd.DataFrame(k_output).replace([np.inf, -np.inf], np.nan)
                else:
                    # return original v because it has odd number of cols therefore dont know where is the belly
                    k_output = v
                    
        else:
            # return original v because it has only 1 col
            k_output = v
        
        output[k] = k_output            
    return output 
            

def pivot(data, l1s =[] , l2s =[], filter_f = lambda a, b:  f'{a}_{b}', filtered_col_rename_f = lambda c: c):
    '''
    Parameters
    ----------
    data : dataframe 
        data to be pivoted
    l1s : list, optional
        a list of available values to construct the filter for pivot with 
        other levels of filter. The default is [].
    l2s : list, optional
        a list of available values to construct the filter for pivot with
        other levels of filter.. The default is [].
    filter_f : func, optional
        used to construct the filter with l1s and l2s value. 
    filtered_col_rename_f : func, optional
        func to rename the pivoted columns. The default is lambda c: c.

    Returns
    -------
    output : TYPE
        DESCRIPTION.

    '''
    output = {}
    for l2 in l2s:
        for l1 in l1s:
            filter_str = filter_f(l1, l2)
            cols = [c for c in data.columns if filter_str in c]
            if len(cols) > 0:
               output[filter_str] = data[cols].rename(filtered_col_rename_f, axis = 1) 
    return output



  
def pvt_ds_to_table(ds, ridx, cidx, rfmt=lambda x: 'IVOL_{}'.format(x), ridx_name=None, cidx_name=None):
    c_s = {}
    for i in ridx:
        r_s = {}        
        for j in cidx:
            c_name = rfmt('{}_{}'.format(i, j))
            if c_name in ds.index:
                r_s[j] = ds[c_name]
        if len(r_s.keys()) > 0:
            c_s[i] = pd.Series(r_s)
    pvt_table = pd.DataFrame(c_s).transpose()
    if ridx_name is not None:
        pvt_table.index.name= ridx_name
    if cidx_name is not None:
        pvt_table.columns.name= cidx_name        
    return pvt_table






##################
# citi surface 
##################

# country -> fwd structure underline -> col otm spreads/row dates
citi_swaption_premium_data_pvt_by_country_fwd_dict = {k: pivot(v, l1s=exp, l2s=tails, filtered_col_rename_f = lambda c: c.split('_')[2]) 
                                                      for k,v in citi_swaption_premium_data.items()}

# check PCA for skew movement for selected tenor

#short dated long tail
pca.PCA(citi_swaption_premium_data_pvt_by_country_fwd_dict['usd']['3M_10Y'], rates_pca=True).plot_loadings(n=3)
citi_swaption_premium_data_pvt_by_country_fwd_dict['usd']['3M_10Y'].iloc[[-1, -5, -10, -20, -60]].transpose().plot()

# long dated long tail

pca.PCA(citi_swaption_premium_data_pvt_by_country_fwd_dict['usd']['3Y_10Y'], rates_pca=True).plot_loadings(n=3)
citi_swaption_premium_data_pvt_by_country_fwd_dict['usd']['3Y_10Y'].iloc[[-1, -5, -10, -20, -60]].transpose().plot()

#short dated short tail
#avoid the negative strike in the interpolation
pca.PCA(citi_swaption_premium_data_pvt_by_country_fwd_dict['usd']['3M_2Y'].dropna(how='any', axis=1), rates_pca=True).plot_loadings(n=3)
citi_swaption_premium_data_pvt_by_country_fwd_dict['usd']['3M_2Y'].dropna(how='any', axis=1).iloc[[-1, -5, -10, -20, -60]].transpose().plot()

# long dated short tail
pca.PCA(citi_swaption_premium_data_pvt_by_country_fwd_dict['usd']['3Y_2Y'], rates_pca=True).plot_loadings(n=3)
citi_swaption_premium_data_pvt_by_country_fwd_dict['usd']['3Y_2Y'].iloc[[-1, -5, -10, -20, -60]].transpose().plot()


# how about a 3y 2s10s bull flattener? what is the carry like to hold
#pca.PCA(bbg_ivol_swap_data['usd'][['3Y_2Y', '3Y_10Y']],rates_pca=True).plot_loadings(n=3)
#mrt.plot_curve(bbg_ivol_swap_data['usd'][['3Y_2Y', '3Y_10Y']].iloc[-3000:], curve=True)
# mrt.plot_curve(pd.DataFrame({'USD_2S10S': usd_bbg_data['USD_0Y_10Y'] - usd_bbg_data['USD_0Y_2Y'],
#                              'USD_3Y2S10S': usd_bbg_data['USD_3Y_10Y'] - usd_bbg_data['USD_3Y_2Y']}).iloc[-3000:]
#                , curve=False)
              

#short dated utla long tail
#avoid the negative strike in the interpolation
pca.PCA(citi_swaption_premium_data_pvt_by_country_fwd_dict['usd']['3M_30Y'], rates_pca=True).plot_loadings(n=3)
citi_swaption_premium_data_pvt_by_country_fwd_dict['usd']['3M_30Y'].iloc[[-1, -5, -10, -20, -60]].transpose().plot()

# long dated ultra long tail
pca.PCA(citi_swaption_premium_data_pvt_by_country_fwd_dict['usd']['3Y_30Y'], rates_pca=True).plot_loadings(n=3)
citi_swaption_premium_data_pvt_by_country_fwd_dict['usd']['3Y_30Y'].iloc[[-1, -5, -10, -20, -60]].transpose().plot()





# swaption premium and zsc (latest, zsc, combine and vis)

citi_swaption_premium_lastest = {k1: pd.DataFrame({k: v.ffill().iloc[-1] 
                                                     for k, v 
                                                     in v1.items()}).round(1) 
                                   for k1, v1 
                                   in citi_swaption_premium_data_pvt_by_country_fwd_dict.items()}


citi_swaption_premium_90day_zsc = {k1: pd.DataFrame({k:zsc_df(v).iloc[-1] 
                                                     for k, v 
                                                     in v1.items()}).round(2) 
                                   for k1, v1 
                                   in citi_swaption_premium_data_pvt_by_country_fwd_dict.items()}

citi_swaption_premium_90day_zsc_5days_chg = {k1: pd.DataFrame({k:zsc_df(v, lambda x: x.diff(5)).iloc[-1] 
                                                     for k, v 
                                                     in v1.items()}).round(2) 
                                            for k1, v1 
                                            in citi_swaption_premium_data_pvt_by_country_fwd_dict.items()}


citi_swaption_premium_lastest_with_zsc = {k: combine_same_fmt_tables(v, citi_swaption_premium_90day_zsc[k]) 
                                          for (k,v) 
                                          in citi_swaption_premium_lastest.items()}




# heatmap

citi_swaption_premium_lastest_with_zsc_heatmap = {k: plot_heatmap(citi_swaption_premium_90day_zsc[k].transpose(),
                                                                  annot_data=v.transpose(), 
                                                                  title=f'{k} 90days vol premium with zsc ({citi_vol_date})',
                                                                  fsize=(30, 15), min_val=-2.5, max_val = 2.5).get_figure() 
                                                  for k, v 
                                                  in citi_swaption_premium_lastest_with_zsc.items()}



citi_swaption_premium_5days_zsc_chg_heatmap = {k: plot_heatmap(v.transpose(),
                                                                  annot_data=v.transpose(), 
                                                                  title=f'{k} 5days change of 90day vol premium zsc ({citi_vol_date})',
                                                                  fsize=(20, 18), min_val=-0.5, max_val = 0.5).get_figure() 
                                                  for k, v 
                                                  in citi_swaption_premium_60day_zsc_5days_chg.items()}



##################
# citi surface skew
##################

# country -> fwd structure underline -> col otm skew/row dates
citi_swaption_premium_lr_skew_pvt_by_country_fwd_dict = {k: construct_vol_premium_surface_skew(v, skew_type=1) 
                                                      for k,v 
                                                      in citi_swaption_premium_data_pvt_by_country_fwd_dict.items()}

citi_swaption_premium_wingbelly_skew_pvt_by_country_fwd_dict = {k: construct_vol_premium_surface_skew(v, skew_type=2) 
                                                      for k,v 
                                                      in citi_swaption_premium_data_pvt_by_country_fwd_dict.items()}


# 3M10y tail skew
pca.PCA(citi_swaption_premium_lr_skew_pvt_by_country_fwd_dict['usd']['3M_10Y'], 
        rates_pca=True).plot_loadings(n=3)
pca.PCA(citi_swaption_premium_wingbelly_skew_pvt_by_country_fwd_dict['usd']['3M_10Y'], 
        rates_pca=True).plot_loadings(n=3)

pca.PCA(citi_swaption_premium_lr_skew_pvt_by_country_fwd_dict['aud']['3M_10Y'], 
        rates_pca=True).plot_loadings(n=3)
pca.PCA(citi_swaption_premium_wingbelly_skew_pvt_by_country_fwd_dict['aud']['3M_10Y'], 
        rates_pca=True).plot_loadings(n=3)


# 3y10y tail skew
pca.PCA(citi_swaption_premium_lr_skew_pvt_by_country_fwd_dict['usd']['3Y_10Y'], 
        rates_pca=True).plot_loadings(n=3)
pca.PCA(citi_swaption_premium_wingbelly_skew_pvt_by_country_fwd_dict['usd']['3Y_10Y'], 
        rates_pca=True).plot_loadings(n=3)

pca.PCA(citi_swaption_premium_lr_skew_pvt_by_country_fwd_dict['aud']['3Y_10Y'], 
        rates_pca=True).plot_loadings(n=3)
pca.PCA(citi_swaption_premium_wingbelly_skew_pvt_by_country_fwd_dict['aud']['3Y_10Y'], 
        rates_pca=True).plot_loadings(n=3)


# 3M5y tail skew
pca.PCA(citi_swaption_premium_lr_skew_pvt_by_country_fwd_dict['usd']['3M_5Y'], 
        rates_pca=True).plot_loadings(n=3)
pca.PCA(citi_swaption_premium_wingbelly_skew_pvt_by_country_fwd_dict['usd']['3M_5Y'], 
        rates_pca=True).plot_loadings(n=3)

pca.PCA(citi_swaption_premium_lr_skew_pvt_by_country_fwd_dict['aud']['3M_5Y'], 
        rates_pca=True).plot_loadings(n=3)
pca.PCA(citi_swaption_premium_wingbelly_skew_pvt_by_country_fwd_dict['aud']['3M_5Y'], 
        rates_pca=True).plot_loadings(n=3)


# 3y5y tail skew
pca.PCA(citi_swaption_premium_lr_skew_pvt_by_country_fwd_dict['usd']['3Y_5Y'], 
        rates_pca=True).plot_loadings(n=3)
pca.PCA(citi_swaption_premium_wingbelly_skew_pvt_by_country_fwd_dict['usd']['3Y_5Y'], 
        rates_pca=True).plot_loadings(n=3)

pca.PCA(citi_swaption_premium_lr_skew_pvt_by_country_fwd_dict['aud']['3Y_5Y'], 
        rates_pca=True).plot_loadings(n=3)
pca.PCA(citi_swaption_premium_wingbelly_skew_pvt_by_country_fwd_dict['aud']['3Y_5Y'], 
        rates_pca=True).plot_loadings(n=3)



# 3M30y tail skew
pca.PCA(citi_swaption_premium_lr_skew_pvt_by_country_fwd_dict['usd']['3M_30Y'], 
        rates_pca=True).plot_loadings(n=3)
pca.PCA(citi_swaption_premium_wingbelly_skew_pvt_by_country_fwd_dict['usd']['3M_30Y'], 
        rates_pca=True).plot_loadings(n=3)

pca.PCA(citi_swaption_premium_lr_skew_pvt_by_country_fwd_dict['aud']['3M_30Y'], 
        rates_pca=True).plot_loadings(n=3)
pca.PCA(citi_swaption_premium_wingbelly_skew_pvt_by_country_fwd_dict['aud']['3M_30Y'], 
        rates_pca=True).plot_loadings(n=3)


# 3y30y tail skew
pca.PCA(citi_swaption_premium_lr_skew_pvt_by_country_fwd_dict['usd']['3Y_30Y'], 
        rates_pca=True).plot_loadings(n=3)
pca.PCA(citi_swaption_premium_wingbelly_skew_pvt_by_country_fwd_dict['usd']['3Y_30Y'], 
        rates_pca=True).plot_loadings(n=3)

pca.PCA(citi_swaption_premium_lr_skew_pvt_by_country_fwd_dict['aud']['3Y_30Y'], 
        rates_pca=True).plot_loadings(n=3)
pca.PCA(citi_swaption_premium_wingbelly_skew_pvt_by_country_fwd_dict['aud']['3Y_30Y'], 
        rates_pca=True).plot_loadings(n=3)


# swaption premium skew and zsc (latest, zsc, combine and vis)

#lr
citi_swaption_premium_lr_skew_lastest = \
    {k1: pd.DataFrame({k: v.ffill().iloc[-1] 
                       for k, v 
                       in v1.items()}).round(1)      
     for k1, v1 
     in citi_swaption_premium_lr_skew_pvt_by_country_fwd_dict.items()}



citi_swaption_premium_lr_skew_zsc = \
    {k1: pd.DataFrame({k:zsc_df(v).iloc[-1] 
                       for k, v 
                       in v1.items()}).round(2) 
     for k1, v1 
     in citi_swaption_premium_lr_skew_pvt_by_country_fwd_dict.items()}


citi_swaption_premium_lr_zsc_5days_chg = \
    {k1: pd.DataFrame({k:zsc_df(v, lambda x: x.diff(5)).iloc[-1] 
                       for k, v
                       in v1.items()}).round(2) 
     for k1, v1 
     in citi_swaption_premium_data_pvt_by_country_fwd_dict.items()}


citi_swaption_premium_lr_lastest_with_zsc = \
    {k: combine_same_fmt_tables(v, citi_swaption_premium_lr_skew_zsc[k]) 
     for (k,v) 
     in citi_swaption_premium_lr_skew_lastest.items()}




# heatmap

citi_swaption_premium_lr_lastest_with_zsc_heatmap = \
    {k: plot_heatmap(citi_swaption_premium_lr_skew_zsc[k].transpose(),
                     annot_data=v.transpose(),
                     title=f'{k} 90days vol premium lr with zsc ({citi_vol_date})', 
                     fsize=(15, 15), min_val=-2.5, max_val = 2.5).get_figure() 
     for k, v 
     in citi_swaption_premium_lr_lastest_with_zsc.items()}



#wings vs belly

citi_swaption_premium_wb_skew_lastest = \
    {k1: pd.DataFrame({k: v.ffill().iloc[-1] 
                       for k, v 
                       in v1.items()}).round(1)      
     for k1, v1 
     in citi_swaption_premium_wingbelly_skew_pvt_by_country_fwd_dict.items()}



citi_swaption_premium_wb_skew_zsc = \
    {k1: pd.DataFrame({k:zsc_df(v).iloc[-1] 
                       for k, v 
                       in v1.items()}).round(2) 
     for k1, v1 
     in citi_swaption_premium_wingbelly_skew_pvt_by_country_fwd_dict.items()}


citi_swaption_premium_wb_zsc_5days_chg = \
    {k1: pd.DataFrame({k:zsc_df(v, lambda x: x.diff(5)).iloc[-1] 
                       for k, v
                       in v1.items()}).round(2) 
     for k1, v1 
     in citi_swaption_premium_wingbelly_skew_pvt_by_country_fwd_dict.items()}



citi_swaption_premium_wb_lastest_with_zsc = \
    {k: combine_same_fmt_tables(v, citi_swaption_premium_wb_skew_zsc[k]) 
     for (k,v) 
     in citi_swaption_premium_wb_skew_lastest.items()}




# heatmap

citi_swaption_premium_lr_lastest_with_zsc_heatmap = \
    {k: plot_heatmap(citi_swaption_premium_wb_skew_zsc[k].transpose(),
                     annot_data=v.transpose(),
                     title=f'{k} 90days vol premium wb with zsc ({citi_vol_date})', 
                     fsize=(15, 15), min_val=-2.5, max_val = 2.5).get_figure() 
     for k, v 
     in citi_swaption_premium_wb_lastest_with_zsc.items()}








if 2 > 3:
    
    vol_surface_points_10yrs = [c for c in aud_citi_otm_data.columns if '1M_10Y' in c or '3M_10Y' in c]
    interpolated_strikes_10yrs = [ f'+{c}' if c >=0 else f'{c}' for c in  np.arange(-30, 21, 1)[::-1]]
    interpolated_strikes_10yrs = [ 1.55, 1.8, 1.9]    
    aud_citi_10y_interpolated_interpolated_vol_surface = \
        interpolate_strike_space(aud_citi_otm_data[vol_surface_points_10yrs].tail(90), 
                                 aud_citi_swap_data, 
                                 strikes_in = strikes, 
                                 strikes_fit=interpolated_strikes_10yrs)
    
    scaler = [np.sqrt(20/250),np.sqrt(20/250),np.sqrt(20/250), np.sqrt(60/250), np.sqrt(60/250), np.sqrt(60/250)]
    aud_citi_10y_interpolated_interpolated_vol_surface_scaled  = \
        aud_citi_10y_interpolated_interpolated_vol_surface * scaler
     
    vol3m_3y = pd.DataFrame({   
    'fatmax_60': aud_citi_swap_data['3M_3Y'].diff().rolling(60).apply(lambda s: np.max(np.cumsum(s))) * 100,
    'fatmax_20': aud_citi_swap_data['3M_3Y'].diff().rolling(20).apply(lambda s: np.max(np.cumsum(s))) * 100,
    '3m_3y_ivol_range': aud_citi_atm_data['3M_3Y'] * np.sqrt(60/250),
    'rvol_20d_range':  aud_citi_swap_data['3M_3Y'].diff().rolling(20).std() * 100 * np.sqrt(20),
    'rvol_60d_range': aud_citi_swap_data['3M_3Y'].diff().rolling(60).std() * 100 * np.sqrt(60)})
    
    vol3m_10y = pd.DataFrame({   
    'fatmax_60d': aud_citi_swap_data['3M_10Y'].diff().rolling(60).apply(lambda s: np.max(np.cumsum(s))) * 100,
    'fatmax_20d': aud_citi_swap_data['3M_10Y'].diff().rolling(20).apply(lambda s: np.max(np.cumsum(s))) * 100,
    '3m_10y_ivol_range': aud_citi_atm_data['3M_10Y'] * np.sqrt(60/250),
    'rvol_20d_range':  aud_citi_swap_data['3M_10Y'].diff().rolling(20).std() * 100 * np.sqrt(20),
    'rvol_60d_range': aud_citi_swap_data['3M_10Y'].diff().rolling(60).std() * 100 * np.sqrt(60)})
    
    vol3m_10y_usd2 = pd.DataFrame({   
    'fatmax_60d': usd_citi_swap_data['3M_10Y'].diff().rolling(60).apply(lambda s: np.max(np.cumsum(s))) * 100,
    'fatmax_20d': usd_citi_swap_data['3M_10Y'].diff().rolling(20).apply(lambda s: np.max(np.cumsum(s))) * 100,
    '3m_10y_ivol_range': usd_citi_atm_data['3M_10Y'] * np.sqrt(60/250),
    'rvol_20d_range':  bbg_ivol_swap_data['usd']['3M_10Y'].diff().rolling(20).std() * 100 * np.sqrt(20),
    'rvol_60d_range': bbg_ivol_swap_data['usd']['3M_10Y'].diff().rolling(60).std() * 100 * np.sqrt(60)})
    
    vol1m_usd = pd.DataFrame({   
    #'fatmax_60d': usd_citi_swap_data['3M_10Y'].diff().rolling(60).apply(lambda s: np.max(np.cumsum(s))) * 100,
    #'fatmax_20d': usd_citi_swap_data['3M_10Y'].diff().rolling(20).apply(lambda s: np.max(np.cumsum(s))) * 100,
    '1m_5y_ivol': usd_citi_atm_data['1M_5Y'] * np.sqrt(20/250),
    '1m_5y_rvol': bbg_ivol_swap_data['usd']['1M_5Y'].diff().rolling(20).std() * 100 * np.sqrt(20),
    '1m_30y_ivol': usd_citi_atm_data['1M_30Y'] * np.sqrt(20/250),
    '1m_30y_rvol': bbg_ivol_swap_data['usd']['1M_30Y'].diff().rolling(20).std() * 100 * np.sqrt(20),
       
    #'rvol_20d_range':  bbg_ivol_swap_data['usd']['3M_10Y'].diff().rolling(20).std() * 100 * np.sqrt(20),
    #'rvol_60d_range': bbg_ivol_swap_data['usd']['3M_10Y'].diff().rolling(60).std() * 100 * np.sqrt(60)
    }).ffill()
    
    vol1m_usd['1m_5y_ivol/rvol'] = vol1m_usd['1m_5y_ivol']/vol1m_usd['1m_5y_rvol']
    vol1m_usd['1m_30y_ivol/rvol'] = vol1m_usd['1m_30y_ivol']/vol1m_usd['1m_30y_rvol']
    
    
    vol6m_usd = pd.DataFrame({   
    #'fatmax_60d': usd_citi_swap_data['3M_10Y'].diff().rolling(60).apply(lambda s: np.max(np.cumsum(s))) * 100,
    #'fatmax_20d': usd_citi_swap_data['3M_10Y'].diff().rolling(20).apply(lambda s: np.max(np.cumsum(s))) * 100,
    '6m_5y_ivol': usd_citi_atm_data['6M_5Y'] * np.sqrt(120/250),
    '6m_5y_rvol': bbg_ivol_swap_data['usd']['6M_5Y'].diff().rolling(120).std() * 100 * np.sqrt(120),
    '6m_30y_ivol': usd_citi_atm_data['6M_30Y'] * np.sqrt(120/250),
    '6m_30y_rvol': bbg_ivol_swap_data['usd']['6M_30Y'].diff().rolling(120).std() * 100 * np.sqrt(120),
       
    #'rvol_20d_range':  bbg_ivol_swap_data['usd']['3M_10Y'].diff().rolling(20).std() * 100 * np.sqrt(20),
    #'rvol_60d_range': bbg_ivol_swap_data['usd']['3M_10Y'].diff().rolling(60).std() * 100 * np.sqrt(60)
    }).ffill()
    
    vol6m_usd['6m_5y_ivol/rvol'] = vol6m_usd['6m_5y_ivol']/vol6m_usd['6m_5y_rvol']
    vol6m_usd['6m_30y_ivol/rvol'] = vol6m_usd['6m_30y_ivol']/vol6m_usd['6m_30y_rvol']
    
    
    
    
    
    
    
    
    
    

     auus_atm_ivol_premium = (citi_atm_swaption_premium_data['aud'] - citi_atm_swaption_premium_data['usd'])
     
     auus_atm_ivol_premium_5y_zsc = zsc_df(auus_atm_ivol_premium.iloc[-1250:], lambda d: d.iloc[-1])
     
     
     auus_atm_ivol_premium_with_5y_zsc = combine_same_fmt_tables(auus_atm_ivol_premium.round(1), auus_atm_ivol_premium_5y_zsc.round(2)) 
     
   
    
     v = pvt_ds_to_table(auus_atm_ivol_premium.iloc[-1], exp, tails, rfmt=lambda x: f'{x}_+0')
     vz = pvt_ds_to_table(auus_atm_ivol_premium_5y_zsc, exp, tails, rfmt=lambda x: f'{x}_+0')
     v_vz = combine_same_fmt_tables(v.round(1), vz.round(2)) 
     
     plot_heatmap(vz, annot_data=v_vz, title=f'AU/US vol premium with 5y zsc ({citi_vol_date})', 
                  fsize=(15, 15), min_val=-2.5, max_val = 2.5)




     #
     pivot(auus_atm_ivol_premium, l1s=exp, l2s=tails, filtered_col_rename_f = lambda c: c.split('_')[2]) 




#### test analytic risk calc

# =============================================================================
# 
# if 2 > 3:
#     #5y aud swap
#     (audswaprate, risk, carry, roll) = qlutils.calc_par_rate(aud_bbsw_swap_curve, aud_ois_curve,
#                                                              '0Y', '10Y', ql.Actual365Fixed(), 
#                                                              ql.Actual365Fixed(),
#                                                              bump_discount_curve = True)
#         
#     usdswap5_risk = qlutils.calc_swap_analytical_risk(usd_3m_libor_swap_curve, usd_3m_libor_swap_curve,
#                                                       '0D', '5Y', ql.Thirty360(), ql.Actual360(),
#                                                       bump_discount_curve = False)
#     
# 
# 
# =============================================================================



# =============================================================================
# #### check the bootstraped curve can price the original nodes correctly
# if 2 > 3:
#     for t, r in bbg_swap_curve_data['aud'].iloc[-1,:].items():
#         fpr = None
#         if ql.Period(t) <= ql.Period('3Y'):
#             fpr = ql.MakeVanillaSwap(ql.Period(t), ql.Bbsw3M(ql.RelinkableYieldTermStructureHandle(aud_bbsw_swap_curve)), 0.01, ql.Period('0D'),
#                                      pricingEngine=ql.DiscountingSwapEngine(ql.RelinkableYieldTermStructureHandle(aud_ois_curve)),
#                                      fixedLegDayCount=ql.Actual365Fixed(),
#                                      floatingLegDayCount=ql.Actual365Fixed()).fairRate()
#         else:
#             fpr = ql.MakeVanillaSwap(ql.Period(t), ql.Bbsw6M(ql.RelinkableYieldTermStructureHandle(aud_bbsw_swap_curve)), 0.01, ql.Period('0D'),
#                                      pricingEngine=ql.DiscountingSwapEngine(ql.RelinkableYieldTermStructureHandle(aud_ois_curve)),
#                                      fixedLegDayCount=ql.Actual365Fixed(),
#                                      floatingLegDayCount=ql.Actual365Fixed()).fairRate()
#             
#         print(f"{t}, r: {r}: fpr {round(fpr*100, 5)}")
# 
#     
#     for t, r in bbg_swap_curve_data['usd'].iloc[-1,:].items():
#             
#             fpr = ql.MakeVanillaSwap(ql.Period(t), ql.USDLibor(ql.Period('3M'), ql.RelinkableYieldTermStructureHandle(usd_3m_libor_swap_curve)), 
#                                      0.01, ql.Period('0D'),
#                                      pricingEngine=ql.DiscountingSwapEngine(ql.RelinkableYieldTermStructureHandle(sofr_ois_curve)),
#                                      fixedLegDayCount=ql.Thirty360(),
#                                      floatingLegDayCount=ql.Actual360()).fairRate()
#             
#             print(f"{t}, r: {r}: fpr {round(fpr*100, 5)}")
#             
# 
# 
# 
# =============================================================================



# =============================================================================
# 
# if 2 > 3:
#     from statistics.derivative.pysabr import hagan_2002_normal_sabr as normal_sabr
# 
#     #v = np.array([20.58, 17.64, 16.93, 18.01, 20.46, 22.90, 26.11, 28.89, 31.91])/ 10000
#     #k = np.array([-0.0067, -0.0042, -0.0017, 0.0008, 0.0033, 0.0058, 0.0083, 0.0108, 0.0133])
#     
#     v = citi_ivol_data['usd'][['3M_10Y_+50',
#                              '3M_10Y_+25',
#                              '3M_10Y_+10',
#                              '3M_10Y_+0',
#                              '3M_10Y_-10',
#                              '3M_10Y_-25',
#                              '3M_10Y_-50']].iloc[-1][::-1].values/10000
#     
#     
#     k = np.array([citi_ivol_swap_data['usd']['3M_10Y'].iloc[-1] * 100 + sp for sp in [-50, -25, -10, 0, 10, 25, 50]])/100
#     ktf = np.array([citi_ivol_swap_data['usd']['3M_10Y'].iloc[-1] * 100 + sp for sp in np.arange(-50,55,5)])/100
#     
#     
#     f = k[3]
#     t = 0.25
#     s = 0
#     beta = 0
#     
#     sabr_model = normal_sabr.Hagan2002NormalSABR(f, s, t, beta=beta)
#     sabr_model_params = sabr_model.fit(k, v)
#     
#     (alpha, rho, volvol) = sabr_model_params
#     
#     v_out = []
#     v_in = []    
#     for i in np.arange(len(ktf)):
#         v_out.append(normal_sabr.normal_vol(ktf[i], f, t, alpha, beta, rho, volvol))    
#         v_in.append(v[np.where(k == ktf[i])[0][0]] if ktf[i] in k else np.nan)
#     
#     ax = pd.DataFrame({'v':v_in, 'v_out':v_out}, index = ktf)['v_out'].plot()
#     pd.DataFrame({'v':v_in, 'v_out':v_out}, index = ktf)['v'].reset_index().plot.scatter(x='index',y ='v', ax=ax)
# 
# 
# 
# 
# =============================================================================
