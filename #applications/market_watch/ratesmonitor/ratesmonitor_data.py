import pandas as pd
import numpy as np
import os, sys, inspect
from operator import itemgetter
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from cycler import cycler
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations, chain
from pykalman import KalmanFilter
import xlwings as xw

from pandas.tseries.offsets import DateOffset


_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper
from statistics.derivative import bs as bs




from scipy import stats
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, fcluster
import scipy.cluster.hierarchy as cl
from scipy.spatial.distance import pdist, squareform



import QuantLib as ql


pd.set_option('display.expand_frame_repr', False)


####################################################################################################
## Function
####################################################################################################


def bootstrap_ifs_fwd(data):
    nc =  { c: int( c[4:].replace('_SWIT','').replace('Y','') ) for c in data.columns}
    data = data.rename(columns=nc)
    a = data.columns.tolist()[1:]
    b = data.columns.tolist()[:-1]
    fwd = {}
    fwd['{0}Y'.format(data.columns[0])] = data[data.columns[0]]
    for (ca,cb) in zip(a, b):
        term = ca - cb
        forward = cb
        cca = (1 + data[ca]/100).pow(ca)
        ccb = (1 + data[cb]/100).pow(cb)
        fwd['{0}Y{1}Y'.format(forward, term)] = np.log(cca/ccb)/term * 100
    fwd = pd.DataFrame(fwd)
    return fwd


def smooth_series(ds, lim=2, half_window=1):

    def smooth(row):

        if np.isnan(row['ds_prev']) or np.isnan(row['ds_next']):
            return row['ds']

        if abs(row['ds_prev'] - row['ds']) > lim and abs(row['ds_next'] - row['ds']) > lim and abs((row['ds_prev'] + row['ds_next'])/2 - row['ds']) > lim:
            return (row['ds_prev'] + row['ds_next'])/2
        else:
            return row['ds']

    data = pd.DataFrame({'ds':ds, 'ds_prev': ds.shift(half_window), 'ds_next': ds.shift(-1*half_window)})

    return data.apply(smooth, axis=1)
 
    

def smooth_rates(data, limit, half_window): 
    out = {}
    for col, ds in data.iteritems():
        out[col] = smooth_series(ds, lim=limit, half_window=half_window)
    
    return pd.DataFrame(out)

def smooth_fwd_rates(data, limit, half_window): 
    
    out = {}
    
    for col, ds in data.iteritems():
        
        if len(col.split('_')) > 1 and int(col.split('_')[1].replace('Y','').replace('M','')) != 0:
            out[col] = smooth_series(ds, lim=limit, half_window=half_window)
        else:
            out[col] = ds
    
    return pd.DataFrame(out)


def restructure(data, tenors, curve_names):
    df_data = {}
    curve_names.sort()    
    for curve in curve_names:
        curve_columns = [curve + '_' + tenor for tenor in tenors]
        curve_data = data[curve_columns].rename(lambda x: x.split('_')[2], axis='columns')
        ds = curve_data.iloc[0]
        df_data[curve] = ds 
    df = pd.DataFrame(df_data)
    df.index.name = 'Tenor'
    return df


def create_pca(data, momentum_size=None, resample_freq= 'W', dummy=None,  matrixoveride = None):
    
    if dummy is not None:
        print(dummy)

    data_sub = data.resample(resample_freq).last() if resample_freq is not None else data  
    
    return pca.PCA(data_sub.dropna(how='all',axis=1).dropna(how='all',axis=0), rates_pca = True, momentum_size = momentum_size, matrixoveride = matrixoveride)


def create_combo_mr(data, elements = 2, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None, matrixoveride = None):
    mrs = {}
    data_sub = data
    combos = list(combinations(data.columns, elements))
    instrument = combos[0][0][3:]
    
    for combo in combos:
        _sub_data = data_sub[list(combo)].resample(resample_freq).last() if resample_freq is not None else data_sub[list(combo)]
        data_sub_pca = pca.PCA(_sub_data.dropna(how='all',axis=1).dropna(how='all',axis=0), rates_pca = True, momentum_size = momentum_size,matrixoveride = matrixoveride)
        hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_pca.pc_hedge
        mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
        mrs[('_').join(combo) + '_PCW'] = (mr_obj, data_sub_pca)
        if include_normal_weighted_series:
            if len(_sub_data.columns) == 2:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] - _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s - %s' % (_sub_data.columns[0], _sub_data.columns[1])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)
            elif len(_sub_data.columns) == 3:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] + _sub_data[_sub_data.columns[2]] - 2 * _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s -2 x %s + %s' % (_sub_data.columns[0], _sub_data.columns[1], _sub_data.columns[2])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)

    return mrs

def realign_cols(df, cols):
    out_df = df
    for c in cols:
        if c not in out_df:
            out_df[c] = np.NaN
    return out_df



def generate_zscore(data,periods):
    #rts = {}
    #spots = {}
    #spots[0] = data
    zsc = {}
    for p in periods:
        #rts[p] = data - data.shift(p)
        zsc[p] = (data - data.rolling(p).mean()) / data.rolling(p).std()
        #spots[p] = data.shift(p)
        #zsc[p] = pd.DataFrame({"{} days zsc".format(p): (data.iloc[-1] - data.iloc[-30:].mean()) / data.iloc[-30:].std()}).transpose()
    #week to date        
    return zsc



def refmt_returns_by_p(rts, tickers, date_offset):
    rfmt_rts_by_p = {}
    for rt_key, rt in rts.items():
        rfmt_rts = []
        for ticker in tickers:
                tmp = rt.iloc[date_offset][ticker].rename(lambda n: n[:3]).rename(ticker[0][3:]).to_frame()
                #print(tmp)
                rfmt_rts.append(tmp)
                #if len(rfmt_rts) > 1:
                #    print(pd.concat(rfmt_rts, axis=1, join='outer',sort=False))
                
        tmp = pd.concat(rfmt_rts, axis=1, join='outer',sort=False).transpose()
        if 'AUD' in tmp.columns and  'USD' in tmp.columns:
            tmp['AUDUSD'] = tmp['AUD'] - tmp['USD']
        if 'CAD' in tmp.columns and  'USD' in tmp.columns:
            tmp['CADUSD'] = tmp['CAD'] - tmp['USD']
        if 'EUR' in tmp.columns and  'USD' in tmp.columns:
            tmp['EURUSD'] = tmp['EUR'] - tmp['USD']
        if 'AUD' in tmp.columns and  'NZD' in tmp.columns:
            tmp['AUDNZD'] = tmp['AUD'] - tmp['NZD']
        rfmt_rts_by_p[rt_key] = tmp
    return rfmt_rts_by_p


def make_fly(df, w=[-1,2,-1]):
    d = df.iloc[:,1] * w[1] + df.iloc[:,0] * w[0] + df.iloc[:,2] * w[2]    
    return d

def make_zsc(df):    
    z = (df - df.mean())/df.std()
    return z

def make_df_from_ddds(ddds, df_row_idx = -1):
    df = {}
    for k1, dds in ddds.items():
        s = {}
        for k2, ds in dds.items():
            s[k2] = ds.iloc[-1]
        df[k1] = pd.Series(s)
    df = pd.DataFrame(df)
    return df

def make_ds_from_ddds(ddds, df_row_idx = -1):
    s = {}
    for k1, dds in ddds.items():      
        for k2, ds in dds.items():
            s['{}_{}'.format(k1,k2)] = ds.iloc[df_row_idx]            
    ds = pd.Series(s)
    return ds



def blanace_wings(l, c, r, lim=3):
    r = abs(l-c)/abs(r-c)
    if(r > lim or 1/r > lim):
        return False
    else: 
        return True
    
def get_cr(swaps, carryroll_store, w):
    #swap is a string like 'USD_1Y_3Y'
    #carryroll_store is dict[('1Y','3Y')] of dict keys 'rate', 'carry', 'roll', 'cr'
    
    #f = swap.split('_')[1]
    #t = swap.split('_')[2]
    
    crs = [carryroll_store[(swap.split('_')[1], swap.split('_')[2])]['cr'] for swap in swaps]
    total_cr = np.dot(np.array(crs), np.array(w))
    return total_cr

def kca(d):        
    h = 1 # 1day
    A=np.array([[1,h,.5*h**2], 
                [0,1,h], 
                [0,0,1]])
    
    H=np.array([1,0,0])
    q = 1
    Q=q*np.eye(A.shape[0]) #cov matrix is set to be the identity matrix
    R=q*np.eye(H.shape[0])
    
    #2 create the filter 
    kf=KalmanFilter(transition_matrices=A,
                    observation_matrices=H,
                    transition_covariance=Q) 
    kf = kf.em(d)
    #smoothing
    kf_mean, kf_covar = kf.smooth(d)
    df = pd.DataFrame(kf_mean, columns=['mean', 'momentum', 'gamma'], index=d.index)
    df['original'] = d 
    return df 

def pvt_ds_to_table(ds, ridx, cidx, rfmt=lambda x: 'IVOL_{}'.format(x), ridx_name=None, cidx_name=None):
    c_s = {}
    for i in ridx:
        r_s = {}        
        for j in cidx:
            c_name = rfmt('{}_{}'.format(i, j))
            r_s[j] = ds[c_name]      
        c_s[i] = pd.Series(r_s)
    pvt_table = pd.DataFrame(c_s).transpose()
    if ridx_name is not None:
        pvt_table.index.name= ridx_name
    if cidx_name is not None:
        pvt_table.columns.name= cidx_name        
    return pvt_table



def combine_same_fmt_tables(table1, table2, combine_func=lambda x, y: '{} / {}'.format(x,y) ):
    c_s = {}
    for i in table1.index:
        r_s = {}        
        for j in table1.columns:            
            r_s[j] = combine_func(table1.loc[i,j], table2.loc[i,j])      
        c_s[i] = pd.Series(r_s)
    combined_table = pd.DataFrame(c_s).transpose()
    if table1.index.name is not None:
        combined_table.index.name = table1.index.name
    return combined_table
    

def plot_heatmap(data, annot_data=None, ax=None, title=None, fsize=(10,4)):    
    _annot_data = annot_data if annot_data is not None else data     
    _ax = None
    if ax is not None:
        _ax = ax
    else:
        
        fig1 = plt.figure(figsize=fsize)
        gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios =[1])
        _ax = plt.subplot(gs[0])             
    sns.heatmap(data, annot=_annot_data, fmt='', square =False, cbar =False, linewidths=.5, cmap="coolwarm", annot_kws={"size": 8})
    _ax.set_yticklabels(_ax.get_yticklabels(), rotation=0, fontsize=8)
    _ax.set_xticklabels(_ax.get_xticklabels(), rotation=0, fontsize=8)
    _ax.set_xlabel(_ax.get_xlabel(), fontsize=8)
    _ax.set_ylabel(_ax.get_ylabel(), fontsize=8)
    
    _ax.xaxis.set_ticks_position("top")
                   
    if title is not None:
        _ax.set_title(title, fontsize = 10)
    
    #_ax.tick_params(labelsize=8)
    return _ax


def add_fig_to_excel(f, sht, fname, cell):
    sht.pictures.add(f, name=fname, left=sht1.range(cell).left, top=sht1.range(cell).top, update=True)
    plt.close(_f)
        



####################################################################################################
## Data BBG
####################################################################################################

filedate = '2021-04-12'

usd_data = pd.read_csv('C:/Temp/test/market_watch/staging/'+ filedate + '_USD_historical.csv')  
usd_data['date'] = pd.to_datetime(usd_data['date'])
usd_data = usd_data.set_index('date').ffill()


swap_data = pd.read_csv('C:/Temp/test/market_watch/staging/'+ filedate + '_SWAP_ALL_V2_historical.csv')  
swap_data['date'] = pd.to_datetime(swap_data['date'])
swap_data = swap_data.set_index('date').ffill()

all_vol_data = pd.read_csv('C:/Temp/test/market_watch/staging/'+ filedate + '_VOL_historical.csv')  
all_vol_data['date'] = pd.to_datetime(all_vol_data['date'])
all_vol_data = all_vol_data.set_index('date').ffill()






#libor
usd_libor_cls = [c for c in usd_data.columns if c.startswith('LIBOR_')]
usd_libor_data = usd_data[usd_libor_cls]



#swaps

usd_swap_cls = [c for c in usd_data.columns if c.startswith('USD_') and (not c.endswith('SWIT'))]
usd_fwd_terms = ['0Y','6M','1Y','2Y','3Y','5Y'] #,'4Y', '6Y','7Y','8Y','9Y','1M','9M','10Y'
usd_swap_terms = ['1Y','2Y','3Y','4Y', '5Y', '6Y', '7Y', '8Y', '9Y', '10Y' ,'20Y' ,'30Y']
usd_curve_swap_terms = [c for c in combinations(usd_swap_terms,2)]
usd_fly_swap_terms = [c for c in combinations(usd_swap_terms,3) if blanace_wings(int(c[0].replace('Y','')), int(c[1].replace('Y','')), int(c[2].replace('Y','')))]

usd_swap_fly_data = {f: {'{}{}{}'.format(l,c,r):usd_data[['USD_{}_{}'.format(f,l), 'USD_{}_{}'.format(f,c), 'USD_{}_{}'.format(f,r)]] for (l,c,r) in usd_fly_swap_terms} for f in usd_fwd_terms}


#fix this
extra = [('1Y_1Y', '2Y_2Y', '5Y_5Y'),('1Y_1Y', '2Y_2Y', '5Y_5Y'),('2Y_2Y', '5Y_5Y', '10Y_10Y')]

for (l,c,r) in extra:
    usd_swap_fly_data['0Y']['{}_{}_{}'.format(l.replace('_',''),c.replace('_',''),r.replace('_',''))] = \
        usd_data[['USD_{}'.format(l), 'USD_{}'.format(c), 'USD_{}'.format(r)]]     


#vol

#pre determined exps and tails
usd_ivol_exp = ['1M', '3M', '6M', '1Y',  '2Y', '3Y', '4Y', '5Y', '10Y'] # remove '6Y',  '7Y', '8Y', '9Y', as they are dead 
usd_ivol_tails = ['1Y', '2Y', '3Y','4Y', '5Y', '6Y', '7Y', '8Y', '9Y', '10Y',  '20Y', '30Y']

#make ivol columns
usd_ivol_cls = list(chain(*[[f'IVOL_{e}_{t}' for t in usd_ivol_tails] for e in usd_ivol_exp]))
aud_bbg_ivol_cls = [c.replace('IVOL', 'AUD') for c in list(chain(*[[f'IVOL_{e}_{t}' for t in usd_ivol_tails] for e in usd_ivol_exp]))]



#usd_ivol_fwd = [ '0Y','1M','3M','6M', '1Y', '2Y', '3Y', '4Y', '5Y', '6Y', '7Y', '8Y', '9Y', '10Y']

#bbg ivol
usd_ivol_data = usd_data[usd_ivol_cls]
aud_bbg_ivol_data = all_vol_data[aud_bbg_ivol_cls].rename(lambda c: c.replace('AUD','IVOL'), axis=1)

#bbg ivol matched swap
usd_ivol_matching_swap_cols = list(chain(*[[f'USD_{e}_{t}' for t in usd_ivol_tails] for e in usd_ivol_exp]))
usd_ivol_matching_swap_data = usd_data[usd_ivol_matching_swap_cols]

aud_bbg_ivol_matching_swap_data = swap_data[aud_bbg_ivol_cls]



#rvol

usd_rvol_data_dict = {
    '1M': ((usd_ivol_matching_swap_data * 100).diff().rolling(20).std() * np.sqrt(250)).rename(lambda c: c.replace('USD', 'RVOL'), axis=1), 
    '3M': ((usd_ivol_matching_swap_data * 100).diff().rolling(60).std() * np.sqrt(250)).rename(lambda c: c.replace('USD', 'RVOL'), axis=1)        
    }


aud_rvol_data_dict = {
    '1M': ((aud_bbg_ivol_matching_swap_data * 100).diff().rolling(20).std() * np.sqrt(250)).rename(lambda c: c.replace('AUD', 'RVOL'), axis=1), 
    '3M': ((aud_bbg_ivol_matching_swap_data * 100).diff().rolling(60).std() * np.sqrt(250)).rename(lambda c: c.replace('AUD', 'RVOL'), axis=1)        
    }




####################################################################################################
## Data Citi
####################################################################################################

#filedate = '2021-03-31'

def clean_citi_data(df, col_rename_func, col_name_filter_func=None):
    #col 0 is the date
    _df = df[(~ df.iloc[:,0].str.contains('---')) & \
             (~ df.iloc[:,0].str.contains('confidential'))]
    #name the date column
    _df = _df.rename(columns={_df.columns[0]: 'date'})  
    _df['date'] = pd.to_datetime(_df['date'])
    _df = _df.set_index('date')
    # filter col  
    _dfc = _df.columns if col_name_filter_func is None else [ c for c in _df.columns if col_name_filter_func(c)]
    _df = _df[_dfc]
    # rename col    
    _df = _df.rename(col_rename_func, axis=1)    
    return _df

def otm_vol_col_name_cleaner(c):    
    cs = c.split(' ')
    keys = [c.upper() for c in itemgetter(*[5,6,7])(cs)]
    new_c = '{}_{}_{}'.format(*keys)
    return new_c

def swap_col_name_cleaner(c):
    if 'PAR' in c.upper():
        cs = c.split(' ')        
        new_c = '0Y_{}'.format(cs[1])
        return new_c
    else:            
        cs = c.split(' ')
        keys = [c.upper() for c in itemgetter(*[1,3])(cs)]
        new_c = '{}_{}'.format(*keys)
        return new_c


def calc_premium(vol_df, fwd_df):
    #vol col name is expected to be like '5Y_10Y_+10' indicating 5y exp 10y tail and atm +10bps
    def call_pricer(row):
        #not going to price when vol or fwd is not available or vol is less equal than 0 or strike is negative
        if (np.isnan(row.iloc[1]) or np.isnan(fwd[row.iloc[0]]) or row.iloc[1] <= 0 or fwd[row.iloc[0]] + otm_spread <= 0 ):
            return np.nan
        else:
            _premium = bs.price_swaption(fwd[row.iloc[0]],
                                        fwd[row.iloc[0]] + otm_spread, #strike 
                                        exp, # t in years
                                        row.iloc[1], # vol in bps
                                        payer=True if otm_spread >= 0 else False, 
                                        normal=True)
            return _premium
                                  
    premium_df = {}
    for c in vol_df.columns:
        exp, tail, otm_spread = itemgetter(*[0,1,2])(c.split('_'))
        fwd_col = f'{exp}_{tail}'
        exp = bs.convert_to_year(exp)
        tail = bs.convert_to_year(tail)
        otm_spread = float(otm_spread)/100
        #get vol history and fwd history
        vol = vol_df[c]
        fwd = fwd_df[fwd_col].reindex(vol.index)
        
        # --- have to reset index to convert series to frame to iterate through the data
        # ----iloc[0] is the date and iloc[1] is the vol value
        
        #print(f'{exp} {tail} {otm_spread}')
        
        premium = vol.reset_index().apply(call_pricer, axis = 1)
        premium_df[c] = pd.Series(premium.values, vol.index)
        
        
        #print(premium)
    return pd.DataFrame(premium_df)
         
        
    


#OTM vol file

# pre made
usd_citi_otm_exp = ['3M', '6M', '1Y', '2Y', '3Y', '5Y', '10Y'] 
#usd_citi_otm_tails = ['1Y','2Y', '5Y','7Y', '10Y',  '30Y']
usd_citi_otm_tails = ['1Y','2Y', '3Y', '5Y','7Y', '10Y', '20Y', '30Y']  
usd_citi_otm_strikes = ['+50', '+25','+10','+0','-10', '-25','-50']

usd_citi_exp_tails_col_ordered = list(chain(*[[f'{e}_{t}' for e in usd_citi_otm_exp] for t in usd_citi_otm_tails]))

#load and clean up
usd_citi_otm_data = pd.read_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/USD_OTM.csv', skiprows=2)  
usd_citi_otm_data = clean_citi_data(usd_citi_otm_data, otm_vol_col_name_cleaner)

usd_citi_swap_data = pd.read_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/USD_SWAP.csv', skiprows=2)  
usd_citi_swap_data = clean_citi_data(usd_citi_swap_data, swap_col_name_cleaner, col_name_filter_func=lambda c: 'Swap Rate'.upper() in c.upper())

aud_citi_otm_data = pd.read_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/AUD_OTM.csv', skiprows=2)  
aud_citi_otm_data = clean_citi_data(aud_citi_otm_data, otm_vol_col_name_cleaner)

aud_citi_swap_data = pd.read_csv('C:/Users/zngyiq/OneDrive/Library/Data/Citi/AUD_SWAP.csv', skiprows=2)  
aud_citi_swap_data = clean_citi_data(aud_citi_swap_data, swap_col_name_cleaner, col_name_filter_func=lambda c: 'Swap Rate'.upper() in c.upper())




#usd_citi_otm_data_cls = [c for c in usd_citi_otm_data.columns]

#convert to dict by strikes and removing the strike from the name

#usd
usd_citi_otm_data_dict = {s: usd_citi_otm_data[[c for c in usd_citi_otm_data.columns if s in c]].rename(lambda cl: cl.replace('_{}'.format(s),''), axis=1) for s in usd_citi_otm_strikes}

usd_citi_otm_data_dict['RR50'] = usd_citi_otm_data_dict['+50'] - usd_citi_otm_data_dict['-50']
usd_citi_otm_data_dict['RR25'] = usd_citi_otm_data_dict['+25'] - usd_citi_otm_data_dict['-25']
usd_citi_otm_data_dict['RR10'] = usd_citi_otm_data_dict['+10'] - usd_citi_otm_data_dict['-10']
usd_citi_otm_data_dict['BF50'] = usd_citi_otm_data_dict['+50'] + usd_citi_otm_data_dict['-50'] - usd_citi_otm_data_dict['+0'] * 2
usd_citi_otm_data_dict['BF25'] = usd_citi_otm_data_dict['+25'] + usd_citi_otm_data_dict['-25'] - usd_citi_otm_data_dict['+0'] * 2
usd_citi_otm_data_dict['BF10'] = usd_citi_otm_data_dict['+10'] + usd_citi_otm_data_dict['-10'] - usd_citi_otm_data_dict['+0'] * 2

#aud
aud_citi_otm_data_dict = {s: aud_citi_otm_data[[c for c in aud_citi_otm_data.columns if s in c]].rename(lambda cl: cl.replace('_{}'.format(s),''), axis=1) for s in usd_citi_otm_strikes}

aud_citi_otm_data_dict['RR50'] = aud_citi_otm_data_dict['+50'] - aud_citi_otm_data_dict['-50']
aud_citi_otm_data_dict['RR25'] = aud_citi_otm_data_dict['+25'] - aud_citi_otm_data_dict['-25']
aud_citi_otm_data_dict['RR10'] = aud_citi_otm_data_dict['+10'] - aud_citi_otm_data_dict['-10']
aud_citi_otm_data_dict['BF50'] = aud_citi_otm_data_dict['+50'] + aud_citi_otm_data_dict['-50'] - aud_citi_otm_data_dict['+0'] * 2
aud_citi_otm_data_dict['BF25'] = aud_citi_otm_data_dict['+25'] + aud_citi_otm_data_dict['-25'] - aud_citi_otm_data_dict['+0'] * 2
aud_citi_otm_data_dict['BF10'] = aud_citi_otm_data_dict['+10'] + aud_citi_otm_data_dict['-10'] - aud_citi_otm_data_dict['+0'] * 2





#find matching swap from citi

#usd
#usd_citi_ivol_matching_swap_data = usd_citi_swap_data[list(chain(*[[f'{e}_{t}' for t in usd_ivol_tails] for e in usd_ivol_exp]))]
usd_citi_ivol_matching_swap_data = usd_citi_swap_data[list(chain(*[[f'{e}_{t}' for t in usd_citi_otm_tails] for e in usd_citi_otm_exp]))]

#aud same tail and exp 
aud_citi_ivol_matching_swap_data = aud_citi_swap_data[list(chain(*[[f'{e}_{t}' for t in usd_citi_otm_tails] for e in usd_citi_otm_exp]))]




#convert ivol to premium, x 100 to get to bps
#usd
usd_citi_ivol_premium_data_3m = calc_premium(usd_citi_otm_data.iloc[-60:,:], usd_citi_ivol_matching_swap_data) * 100
usd_citi_ivol_premium_data_3m_dict = {s: usd_citi_ivol_premium_data_3m[[c for c in usd_citi_ivol_premium_data_3m.columns if s in c]].rename(lambda cl: cl.replace('_{}'.format(s),''), axis=1) for s in usd_citi_otm_strikes}

usd_citi_ivol_premium_data_3m_dict['RR50'] = usd_citi_ivol_premium_data_3m_dict['+50'] - usd_citi_ivol_premium_data_3m_dict['-50']
usd_citi_ivol_premium_data_3m_dict['RR25'] = usd_citi_ivol_premium_data_3m_dict['+25'] - usd_citi_ivol_premium_data_3m_dict['-25']
usd_citi_ivol_premium_data_3m_dict['RR10'] = usd_citi_ivol_premium_data_3m_dict['+10'] - usd_citi_ivol_premium_data_3m_dict['-10']
usd_citi_ivol_premium_data_3m_dict['BF50'] = usd_citi_ivol_premium_data_3m_dict['+50'] + usd_citi_ivol_premium_data_3m_dict['-50'] - usd_citi_ivol_premium_data_3m_dict['+0'] * 1 # for premium only times 1
usd_citi_ivol_premium_data_3m_dict['BF25'] = usd_citi_ivol_premium_data_3m_dict['+25'] + usd_citi_ivol_premium_data_3m_dict['-25'] - usd_citi_ivol_premium_data_3m_dict['+0'] * 1
usd_citi_ivol_premium_data_3m_dict['BF10'] = usd_citi_ivol_premium_data_3m_dict['+10'] + usd_citi_ivol_premium_data_3m_dict['-10'] - usd_citi_ivol_premium_data_3m_dict['+0'] * 1
 
#aud

aud_citi_ivol_premium_data_3m = calc_premium(aud_citi_otm_data.iloc[-60:,:], aud_citi_ivol_matching_swap_data) * 100
aud_citi_ivol_premium_data_3m_dict = {s: aud_citi_ivol_premium_data_3m[[c for c in aud_citi_ivol_premium_data_3m.columns if s in c]].rename(lambda cl: cl.replace('_{}'.format(s),''), axis=1) for s in usd_citi_otm_strikes}

aud_citi_ivol_premium_data_3m_dict['RR50'] = aud_citi_ivol_premium_data_3m_dict['+50'] - aud_citi_ivol_premium_data_3m_dict['-50']
aud_citi_ivol_premium_data_3m_dict['RR25'] = aud_citi_ivol_premium_data_3m_dict['+25'] - aud_citi_ivol_premium_data_3m_dict['-25']
aud_citi_ivol_premium_data_3m_dict['RR10'] = aud_citi_ivol_premium_data_3m_dict['+10'] - aud_citi_ivol_premium_data_3m_dict['-10']
aud_citi_ivol_premium_data_3m_dict['BF50'] = aud_citi_ivol_premium_data_3m_dict['+50'] + aud_citi_ivol_premium_data_3m_dict['-50'] - aud_citi_ivol_premium_data_3m_dict['+0'] * 1 # for premium only times 1
aud_citi_ivol_premium_data_3m_dict['BF25'] = aud_citi_ivol_premium_data_3m_dict['+25'] + aud_citi_ivol_premium_data_3m_dict['-25'] - aud_citi_ivol_premium_data_3m_dict['+0'] * 1
aud_citi_ivol_premium_data_3m_dict['BF10'] = aud_citi_ivol_premium_data_3m_dict['+10'] + aud_citi_ivol_premium_data_3m_dict['-10'] - aud_citi_ivol_premium_data_3m_dict['+0'] * 1





#covernt swap rate to curve gradiant

#usd
usd_citi_swap_gradiant_col = ['0Y_3M','3M_3M','6M_6M','1Y_1Y','2Y_1Y','3Y_1Y','4Y_1Y','5Y_1Y','6Y_1Y',
 '7Y_1Y','8Y_1Y','9Y_1Y','10Y_1Y','11Y_1Y','12Y_1Y','13Y_1Y','14Y_1Y','15Y_1Y','20Y_1Y']


usd_citi_swap_gradiant_data = usd_citi_swap_data[usd_citi_swap_gradiant_col]
usd_citi_swap_gradiant_data = {usd_citi_swap_gradiant_col[i] + '/' + usd_citi_swap_gradiant_col[i + 1] : usd_citi_swap_gradiant_data.iloc[:,i + 1] - usd_citi_swap_gradiant_data.iloc[:,i]  for i in np.arange(len(usd_citi_swap_gradiant_col)-1)}
usd_citi_swap_gradiant_data = pd.DataFrame(usd_citi_swap_gradiant_data)
usd_citi_swap_gradiant_data = (usd_citi_swap_gradiant_data * 100).round(1)

#aud

aud_citi_swap_gradiant_data = aud_citi_swap_data[usd_citi_swap_gradiant_col]
aud_citi_swap_gradiant_data = {usd_citi_swap_gradiant_col[i] + '/' + usd_citi_swap_gradiant_col[i + 1] : aud_citi_swap_gradiant_data.iloc[:,i + 1] - aud_citi_swap_gradiant_data.iloc[:,i]  for i in np.arange(len(usd_citi_swap_gradiant_col)-1)}
aud_citi_swap_gradiant_data = pd.DataFrame(aud_citi_swap_gradiant_data)
aud_citi_swap_gradiant_data = (usd_citi_swap_gradiant_data * 100).round(1)





####################################################################################################
##
## Model 
## 
####################################################################################################

def plot_curves(*curves):
    
    from matplotlib.ticker import FuncFormatter
    from statistics.derivative import quantlibutils as utils
    
    fig, ax = utils.plot()
    ax.yaxis.set_major_formatter(FuncFormatter(lambda r,pos: utils.format_rate(r)))
    ax.set_xlim(0,15)
    ax.set_xticks([0,5,10,15])
    times = np.linspace(0.0, 15.0, 400)
    for curve, style in curves:
        rates = [ curve.zeroRate(t, ql.Continuous).rate() for t in times ]
        ax.plot(times, rates, style)

def plot_curve(curve):
    plot_curves((curve,'-'))




# date
t = ql.Date().todaysDate() #which country is this??
us_calendar = ql.UnitedStates() 
t_plus2 = us_calendar.advance(t, 2, ql.Days)
input_tenor_data = usd_libor_data.iloc[-1].rename(lambda x: x.replace('LIBOR_','')).to_dict()

#helper for the forecast rates helper
us_libor_curve_inputs = ql.RateHelperVector()

#adding the market quotes to the helper collection
for t, r in input_tenor_data.items():
    if t == '3M':
        us_libor_curve_inputs.append(ql.DepositRateHelper(r/100, ql.USDLibor(ql.Period('3M'))))
    else:
        par_swap = ql.UsdLiborSwapIsdaFixAm(ql.Period(t))  
        us_libor_curve_inputs.append(ql.SwapRateHelper(r/100, par_swap)) 


#bootstrap the curve. 2 methods here: peicewiseflatforwards are what bbg uses for default

#us_libor_curve_fitted = ql.PiecewiseLogLinearDiscount(t_plus2, us_libor_curve_inputs, ql.Thirty360())
us_libor_curve_fitted = ql.PiecewiseFlatForward(t_plus2, us_libor_curve_inputs, ql.Thirty360())

# test YieldTermStructureHandle vs RelinkableYieldTermStructureHandle
# -- us_libor_curve_yts = ql.YieldTermStructureHandle(us_libor_curve_fitted)
# relinkable produces the same result
# this is the curve handle
us_libor_curve_yts = ql.RelinkableYieldTermStructureHandle(us_libor_curve_fitted)



#here the same libor term structure handler (us_libor_curve_yts) is used to populate the discount engine.
us_libor_pricing_engine = ql.DiscountingSwapEngine(us_libor_curve_yts)

#use the term structure handler above to create a usd libor fixing for forecasting floating rate fixings.
usdlibor3m_index = ql.USDLibor(ql.Period('3M'), us_libor_curve_yts)



####################################################################################################
# libor pricing engine - Price all swaps for carry roll
####################################################################################################

usd_swaps_to_price = list(set(np.concatenate([[v2.columns.tolist() for k2,v2 in usd_swap_fly_data[k].items()] for k in usd_swap_fly_data.keys()]).flat))


carryroll_store = {}

#carryroll_store_before_changing_relink = carryroll_store


for s in usd_swaps_to_price:
    s_fwd = s.split('_')[1]
    s_tenor = s.split('_')[2]
    #going to do 3 price here
    ql_s_tenor = ql.Period('{}'.format(s_tenor))
    ql_f_tenor = ql.Period('{}'.format(s_fwd))
    
    #now create and price the swap with the fixings and pricing discount engine.   
    s_priced = ql.MakeVanillaSwap(ql_s_tenor, usdlibor3m_index, 0.01, ql_f_tenor, pricingEngine=us_libor_pricing_engine,
                                  fixedLegDayCount=ql.Thirty360(),floatingLegDayCount=ql.Actual360()).fairRate()         
        
    #print(f"{s_fwd}, {s_tenor}, {s_priced.fairRate()*100:.6f}")

    #rolled
    ql_f_tenor_rolled = ql_f_tenor if s_fwd.startswith('0') else ql_f_tenor - ql.Period('3M')
    ql_s_tenor_rolled = ql_s_tenor- ql.Period('3M') if s_fwd.startswith('0') else ql_s_tenor
    
    s_priced_rolled = ql.MakeVanillaSwap(ql_s_tenor_rolled, usdlibor3m_index, 0.01, ql_f_tenor_rolled, 
                                  pricingEngine=us_libor_pricing_engine, fixedLegDayCount=ql.Thirty360(),
                                  floatingLegDayCount=ql.Actual360()).fairRate()
            
    #aged
    ql_f_tenor_aged = ql_f_tenor + ql.Period('3M') if s_fwd.startswith('0') else ql_f_tenor 
    ql_s_tenor_aged = ql_s_tenor- ql.Period('3M') if s_fwd.startswith('0') else ql_s_tenor
    
    s_priced_aged = ql.MakeVanillaSwap(ql_s_tenor_aged, usdlibor3m_index, 0.01, ql_f_tenor_aged, 
                                  pricingEngine=us_libor_pricing_engine, fixedLegDayCount=ql.Thirty360(),
                                  floatingLegDayCount=ql.Actual360()).fairRate()
    
    
    carry = (s_priced_aged - s_priced) 
    roll = (s_priced - s_priced_rolled) 
    
    carryroll_store[(str(ql_f_tenor), str(ql_s_tenor))] = {'rate': s_priced * 100, 'carry': carry * 100, 'roll': roll * 100, 'cr': (carry + roll) * 100}
        
    #print(f"{ql_f_tenor}, {ql_s_tenor}, {s_priced_rolled * 100:.6f} {carry*10000: .2f} {roll*10000: .2f}")
                    


####################################################################################################
# libor pricing engine - Price all swaps for dv01
####################################################################################################


def calc_usd_libor_swap_analytical_risk(libor_curve_from_bmk_data, discount_curve, forward, tenor, bump_discount_curve=False):
    
    def bp(n):
        return ql.QuoteHandle(ql.SimpleQuote(n*0.0001))
    
    #use relinkable handle to price swap
    curve_handle = ql.RelinkableYieldTermStructureHandle(libor_curve_from_bmk_data)    
    curve_handle_base = ql.YieldTermStructureHandle(libor_curve_from_bmk_data)    
    #default no curve shift
    curve_handle.linkTo(ql.ZeroSpreadedTermStructure(curve_handle_base,bp(0)))
        
    #disc pricing engine    
    discount_curve_handle = ql.RelinkableYieldTermStructureHandle(discount_curve)
    discount_curve_handle_base = ql.YieldTermStructureHandle(discount_curve)  
    #default no curve shift
    discount_curve_handle.linkTo(ql.ZeroSpreadedTermStructure(discount_curve_handle_base,bp(0)))
    
    #create the pricing engine with the discount curve
    #pricing_engine = ql.DiscountingSwapEngine(ql.YieldTermStructureHandle(discount_curve))
    pricing_engine = ql.DiscountingSwapEngine(discount_curve_handle)
 
    
    #usdlibor3m_index created with the forecast curve
    usdlibor3m_index = ql.USDLibor(ql.Period('3M'),  curve_handle)
    
    #1st price to get the par rate
    par_swap = ql.MakeVanillaSwap(ql.Period(tenor), usdlibor3m_index, 
                              0.01, ql.Period(forward), 
                              pricingEngine=pricing_engine,
                              fixedLegDayCount=ql.Thirty360(),
                              floatingLegDayCount=ql.Actual360())    
    par_rate = par_swap.fairRate()
    
    print(par_rate)

    #create the par swap now
    par_swap = ql.MakeVanillaSwap(ql.Period(tenor), usdlibor3m_index, 
                              par_rate, ql.Period(forward), 
                              pricingEngine=pricing_engine,
                              fixedLegDayCount=ql.Thirty360(),
                              floatingLegDayCount=ql.Actual360())

    #bump libor curve up by 1bp
    curve_handle.linkTo(ql.ZeroSpreadedTermStructure(curve_handle_base, bp(1)))
    if (bump_discount_curve):
        discount_curve_handle.linkTo(ql.ZeroSpreadedTermStructure(discount_curve_handle_base, bp(1)))
     
    a = par_swap.NPV()
    print(par_swap.fairRate())
    
    #bump libor curve down by 1bp
    curve_handle.linkTo(ql.ZeroSpreadedTermStructure(curve_handle_base, bp(-1)))
    if (bump_discount_curve):
        discount_curve_handle.linkTo(ql.ZeroSpreadedTermStructure(discount_curve_handle_base, bp(-1)))
      
    b = par_swap.NPV()
    print(par_swap.fairRate())
    
    analystical_risk = abs(a - b)/2 * 10000    
    
    return analystical_risk

    
 
calc_usd_libor_swap_analytical_risk(us_libor_curve_fitted, us_libor_curve_fitted, '0Y', '10Y')
calc_usd_libor_swap_analytical_risk(us_libor_curve_fitted, us_libor_curve_fitted, '0Y', '5Y')
calc_usd_libor_swap_analytical_risk(us_libor_curve_fitted, us_libor_curve_fitted, '5Y', '1Y')
    


#################################################
# bp = 0.0001

# #back up the old curve
# base_curve = ql.YieldTermStructureHandle(us_libor_curve_fitted)

# # without the bump
# us_libor_curve_yts.linkTo(ql.ZeroSpreadedTermStructure(base_curve, ql.QuoteHandle(ql.SimpleQuote(0*bp))))
# us_libor_pricing_engine = ql.DiscountingSwapEngine(us_libor_curve_yts)

# # 1st price to get the par rate
# par_swap = ql.MakeVanillaSwap(ql.Period('10Y'), usdlibor3m_index, 
#                               0.01, ql.Period('0Y'), 
#                               pricingEngine=us_libor_pricing_engine,
#                               fixedLegDayCount=ql.Thirty360(),floatingLegDayCount=ql.Actual360())

# par_rate = par_swap.fairRate()

# #2nd price with the par rate
# s1 = ql.MakeVanillaSwap(ql.Period('10Y'), usdlibor3m_index, par_rate, ql.Period('0Y'), pricingEngine=us_libor_pricing_engine,
#                                   fixedLegDayCount=ql.Thirty360(),floatingLegDayCount=ql.Actual360())

# print(s1.NPV())

# #shift curve down by 1bp
# us_libor_curve_yts.linkTo(ql.ZeroSpreadedTermStructure(base_curve, ql.QuoteHandle(ql.SimpleQuote(-1 * bp))))

# a = s1.NPV()

# print(s1.NPV())

# #shift curve up by 1bp
# us_libor_curve_yts.linkTo(ql.ZeroSpreadedTermStructure(base_curve, ql.QuoteHandle(ql.SimpleQuote(1 * bp))))

# b = s1.NPV()

# print(s1.NPV())
    
# analystical_risk = abs(a - b)/2 * 10000    




####################################################################################################
# VOL BBG
####################################################################################################


def make_exp_tail_ratio(data, pair_idx = None, idx = None, pair_exp=True, rfmt=lambda x: 'IVOL_{}'.format(x)): 
    output = {}
    ratio_list = []
    for i in np.arange(len(pair_idx)-1):    
        for j in idx:            
            if pair_exp:
                f = rfmt('{}_{}'.format(pair_idx[i], j))
                b = rfmt('{}_{}'.format(pair_idx[i+1], j))            
                output[rfmt('{}/{}_{}'.format(pair_idx[i], pair_idx[i+1], j))] = data[f] / data[b]            
            else:
                f = rfmt('{}_{}'.format(j, pair_idx[i]))
                b = rfmt('{}_{}'.format(j, pair_idx[i+1]))
                output[rfmt('{}_{}/{}'.format(j, pair_idx[i], pair_idx[i+1]))] = data[f] / data[b]                
            if '{}/{}'.format(pair_idx[i], pair_idx[i+1]) not in ratio_list:
                ratio_list.append('{}/{}'.format(pair_idx[i], pair_idx[i+1]))     
    return pd.DataFrame(output), ratio_list


def make_ivol_rvol_ratio(ivol_data, rvol_data):
    _ivol_data = ivol_data.rename(lambda c: c.replace('IVOL_',''), axis=1)    
    _rvol_data = rvol_data.rename(lambda c: c.replace('RVOL_',''), axis=1)  
    #assuming there are more rvol data always
    _rvol_data = _rvol_data.reindex(_ivol_data.index, axis=0)
    _ivr = (_ivol_data/_rvol_data).rename(lambda c: f'IV/RV_{c}', axis=1).ffill()
    return _ivr
    
    


bbg_vol_date = usd_ivol_data.index[-1].strftime('%Y-%m-%d')   


# derive extra data from vol data

#exp and tail ratio
#usd
usd_ivol_data_exp_ratio, usd_ivol_data_exp_ratio_pairs = make_exp_tail_ratio(usd_ivol_data, pair_idx=usd_ivol_exp, idx=usd_ivol_tails, pair_exp=True)
usd_ivol_data_tail_ratio, usd_ivol_data_tail_ratio_pairs = make_exp_tail_ratio(usd_ivol_data, pair_idx=usd_ivol_tails, idx=usd_ivol_exp, pair_exp=False)

#aud
aud_ivol_data_exp_ratio, aud_ivol_data_exp_ratio_pairs = make_exp_tail_ratio(aud_bbg_ivol_data, pair_idx=usd_ivol_exp, idx=usd_ivol_tails, pair_exp=True)
aud_ivol_data_tail_ratio, aud_ivol_data_tail_ratio_pairs = make_exp_tail_ratio(aud_bbg_ivol_data, pair_idx=usd_ivol_tails, idx=usd_ivol_exp, pair_exp=False)



#ivol/rvol ratio

#usd
usd_ivolrvol_ratio_dict = {k: make_ivol_rvol_ratio(usd_ivol_data, v) for (k,v) in usd_rvol_data_dict.items()}

#aud
aud_ivolrvol_ratio_dict = {k: make_ivol_rvol_ratio(aud_bbg_ivol_data, v) for (k,v) in aud_rvol_data_dict.items()}

 

#momentum

#usd
usd_ivol_data_1m_chg = usd_ivol_data.diff(20)
usd_ivol_data_3m_chg = usd_ivol_data.diff(60)

#aud
aud_ivol_data_1m_chg = aud_bbg_ivol_data.diff(20)
aud_ivol_data_3m_chg = aud_bbg_ivol_data.diff(60)



#zsc

#usd
usd_ivol_data_1m_zsc = make_zsc(usd_ivol_data.iloc[-20:,:])
usd_ivol_data_3m_zsc = make_zsc(usd_ivol_data.iloc[-60:,:])
usd_ivol_data_1y_zsc = make_zsc(usd_ivol_data.iloc[-250:,:])
usd_ivol_data_3y_zsc = make_zsc(usd_ivol_data.iloc[-750:,:])
usd_ivol_data_10y_zsc = make_zsc(usd_ivol_data.iloc[-2500:,:])

usd_ivol_data_exp_ratio_3m_zsc = make_zsc(usd_ivol_data_exp_ratio.iloc[-60:,:])
usd_ivol_data_tail_ratio_3m_zsc = make_zsc(usd_ivol_data_tail_ratio.iloc[-60:,:])

#aud
aud_ivol_data_1m_zsc = make_zsc(aud_bbg_ivol_data.iloc[-20:,:])
aud_ivol_data_3m_zsc = make_zsc(aud_bbg_ivol_data.iloc[-60:,:])
aud_ivol_data_1y_zsc = make_zsc(aud_bbg_ivol_data.iloc[-250:,:])
aud_ivol_data_3y_zsc = make_zsc(aud_bbg_ivol_data.iloc[-750:,:])
aud_ivol_data_10y_zsc = make_zsc(aud_bbg_ivol_data.iloc[-2500:,:])

aud_ivol_data_exp_ratio_3m_zsc = make_zsc(aud_ivol_data_exp_ratio.iloc[-60:,:])
aud_ivol_data_tail_ratio_3m_zsc = make_zsc(aud_ivol_data_tail_ratio.iloc[-60:,:])




#
usd_ivolrvol_ratio_1y_zsc_dict = {k: make_zsc(v.iloc[-250:,:]) for (k,v) in usd_ivolrvol_ratio_dict.items()}

aud_ivolrvol_ratio_1y_zsc_dict = {k: make_zsc(v.iloc[-250:,:]) for (k,v) in aud_ivolrvol_ratio_dict.items()}


##########
#pvt
##########

#ivol

#usd
usd_ivol_data_current_pvt = pvt_ds_to_table(usd_ivol_data.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')
usd_ivol_1m_change_current_pvt = pvt_ds_to_table(usd_ivol_data_1m_chg.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')
usd_ivol_3m_change_current_pvt = pvt_ds_to_table(usd_ivol_data_3m_chg.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')

usd_ivol_data_1m_zsc_current_pvt = pvt_ds_to_table(usd_ivol_data_1m_zsc.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')
usd_ivol_data_3m_zsc_current_pvt = pvt_ds_to_table(usd_ivol_data_3m_zsc.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')
usd_ivol_data_1y_zsc_current_pvt = pvt_ds_to_table(usd_ivol_data_1m_zsc.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')
usd_ivol_data_3y_zsc_current_pvt = pvt_ds_to_table(usd_ivol_data_3m_zsc.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')
usd_ivol_data_10y_zsc_current_pvt = pvt_ds_to_table(usd_ivol_data_10y_zsc.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')


#aud
aud_ivol_data_current_pvt = pvt_ds_to_table(aud_bbg_ivol_data.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')
aud_ivol_1m_change_current_pvt = pvt_ds_to_table(aud_ivol_data_1m_chg.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')
aud_ivol_3m_change_current_pvt = pvt_ds_to_table(aud_ivol_data_3m_chg.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')

aud_ivol_data_1m_zsc_current_pvt = pvt_ds_to_table(aud_ivol_data_1m_zsc.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')
aud_ivol_data_3m_zsc_current_pvt = pvt_ds_to_table(aud_ivol_data_3m_zsc.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')
aud_ivol_data_1y_zsc_current_pvt = pvt_ds_to_table(aud_ivol_data_1m_zsc.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')
aud_ivol_data_3y_zsc_current_pvt = pvt_ds_to_table(aud_ivol_data_3m_zsc.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')
aud_ivol_data_10y_zsc_current_pvt = pvt_ds_to_table(aud_ivol_data_10y_zsc.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')




#exp ratio tail ratio

#usd

usd_ivol_data_exp_ratio_current_pvt = pvt_ds_to_table(usd_ivol_data_exp_ratio.iloc[-1,:].round(2), usd_ivol_data_exp_ratio_pairs, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')
usd_ivol_data_tail_ratio_current_pvt = pvt_ds_to_table(usd_ivol_data_tail_ratio.iloc[-1,:].round(2), usd_ivol_exp, usd_ivol_data_tail_ratio_pairs, ridx_name='Exp', cidx_name='Tail')

usd_ivol_data_exp_ratio_3m_zsc_current_pvt = pvt_ds_to_table(usd_ivol_data_exp_ratio_3m_zsc.iloc[-1,:].round(1), usd_ivol_data_exp_ratio_pairs, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')
usd_ivol_data_tail_ratio_3m_zsc_current_pvt = pvt_ds_to_table(usd_ivol_data_tail_ratio_3m_zsc.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_data_tail_ratio_pairs, ridx_name='Exp', cidx_name='Tail')

#aud
aud_ivol_data_exp_ratio_current_pvt = pvt_ds_to_table(aud_ivol_data_exp_ratio.iloc[-1,:].round(2), usd_ivol_data_exp_ratio_pairs, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')
aud_ivol_data_tail_ratio_current_pvt = pvt_ds_to_table(aud_ivol_data_tail_ratio.iloc[-1,:].round(2), usd_ivol_exp, usd_ivol_data_tail_ratio_pairs, ridx_name='Exp', cidx_name='Tail')
aud_ivol_data_exp_ratio_3m_zsc_current_pvt = pvt_ds_to_table(aud_ivol_data_exp_ratio_3m_zsc.iloc[-1,:].round(1), usd_ivol_data_exp_ratio_pairs, usd_ivol_tails, ridx_name='Exp', cidx_name='Tail')
aud_ivol_data_tail_ratio_3m_zsc_current_pvt = pvt_ds_to_table(aud_ivol_data_tail_ratio_3m_zsc.iloc[-1,:].round(1), usd_ivol_exp, usd_ivol_data_tail_ratio_pairs, ridx_name='Exp', cidx_name='Tail')



#ivol/rvol


# usd

usd_ivolrvol_ratio_current_pvt = {k: pvt_ds_to_table(v.iloc[-1,:].round(2), usd_ivol_exp, usd_ivol_tails, 
                                                            rfmt=lambda x: 'IV/RV_{}'.format(x), 
                                                            ridx_name='Exp', cidx_name='Tail') for (k,v) in usd_ivolrvol_ratio_dict.items()} 


usd_ivolrvol_ratio_1y_zsc_current_pvt = {k: pvt_ds_to_table(v.iloc[-1,:].round(2), usd_ivol_exp, usd_ivol_tails, 
                                                            rfmt=lambda x: 'IV/RV_{}'.format(x), 
                                                            ridx_name='Exp', cidx_name='Tail') for (k,v) in usd_ivolrvol_ratio_1y_zsc_dict.items()} 
# aud

aud_ivolrvol_ratio_current_pvt = {k: pvt_ds_to_table(v.iloc[-1,:].round(2), usd_ivol_exp, usd_ivol_tails, 
                                                            rfmt=lambda x: 'IV/RV_{}'.format(x), 
                                                            ridx_name='Exp', cidx_name='Tail') for (k,v) in aud_ivolrvol_ratio_dict.items()} 


aud_ivolrvol_ratio_1y_zsc_current_pvt = {k: pvt_ds_to_table(v.iloc[-1,:].round(2), usd_ivol_exp, usd_ivol_tails, 
                                                            rfmt=lambda x: 'IV/RV_{}'.format(x), 
                                                            ridx_name='Exp', cidx_name='Tail') for (k,v) in aud_ivolrvol_ratio_1y_zsc_dict.items()} 


#################
#combine
#################

#ivol

#usd
usd_ivol_data_current_pvt_with_1m_chg = combine_same_fmt_tables(usd_ivol_data_current_pvt, usd_ivol_1m_change_current_pvt)
usd_ivol_data_current_pvt_with_3m_chg = combine_same_fmt_tables(usd_ivol_data_current_pvt, usd_ivol_3m_change_current_pvt)

usd_ivol_data_current_pvt_with_1m_zsc = combine_same_fmt_tables(usd_ivol_data_current_pvt, usd_ivol_data_1m_zsc_current_pvt)
usd_ivol_data_current_pvt_with_3m_zsc = combine_same_fmt_tables(usd_ivol_data_current_pvt, usd_ivol_data_3m_zsc_current_pvt)
usd_ivol_data_current_pvt_with_1y_zsc = combine_same_fmt_tables(usd_ivol_data_current_pvt, usd_ivol_data_1y_zsc_current_pvt)
usd_ivol_data_current_pvt_with_3y_zsc = combine_same_fmt_tables(usd_ivol_data_current_pvt, usd_ivol_data_3y_zsc_current_pvt)
usd_ivol_data_current_pvt_with_10y_zsc = combine_same_fmt_tables(usd_ivol_data_current_pvt, usd_ivol_data_10y_zsc_current_pvt)

#aud
aud_ivol_data_current_pvt_with_1m_chg = combine_same_fmt_tables(aud_ivol_data_current_pvt, aud_ivol_1m_change_current_pvt)
aud_ivol_data_current_pvt_with_3m_chg = combine_same_fmt_tables(aud_ivol_data_current_pvt, aud_ivol_3m_change_current_pvt)

aud_ivol_data_current_pvt_with_1m_zsc = combine_same_fmt_tables(aud_ivol_data_current_pvt, aud_ivol_data_1m_zsc_current_pvt)
aud_ivol_data_current_pvt_with_3m_zsc = combine_same_fmt_tables(aud_ivol_data_current_pvt, aud_ivol_data_3m_zsc_current_pvt)
aud_ivol_data_current_pvt_with_1y_zsc = combine_same_fmt_tables(aud_ivol_data_current_pvt, aud_ivol_data_1y_zsc_current_pvt)
aud_ivol_data_current_pvt_with_3y_zsc = combine_same_fmt_tables(aud_ivol_data_current_pvt, aud_ivol_data_3y_zsc_current_pvt)
aud_ivol_data_current_pvt_with_10y_zsc = combine_same_fmt_tables(aud_ivol_data_current_pvt, aud_ivol_data_10y_zsc_current_pvt)




#exp ratio tail ratio

#usd
usd_ivol_data_exp_ratio_current_pvt_with_3m_zsc = combine_same_fmt_tables(usd_ivol_data_exp_ratio_current_pvt, usd_ivol_data_exp_ratio_3m_zsc_current_pvt)
usd_ivol_data_tail_ratio_current_pvt_with_3m_zsc = combine_same_fmt_tables(usd_ivol_data_tail_ratio_current_pvt, usd_ivol_data_tail_ratio_3m_zsc_current_pvt)

#aud
aud_ivol_data_exp_ratio_current_pvt_with_3m_zsc = combine_same_fmt_tables(aud_ivol_data_exp_ratio_current_pvt, aud_ivol_data_exp_ratio_3m_zsc_current_pvt)
aud_ivol_data_tail_ratio_current_pvt_with_3m_zsc = combine_same_fmt_tables(aud_ivol_data_tail_ratio_current_pvt, aud_ivol_data_tail_ratio_3m_zsc_current_pvt)


#ivol/rvol ratio
usd_ivolrvol_ratio_current_pvt_with_1y_zsc_dict = {k: combine_same_fmt_tables(v, usd_ivolrvol_ratio_1y_zsc_current_pvt[k]) for (k,v) in usd_ivolrvol_ratio_current_pvt.items()}

aud_ivolrvol_ratio_current_pvt_with_1y_zsc_dict = {k: combine_same_fmt_tables(v, aud_ivolrvol_ratio_1y_zsc_current_pvt[k]) for (k,v) in aud_ivolrvol_ratio_current_pvt.items()}



####################################################################################################
# VOL Citi
####################################################################################################

citi_vol_date = usd_citi_otm_data.index[-1].strftime('%Y-%m-%d')   

usd_citi_otm_data_dict # dict

usd_citi_ivol_premium_data_3m_dict #dict


#momentum
usd_citi_otm_data_dict_1m_chg = {k: v.diff(20) for (k, v) in usd_citi_otm_data_dict.items()}
usd_citi_otm_data_dict_3m_chg = {k: v.diff(60) for (k, v) in usd_citi_otm_data_dict.items()}


#zsc
usd_citi_otm_data_dict_1m_zsc = {k: make_zsc(v.iloc[-20:]) for (k, v) in usd_citi_otm_data_dict.items()}
usd_citi_otm_data_dict_3m_zsc = {k: make_zsc(v.iloc[-60:]) for (k, v) in usd_citi_otm_data_dict.items()}


usd_citi_ivol_premium_data_3m_dict_1m_zsc = {k: make_zsc(v.iloc[-20:]) for (k, v) in usd_citi_ivol_premium_data_3m_dict.items()}
usd_citi_ivol_premium_data_3m_dict_3m_zsc = {k: make_zsc(v.iloc[-60:]) for (k, v) in usd_citi_ivol_premium_data_3m_dict.items()}

usd_citi_ivol_premium_data_3m_all_3m_zsc = make_zsc(usd_citi_ivol_premium_data_3m.iloc[-60:])


#pvt 
# - current
usd_citi_otm_data_dict_current_pvt = {k: pvt_ds_to_table(v.iloc[-1,:].round(1), usd_citi_otm_exp, usd_citi_otm_tails,
                                                    rfmt=lambda x: '{}'.format(x),
                                                    ridx_name='Exp', cidx_name='Tail') for (k,v) in usd_citi_otm_data_dict.items()}
# - momeumt
usd_citi_otm_data_dict_1m_chg_pvt = {k: pvt_ds_to_table(v.iloc[-1,:].round(1), usd_citi_otm_exp, usd_citi_otm_tails,
                                                    rfmt=lambda x: '{}'.format(x),
                                                    ridx_name='Exp', cidx_name='Tail') for (k,v) in usd_citi_otm_data_dict_1m_chg.items()}
usd_citi_otm_data_dict_3m_chg_pvt = {k: pvt_ds_to_table(v.iloc[-1,:].round(1), usd_citi_otm_exp, usd_citi_otm_tails,
                                                    rfmt=lambda x: '{}'.format(x),
                                                    ridx_name='Exp', cidx_name='Tail') for (k,v) in usd_citi_otm_data_dict_3m_chg.items()}
# - zsc
usd_citi_otm_data_dict_1m_zsc_pvt = {k: pvt_ds_to_table(v.iloc[-1,:].round(1), usd_citi_otm_exp, usd_citi_otm_tails,
                                                    rfmt=lambda x: '{}'.format(x),
                                                    ridx_name='Exp', cidx_name='Tail') for (k,v) in usd_citi_otm_data_dict_1m_zsc.items()}
usd_citi_otm_data_dict_3m_zsc_pvt = {k: pvt_ds_to_table(v.iloc[-1,:].round(1), usd_citi_otm_exp, usd_citi_otm_tails,
                                                    rfmt=lambda x: '{}'.format(x),
                                                    ridx_name='Exp', cidx_name='Tail') for (k,v) in usd_citi_otm_data_dict_3m_zsc.items()}


usd_citi_ivol_premium_data_3m_dict_current_pvt = {k: pvt_ds_to_table(v.iloc[-1,:].round(1), usd_citi_otm_exp, usd_citi_otm_tails,
                                                    rfmt=lambda x: '{}'.format(x),
                                                    ridx_name='Exp', cidx_name='Tail') for (k,v) in usd_citi_ivol_premium_data_3m_dict.items()}


usd_citi_ivol_premium_data_3m_dict_1m_zsc_pvt = {k: pvt_ds_to_table(v.iloc[-1,:].round(1), usd_citi_otm_exp, usd_citi_otm_tails,
                                                    rfmt=lambda x: '{}'.format(x),
                                                    ridx_name='Exp', cidx_name='Tail') for (k,v) in usd_citi_ivol_premium_data_3m_dict_1m_zsc.items()}

usd_citi_ivol_premium_data_3m_dict_3m_zsc_pvt = {k: pvt_ds_to_table(v.iloc[-1,:].round(1), usd_citi_otm_exp, usd_citi_otm_tails,
                                                    rfmt=lambda x: '{}'.format(x),
                                                    ridx_name='Exp', cidx_name='Tail') for (k,v) in usd_citi_ivol_premium_data_3m_dict_3m_zsc.items()}



usd_citi_ivol_premium_data_3m_all_current_pvt = pvt_ds_to_table(usd_citi_ivol_premium_data_3m.iloc[-1,:].round(1), 
                                                                usd_citi_exp_tails_col_ordered, usd_citi_otm_strikes,
                                                                rfmt=lambda x: '{}'.format(x),
                                                                ridx_name='Structure', cidx_name='Strike').transpose()


usd_citi_ivol_premium_data_3m_all_current_zsc_pvt = pvt_ds_to_table(usd_citi_ivol_premium_data_3m_all_3m_zsc.iloc[-1,:].round(1), 
                                                                    usd_citi_exp_tails_col_ordered, usd_citi_otm_strikes,
                                                                    rfmt=lambda x: '{}'.format(x),
                                                                    ridx_name='Structure', cidx_name='Strike').transpose()











#combine
usd_citi_otm_data_dict_current_pvt_with_1m_chg = {k: combine_same_fmt_tables(v, usd_citi_otm_data_dict_1m_chg_pvt[k]) for (k,v) in usd_citi_otm_data_dict_current_pvt.items()} 
usd_citi_otm_data_dict_current_pvt_with_3m_chg = {k: combine_same_fmt_tables(v, usd_citi_otm_data_dict_3m_chg_pvt[k]) for (k,v) in usd_citi_otm_data_dict_current_pvt.items()} 
usd_citi_otm_data_dict_current_pvt_with_1m_zsc = {k: combine_same_fmt_tables(v, usd_citi_otm_data_dict_1m_zsc_pvt[k]) for (k,v) in usd_citi_otm_data_dict_current_pvt.items()} 
usd_citi_otm_data_dict_current_pvt_with_3m_zsc = {k: combine_same_fmt_tables(v, usd_citi_otm_data_dict_3m_zsc_pvt[k]) for (k,v) in usd_citi_otm_data_dict_current_pvt.items()} 

usd_citi_ivol_premium_data_3m_dict_current_pvt_with_1m_zsc = {k: combine_same_fmt_tables(v, usd_citi_ivol_premium_data_3m_dict_1m_zsc_pvt[k]) \
                                                              for (k,v) in usd_citi_ivol_premium_data_3m_dict_current_pvt.items()} 
usd_citi_ivol_premium_data_3m_dict_current_pvt_with_3m_zsc = {k: combine_same_fmt_tables(v, usd_citi_ivol_premium_data_3m_dict_3m_zsc_pvt[k]) \
                                                              for (k,v) in usd_citi_ivol_premium_data_3m_dict_current_pvt.items()} 

usd_citi_ivol_premium_data_3m_all_current_pvt_with_3m_zsc = combine_same_fmt_tables(usd_citi_ivol_premium_data_3m_all_current_pvt, 
                                                                                    usd_citi_ivol_premium_data_3m_all_current_zsc_pvt)    



###############################################################################################################
# vol plot
###############################################################################################################

wb1 = xw.Book(r'C:\Dev\Models\QuantModels\#applications\market_watch\ratesmonitor\ratesmonitor.xlsm')
sht1 = wb1.sheets('USD_Vol')


vrow = 2

_f = plot_heatmap(usd_ivol_1m_change_current_pvt, annot_data=usd_ivol_data_current_pvt_with_1m_chg, title=f'1m vol change ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '1M_Vol_Chg',f'B{vrow}')

_f = plot_heatmap(usd_ivol_3m_change_current_pvt, annot_data=usd_ivol_data_current_pvt_with_3m_chg, title=f'3m vol change  ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '3M_Vol_Chg',f'M{vrow}')

vrow = vrow + 18
_f = plot_heatmap(usd_ivol_data_1m_zsc_current_pvt, annot_data=usd_ivol_data_current_pvt_with_1m_zsc, title=f'1m vol zsc  ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '1M_Vol_Zsc',f'B{vrow}')

_f = plot_heatmap(usd_ivol_data_3m_zsc_current_pvt, annot_data=usd_ivol_data_current_pvt_with_3m_zsc, title=f'3m vol zsc ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '3M_Vol_Zsc',f'M{vrow}')

vrow = vrow + 18
_f = plot_heatmap(usd_ivol_data_1y_zsc_current_pvt, annot_data=usd_ivol_data_current_pvt_with_1y_zsc, title=f'1y vol zsc  ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '1Y_Vol_Zsc',f'B{vrow}')

_f = plot_heatmap(usd_ivol_data_3y_zsc_current_pvt, annot_data=usd_ivol_data_current_pvt_with_3y_zsc, title=f'3y vol zsc ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '3Y_Vol_Zsc',f'M{vrow}')

vrow = vrow + 18
_f = plot_heatmap(usd_ivol_data_exp_ratio_3m_zsc_current_pvt, annot_data=usd_ivol_data_exp_ratio_current_pvt_with_3m_zsc, title=f'3m vol exp ratio zsc ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '3M_Vol_Exp_Ratio_Zsc',f'B{vrow}')

_f = plot_heatmap(usd_ivol_data_tail_ratio_3m_zsc_current_pvt, annot_data=usd_ivol_data_tail_ratio_current_pvt_with_3m_zsc, title=f'3m vol tail ratio zsc ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '3M_Vol_Tail_Ratio_Zsc',f'M{vrow}')

vrow = vrow + 18
_f = plot_heatmap(usd_ivolrvol_ratio_current_pvt['1M'], annot_data=usd_ivolrvol_ratio_current_pvt_with_1y_zsc_dict['1M'], title=f'1m ivol/rvol 1y zsc ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '1M_IVolRVol_1Y_Zsc',f'B{vrow}')


_f = plot_heatmap(usd_ivolrvol_ratio_current_pvt['3M'], annot_data=usd_ivolrvol_ratio_current_pvt_with_1y_zsc_dict['3M'], title=f'3m ivol/rvol 1y zsc ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '3M_IVolRVol_1Y_Zsc',f'M{vrow}')


# -- citi
vrow = vrow + 18
_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_1m_chg_pvt[s], 
                           annot_data=usd_citi_otm_data_dict_current_pvt_with_1m_chg[s], title=t))('+25', f'ATM + 25, 1m change ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'ATM+25_1m_chg',f'B{vrow}')


_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_1m_chg_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_1m_chg[s], title=t))('+10', f'ATM + 10, 1m change ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'ATM+10_1m_chg',f'M{vrow}')


vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_1m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_1m_zsc[s], title=t))('+25', f'ATM + 25, 1m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'ATM+25_1m_zsc',f'B{vrow}')

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_1m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_1m_zsc[s], title=t))('+10', f'ATM + 10, 1m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'ATM+10_1m_zsc',f'M{vrow}')


vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_1m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_1m_zsc[s], title=t))('RR25', f'RR25, 1m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'RR25_1m_zsc',f'B{vrow}')

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_1m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_1m_zsc[s], title=t))('BF25', f'BF25, 1m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'BF25_1m_zsc',f'M{vrow}')

vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_1m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_1m_zsc[s], title=t))('RR50', f'RR50, 1m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'RR50_1m_zsc',f'B{vrow}')

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_1m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_1m_zsc[s], title=t))('BF50', f'BF50, 1m zsc ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'BF50_1m_zsc',f'M{vrow}')


vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_3m_chg_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_3m_chg[s], title=t))('+25', f'ATM + 25, 3m change ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'ATM+25_3m_chg',f'B{vrow}')


_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_3m_chg_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_3m_chg[s], title=t))('+10', f'ATM + 10, 3m change ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'ATM+10_3m_chg',f'M{vrow}')

vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_3m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_3m_zsc[s], title=t))('+25', f'ATM + 25, 3m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'ATM+25_3m_zsc',f'B{vrow}')

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_3m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_3m_zsc[s], title=t))('+10', f'ATM + 10, 3m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'ATM+10_3m_zsc',f'M{vrow}')


vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_3m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_3m_zsc[s], title=t))('RR25', f'RR25, 3m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'RR25_3m_zsc',f'B{vrow}')


_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_3m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_3m_zsc[s], title=t))('BF25', f'BF25, 3m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'BF25_3m_zsc',f'M{vrow}')


vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_3m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_3m_zsc[s], title=t))('RR50', f'RR50, 3m zsc ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'RR50_3m_zsc',f'B{vrow}')

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_3m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_3m_zsc[s], title=t))('BF50', f'BF50, 3m zsc ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'BF50_3m_zsc',f'M{vrow}')



vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_ivol_premium_data_3m_dict_1m_zsc_pvt[s], 
                                annot_data=usd_citi_ivol_premium_data_3m_dict_current_pvt_with_1m_zsc[s], title=t))('RR25', f'RR25 premium, 1m zsc ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'RR25_premium_1m_zsc',f'B{vrow}')


_f = (lambda s, t: plot_heatmap(usd_citi_ivol_premium_data_3m_dict_3m_zsc_pvt[s], 
                                annot_data=usd_citi_ivol_premium_data_3m_dict_current_pvt_with_3m_zsc[s], title=t))('RR50', f'RR50 premium, 1m zsc ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'RR50_premium_1m_zsc',f'M{vrow}')


vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_ivol_premium_data_3m_dict_1m_zsc_pvt[s], 
                                annot_data=usd_citi_ivol_premium_data_3m_dict_current_pvt_with_1m_zsc[s], title=t))('BF25', f'BF25 premium, 1m zsc ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'BF25_premium_1m_zsc',f'B{vrow}')


_f = (lambda s, t: plot_heatmap(usd_citi_ivol_premium_data_3m_dict_3m_zsc_pvt[s], 
                                annot_data=usd_citi_ivol_premium_data_3m_dict_current_pvt_with_3m_zsc[s], title=t))('BF50', f'BF50 premium, 1m zsc ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'BF50_premium_1m_zsc',f'M{vrow}')



vrow = vrow + 18

_f = (lambda t: plot_heatmap(usd_citi_ivol_premium_data_3m_all_current_pvt, 
                             annot_data=usd_citi_ivol_premium_data_3m_all_current_pvt_with_3m_zsc,
                             title=t, fsize = (35, 4)))(f'Vol premium surface, ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'Vol premium surface',f'B{vrow}')


vrow = vrow + 18

_f = (lambda t: plot_heatmap(usd_citi_ivol_premium_data_3m_all_current_zsc_pvt, 
                             annot_data=usd_citi_ivol_premium_data_3m_all_current_pvt_with_3m_zsc,
                             title=t, fsize = (35, 4)))(f'Vol premium surface, ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'Vol premium surface by zsc',f'B{vrow}')


#######################################################################################################


vrow = 2

_f = plot_heatmap(usd_ivol_1m_change_current_pvt, annot_data=usd_ivol_data_current_pvt_with_1m_chg, title=f'1m vol change ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '1M_Vol_Chg',f'B{vrow}')

_f = plot_heatmap(usd_ivol_3m_change_current_pvt, annot_data=usd_ivol_data_current_pvt_with_3m_chg, title=f'3m vol change  ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '3M_Vol_Chg',f'M{vrow}')

vrow = vrow + 18
_f = plot_heatmap(usd_ivol_data_1m_zsc_current_pvt, annot_data=usd_ivol_data_current_pvt_with_1m_zsc, title=f'1m vol zsc  ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '1M_Vol_Zsc',f'B{vrow}')

_f = plot_heatmap(usd_ivol_data_3m_zsc_current_pvt, annot_data=usd_ivol_data_current_pvt_with_3m_zsc, title=f'3m vol zsc ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '3M_Vol_Zsc',f'M{vrow}')

vrow = vrow + 18
_f = plot_heatmap(usd_ivol_data_1y_zsc_current_pvt, annot_data=usd_ivol_data_current_pvt_with_1y_zsc, title=f'1y vol zsc  ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '1Y_Vol_Zsc',f'B{vrow}')

_f = plot_heatmap(usd_ivol_data_3y_zsc_current_pvt, annot_data=usd_ivol_data_current_pvt_with_3y_zsc, title=f'3y vol zsc ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '3Y_Vol_Zsc',f'M{vrow}')

vrow = vrow + 18
_f = plot_heatmap(usd_ivol_data_exp_ratio_3m_zsc_current_pvt, annot_data=usd_ivol_data_exp_ratio_current_pvt_with_3m_zsc, title=f'3m vol exp ratio zsc ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '3M_Vol_Exp_Ratio_Zsc',f'B{vrow}')

_f = plot_heatmap(usd_ivol_data_tail_ratio_3m_zsc_current_pvt, annot_data=usd_ivol_data_tail_ratio_current_pvt_with_3m_zsc, title=f'3m vol tail ratio zsc ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '3M_Vol_Tail_Ratio_Zsc',f'M{vrow}')

vrow = vrow + 18
_f = plot_heatmap(aud_ivolrvol_ratio_current_pvt['1M'], annot_data=aud_ivolrvol_ratio_current_pvt_with_1y_zsc_dict['1M'], title=f'1m ivol/rvol 1y zsc ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '1M_IVolRVol_1Y_Zsc',f'B{vrow}')


_f = plot_heatmap(aud_ivolrvol_ratio_current_pvt['3M'], annot_data=aud_ivolrvol_ratio_current_pvt_with_1y_zsc_dict['3M'], title=f'3m ivol/rvol 1y zsc ({bbg_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, '3M_IVolRVol_1Y_Zsc',f'M{vrow}')


# -- citi
vrow = vrow + 18
_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_1m_chg_pvt[s], 
                           annot_data=usd_citi_otm_data_dict_current_pvt_with_1m_chg[s], title=t))('+25', f'ATM + 25, 1m change ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'ATM+25_1m_chg',f'B{vrow}')


_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_1m_chg_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_1m_chg[s], title=t))('+10', f'ATM + 10, 1m change ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'ATM+10_1m_chg',f'M{vrow}')


vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_1m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_1m_zsc[s], title=t))('+25', f'ATM + 25, 1m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'ATM+25_1m_zsc',f'B{vrow}')

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_1m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_1m_zsc[s], title=t))('+10', f'ATM + 10, 1m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'ATM+10_1m_zsc',f'M{vrow}')


vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_1m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_1m_zsc[s], title=t))('RR25', f'RR25, 1m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'RR25_1m_zsc',f'B{vrow}')

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_1m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_1m_zsc[s], title=t))('BF25', f'BF25, 1m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'BF25_1m_zsc',f'M{vrow}')

vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_1m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_1m_zsc[s], title=t))('RR50', f'RR50, 1m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'RR50_1m_zsc',f'B{vrow}')

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_1m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_1m_zsc[s], title=t))('BF50', f'BF50, 1m zsc ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'BF50_1m_zsc',f'M{vrow}')


vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_3m_chg_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_3m_chg[s], title=t))('+25', f'ATM + 25, 3m change ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'ATM+25_3m_chg',f'B{vrow}')


_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_3m_chg_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_3m_chg[s], title=t))('+10', f'ATM + 10, 3m change ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'ATM+10_3m_chg',f'M{vrow}')

vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_3m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_3m_zsc[s], title=t))('+25', f'ATM + 25, 3m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'ATM+25_3m_zsc',f'B{vrow}')

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_3m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_3m_zsc[s], title=t))('+10', f'ATM + 10, 3m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'ATM+10_3m_zsc',f'M{vrow}')


vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_3m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_3m_zsc[s], title=t))('RR25', f'RR25, 3m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'RR25_3m_zsc',f'B{vrow}')


_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_3m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_3m_zsc[s], title=t))('BF25', f'BF25, 3m zsc ({citi_vol_date})').get_figure()
add_fig_to_excel(_f, sht1, 'BF25_3m_zsc',f'M{vrow}')


vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_3m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_3m_zsc[s], title=t))('RR50', f'RR50, 3m zsc ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'RR50_3m_zsc',f'B{vrow}')

_f = (lambda s, t: plot_heatmap(usd_citi_otm_data_dict_3m_zsc_pvt[s], 
                                annot_data=usd_citi_otm_data_dict_current_pvt_with_3m_zsc[s], title=t))('BF50', f'BF50, 3m zsc ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'BF50_3m_zsc',f'M{vrow}')



vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_ivol_premium_data_3m_dict_1m_zsc_pvt[s], 
                                annot_data=usd_citi_ivol_premium_data_3m_dict_current_pvt_with_1m_zsc[s], title=t))('RR25', f'RR25 premium, 1m zsc ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'RR25_premium_1m_zsc',f'B{vrow}')


_f = (lambda s, t: plot_heatmap(usd_citi_ivol_premium_data_3m_dict_3m_zsc_pvt[s], 
                                annot_data=usd_citi_ivol_premium_data_3m_dict_current_pvt_with_3m_zsc[s], title=t))('RR50', f'RR50 premium, 1m zsc ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'RR50_premium_1m_zsc',f'M{vrow}')


vrow = vrow + 18

_f = (lambda s, t: plot_heatmap(usd_citi_ivol_premium_data_3m_dict_1m_zsc_pvt[s], 
                                annot_data=usd_citi_ivol_premium_data_3m_dict_current_pvt_with_1m_zsc[s], title=t))('BF25', f'BF25 premium, 1m zsc ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'BF25_premium_1m_zsc',f'B{vrow}')


_f = (lambda s, t: plot_heatmap(usd_citi_ivol_premium_data_3m_dict_3m_zsc_pvt[s], 
                                annot_data=usd_citi_ivol_premium_data_3m_dict_current_pvt_with_3m_zsc[s], title=t))('BF50', f'BF50 premium, 1m zsc ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'BF50_premium_1m_zsc',f'M{vrow}')



vrow = vrow + 18

_f = (lambda t: plot_heatmap(usd_citi_ivol_premium_data_3m_all_current_pvt, 
                             annot_data=usd_citi_ivol_premium_data_3m_all_current_pvt_with_3m_zsc,
                             title=t, fsize = (35, 4)))(f'Vol premium surface, ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'Vol premium surface',f'B{vrow}')


vrow = vrow + 18

_f = (lambda t: plot_heatmap(usd_citi_ivol_premium_data_3m_all_current_zsc_pvt, 
                             annot_data=usd_citi_ivol_premium_data_3m_all_current_pvt_with_3m_zsc,
                             title=t, fsize = (35, 4)))(f'Vol premium surface, ({citi_vol_date})').get_figure()

add_fig_to_excel(_f, sht1, 'Vol premium surface by zsc',f'B{vrow}')








####################################################################################################
# Swap structures
####################################################################################################

usd_fly_level = {}
usd_fly_zsc_20 = {}
usd_fly_zsc_60 = {}
usd_fly_zsc_750 = {}
usd_fly_zsc_1250 = {}
usd_fly_min_60 = {}
usd_fly_max_60 = {}
usd_fly_range_60 = {}
usd_fly_cr = {}
usd_fly_mtn = {}

usd_fly_pc_wtl = {}
usd_fly_pc_level = {}
usd_fly_pc_zsc_20 = {}
usd_fly_pc_zsc_60 = {}
usd_fly_pc_zsc_750 = {}
usd_fly_pc_zsc_1250 = {}
usd_fly_pc_min_60 = {}
usd_fly_pc_max_60 = {}
usd_fly_pc_range_60 = {}
usd_fly_pc_cr = {}
usd_fly_pc_mtn = {}


for (f,flys) in usd_swap_fly_data.items():
    
    usd_fly_level[f] = {}  
    usd_fly_zsc_20[f] = {}
    usd_fly_zsc_60[f] = {}    
    usd_fly_zsc_750[f] = {}    
    usd_fly_zsc_1250[f] = {}
    usd_fly_min_60[f] = {}
    usd_fly_max_60[f] = {}
    usd_fly_range_60[f] = {}
    usd_fly_cr[f] = {}
    usd_fly_mtn[f] = {}
    
    usd_fly_pc_wtl[f] = {}    
    usd_fly_pc_level[f] = {}
    usd_fly_pc_zsc_20[f] = {}
    usd_fly_pc_zsc_60[f] = {}
    usd_fly_pc_zsc_750[f] = {}
    usd_fly_pc_zsc_1250[f] = {}
    usd_fly_pc_min_60[f] = {}
    usd_fly_pc_max_60[f] = {}
    usd_fly_pc_range_60[f] = {}
    usd_fly_pc_cr[f] = {}
    usd_fly_pc_mtn[f] = {}


    for (fly, d) in flys.items():
        fs = make_fly(d, w=[-1,2,-1])        
        usd_fly_zsc_20[f][fly] = make_zsc(fs.iloc[-20:])
        usd_fly_zsc_60[f][fly] = make_zsc(fs.iloc[-60:])        
        usd_fly_zsc_750[f][fly] = make_zsc(fs.iloc[-750:])
        usd_fly_zsc_1250[f][fly] = make_zsc(fs.iloc[-1250:]) 
        usd_fly_min_60[f][fly] = fs.rolling(60).min()
        usd_fly_max_60[f][fly] = fs.rolling(60).max()
        usd_fly_range_60[f][fly] = usd_fly_max_60[f][fly] - usd_fly_min_60[f][fly] 
        usd_fly_level[f][fly] = fs        
        usd_fly_cr[f][fly] = pd.Series([get_cr(d.columns, carryroll_store, [1,-2, 1])]) #paying the structure
        _mm = kca(fs.iloc[-60:])
        usd_fly_mtn[f][fly] = pd.Series(['{}/{}'.format(round(_mm.iloc[-1]['momentum'] * 100,2), round(_mm.iloc[-1]['gamma'] * 100,2))])
        
        #####
        (_,w,_,_) = pca.PCA(d.iloc[-1250:],rates_pca=True).hedge_pcs()
        w = list(np.round(w.values/w.values[1]*2,2))
        usd_fly_pc_wtl[f][fly] = pd.Series(['{}/{}/{}'.format(*w)])
        fspca = make_fly(d, w=w)  
 
        usd_fly_pc_level[f][fly] = fspca
        usd_fly_pc_zsc_20[f][fly] = make_zsc(fspca.iloc[-20:])
        usd_fly_pc_zsc_60[f][fly] = make_zsc(fspca.iloc[-60:])
        usd_fly_pc_zsc_750[f][fly] = make_zsc(fspca.iloc[-750:])
        usd_fly_pc_zsc_1250[f][fly] = make_zsc(fspca.iloc[-1250:])
        usd_fly_pc_min_60[f][fly] = fspca.rolling(60).min()
        usd_fly_pc_max_60[f][fly] = fspca.rolling(60).max()
        usd_fly_pc_range_60[f][fly] = usd_fly_pc_max_60[f][fly] - usd_fly_pc_min_60[f][fly]
        usd_fly_pc_cr[f][fly] = pd.Series([get_cr(d.columns, carryroll_store, -1 * np.array(w))])
        _mm_pca = kca(fspca.iloc[-60:])
        usd_fly_pc_mtn[f][fly] = pd.Series(['{}/{}'.format(round(_mm_pca.iloc[-1]['momentum'] * 100,2), round(_mm_pca.iloc[-1]['gamma'] * 100,2))])
        
      
        

output = pd.DataFrame({
        'Level':make_ds_from_ddds(usd_fly_level),
        'min60':make_ds_from_ddds(usd_fly_min_60),
        'max60':make_ds_from_ddds(usd_fly_max_60),
        'rng60':make_ds_from_ddds(usd_fly_range_60),
        'zsc20':make_ds_from_ddds(usd_fly_zsc_20),
        'zsc60':make_ds_from_ddds(usd_fly_zsc_60),
        'zsc750':make_ds_from_ddds(usd_fly_zsc_750),
        'zsc1250':make_ds_from_ddds(usd_fly_zsc_1250),
        'CR pb':make_ds_from_ddds(usd_fly_cr),
        'mtn':make_ds_from_ddds(usd_fly_mtn),
        'pcw':make_ds_from_ddds(usd_fly_pc_wtl),
        'Level_pca':make_ds_from_ddds(usd_fly_pc_level),
        'min60_pca':make_ds_from_ddds(usd_fly_pc_min_60),
        'max60_pca':make_ds_from_ddds(usd_fly_pc_max_60),
        'rng60_pca':make_ds_from_ddds(usd_fly_pc_range_60),
        'zsc20_pca':make_ds_from_ddds(usd_fly_pc_zsc_20),
        'zsc60_pca':make_ds_from_ddds(usd_fly_pc_zsc_60),
        'zsc750_pca':make_ds_from_ddds(usd_fly_pc_zsc_750),
        'zsc1250_pca':make_ds_from_ddds(usd_fly_pc_zsc_1250),
        'CR pb_pca':make_ds_from_ddds(usd_fly_pc_cr),
        'mtn_pca':make_ds_from_ddds(usd_fly_pc_mtn)        
    })


wb1 = xw.Book(r'C:\Dev\Models\QuantModels\#applications\market_watch\ratesmonitor\ratesmonitor.xlsm')
sht1 = wb1.sheets('USD_Fly')
sht1.range((1,1),(10000,300)).clear_contents()



row = 1
col = 2

#output returns

sht1.range((row, col)).value = output
sht1.range((row, col)).value = ''


l = len(output.columns)

row = 1
col = 2 + (l + 2)






if (2 > 3):
    
    from statistics.mr import tools as mrt
        
    
    
    
    mrt.plot_fly(usd_data[['USD_2Y_2Y', 'USD_2Y_5Y', 'USD_2Y_10Y']].dropna().iloc[-2500:])
    mrt.plot_fly(usd_data[['USD_2Y_2Y', 'USD_2Y_10Y', 'USD_2Y_20Y']].dropna().iloc[-2500:])
    mrt.plot_fly(usd_data[['USD_6M_2Y', 'USD_6M_10Y', 'USD_6M_20Y']].dropna().iloc[-2500:])
    mrt.plot_fly(usd_data[['USD_0Y_2Y', 'USD_0Y_10Y', 'USD_0Y_20Y']].dropna().iloc[-2500:])
    
    mrt.plot_fly(usd_data[['USD_6M_2Y', 'USD_6M_7Y', 'USD_6M_10Y']].dropna().iloc[-2500:] * [1, 1/2, 1] * [0.36, 2.0, 1.69])
    
    mrt.plot_fly(usd_data[['USD_1Y_1Y', 'USD_2Y_2Y', 'USD_5Y_5Y']].dropna().iloc[-500:]* [1, 1/2, 1] * [1.14, 2.0, 1.03] )
    
    mrt.plot_fly(usd_data[['USD_2Y_1Y', 'USD_2Y_4Y', 'USD_2Y_9Y']].dropna().iloc[-2500:]* [1, 1/2, 1] * [0.79, 2.0, 1.25])
    
    mrt.plot_fly(usd_data[['USD_2Y_1Y', 'USD_2Y_4Y', 'USD_2Y_9Y']].dropna().iloc[-2500:])
    
    
    [0.79, 2.0, 1.25]
    
    
    kca(usd_fly_level['0Y']['1Y2Y3Y'].iloc[-60:])
    
    kca(usd_fly_level['5Y']['6Y20Y30Y'].iloc[-60:]).plot()
    usd_fly_level['1Y']['4Y6Y9Y'].iloc[:].plot()
    pca.PCA(usd_swap_data['1Y']['2Y5Y8Y'].iloc[-1250:], rates_pca=True).plot_loadings(n=3)
    pca.PCA(usd_swap_data['1Y']['2Y5Y8Y'].iloc[-2500:], rates_pca=True).create_hedged_pcs_fig()
    pca.PCA(usd_swap_fly_data['0Y']['1Y3Y4Y'].iloc[-1250:], rates_pca=True).create_hedged_pcs_fig()


    pca.PCA(usd_citi_swap_gradiant_data.iloc[-1250:-750].ffill(), rates_pca=True).plot_loadings(n=3)







































