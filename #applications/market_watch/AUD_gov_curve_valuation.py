import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations
import xlwings as xw

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper


#################################################################################################################
#################################################################################################################
#functions
#################################################################################################################
#################################################################################################################

def create_pca(data, date_index, momentum_size=None, resample_freq= 'W'):

    data_sub = data[date_index].resample(resample_freq).last() if resample_freq is not None else data[date_index]    
    return pca.PCA(data_sub, rates_pca = True, momentum_size = momentum_size)



def generate_returns(data,periods):
    rts = {}
    spots = {}
    spots[0] = data
    for p in periods:
        rts[p] = data - data.shift(p)
        spots[p] = data.shift(p)
    
    #week to date
    
    rts['WTD'] = (data.resample('W', convention='end').last().iloc[-1] - data.resample('W', convention='end').last())[:-1]
    spots['WTD'] = data.resample('W', convention='end').last()[:-1]
    rts['MTD'] = (data.resample('M', convention='end').last().iloc[-1] - data.resample('M', convention='end').last())[:-1]
    spots['MTD'] = data.resample('W', convention='end').last()[:-1]
    
        
    return (rts, spots)


def refmt_returns_by_p(rts, tickers, date_offset):
    rfmt_rts_by_p = {}
    for rt_key, rt in rts.items():
        rfmt_rts = []
        for ticker in tickers:
                tmp = rt.iloc[date_offset][ticker].rename(lambda n: n[:3]).rename(ticker[0][3:]).to_frame()
                rfmt_rts.append(tmp)
                
        tmp = pd.concat(rfmt_rts, axis=1, join='outer',sort=False).transpose()

        rfmt_rts_by_p[rt_key] = tmp
    return rfmt_rts_by_p





#################################################################################################################
#################################################################################################################
#################################################################################################################
# rates
#################################################################################################################
#################################################################################################################
#################################################################################################################



data = pd.read_csv('C:/Temp/test/market_watch/staging/2020-03-09_GOV_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()

#dates
dt_10yrs_ago = data.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_13yrs_ago = data.index[-1] - DateOffset(years = 13, months=0, days=0)
dt_5yrs_ago = data.index[-1] - DateOffset(years = 5, months=0, days=0)
dt_3yrs_ago = data.index[-1] - DateOffset(years = 3, months=0, days=0)
dt_2yrs_ago = data.index[-1] - DateOffset(years = 2, months=0, days=0)
dt_1yrs_ago = data.index[-1] - DateOffset(years = 1, months=0, days=0)
dt_18mths_ago = data.index[-1] - DateOffset(years = 0, months=18, days=0)

cross_countries_pca_history = dt_5yrs_ago






####################
#derive rates data
####################


data_3ms2s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y)):
         data_3ms2s[rate_ticker_2y[i][:3] + '3ms2s'] =  data[rate_ticker_2y[i]] - data[rate_ticker_3m[i]]
    data_3ms2s = pd.DataFrame(data_3ms2s)

data_2s10s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y)):
         data_2s10s[rate_ticker_10y[i][:3] + '2s10s'] =  data[rate_ticker_10y[i]] - data[rate_ticker_2y[i]]
    data_2s10s = pd.DataFrame(data_2s10s)

data_2s5s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y)):
         data_2s5s[rate_ticker_5y[i][:3] + '2s5s'] =  data[rate_ticker_5y[i]] - data[rate_ticker_2y[i]]
    data_2s5s = pd.DataFrame(data_2s5s)

data_5s10s = {}
if True:
    for i in np.arange(0, len(rate_ticker_10y)):
         data_5s10s[rate_ticker_5y[i][:3] + '5s10s'] =  data[rate_ticker_10y[i]] - data[rate_ticker_5y[i]]
    data_5s10s = pd.DataFrame(data_5s10s)
    
data_10s30s = {}
rate_ticker_temp = [c.replace('_0Y_30Y', '_0Y_10Y') for c in rate_ticker_30y]
if True:
    for i in np.arange(0, len(rate_ticker_30y)):
         data_10s30s[rate_ticker_30y[i][:3] + '10s30s'] =  data[rate_ticker_30y[i]] - data[rate_ticker_temp[i]]
    data_10s30s = pd.DataFrame(data_10s30s)
    

data_2s30s = {}
rate_ticker_temp = [c.replace('_0Y_30Y', '_0Y_2Y') for c in rate_ticker_30y]
if True:
    for i in np.arange(0, len(rate_ticker_30y)):
         data_2s30s[rate_ticker_30y[i][:3] + '2s30s'] =  data[rate_ticker_30y[i]] - data[rate_ticker_temp[i]]
    data_2s30s = pd.DataFrame(data_2s30s)


data_2s5s10s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y)):
         data_2s5s10s[rate_ticker_2y[i][:3] + 'P2s5s10s'] =  2 * data[rate_ticker_5y[i]] - data[rate_ticker_2y[i]] - data[rate_ticker_10y[i]]
    data_2s5s10s = pd.DataFrame(data_2s5s10s)
    

data_5s30s = {}
rate_ticker_temp = [c.replace('_0Y_30Y', '_0Y_5Y') for c in rate_ticker_30y]
if True:
    for i in np.arange(0, len(rate_ticker_30y)):
         data_5s30s[rate_ticker_30y[i][:3] + '5s30s'] =  data[rate_ticker_30y[i]] - data[rate_ticker_temp[i]]
    data_5s30s = pd.DataFrame(data_5s30s)



data_5s10s30s = {}
rate_ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_5Y') for c in rate_ticker_30y]
rate_ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in rate_ticker_30y]
if True:
    for i in np.arange(0, len(rate_ticker_30y)):
         data_5s10s30s[rate_ticker_30y[i][:3] + 'P5s10s30s'] =  2 * data[rate_ticker_temp2[i]] - data[rate_ticker_temp1[i]] - data[rate_ticker_30y[i]]
    data_5s10s30s = pd.DataFrame(data_5s10s30s)
    

data_2s10s30s = {}
rate_ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_2Y') for c in rate_ticker_30y]
rate_ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in rate_ticker_30y]
if True:
    for i in np.arange(0, len(rate_ticker_30y)):
         data_2s10s30s[rate_ticker_30y[i][:3] + 'P2s10s30s'] =  2 * data[rate_ticker_temp2[i]] - data[rate_ticker_temp1[i]] - data[rate_ticker_30y[i]]
    data_2s10s30s = pd.DataFrame(data_2s10s30s)



data_22s55s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y2y)):
         data_22s55s[rate_ticker_2y2y[i][:3] + '22s55s'] =  data[rate_ticker_5y5y[i]] - data[rate_ticker_2y2y[i]]
    data_22s55s = pd.DataFrame(data_22s55s)



data_12s110s = {}
if True:
    for i in np.arange(0, len(rate_ticker_1y2y)):
         data_12s110s[rate_ticker_1y2y[i][:3] + '12s110s'] =  data[rate_ticker_1y10y[i]] - data[rate_ticker_1y2y[i]]
    data_12s110s = pd.DataFrame(data_12s110s)



data_2s5y5s = {}
if True:
    for i in np.arange(0, len(rate_ticker_5y5y)):
         data_2s5y5s[rate_ticker_5y5y[i][:3] + '2s55s'] =  data[rate_ticker_5y5y[i]] - data[rate_ticker_5y5y[i][:3] + '_0Y_2Y']
    data_2s5y5s = pd.DataFrame(data_2s5y5s)


data_2s55s30s = {}
ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_2Y') for c in rate_ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_5Y_5Y') for c in rate_ticker_30y]
if True:
    for i in np.arange(0, len(rate_ticker_30y)):
         data_2s55s30s[rate_ticker_30y[i][:3] + 'P2s55s30s'] =  2 * data[ticker_temp2[i]] - data[ticker_temp1[i]] - data[rate_ticker_30y[i]]
    data_2s55s30s = pd.DataFrame(data_2s55s30s)



data_11s22s55s = {}
if True:
    for i in np.arange(0, len(rate_ticker_1y1y)):
         data_11s22s55s[rate_ticker_1y1y[i][:3] + 'P11s22s55s'] =  2 * data[rate_ticker_2y2y[i]] - data[rate_ticker_1y1y[i]] - data[rate_ticker_5y5y[i]]
    data_11s22s55s = pd.DataFrame(data_11s22s55s)


data_22s25s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y2y)):
         data_22s25s[rate_ticker_2y2y[i][:3] + '22s25s'] =  data[rate_ticker_2y5y[i]] - data[rate_ticker_2y2y[i]]
    data_22s25s = pd.DataFrame(data_22s25s)


data_11s55s = {}
if True:
    for i in np.arange(0, len(rate_ticker_1y1y)):
         data_11s55s[rate_ticker_1y1y[i][:3] + '11s55s'] =  data[rate_ticker_5y5y[i]] - data[rate_ticker_1y1y[i]]
    data_11s55s = pd.DataFrame(data_11s55s)



data_2s22s55s = {}
if True:
    for i in np.arange(0, len(rate_ticker_5y5y)):
         data_2s22s55s[rate_ticker_5y5y[i][:3] + 'P2s22s55s'] =  2 * data[rate_ticker_5y5y[i][:3] + '_2Y_2Y'] - data[rate_ticker_5y5y[i][:3] + '_0Y_2Y'] - data[rate_ticker_5y5y[i]]
    data_2s22s55s = pd.DataFrame(data_2s22s55s)



data_22s210s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y2y)):
         data_22s210s[rate_ticker_2y2y[i][:3] + '22s210s'] =  data[rate_ticker_2y10y[i]] - data[rate_ticker_2y2y[i]]
    data_22s210s = pd.DataFrame(data_22s210s)


data_25s210s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y5y)):
         data_25s210s[rate_ticker_2y5y[i][:3] + '25s210s'] =  data[rate_ticker_2y10y[i]] - data[rate_ticker_2y5y[i]]
    data_25s210s = pd.DataFrame(data_25s210s)



data_12s15s = {}
if True:
    for i in np.arange(0, len(rate_ticker_1y2y)):
         data_12s15s[rate_ticker_1y2y[i][:3] + '12s15s'] =  data[rate_ticker_1y5y[i]] - data[rate_ticker_1y2y[i]]
    data_12s15s = pd.DataFrame(data_12s15s)



data_12s110s = {}
if True:
    for i in np.arange(0, len(rate_ticker_1y2y)):
         data_12s110s[rate_ticker_1y2y[i][:3] + '12s110s'] =  data[rate_ticker_1y10y[i]] - data[rate_ticker_1y2y[i]]
    data_12s110s = pd.DataFrame(data_12s110s)


data_15s110s = {}
if True:
    for i in np.arange(0, len(rate_ticker_1y5y)):
         data_15s110s[rate_ticker_1y5y[i][:3] + '15s110s'] =  data[rate_ticker_1y10y[i]] - data[rate_ticker_1y5y[i]]
    data_15s110s = pd.DataFrame(data_15s110s)
    
    

data_12s15s110s = {}
if True:
    for i in np.arange(0, len(rate_ticker_1y2y)):
         data_12s15s110s[rate_ticker_1y2y[i][:3] + 'P12s15s110s'] =  2 * data[rate_ticker_1y5y[i]] - data[rate_ticker_1y2y[i]] - data[rate_ticker_1y10y[i]]
    data_12s15s110s = pd.DataFrame(data_12s15s110s)

data_22s25s210s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y2y)):
         data_22s25s210s[rate_ticker_2y2y[i][:3] + 'P22s25s210s'] =  2 * data[rate_ticker_2y5y[i]] - data[rate_ticker_2y2y[i]] - data[rate_ticker_2y10y[i]]
    data_22s25s210s = pd.DataFrame(data_22s25s210s)







AUD = ['AUD_0Y_1Y', 'AUD_0Y_2Y', 'AUD_0Y_3Y', 'AUD_0Y_4Y', 'AUD_0Y_5Y',
       'AUD_0Y_6Y', 'AUD_0Y_7Y', 'AUD_0Y_8Y', 'AUD_0Y_9Y', 'AUD_0Y_10Y',
       'AUD_0Y_15Y', 'AUD_0Y_20Y', 'AUD_0Y_30Y']

aud_gov_data = data[AUD] 


pcas = {}
pcas['aud_gov'] = create_pca(aud_gov_data, aud_gov_data.index> dt_3yrs_ago, resample_freq= 'B', momentum_size=None)
pcas['aud_gov'].plot_loadings(n=5, m=7)
pcas['aud_gov'].plot_valuation_history(2300, n=[1], w=30, h=16, rows=5, cols=5, normalise_valuation_yaxis = False,  normalise_data_yaxis = False, exp_standardise_val = False )






combined_data = pd.concat([data, data_3ms2s, data_2s10s, data_5s10s, data_2s5s, data_10s30s, data_22s55s,
                           data_5s30s, data_2s30s, data_2s5s10s, data_5s10s30s, data_2s10s30s, data_12s110s,data_2s5y5s, data_2s55s30s,
                           data_11s22s55s, data_22s25s, data_2s22s55s, data_22s210s, data_25s210s, data_12s15s,  data_15s110s,
                           data_12s15s110s, data_22s25s210s, data_11s55s], 
                          axis=1, join='outer',sort=False)




combined_data_tickers = [rate_ticker_3m, rate_ticker_1y, rate_ticker_2y, rate_ticker_3y, rate_ticker_5y, rate_ticker_10y, rate_ticker_1y1y, rate_ticker_1y2y,
               rate_ticker_1y5y, rate_ticker_1y10y, rate_ticker_2y1y, rate_ticker_2y2y, rate_ticker_2y5y, rate_ticker_2y10y, rate_ticker_5y5y, 
               rate_ticker_30y, data_3ms2s.columns, data_2s10s.columns, data_5s10s.columns, data_2s5s.columns,
               data_22s55s.columns, data_10s30s.columns, data_5s30s.columns, data_2s30s.columns, data_2s5s10s.columns, 
               data_5s10s30s.columns, data_2s10s30s.columns, data_12s110s.columns, data_2s5y5s.columns,data_2s55s30s.columns,
               data_11s22s55s.columns, data_22s25s.columns, data_2s22s55s.columns, data_22s210s.columns, data_25s210s.columns,
               data_12s15s.columns, data_15s110s.columns, data_12s15s110s.columns, data_22s25s210s.columns, data_11s55s.columns]



##########################
# create spots and returns
##########################


(rts, spots) = generate_returns(combined_data, [1,2,3,5,10,20,60,120,240])
rfmt_rts = refmt_returns_by_p(rts, combined_data_tickers, -1)
rfmt_spots = refmt_returns_by_p(spots, combined_data_tickers, -1)

##########################
# output to a new workbook
##########################
wb1 = xw.Book(r'C:\Dev\Models\QuantModels\#applications\market_watch\global_rate_curve_valuation.xlsx')
sht1 = wb1.sheets('Rates')
sht1.range((1,1),(10000,300)).clear_contents()

row = 1
col = 2
#output returns
for rts_key, rts in rfmt_rts.items():
    sht1.range((row, col)).value = (rts * 100).round(1)
    sht1.range((row, col)).value = "{0} days rts".format(rts_key)
    row = row + 2 + len(rts.index)
#output spots
row = 1
col = 2 + len(rfmt_rts[list(rfmt_rts.keys())[0]].columns) + 2

sht1.range((row,col)).value = rfmt_spots[list(rfmt_spots.keys())[0]]

col = 2 + len(rfmt_rts[list(rfmt_rts.keys())[0]].columns) + 2 + len(rfmt_spots[list(rfmt_spots.keys())[0]].columns) + 2 

for spots_key, spots in rfmt_spots.items():
    if spots_key != 0:
        sht1.range((row, col)).value = (spots * 100).round(1)
        sht1.range((row, col)).value = "{0} days spot".format(spots_key)
        row = row + 2 + len(spots.index)




#################################################################################################################
#################################################################################################################
#################################################################################################################
# swap spreads
#################################################################################################################
#################################################################################################################
#################################################################################################################


data_ss = pd.read_csv('C:/Temp/test/market_watch/staging/2020-02-26_SWAP_SPREAD_historical.csv')  
data_ss['date'] = pd.to_datetime(data_ss['date'])
data_ss = data_ss.set_index('date').ffill()


#ss tickers
 
#needs to sort the tenor as int
ss_ticker_lists_by_tenor = [ [c for c in data_ss.columns if '_{0}Y_'.format(unique_tenor) in c] 
                            for unique_tenor 
                            in sorted(list(set([ int(c.split('_')[1].replace('Y','')) for c in data_ss.columns])))]



##########################
# create spots and returns
##########################


(rts_ss, spots_ss) = generate_returns(data_ss, [1,2,3,5,10,20,60,120,240])
rfmt_rts_ss = refmt_returns_by_p(rts_ss, ss_ticker_lists_by_tenor, -1)
rfmt_spots_ss = refmt_returns_by_p(spots_ss, ss_ticker_lists_by_tenor, -1)



##########################
# output to a new workbook
##########################
wb1 = xw.Book(r'C:\Dev\Models\QuantModels\#applications\market_watch\global_rate_curve_valuation.xlsx')
sht1 = wb1.sheets('SS')
sht1.range((1,1),(10000,300)).clear_contents()

row = 1
col = 2
#output returns
for rts_key, rts in rfmt_rts_ss.items():
    sht1.range((row, col)).value = rts.round(1)
    sht1.range((row, col)).value = "{0} days rts".format(rts_key)
    row = row + 2 + len(rts.index)
#output spots
row = 1
col = 2 + len(rfmt_rts[list(rfmt_rts_ss.keys())[0]].columns) + 2

sht1.range((row,col)).value = rfmt_spots_ss[list(rfmt_spots_ss.keys())[0]].round(1)

col = 2 + len(rfmt_rts[list(rfmt_rts_ss.keys())[0]].columns) + 2 + len(rfmt_spots[list(rfmt_spots_ss.keys())[0]].columns) + 2 

for spots_key, spots in rfmt_spots_ss.items():
    if spots_key != 0:
        sht1.range((row, col)).value = spots.round(1)
        sht1.range((row, col)).value = "{0} days spot".format(spots_key)
        row = row + 2 + len(spots.index)






























################################# PCA stuffs #######################################





#i have to remove the data[rate_ticker_3m] as the matrix is too big and introducing floating number calculation error


pcas = {}
pcas['aud_gov'] = create_pca(data, data.index> dt_3yrs_ago, resample_freq= 'B', momentum_size=None)


pcas['aud_gov'].plot_loadings(n=5, m=7)
pcas['aud_gov'].plot_valuation_history(2300, n=[1], w=30, h=16, rows=5, cols=5, normalise_valuation_yaxis = False,  normalise_data_yaxis = False, exp_standardise_val = False )




pcs = {}
usd_rates_curves = pd.concat([
    data[['USD_0Y_2Y', 'USD_0Y_10Y']], 
    pd.DataFrame(data['USD_0Y_10Y'] - data['USD_0Y_2Y'],columns=['USD2s10s']),
    pd.DataFrame(data['USD_0Y_2Y'] - data['USD_0Y_3M'],columns=['USD3ms2s'])
    ], join='outer', axis=1)
pcas['usd_rates_curvs'] = create_pca(usd_rates_curves, data.index> dt_10yrs_ago, resample_freq= 'B', momentum_size=None)
pcas['usd_rates_curvs'].plot_loadings(n=3, m=7)
pcas['usd_rates_curvs'].plot_valuation_history(2300, n=[1, 2], w=15, h=8, rows=2, cols=2, normalise_valuation_yaxis = False,  normalise_data_yaxis = True, exp_standardise_val = True )













    














