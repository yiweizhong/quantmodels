import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations
import xlwings as xw

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)


from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper



from scipy import stats
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, fcluster
from scipy.spatial.distance import pdist, squareform




#################################################################################################################
#################################################################################################################
#functions
#################################################################################################################
#################################################################################################################





def get_linear_betas(xs, ys):
    betas, _, _, _, _ = np.polyfit(list(xs), list(ys), deg = 1, full=True)
    (beta_x, beta_0) = betas
    #xs = df[df.columns[0]]
    #ys = df[df.columns[1]]
    #start_x = xs.min() 
    #last_x = xs.max()
    #step = (last_x - start_x)/100 
    #xs = np.arange(start_x,last_x, step)
    fitted_ys = [round(beta_0 + beta_x * x, 4) for x in list(xs)]
    residual_ys = [y - fitted_y for y, fitted_y in  zip(ys, fitted_ys)]
    residual_ys_std = np.std(residual_ys, ddof=1) 
    residual_ys_p_1std =  [fitted_y + residual_ys_std for fitted_y in  fitted_ys]
    residual_ys_m_1std =  [fitted_y - residual_ys_std for fitted_y in  fitted_ys]
    
    return betas, (xs, fitted_ys, residual_ys, residual_ys_std, residual_ys_p_1std, residual_ys_m_1std)
    


def create_pca(data, date_index, momentum_size=None, resample_freq= 'W'):

    data_sub = data[date_index].resample(resample_freq).last() if resample_freq is not None else data[date_index]    
    return pca.PCA(data_sub, rates_pca = True, momentum_size = momentum_size)




def create_combo_mr(data, date_index, elements = 2, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None):
    mrs = {}
    data_sub = data[date_index]
    combos = list(combinations(data.columns, elements))
    instrument = combos[0][0][3:]
    
    for combo in combos:
        _sub_data = data_sub[list(combo)].resample(resample_freq).last() if resample_freq is not None else data_sub[list(combo)]
        data_sub_pca = pca.PCA(_sub_data, rates_pca = True, momentum_size = momentum_size)
        hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_pca.pc_hedge
        mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
        mrs[('_').join(combo) + '_PCW'] = (mr_obj, data_sub_pca)
        if include_normal_weighted_series:
            if len(_sub_data.columns) == 2:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] - _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s - %s' % (_sub_data.columns[0], _sub_data.columns[1])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)
            elif len(_sub_data.columns) == 3:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] + _sub_data[_sub_data.columns[2]] - 2 * _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s -2 x %s + %s' % (_sub_data.columns[0], _sub_data.columns[1], _sub_data.columns[2])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)

    return mrs




def generate_returns(data,periods, val_zsc=750):
    rts = {}
    spots = {}
    distr = {}
    spot_dist = {} #get the value zscore for 5years
    spots[0] = data
    spot_dist[0] = (spots[0].iloc[-val_zsc:] - spots[0].iloc[-val_zsc:].mean())/spots[0].iloc[-val_zsc:].std() 
    rts1d = data - data.shift(1)
    for p in periods:
        rts[p] = data - data.shift(p)
        spots[p] = data.shift(p)
        spot_dist[p] = (spots[p].iloc[-val_zsc:] - spots[p].iloc[-val_zsc:].mean())/spots[p].iloc[-val_zsc:].std()
        if (p > 1):  
            distr[p] = rts1d.resample('{}D'.format(p), closed='right', label='right').std()
    
    #week to date
    
    rts['WTD'] = (data.resample('W', convention='end').last().iloc[-1] - data.resample('W', convention='end').last())[:-1]
    spots['WTD'] = data.resample('W', convention='end').last()[:-1]
    rts['MTD'] = (data.resample('M', convention='end').last().iloc[-1] - data.resample('M', convention='end').last())[:-1]
    spots['MTD'] = data.resample('W', convention='end').last()[:-1]
        
    #return (rts, spots, distr)
    return (rts, spots, spot_dist)


def generate_zscore(data,periods):
    #rts = {}
    #spots = {}
    #spots[0] = data
    zsc = {}
    for p in periods:
        #rts[p] = data - data.shift(p)
        zsc[p] = (data - data.rolling(p).mean()) / data.rolling(p).std()
        #spots[p] = data.shift(p)
        #zsc[p] = pd.DataFrame({"{} days zsc".format(p): (data.iloc[-1] - data.iloc[-30:].mean()) / data.iloc[-30:].std()}).transpose()
    #week to date        
    return zsc





def refmt_returns_by_p(rts, tickers, date_offset):
    rfmt_rts_by_p = {}
    for rt_key, rt in rts.items():
        rfmt_rts = []
        for ticker in tickers:
                tmp = rt.iloc[date_offset][ticker].rename(lambda n: n[:3]).rename(ticker[0][3:]).to_frame()
                #print(tmp)
                rfmt_rts.append(tmp)
                #if len(rfmt_rts) > 1:
                #    print(pd.concat(rfmt_rts, axis=1, join='outer',sort=False))
      
        tmp = pd.concat(rfmt_rts, axis=1, join='outer',sort=False).transpose()
        if 'AUD' in tmp.columns and  'USD' in tmp.columns:
            tmp['AUDUSD'] = tmp['AUD'] - tmp['USD']
        if 'CAD' in tmp.columns and  'USD' in tmp.columns:
            tmp['CADUSD'] = tmp['CAD'] - tmp['USD']
        if 'EUR' in tmp.columns and  'USD' in tmp.columns:
            tmp['EURUSD'] = tmp['EUR'] - tmp['USD']
        if 'AUD' in tmp.columns and  'NZD' in tmp.columns:
            tmp['AUDNZD'] = tmp['AUD'] - tmp['NZD']
        rfmt_rts_by_p[rt_key] = tmp
    return rfmt_rts_by_p




#################################################################################################################
#################################################################################################################
#################################################################################################################
# dates
#################################################################################################################
#################################################################################################################
#################################################################################################################




rundate = '2021-05-19' 


#################################################################################################################
#################################################################################################################
#################################################################################################################
# swap rates
#################################################################################################################
#################################################################################################################
#################################################################################################################




data = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_SWAP_ALL_V2_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()
data = data[[c for c in data.columns if 'OIS' not in c]]

#dates
dt_10yrs_ago = data.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_13yrs_ago = data.index[-1] - DateOffset(years = 13, months=0, days=0)
dt_5yrs_ago = data.index[-1] - DateOffset(years = 5, months=0, days=0)
dt_3yrs_ago = data.index[-1] - DateOffset(years = 3, months=0, days=0)
dt_2yrs_ago = data.index[-1] - DateOffset(years = 2, months=0, days=0)
dt_1yrs_ago = data.index[-1] - DateOffset(years = 1, months=0, days=0)
dt_18mths_ago = data.index[-1] - DateOffset(years = 0, months=18, days=0)

cross_countries_pca_history = dt_5yrs_ago



###############
#rates_tickers
###############

rate_ticker_3m = [c for c in data.columns if '_0Y_3M' in c and  'NOK' not in c and  'NOK' not in c]
rate_ticker_1y = [c for c in data.columns if '_0Y_1Y' in c and  'NOK' not in c]
rate_ticker_2y = [c for c in data.columns if '_0Y_2Y' in c and  'NOK' not in c]
rate_ticker_3y = [c for c in data.columns if '_0Y_3Y' in c and  'NOK' not in c]
rate_ticker_4y = [c for c in data.columns if '_0Y_4Y' in c and  'NOK' not in c]
rate_ticker_5y = [c for c in data.columns if '_0Y_5Y' in c and  'NOK' not in c]
rate_ticker_7y = [c for c in data.columns if '_0Y_7Y' in c and  'NOK' not in c]
rate_ticker_10y = [c for c in data.columns if '_0Y_10Y' in c and  'NOK' not in c]
rate_ticker_1y1y = [c for c in data.columns if '_1Y_1Y' in c and  'NOK' not in c]
rate_ticker_1y2y = [c for c in data.columns if '_1Y_2Y' in c and  'NOK' not in c]
rate_ticker_1y5y = [c for c in data.columns if '_1Y_5Y' in c and  'NOK' not in c]
rate_ticker_1y10y = [c for c in data.columns if '_1Y_10Y' in c and  'NOK' not in c]
rate_ticker_2y1y = [c for c in data.columns if '_2Y_1Y' in c and  'NOK' not in c]
rate_ticker_2y2y = [c for c in data.columns if '_2Y_2Y' in c and  'NOK' not in c]
rate_ticker_2y5y = [c for c in data.columns if '_2Y_5Y' in c and  'NOK' not in c]
rate_ticker_2y10y = [c for c in data.columns if '_2Y_10Y' in c and  'NOK' not in c]
rate_ticker_3y2y = [c for c in data.columns if '_3Y_2Y' in c and  'NOK' not in c]
rate_ticker_5y5y = [c for c in data.columns if '_5Y_5Y' in c and  'NOK' not in c]
rate_ticker_2y2y = [c for c in data.columns if '_2Y_2Y' in c and  'NOK' not in c]
rate_ticker_10y5y = [c for c in data.columns if '_10Y_5Y' in c and  'NOK' not in c]
rate_ticker_10y10y = [c for c in data.columns if '_10Y_10Y' in c and  'NOK' not in c]
rate_ticker_30y = [c for c in data.columns if '_0Y_30Y' in c and  'NOK' not in c and 'THB' not in c and 'HKD' not in c and 'NZD' not in c and 'SGD' not in c]








####################
#derive rates data
####################


data_3ms2s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y)):
         data_3ms2s[rate_ticker_2y[i][:3] + '3ms2s'] =  data[rate_ticker_2y[i]] - data[rate_ticker_3m[i]]
    data_3ms2s = pd.DataFrame(data_3ms2s)

data_2s10s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y)):
         data_2s10s[rate_ticker_10y[i][:3] + '2s10s'] =  data[rate_ticker_10y[i]] - data[rate_ticker_2y[i]]
    data_2s10s = pd.DataFrame(data_2s10s)

data_2s5s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y)):
         data_2s5s[rate_ticker_5y[i][:3] + '2s5s'] =  data[rate_ticker_5y[i]] - data[rate_ticker_2y[i]]
    data_2s5s = pd.DataFrame(data_2s5s)

data_5s10s = {}
if True:
    for i in np.arange(0, len(rate_ticker_10y)):
         data_5s10s[rate_ticker_5y[i][:3] + '5s10s'] =  data[rate_ticker_10y[i]] - data[rate_ticker_5y[i]]
    data_5s10s = pd.DataFrame(data_5s10s)
    
data_10s30s = {}
rate_ticker_temp = [c.replace('_0Y_30Y', '_0Y_10Y') for c in rate_ticker_30y]
if True:
    for i in np.arange(0, len(rate_ticker_30y)):
         data_10s30s[rate_ticker_30y[i][:3] + '10s30s'] =  data[rate_ticker_30y[i]] - data[rate_ticker_temp[i]]
    data_10s30s = pd.DataFrame(data_10s30s)
    

data_2s30s = {}
rate_ticker_temp = [c.replace('_0Y_30Y', '_0Y_2Y') for c in rate_ticker_30y]
if True:
    for i in np.arange(0, len(rate_ticker_30y)):
         data_2s30s[rate_ticker_30y[i][:3] + '2s30s'] =  data[rate_ticker_30y[i]] - data[rate_ticker_temp[i]]
    data_2s30s = pd.DataFrame(data_2s30s)


data_2s5s10s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y)):
         data_2s5s10s[rate_ticker_2y[i][:3] + 'P2s5s10s'] =  2 * data[rate_ticker_5y[i]] - data[rate_ticker_2y[i]] - data[rate_ticker_10y[i]]
    data_2s5s10s = pd.DataFrame(data_2s5s10s)
    

data_5s30s = {}
rate_ticker_temp = [c.replace('_0Y_30Y', '_0Y_5Y') for c in rate_ticker_30y]
if True:
    for i in np.arange(0, len(rate_ticker_30y)):
         data_5s30s[rate_ticker_30y[i][:3] + '5s30s'] =  data[rate_ticker_30y[i]] - data[rate_ticker_temp[i]]
    data_5s30s = pd.DataFrame(data_5s30s)



data_5s10s30s = {}
rate_ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_5Y') for c in rate_ticker_30y]
rate_ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in rate_ticker_30y]
if True:
    for i in np.arange(0, len(rate_ticker_30y)):
         data_5s10s30s[rate_ticker_30y[i][:3] + 'P5s10s30s'] =  2 * data[rate_ticker_temp2[i]] - data[rate_ticker_temp1[i]] - data[rate_ticker_30y[i]]
    data_5s10s30s = pd.DataFrame(data_5s10s30s)
    

data_2s10s30s = {}
rate_ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_2Y') for c in rate_ticker_30y]
rate_ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in rate_ticker_30y]
if True:
    for i in np.arange(0, len(rate_ticker_30y)):
         data_2s10s30s[rate_ticker_30y[i][:3] + 'P2s10s30s'] =  2 * data[rate_ticker_temp2[i]] - data[rate_ticker_temp1[i]] - data[rate_ticker_30y[i]]
    data_2s10s30s = pd.DataFrame(data_2s10s30s)



data_22s55s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y2y)):
         data_22s55s[rate_ticker_2y2y[i][:3] + '22s55s'] =  data[rate_ticker_5y5y[i]] - data[rate_ticker_2y2y[i]]
    data_22s55s = pd.DataFrame(data_22s55s)



data_12s110s = {}
if True:
    for i in np.arange(0, len(rate_ticker_1y2y)):
         data_12s110s[rate_ticker_1y2y[i][:3] + '12s110s'] =  data[rate_ticker_1y10y[i]] - data[rate_ticker_1y2y[i]]
    data_12s110s = pd.DataFrame(data_12s110s)



data_2s5y5s = {}
if True:
    for i in np.arange(0, len(rate_ticker_5y5y)):
         data_2s5y5s[rate_ticker_5y5y[i][:3] + '2s55s'] =  data[rate_ticker_5y5y[i]] - data[rate_ticker_5y5y[i][:3] + '_0Y_2Y']
    data_2s5y5s = pd.DataFrame(data_2s5y5s)


data_2s55s30s = {}
ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_2Y') for c in rate_ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_5Y_5Y') for c in rate_ticker_30y]
if True:
    for i in np.arange(0, len(rate_ticker_30y)):
         if (ticker_temp2[i] in data.columns and ticker_temp1[i] in data.columns): 
             data_2s55s30s[rate_ticker_30y[i][:3] + 'P2s55s30s'] =  2 * data[ticker_temp2[i]] - data[ticker_temp1[i]] - data[rate_ticker_30y[i]]
    data_2s55s30s = pd.DataFrame(data_2s55s30s)



data_55s105s = {}
ticker_temp2 = [c.replace('_10Y_5Y', '_5Y_5Y') for c in rate_ticker_10y5y]
if True:
    for i in np.arange(0, len(rate_ticker_10y5y)):
        if (ticker_temp2[i] in data.columns):
            data_55s105s[rate_ticker_10y5y[i][:3] + '55s105s'] = data[rate_ticker_10y5y[i]] - data[ticker_temp2[i]]  
    data_55s105s = pd.DataFrame(data_55s105s)



data_55s30s = {}
ticker_temp2 = [c.replace('_0Y_30Y', '_5Y_5Y') for c in rate_ticker_30y]
if True:
    for i in np.arange(0, len(rate_ticker_30y)):
        if (ticker_temp2[i] in data.columns):
            data_55s30s[rate_ticker_30y[i][:3] + '55s30s'] = data[rate_ticker_30y[i]] - data[ticker_temp2[i]]  
    data_55s30s = pd.DataFrame(data_55s30s)


data_55s1010s = {}

if True:
    for i in np.arange(0, len(rate_ticker_5y5y)):
         data_55s1010s[rate_ticker_5y5y[i][:3] + '55s1010s'] = data[rate_ticker_10y10y[i]] - data[rate_ticker_5y5y[i]]  
    data_55s1010s = pd.DataFrame(data_55s1010s)


data_11s55s1010s = {}
if True:
    for i in np.arange(0, len(rate_ticker_1y1y)):
         data_11s55s1010s[rate_ticker_1y1y[i][:3] + 'P11s55s1010s'] =  2 * data[rate_ticker_5y5y[i]] - data[rate_ticker_1y1y[i]] - data[rate_ticker_10y10y[i]]
    data_11s55s1010s = pd.DataFrame(data_11s55s1010s)


data_11s22s55s = {}
if True:
    for i in np.arange(0, len(rate_ticker_1y1y)):
         data_11s22s55s[rate_ticker_1y1y[i][:3] + 'P11s22s55s'] =  2 * data[rate_ticker_2y2y[i]] - data[rate_ticker_1y1y[i]] - data[rate_ticker_5y5y[i]]
    data_11s22s55s = pd.DataFrame(data_11s22s55s)


data_22s25s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y2y)):
         data_22s25s[rate_ticker_2y2y[i][:3] + '22s25s'] =  data[rate_ticker_2y5y[i]] - data[rate_ticker_2y2y[i]]
    data_22s25s = pd.DataFrame(data_22s25s)


data_11s55s = {}
if True:
    for i in np.arange(0, len(rate_ticker_1y1y)):
         data_11s55s[rate_ticker_1y1y[i][:3] + '11s55s'] =  data[rate_ticker_5y5y[i]] - data[rate_ticker_1y1y[i]]
    data_11s55s = pd.DataFrame(data_11s55s)



data_2s22s55s = {}
if True:
    for i in np.arange(0, len(rate_ticker_5y5y)):
         data_2s22s55s[rate_ticker_5y5y[i][:3] + 'P2s22s55s'] =  2 * data[rate_ticker_5y5y[i][:3] + '_2Y_2Y'] - data[rate_ticker_5y5y[i][:3] + '_0Y_2Y'] - data[rate_ticker_5y5y[i]]
    data_2s22s55s = pd.DataFrame(data_2s22s55s)



data_22s210s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y2y)):
         data_22s210s[rate_ticker_2y2y[i][:3] + '22s210s'] =  data[rate_ticker_2y10y[i]] - data[rate_ticker_2y2y[i]]
    data_22s210s = pd.DataFrame(data_22s210s)


data_25s210s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y5y)):
         data_25s210s[rate_ticker_2y5y[i][:3] + '25s210s'] =  data[rate_ticker_2y10y[i]] - data[rate_ticker_2y5y[i]]
    data_25s210s = pd.DataFrame(data_25s210s)



data_12s15s = {}
if True:
    for i in np.arange(0, len(rate_ticker_1y2y)):
         data_12s15s[rate_ticker_1y2y[i][:3] + '12s15s'] =  data[rate_ticker_1y5y[i]] - data[rate_ticker_1y2y[i]]
    data_12s15s = pd.DataFrame(data_12s15s)



data_12s110s = {}
if True:
    for i in np.arange(0, len(rate_ticker_1y2y)):
         data_12s110s[rate_ticker_1y2y[i][:3] + '12s110s'] =  data[rate_ticker_1y10y[i]] - data[rate_ticker_1y2y[i]]
    data_12s110s = pd.DataFrame(data_12s110s)


data_15s110s = {}
if True:
    for i in np.arange(0, len(rate_ticker_1y5y)):
         data_15s110s[rate_ticker_1y5y[i][:3] + '15s110s'] =  data[rate_ticker_1y10y[i]] - data[rate_ticker_1y5y[i]]
    data_15s110s = pd.DataFrame(data_15s110s)
    
    

data_12s15s110s = {}
if True:
    for i in np.arange(0, len(rate_ticker_1y2y)):
         data_12s15s110s[rate_ticker_1y2y[i][:3] + 'P12s15s110s'] =  2 * data[rate_ticker_1y5y[i]] - data[rate_ticker_1y2y[i]] - data[rate_ticker_1y10y[i]]
    data_12s15s110s = pd.DataFrame(data_12s15s110s)

data_22s25s210s = {}
if True:
    for i in np.arange(0, len(rate_ticker_2y2y)):
         data_22s25s210s[rate_ticker_2y2y[i][:3] + 'P22s25s210s'] =  2 * data[rate_ticker_2y5y[i]] - data[rate_ticker_2y2y[i]] - data[rate_ticker_2y10y[i]]
    data_22s25s210s = pd.DataFrame(data_22s25s210s)








combined_data = pd.concat([data, data_3ms2s, data_2s10s, data_5s10s, data_2s5s, data_10s30s, data_22s55s,
                           data_5s30s, data_2s30s, data_2s5s10s, data_5s10s30s, data_2s10s30s, data_12s110s,data_2s5y5s, data_2s55s30s, data_55s105s, data_55s30s,
                           data_11s22s55s, data_22s25s, data_2s22s55s, data_22s210s, data_25s210s, data_12s15s,  data_15s110s,
                           data_12s15s110s, data_22s25s210s, data_11s55s, data_55s1010s, data_11s55s1010s], 
                          axis=1, join='outer',sort=False)




combined_data_tickers = [rate_ticker_3m, rate_ticker_1y, rate_ticker_2y, rate_ticker_3y, rate_ticker_5y, rate_ticker_10y, rate_ticker_1y1y, rate_ticker_1y2y,
               rate_ticker_1y5y, rate_ticker_1y10y, rate_ticker_2y1y, rate_ticker_2y2y, rate_ticker_2y5y, rate_ticker_2y10y, rate_ticker_5y5y, rate_ticker_10y5y, rate_ticker_10y10y,
               rate_ticker_30y, data_3ms2s.columns, data_2s10s.columns, data_5s10s.columns, data_2s5s.columns,
               data_22s55s.columns, data_10s30s.columns, data_5s30s.columns, data_2s30s.columns, data_2s5s10s.columns, 
               data_5s10s30s.columns, data_2s10s30s.columns, data_12s110s.columns, data_2s5y5s.columns,data_2s55s30s.columns, data_55s105s.columns, data_55s30s.columns,
               data_11s22s55s.columns, data_22s25s.columns, data_2s22s55s.columns, data_22s210s.columns, data_25s210s.columns,
               data_12s15s.columns, data_15s110s.columns, data_12s15s110s.columns, data_22s25s210s.columns, data_11s55s.columns,  data_55s1010s.columns, data_11s55s1010s.columns]



##########################
# create spots and returns
##########################


(rts, spots, dist) = generate_returns(combined_data, [1,2,3,5,10,20,60,120,240])
rfmt_rts = refmt_returns_by_p(rts, combined_data_tickers, -1)
rfmt_spots = refmt_returns_by_p(spots, combined_data_tickers, -1)
rfmt_dist = refmt_returns_by_p(dist, combined_data_tickers, -1)


##########################
# output to a new workbook
##########################
wb1 = xw.Book(r'C:\Dev\Models\QuantModels\#applications\market_watch\global_rate_curve_valuation.xlsx')
sht1 = wb1.sheets('Swap')
sht1.range((1,1),(10000,300)).clear_contents()

row = 1
col = 2
#output returns
for rts_key, rts in rfmt_rts.items():
    sht1.range((row, col)).value = (rts * 100).round(1)
    sht1.range((row, col)).value = "{0} days rts".format(rts_key)
    row = row + 2 + len(rts.index)

#output spots
row = 1
col = 2 + len(rfmt_rts[list(rfmt_rts.keys())[0]].columns) + 2

#sht1.range((row,col)).value = rfmt_spots[list(rfmt_spots.keys())[0]]

for rts_key, rts in rfmt_rts.items():
    sht1.range((row,col)).value = rfmt_spots[list(rfmt_spots.keys())[0]]
    row = row + 2 + len(rts.index)



row = 1
col = 2 + len(rfmt_rts[list(rfmt_rts.keys())[0]].columns) + 2 + len(rfmt_spots[list(rfmt_spots.keys())[0]].columns) + 2 

for dist_key, dist in rfmt_dist.items():
    if dist_key != 0:
        sht1.range((row, col)).value = dist.round(2)
        sht1.range((row, col)).value = "{0} days spot zsc".format(dist_key)
        row = row + 2 + len(dist.index)



#################################################################################################################
#################################################################################################################
#################################################################################################################
# Gov rates
#################################################################################################################
#################################################################################################################
#################################################################################################################

data_gov = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_GOV_historical.csv')  
data_gov['date'] = pd.to_datetime(data_gov['date'])
data_gov = data_gov.set_index('date').ffill()

#dates
dt_10yrs_ago = data_gov.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_13yrs_ago = data_gov.index[-1] - DateOffset(years = 13, months=0, days=0)
dt_5yrs_ago = data_gov.index[-1] - DateOffset(years = 5, months=0, days=0)
dt_3yrs_ago = data_gov.index[-1] - DateOffset(years = 3, months=0, days=0)
dt_2yrs_ago = data_gov.index[-1] - DateOffset(years = 2, months=0, days=0)
dt_1yrs_ago = data_gov.index[-1] - DateOffset(years = 1, months=0, days=0)
dt_18mths_ago = data_gov.index[-1] - DateOffset(years = 0, months=18, days=0)

cross_countries_pca_history = dt_5yrs_ago


###############
#rates_tickers
###############
rate_gov_ticker_1y = [c for c in data_gov.columns if '_0Y_1Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_2y = [c for c in data_gov.columns if '_0Y_2Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_3y = [c for c in data_gov.columns if '_0Y_3Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_4y = [c for c in data_gov.columns if '_0Y_4Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_5y = [c for c in data_gov.columns if '_0Y_5Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_7y = [c for c in data_gov.columns if '_0Y_7Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_8y = [c for c in data_gov.columns if '_0Y_8Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_10y = [c for c in data_gov.columns if '_0Y_10Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_15y = [c for c in data_gov.columns if '_0Y_15Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_20y = [c for c in data_gov.columns if '_0Y_20Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_30y = [c for c in data_gov.columns if '_0Y_30Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c] 


#[t for t in rate_gov_ticker_2y if (t[:3] not in [tt[:3] for tt in rate_gov_ticker_5y])]

####################
#derive rates data
####################


data_gov_2s10s = {}
if True:
    for i in np.arange(0, len(rate_gov_ticker_2y)):
         data_gov_2s10s[rate_gov_ticker_10y[i][:3] + '2s10s'] =  data_gov[rate_gov_ticker_10y[i]] - data_gov[rate_gov_ticker_2y[i]]
    data_gov_2s10s = pd.DataFrame(data_gov_2s10s)


data_gov_2s5s = {}
if True:
    for i in np.arange(0, len(rate_gov_ticker_2y)):
         data_gov_2s5s[rate_gov_ticker_5y[i][:3] + '2s5s'] =  data_gov[rate_gov_ticker_5y[i]] - data_gov[rate_gov_ticker_2y[i]]
    data_gov_2s5s = pd.DataFrame(data_gov_2s5s)


data_gov_5s10s = {}
if True:
    for i in np.arange(0, len(rate_gov_ticker_5y)):
         data_gov_5s10s[rate_gov_ticker_10y[i][:3] + '5s10s'] =  data_gov[rate_gov_ticker_10y[i]] - data_gov[rate_gov_ticker_5y[i]]
    data_gov_5s10s = pd.DataFrame(data_gov_5s10s)


data_gov_2s30s = {}
if True:
    for i in np.arange(0, len(rate_gov_ticker_30y)):
         data_gov_2s30s[rate_gov_ticker_30y[i][:3] + '2s30s'] =  data_gov[rate_gov_ticker_30y[i]] - data_gov[rate_gov_ticker_2y[i]]
    data_gov_2s30s = pd.DataFrame(data_gov_2s30s)



data_gov_5s30s = {}
if True:
    for i in np.arange(0, len(rate_gov_ticker_30y)):
         data_gov_5s30s[rate_gov_ticker_30y[i][:3] + '5s30s'] =  data_gov[rate_gov_ticker_30y[i]] - data_gov[rate_gov_ticker_5y[i]]
    data_gov_5s30s = pd.DataFrame(data_gov_5s30s)

data_gov_10s30s = {}
if True:
    for i in np.arange(0, len(rate_gov_ticker_30y)):
         data_gov_10s30s[rate_gov_ticker_30y[i][:3] + '10s30s'] =  data_gov[rate_gov_ticker_30y[i]] - data_gov[rate_gov_ticker_10y[i]]
    data_gov_10s30s = pd.DataFrame(data_gov_10s30s)



data_gov_5s10s30s = {}
rate_ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_5Y') for c in rate_gov_ticker_30y]
rate_ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in rate_gov_ticker_30y]
if True:
    for i in np.arange(0, len(rate_gov_ticker_30y)):
         data_gov_5s10s30s[rate_gov_ticker_30y[i][:3] + 'P5s10s30s'] =  2 * data_gov[rate_ticker_temp2[i]] - data_gov[rate_ticker_temp1[i]] - data_gov[rate_gov_ticker_30y[i]]
    data_gov_5s10s30s = pd.DataFrame(data_gov_5s10s30s)


data_gov_2s10s30s = {}
rate_ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_2Y') for c in rate_gov_ticker_30y]
rate_ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in rate_gov_ticker_30y]
if True:
    for i in np.arange(0, len(rate_gov_ticker_30y)):
         data_gov_2s10s30s[rate_gov_ticker_30y[i][:3] + 'P2s10s30s'] =  2 * data_gov[rate_ticker_temp2[i]] - data_gov[rate_ticker_temp1[i]] - data_gov[rate_gov_ticker_30y[i]]
    data_gov_2s10s30s = pd.DataFrame(data_gov_2s10s30s)



data_gov_2s5s10s = {}
rate_ticker_temp1 = [c.replace('_0Y_10Y', '_0Y_2Y') for c in rate_gov_ticker_10y]
rate_ticker_temp2 = [c.replace('_0Y_10Y', '_0Y_5Y') for c in rate_gov_ticker_10y]
if True:
    for i in np.arange(0, len(rate_gov_ticker_10y)):
         data_gov_2s5s10s[rate_gov_ticker_10y[i][:3] + 'P2s5s10s'] =  2 * data_gov[rate_ticker_temp2[i]] - data_gov[rate_ticker_temp1[i]] - data_gov[rate_gov_ticker_10y[i]]
    data_gov_2s5s10s = pd.DataFrame(data_gov_2s5s10s)




####################
#combine data and ticker
####################



combined_data_gov = pd.concat([data_gov, data_gov_2s10s, data_gov_2s5s, data_gov_5s10s, data_gov_2s30s, data_gov_5s30s, data_gov_10s30s, data_gov_5s10s30s,
                           data_gov_2s10s30s, data_gov_2s5s10s], 
                          axis=1, join='outer',sort=False)




combined_data_gov_tickers = [
    rate_gov_ticker_1y, rate_gov_ticker_2y, rate_gov_ticker_3y, rate_gov_ticker_4y, rate_gov_ticker_5y, rate_gov_ticker_7y, 
    rate_gov_ticker_8y, rate_gov_ticker_10y, rate_gov_ticker_15y, rate_gov_ticker_20y, rate_gov_ticker_30y,
    data_gov_2s10s.columns, data_gov_2s5s.columns, data_gov_5s10s.columns, data_gov_2s30s.columns, data_gov_5s30s.columns, 
    data_gov_10s30s.columns, data_gov_5s10s30s.columns, data_gov_2s10s30s.columns, data_gov_2s5s10s.columns]
    


##########################
# create spots and returns
##########################


(rts, spots, dist) = generate_returns(combined_data_gov, [1,2,3,5,10,20,60,120,240])
rfmt_rts = refmt_returns_by_p(rts, combined_data_gov_tickers, -1)
rfmt_spots = refmt_returns_by_p(spots, combined_data_gov_tickers, -1)
rfmt_dist = refmt_returns_by_p(dist, combined_data_gov_tickers, -1)

##########################
# output to a new workbook
##########################
wb1 = xw.Book(r'C:\Dev\Models\QuantModels\#applications\market_watch\global_rate_curve_valuation.xlsx')
sht1 = wb1.sheets('Gov')
sht1.range((1,1),(10000,300)).clear_contents()

row = 1
col = 2
#output returns
for rts_key, rts in rfmt_rts.items():
    sht1.range((row, col)).value = (rts * 100).round(1)
    sht1.range((row, col)).value = "{0} days rts".format(rts_key)
    row = row + 2 + len(rts.index)

#output spots
row = 1
col = 2 + len(rfmt_rts[list(rfmt_rts.keys())[0]].columns) + 2

for rts_key, rts in rfmt_rts.items():
    sht1.range((row,col)).value = rfmt_spots[list(rfmt_spots.keys())[0]]
    row = row + 2 + len(rts.index)


row = 1
col = 2 + len(rfmt_rts[list(rfmt_rts.keys())[0]].columns) + 2 + len(rfmt_spots[list(rfmt_spots.keys())[0]].columns) + 2 
for dist_key, dist in rfmt_dist.items():
    if dist_key != 0:
        sht1.range((row, col)).value = (dist).round(2)
        sht1.range((row, col)).value = "{0} days spot".format(dist_key)
        row = row + 2 + len(dist.index)








#################################################################################################################
#################################################################################################################
#################################################################################################################
# swap spreads
#################################################################################################################
#################################################################################################################
#################################################################################################################


data_ss = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_SWAP_SPREAD_historical.csv')  
data_ss['date'] = pd.to_datetime(data_ss['date'])
data_ss = data_ss.set_index('date').ffill()


#ss tickers
 
#needs to sort the tenor as int
ss_ticker_lists_by_tenor = [ [c for c in data_ss.columns if '_{0}Y_'.format(unique_tenor) in c] 
                            for unique_tenor 
                            in sorted(list(set([ int(c.split('_')[1].replace('Y','')) for c in data_ss.columns])))]



##########################
# create spots and returns
##########################


(rts_ss, spots_ss, dist_ss) = generate_returns(data_ss, [1,2,3,5,10,20,60,120,240])
rfmt_rts_ss = refmt_returns_by_p(rts_ss, ss_ticker_lists_by_tenor, -1)
rfmt_spots_ss = refmt_returns_by_p(spots_ss, ss_ticker_lists_by_tenor, -1)
rfmt_dist_ss = refmt_returns_by_p(dist_ss, ss_ticker_lists_by_tenor, -1)


zsc_ss = generate_zscore(data_ss, [10,20,60,120,240,500,1250,2500])
rfmt_zsc_ss = refmt_returns_by_p(zsc_ss, ss_ticker_lists_by_tenor, -1)



##########################
# output to a new workbook
##########################
wb1 = xw.Book(r'C:\Dev\Models\QuantModels\#applications\market_watch\global_rate_curve_valuation.xlsx')
sht1 = wb1.sheets('SS')
sht1.range((1,1),(10000,300)).clear_contents()

row = 1
col = 2
#output returns
for rts_key, rts in rfmt_rts_ss.items():
    sht1.range((row, col)).value = rts.round(1)
    sht1.range((row, col)).value = "{0} days rts".format(rts_key)
    row = row + 2 + len(rts.index)
#output spots
row = 1
col = 2 + len(rfmt_rts_ss[list(rfmt_rts_ss.keys())[0]].columns) + 2

sht1.range((row,col)).value = rfmt_spots_ss[list(rfmt_spots_ss.keys())[0]].round(1)

col = 2 + len(rfmt_rts_ss[list(rfmt_rts_ss.keys())[0]].columns) + 2 + len(rfmt_spots_ss[list(rfmt_spots_ss.keys())[0]].columns) + 2 

row = 1
for spots_key, spots in rfmt_spots_ss.items():
    if spots_key != 0:
        sht1.range((row, col)).value = spots.round(1)
        sht1.range((row, col)).value = "{0} days spot".format(spots_key)
        row = row + 2 + len(spots.index)


row = 1
col = 2 + len(rfmt_rts_ss[list(rfmt_rts_ss.keys())[0]].columns) + 2 + len(rfmt_spots_ss[list(rfmt_spots_ss.keys())[0]].columns) + 2  + len(rfmt_spots_ss[list(rfmt_spots_ss.keys())[0]].columns) + 2 


for zsc_key, zsc in rfmt_dist_ss.items():
    if zsc_key != 0:
        sht1.range((row, col)).value = zsc.round(2)
        sht1.range((row, col)).value = "{0} days zscore".format(zsc_key)
        row = row + 2 + len(zsc.index)





#################################################################################################################
#################################################################################################################
#################################################################################################################
# SWIT
#################################################################################################################
#################################################################################################################
#################################################################################################################


data_swit = pd.read_csv('C:/Temp/test/market_watch/staging/'+ rundate +'_SWIT_historical.csv')  
data_swit['date'] = pd.to_datetime(data_swit['date'])
data_swit = data_swit.set_index('date').ffill()


#ss tickers
 
#needs to sort the tenor as int



rate_swit_ticker_1y = [c for c in data_swit.columns if '_1Y' in c]
rate_swit_ticker_2y = [c for c in data_swit.columns if '_2Y' in c]
rate_swit_ticker_5y = [c for c in data_swit.columns if '_5Y' in c]
rate_swit_ticker_10y = [c for c in data_swit.columns if '_10Y' in c]
rate_swit_ticker_30y = [c for c in data_swit.columns if '_30Y' in c]



####################
#derive rates data
####################


data_swit_2s10s = {}
if True:
    for i in np.arange(0, len(rate_swit_ticker_2y)):
         data_swit_2s10s[rate_swit_ticker_10y[i][:3] + '2s10s'] =  data_swit[rate_swit_ticker_10y[i]] - data_swit[rate_swit_ticker_2y[i]]
    data_swit_2s10s = pd.DataFrame(data_swit_2s10s)
    
  

data_swit_2s5s = {}
if True:
    for i in np.arange(0, len(rate_swit_ticker_2y)):
         data_swit_2s5s[rate_swit_ticker_5y[i][:3] + '2s5s'] =  data_swit[rate_swit_ticker_5y[i]] - data_swit[rate_swit_ticker_2y[i]]
    data_swit_2s5s = pd.DataFrame(data_swit_2s5s)
  

data_swit_5s10s = {}
if True:
    for i in np.arange(0, len(rate_swit_ticker_5y)):
         data_swit_5s10s[rate_swit_ticker_10y[i][:3] + '5s10s'] =  data_swit[rate_swit_ticker_10y[i]] - data_swit[rate_swit_ticker_5y[i]]
    data_swit_5s10s = pd.DataFrame(data_swit_5s10s)


data_swit_5s30s = {}
if True:
    for i in np.arange(0, len(rate_swit_ticker_5y)):
         data_swit_5s30s[rate_swit_ticker_30y[i][:3] + '5s30s'] =  data_swit[rate_swit_ticker_30y[i]] - data_swit[rate_swit_ticker_5y[i]]
    data_swit_5s30s = pd.DataFrame(data_swit_5s30s)


data_swit_10s30s = {}
if True:
    for i in np.arange(0, len(rate_swit_ticker_10y)):
         data_swit_10s30s[rate_swit_ticker_30y[i][:3] + '10s30s'] =  data_swit[rate_swit_ticker_30y[i]] - data_swit[rate_swit_ticker_10y[i]]
    data_swit_10s30s = pd.DataFrame(data_swit_10s30s)



data_swit_2s30s = {}
if True:
    for i in np.arange(0, len(rate_swit_ticker_2y)):
         data_swit_2s30s[rate_swit_ticker_30y[i][:3] + '2s30s'] =  data_swit[rate_swit_ticker_30y[i]] - data_swit[rate_swit_ticker_2y[i]]
    data_swit_2s30s = pd.DataFrame(data_swit_2s30s)



data_swit_2s5s10s = {}
rate_ticker_temp1 = [c.replace('_10Y', '_2Y') for c in rate_swit_ticker_10y]
rate_ticker_temp2 = [c.replace('_10Y', '_5Y') for c in rate_swit_ticker_10y]
if True:
    for i in np.arange(0, len(rate_swit_ticker_10y)):
         data_swit_2s5s10s[rate_swit_ticker_10y[i][:3] + 'P2s5s10s'] =  2 * data_swit[rate_ticker_temp2[i]] - data_swit[rate_ticker_temp1[i]] - data_swit[rate_swit_ticker_10y[i]]
    data_swit_2s5s10s = pd.DataFrame(data_swit_2s5s10s)


data_swit_5s10s30s = {}
rate_ticker_temp1 = [c.replace('_30Y', '_5Y') for c in rate_swit_ticker_30y]
rate_ticker_temp2 = [c.replace('_30Y', '_10Y') for c in rate_swit_ticker_30y]
if True:
    for i in np.arange(0, len(rate_swit_ticker_30y)):
         data_swit_5s10s30s[rate_swit_ticker_30y[i][:3] + 'P5s10s30s'] =  2 * data_swit[rate_ticker_temp2[i]] - data_swit[rate_ticker_temp1[i]] - data_swit[rate_swit_ticker_30y[i]]
    data_swit_5s10s30s = pd.DataFrame(data_swit_5s10s30s)



combined_data_swit = pd.concat([data_swit, data_swit_2s5s, data_swit_2s10s, data_swit_5s10s, data_swit_5s30s, data_swit_10s30s, data_swit_2s30s, data_swit_2s5s10s, data_swit_5s10s30s], 
                          axis=1, join='outer',sort=False)



swit_ticker_lists_by_tenor = [ [c for c in data_swit.columns if '_{0}Y_'.format(unique_tenor) in c] 
                            for unique_tenor 
                            in sorted(list(set([ int(c.split('_')[1].replace('Y','')) for c in data_swit.columns])))]



swit_ticker_lists_by_tenor = swit_ticker_lists_by_tenor + [data_swit_2s5s.columns, data_swit_2s10s.columns, data_swit_5s10s.columns, 
                              data_swit_5s30s.columns, data_swit_10s30s.columns, data_swit_2s30s.columns, data_swit_2s5s10s.columns, 
                              data_swit_5s10s30s.columns]




##########################
# create spots and returns
##########################


(rts_swit, spots_swit, dist_swit) = generate_returns(combined_data_swit, [1,2,3,5,10,20,60,120,240])
rfmt_rts_swit = refmt_returns_by_p(rts_swit, swit_ticker_lists_by_tenor, -1)
rfmt_spots_swit = refmt_returns_by_p(spots_swit, swit_ticker_lists_by_tenor, -1)
rfmt_dist_swit = refmt_returns_by_p(dist_swit, swit_ticker_lists_by_tenor, -1)



##########################
# output to a new workbook
##########################
wb1 = xw.Book(r'C:\Dev\Models\QuantModels\#applications\market_watch\global_rate_curve_valuation.xlsx')
sht1 = wb1.sheets('SWIT')
sht1.range((1,1),(10000,300)).clear_contents()

row = 1
col = 2
#output returns
for rts_key, rts in rfmt_rts_swit.items():
    sht1.range((row, col)).value = (rts*100).round(2)
    sht1.range((row, col)).value = "{0} days rts".format(rts_key)
    row = row + 2 + len(rts.index)
#output spots
row = 1
col = 2 + len(rfmt_rts_swit[list(rfmt_rts_swit.keys())[0]].columns) + 2

sht1.range((row,col)).value = rfmt_spots_swit[list(rfmt_spots_swit.keys())[0]].round(2)

col = 2 + len(rfmt_rts_swit[list(rfmt_rts_swit.keys())[0]].columns) + 2 + len(rfmt_spots_swit[list(rfmt_spots_swit.keys())[0]].columns) + 2 

for dist_key, dist in rfmt_dist_swit.items():
    if dist_key != 0:
        sht1.range((row, col)).value = (dist).round(2)
        sht1.range((row, col)).value = "{0} days spot".format(dist_key)
        row = row + 2 + len(dist.index)




######################################################################################################################3
# variation
######################################################################################################################3

exclusion = ['HKD_0Y_30Y', 'THB_0Y_30Y','THB_0Y_8Y', 'THB_0Y_6Y', 'THB_0Y_9Y','BRL']
countries = list(set([c[:3] for c in [c for c in data.columns if '_0Y_' in c]]))        
tenors = [[t for t in data.columns if c in t and '_0Y_' in t and t not in exclusion] for c in countries if c not in exclusion]    
pcas1 ={data[t].columns[0][:3]: pca.PCA(data[t].ffill().iloc[-min(2500,len(data[t].ffill().dropna())):], rates_pca = True, momentum_size = None) for t in tenors}
exp1 = pd.DataFrame({c:pca.eigvals[:5] * 100 for (c, pca) in pcas1.items()}).transpose().apply(lambda x: round(x,2))
var1 = pd.DataFrame({c:(pca.pcs[pca.pcs.columns[:5]] - pca.pcs[pca.pcs.columns[:5]].shift(20)).iloc[-1] for (c, pca) in pcas1.items()}).transpose() 









######################################################################################################################3


if (2 > 3):
    
    data_swit_2s10s_test = data_swit_2s10s.dropna().iloc[-150:]
    
    data_swit_2s10s_test_pca = pca.PCA(data_swit_2s10s_test, rates_pca = True, momentum_size = None)
    data_swit_2s10s_test_pca.plot_loadings(n=3, m=5)
    
    
    
    
    
    
    
    #dummy = combined_data_gov[['USD5s10s','USD10s30s']]
    dummy = combined_data_gov[['USD_0Y_5Y','USD_0Y_10Y', 'USD_0Y_30Y']]
    dt_ago = dummy.index[-1] - DateOffset(years = 10, months=0, days=0)
    dummy = dummy[dummy.index >= dt_ago]
    
    dummy_pca = pca.PCA(dummy, rates_pca = True, momentum_size = None)
    dummy_pca.plot_loadings(n=3, m=5)
    
    
    auus_gov = combined_data_gov[['USD_0Y_10Y','AUD_0Y_10Y']] * 100
    auus_sw = combined_data[['USD_0Y_10Y','AUD_0Y_10Y']] * 100
    auus_ss = data_ss[['USD_10Y_SS','AUD_10Y_SS']]
    
    au = pd.DataFrame({
            'AUD_GOV_10Y': combined_data_gov['AUD_0Y_10Y'] * 100,
            'AUD_SW_10Y': combined_data['AUD_0Y_10Y'] * 100,
            'AUD_SS_10Y': data_ss['AUD_10Y_SS']    
        })
    
    
    
    dt_ago = au.index[-1] - DateOffset(years = 0, months=3, days=0)
    au = au[au.index >= dt_ago]
    
    au_pca = pca.PCA(au, rates_pca = True, momentum_size = None)
    au_pca.plot_loadings(n=3, m=5)
    
    
    
    proxy = pd.DataFrame({'PC2':au_pca.pcs['PC2'], 'AUD_SW_10Y': au_pca.data['AUD_SW_10Y']})
    
    c1 = mpl.dates.date2num(proxy.index.to_pydatetime())
    cmap1 = plt.cm.get_cmap('Reds')
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(proxy.PC2, proxy.AUD_SW_10Y,  c=c1, cmap=cmap1, marker='o', s = 9)
       
    
    (beta_x, beta_0), (xs, fitted_ys, residual_ys, residual_ys_std, residual_ys_p_1std, residual_ys_m_1std) = get_linear_betas(proxy.PC2, proxy.AUD_SW_10Y)
    label = r'{0}>2.5, {1} = {3:.2f} + {2:.2f} * {0} '.format(proxy.AUD_SW_10Y, proxy.PC2, beta_x, beta_0)
     
    ls1 = ax.plot(xs, fitted_ys, lw=1, c='black', ls = '-', marker = '.', ms = 0, label=label)
    
    
    auus = pd.DataFrame({
            'AUD_GOV_10Y': combined_data_gov['AUD_0Y_10Y'] * 100,
            'AUD_SW_10Y': combined_data['AUD_0Y_10Y'] * 100,
            'AUD_SS_10Y': data_ss['AUD_10Y_SS'],
            'USD_GOV_10Y': combined_data_gov['USD_0Y_10Y'] * 100,
            'USD_SW_10Y': combined_data['USD_0Y_10Y'] * 100,
            'USD_SS_10Y': data_ss['USD_10Y_SS']  
        })
    
    
    dt_ago = auus.index[-1] - DateOffset(years = 0, months=3, days=0)
    auus = auus[auus.index >= dt_ago]
    
    auus_pca = pca.PCA(auus, rates_pca = True, momentum_size = None)
    auus_pca.plot_loadings(n=3, m=5)
    
    
    
    
    
    
    
    data_gov_10y = [
     'GBP_0Y_10Y',
     'AUD_0Y_10Y',
     'NZD_0Y_10Y',
     'EUR_0Y_10Y',
     'CAD_0Y_10Y',
     'USD_0Y_10Y',
     'CHF_0Y_10Y',
     'CLP_0Y_10Y',
     'CNY_0Y_10Y',
     'ESP_0Y_10Y',
     'FRA_0Y_10Y',
     'HKD_0Y_10Y',
     'IDR_0Y_10Y',
     'INR_0Y_10Y',
     'ITA_0Y_10Y',
     'JPY_0Y_10Y',
     'KOR_0Y_10Y',
     'MEX_0Y_10Y',
     'MYS_0Y_10Y',
     'SEK_0Y_10Y',
     'THB_0Y_10Y',
     'ZAR_0Y_10Y',
     'TWD_0Y_10Y']
    
    data_gov_sub = data_gov[data_gov_10y].dropna()
    
    
    
    data_gov_sub_pca = pca.PCA(data_gov_sub, rates_pca = True, momentum_size = None)
    data_gov_sub_pca.plot_loadings(n=3, m=5)
    
    
    data_gov_sub_pca.plot_valuation_history(1500, n=[1], w=16, h=12, rows=6, cols=4, normalise_valuation_yaxis = False, 
                                   normalise_data_yaxis=False, exp_standardise_val=True,
                                   selected_series=data_gov_10y[:-1])    
    
    
    
    
    c_5y5y = [c for c in combined_data.columns if '_2Y_2Y' in c]
    
    combined_data_sub = combined_data[c_5y5y].dropna()
    
    combined_data_sub_pca = pca.PCA(combined_data_sub, rates_pca = True, momentum_size = None)
    combined_data_sub_pca.plot_loadings(n=3, m=200)
    
    

    
    
    data_2s5y5s
    pca.PCA(data_2s5y5s.ffill().iloc[-600:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
    
    pca.PCA(data_2s5s10s.ffill().iloc[-600:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
    
    pca.PCA(data_55s1010s.ffill().iloc[-2600:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
    
    data_55s105s.ffill().iloc[-2600:][['USD55s105s', 'AUD55s105s', 'CAD55s105s', 'NZD55s105s', 'GBP55s105s','EUR55s105s']].plot()
    
    pca.PCA(pd.concat([data_55s105s.ffill().iloc[-2600:][['USD55s105s', 'AUD55s105s', 'CAD55s105s', 'NZD55s105s', 'GBP55s105s','EUR55s105s']],
                       data_5s10s.ffill().iloc[-2600:][['USD5s10s', 'AUD5s10s', 'CAD5s10s', 'NZD5s10s', 'GBP5s10s','EUR5s10s']]], axis=1), 
            rates_pca = True, momentum_size = None).plot_loadings(n=3)
    
    

    #CAD
    
    pca.PCA(data[[c for c in data.columns if c.startswith('CAD')  and c.endswith('_1Y') ]].iloc[-2600:], 
            rates_pca = True, momentum_size = None).plot_loadings(n=4)
    
    pca.PCA(data[[c for c in data.columns if c.startswith('CAD')  and (c.endswith('_3M') or c.endswith('_1Y')) ]].iloc[-2600:], 
            rates_pca = True, momentum_size = None).plot_loadings(n=5)
    
    pd.concat([pca.PCA(data[[c for c in data.columns if c.startswith('CAD')  and (c.endswith('_3M') or c.endswith('_1Y')) ]].iloc[-2600:], rates_pca = True, momentum_size = None).pcs[['PC1','PC2','PC3']],
               data_22s55s[['CAD22s55s']].iloc[-2600:]], axis=1).plot(secondary_y=['PC1', 'PC2', 'PC3'])
                   
        
    
    pca.PCA(data[[c for c in data.columns if c.startswith('CAD')  and c.endswith('_1Y') ]].iloc[-2600:], 
            rates_pca = True, momentum_size = None).reconstruct_compare_data_by_n_pc('CAD_7Y_1Y', n=[1,2, 3]).plot(secondary_y=['PC1', 'PC2'])      
    
    
    
    pca.PCA(data[[c for c in data.columns if c.endswith('_2Y_1Y') or c.endswith('_3Y_1Y') ]].iloc[-2600:], rates_pca = True, momentum_size = None).plot_loadings(n=4)
    
    
    #variation

    exclusion = ['HKD_0Y_30Y', 'THB_0Y_30Y','THB_0Y_8Y', 'THB_0Y_6Y', 'THB_0Y_9Y','BRL']
    countries = list(set([c[:3] for c in [c for c in data.columns if '_0Y_' in c]]))        
    tenors = [[t for t in data.columns if c in t and '_0Y_' in t and t not in exclusion] for c in countries if c not in exclusion]    
    pcas1 ={data[t].columns[0][:3]: pca.PCA(data[t].ffill().iloc[-min(5000,len(data[t].ffill().dropna())):], rates_pca = True, momentum_size = None) for t in tenors}
    exp1 = pd.DataFrame({c:pca.eigvals[:5] * 100 for (c, pca) in pcas1.items()}).transpose().apply(lambda x: round(x,2))
    var1 = pd.DataFrame({c:(pca.pcs.iloc[:,:5] - pca.pcs.iloc[:,:5].shift(20)).iloc[-1] for (c, pca) in pcas1.items()}).transpose() 
    var1
    
    
   #AUD clustering   
       
    def reformat_labelled_data(labelled_data):
        labels = labelled_data[labelled_data.columns[1]].unique()
        labels.sort()
        output = {}
        for l in labels:
            output["{0}_{1}".format(labelled_data.columns[1], l)] = labelled_data[labelled_data.columns[0]][labelled_data[labelled_data.columns[1]] == l].reindex(labelled_data.index)
        return pd.DataFrame(output)

   
   
    country = 'USD'
    no_clusters = 4
    #predictors = (pcas1['AUD'].pcs.iloc[:,1:4] - pcas1['AUD'].pcs.iloc[:,1:4].shift(10)).dropna()
    predictors = pcas1[country].pcs
    predictors['PC1'] = (predictors['PC1'] - predictors['PC1'].shift(250)) #.rolling(40).mean()
    predictors = predictors.dropna(axis=0, how='any').iloc[:,1:4]
    linkage_matrix = linkage(predictors, method='ward', optimal_ordering=False)
    cluster_ts = pd.Series(data=fcluster(linkage_matrix, no_clusters, criterion='maxclust'), index=predictors.index)
    reformat_labelled_data(pd.DataFrame({"{}5s10s".format(country):(data['{}_0Y_10Y'.format(country)] - data['{}_0Y_5Y'.format(country)]).reindex(cluster_ts.index),"Regime":cluster_ts})).plot(title='{}5s10s'.format(country))
    reformat_labelled_data(pd.DataFrame({"{}2s10s".format(country):(data['{}_0Y_10Y'.format(country)] - data['{}_0Y_2Y'.format(country)]).reindex(cluster_ts.index),"Regime":cluster_ts})).plot(title='{}2s10s'.format(country))
    reformat_labelled_data(pd.DataFrame({"{}10".format(country):(data['{}_0Y_10Y'.format(country)]).reindex(cluster_ts.index),"Regime":cluster_ts})).plot(title='{}10'.format(country))
    reformat_labelled_data(pd.DataFrame({"{}2s5s10s".format(country):(data['{}_0Y_5Y'.format(country)] * 2 - data['{}_0Y_10Y'.format(country)] - data['{}_0Y_2Y'.format(country)]).reindex(cluster_ts.index),"Regime":cluster_ts})).plot(title='{}2s5s10s'.format(country))



   
    
    pca.PCA(data[['USD_1Y_1Y', 'USD_5Y_5Y', 'USD_10Y_10Y']].iloc[-120:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
    
    
    pca.PCA(data[['EUR_1Y_2Y', 'EUR_1Y_5Y', 'EUR_1Y_10Y']].iloc[-2000:], rates_pca = True, momentum_size = None).plot_loadings(n=3)