from __future__ import division
import datetime
import os, sys, inspect
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns

from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations
import xlwings as xw

from scipy import stats
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet
from scipy.spatial.distance import pdist, squareform


_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper


from sklearn.decomposition import FastICA, PCA

import argparse
pd.set_option('display.expand_frame_repr', False)




def bootstrap_ifs_fwd(data):
    nc =  { c: int( c[4:].replace('_SWIT','').replace('Y','') ) for c in data.columns}
    data = data.rename(columns=nc)
    a = data.columns.tolist()[1:]
    b = data.columns.tolist()[:-1]
    fwd = {}
    fwd['{0}Y'.format(data.columns[0])] = data[data.columns[0]]
    for (ca,cb) in zip(a, b):
        term = ca - cb
        forward = cb
        cca = (1 + data[ca]/100).pow(ca)
        ccb = (1 + data[cb]/100).pow(cb)
        fwd['{0}Y{1}Y'.format(forward, term)] = np.log(cca/ccb)/term * 100
    fwd = pd.DataFrame(fwd)
    return fwd





def smooth_series(ds, lim=2, half_window=1):

    def smooth(row):

        if np.isnan(row['ds_prev']) or np.isnan(row['ds_next']):
            return row['ds']

        if abs(row['ds_prev'] - row['ds']) > lim and abs(row['ds_next'] - row['ds']) > lim and abs((row['ds_prev'] + row['ds_next'])/2 - row['ds']) > lim:
            return (row['ds_prev'] + row['ds_next'])/2
        else:
            return row['ds']

    data = pd.DataFrame({'ds':ds, 'ds_prev': ds.shift(half_window), 'ds_next': ds.shift(-1*half_window)})

    return data.apply(smooth, axis=1)
 
    

def smooth_rates(data, limit, half_window): 
    out = {}
    for col, ds in data.iteritems():
        out[col] = smooth_series(ds, lim=limit, half_window=half_window)
    
    return pd.DataFrame(out)

def smooth_fwd_rates(data, limit, half_window): 
    
    out = {}
    
    for col, ds in data.iteritems():
        
        if len(col.split('_')) > 1 and int(col.split('_')[1].replace('Y','').replace('M','')) != 0:
            out[col] = smooth_series(ds, lim=limit, half_window=half_window)
        else:
            out[col] = ds
    
    return pd.DataFrame(out)


def restructure(data, tenors, curve_names):
    df_data = {}
    curve_names.sort()    
    for curve in curve_names:
        curve_columns = [curve + '_' + tenor for tenor in tenors]
        curve_data = data[curve_columns].rename(lambda x: x.split('_')[2], axis='columns')
        ds = curve_data.iloc[0]
        df_data[curve] = ds 
    df = pd.DataFrame(df_data)
    df.index.name = 'Tenor'
    return df


def create_pca(data, momentum_size=None, resample_freq= 'W', dummy=None,  matrixoveride = None):
    
    if dummy is not None:
        print(dummy)

    data_sub = data.resample(resample_freq).last() if resample_freq is not None else data  
    
    return pca.PCA(data_sub.dropna(how='all',axis=1).dropna(how='all',axis=0), rates_pca = True, momentum_size = momentum_size, matrixoveride = matrixoveride)


def create_combo_mr(data, elements = 2, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None, matrixoveride = None):
    mrs = {}
    data_sub = data
    combos = list(combinations(data.columns, elements))
    instrument = combos[0][0][3:]
    
    for combo in combos:
        _sub_data = data_sub[list(combo)].resample(resample_freq).last() if resample_freq is not None else data_sub[list(combo)]
        data_sub_pca = pca.PCA(_sub_data.dropna(how='all',axis=1).dropna(how='all',axis=0), rates_pca = True, momentum_size = momentum_size,matrixoveride = matrixoveride)
        hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_pca.pc_hedge
        mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
        mrs[('_').join(combo) + '_PCW'] = (mr_obj, data_sub_pca)
        if include_normal_weighted_series:
            if len(_sub_data.columns) == 2:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] - _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s - %s' % (_sub_data.columns[0], _sub_data.columns[1])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)
            elif len(_sub_data.columns) == 3:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] + _sub_data[_sub_data.columns[2]] - 2 * _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s -2 x %s + %s' % (_sub_data.columns[0], _sub_data.columns[1], _sub_data.columns[2])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)

    return mrs

def realign_cols(df, cols):
    out_df = df
    for c in cols:
        if c not in out_df:
            out_df[c] = np.NaN
    return out_df






def generate_zscore(data,periods):
    #rts = {}
    #spots = {}
    #spots[0] = data
    zsc = {}
    for p in periods:
        #rts[p] = data - data.shift(p)
        zsc[p] = (data - data.rolling(p).mean()) / data.rolling(p).std()
        #spots[p] = data.shift(p)
        #zsc[p] = pd.DataFrame({"{} days zsc".format(p): (data.iloc[-1] - data.iloc[-30:].mean()) / data.iloc[-30:].std()}).transpose()
    #week to date        
    return zsc





def refmt_returns_by_p(rts, tickers, date_offset):
    rfmt_rts_by_p = {}
    for rt_key, rt in rts.items():
        rfmt_rts = []
        for ticker in tickers:
                tmp = rt.iloc[date_offset][ticker].rename(lambda n: n[:3]).rename(ticker[0][3:]).to_frame()
                #print(tmp)
                rfmt_rts.append(tmp)
                #if len(rfmt_rts) > 1:
                #    print(pd.concat(rfmt_rts, axis=1, join='outer',sort=False))
                
        tmp = pd.concat(rfmt_rts, axis=1, join='outer',sort=False).transpose()
        if 'AUD' in tmp.columns and  'USD' in tmp.columns:
            tmp['AUDUSD'] = tmp['AUD'] - tmp['USD']
        if 'CAD' in tmp.columns and  'USD' in tmp.columns:
            tmp['CADUSD'] = tmp['CAD'] - tmp['USD']
        if 'EUR' in tmp.columns and  'USD' in tmp.columns:
            tmp['EURUSD'] = tmp['EUR'] - tmp['USD']
        if 'AUD' in tmp.columns and  'NZD' in tmp.columns:
            tmp['AUDNZD'] = tmp['AUD'] - tmp['NZD']
        rfmt_rts_by_p[rt_key] = tmp
    return rfmt_rts_by_p




filedate = '2021-04-21'
modeldate = '2021-04-21'
cross_countries_pca_history_window = 24
pair_pca_history_window = 24


    
if(2>3):
    
    #command line args
    parser = argparse.ArgumentParser(description='---- global rates overview ----')
    parser.add_argument('filedate', metavar='filedate', type=str, nargs=1, help='date in yyyy-mm-dd')
    parser.add_argument('modeldate', metavar='modeldate', type=str, nargs=1, help='date in yyyy-mm-dd')
    parser.add_argument('cross_countries_pca_history', metavar='cross_countries_pca_history', type=int, nargs=1, help='number of the months of history will be used for cross country pca')
    parser.add_argument('pair_pca_history', metavar='pair_pca_history', type=int, nargs=1, help='number of the months of history will be used for cross country pca')
    
    args = parser.parse_args()
    print(args.filedate)
    print(args.modeldate)
    print(args.cross_countries_pca_history)
    print(args.pair_pca_history)
    # data
        
    filedate = args.filedate[0]
    modeldate = args.modeldate[0]
    cross_countries_pca_history = args.cross_countries_pca_history[0]
    pair_pca_history = args.pair_pca_history[0]




data = pd.read_csv('C:/Temp/test/market_watch/staging/'+ filedate + '_SWAP_ALL_V2_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()

####################################################################################################################
# realign dates for the region and smooth forward rates
#################################################################################################################### 

tminus1_secs = [c for c in data.columns if [country for country in ['NOK','GBP','SEK','CAD','EUR','USD'] if country in c]]
t_secs = np.setdiff1d(data.columns.tolist(),tminus1_secs)
data_aligned = pd.concat([data[tminus1_secs].shift(-1), data[t_secs]],axis=1).iloc[:-1] 
data_aligned_smoothed = smooth_fwd_rates(data_aligned, 0.2, 1)




####################################################################################################################
# create cross country data and curve fly structure 
####################################################################################################################

#take out the history for model period
data_4model = data_aligned_smoothed[data_aligned_smoothed.index <= modeldate]


#dates
dt_10yrs_ago = data_4model.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_5yrs_ago = data_4model.index[-1] - DateOffset(years = 5, months=0, days=0)
dt_3yrs_ago = data_4model.index[-1] - DateOffset(years = 3, months=0, days=0)
dt_2yrs_ago = data_4model.index[-1] - DateOffset(years = 2, months=0, days=0)
dt_1yrs_ago = data_4model.index[-1] - DateOffset(years = 1, months=0, days=0)
dt_18mths_ago = data_4model.index[-1] - DateOffset(years = 0, months=18, days=0)

cross_countries_pca_history = data_4model.index[-1] - DateOffset(years = 0, months=cross_countries_pca_history_window, days=0)
pair_pca_history = data_4model.index[-1] - DateOffset(years = 0, months=pair_pca_history_window, days=0)

#countries
countries = list(set(data_4model.columns.map(lambda x: x[:3])))

#tenors
tenors = list(set(data_4model.columns.map(lambda x: x[3:])))
tenors.remove('_0Y_25Y')

#spot tenors
spot_tenors = sorted([c for c in set(data_4model.columns.map(lambda x: x[3:])) if ('_0Y_' in c) and ('M' in c)], key=lambda x: int(x.split('_')[2].replace('M','')))
spot_tenors = spot_tenors + sorted([c for c in set(data_4model.columns.map(lambda x: x[3:])) if ('_0Y_' in c) and ('M' not in c)], key=lambda x: int(x.split('_')[2].replace('Y','')))

fwd_tenors = sorted([c for c in set(data_4model.columns.map(lambda x: x[3:])) if ('_0Y_' not in c) and ('M' not in c)], key=lambda x: int(x.split('_')[1].replace('Y','')) * 100 + int(x.split('_')[2].replace('Y','')))


#countries spot data
country_spot_data = {cn:data_4model[sum([[c for c in data_4model.columns if (st in c) and (cn in c)] for st in spot_tenors],[])] for cn in countries}


#countries fwd data
country_fwd_data = {cn:data_4model[sum([[c for c in data_4model.columns if (st in c) and (cn in c)] for st in fwd_tenors],[])] for cn in countries}



#cross countries data
cross_country_data = {t:realign_cols(data_4model[[c for c in data_4model.columns if t in c]].rename(lambda c: c[:3], axis=1), countries) for t in tenors}
#
cross_country_structure = {}
cross_country_structure['0Y_2S5S'] = cross_country_data['_0Y_5Y'] - cross_country_data['_0Y_2Y']
cross_country_structure['1Y_2S5S'] = cross_country_data['_1Y_5Y'] - cross_country_data['_1Y_2Y']
cross_country_structure['2Y_2S5S'] = cross_country_data['_2Y_5Y'] - cross_country_data['_2Y_2Y']
cross_country_structure['3Y_2S5S'] = cross_country_data['_3Y_5Y'] - cross_country_data['_3Y_2Y']
cross_country_structure['4Y_2S5S'] = cross_country_data['_4Y_5Y'] - cross_country_data['_4Y_2Y']
cross_country_structure['5Y_2S5S'] = cross_country_data['_5Y_5Y'] - cross_country_data['_5Y_2Y']
#
cross_country_structure['0Y_2S10S'] = cross_country_data['_0Y_10Y'] - cross_country_data['_0Y_2Y']
cross_country_structure['1Y_2S10S'] = cross_country_data['_1Y_10Y'] - cross_country_data['_1Y_2Y']
cross_country_structure['2Y_2S10S'] = cross_country_data['_2Y_10Y'] - cross_country_data['_2Y_2Y']
cross_country_structure['3Y_2S10S'] = cross_country_data['_3Y_10Y'] - cross_country_data['_3Y_2Y']
cross_country_structure['4Y_2S10S'] = cross_country_data['_4Y_10Y'] - cross_country_data['_4Y_2Y']
cross_country_structure['5Y_2S10S'] = cross_country_data['_5Y_10Y'] - cross_country_data['_5Y_2Y']
#
cross_country_structure['0Y_5S10S'] = cross_country_data['_0Y_10Y'] - cross_country_data['_0Y_5Y']
cross_country_structure['1Y_5S10S'] = cross_country_data['_1Y_10Y'] - cross_country_data['_1Y_5Y']
cross_country_structure['2Y_5S10S'] = cross_country_data['_2Y_10Y'] - cross_country_data['_2Y_5Y']
cross_country_structure['3Y_5S10S'] = cross_country_data['_3Y_10Y'] - cross_country_data['_3Y_5Y']
cross_country_structure['4Y_5S10S'] = cross_country_data['_4Y_10Y'] - cross_country_data['_4Y_5Y']
cross_country_structure['5Y_5S10S'] = cross_country_data['_5Y_10Y'] - cross_country_data['_5Y_5Y']
#
cross_country_structure['0Y_2S30S'] = cross_country_data['_0Y_30Y'] - cross_country_data['_0Y_2Y']
cross_country_structure['0Y_5S30S'] = cross_country_data['_0Y_30Y'] - cross_country_data['_0Y_5Y']
cross_country_structure['0Y_10S30S'] = cross_country_data['_0Y_30Y'] - cross_country_data['_0Y_10Y']
cross_country_structure['55S30S'] = cross_country_data['_0Y_30Y'] - cross_country_data['_5Y_5Y']
cross_country_structure['1005S30S'] = cross_country_data['_0Y_30Y'] - cross_country_data['_10Y_5Y']
cross_country_structure['1010S30S'] = cross_country_data['_0Y_30Y'] - cross_country_data['_10Y_10Y']

#
cross_country_structure['11S22S'] = cross_country_data['_2Y_2Y'] - cross_country_data['_1Y_1Y']
cross_country_structure['2S22S'] = cross_country_data['_2Y_2Y'] - cross_country_data['_0Y_2Y']
cross_country_structure['2S32S'] = cross_country_data['_3Y_2Y'] - cross_country_data['_0Y_2Y']
cross_country_structure['2S42S'] = cross_country_data['_4Y_2Y'] - cross_country_data['_0Y_2Y']
cross_country_structure['12S42S'] = cross_country_data['_4Y_2Y'] - cross_country_data['_1Y_2Y']
cross_country_structure['22S42S'] = cross_country_data['_4Y_2Y'] - cross_country_data['_2Y_2Y']

#
cross_country_structure['2S33S'] = cross_country_data['_3Y_3Y'] - cross_country_data['_0Y_1Y']
cross_country_structure['2S33S'] = cross_country_data['_3Y_3Y'] - cross_country_data['_0Y_2Y']
cross_country_structure['3S33S'] = cross_country_data['_3Y_3Y'] - cross_country_data['_0Y_3Y']
cross_country_structure['11S33S'] = cross_country_data['_3Y_3Y'] - cross_country_data['_1Y_1Y']
#
cross_country_structure['11S55S'] = cross_country_data['_5Y_5Y'] - cross_country_data['_1Y_1Y']
cross_country_structure['12S55S'] = cross_country_data['_5Y_5Y'] - cross_country_data['_1Y_2Y']
cross_country_structure['22S55S'] = cross_country_data['_5Y_5Y'] - cross_country_data['_2Y_2Y']
cross_country_structure['32S55S'] = cross_country_data['_5Y_5Y'] - cross_country_data['_3Y_2Y']
cross_country_structure['32S510S'] = cross_country_data['_5Y_10Y'] - cross_country_data['_3Y_2Y']
cross_country_structure['32S1010S'] = cross_country_data['_10Y_10Y'] - cross_country_data['_3Y_2Y']
cross_country_structure['2S55S'] = cross_country_data['_5Y_5Y'] - cross_country_data['_0Y_1Y']
cross_country_structure['2S55S'] = cross_country_data['_5Y_5Y'] - cross_country_data['_0Y_2Y']
cross_country_structure['3S55S'] = cross_country_data['_5Y_5Y'] - cross_country_data['_0Y_3Y']
cross_country_structure['4S55S'] = cross_country_data['_5Y_5Y'] - cross_country_data['_0Y_4Y']
#
cross_country_structure['5S1005S'] = cross_country_data['_10Y_5Y'] - cross_country_data['_0Y_5Y']
cross_country_structure['55S1005S'] = cross_country_data['_10Y_5Y'] - cross_country_data['_5Y_5Y']
cross_country_structure['10S1005S'] = cross_country_data['_10Y_5Y'] - cross_country_data['_0Y_10Y']

#
cross_country_structure['5S1010S'] = cross_country_data['_10Y_10Y'] - cross_country_data['_0Y_5Y']
cross_country_structure['55S1010S'] = cross_country_data['_10Y_10Y'] - cross_country_data['_5Y_5Y']
cross_country_structure['10S1010S'] = cross_country_data['_10Y_10Y'] - cross_country_data['_0Y_10Y']
#
cross_country_structure['0Y_2S5S10S'] = cross_country_data['_0Y_5Y'] * 2 - cross_country_data['_0Y_10Y'] - cross_country_data['_0Y_2Y']
cross_country_structure['1Y_2S5S10S'] = cross_country_data['_1Y_5Y'] * 2 - cross_country_data['_1Y_10Y'] - cross_country_data['_1Y_2Y']
cross_country_structure['2Y_2S5S10S'] = cross_country_data['_2Y_5Y'] * 2 - cross_country_data['_2Y_10Y'] - cross_country_data['_2Y_2Y']
cross_country_structure['11S22S55S'] = cross_country_data['_2Y_2Y'] * 2 - cross_country_data['_1Y_1Y'] - cross_country_data['_5Y_5Y']
cross_country_structure['0Y_5S10S30S'] = cross_country_data['_0Y_10Y'] * 2 - cross_country_data['_0Y_5Y'] - cross_country_data['_0Y_30Y']
cross_country_structure['0Y_2S10S30S'] = cross_country_data['_0Y_10Y'] * 2 - cross_country_data['_0Y_2Y'] - cross_country_data['_0Y_30Y']
cross_country_structure['22S10S30S'] = cross_country_data['_0Y_10Y'] * 2 - cross_country_data['_2Y_2Y'] - cross_country_data['_0Y_30Y']
cross_country_structure['22S55S30S'] = cross_country_data['_5Y_5Y'] * 2 - cross_country_data['_2Y_2Y'] - cross_country_data['_0Y_30Y']
cross_country_structure['2S55S1010S'] = cross_country_data['_5Y_5Y'] * 2 - cross_country_data['_10Y_10Y'] - cross_country_data['_0Y_2Y']
cross_country_structure['22S55S1010S'] = cross_country_data['_5Y_5Y'] * 2 - cross_country_data['_10Y_10Y'] - cross_country_data['_2Y_2Y']
cross_country_structure['55S1010S'] = cross_country_data['_10Y_10Y'] - cross_country_data['_5Y_5Y'] * 2 - cross_country_data['_10Y_10Y'] - cross_country_data['_2Y_2Y']




l = 500

#xx = { k : len(c.dropna(axis=1, how='all').dropna(axis=0).index) for k, c in country_spot_data.items()}

country_based_mdl = {k: pca.PCA(c.dropna(axis=1, how='all').iloc[-l:], rates_pca=True) for k, c in country_spot_data.items() if k not in ['BRL', 'TRY']}

level = pca.PCA(pd.DataFrame({ k: c.pcs['PC1'] for k, c in country_based_mdl.items()}), rates_pca=True)
curve = pca.PCA(pd.DataFrame({ k: c.pcs['PC2'] for k, c in country_based_mdl.items()}), rates_pca=True)


pca.PCA((cross_country_data['_10Y_10Y'] - cross_country_data['_5Y_5Y']).dropna(axis=1, how='all').iloc[-l:], rates_pca=True).plot_loadings(n=5)


#def create_combo_mr(data, date_index, elements = 2, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None):

mrs = create_combo_mr(country_fwd_data['AUD'][['AUD_5Y_5Y', 'AUD_10Y_10Y']].dropna(axis=1, how='all').iloc[-2000:-500])

mrs = create_combo_mr(cross_country_data['_5Y_5Y'][['AUD', 'CAD']].dropna(axis=1, how='all').iloc[-1000:])

pca.PCA(pd.concat([country_fwd_data['THB'][['THB_2Y_1Y', 'THB_5Y_5Y']], country_spot_data['USD']], axis=1).dropna(axis=1, how='all').iloc[-2000:], rates_pca=True).plot_loadings(n=5)

pca.PCA(pd.concat([country_spot_data['AUD'], country_spot_data['USD']], axis=1).dropna(axis=1, how='all').iloc[-3000:-1500], rates_pca=True).plot_loadings(n=5)
pca.PCA(pd.concat([country_spot_data['AUD'], country_spot_data['USD']], axis=1).dropna(axis=1, how='all').iloc[-1500:], rates_pca=True).plot_loadings(n=5)


pca.PCA(pca.PCA(pd.concat([country_spot_data['USD']], axis=1).dropna(axis=1, how='all').iloc[-3000:-1500], rates_pca=True).data_gaps_from_reconstruction(n=[1, 2])).plot_loadings(n=3)


# Compute ICA
ica = FastICA(n_components=3)
S_ = ica.fit_transform(pd.concat([country_spot_data['USD']], axis=1).dropna(axis=1, how='all').iloc[-1500:])  # Reconstruct signals
A_ = ica.mixing_  # Get estimated mixing matrix
C_ = ica.components_


pd.DataFrame(ica.inverse_transform(S_, copy=True), index=pd.concat([country_spot_data['USD']], axis=1).dropna(axis=1, how='all').iloc[-1500:].index, 
             columns=pd.concat([country_spot_data['USD']], axis=1).dropna(axis=1, how='all').iloc[-1500:].columns).plot()


d = pd.concat([country_spot_data['USD']], axis=1).dropna(axis=1, how='all').iloc[-1500:]

pd.DataFrame(np.dot(A_.T, d.values.T)).rename(lambda x: 'ICA{}'.format(1+x), axis=1).plot()

pd.DataFrame(ica.transform(pd.concat([country_spot_data['USD']], axis=1).dropna(axis=1, how='all').iloc[-1500:])).plot()

pd.DataFrame(S_, index=pd.concat([country_spot_data['USD']], axis=1).dropna(axis=1, how='all').iloc[-1500:].index).rename(lambda x: 'ICA{}'.format(1+x), axis=1).plot()


for k, (mr, p) in create_combo_mr(pd.DataFrame([country_fwd_data['THB'][['THB_2Y_1Y', 'THB_5Y_5Y']], country_fwd_data['USD']]).dropna(axis=1, how='all').iloc[-2000:]).items():
    mr.create_mr_info_fig((18, 7), pca_obj = p)
















#############################################################################################################################
##### Looking at market movement from hierarchical clustering perspective
#############################################################################################################################

#10y
#cross_country_data['_0Y_10Y']
#cross_country_data['_0Y_3Y'].tail(10)
#cross_country_data['_0Y_5Y'].tail(10).dropna(axis=1)


sample_return_start = 60
sample_return_end = 1
starting_offset = 10
resample_freq = '3B'

sample_retunrs_corrs = []


for i in np.arange(-1 * starting_offset, 1):        
    sample_retunrs = pd.concat([ \
                                (cross_country_data['_0Y_5Y'] - cross_country_data['_0Y_5Y'].shift(1)).iloc[-1 * sample_return_start + i: - 1 * sample_return_end + i].rename(lambda c: c + '_5Y', axis=1),
                                (cross_country_data['_0Y_10Y'] - cross_country_data['_0Y_10Y'].shift(1)).iloc[-1 * sample_return_start + i: - 1 * sample_return_end + i].rename(lambda c: c + '_10Y', axis=1) \
                                ],
                               axis=1).dropna(axis=1)
    #print(sample_retunrs['CAD_10Y'].tail())
    # resample into weekly
    sample_retunrs = sample_retunrs.resample(resample_freq, label='right').sum()
    #print(((sample_retunrs.corr() + sample_retunrs.corr().transpose())/2)['CAD_10Y'].tail())
    sample_retunrs_corrs = sample_retunrs_corrs + [(sample_retunrs.corr() + sample_retunrs.corr().transpose())/2]

sample_retunrs_corr = (sum(sample_retunrs_corrs)/len(sample_retunrs_corrs)).fillna(0)
np.fill_diagonal(sample_retunrs_corr.values, 1)


sample_retunrs_corr_dist_matrix = sample_retunrs_corr.apply(lambda x: np.sqrt(4 *( 1 - x)))
sample_retunrs_corr_dist_matrix_condensed = squareform(sample_retunrs_corr_dist_matrix)
sample_retunrs_corr_cluster_linkage_matrix = linkage(sample_retunrs_corr_dist_matrix_condensed, method='ward', optimal_ordering=False)
c, coph_dists = cophenet(sample_retunrs_corr_cluster_linkage_matrix, sample_retunrs_corr_dist_matrix_condensed)

#
sample_retunrs_clusters_fig = plt.figure(figsize = (45, 15))
plt.title('Rates Clustering')
plt.xlabel('Rates')
plt.ylabel('Independence')

dendrogram(
    sample_retunrs_corr_cluster_linkage_matrix,
    leaf_rotation=45.,  # rotates the x axis labels
    leaf_font_size=8.,  # font size for the x axis labels
    labels = sample_retunrs_corr.columns.tolist(),
    color_threshold = 2
)


pca.PCA(cross_country_data['_0Y_10Y'].iloc[-200:],rates_pca = True, momentum_size = None).plot_loadings(n=6)




x = pca.PCA(cross_country_structure['2Y_5S10S'].iloc[-200:].drop(['TRY', 'IDR' , 'ZAR', 'RUB', 'INR', 'BRL', 'MXN', 'TWD', 'THB', 'HKD'], axis=1).dropna(axis=1),
        rates_pca = True, momentum_size = None).plot_loadings(n=10)


#############################################################################################################################
##### Looking at market movement PCA loadings from hierarchical clustering perspective
#############################################################################################################################



sample_y_offset = 0
sample_m_offset = 0
sample_d_offset = 350
historical_offset = 30 
resample_freq = '5B'
pc_window = 40

cross_country_daily_returns = (cross_country_data['_0Y_10Y'] - cross_country_data['_0Y_10Y'].shift(1)).drop(['TRY','BRL','TWD'], axis=1)

min_sample_size = len(cross_country_daily_returns.columns)

#historical
historical_loadings = {}

for i in np.arange(len(cross_country_daily_returns.index)-1, 0, -historical_offset):
    end_date = cross_country_daily_returns.index[i]
    start_date = end_date - DateOffset(years = sample_y_offset, months=sample_m_offset, days=sample_d_offset)
    print(start_date)
    print(end_date)
    samples = cross_country_daily_returns[(cross_country_daily_returns.index > start_date) & (cross_country_daily_returns.index < end_date)].dropna(axis=0)    
    print(len(samples))
    if (len(samples) >=  pc_window):
        #print('Added')
        sample_returns = samples.resample(resample_freq, label='right').sum().dropna()                   
        pca_obj = pca.PCA(sample_returns, rates_pca = True, momentum_size = None)
        
        #loadings = pd.concat([pca_obj.loadings['Loading1'].rename(lambda x: 'L1_'+ x), 
        #                      #pca_obj.loadings['Loading2'].rename(lambda x: 'L2_'+ x),
        #                      #pca_obj.loadings['Loading3'].rename(lambda x: 'L3_'+ x)
        #                      ])
        
        pcs = pd.Series(pca_obj.pcs['PC1'].tail(pc_window).values)
        print(pcs)
        if (len(pcs.index) >=  pc_window):
            print('Added')
            historical_loadings[end_date] = pcs
    else:
        print('ignore')
        

historical_pcs_corr = pd.DataFrame(historical_loadings).corr()
historical_pcs_corr = (historical_pcs_corr + historical_pcs_corr.transpose())/2
np.fill_diagonal(historical_pcs_corr.values, 1)

historical_pcs_corr_corr_dist_matrix = historical_pcs_corr.apply(lambda x: np.sqrt(4 *( 1 - x)))
historical_pcs_corr_corr_dist_matrix_condensed = squareform(historical_pcs_corr_corr_dist_matrix)

loadings_cluster_linkage_matrix = linkage(historical_pcs_corr_corr_dist_matrix_condensed, method='ward', optimal_ordering=False)


loading_clusters_fig = plt.figure(figsize = (45, 15))
plt.title('Loading Clustering')
plt.xlabel('Dates')
plt.ylabel('Independence')

dendrogram(
    loadings_cluster_linkage_matrix,
    leaf_rotation=45.,  # rotates the x axis labels
    leaf_font_size=8.,  # font size for the x axis labels
    labels = historical_pcs_corr.columns.strftime('%Y-%m-%d'),
    #color_threshold = 2
)








filedate = '2021-01-21'
xccybsdata = pd.read_csv('C:/Temp/test/market_watch/staging/'+ filedate + '_XCCYBS_historical.csv')  
xccybsdata['date'] = pd.to_datetime(xccybsdata['date'])
xccybsdata = xccybsdata.set_index('date').ffill()

pca.PCA(xccybsdata[[c for c in xccybsdata.columns if 'BS5' in c]].iloc[-3000:].drop(['CCBS5', 'RRBS5' , 'HFEBS5', 'TYBS5 CMPN', 'MPBS5E'], axis = 1),
                                                                                    rates_pca = True,  momentum_size = None).plot_loadings(n=4)



pca.PCA(xccybsdata[['CDBS1', 'CDBS2', 'CDBS3', 'CDBS4', 'CDBS5']].iloc[-1500:],
rates_pca = True,  momentum_size = None).plot_loadings(n=4)



#############################################################################################################################3
##### old PCA part
#############################################################################################################################3



#cross_country_data
#cross_country_structure


def generate_returns(data,periods,return_index=-1):
    rts = {}
    rts['spot'] = data.iloc[return_index]
    
    for p in periods:
        rts[p] = (data - data.shift(p)).iloc[return_index]
                    
    rts['WTD'] = ((data.resample('W', convention='end').last().iloc[-1] - data.resample('W', convention='end').last())[:-1]).iloc[return_index]
    rts['MTD'] = ((data.resample('M', convention='end').last().iloc[-1] - data.resample('M', convention='end').last())[:-1]).iloc[return_index]        
        
    rts = pd.DataFrame(rts).transpose()                
    return rts


cross_country_data_returns_by_name = {k: generate_returns(v, [1, 2, 5, 10, 20, 60, 120, 240]) for (k,v) in cross_country_data.items()}
cross_country_structure_returns_by_name = {k: generate_returns(v, [1, 2, 5, 10, 20, 60, 120, 240]) for (k,v) in cross_country_structure.items()}


cross_country_data_returns_by_p = {i:pd.DataFrame({k: v.loc[i] for (k,v) in cross_country_data_returns_by_name.items()}).transpose() for i in list(cross_country_data_returns_by_name.values())[0].index}
cross_country_structure_returns_by_p = {i:pd.DataFrame({k: v.loc[i] for (k,v) in cross_country_structure_returns_by_name.items()}).transpose() for i in list(cross_country_structure_returns_by_name.values())[0].index}





##########################
# output to a new workbook
##########################
wb1 = xw.Book(r'C:\Dev\Models\QuantModels\#applications\market_watch\global_rate_curve_valuation.xlsx')
sht1 = wb1.sheets('Swap')
sht1.range((1,1),(10000,300)).clear_contents()

row = 1
col = 2
#output returns
for rts_key, rts in rfmt_rts.items():
    sht1.range((row, col)).value = (rts * 100).round(1)
    sht1.range((row, col)).value = "{0} days rts".format(rts_key)
    row = row + 2 + len(rts.index)
#output spots
row = 1
col = 2 + len(rfmt_rts[list(rfmt_rts.keys())[0]].columns) + 2

sht1.range((row,col)).value = rfmt_spots[list(rfmt_spots.keys())[0]]

col = 2 + len(rfmt_rts[list(rfmt_rts.keys())[0]].columns) + 2 + len(rfmt_spots[list(rfmt_spots.keys())[0]].columns) + 2 

for spots_key, spots in rfmt_spots.items():
    if spots_key != 0:
        sht1.range((row, col)).value = (spots * 100).round(1)
        sht1.range((row, col)).value = "{0} days spot".format(spots_key)
        row = row + 2 + len(spots.index)














# calc pca for all the structures for defined history window for available countries
#cross_country_data_return = {k:(cross_country_data[k] - cross_country_data[k].shift(1)) for k in cross_country_data}
cross_country_data_return = {k:(cross_country_data[k]) for k in cross_country_data}


cross_country_data_pcas = {k:create_pca(cross_country_data_return[k], cross_country_data_return[k].index > cross_countries_pca_history, resample_freq= None, dummy=k) 
                                for k in cross_country_data_return}


cross_country_data_pc1_valuation_histories = {k:realign_cols(cross_country_data_pcas[k].data_gaps_from_reconstruction(n=[1]),countries) 
                                                 for k in cross_country_data_pcas}

cross_country_data_pc12_valuation_histories = {k:realign_cols(cross_country_data_pcas[k].data_gaps_from_reconstruction(n=[1,2]),countries) 
                                                 for k in cross_country_data_pcas}



cross_country_data_pc1_valuation_1d = pd.DataFrame({k:(cross_country_data_pc1_valuation_histories[k].rolling(1).mean().iloc[-1]*100).round(1)  
                                                              for k in cross_country_data_pc1_valuation_histories}).transpose()


cross_country_data_pc12_valuation_1d = pd.DataFrame({k:(cross_country_data_pc12_valuation_histories[k].rolling(1).mean().iloc[-1]*100).round(1)  
                                                              for k in cross_country_data_pc12_valuation_histories}).transpose()





# calc pca for all the structures for defined history window for available countries


#cross_country_structure_return = {k:(cross_country_structure[k] - cross_country_structure[k].shift(1)) for k in cross_country_structure}
cross_country_structure_return = {k:(cross_country_structure[k]) for k in cross_country_structure}


cross_country_structure_pcas = {k:create_pca(cross_country_structure_return[k],cross_country_structure_return[k].index > cross_countries_pca_history) 
                                for k in cross_country_structure_return}


cross_country_structure_pc1_valuation_histories = {k:realign_cols(cross_country_structure_pcas[k].data_gaps_from_reconstruction(n=[1]),countries) 
                                                 for k in cross_country_structure_pcas}


cross_country_structure_pc12_valuation_histories = {k:realign_cols(cross_country_structure_pcas[k].data_gaps_from_reconstruction(n=[1,2]),countries) 
                                                 for k in cross_country_structure_pcas}



cross_country_structure_pc1_valuation_1d = pd.DataFrame({k:(cross_country_structure_pc1_valuation_histories[k].rolling(1).mean().iloc[-1]*100).round(1)  
                                                              for k in cross_country_structure_pc1_valuation_histories}).transpose()


cross_country_structure_pc12_valuation_1d = pd.DataFrame({k:(cross_country_structure_pc12_valuation_histories[k].rolling(1).mean().iloc[-1]*100).round(1)  
                                                              for k in cross_country_structure_pc12_valuation_histories}).transpose()



#output to workbooks

wb1 = xw.Book()
sht1 = wb1.sheets(1)
sht1.range((1,1),(10000,300)).clear_contents()

sht1.range((1,1)).value = cross_country_data_pc1_valuation_1d
sht1.range((1, 3 + len(cross_country_data_pc1_valuation_1d.columns))).value = cross_country_data_pc12_valuation_1d
sht1.range((1, (3 + len(cross_country_data_pc1_valuation_1d.columns))*2)).value = cross_country_structure_pc1_valuation_1d
sht1.range((1, (3 + len(cross_country_data_pc1_valuation_1d.columns))*3)).value = cross_country_structure_pc12_valuation_1d




for pca_description, pca_object in cross_country_data_pcas.items():
    print(pca_description)
    loading_fig = pca_object.plot_loadings(n=5)
    plt.savefig('C:/Temp/test/market_watch/output/global_overview/%s_%s_7d_%s.png' % (filedate, modeldate, pca_description), bbox_inches='tight')
    plt.close()
    
   

for pca_description, pca_object in cross_country_structure_pcas.items():
    print(pca_description)
    loading_fig = pca_object.plot_loadings(n=5)
    plt.savefig('C:/Temp/test/market_watch/output/global_overview/%s_%s_7d_%s.png' % (filedate, modeldate, pca_description), bbox_inches='tight')
    plt.close()
    



#country_spot_return = {k:(country_spot_data[k] - country_spot_data[k].shift(7)) for k in country_spot_data}
country_spot_return = {k:(country_spot_data[k]) for k in country_spot_data}

country_spot_data_pcas = {k:create_pca(country_spot_data[k][country_spot_return[k].index > cross_countries_pca_history], resample_freq= None, dummy=k) 
                                for k in country_spot_return}

















#trouble shoot

#test1

data_tenors_test1 = '_3Y_10Y'

structure_tenors_test1 = '0Y_2S10S'  # '2S55S' '0Y_2S10S'

#data_test1 = cross_country_data['_3Y_3Y'][['USD', 'AUD']].rename(lambda c: "{}_{}".format(c,data_tenors_test1) , axis=1)
data_test1 = cross_country_data['_3Y_3Y'].rename(lambda c: "{}_{}".format(c,'3Y_3Y') , axis=1)

#data_test1 = cross_country_structure[structure_tenors_test1][[ 'AUD', 'USD']].rename(lambda c: "{}_{}".format(c,structure_tenors_test1) , axis=1)

data_return_test1 = data_test1 - data_test1.shift(7)

#pca_obj1_test1 = create_pca(data_test1[data_test1.index > "2020-04-01"][['AUD','EUR']] , resample_freq= None, matrixoveride=data_return_test1[data_return_test1.index > "2020-04-01"][['AUD','EUR']].cov())
create_pca(data_test1[data_test1.index > "2020-04-01"] , resample_freq= None).plot_loadings(n=5)
create_pca(data_test1[data_test1.index > "2020-01-01"] , resample_freq= None).plot_loadings(n=5)

create_pca(data_return_test1[data_return_test1.index > "2020-04-01"] , resample_freq= None).plot_loadings(n=5)



data_test2 = cross_country_data['_5Y_5Y'].rename(lambda c: "{}_{}".format(c,'5Y_5Y') , axis=1)
create_pca(data_test2[data_test2.index > "2020-04-01"] , resample_freq= None).plot_loadings(n=5)
create_pca(data_test2[data_test2.index > "2020-01-01"] , resample_freq= None).plot_loadings(n=5)


data_test2 = cross_country_data['_2Y_1Y'].rename(lambda c: "{}_{}".format(c,'2Y_1Y') , axis=1)
create_pca(data_test2[data_test2.index > "2020-04-01"] , resample_freq= None).plot_loadings(n=5)
create_pca(data_test2[data_test2.index > "2020-01-01"] , resample_freq= None).plot_loadings(n=5)



data_test2 = cross_country_structure['11S33S'].rename(lambda c: "{}_{}".format(c,'11S33S') , axis=1)
create_pca(data_test2[data_test2.index > "2020-04-01"] , resample_freq= None).plot_loadings(n=5)
create_pca(data_test2[data_test2.index > "2020-01-01"] , resample_freq= None).plot_loadings(n=5)










cross_country_mrs = create_combo_mr(data_test1[data_test1.index > "2020-04-01"], elements = 2, resample_freq= 'B', 
                                    include_normal_weighted_series=True, 
                                    matrixoveride = None)



for mr_desc, (mr_obj, pca_obj) in cross_country_mrs.items():
    print(mr_desc)
    mr_obj.create_mr_info_fig((18, 7), pca_obj = pca_obj)

    

(b, a) = np.polyfit(list(ds.bin_mid.values), list(ds.bin_avg.values), deg = 1)
fitted_ys = [round(a + b * x, 4) for x in list(ds.bin_mid.values)]


d = audusd_2255_rtn_test1[audusd_2255_rtn_test1.index > "2020-02-01"]




#################################################################################################################################################

pca.PCA(cross_country_data['_3Y_3Y'].iloc[-2500:].dropna(axis=1), rates_pca = True, momentum_size = None).plot_loadings(n=3)

pca.PCA(cross_country_data['_3Y_3Y'].iloc[-750:].dropna(axis=1), rates_pca = True, momentum_size = None).plot_loadings(n=3)

pca.PCA(cross_country_data['_3Y_3Y'].iloc[-1250:][['USD','KRW','NZD']].dropna(axis=1), rates_pca = True, momentum_size = None).plot_loadings(n=3)

pca.PCA(cross_country_data['_3Y_3Y'].iloc[-2500:][['USD','KRW']].dropna(axis=1), rates_pca = True, momentum_size = None).create_hedged_pcs_fig()

pca.PCA(cross_country_data['_3Y_3Y'].iloc[-750:][['NZD','KRW']].dropna(axis=1), rates_pca = True, momentum_size = None).create_hedged_pcs_fig()


pca.PCA(cross_country_data['_3Y_3Y'].iloc[-2500:].dropna(axis=1), rates_pca = True, momentum_size = None).create_hedged_pcs_fig()


pca.PCA(pd.concat([cross_country_data['_3Y_3Y'].rename(lambda x: x +"_2Y2Y", axis=1), 
                   cross_country_data['_5Y_5Y'].rename(lambda x: x +"_5Y5Y", axis=1),
                   cross_country_structure['2Y_2S10S'].rename(lambda x: x +"_2Y_210", axis=1)], axis=1).iloc[-2500:].dropna(axis=1), 
        rates_pca = True, momentum_size = None).plot_loadings(n=6)



pca.PCA(pd.concat([cross_country_data['_3Y_3Y'].rename(lambda x: x +"_3Y3Y", axis=1), cross_country_data['_5Y_5Y'].rename(lambda x: x +"_5Y5Y", axis=1)], axis=1).iloc[-2500:].dropna(axis=1), 
        rates_pca = True, momentum_size = None).plot_valuation_history(750, n=[1], w=16, h=12, rows=6, cols=6, normalise_valuation_yaxis = False, 
                               normalise_data_yaxis=False, exp_standardise_val=False,
                               selected_series=[])

                                                                                                                                             
pca.PCA(pd.concat([cross_country_data['_3Y_2Y'][['AUD','NZD']].rename(lambda x: x +"_3Y2Y", axis=1), 
                   cross_country_data['_3Y_10Y'][['AUD','NZD']].rename(lambda x: x +"_3Y10Y", axis=1)], 
                  axis=1).iloc[-2500:].dropna(axis=1), 
        rates_pca = True, momentum_size = None).plot_loadings(n=6)


                                                                                                                                             
pca.PCA(pd.concat([cross_country_data['_1Y_2Y'][['AUD']].rename(lambda x: x +"_1Y2Y", axis=1), 
                   cross_country_data['_3Y_3Y'][['AUD']].rename(lambda x: x +"_3Y3Y", axis=1),
                   cross_country_data['_0Y_10Y'][['AUD']].rename(lambda x: x +"_0Y10Y", axis=1),
                   cross_country_data['_0Y_30Y'][['AUD']].rename(lambda x: x +"_0Y30Y", axis=1)], 
                  axis=1).iloc[-2500:].dropna(axis=1), 
        rates_pca = True, momentum_size = None).plot_loadings(n=6)
                                                                       