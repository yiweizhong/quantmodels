"""Config for BBG"""
import os, sys, inspect
import logging
import datetime

TEST_MODE = True

"""Config for BBG"""
APP_NAME = r"market_watch"
CURRENT_PATH = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0])) 
APP_ROOT = r'c:/temp/test/' + APP_NAME

DTYPE_DATEIME64 = r"datetime64[ns]"
ODBC_STYLE_CSTR_PREFIX = r"mssql+pyodbc:///?odbc_connect=%s"
LOG_FORMAT = r"%(asctime)s - %(name)s - %(levelname)s - %(message)s"
TIMESTAMP_FMT = r"%Y-%m-%d %H:%M:%S.%f"

START_DATE_OFFSET = 30
FILE_SUFFIX = "_bar.csv"
EVENT = "TRADE"
BAR_INTERVAL = 1


#if there is any data object will need to be restart from fresh in firebase, specify the name here
FDB_ROOT = "BARDATA"
SAVE_TO_FDB = []
OVERRIDE_EXISTING_DATA = []


LOG_FOLDER = ''
DEBUG_FOLDER = ''
OUTPUT_FOLDER = ''
STAGING_FOLDER = ''
ARCHIVE_FOLDER = ''
ARCHIVE_LOG_FOLDER = ''
ARCHIVE_DEBUG_FOLDER = ''
ARCHIVE_OUTPUT_FOLDER = ''
ARCHIVE_STAGING_FOLDER = ''
INPUT_FOLDER = ''

ARCHIVE_FILES = False

def config_path():

    _parent_path = os.path.abspath(os.path.join(CURRENT_PATH, os.pardir))
    _gparent_path = os.path.abspath(os.path.join(_parent_path, os.pardir))
    _ggparent_path = os.path.abspath(os.path.join(_gparent_path, os.pardir))

    #adding the path here
    if (TEST_MODE):        
        _core_path = _gparent_path
        if _core_path not in sys.path: sys.path.insert(0, _core_path)
    else:
        pass

    import core.dirs as dirs
    
    global INPUT_FOLDER, LOG_FOLDER, DEBUG_FOLDER, OUTPUT_FOLDER, ARCHIVE_FOLDER, ARCHIVE_LOG_FOLDER, ARCHIVE_DEBUG_FOLDER, ARCHIVE_OUTPUT_FOLDER, STAGING_FOLDER, ARCHIVE_STAGING_FOLDER

    INPUT_FOLDER = dirs.get_creat_sub_folder(folder_name='input', parent_dir=APP_ROOT) 
    LOG_FOLDER = dirs.get_creat_sub_folder(folder_name='log', parent_dir=APP_ROOT)
    DEBUG_FOLDER = dirs.get_creat_sub_folder(folder_name='debug', parent_dir=APP_ROOT)
    OUTPUT_FOLDER = dirs.get_creat_sub_folder(folder_name='output', parent_dir=APP_ROOT)
    STAGING_FOLDER = dirs.get_creat_sub_folder(folder_name='staging', parent_dir=APP_ROOT)
    ARCHIVE_FOLDER  = dirs.get_creat_sub_folder(folder_name='archive', parent_dir=APP_ROOT)
    ARCHIVE_LOG_FOLDER = dirs.get_creat_sub_folder(folder_name="log", parent_dir=ARCHIVE_FOLDER)
    ARCHIVE_DEBUG_FOLDER = dirs.get_creat_sub_folder(folder_name="debug", parent_dir=ARCHIVE_FOLDER)
    ARCHIVE_OUTPUT_FOLDER = dirs.get_creat_sub_folder(folder_name="Output", parent_dir=ARCHIVE_FOLDER)
    ARCHIVE_STAGING_FOLDER = dirs.get_creat_sub_folder(folder_name="staging", parent_dir=ARCHIVE_FOLDER)

    ###
    _log_path = os.path.join(LOG_FOLDER + '/' + APP_NAME + '_' + datetime.datetime.now().strftime("%Y%m%d_%H")+'.log')
    logging.basicConfig(filename=_log_path, level=logging.INFO, format = '%(asctime)s - %(levelname)s - %(message)s')
    
    logging.info("sys.path:")
    for p in sys.path: logging.info(p)
    #for p in sys.path: print p
    return

###
config_path()

#override the output folder in case it is part of other program
OUTPUT_FOLDER = STAGING_FOLDER
ARCHIVE_OUTPUT_FOLDER = ARCHIVE_STAGING_FOLDER
