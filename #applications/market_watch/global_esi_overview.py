from __future__ import division
import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper

import argparse




def create_combo_mr(data, date_index, elements = 2, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None):
    mrs = {}
    data_sub = data[date_index]
    combos = list(combinations(data.columns, elements))
    instrument = combos[0][0][3:]
    
    for combo in combos:
        _sub_data = data_sub[list(combo)].resample(resample_freq).last() if resample_freq is not None else data_sub[list(combo)]
        data_sub_pca = pca.PCA(_sub_data, rates_pca = True, momentum_size = momentum_size)
        hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_pca.pc_hedge
        mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
        mrs[('_').join(combo) + '_PCW'] = (mr_obj, data_sub_pca)
        if include_normal_weighted_series:
            if len(_sub_data.columns) == 2:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] - _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s - %s' % (_sub_data.columns[0], _sub_data.columns[1])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)
            elif len(_sub_data.columns) == 3:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] + _sub_data[_sub_data.columns[2]] - 2 * _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s -2 x %s + %s' % (_sub_data.columns[0], _sub_data.columns[1], _sub_data.columns[2])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)

    return mrs



#command line args
parser = argparse.ArgumentParser(description='---- global rates overview ----')
parser.add_argument('filedate', metavar='filedate', type=str, nargs=1, help='date in yyyy-mm-dd')
parser.add_argument('modeldate', metavar='modeldate', type=str, nargs=1, help='date in yyyy-mm-dd')
parser.add_argument('cross_countries_pca_history', metavar='cross_countries_pca_history', type=int, nargs=1, help='number of the months of history will be used for cross country pca')
parser.add_argument('pair_pca_history', metavar='pair_pca_history', type=int, nargs=1, help='number of the months of history will be used for cross country pca')

args = parser.parse_args()
print(args.filedate)
print(args.modeldate)
print(args.cross_countries_pca_history)
print(args.pair_pca_history)

# data
    
filedate = args.filedate[0]
modeldate = args.modeldate[0]
cross_countries_pca_history = args.cross_countries_pca_history[0]
pair_pca_history = args.pair_pca_history[0]


data = pd.read_csv('C:/Temp/test/market_watch/staging/'+ filedate + '_ESI_historical.csv')

data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()


ticker_exp = [c for c in data.columns if '_EXP' in c]
ticker_data = [c for c in data.columns if '_DATA' in c]





#dates
dt_10yrs_ago = data.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_5yrs_ago = data.index[-1] - DateOffset(years = 5, months=0, days=0)
dt_3yrs_ago = data.index[-1] - DateOffset(years = 3, months=0, days=0)
dt_2yrs_ago = data.index[-1] - DateOffset(years = 2, months=0, days=0)
dt_1yrs_ago = data.index[-1] - DateOffset(years = 1, months=0, days=0)
dt_18mths_ago = data.index[-1] - DateOffset(years = 0, months=18, days=0)

cross_countries_pca_history = data.index[-1] - DateOffset(years = 0, months=cross_countries_pca_history, days=0)
pair_pca_history = data.index[-1] - DateOffset(years = 0, months=pair_pca_history, days=0)


data_per_country = {}

for i in np.arange(0, len(ticker_exp)):
    data_per_country[ticker_exp[i][:3]] = pd.DataFrame({ticker_exp[i]: data[ticker_exp[i]], ticker_data: data[ticker_data[i]]})

per_country_mrs = {} 

for c_label, c_data in data_per_country.items():
    per_country_mrs = {**per_country_mrs, **create_combo_mr(c_data, data.index > pair_pca_history, elements = 2, resample_freq= 'W', include_normal_weighted_series=True)}

for mr_desc, (mr_obj, pca_obj) in per_country_mrs.items():
    print(mr_desc)
    mr_fig = mr_obj.create_mr_info_fig((18, 7), pca_obj = pca_obj)
    plt.savefig('C:/Temp/test/market_watch/output/global_esi_overview/%s_%s_%s.png' % (filedate, modeldate, mr_desc), bbox_inches='tight')
    plt.close()


