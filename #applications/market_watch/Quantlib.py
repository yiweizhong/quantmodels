import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
import statsmodels.formula.api as smf
import statsmodels.tsa as tsa
from statsmodels.graphics.tsaplots import plot_acf
from pandas.plotting import autocorrelation_plot

import seaborn as sns
import numpy as np
import scipy


import QuantLib as ql


today = ql.Date().todaysDate()
us_calendar = ql.UnitedStates()
t_plus2 = calendar.advance(today, 2, ql.Days)



#https://quant.stackexchange.com/questions/54601/quantlib-convert-par-swap-rates-to-zero-rates-back-and-forth
#https://quant.stackexchange.com/questions/32345/quantlib-python-dual-curve-bootstrapping-example

#depoist
#future
#swaps

curve_tenors = {3:0.19688,
                6:0.184,
                9:0.1864,
                12:0.2036,
                18:0.2163,
                21:0.2329,
                24:0.2589,
                36:0.4413,
                48:0.7004,
                60:0.9474,
                72:1.1678,
                84:1.3493,
                96:1.4925,
                108:1.606,
                120:1.7005,
                132:1.779,
                144:1.8449,
                180:1.9769,
                240:2.08,
                300:2.1171,
                360:2.1312,
                480:2.0814,
                600:2.001}


us_swap_curve_inputs = ql.RateHelperVector()

for t, r in curve_tenors.items():
    if t == 3:
        us_swap_curve_inputs.append(ql.DepositRateHelper(r/100, ql.USDLibor(ql.Period('3M'))))
    else:
        par_swap = ql.UsdLiborSwapIsdaFixAm(ql.Period('{}M'.format(t)))  
        us_swap_curve_inputs.append(ql.SwapRateHelper(r/100, par_swap)) 


us_swap_curve = ql.PiecewiseLogLinearDiscount(t_plus2, us_swap_curve_inputs, ql.Thirty360())
us_swap_curve = ql.PiecewiseFlatForward(t_plus2, us_swap_curve_inputs, ql.Thirty360())

us_swap_curve_yts = ql.YieldTermStructureHandle(us_swap_curve)
us_swap_pricing_engine = ql.DiscountingSwapEngine(us_swap_curve_yts)

usdlibor3m = ql.USDLibor(ql.Period('3M'), us_swap_curve_yts)

print("maturity, market, model")

for tenor, rate in curve_tenors.items():
    swap = ql.MakeVanillaSwap(ql.Period('{}M'.format(tenor)), usdlibor3m, 0.01, ql.Period('0D'), pricingEngine=us_swap_pricing_engine,
                              fixedLegDayCount=ql.Thirty360(),
                              floatingLegDayCount=ql.Actual360())    
    print(f"{tenor}, {rate:.6f}, {swap.fairRate():.6f}")



#SOFR OIS

#https://quant.stackexchange.com/questions/37532/set-up-overnightindex-quantlib

name = 'SOFRATE'
fixingDays = 2
currency = ql.USDCurrency()
calendar = ql.UnitedStates()
dayCounter = ql.Actual360()
sofr_index = ql.OvernightIndex(name, fixingDays, currency, calendar, dayCounter)


SOFR_tenors = {
                '1D':0.01,
                # '1W':0.011305,
                # '2W':0.011305,
                # '3W':0.0109,
                # '1M':0.011,
                # '2M':0.0141,
                # '3M':0.01645,
                # '4M':0.019,
                # '5M':0.0204,
                # '6M':0.0214505,
                # '7M':0.027,
                # '8M':0.02525,
                # '9M':0.032,
                # '10M':0.0317,
                # '11M':0.0354,
                 '12M':0.03895,
                # '18M':0.0532,
                # '2Y':0.09512,
                # '3Y':0.261,
                # '4Y':0.49944,
                '5Y':0.72953,
                # '6Y':0.937,
                # '7Y':1.1110005,
                # '8Y':1.2525105,
                # '9Y':1.361,
                # '10Y':1.44945,
                # '12Y':1.5876005,
                # '15Y':1.71439,
                # '20Y':1.8140005,
                # '25Y':1.8466,
                # '30Y':1.86075,
                # '40Y':1.8045005,
                # '50Y':1.7195
}




sofr_curve_inputs = ql. RateHelperVector()

for t, r in SOFR_tenors.items():
    if t == '1D':
        sofr_curve_inputs.append(ql.DepositRateHelper(ql.QuoteHandle(ql.SimpleQuote(r/100)),
                                                      ql.Period(1,ql.Days), 
                                                      0, #overnight
                                                      calendar,
                                                      ql.Following,
                                                      False, 
                                                      dayCounter))
    else: 
        sofr_curve_inputs.append(ql.OISRateHelper(2, 
                                                  ql.Period(t),
                                                  ql.QuoteHandle(ql.SimpleQuote(r/100)),
                                                  sofr_index)) 

sofr_curve = ql.PiecewiseLogLinearDiscount(t_plus2, sofr_curve_inputs,dayCounter)
sofr_curve.enableExtrapolation()
sofr_yts = ql.YieldTermStructureHandle(sofr_curve)
sofr_pricing_engine = ql.DiscountingSwapEngine(sofr_yts)
sofr_index_new = ql.OvernightIndex(name, fixingDays, currency, calendar, dayCounter, sofr_yts)




swap = ql.MakeVanillaSwap(ql.Period('12M'), sofr_index_new, 0.01, ql.Period('0D'), pricingEngine=sofr_pricing_engine,
                              fixedLegDayCount=ql.Actual360(),
                              floatingLegDayCount=ql.Actual360())    
    print(f"{tenor}, {rate:.6f}, {swap.fairRate():.6f}")





for i in [5]:
    sofr = sofr_curve.forwardRate(t_plus2, 
                           calendar.advance(t_plus2,i,ql.Years),                            
                           dayCounter,
                           ql.Compounded,
                           ql.Annual).rate()
    print(sofr)
                       






