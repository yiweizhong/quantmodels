import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations
import xlwings as xw

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper


#################################################################################################################
#################################################################################################################
#functions
#################################################################################################################
#################################################################################################################

def create_pca(data, date_index, momentum_size=None, resample_freq= 'W'):

    data_sub = data[date_index].resample(resample_freq).last() if resample_freq is not None else data[date_index]    
    return pca.PCA(data_sub, rates_pca = True, momentum_size = momentum_size)




def create_combo_mr(data, date_index, elements = 2, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None):
    mrs = {}
    data_sub = data[date_index]
    combos = list(combinations(data.columns, elements))
    instrument = combos[0][0][3:]
    
    for combo in combos:
        _sub_data = data_sub[list(combo)].resample(resample_freq).last() if resample_freq is not None else data_sub[list(combo)]
        data_sub_pca = pca.PCA(_sub_data, rates_pca = True, momentum_size = momentum_size)
        hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_pca.pc_hedge
        mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
        mrs[('_').join(combo) + '_PCW'] = (mr_obj, data_sub_pca)
        if include_normal_weighted_series:
            if len(_sub_data.columns) == 2:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] - _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s - %s' % (_sub_data.columns[0], _sub_data.columns[1])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)
            elif len(_sub_data.columns) == 3:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] + _sub_data[_sub_data.columns[2]] - 2 * _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s -2 x %s + %s' % (_sub_data.columns[0], _sub_data.columns[1], _sub_data.columns[2])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)

    return mrs




def generate_returns(data,periods):
    rts = {}
    spots = {}
    spots[0] = data
    for p in periods:
        rts[p] = data - data.shift(p)
        spots[p] = data.shift(p)
    
    #week to date
    
    rts['WTD'] = (data.resample('W', convention='end').last().iloc[-1] - data.resample('W', convention='end').last())[:-1]
    spots['WTD'] = data.resample('W', convention='end').last()[:-1]
    rts['MTD'] = (data.resample('M', convention='end').last().iloc[-1] - data.resample('M', convention='end').last())[:-1]
    spots['MTD'] = data.resample('W', convention='end').last()[:-1]
    
        
    return (rts, spots)


def generate_zscore(data,periods):
    #rts = {}
    #spots = {}
    #spots[0] = data
    zsc = {}
    for p in periods:
        #rts[p] = data - data.shift(p)
        zsc[p] = (data - data.rolling(p).mean()) / data.rolling(p).std()
        #spots[p] = data.shift(p)
        #zsc[p] = pd.DataFrame({"{} days zsc".format(p): (data.iloc[-1] - data.iloc[-30:].mean()) / data.iloc[-30:].std()}).transpose()
    #week to date        
    return zsc





def refmt_returns_by_p(rts, tickers, date_offset):
    rfmt_rts_by_p = {}
    for rt_key, rt in rts.items():
        rfmt_rts = []
        for ticker in tickers:
                tmp = rt.iloc[date_offset][ticker].rename(lambda n: n[:3]).rename(ticker[0][3:]).to_frame()
                #print(tmp)
                rfmt_rts.append(tmp)
                #if len(rfmt_rts) > 1:
                #    print(pd.concat(rfmt_rts, axis=1, join='outer',sort=False))
                
        tmp = pd.concat(rfmt_rts, axis=1, join='outer',sort=False).transpose()
        if 'AUD' in tmp.columns and  'USD' in tmp.columns:
            tmp['AUDUSD'] = tmp['AUD'] - tmp['USD']
        if 'CAD' in tmp.columns and  'USD' in tmp.columns:
            tmp['CADUSD'] = tmp['CAD'] - tmp['USD']
        if 'EUR' in tmp.columns and  'USD' in tmp.columns:
            tmp['EURUSD'] = tmp['EUR'] - tmp['USD']
        if 'AUD' in tmp.columns and  'NZD' in tmp.columns:
            tmp['AUDNZD'] = tmp['AUD'] - tmp['NZD']
        rfmt_rts_by_p[rt_key] = tmp
    return rfmt_rts_by_p



def plot_heatmap(tbl, ax):
    #mask = np.triu(np.ones_like(cov, dtype=np.bool))

    
    # Generate a custom diverging colormap
    cmap = sns.diverging_palette(10, 240, as_cmap=True)
    
    # Draw the heatmap with the mask and correct aspect ratio
    #sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
    _ax = sns.heatmap(tbl, cmap=cmap, center=0,
                square=True, annot=True, linewidths=.5, cbar_kws={"shrink": .5},
                annot_kws={"size": 9}, ax=ax)
    _ax.set_yticklabels(ax.get_yticklabels(), rotation=90, horizontalalignment='right')
    
    return  _ax



#################################################################################################################
#################################################################################################################
#################################################################################################################
# dates
#################################################################################################################
#################################################################################################################
#################################################################################################################




rundate = '2020-06-17' 

#################################################################################################################
#################################################################################################################
#################################################################################################################
# Gov rates
#################################################################################################################
#################################################################################################################
#################################################################################################################

data_gov = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_GOV_historical.csv')  
data_gov['date'] = pd.to_datetime(data_gov['date'])
data_gov = data_gov.set_index('date').ffill()

#dates
dt_10yrs_ago = data_gov.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_13yrs_ago = data_gov.index[-1] - DateOffset(years = 13, months=0, days=0)
dt_5yrs_ago = data_gov.index[-1] - DateOffset(years = 5, months=0, days=0)
dt_3yrs_ago = data_gov.index[-1] - DateOffset(years = 3, months=0, days=0)
dt_2yrs_ago = data_gov.index[-1] - DateOffset(years = 2, months=0, days=0)
dt_1yrs_ago = data_gov.index[-1] - DateOffset(years = 1, months=0, days=0)
dt_18mths_ago = data_gov.index[-1] - DateOffset(years = 0, months=18, days=0)

cross_countries_pca_history = dt_5yrs_ago


###############
#rates_tickers
###############
rate_gov_ticker_1y = [c for c in data_gov.columns if '_0Y_1Y' in c and  'NOK' not in c and  'ZAR' not in c]
rate_gov_ticker_2y = [c for c in data_gov.columns if '_0Y_2Y' in c and  'NOK' not in c and  'ZAR' not in c]
rate_gov_ticker_3y = [c for c in data_gov.columns if '_0Y_3Y' in c and  'NOK' not in c and  'ZAR' not in c]
rate_gov_ticker_4y = [c for c in data_gov.columns if '_0Y_4Y' in c and  'NOK' not in c and  'ZAR' not in c]
rate_gov_ticker_5y = [c for c in data_gov.columns if '_0Y_5Y' in c and  'NOK' not in c and  'ZAR' not in c]
rate_gov_ticker_7y = [c for c in data_gov.columns if '_0Y_7Y' in c and  'NOK' not in c and  'ZAR' not in c]
rate_gov_ticker_8y = [c for c in data_gov.columns if '_0Y_8Y' in c and  'NOK' not in c and  'ZAR' not in c]
rate_gov_ticker_10y = [c for c in data_gov.columns if '_0Y_10Y' in c and  'NOK' not in c and  'ZAR' not in c]
rate_gov_ticker_15y = [c for c in data_gov.columns if '_0Y_15Y' in c and  'NOK' not in c and  'ZAR' not in c]
rate_gov_ticker_20y = [c for c in data_gov.columns if '_0Y_20Y' in c and  'NOK' not in c and  'ZAR' not in c]
rate_gov_ticker_30y = [c for c in data_gov.columns if '_0Y_30Y' in c and  'NOK' not in c and  'ZAR' not in c] 


rates_gov_ticker = [
    'GBP_0Y_1Y',
    'AUD_0Y_1Y',
    'NZD_0Y_1Y',
    'EUR_0Y_1Y',
    'CAD_0Y_1Y',
    'USD_0Y_1Y'] +[
     'GBP_0Y_10Y',
     'AUD_0Y_10Y',
     'NZD_0Y_10Y',
     'EUR_0Y_10Y',
     'CAD_0Y_10Y',
     'USD_0Y_10Y',
     'AUT_0Y_10Y',
     'BEL_0Y_10Y',
     #'BRL_0Y_10Y',
     #'CHF_0Y_10Y',
     #'CLP_0Y_10Y',
     #'CNY_0Y_10Y',
     'ESP_0Y_10Y',
     'FRA_0Y_10Y',
     'HKD_0Y_10Y',
     'IDR_0Y_10Y',
     'INR_0Y_10Y',
     'ITA_0Y_10Y',
     'JPY_0Y_10Y',
     'KOR_0Y_10Y',
     #'MEX_0Y_10Y',
     'MYS_0Y_10Y',
     'NLD_0Y_10Y',
     #'NOK_0Y_10Y',
     'PHP_0Y_10Y',
     'PLN_0Y_10Y',
     'PRT_0Y_10Y',
     #'RUS_0Y_10Y',
     #'SGD_0Y_10Y',
     #'SEK_0Y_10Y',
     'THB_0Y_10Y',
     'ZAR_0Y_10Y',
     'TWD_0Y_10Y']

rates_10y_valuation_data = data_gov[rates_gov_ticker]


rates_10y_valuation_data.plot()


rates_10y_valuation_data.cov()        

fig = plt.figure(figsize=(20,20))
ax0 = fig.add_subplot(111)


plot_heatmap(rates_10y_valuation_data.cov(), ax0)

rates_10y_valuation_data_pca = pca.PCA(rates_10y_valuation_data, rates_pca = True, momentum_size = None)
rates_10y_valuation_data_pca.plot_loadings(n=3, m=5)

val_countries=[
     'GBP_0Y_10Y',
     'AUD_0Y_10Y',
     'NZD_0Y_10Y',
     'EUR_0Y_10Y',
     'CAD_0Y_10Y',
     'USD_0Y_10Y',
     'ESP_0Y_10Y',
     'FRA_0Y_10Y',
     'HKD_0Y_10Y',
     'IDR_0Y_10Y',
     'INR_0Y_10Y',
     'ITA_0Y_10Y',
     'JPY_0Y_10Y',
     'KOR_0Y_10Y',
     'MYS_0Y_10Y',
     'NLD_0Y_10Y',
     'PHP_0Y_10Y',
     'PLN_0Y_10Y',
     'PRT_0Y_10Y',
     'THB_0Y_10Y',
     'ZAR_0Y_10Y',
     'TWD_0Y_10Y']

rates_10y_valuation_data_pca.plot_valuation_history(2000, n=[1,2,3], w=16, h=12, rows=6, cols=6, normalise_valuation_yaxis = False,  normalise_data_yaxis = False )








    














