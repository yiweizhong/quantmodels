import pandas as pd
import numpy as np
import os, sys, inspect
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from cycler import cycler
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations

from pandas.tseries.offsets import DateOffset


_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import sbins
from statistics.mr import stochastic as stoch
from statistics.mr import helper


from scipy import stats
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, fcluster
from scipy.spatial.distance import pdist, squareform

from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from pandas.plotting import autocorrelation_plot

import seaborn as sns
import numpy as np
import scipy
import scipy.stats as st



pd.set_option('display.expand_frame_repr', False)



def convert_atmf_vol_to_yield(option, vols):

    base = 1/np.sqrt(2*3.14*12)
    if option.upper() == '1M':
        return base * np.sqrt(1) * vols
    elif option.upper() == '2M':
        return base * np.sqrt(2) * vols     
    elif option.upper() == '3M':
        return base * np.sqrt(3) * vols
    elif option.upper() == '6M':
        return base * np.sqrt(6)* vols
    elif option.upper() == '1Y':
        return base * np.sqrt(12)* vols
    elif option.upper() == '2Y':
        return base * np.sqrt(24)* vols
    elif option.upper() == '3Y':
        return base * np.sqrt(36)* vols
    elif option.upper() == '4Y':
        return base * np.sqrt(48)* vols
    elif option.upper() == '5Y':
        return base * np.sqrt(60)* vols
    elif option.upper() == '6Y':
        return base * np.sqrt(72)* vols
    elif option.upper() == '7Y':
        return base * np.sqrt(84)* vols
    elif option.upper() == '8Y':
        return base * np.sqrt(96)* vols
    elif option.upper() == '9Y':
        return base * np.sqrt(108)* vols
    elif option.upper() == '10Y':
        return base * np.sqrt(120)* vols    
    
    else:
        raise ValueError("unknown optin tenor {0}".format(option))


def plot_scatter_main(ax, xs, ys, colors, cmaps, ticklabelsize=8):
    mapable = ax.scatter(xs, ys, c=colors, cmap=cmaps, marker='o', s = 9)
        
    ax.set_ylabel(ys.name, fontsize=8)
    ax.set_xlabel(xs.name, fontsize=8)
    ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
    ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
    
    for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    
    for label,tick in zip(ax.get_xticklabels(), ax.get_xticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    return mapable


def plot_scatter_endpoints(ax, xs, ys, colors, sizes, labels, markers, legend_loc=None,ncol=1, fontsize=8, lw =3):
    ds = [ax.scatter(xs[i], ys[i], c=colors[i], s=sizes[i], label=labels[i], marker=markers[i], linewidth= lw) for i in np.arange(len(xs))]
    if legend_loc is not None:
        lg =ax.legend(ds, labels, scatterpoints=1, fancybox=True, framealpha=0, loc=legend_loc, ncol=ncol, fontsize=fontsize)
        ax.add_artist(lg)
    return ds
    


def plot_scatter_fits(ax, xss, yss, label_prefixes, colors, legend_loc=None, ncol=1, fontsize=8):
    #ploting multiple fits
    pcs = []
    for (xs, ys, label_prefix, color) in zip(xss, yss, label_prefixes, colors): 
        (beta_x, beta_0), (xs, fitted_ys, residual_ys, residual_ys_std, residual_ys_p_1std, residual_ys_m_1std) = get_linear_betas(xs, ys)
        label = r'{1} = {3:.2f} + {2:.2f} * {0} with stdev {4:.2f} '.format(xs.name, ys.name, beta_x, beta_0, residual_ys_std)
        label = label_prefix + ', ' + label if label_prefix is not None else label
        pcs = pcs + ax.plot(xs, fitted_ys, lw=1, c=color, ls = '-', marker = '.', ms = 0, label=label)
        ax.plot(xs, residual_ys_p_1std, lw=0.5, c=color, ls = ':', ms = 0)
        ax.plot(xs, residual_ys_m_1std, lw=0.5, c=color, ls = ':', ms = 0)

    if legend_loc is not None:
        lgd = ax.legend(handles=pcs, loc=legend_loc, fancybox=True, framealpha=0, ncol=ncol, fontsize=fontsize)
        ax.add_artist(lgd)
    return pcs






def get_linear_betas(xs, ys):
    betas, _, _, _, _ = np.polyfit(list(xs), list(ys), deg = 1, full=True)
    (beta_x, beta_0) = betas
    #xs = df[df.columns[0]]
    #ys = df[df.columns[1]]
    #start_x = xs.min() 
    #last_x = xs.max()
    #step = (last_x - start_x)/100 
    #xs = np.arange(start_x,last_x, step)
    fitted_ys = [round(beta_0 + beta_x * x, 4) for x in list(xs)]
    residual_ys = [y - fitted_y for y, fitted_y in  zip(ys, fitted_ys)]
    residual_ys_std = np.std(residual_ys, ddof=1) 
    residual_ys_p_1std =  [fitted_y + residual_ys_std for fitted_y in  fitted_ys]
    residual_ys_m_1std =  [fitted_y - residual_ys_std for fitted_y in  fitted_ys]
    
    return betas, (xs, fitted_ys, residual_ys, residual_ys_std, residual_ys_p_1std, residual_ys_m_1std)
   


def bootstrap_ifs_fwd(data):
    nc =  { c: int( c[4:].replace('_SWIT','').replace('Y','') ) for c in data.columns}
    data = data.rename(columns=nc)
    a = data.columns.tolist()[1:]
    b = data.columns.tolist()[:-1]
    fwd = {}
    fwd['{0}Y'.format(data.columns[0])] = data[data.columns[0]]
    for (ca,cb) in zip(a, b):
        term = ca - cb
        forward = cb
        cca = (1 + data[ca]/100).pow(ca)
        ccb = (1 + data[cb]/100).pow(cb)
        fwd['{0}Y{1}Y'.format(forward, term)] = np.log(cca/ccb)/term * 100
    fwd = pd.DataFrame(fwd)
    return fwd


def plot_main(ax, xs, ys, xlegend, ylegend, colors, cmaps, ticklabelsize=8, mirror_limit_x = False, mirror_limit_y = False):
    
    mapable = ax.scatter(xs, ys, c=colors, cmap=cmaps, marker='o', s = 6)
        
    ax.set_ylabel(ys.name, fontsize=8)
    ax.set_xlabel(xs.name, fontsize=8)
    ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
    ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
    
    for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    
    #for label,tick in zip(ax.get_xticklabels(), ax.get_xticks()):
    #    label.set_fontsize(ticklabelsize)
    #    if tick < 0:
    #        label.set_color("red")
    #    else:
    #        label.set_color("black")
    
    ymin, ymax = ax.get_ylim()
    xmin, xmax = ax.get_xlim()    
    
    ymax_abs = max([abs(ymin), abs(ymax)])
    xmax_abs = max([abs(xmin), abs(xmax)])
    
    xymax = max([abs(ymin), abs(ymax), abs(xmin), abs(xmax)])
    
    if mirror_limit_x:
        ax.set_xlim(-1 * xmax_abs, xmax_abs)
    if mirror_limit_y:
        ax.set_ylim(-1 * ymax_abs, ymax_abs)
    
    # Move left y-axis and bottim x-axis to centre, passing through (0,0)
    # ax.spines['left'].set_position('center')
    
    
    #ax.spines['bottom'].set_position('center')
    # Eliminate upper and right axes
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    # Show ticks in the left and lower axes only
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    
    ax.xaxis.set_major_locator(MultipleLocator(0.5))
    ax.yaxis.set_major_locator(MultipleLocator(1))
    ax.xaxis.grid(True, which='major')
    #ax.xaxis.grid(True, which='minor')
    ax.yaxis.grid(True, which='major')
    
    for label in ax.get_xticklabels(minor =False):
        label.set_fontsize(7)

    for label in ax.get_yticklabels(minor =False):
        label.set_fontsize(7)
        
    ax.set_ylabel(ylegend, fontsize=9)
    ax.yaxis.set_label_coords(0, 0.12)

    ax.set_xlabel(xlegend, fontsize=9)
    ax.xaxis.set_label_coords(0.85, 0.045)
    
    # end points
    start_x = xs.loc[xs.first_valid_index()]
    start_y = ys.loc[ys.first_valid_index()]
    start_date = xs.first_valid_index().strftime(r'%Y-%m-%d')
    
    ds1 = ax.scatter(start_x, start_y, c='#f5d9c8', s=90, alpha=0.5, label=start_date, edgecolors='b')
    
    last_x = xs.loc[xs.last_valid_index()]
    last_y = ys.loc[ys.last_valid_index()]
    last_date = xs.last_valid_index().strftime(r'%Y-%m-%d')

    ds2 = ax.scatter(last_x, last_y, c='darkred', s=90, alpha=1, label=last_date)
    
    
    ax.legend([ds1, ds2], [start_date, last_date], scatterpoints=1, fancybox=True, framealpha=0, loc=1, ncol=1, fontsize=9)
    
    return mapable


def plot_multi_lines(data, cols=None, spacing=.1, **kwargs):

    from pandas import plotting

    # Get default color style from pandas - can be changed to any other color list
    if cols is None: cols = data.columns
    if len(cols) == 0: return
    colors = getattr(getattr(plotting, '_matplotlib').style, '_get_standard_colors')(num_colors=len(cols))

    # First axis
    ax = data.loc[:, cols[0]].plot(label=cols[0], color=colors[0], **kwargs)
    ax.set_ylabel(ylabel=cols[0])
    lines, labels = ax.get_legend_handles_labels()

    for n in range(1, len(cols)):
        # Multiple y-axes
        ax_new = ax.twinx()
        ax_new.spines['right'].set_position(('axes', 1 + spacing * (n - 1)))
        data.loc[:, cols[n]].plot(ax=ax_new, label=cols[n], color=colors[n % len(colors)], **kwargs)
        ax_new.set_ylabel(ylabel=cols[n])

        # Proper legend position
        line, label = ax_new.get_legend_handles_labels()
        lines += line
        labels += label

    ax.legend(lines, labels, loc=0)
    return ax


def rolling_apply(df, window_size, func, min_window_size=None):
    if min_window_size is None:
        min_window_size = window_size
    result = {}
    for i in range(1, len(df)+1):
        sub_df = df.iloc[max(i - window_size, 0):i, :] #I edited here

        if len(sub_df) >= min_window_size:            
            idx = sub_df.index[-1]
            result[idx] = func(sub_df)
    return result


def get_flattened_cov(df):
    covmatx = df.cov()
    nodes = set([comb for comb in combinations(covmatx.columns.tolist() + covmatx.columns.tolist(), 2)])
    result = pd.Series({col + '_vs_' + row : covmatx[col][row]  for (col, row) in nodes})
    return result


def get_flattened_corr(df):
    covmatx = df.corr()
    nodes = set([comb for comb in combinations(covmatx.columns.tolist() + covmatx.columns.tolist(), 2)])
    result = pd.Series({col + '_vs_' + row : covmatx[col][row]  for (col, row) in nodes})
    return result


def func(df): 
    return pd.DataFrame({
                      'Now':df.iloc[-2:].mean(),
                      #'2020-09-11':df[(df.index >= '2020-09-08') & (df.index <= '2020-09-12')].mean(),
                      #'2020-09-04':df[(df.index >= '2020-09-02') & (df.index <= '2020-09-05')].mean(),
                      #'2020-08-12':df[(df.index >= '2020-08-10') & (df.index <= '2020-08-13')].mean(),
                      #'Pre speech':df.iloc[-3],
                      '7d avg':df.iloc[-7:].mean(), 
                      '7d avg 7d ago':df.iloc[-14:-7].mean(), 
                      '7d avg 14d ago':df.iloc[-21:-14].mean(), 
                      '7d avg 21d ago':df.iloc[-28:-21].mean(), 
                      #'7d avg 28d ago':df.iloc[-35:-28].mean(), 
                      #'avg last 3 years':df.iloc[-97:-90].mean(),
                      #'7d avg 180 day ago':df.iloc[-187:-180].mean(),
                      #'7d avg 360 day ago':df.iloc[-367:-360].mean(),
                      #'2010-2011 avg': df[(df.index > '2010-01-01') & (df.index < '2011-12-31')].mean(),
                      #'2010-Oct-2011-Jul': df[(df.index > '2010-10-01') & (df.index < '2011-07-01')].mean(),
                      #'2012-2013 avg': df[(df.index > '2012-01-01') & (df.index < '2013-12-31')].mean(),
                      #'2016-2019 avg': df[(df.index > '2016-01-01') & (df.index < '2019-12-31')].mean(),
                      #'2016-08': df[(df.index > '2016-08-01') & (df.index < '2016-09-01')].mean(),
                      #'2016-09': df[(df.index > '2016-09-01') & (df.index < '2016-10-01')].mean(),
                      #'2016-10': df[(df.index > '2016-10-01') & (df.index < '2016-11-01')].mean()
                      }) 


def vol_snapshot(df,n=20):
    
    rtn = df - df.shift(1)    
    
    return pd.DataFrame({
                      'Now':rtn.iloc[-n:].std(),
                      '1m ago':rtn.iloc[-2*n:-n].std(), 
                      '2m ago':rtn.iloc[-3*n:-2*n].std(), 
                      '3m ago':rtn.iloc[-4*n:-3*n].std()                      
                      }) * np.sqrt(252)




def create_combo_mr(data, elements = 2, resample_freq= 'W', 
                    include_normal_weighted_series=True, 
                    momentum_size=None, matrixoveride = None,
                    sample_range = None):
    mrs = {}
    data_sub = data
    combos = list(combinations(data.columns, elements))
    instrument = combos[0][0][3:]
    
    for combo in combos:
        _sub_data = data_sub[list(combo)].resample(resample_freq).last() if resample_freq is not None else data_sub[list(combo)]
        data_sub_pca = pca.PCA(_sub_data.dropna(how='all',axis=1).dropna(how='all',axis=0), 
                               rates_pca = True, momentum_size = momentum_size,matrixoveride = matrixoveride,
                               sample_range = sample_range)
        hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_pca.pc_hedge
        mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
        mrs[('_').join(combo) + '_PCW'] = (mr_obj, data_sub_pca)
        if include_normal_weighted_series:
            if len(_sub_data.columns) == 2:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] - _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s - %s' % (_sub_data.columns[0], _sub_data.columns[1])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)
            elif len(_sub_data.columns) == 3:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] + _sub_data[_sub_data.columns[2]] - 2 * _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s -2 x %s + %s' % (_sub_data.columns[0], _sub_data.columns[1], _sub_data.columns[2])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)

    return mrs








rundate = '2021-05-12'



# data_fut10_pos = pd.read_excel('C:/Dev/Models/QuantModels/#applications/market_watch/data/FUT10_POS_' + rundate + '.xlsx', sheet_name='Data Table')  
# data_fut10_pos['Date'] = pd.to_datetime(data_fut10_pos['Date'])
# data_fut10_pos = data_fut10_pos.set_index('Date').ffill()



data_usd_all = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + '_USD_historical.csv')  
data_usd_all['date'] = pd.to_datetime(data_usd_all['date'])
data_usd_all = data_usd_all.set_index('date').ffill()



data_econ = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + '_ECON_historical.csv')  
data_econ['date'] = pd.to_datetime(data_econ['date'])
data_econ = data_econ.set_index('date').ffill()


data = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + '_BEIRR_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()


data_swap = pd.read_csv('C:/Temp/test/market_watch/staging/'  + rundate +  '_SWAP_ALL_historical.csv')  
data_swap['date'] = pd.to_datetime(data_swap['date'])
data_swap = data_swap.set_index('date').ffill()

data_swap2 = pd.read_csv('C:/Temp/test/market_watch/staging/'  + rundate +  '_SWAP_ALL_V2_historical.csv')  
data_swap2['date'] = pd.to_datetime(data_swap2['date'])
data_swap2 = data_swap2.set_index('date').ffill()


# data_bcs = pd.read_csv(r'C:/Dev/Models/QuantModels/#test/BCS_2020-07-29.csv')  
# data_bcs['date'] = pd.to_datetime(data_bcs['date'])
# data_bcs = data_bcs.set_index('date').ffill()
# data_bcs_rtn = (data_bcs - data_bcs.shift(1))/100

data_ifs = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_SWIT_historical.csv')  
data_ifs['date'] = pd.to_datetime(data_ifs['date'])
data_ifs = data_ifs.set_index('date').ffill()


data_ccy_basis = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_BS_ALL_historical.csv')  
data_ccy_basis['date'] = pd.to_datetime(data_ccy_basis['date'])
data_ccy_basis = data_ccy_basis.set_index('date').ffill()
data_ccy_basis = data_ccy_basis[[c for c in data_ccy_basis.columns if '3M' not in c]]



data_sovcds = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_SOVCDS_historical.csv')  
data_sovcds['date'] = pd.to_datetime(data_sovcds['date'])
data_sovcds = data_sovcds.set_index('date').ffill() #.drop(['IND','GRC'],axis=1)



data_gov = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_GOV_historical.csv')  
data_gov['date'] = pd.to_datetime(data_gov['date'])
data_gov = data_gov.set_index('date').ffill()

data_vol = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_VOL_historical.csv')  
data_vol['date'] = pd.to_datetime(data_vol['date'])
data_vol = data_vol.set_index('date').ffill()


data_ivol_in_yields_daily = pd.DataFrame({c: convert_atmf_vol_to_yield(c.split('_')[1], data_vol[c]) for c in data_vol.columns})

data_rvol_swap_1m_daily = (data_swap - data_swap.shift(1)).rolling(20).std() * 100
data_rvol_swap_3m_daily = (data_swap - data_swap.shift(1)).rolling(60).std() * 100

data_rvol_swap2_1m_daily = (data_swap2 - data_swap2.shift(1)).rolling(20).std() * 100
data_rvol_swap2_3m_daily = (data_swap2 - data_swap2.shift(1)).rolling(60).std() * 100




ifs_c = list(set([c[:3] for c in data_ifs.columns]))
ifs_cc = {cnty:[c for c in data_ifs.columns if cnty in c] for cnty in ifs_c}

data_fwd_ifs = {c: bootstrap_ifs_fwd(data_ifs[cc]) for (c,cc) in ifs_cc.items()}


##################
# needs to be changed
##################

US_cols =  [c for c in data.columns if 'US' in c]
US_cols.sort()
data_usd = data[US_cols].dropna()



data_usd['USD_NM_02Y'] = data_usd['USD_BE_02Y'] + data_usd['USD_RR_02Y'] 
data_usd['USD_NM_05Y'] = data_usd['USD_BE_05Y'] + data_usd['USD_RR_05Y']
data_usd['USD_NM_10Y'] = data_usd['USD_BE_10Y'] + data_usd['USD_RR_10Y']
data_usd['USD_NM_30Y'] = data_usd['USD_BE_30Y'] + data_usd['USD_RR_30Y']



data_usd['USD_NM_210'] = data_usd['USD_NM_10Y'] - data_usd['USD_NM_02Y']
data_usd['USD_NM_510'] = data_usd['USD_NM_10Y'] - data_usd['USD_NM_05Y']
data_usd['USD_NM_530'] = data_usd['USD_NM_30Y'] - data_usd['USD_NM_05Y']
data_usd['USD_NM_1030'] = data_usd['USD_NM_30Y'] - data_usd['USD_NM_10Y']
data_usd['USD_NM_020510'] = data_usd['USD_NM_10Y'] - 2 * data_usd['USD_NM_05Y'] + data_usd['USD_NM_02Y']
data_usd['USD_NM_051030'] = data_usd['USD_NM_30Y'] - 2 * data_usd['USD_NM_10Y'] + data_usd['USD_NM_05Y']


data_usd['USD_RR_210'] = data_usd['USD_RR_10Y'] - data_usd['USD_RR_02Y']
data_usd['USD_RR_510'] = data_usd['USD_RR_10Y'] - data_usd['USD_RR_05Y']
data_usd['USD_RR_530'] = data_usd['USD_RR_30Y'] - data_usd['USD_RR_05Y']
data_usd['USD_RR_230'] = data_usd['USD_RR_30Y'] - data_usd['USD_RR_02Y']
data_usd['USD_RR_1030'] = data_usd['USD_RR_30Y'] - data_usd['USD_RR_10Y']
data_usd['USD_RR_020510'] = data_usd['USD_RR_10Y'] - 2 * data_usd['USD_RR_05Y'] + data_usd['USD_RR_02Y']
data_usd['USD_RR_051030'] = data_usd['USD_RR_30Y'] - 2 * data_usd['USD_RR_10Y'] + data_usd['USD_RR_05Y']

data_usd['USD_BE_210'] = data_usd['USD_BE_10Y'] - data_usd['USD_BE_02Y']
data_usd['USD_BE_510'] = data_usd['USD_BE_10Y'] - data_usd['USD_BE_05Y']
data_usd['USD_BE_530'] = data_usd['USD_BE_30Y'] - data_usd['USD_BE_05Y']
data_usd['USD_BE_230'] = data_usd['USD_BE_30Y'] - data_usd['USD_BE_02Y']
data_usd['USD_BE_1030'] = data_usd['USD_BE_30Y'] - data_usd['USD_BE_10Y']
data_usd['USD_BE_020510'] = data_usd['USD_BE_10Y'] - 2 * data_usd['USD_BE_05Y'] + data_usd['USD_BE_02Y']
data_usd['USD_BE_051030'] = data_usd['USD_BE_30Y'] - 2 * data_usd['USD_BE_10Y'] + data_usd['USD_BE_05Y']

data_usd['USD_BE2NM30'] = data_usd['USD_NM_30Y'] - data_usd['USD_BE_02Y']

data_usd_rtn = data_usd - data_usd.shift(1)


##################
# needs to be changed
##################




#test11 ivol 

#### AUD
aud_ivol_cols = [c for c in data_ivol_in_yields_daily.columns if 'AUD' in c and ('_3M_' in c  or '_6M_' in c or '_9M_' in c  or '_1Y_' in c  or '_2Y_' in c or '_3Y_' in c)] 
aud_ivol_in_yields_daily = data_ivol_in_yields_daily[aud_ivol_cols].dropna(how='all', axis=1).ffill()
pca.PCA(aud_ivol_in_yields_daily.iloc[-250:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
####



usd_ivol_cols = [c for c in data_ivol_in_yields_daily.columns if 'USD' in c and ('_3M_' in c  or '_6m_' in c or '_9M_' in c  or '_1Y_' in c  or '_2Y_' in c or '_3Y_' in c)] 
usd_ivol_in_yields_daily = data_ivol_in_yields_daily[usd_ivol_cols].dropna(how='all', axis=1).ffill()
pca.PCA(usd_ivol_in_yields_daily.iloc[-2500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)






#test13 USD only

# swap
# ifs


usd_spot_swap_test13 = data_swap[['USD_0Y_1Y',
                         'USD_0Y_2Y',
                         'USD_0Y_3Y',
                         #'USD_0Y_4Y',
                         'USD_0Y_5Y',
                         #'USD_0Y_6Y',
                         #'USD_0Y_7Y',
                         #'USD_0Y_8Y',
                         #'USD_0Y_9Y',
                         'USD_0Y_10Y',
                         #'USD_0Y_20Y',
                         'USD_0Y_30Y']].rename(lambda c: c.replace("_0Y_","_"), axis="columns")


usd_spot_gov_test13 = data_gov[[ 'USD_0Y_1Y',
 'USD_0Y_2Y',
 'USD_0Y_3Y',
 'USD_0Y_5Y',
 #'USD_0Y_7Y',
 'USD_0Y_10Y',
 #'USD_0Y_20Y',
 'USD_0Y_30Y']].rename(lambda c: c.replace("_0Y_","_"), axis="columns")




usd_spot_ifs_test13 = data_ifs[['USD_1Y_SWIT',
                         'USD_2Y_SWIT',
                         'USD_3Y_SWIT',
                         #'USD_4Y_SWIT',
                         'USD_5Y_SWIT',
                         #'USD_6Y_SWIT',
                         #'USD_7Y_SWIT',
                         #'USD_8Y_SWIT',
                         #'USD_9Y_SWIT',
                         'USD_10Y_SWIT',
                         'USD_20Y_SWIT',
                         'USD_30Y_SWIT']]




# eur_spot_ifs_test13 = data_ifs[['EUR_1Y_SWIT',
#                          'EUR_2Y_SWIT',
#                          'EUR_3Y_SWIT',
#                          #'EUR_4Y_SWIT',
#                          'EUR_5Y_SWIT',
#                          #'EUR_6Y_SWIT',
#                          #'EUR_7Y_SWIT',
#                          #'EUR_8Y_SWIT',
#                          #'EUR_9Y_SWIT',
#                          'EUR_10Y_SWIT',
#                          'EUR_20Y_SWIT',
#                          'EUR_30Y_SWIT']]




usd_data_rvol_daily_1m_test13 = data_rvol_swap_1m_daily[[
 #'USD_0Y_3M',
 'USD_0Y_1Y',
 'USD_0Y_2Y',
 'USD_0Y_3Y',
 #'USD_0Y_4Y',
 'USD_0Y_5Y',
 #'USD_0Y_6Y',
 #'USD_0Y_7Y',
 #'USD_0Y_8Y',
 #'USD_0Y_9Y',
 'USD_0Y_10Y',
 'USD_0Y_20Y',
 'USD_0Y_30Y']].rename(lambda c: c.replace('_0Y',''), axis=1) #* np.sqrt(256)



usd_data_rvol_daily_3m_test13 = data_rvol_swap_3m_daily[[
 #'USD_0Y_3M',
 'USD_0Y_1Y',
 'USD_0Y_2Y',
 'USD_0Y_3Y',
 #'USD_0Y_4Y',
 'USD_0Y_5Y',
 #'USD_0Y_6Y',
 #'USD_0Y_7Y',
 #'USD_0Y_8Y',
 #'USD_0Y_9Y',
 'USD_0Y_10Y',
 'USD_0Y_20Y',
 'USD_0Y_30Y']].rename(lambda c: c.replace('_0Y',''), axis=1)  #* np.sqrt(256)




usd_data_ivol_1m_in_yields_daily_test13 = data_ivol_in_yields_daily[[
 'USD_1M_1Y',
 'USD_1M_2Y',
 'USD_1M_3Y',
 'USD_1M_5Y',
 'USD_1M_10Y',
 'USD_1M_20Y',
 'USD_1M_30Y']].rename(lambda c: c.replace('_1M',''), axis=1)


usd_data_ivol_3m_in_yields_daily_test13 = data_ivol_in_yields_daily[[
 'USD_3M_1Y',
 'USD_3M_2Y',
 'USD_3M_3Y',
 'USD_3M_5Y',
 'USD_3M_10Y',
 'USD_3M_20Y',
 'USD_3M_30Y']].rename(lambda c: c.replace('_3M',''), axis=1)



usd_data_ivol_1y_in_yields_daily_test13 = data_ivol_in_yields_daily[[
 'USD_1Y_1Y',
 'USD_1Y_2Y',
 'USD_1Y_3Y',
 'USD_1Y_5Y',
 'USD_1Y_10Y',
 'USD_1Y_20Y',
 'USD_1Y_30Y']].rename(lambda c: c.replace('_1Y_',''), axis=1)




usd_data_ivol_rvol_1m_test13 = usd_data_ivol_1m_in_yields_daily_test13 / (usd_data_rvol_daily_1m_test13 * np.sqrt(256))

usd_data_ivol_rvol_3m_test13 = usd_data_ivol_3m_in_yields_daily_test13 / (usd_data_rvol_daily_3m_test13 * np.sqrt(256))




#inflation swap date seems to be always 1day lag. check

# use swap as the nominal rate
usd_spot_rr_test13 = usd_spot_swap_test13 - usd_spot_ifs_test13.rename(lambda c: c.replace('_SWIT',''), axis=1).reindex(usd_spot_swap_test13.index).ffill()


# use gov as the nominal rate
#usd_spot_rr_test13 = usd_spot_gov_test13 - usd_spot_ifs_test13.rename(lambda c: c.replace('_SWIT',''), axis=1).reindex(usd_spot_gov_test13.index).ffill()


#
usd_spot_ss_test13 = usd_spot_swap_test13.reindex(usd_spot_gov_test13.index).ffill() - usd_spot_gov_test13 


usd_fwd_swap_test13 = bootstrap_ifs_fwd(usd_spot_swap_test13)
usd_fwd_gov_test13 = bootstrap_ifs_fwd(usd_spot_gov_test13)
usd_fwd_ifs_test13 =  bootstrap_ifs_fwd(usd_spot_ifs_test13)
usd_fwd_rr_test13 =  bootstrap_ifs_fwd(usd_spot_rr_test13)
usd_fwd_ss_test13 =  usd_fwd_swap_test13.reindex(usd_fwd_gov_test13.index).ffill() - usd_fwd_gov_test13


#eur_fwd_ifs_test13 =  bootstrap_ifs_fwd(eur_spot_ifs_test13)


### Output charts

#ivol/rvol

f0 = plt.figure(figsize = (15, 10))
gs0 = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
f0ax0 = plt.subplot(gs0[0])
f0ax1 = plt.subplot(gs0[1])
f0ax2 = plt.subplot(gs0[2])
f0ax3 = plt.subplot(gs0[3])


func(usd_data_ivol_rvol_1m_test13).rename(lambda c: c.split('_')[1]).plot(ax=f0ax0)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f0ax0.set_title('ivol/1 stdev ratio swap curves')


usd_data_ivol_rvol_1m_test13.iloc[-100:].plot(ax=f0ax1)
f0ax1.set_title('ivol/1 stdev ratio swap curves')

usd_data_ivol_rvol_1m_test13.iloc[-2000:-1000].plot(ax=f0ax2)
f0ax2.set_title('ivol/1 stdev ratio swap curves')

usd_data_ivol_rvol_1m_test13.iloc[-3000:-2000].plot(ax=f0ax3)
f0ax3.set_title('ivol/1 stdev ratio swap curves')


pca.PCA(usd_data_ivol_rvol_1m_test13.iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_data_ivol_rvol_1m_test13.iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_data_ivol_rvol_1m_test13.iloc[-3000:], rates_pca = True, momentum_size = None).plot_loadings(n=3)

usd_data_ivol_rvol_1m_long_cycle_pca_test13 = pca.PCA(usd_data_ivol_rvol_1m_test13[usd_data_ivol_rvol_1m_test13.index > '2008-01-01'], rates_pca = True, momentum_size = None)

#pick pc1 and pc2 as the input for market regime analysis

usd_rvol_1m_long_cycle_pcs_test13 = usd_data_ivol_rvol_1m_long_cycle_pca_test13.pcs[['PC1', 'PC2']].rename(lambda x: "vol_ratio_" + x, axis=1)


win = 40
data_rtns_d_1m_rolling_corrs = pd.DataFrame(rolling_apply(usd_data_ivol_rvol_1m_long_cycle_pca_test13.pcs[['PC1', 'PC2']], win, get_flattened_corr)).transpose()
regime_line1_test13 = (data_rtns_d_1m_rolling_corrs['PC1_vs_PC2'] * np.sign(usd_data_ivol_rvol_1m_long_cycle_pca_test13.pcs['PC1'].ewm(win).mean())).ewm(win).mean()


win = 20
data_rtns_d_1m_rolling_corrs = pd.DataFrame(rolling_apply(usd_data_ivol_rvol_1m_long_cycle_pca_test13.pcs[['PC1', 'PC2']], win, get_flattened_corr)).transpose()
regime_line2_test13 = (data_rtns_d_1m_rolling_corrs['PC1_vs_PC2'] * np.sign(usd_data_ivol_rvol_1m_long_cycle_pca_test13.pcs['PC1'].ewm(win).mean())).ewm(win).mean()

regime_test13 = pd.DataFrame({'1M implied': regime_line2_test13, '3M implied': regime_line1_test13})

regime_test13.plot(title='US interest rate trading regime')



#inflation USD

if(3>2):
    
    def func(df): 
        return pd.DataFrame({
                          'Now':df.iloc[-2:].mean(),                      
                          #'7d avg':df.iloc[-7:].mean(), 
                          #'7d avg 7d ago':df.iloc[-14:-7].mean(), 
                          '7d avg 14d ago':df.iloc[-21:-14].mean(), 
                          '7d avg 28d ago':df.iloc[-35:-28].mean(),
                          '7d avg 60d ago':df.iloc[-67:-60].mean(),
                          #'2020 April avg': df[(df.index > '2020-03-31') & (df.index < '2020-05-01')].mean(),
                          #'2020 May avg': df[(df.index > '2020-04-30') & (df.index < '2020-06-01')].mean(),
                          #'2020 June avg': df[(df.index > '2020-05-31') & (df.index < '2020-07-01')].mean(),
                          #'2020 July avg': df[(df.index > '2020-06-30') & (df.index < '2020-08-01')].mean(),
                          #'2010-Oct-2011-Jul': df[(df.index > '2010-10-01') & (df.index < '2011-07-01')].mean(),
                          #'2012-2013 avg': df[(df.index > '2012-01-01') & (df.index < '2013-12-31')].mean(),
                          #'2016-2019 avg': df[(df.index > '2016-01-01') & (df.index < '2019-12-31')].mean(),
                          #'2016-08': df[(df.index > '2016-08-01') & (df.index < '2016-09-01')].mean(),
                          #'2016-09': df[(df.index > '2016-09-01') & (df.index < '2016-10-01')].mean(),
                          #'2016-10': df[(df.index > '2016-10-01') & (df.index < '2016-11-01')].mean()
                          }) 


f1 = plt.figure(figsize = (15, 10))
gs = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
f1ax0 = plt.subplot(gs[0])
f1ax1 = plt.subplot(gs[1])
f1ax2 = plt.subplot(gs[2])
f1ax3 = plt.subplot(gs[3])


func(usd_spot_ifs_test13).rename(lambda c: c.split('_')[1]).plot(ax=f1ax0)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f1ax0.set_title('Spot inflation swap curves')

func(usd_fwd_ifs_test13).plot(ax=f1ax1)
#plt.xticks(np.arange(len(func(usd_fwd_ifs_test13).index)), func(usd_fwd_ifs_test13).index.tolist())
f1ax1.set_title('Forward inflation swap curves')

vol_snapshot(usd_spot_ifs_test13).rename(lambda c: c.split('_')[1]).plot(ax=f1ax2)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f1ax2.set_title('Spot inflation swap curves vol')

vol_snapshot(usd_fwd_ifs_test13).plot(ax=f1ax3)
#plt.xticks(np.arange(len(func(usd_fwd_ifs_test13).index)), func(usd_fwd_ifs_test13).index.tolist())
f1ax3.set_title('Forward inflation swap curves vol')

pca.PCA(usd_spot_ifs_test13.iloc[-2500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_fwd_ifs_test13.iloc[-2500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)

pca.PCA(usd_fwd_ifs_test13.iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_spot_ifs_test13.iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)

usd_spot_ifs_long_cycle_pca_test13 = pca.PCA(usd_spot_ifs_test13[usd_spot_ifs_test13.index > '2008-01-01'], rates_pca = True, momentum_size = None)


usd_spot_ifs_long_cycle_pcs_test13 = usd_spot_ifs_long_cycle_pca_test13.pcs[['PC1', 'PC2']].rename(lambda x: "ifs_" + x, axis=1)





#### correlation chart ##########

win = 100
data_rtns_d_1m_rolling_corrs = pd.DataFrame(rolling_apply(usd_spot_ifs_long_cycle_pca_test13.pcs[['PC1', 'PC2']], win, get_flattened_corr)).transpose()
regime_line1_test13 = (data_rtns_d_1m_rolling_corrs['PC1_vs_PC2'] * np.sign(usd_spot_ifs_long_cycle_pca_test13.pcs['PC1'].ewm(win).mean())).ewm(win).mean()


win = 20
data_rtns_d_1m_rolling_corrs = pd.DataFrame(rolling_apply(usd_spot_ifs_long_cycle_pca_test13.pcs[['PC1', 'PC2']], win, get_flattened_corr)).transpose()
regime_line2_test13 = (data_rtns_d_1m_rolling_corrs['PC1_vs_PC2'] * np.sign(usd_spot_ifs_long_cycle_pca_test13.pcs['PC1'].ewm(win).mean())).ewm(win).mean()

regime_test13 = pd.DataFrame({'1M implied': regime_line2_test13, '3M implied': regime_line1_test13})

regime_test13.plot(title='US interest rate trading regime')





############# clustering



def reformat_labelled_data(labelled_data):
    labels = labelled_data[labelled_data.columns[1]].unique()
    labels.sort()
    output = {}
    for l in labels:
        output["{0}_{1}".format(labelled_data.columns[1], l)] = labelled_data[labelled_data.columns[0]][labelled_data[labelled_data.columns[1]] == l].reindex(labelled_data.index)
    return pd.DataFrame(output)



regime_training_date = '2015-12-31'
usd_data_ivol_rvol_1m_long_cycle_pca_test13 = pca.PCA(usd_data_ivol_rvol_1m_test13[usd_data_ivol_rvol_1m_test13.index > '2008-01-01'], rates_pca = True, momentum_size = None)
usd_spot_ifs_long_cycle_pca_test13 = pca.PCA(usd_spot_ifs_test13[usd_spot_ifs_test13.index > '2008-01-01'], rates_pca = True, momentum_size = None)
usd_data_ivol_1m_in_yields_daily_pca_test13 = pca.PCA(usd_data_ivol_1m_in_yields_daily_test13[usd_data_ivol_1m_in_yields_daily_test13.index > '2008-01-01'], rates_pca = True, momentum_size = None)
usd_data_ivol_3m_in_yields_daily_pca_test13 = pca.PCA(usd_data_ivol_3m_in_yields_daily_test13[usd_data_ivol_3m_in_yields_daily_test13.index > '2008-01-01'], rates_pca = True, momentum_size = None)
usd_data_ivol_1y_in_yields_daily_pca_test13 = pca.PCA(usd_data_ivol_1y_in_yields_daily_test13[usd_data_ivol_1y_in_yields_daily_test13.index > '2008-01-01'], rates_pca = True, momentum_size = None)
usd_spot_gov_pca_test13 = pca.PCA(usd_spot_gov_test13.drop(['USD_1Y'], axis=1)[usd_spot_gov_test13.index > '2008-01-01'], rates_pca = True, momentum_size = None)


# usd_data_ivol_rvol_1m_long_cycle_pca_test13.plot_loadings(n=5)
# usd_spot_ifs_long_cycle_pca_test13.plot_loadings(n=5)
# usd_data_ivol_1m_in_yields_daily_pca_test13.plot_loadings(n=5)
# usd_data_ivol_3m_in_yields_daily_pca_test13.plot_loadings(n=5)
# usd_data_ivol_1y_in_yields_daily_pca_test13.plot_loadings(n=5)
# usd_spot_gov_pca_test13.plot_loadings(n=5)


#(usd_spot_ifs_long_cycle_pca_test13.data["USD_5Y_SWIT"] * 2 - usd_spot_ifs_long_cycle_pca_test13.data["USD_3Y_SWIT"] - usd_spot_ifs_long_cycle_pca_test13.data["USD_3Y_SWIT"]).plot()

#ivol/stdev ratio pc1 - level. it is inverted
#ivol/stdev ratio pc2 - curve. 1y and 2y are facing down while the rest are up
#ifs pc1 - level it is also inverted
#ifs pc2 - curve. 1y, 2y and 3y are facing down while the rest are up.


combined_pcs_test13 =  pd.concat([usd_rvol_1m_long_cycle_pcs_test13, usd_spot_ifs_long_cycle_pcs_test13], axis=1)

combined_pcs_test13 = pd.concat([usd_spot_ifs_long_cycle_pca_test13.pcs[['PC1','PC2']], 
                                 usd_data_ivol_rvol_1m_long_cycle_pca_test13.pcs[['PC1','PC2']],
                                 #usd_data_ivol_1y_in_yields_daily_pca_test13.pcs[['PC2']],
                                 usd_data_ivol_1m_in_yields_daily_pca_test13.pcs[['PC2']]/2,
                                 #usd_spot_gov_pca_test13.pcs[['PC3']],
                                 ], axis=1).ffill()



combined_pcs_test13_new #= combined_pcs_test13

combined_pcs_test13_new[combined_pcs_test13_new.index > '2021-02-10']

combined_pcs_test13_updated = pd.concat([combined_pcs_test13, 
                                         combined_pcs_test13_new[combined_pcs_test13_new.index > '2021-02-10'] ])


combined_pc_linkage_matrix = linkage(combined_pcs_test13_updated, method='ward', optimal_ordering=False)



# loading_clusters_fig = plt.figure(figsize = (45, 15))
# plt.title('combined pcs clustering')
# plt.xlabel('Dates')
# plt.ylabel('Independence')

# dendrogram(
#     combined_pc_linkage_matrix,
#     leaf_rotation=45.,  # rotates the x axis labels
#     leaf_font_size=8.,  # font size for the x axis labels
#     labels = combined_pcs_test13.index.strftime('%Y-%m'),
#     #color_threshold = 35
# )

# # 4 groups
# plt.axhline(y=36, c='k')
# cluster_1 =  fcluster(combined_pc_linkage_matrix, 35, criterion='distance')
# cluster_1 = pd.Series(data=cluster_1, index=usd_data_ivol_rvol_1m_long_cycle_pca_test13.data.index)

# # 7 groups
# plt.axhline(y=34, c='k')
# cluster_2 =  fcluster(combined_pc_linkage_matrix, 30, criterion='distance')
# cluster_2 = pd.Series(data=cluster_2, index=usd_data_ivol_rvol_1m_long_cycle_pca_test13.data.index)



cluster_3 = pd.Series(data=fcluster(combined_pc_linkage_matrix, 4, criterion='maxclust'), index=combined_pcs_test13_updated.index)

c = cluster_3
labelled_us10 =pd.DataFrame({"US10":usd_spot_gov_test13['USD_10Y'].reindex(c.index),"Regime":c})
refrmated_us10 = reformat_labelled_data(labelled_us10)
refrmated_us10.plot(title='US10')



labelled_us2s10 =pd.DataFrame({"US10S30":(usd_spot_gov_test13['USD_30Y'] - usd_spot_gov_test13['USD_10Y']).reindex(c.index),"Regime":c})
refrmated_us2s10 = reformat_labelled_data(labelled_us2s10)
refrmated_us2s10.plot(title='US10S30')





loading_clusters_fig = plt.figure(figsize = (45, 15))
plt.title('combined pcs clustering')
plt.xlabel('Dates')
plt.ylabel('Independence')

dendrogram(
    combined_pc_linkage_matrix,
    leaf_rotation=45.,  # rotates the x axis labels
    leaf_font_size=8.,  # font size for the x axis labels
    labels = cluster_1,
    color_threshold = 30
)










# #inflation eur

# if(2>1):
    
#     def func(df): 
#         return pd.DataFrame({
#                           'Now':df.iloc[-2:].mean(),                      
#                           #'7d avg':df.iloc[-7:].mean(), 
#                           #'7d avg 7d ago':df.iloc[-14:-7].mean(), 
#                           #'7d avg 14d ago':df.iloc[-21:-14].mean(), 
#                           #'7d avg 21d ago':df.iloc[-28:-21].mean(),                      
#                           '2020 April avg': df[(df.index > '2020-03-31') & (df.index < '2020-05-01')].mean(),
#                           '2020 May avg': df[(df.index > '2020-04-30') & (df.index < '2020-06-01')].mean(),
#                           '2020 June avg': df[(df.index > '2020-05-31') & (df.index < '2020-07-01')].mean(),
#                           '2020 July avg': df[(df.index > '2020-06-30') & (df.index < '2020-08-01')].mean(),
#                           #'2010-Oct-2011-Jul': df[(df.index > '2010-10-01') & (df.index < '2011-07-01')].mean(),
#                           #'2012-2013 avg': df[(df.index > '2012-01-01') & (df.index < '2013-12-31')].mean(),
#                           #'2016-2019 avg': df[(df.index > '2016-01-01') & (df.index < '2019-12-31')].mean(),
#                           #'2016-08': df[(df.index > '2016-08-01') & (df.index < '2016-09-01')].mean(),
#                           #'2016-09': df[(df.index > '2016-09-01') & (df.index < '2016-10-01')].mean(),
#                           #'2016-10': df[(df.index > '2016-10-01') & (df.index < '2016-11-01')].mean()
#                           }) 


# f1 = plt.figure(figsize = (15, 10))
# gs = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
# f1ax0 = plt.subplot(gs[0])
# f1ax1 = plt.subplot(gs[1])
# f1ax2 = plt.subplot(gs[2])
# f1ax3 = plt.subplot(gs[3])


# func(eur_spot_ifs_test13).rename(lambda c: c.split('_')[1]).plot(ax=f1ax0)
# #plt.xticks(np.arange(len(func(eur_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(eur_spot_ifs_test13).index])
# f1ax0.set_title('Spot inflation swap curves')

# func(eur_fwd_ifs_test13).plot(ax=f1ax1)
# #plt.xticks(np.arange(len(func(eur_fwd_ifs_test13).index)), func(eur_fwd_ifs_test13).index.tolist())
# f1ax1.set_title('Forward inflation swap curves')

# vol_snapshot(eur_spot_ifs_test13).rename(lambda c: c.split('_')[1]).plot(ax=f1ax2)
# #plt.xticks(np.arange(len(func(eur_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(eur_spot_ifs_test13).index])
# f1ax2.set_title('Spot inflation swap curves vol')

# vol_snapshot(eur_fwd_ifs_test13).plot(ax=f1ax3)
# #plt.xticks(np.arange(len(func(eur_fwd_ifs_test13).index)), func(eur_fwd_ifs_test13).index.tolist())
# f1ax3.set_title('Forward inflation swap curves vol')

# pca.PCA(eur_spot_ifs_test13.iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
# pca.PCA(eur_spot_ifs_test13.iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)

# pca.PCA(eur_fwd_ifs_test13.iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
# pca.PCA(eur_fwd_ifs_test13.iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)






#swap

f2 = plt.figure(figsize = (15, 10))
gs2 = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
f2ax0 = plt.subplot(gs2[0])
f2ax1 = plt.subplot(gs2[1])
f2ax2 = plt.subplot(gs2[2])
f2ax3 = plt.subplot(gs2[3])

func(usd_spot_swap_test13).rename(lambda c: c.split('_')[1]).plot(ax=f2ax0)
#plt.xticks(np.arange(len(func(usd_spot_swap_test13).index)), [c.split('_')[1] for c in func(usd_spot_swap_test13).index])
f2ax0.set_title('Spot nominal swap curves')

func(usd_fwd_swap_test13).plot(ax=f2ax1)
#plt.xticks(np.arange(len(func(usd_fwd_swap_test13).index)), func(usd_fwd_swap_test13).index.tolist())
f2ax1.set_title('Forward nominal swap curves')


vol_snapshot(usd_spot_swap_test13).rename(lambda c: c.split('_')[1]).plot(ax=f2ax2)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f2ax2.set_title('Spot swap curves vol')

vol_snapshot(usd_fwd_swap_test13).plot(ax=f2ax3)
#plt.xticks(np.arange(len(func(usd_fwd_ifs_test13).index)), func(usd_fwd_ifs_test13).index.tolist())
f2ax3.set_title('Forward swap curves vol')


pca.PCA(usd_spot_swap_test13.iloc[-20:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_spot_swap_test13.iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)

pca.PCA(usd_fwd_swap_test13.iloc[-20:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_fwd_swap_test13.iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)


#gov

f3 = plt.figure(figsize = (15, 10))
gs3 = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
f3ax0 = plt.subplot(gs3[0])
f3ax1 = plt.subplot(gs3[1])
f3ax2 = plt.subplot(gs3[2])
f3ax3 = plt.subplot(gs3[3])

func(usd_spot_gov_test13).rename(lambda c: c.split('_')[1]).plot(ax=f3ax0)
#plt.xticks(np.arange(len(func(usd_spot_swap_test13).index)), [c.split('_')[1] for c in func(usd_spot_swap_test13).index])
f3ax0.set_title('Spot nominal gov curves')

func(usd_fwd_gov_test13).plot(ax=f3ax1)
#plt.xticks(np.arange(len(func(usd_fwd_swap_test13).index)), func(usd_fwd_swap_test13).index.tolist())
f3ax1.set_title('Forward nominal gov curves')


vol_snapshot(usd_spot_gov_test13).rename(lambda c: c.split('_')[1]).plot(ax=f3ax2)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f3ax2.set_title('Spot gov curves vol')

vol_snapshot(usd_fwd_gov_test13).plot(ax=f3ax3)
#plt.xticks(np.arange(len(func(usd_fwd_ifs_test13).index)), func(usd_fwd_ifs_test13).index.tolist())
f3ax3.set_title('Forward gov curves vol')



pca.PCA(usd_spot_gov_test13.drop(['USD_20Y'],axis=1).iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_spot_gov_test13.drop(['USD_20Y'],axis=1).iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)


pca.PCA(bootstrap_ifs_fwd(usd_spot_gov_test13.drop(['USD_20Y'],axis=1)).iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(bootstrap_ifs_fwd(usd_spot_gov_test13.drop(['USD_20Y'],axis=1)).iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)



#RR


f4 = plt.figure(figsize = (15, 10))
gs4 = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
f4ax0 = plt.subplot(gs4[0])
f4ax1 = plt.subplot(gs4[1])
f4ax2 = plt.subplot(gs4[2])
f4ax3 = plt.subplot(gs4[3])

func(usd_spot_rr_test13).rename(lambda c: c.split('_')[1]).plot(ax=f4ax0)
#plt.xticks(np.arange(len(func(usd_spot_swap_test13).index)), [c.split('_')[1] for c in func(usd_spot_swap_test13).index])
f4ax0.set_title('Spot nominal swap rr curves')

func(usd_fwd_rr_test13).plot(ax=f4ax1)
#plt.xticks(np.arange(len(func(usd_fwd_swap_test13).index)), func(usd_fwd_swap_test13).index.tolist())
f4ax1.set_title('Forward nominal swap rr curves')


vol_snapshot(usd_spot_rr_test13).rename(lambda c: c.split('_')[1]).plot(ax=f4ax2)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f4ax2.set_title('Spot swap rr curve vol')

vol_snapshot(usd_fwd_rr_test13).plot(ax=f4ax3)
#plt.xticks(np.arange(len(func(usd_fwd_ifs_test13).index)), func(usd_fwd_ifs_test13).index.tolist())
f4ax3.set_title('Forward swap rr curves vol')

pca.PCA(usd_spot_rr_test13.iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_spot_rr_test13.iloc[-2500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)

pca.PCA(usd_fwd_rr_test13.iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_fwd_rr_test13.iloc[-2500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)



#SS


f5 = plt.figure(figsize = (15, 10))
gs5 = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
f5ax0 = plt.subplot(gs5[0])
f5ax1 = plt.subplot(gs5[1])
f5ax2 = plt.subplot(gs5[2])
f5ax3 = plt.subplot(gs5[3])

func(usd_spot_ss_test13).rename(lambda c: c.split('_')[1]).plot(ax=f5ax0)
#plt.xticks(np.arange(len(func(usd_spot_swap_test13).index)), [c.split('_')[1] for c in func(usd_spot_swap_test13).index])
f4ax0.set_title('Spot ss curves')

func(usd_fwd_ss_test13).plot(ax=f5ax1)
#plt.xticks(np.arange(len(func(usd_fwd_swap_test13).index)), func(usd_fwd_swap_test13).index.tolist())
f5ax1.set_title('Forward ss curves')


vol_snapshot(usd_spot_ss_test13).rename(lambda c: c.split('_')[1]).plot(ax=f5ax2)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f5ax2.set_title('Spot ss curve vol')

vol_snapshot(usd_fwd_ss_test13).plot(ax=f5ax3)
#plt.xticks(np.arange(len(func(usd_fwd_ifs_test13).index)), func(usd_fwd_ifs_test13).index.tolist())
f5ax3.set_title('Forward ss curves vol')



pca.PCA(usd_spot_ss_test13.drop(['USD_20Y'],axis=1).iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_spot_ss_test13.drop(['USD_20Y'],axis=1).iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)


pca.PCA(bootstrap_ifs_fwd(usd_spot_ss_test13.drop(['USD_20Y'],axis=1)).iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(bootstrap_ifs_fwd(usd_spot_ss_test13.drop(['USD_20Y'],axis=1)).iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)






#test14

aud_spot_swap_test14 = data_swap[['AUD_0Y_1Y',
                         'AUD_0Y_2Y',
                         'AUD_0Y_3Y',
                         #'AUD_0Y_4Y',
                         'AUD_0Y_5Y',
                         #'AUD_0Y_6Y',
                         #'AUD_0Y_7Y',
                         #'AUD_0Y_8Y',
                         #'AUD_0Y_9Y',
                         'AUD_0Y_10Y',
                         'AUD_0Y_20Y',
                         'AUD_0Y_30Y']].rename(lambda c: c.replace("_0Y_","_"), axis="columns")


aud_spot_gov_test14 = data_gov[[ 'AUD_0Y_1Y',
 'AUD_0Y_2Y',
 'AUD_0Y_3Y',
 'AUD_0Y_5Y',
 #'USD_0Y_7Y',
 'AUD_0Y_10Y',
 'AUD_0Y_20Y',
 'AUD_0Y_30Y']].rename(lambda c: c.replace("_0Y_","_"), axis="columns")





aud_data_rvol_daily_1m_test14 = data_rvol_swap_1m_daily[[
 #'USD_0Y_3M',
 'AUD_0Y_1Y',
 'AUD_0Y_2Y',
 #'AUD_0Y_3Y',
 #'AUD_0Y_4Y',
 'AUD_0Y_5Y',
 #'AUD_0Y_6Y',
 #'AUD_0Y_7Y',
 #'AUD_0Y_8Y',
 #'AUD_0Y_9Y',
 'AUD_0Y_10Y',
 #'AUD_0Y_20Y',
 'AUD_0Y_30Y']].rename(lambda c: c.replace('_0Y',''), axis=1)


aud_data_ivol_1m_in_yields_daily_test14 = data_ivol_in_yields_daily[['AUD_1M_1Y',
 'AUD_1M_2Y',
 'AUD_1M_5Y',
 'AUD_1M_10Y',
 'AUD_1M_30Y']].rename(lambda c: c.replace('_1M',''), axis=1)


aud_data_ivol_rvol_1m_test14 = aud_data_ivol_1m_in_yields_daily_test14 / aud_data_rvol_daily_1m_test14








#ivol/rvol

f0 = plt.figure(figsize = (15, 10))
gs0 = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
f0ax0 = plt.subplot(gs0[0])
f0ax1 = plt.subplot(gs0[1])
f0ax2 = plt.subplot(gs0[2])
f0ax3 = plt.subplot(gs0[3])


func(aud_data_ivol_rvol_1m_test14).rename(lambda c: c.split('_')[1]).plot(ax=f0ax0)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f0ax0.set_title('aud 1m ivol/rvol ratio swap curves')


aud_data_ivol_rvol_1m_test14.iloc[-1000:].plot(ax=f0ax1)
f0ax1.set_title('ivol/rvol ratio swap curves')

aud_data_ivol_rvol_1m_test14.iloc[-2000:-1000].plot(ax=f0ax2)
f0ax2.set_title('ivol/rvol ratio swap curves')

aud_data_ivol_rvol_1m_test14.iloc[-3000:-2000].plot(ax=f0ax3)
f0ax3.set_title('ivol/rvol ratio swap curves')


pca.PCA(data_ivol_rvol_1m_test13.iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(data_ivol_rvol_1m_test13.iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(data_ivol_rvol_1m_test13.iloc[-3000:], rates_pca = True, momentum_size = None).plot_loadings(n=3)














usd_fwd_rr_test13['10Y10Y'].plot()





nzd_spot_swap_test13 = data_swap[['NZD_0Y_1Y',
                         'NZD_0Y_2Y',
                         'NZD_0Y_3Y',
                         'NZD_0Y_4Y',
                         'NZD_0Y_5Y',
                         'NZD_0Y_6Y',
                         'NZD_0Y_7Y',
                         'NZD_0Y_8Y',
                         'NZD_0Y_9Y',
                         'NZD_0Y_10Y',
                         'NZD_5Y_5Y'
                         ]].rename(lambda c: c.replace("_0Y_","_"), axis="columns")


nzd_spot_swap_test13 = data_swap[['NZD_1Y_1Y',
                         
                         'NZD_5Y_5Y'
                         ]].rename(lambda c: c.replace("_0Y_","_"), axis="columns")



pca.PCA(data_ccy_basis[[c for c in data_ccy_basis.columns if 'AUD' in c or 'NZD' in c or 'CAD' in c or 'EUR' in c or 'JPY' in c]].iloc[-750:], rates_pca = True, momentum_size = None).plot_loadings(n=3)



pca.PCA(data_ifs[['USD_5Y_SWIT', 'EUR_10Y_SWIT']].iloc[-750:], rates_pca = True, momentum_size = None).plot_loadings(n=3)

pca.PCA(data_ifs[['USD_5Y_SWIT', 'EUR_10Y_SWIT']].iloc[-2500:], rates_pca = True, momentum_size = None).create_hedged_pcs_fig()

pca.PCA(data_ifs[['USD_10Y_SWIT', 'EUR_5Y_SWIT']].iloc[-2500:], rates_pca = True, momentum_size = None).create_hedged_pcs_fig()
pca.PCA(data_ifs[['USD_10Y_SWIT', 'EUR_5Y_SWIT']].iloc[-2500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)




### how to hedge the US front inflation overshoot
### use the mid curve real rate (and nominal) 
                                                                                                                                             
pca.PCA(pd.concat([usd_spot_ifs_test13[['USD_2Y_SWIT']].rename(lambda x: "USD_2Y_IFS", axis=1),
                   usd_spot_ifs_test13[['USD_3Y_SWIT']].rename(lambda x: "USD_3Y_IFS", axis=1),
                   usd_fwd_rr_test13[['3Y2Y']].rename(lambda x: "USD_RR_3Y2Y", axis=1),
                   usd_fwd_rr_test13[['5Y5Y']].rename(lambda x: "USD_RR_5Y5Y", axis=1),
                   usd_fwd_swap_test13[['3Y2Y']].rename(lambda x: "USD_NOM_SWAP_3Y2Y", axis=1),
                   usd_fwd_swap_test13[['5Y5Y']].rename(lambda x: "USD_NOM_SWAP_5Y5Y", axis=1)], 
                  axis=1).iloc[-2500:].dropna(axis=1), 
        rates_pca = True, momentum_size = None).plot_loadings(n=6)



pca.PCA(pd.concat([usd_spot_ifs_test13[['USD_2Y_SWIT']].rename(lambda x: "USD_2Y_IFS", axis=1),
                   usd_spot_ifs_test13[['USD_3Y_SWIT']].rename(lambda x: "USD_3Y_IFS", axis=1),
                   usd_fwd_rr_test13[['3Y2Y']].rename(lambda x: "USD_RR_3Y2Y", axis=1),
                   usd_fwd_rr_test13[['5Y5Y']].rename(lambda x: "USD_RR_5Y5Y", axis=1),
                   usd_fwd_swap_test13[['3Y2Y']].rename(lambda x: "USD_NOM_SWAP_3Y2Y", axis=1),
                   usd_fwd_swap_test13[['5Y5Y']].rename(lambda x: "USD_NOM_SWAP_5Y5Y", axis=1)], 
                  axis=1).iloc[-2500:].dropna(axis=1), 
        rates_pca = True, momentum_size = None).plot_loadings(n=6)



                                                                      

rvol = (data_swap - data_swap.shift(1)).rolling(5).std() * 100

momentum = (data_swap - data_swap.shift(5)) * 100

rdm = rvol*momentum

rvol_samples = rvol[['AUD_1Y_2Y','AUD_3Y_3Y','AUD_5Y_5Y', 'AUD_0Y_10Y', 'USD_1Y_2Y','USD_3Y_3Y','USD_5Y_5Y', 'USD_0Y_10Y']] * np.sqrt(252)

rvol_samples.iloc[-60:].plot()
rdm[['AUD_1Y_2Y','AUD_3Y_3Y','AUD_5Y_5Y', 'AUD_0Y_10Y', 'USD_1Y_2Y','USD_3Y_3Y','USD_5Y_5Y', 'USD_0Y_10Y']].iloc[-60:].plot()



(data_swap - data_swap.shift(1))['AUD_3Y_3Y'].iloc[-90:].std()*100* np.sqrt(252)



##############################################
# test 15 - curve term premium
##############################################

test15_aud_term_premium_data = pd.DataFrame({'3Y_3S10s': data_swap['AUD_3Y_10Y'] - data_swap['AUD_3Y_3Y'],
                                            '0Y_3S10s': data_swap['AUD_0Y_10Y'] - data_swap['AUD_0Y_3Y'],
                                            '5Y5Y': data_swap['AUD_5Y_5Y'],
                                             '3Y':data_swap['AUD_0Y_3Y']}).ffill().dropna(axis=0)


#set up main history
xs = test15_aud_term_premium_data['3Y']
ys = test15_aud_term_premium_data['3Y_3S10s']
    
start_x = xs.loc[xs.first_valid_index()]
start_y = ys.loc[ys.first_valid_index()]
start_dt = xs.first_valid_index().strftime('%Y-%m-%d')

last_x = xs.loc[xs.last_valid_index()]
last_y = ys.loc[ys.last_valid_index()]
last_dt = xs.last_valid_index().strftime('%Y-%m-%d')

# get regression beta for post gfc history
post_gfc_xs = xs[xs.index > '2011-01-01']
post_gfc_ys = ys[ys.index > '2011-01-01']
(beta_x, beta_0), (unfitted_xs, fitted_ys, residual_ys, residual_ys_std, residual_ys_p_1std, residual_ys_m_1std) = get_linear_betas(post_gfc_xs, post_gfc_ys)
post_gfc_term_premium = pd.DataFrame({'3Y': unfitted_xs,
                                      'Term Premium': pd.Series(data= residual_ys, index=unfitted_xs.index),
                                      'Spot 3s10s':test15_aud_term_premium_data[test15_aud_term_premium_data.index >'2011-01-01'] ['0Y_3S10s'],
                                      '5Y5Y':test15_aud_term_premium_data[test15_aud_term_premium_data.index >'2011-01-01'] ['5Y5Y']})



#set up the figure
test15_aud_term_premium_fig = plt.figure(figsize = (45, 15))
gs = gridspec.GridSpec(1, 3, height_ratios=[1], width_ratios=[1,1,1])
ax0 = plt.subplot(gs[0])

colors0 = mpl.dates.date2num(xs.index.to_pydatetime())
colors1 = 'lightgrey'

cmap0 = plt.cm.get_cmap('binary')
cmap1 = plt.cm.get_cmap('Reds')


#plot main
main_history_mappable = plot_main(ax0, xs, ys, "AUD 3Y", "AUD 3Y FWD 3s10s", colors0, cmap1)


#plot fits
fit_mappables = plot_scatter_fits(ax0, 
                                  [post_gfc_xs], 
                                  [post_gfc_ys],
                                  [''], 
                                  ['black'], 
                                  legend_loc = 2)
    
#plot post gfc xs and term premium
ax1 = plt.subplot(gs[1])
# colors0 = mpl.dates.date2num(post_gfc_term_premium['3Y'].index.to_pydatetime())
# colors1 = 'lightgrey'

# cmap0 = plt.cm.get_cmap('binary')
# cmap1 = plt.cm.get_cmap('Reds')

# post_gfc_term_premium_history_mappable = plot_main(ax1, post_gfc_term_premium['3Y'], post_gfc_term_premium['Term Premium'], "AUD 3Y", "AUD Curve Term Premium", colors0, cmap1)

post_gfc_term_premium[['5Y5Y', 'Term Premium']].plot(ax=ax1, secondary_y=['Term Premium'])

ax2 = plt.subplot(gs[2])    
post_gfc_term_premium[['Spot 3s10s', 'Term Premium']].plot(ax=ax2, secondary_y=['Term Premium'])




##############################################
# test 16 - USD historical 
##############################################

def distinct(seq): 
    checked = []
    for e in seq:
        if e not in checked:
            checked.append(e)
    return checked


def convert_fwd_to_curve_df(data, substract_col_index = None):
    #data is a series
    output = {}
    fwds = distinct([c.split('_')[1] for c in data.index])
    for fwd in fwds:
        col_for_fwd = [c for c in data.index if "_{}_".format(fwd) in c]
        output[fwd] = data.loc[col_for_fwd].rename(lambda x:x.split('_')[2])
    output = pd.DataFrame(output)
    output.index.name = data.name.strftime('%Y-%m-%d')
    if(substract_col_index is not None):
        for c in output.columns:
           output[c] = output[c] - output[output.columns[substract_col_index]]   
    
    return pd.DataFrame(output)
      

usd_swap_curve_cols = [c for c in data_usd_all.columns if ('USD_' in c) and c.split('_')[2] != '3M' ]
usd_swap_curve = data_usd_all[usd_swap_curve_cols]

usd_swap_curve_rvol_daily = (usd_swap_curve - usd_swap_curve.shift(1)).rolling(20).std() * 100 * np.sqrt(365)
#usd_swap_curve_rvol_daily = (usd_swap_curve - usd_swap_curve.shift(1)).rolling(60).std() * 100 * np.sqrt(365) 


usd_swap_ivol_curve_cols = [c for c in data_usd_all.columns if ('IVOL_' in c) and c.split('_')[2] != '3M' and c.split('_')[2] != '11Y' and c.split('_')[2] != '12Y' ]
usd_swap_ivol_curve = data_usd_all[usd_swap_ivol_curve_cols]

usd_swap_ivol_curve_in_yields = pd.DataFrame({c: convert_atmf_vol_to_yield(c.split('_')[1], usd_swap_ivol_curve[c]) for c in usd_swap_ivol_curve.columns})



#swap

plt.figure(figsize = (20, 15))
gs = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
ax0 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[1])
ax2 = plt.subplot(gs[2])
ax3 = plt.subplot(gs[3])



colormap = plt.cm.nipy_spectral

swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_curve.iloc[-1], substract_col_index=None)
ax0.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax0.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax0, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_curve.iloc[-1000], substract_col_index=None)
ax1.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax1.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax1, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_curve.iloc[-1500], substract_col_index=None)
ax2.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax2.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax2, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_curve.iloc[-1800], substract_col_index=None)
ax3.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax3.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax3, grid=True, fontsize=8)


ymax = max([ax0.get_ylim()[1], ax1.get_ylim()[1], ax2.get_ylim()[1], ax3.get_ylim()[1]])
ax0.set_ylim(ax0.get_ylim()[0], ymax)
ax1.set_ylim(ax1.get_ylim()[0], ymax)
ax2.set_ylim(ax2.get_ylim()[0], ymax)
ax3.set_ylim(ax3.get_ylim()[0], ymax)

#plt.tight_layout()

#swap vol



plt.figure(figsize = (20, 15))
gs = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
ax0 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[1])
ax2 = plt.subplot(gs[2])
ax3 = plt.subplot(gs[3])



colormap = plt.cm.nipy_spectral

swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_curve_rvol_daily.iloc[-1], substract_col_index=None)
ax0.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax0.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax0, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_curve_rvol_daily.iloc[-30], substract_col_index=None)
ax1.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax1.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax1, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_curve_rvol_daily.iloc[-500], substract_col_index=None)
ax2.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax2.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax2, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_curve_rvol_daily.iloc[-1000], substract_col_index=None)
ax3.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax3.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax3, grid=True, fontsize=8)


ymax = max([ax0.get_ylim()[1], ax1.get_ylim()[1], ax2.get_ylim()[1], ax3.get_ylim()[1]])
ax0.set_ylim(ax0.get_ylim()[0], ymax)
ax1.set_ylim(ax1.get_ylim()[0], ymax)
ax2.set_ylim(ax2.get_ylim()[0], ymax)
ax3.set_ylim(ax3.get_ylim()[0], ymax)
plt.tight_layout()




#swap ivol

plt.figure(figsize = (20, 15))
gs = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
ax0 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[1])
ax2 = plt.subplot(gs[2])
ax3 = plt.subplot(gs[3])



colormap = plt.cm.nipy_spectral

swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_ivol_curve.iloc[-1], substract_col_index=None)

ax0.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax0.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax0, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_ivol_curve.iloc[-1000], substract_col_index=None)
ax1.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax1.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax1, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_ivol_curve.iloc[-1500], substract_col_index=None)
ax2.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax2.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax2, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_ivol_curve.iloc[-1800], substract_col_index=None)
ax3.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax3.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax3, grid=True, fontsize=8)


ymax = max([ax0.get_ylim()[1], ax1.get_ylim()[1], ax2.get_ylim()[1], ax3.get_ylim()[1]])
ax0.set_ylim(ax0.get_ylim()[0], ymax)
ax1.set_ylim(ax1.get_ylim()[0], ymax)
ax2.set_ylim(ax2.get_ylim()[0], ymax)
ax3.set_ylim(ax3.get_ylim()[0], ymax)

#plt.tight_layout()
   



#swap ivol in yields

plt.figure(figsize = (20, 15))
gs = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
ax0 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[1])
ax2 = plt.subplot(gs[2])
ax3 = plt.subplot(gs[3])



colormap = plt.cm.nipy_spectral

swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_ivol_curve_in_yields.iloc[-1], substract_col_index=None)

ax0.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax0.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax0, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_ivol_curve_in_yields.iloc[-7], substract_col_index=None)
ax1.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax1.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax1, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_ivol_curve_in_yields.iloc[-2000], substract_col_index=None)
ax2.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax2.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax2, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_ivol_curve_in_yields.iloc[-1800], substract_col_index=None)
ax3.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax3.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax3, grid=True, fontsize=8)


ymax = max([ax0.get_ylim()[1], ax1.get_ylim()[1], ax2.get_ylim()[1], ax3.get_ylim()[1]])
ax0.set_ylim(ax0.get_ylim()[0], ymax)
ax1.set_ylim(ax1.get_ylim()[0], ymax)
ax2.set_ylim(ax2.get_ylim()[0], ymax)
ax3.set_ylim(ax3.get_ylim()[0], ymax)

#plt.tight_layout()
 


usd_swap_curve['USD_3Y_3Y']
usd_swap_ivol_curve_in_yields['IVOL_3Y_3Y']




# t15 2y1y vs ifs 

t15_data = pd.DataFrame({"SW2Y1Y":usd_fwd_swap_test13['2Y1Y'],
                         "RR2Y1Y":usd_fwd_rr_test13['2Y1Y'],
                         "IFS3Y":usd_spot_ifs_test13['USD_3Y_SWIT'],
                         "IFS5Y":usd_spot_ifs_test13['USD_5Y_SWIT']                         
                         })


pca.PCA(t15_data[t15_data.index > '2008-01-01'], rates_pca = True, momentum_size = None).plot_loadings(n=3)

pca.PCA(t15_data[t15_data.index > '2019-01-01'], rates_pca = True, momentum_size = None).plot_loadings(n=3)

pca.PCA(t15_data[t15_data.index > '2020-07-01'], rates_pca = True, momentum_size = None).plot_loadings(n=3)


t15_data_corr = pd.DataFrame(rolling_apply(t15_data[["SW2Y1Y","IFS3Y","IFS5Y"]][t15_data.index > '2020-07-01'], 10, get_flattened_corr)).transpose()

t15_data_corr = t15_data_corr[['SW2Y1Y_vs_IFS3Y', 'SW2Y1Y_vs_IFS5Y']]



# test 20
if(2 > 3):
    
    AU_cols =  [c for c in data.columns if 'AU' in c]
    AU_cols.sort()
    data_aud = data[AU_cols].dropna()
    
    pca.PCA(data_aud, rates_pca = True, momentum_size = None).plot_loadings(n=3)    
    pca.PCA(data_aud.iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
    pca.PCA(data_aud.iloc[-200:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
    



#test 21 sbins

if (2> 3):
    data_21 = data_gov[['USD_0Y_2Y', 'USD_0Y_5Y', 'USD_0Y_10Y', 'USD_0Y_30Y']].iloc[:-1]
    data_21_next_day_rtn = (data_21 - data_21.shift(1)).shift(-1).rename(lambda x: '{}_return'.format(x), axis=1)
    data_21  = pd.concat([data_21, data_21_next_day_rtn], axis=1).dropna()
    
    from statistics.mr import sbins
    data_21_sbin = sbins.sbins(data_21.iloc[-90:], 'USD_0Y_10Y', 10)
    
    data_21_sbin.plot_bin_mean_variance_for_col('USD_0Y_10Y_return')
    data_21_sbin.plot_bin_mean_variance_for_col('USD_0Y_30Y_return')
    
    from statsmodels.distributions.empirical_distribution import ECDF
    
    
    ecdf_dist = ECDF(data_21['USD_0Y_10Y_return'].iloc[-30:])
    plt.plot(ecdf_dist.x, ecdf_dist.y)
    plt.show()
    
    pdf_dist_y = np.gradient(ecdf_dist.y, ecdf_dist.x)
    plt.plot(ecdf_dist.x, pdf_dist_y)    
    plt.show()

    
    
    #
    from statistics.mr import dist
    
    
    data_21['USD_0Y_10Y'].iloc[-40:].plot()
    
    data_21['USD_0Y_10Y'].iloc[-40:].diff().sum()
    
    d1 = data_21.iloc[-60:][data_21['USD_0Y_10Y']>1.6]['USD_0Y_10Y_return'].rename('above_150')
    d2 =data_21.iloc[-60:][(data_21['USD_0Y_10Y'] < 1.6) & (data_21['USD_0Y_10Y'] > 1.4) ]['USD_0Y_10Y_return'].rename('above_140to160')
    d2 =data_21.iloc[-60:][(data_21['USD_0Y_10Y'] > 1.4) ]['USD_0Y_10Y_return'].rename('above_140to160')
    
    
    dist.plot_best_fit_pdf(d1,bin_size=20,xaxis_labels='US10 daily return')
    
    dist.plot_best_fit_pdf(d2,bin_size=20,xaxis_labels='US10 daily return')
    
    # Load data from statsmodels datasets
    #data = pd.Series(sm.datasets.elnino.load_pandas().data.set_index('YEAR').values.ravel())
    
    
    cad_fly = data_swap[['CAD_0Y_2Y', 'CAD_2Y_2Y', 'CAD_5Y_5Y']]
    pca.PCA(cad_fly.iloc[-1250:],rates_pca = True).plot_loadings(n=3)
    pca.PCA(cad_fly.iloc[-90:],rates_pca = True).plot_loadings(n=3)
    pca.PCA(cad_fly.iloc[-2000:-1750],rates_pca = True).plot_loadings(n=3)
    pca.PCA(cad_fly.iloc[-2200:-1900],rates_pca = True).plot_loadings(n=3)
    pca.PCA(cad_fly.iloc[-90:],rates_pca = True).plot_loadings(n=3)
    
    cad_fly.iloc[-2200:-1900].apply(lambda x: x['CAD_0Y_2Y'] * -0.5 + x['CAD_2Y_2Y'] * -0.66 + x['CAD_5Y_5Y'] * + 0.56, axis=1).plot()
    cad_fly.iloc[-1900:-1700].apply(lambda x: x['CAD_0Y_2Y'] * -0.5 + x['CAD_2Y_2Y'] * -0.66 + x['CAD_5Y_5Y'] * + 0.56, axis=1).plot()
    cad_fly.iloc[-1900:-1700].apply(lambda x: - x['CAD_0Y_2Y'] + x['CAD_5Y_5Y'], axis=1).plot()
    cad_fly.iloc[-1900:-1700].apply(lambda x: - x['CAD_2Y_2Y'] + x['CAD_5Y_5Y'], axis=1).plot()
    
    cad_fly.iloc[-2200:].apply(lambda x: - x['CAD_2Y_2Y'] + x['CAD_5Y_5Y'], axis=1).plot()
    
    
    cad_curve = pd.DataFrame({'CAD_2Y_1Y':data_swap['CAD_0Y_3Y'] * 3  - data_swap['CAD_0Y_2Y'] *2,
                             'CAD_4s8s': data_swap['CAD_0Y_8Y'] - data_swap['CAD_0Y_4Y']})
    
    
    pca.PCA(cad_curve.iloc[-2000:-1750],rates_pca = True).plot_loadings(n=2)    
    pca.PCA(cad_curve.iloc[-1900:-1750],rates_pca = True).plot_loadings(n=2)
    pca.PCA(cad_curve.iloc[-2000:-1900],rates_pca = True).plot_loadings(n=2)
    pca.PCA(cad_curve.iloc[-2000:],rates_pca = True).plot_loadings(n=2)
    pca.PCA(cad_curve.iloc[-60:],rates_pca = True).plot_loadings(n=2)
    
    cad_curve.iloc[-2200:].plot(secondary_y=['CAD_2Y_1Y'])
    
    
    (data_swap['CAD_5Y_5Y'] - data_swap['CAD_3Y_2Y']).dropna().plot()    
    (data_swap['CAD_5Y_5Y'] - data_swap['CAD_2Y_2Y']).dropna().plot()
    
    (data_swap['CAD_0Y_8Y'] - data_swap['CAD_0Y_4Y']).dropna().plot()
    (data_swap['CAD_0Y_7Y'] - data_swap['CAD_0Y_4Y']).dropna().plot()
    (data_swap['CAD_0Y_6Y'] - data_swap['CAD_0Y_4Y']).dropna().plot()    
    
    pca.PCA(data_swap[['CAD_0Y_4Y', 'CAD_0Y_5Y', 'CAD_0Y_6Y', 'CAD_0Y_7Y', 'CAD_0Y_8Y', 'CAD_0Y_9Y', 'CAD_0Y_10Y']],rates_pca=True).plot_loadings(n=3)
    pca.PCA(data_swap[['CAD_0Y_4Y', 'CAD_0Y_5Y', 'CAD_0Y_6Y', 'CAD_0Y_7Y', 'CAD_0Y_8Y', 'CAD_0Y_9Y', 'CAD_0Y_10Y']].iloc[-90:],
            rates_pca=True).plot_loadings(n=3)
    
    
    
    cad_fly = pd.concat([data_swap[['CAD_2Y_1Y', 'CAD_0Y_8Y']].dropna(), data_swap[['CAD_2Y_1Y', 'CAD_0Y_8Y']].dropna().diff().pow(2).rename(lambda x: '{}_vol'.format(x), axis=1)], axis=1).dropna()
    cad_fly.plot(secondary_y=['CAD_2Y_1Y', 'CAD_0Y_8Y'])

    
    data_swap[['CAD_2Y_1Y', 'CAD_0Y_8Y']].dropna().diff().pow(2).apply(lambda x: x['CAD_2Y_1Y']-x['CAD_0Y_8Y'], axis=1)


    pd.concat([data_swap[['CAD_2Y_1Y', 'CAD_0Y_8Y']].dropna(), 
    
    
    ##
    from statistics.mr import sbins               
    
    CAD_2y1ys8s = data_swap[['CAD_2Y_1Y', 'CAD_0Y_8Y']].dropna().apply(lambda x: - x['CAD_2Y_1Y'] + x['CAD_0Y_8Y'], axis=1)
    CAD_2y1ys8s_rtns = (CAD_2y1ys8s - CAD_2y1ys8s.shift(1)).shift(-1)
    
    cad_curve = pd.DataFrame({'VOL_2y1ys8s': data_swap[['CAD_2Y_1Y', 'CAD_0Y_8Y']].dropna().diff().pow(2).apply(lambda x: - x['CAD_2Y_1Y'] + x['CAD_0Y_8Y'], axis=1),
                               'CAD_2y1ys8s_rtns': CAD_2y1ys8s_rtns                  
                               }).dropna()
    
    cad_curve['VOL_2y1ys8s'].plot.hist(bin=50)
    
    cad_curve_sbin = sbins.sbins(cad_curve[(cad_curve['VOL_2y1ys8s'] > -0.002) & (cad_curve['VOL_2y1ys8s'] < 0.002)], 'VOL_2y1ys8s', 20)
    cad_curve_sbin.plot_bin_mean_variance_for_col('CAD_2y1ys8s_rtns')

    
    #plot_curve()
    from statistics.mr import tools as mrt
    
    mrt.plot_fly(data_swap[['USD_0Y_5Y', 'USD_0Y_10Y', 'USD_0Y_30Y']].dropna().iloc[-2500:])
    
    
    
    mrt.plot_curve(data_swap[['USD_0Y_5Y', 'USD_0Y_10Y']].dropna().iloc[-1600:-1400], curve=True)
    mrt.plot_curve(data_swap[['USD_0Y_5Y', 'USD_0Y_10Y']].dropna().iloc[-1600:-1500], curve=True)
    mrt.plot_curve(data_swap[['USD_0Y_5Y', 'USD_0Y_30Y']].dropna().iloc[-125:], curve=True)
    
    
    mrt.plot_curve(data_swap[['USD_0Y_5Y', 'USD_0Y_10Y']].dropna().iloc[-1900:-1700], curve=True)
    mrt.plot_curve(data_swap[['USD_0Y_10Y', 'USD_0Y_30Y']].dropna().iloc[-1900:-1700], curve=True)
    
    mrt.plot_curve(data_swap[['USD_0Y_5Y', 'USD_0Y_10Y']].dropna().iloc[-270:], curve=True)
    
   
    pca.PCA(data_swap2[['AUD_5Y_5Y', 'GBP_5Y_5Y']].diff().iloc[-200:],rates_pca=True).plot_loadings(n=3)
    
    
    pca.PCA(data_swap2[['AUD_5Y_5Y', 'GBP_5Y_5Y']].iloc[-0:],rates_pca=True).plot_loadings(n=3)
    
    
    pca.PCA(data_swap2[['AUD_0Y_5Y', 'AUD_0Y_10Y']].diff().iloc[-50:],rates_pca=True).plot_loadings(n=3)
    
    mrt.plot_curve(data_swap[['AUD_0Y_5Y', 'AUD_0Y_10Y']].dropna().iloc[-100:], curve=True)
    
    ##
    from statistics.mr import bins
    from statistics.mr import sbins  
    from statistics.mr import tools as mrt
    
    pca.PCA(data_swap2[['AUD_5Y_5Y', 'GBP_5Y_5Y']].diff().iloc[-200:],rates_pca=True).plot_loadings(n=3)
    pca.PCA(data_swap2[['AUD_5Y_5Y', 'GBP_5Y_5Y']].iloc[-200:],rates_pca=True).plot_loadings(n=3)
    
    
    for k, (mr, p) in \
        create_combo_mr(data_swap2[['AUD_5Y_5Y', 'USD_5Y_5Y']].iloc[-3000:], 
                            elements = 2, resample_freq= 'W', 
                            include_normal_weighted_series=False, 
                            matrixoveride = None,
                            sample_range = ('2018-01-01', '2019-11-01')).items():
                
        mr.create_mr_info_fig((18, 7), pca_obj = p)
        
    
    mrs = create_combo_mr(data_swap2[['AUD_5Y_5Y', 'USD_5Y_5Y']].iloc[-3000:], 
                            elements = 2, resample_freq= 'W', 
                            include_normal_weighted_series=False, 
                            matrixoveride = None,
                            sample_range = ('2018-01-01', '2019-11-01'))
    
    # first object is the mr
    # call its ou to simulate
 
    mrs['AUD_5Y_5Y_USD_5Y_5Y_PCW'][0].create_mr_info_fig((18, 7), pca_obj = p)
    sim, distrib = mrs['AUD_5Y_5Y_USD_5Y_5Y_PCW'][0].ou.simulate_with_model_parameters(k=5000, target=-0.25, stop=-0.9)
        
    
    
        
    
    data_21 = data_gov[['USD_0Y_2Y', 'USD_0Y_5Y', 'USD_0Y_10Y', 'USD_0Y_30Y']].iloc[:-1]
    data_21_next_day_rtn = (data_21 - data_21.shift(1)).shift(-1).rename(lambda x: '{}_return'.format(x), axis=1)
    data_21  = pd.concat([data_21, data_21_next_day_rtn], axis=1).dropna()
    
    data_21_sbin = sbins.sbins(data_21.iloc[-90:], 'USD_0Y_10Y', 10)
    
    data_21_sbin.plot_bin_mean_variance_for_col('USD_0Y_10Y_return')
    data_21_sbin.plot_bin_mean_variance_for_col('USD_0Y_30Y_return')
    
    
    
    
    









    

