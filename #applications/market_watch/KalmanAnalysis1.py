import pandas as pd
import numpy as np
import os, sys, inspect
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from cycler import cycler
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations
from pykalman import KalmanFilter

from pandas.tseries.offsets import DateOffset


_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper


from scipy import stats
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, fcluster
from scipy.spatial.distance import pdist, squareform



pd.set_option('display.expand_frame_repr', False)



def convert_atmf_vol_to_yield(option, vols):

    base = 1/np.sqrt(2*3.14*12)
    if option.upper() == '1M':
        return base * np.sqrt(1) * vols
    elif option.upper() == '2M':
        return base * np.sqrt(2) * vols     
    elif option.upper() == '3M':
        return base * np.sqrt(3) * vols
    elif option.upper() == '6M':
        return base * np.sqrt(6)* vols
    elif option.upper() == '1Y':
        return base * np.sqrt(12)* vols
    elif option.upper() == '2Y':
        return base * np.sqrt(24)* vols
    elif option.upper() == '3Y':
        return base * np.sqrt(36)* vols
    elif option.upper() == '4Y':
        return base * np.sqrt(48)* vols
    elif option.upper() == '5Y':
        return base * np.sqrt(60)* vols
    elif option.upper() == '6Y':
        return base * np.sqrt(72)* vols
    elif option.upper() == '7Y':
        return base * np.sqrt(84)* vols
    elif option.upper() == '8Y':
        return base * np.sqrt(96)* vols
    elif option.upper() == '9Y':
        return base * np.sqrt(108)* vols
    elif option.upper() == '10Y':
        return base * np.sqrt(120)* vols    
    
    else:
        raise ValueError("unknown optin tenor {0}".format(option))


def plot_scatter_main(ax, xs, ys, colors, cmaps, ticklabelsize=8):
    mapable = ax.scatter(xs, ys, c=colors, cmap=cmaps, marker='o', s = 9)
        
    ax.set_ylabel(ys.name, fontsize=8)
    ax.set_xlabel(xs.name, fontsize=8)
    ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
    ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
    
    for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    
    for label,tick in zip(ax.get_xticklabels(), ax.get_xticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    return mapable


def plot_scatter_endpoints(ax, xs, ys, colors, sizes, labels, markers, legend_loc=None,ncol=1, fontsize=8, lw =3):
    ds = [ax.scatter(xs[i], ys[i], c=colors[i], s=sizes[i], label=labels[i], marker=markers[i], linewidth= lw) for i in np.arange(len(xs))]
    if legend_loc is not None:
        lg =ax.legend(ds, labels, scatterpoints=1, fancybox=True, framealpha=0, loc=legend_loc, ncol=ncol, fontsize=fontsize)
        ax.add_artist(lg)
    return ds
    


def plot_scatter_fits(ax, xss, yss, label_prefixes, colors, legend_loc=None, ncol=1, fontsize=8):
    #ploting multiple fits
    pcs = []
    for (xs, ys, label_prefix, color) in zip(xss, yss, label_prefixes, colors): 
        (beta_x, beta_0), (xs, fitted_ys, residual_ys, residual_ys_std, residual_ys_p_1std, residual_ys_m_1std) = get_linear_betas(xs, ys)
        label = r'{1} = {3:.2f} + {2:.2f} * {0} with stdev {4:.2f} '.format(xs.name, ys.name, beta_x, beta_0, residual_ys_std)
        label = label_prefix + ', ' + label if label_prefix is not None else label
        pcs = pcs + ax.plot(xs, fitted_ys, lw=1, c=color, ls = '-', marker = '.', ms = 0, label=label)
        ax.plot(xs, residual_ys_p_1std, lw=0.5, c=color, ls = ':', ms = 0)
        ax.plot(xs, residual_ys_m_1std, lw=0.5, c=color, ls = ':', ms = 0)

    if legend_loc is not None:
        lgd = ax.legend(handles=pcs, loc=legend_loc, fancybox=True, framealpha=0, ncol=ncol, fontsize=fontsize)
        ax.add_artist(lgd)
    return pcs





def get_linear_betas(xs, ys):
    betas, _, _, _, _ = np.polyfit(list(xs), list(ys), deg = 1, full=True)
    (beta_x, beta_0) = betas
    #xs = df[df.columns[0]]
    #ys = df[df.columns[1]]
    #start_x = xs.min() 
    #last_x = xs.max()
    #step = (last_x - start_x)/100 
    #xs = np.arange(start_x,last_x, step)
    fitted_ys = [round(beta_0 + beta_x * x, 4) for x in list(xs)]
    residual_ys = [y - fitted_y for y, fitted_y in  zip(ys, fitted_ys)]
    residual_ys_std = np.std(residual_ys, ddof=1) 
    residual_ys_p_1std =  [fitted_y + residual_ys_std for fitted_y in  fitted_ys]
    residual_ys_m_1std =  [fitted_y - residual_ys_std for fitted_y in  fitted_ys]
    
    return betas, (xs, fitted_ys, residual_ys, residual_ys_std, residual_ys_p_1std, residual_ys_m_1std)
   


def bootstrap_ifs_fwd(data):
    nc =  { c: int( c[4:].replace('_SWIT','').replace('Y','') ) for c in data.columns}
    data = data.rename(columns=nc)
    a = data.columns.tolist()[1:]
    b = data.columns.tolist()[:-1]
    fwd = {}
    fwd['{0}Y'.format(data.columns[0])] = data[data.columns[0]]
    for (ca,cb) in zip(a, b):
        term = ca - cb
        forward = cb
        cca = (1 + data[ca]/100).pow(ca)
        ccb = (1 + data[cb]/100).pow(cb)
        fwd['{0}Y{1}Y'.format(forward, term)] = np.log(cca/ccb)/term * 100
    fwd = pd.DataFrame(fwd)
    return fwd


def plot_main(ax, xs, ys, xlegend, ylegend, colors, cmaps, ticklabelsize=8, mirror_limit_x = False, mirror_limit_y = False):
    
    mapable = ax.scatter(xs, ys, c=colors, cmap=cmaps, marker='o', s = 6)
        
    ax.set_ylabel(ys.name, fontsize=8)
    ax.set_xlabel(xs.name, fontsize=8)
    ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
    ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
    
    for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    
    #for label,tick in zip(ax.get_xticklabels(), ax.get_xticks()):
    #    label.set_fontsize(ticklabelsize)
    #    if tick < 0:
    #        label.set_color("red")
    #    else:
    #        label.set_color("black")
    
    ymin, ymax = ax.get_ylim()
    xmin, xmax = ax.get_xlim()    
    
    ymax_abs = max([abs(ymin), abs(ymax)])
    xmax_abs = max([abs(xmin), abs(xmax)])
    
    xymax = max([abs(ymin), abs(ymax), abs(xmin), abs(xmax)])
    
    if mirror_limit_x:
        ax.set_xlim(-1 * xmax_abs, xmax_abs)
    if mirror_limit_y:
        ax.set_ylim(-1 * ymax_abs, ymax_abs)
    
    # Move left y-axis and bottim x-axis to centre, passing through (0,0)
    # ax.spines['left'].set_position('center')
    
    
    #ax.spines['bottom'].set_position('center')
    # Eliminate upper and right axes
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    # Show ticks in the left and lower axes only
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    
    ax.xaxis.set_major_locator(MultipleLocator(0.5))
    ax.yaxis.set_major_locator(MultipleLocator(1))
    ax.xaxis.grid(True, which='major')
    #ax.xaxis.grid(True, which='minor')
    ax.yaxis.grid(True, which='major')
    
    for label in ax.get_xticklabels(minor =False):
        label.set_fontsize(7)

    for label in ax.get_yticklabels(minor =False):
        label.set_fontsize(7)
        
    ax.set_ylabel(ylegend, fontsize=9)
    ax.yaxis.set_label_coords(0, 0.12)

    ax.set_xlabel(xlegend, fontsize=9)
    ax.xaxis.set_label_coords(0.85, 0.045)
    
    # end points
    start_x = xs.loc[xs.first_valid_index()]
    start_y = ys.loc[ys.first_valid_index()]
    start_date = xs.first_valid_index().strftime(r'%Y-%m-%d')
    
    ds1 = ax.scatter(start_x, start_y, c='#f5d9c8', s=90, alpha=0.5, label=start_date, edgecolors='b')
    
    last_x = xs.loc[xs.last_valid_index()]
    last_y = ys.loc[ys.last_valid_index()]
    last_date = xs.last_valid_index().strftime(r'%Y-%m-%d')

    ds2 = ax.scatter(last_x, last_y, c='darkred', s=90, alpha=1, label=last_date)
    
    
    ax.legend([ds1, ds2], [start_date, last_date], scatterpoints=1, fancybox=True, framealpha=0, loc=1, ncol=1, fontsize=9)
    
    return mapable


def plot_multi_lines(data, cols=None, spacing=.1, **kwargs):

    from pandas import plotting

    # Get default color style from pandas - can be changed to any other color list
    if cols is None: cols = data.columns
    if len(cols) == 0: return
    colors = getattr(getattr(plotting, '_matplotlib').style, '_get_standard_colors')(num_colors=len(cols))

    # First axis
    ax = data.loc[:, cols[0]].plot(label=cols[0], color=colors[0], **kwargs)
    ax.set_ylabel(ylabel=cols[0])
    lines, labels = ax.get_legend_handles_labels()

    for n in range(1, len(cols)):
        # Multiple y-axes
        ax_new = ax.twinx()
        ax_new.spines['right'].set_position(('axes', 1 + spacing * (n - 1)))
        data.loc[:, cols[n]].plot(ax=ax_new, label=cols[n], color=colors[n % len(colors)], **kwargs)
        ax_new.set_ylabel(ylabel=cols[n])

        # Proper legend position
        line, label = ax_new.get_legend_handles_labels()
        lines += line
        labels += label

    ax.legend(lines, labels, loc=0)
    return ax


def rolling_apply(df, window_size, func, min_window_size=None):
    if min_window_size is None:
        min_window_size = window_size
    result = {}
    for i in range(1, len(df)+1):
        sub_df = df.iloc[max(i - window_size, 0):i, :] #I edited here

        if len(sub_df) >= min_window_size:            
            idx = sub_df.index[-1]
            result[idx] = func(sub_df)
    return result


def get_flattened_cov(df):
    covmatx = df.cov()
    nodes = set([comb for comb in combinations(covmatx.columns.tolist() + covmatx.columns.tolist(), 2)])
    result = pd.Series({col + '_vs_' + row : covmatx[col][row]  for (col, row) in nodes})
    return result


def get_flattened_corr(df):
    covmatx = df.corr()
    nodes = set([comb for comb in combinations(covmatx.columns.tolist() + covmatx.columns.tolist(), 2)])
    result = pd.Series({col + '_vs_' + row : covmatx[col][row]  for (col, row) in nodes})
    return result


def func(df): 
    return pd.DataFrame({
                      'Now':df.iloc[-2:].mean(),
                      #'2020-09-11':df[(df.index >= '2020-09-08') & (df.index <= '2020-09-12')].mean(),
                      #'2020-09-04':df[(df.index >= '2020-09-02') & (df.index <= '2020-09-05')].mean(),
                      #'2020-08-12':df[(df.index >= '2020-08-10') & (df.index <= '2020-08-13')].mean(),
                      #'Pre speech':df.iloc[-3],
                      '7d avg':df.iloc[-7:].mean(), 
                      '7d avg 7d ago':df.iloc[-14:-7].mean(), 
                      '7d avg 14d ago':df.iloc[-21:-14].mean(), 
                      '7d avg 21d ago':df.iloc[-28:-21].mean(), 
                      #'7d avg 28d ago':df.iloc[-35:-28].mean(), 
                      #'avg last 3 years':df.iloc[-97:-90].mean(),
                      #'7d avg 180 day ago':df.iloc[-187:-180].mean(),
                      #'7d avg 360 day ago':df.iloc[-367:-360].mean(),
                      #'2010-2011 avg': df[(df.index > '2010-01-01') & (df.index < '2011-12-31')].mean(),
                      #'2010-Oct-2011-Jul': df[(df.index > '2010-10-01') & (df.index < '2011-07-01')].mean(),
                      #'2012-2013 avg': df[(df.index > '2012-01-01') & (df.index < '2013-12-31')].mean(),
                      #'2016-2019 avg': df[(df.index > '2016-01-01') & (df.index < '2019-12-31')].mean(),
                      #'2016-08': df[(df.index > '2016-08-01') & (df.index < '2016-09-01')].mean(),
                      #'2016-09': df[(df.index > '2016-09-01') & (df.index < '2016-10-01')].mean(),
                      #'2016-10': df[(df.index > '2016-10-01') & (df.index < '2016-11-01')].mean()
                      }) 


def vol_snapshot(df,n=20):
    
    rtn = df - df.shift(1)    
    
    return pd.DataFrame({
                      'Now':rtn.iloc[-n:].std(),
                      '1m ago':rtn.iloc[-2*n:-n].std(), 
                      '2m ago':rtn.iloc[-3*n:-2*n].std(), 
                      '3m ago':rtn.iloc[-4*n:-3*n].std()                      
                      }) * np.sqrt(252)





rundate = '2021-03-08'



data_usd_all = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + '_USD_historical.csv')  
data_usd_all['date'] = pd.to_datetime(data_usd_all['date'])
data_usd_all = data_usd_all.set_index('date').ffill()



data_econ = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + '_ECON_historical.csv')  
data_econ['date'] = pd.to_datetime(data_econ['date'])
data_econ = data_econ.set_index('date').ffill()


data = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + '_BEIRR_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()


data_swap = pd.read_csv('C:/Temp/test/market_watch/staging/'  + rundate +  '_SWAP_ALL_historical.csv')  
data_swap['date'] = pd.to_datetime(data_swap['date'])
data_swap = data_swap.set_index('date').ffill()

data_swap2 = pd.read_csv('C:/Temp/test/market_watch/staging/'  + rundate +  '_SWAP_ALL_V2_historical.csv')  
data_swap2['date'] = pd.to_datetime(data_swap2['date'])
data_swap2 = data_swap2.set_index('date').ffill()


# data_bcs = pd.read_csv(r'C:/Dev/Models/QuantModels/#test/BCS_2020-07-29.csv')  
# data_bcs['date'] = pd.to_datetime(data_bcs['date'])
# data_bcs = data_bcs.set_index('date').ffill()
# data_bcs_rtn = (data_bcs - data_bcs.shift(1))/100

data_ifs = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_SWIT_historical.csv')  
data_ifs['date'] = pd.to_datetime(data_ifs['date'])
data_ifs = data_ifs.set_index('date').ffill()


data_ccy_basis = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_BS_ALL_historical.csv')  
data_ccy_basis['date'] = pd.to_datetime(data_ccy_basis['date'])
data_ccy_basis = data_ccy_basis.set_index('date').ffill()
data_ccy_basis = data_ccy_basis[[c for c in data_ccy_basis.columns if '3M' not in c]]



data_sovcds = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_SOVCDS_historical.csv')  
data_sovcds['date'] = pd.to_datetime(data_sovcds['date'])
data_sovcds = data_sovcds.set_index('date').ffill() #.drop(['IND','GRC'],axis=1)



data_gov = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_GOV_historical.csv')  
data_gov['date'] = pd.to_datetime(data_gov['date'])
data_gov = data_gov.set_index('date').ffill()

data_vol = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_VOL_historical.csv')  
data_vol['date'] = pd.to_datetime(data_vol['date'])
data_vol = data_vol.set_index('date').ffill()



##################################################################################################################################################
### KCA logic
##################################################################################################################################################


# 1. Set up
# h - time step 
# A- transtion matrix
# H -
# q - seed multiplier for cov matrix 
# and a seed for Q

h = 1 # 1day
A=np.array([[1,h,.5*h**2], 
            [0,1,h], 
            [0,0,1]])

H=np.array([1,0,0])
q = 1
Q=q*np.eye(A.shape[0]) #cov matrix is set to be the identity matrix
R=q*np.eye(H.shape[0])

#2 create the filter 
kf=KalmanFilter(transition_matrices=A,
                observation_matrices=H,
                transition_covariance=Q) 

#d = pd.DataFrame((data_gov['USD_0Y_30Y'] - data_gov['USD_0Y_10Y']).resample('w').last().tail(600), columns=['USD_10s30s']).ffill().dropna()
d = data_gov[['USD_0Y_10Y']].resample('w').last().tail(600).ffill()

#EM estimate parameters
kf = kf.em(d)

#smoothing
kf_mean, kf_covar = kf.smooth(d)

pd.concat([pd.DataFrame(kf_mean, columns=['mean', 'momentum', 'gamma'], index=d.index),
           d], axis=1).plot(secondary_y=['momentum', 'gamma'])




##################################################################################################################################################
### pair trade logic
##################################################################################################################################################


# 1. Set up
 
# A- transition matrix Xt = Xt-1 + Vt
# H - observation matrix R1t/R2t = Xt + Wt  
# Q - transition cov matrix
# R - observation cov matrix


A=np.array([1])
Q=np.eye(A.shape[0])
H=np.array([1])
R=np.eye(H.shape[0])
#2 create the filter 

kf=KalmanFilter(transition_matrices=A,
                observation_matrices=H,
                transition_covariance=Q,
                observation_covariance=R) 

data_gov_rtns = (data_gov)[['USD_0Y_10Y', 'USD_0Y_30Y']].resample('5B').first() - (data_gov)[['USD_0Y_10Y', 'USD_0Y_30Y']].resample('5B').first().shift(1)
data_gov_rtns = data_gov_rtns.dropna()

d = pd.DataFrame((data_gov_rtns['USD_0Y_30Y']/data_gov_rtns['USD_0Y_10Y']).tail(700), columns=['USD_30sratioOn10s'])

#EM estimate parameters
kf = kf.em(d)

#smoothing and filtering
kf_smooth_mean, kf_smooth_covar = kf.smooth(d)
kf_filter_mean, kf_filter_covar = kf.filter(d)

#vals = data_gov_rtns['USD_0Y_30Y'].resample('w').last().reindex(index=d.index) - data_gov_rtns['USD_0Y_10Y'].resample('w').last().reindex(index=d.index) * pd.DataFrame(kf_filter_mean, columns=['filter_beta'], index=d.index)['filter_beta']
#vals.std()


pd.DataFrame({'+std': pd.Series(kf_filter_covar.squeeze(), index=d.index),
              'mean':pd.Series(kf_filter_mean.squeeze(), index=d.index),
              '-std': -1 * pd.Series(kf_filter_covar.squeeze(), index=d.index)}).tail(100).plot()
              
ax = pd.DataFrame({'US10s30': (data_gov['USD_0Y_30Y'] - data_gov['USD_0Y_10Y']).resample('5B').first(),
              'mean':pd.Series(kf_filter_mean.squeeze(), index=d.index)}).tail(700).plot(secondary_y=['US10s30'])

ax.axhline(y=0, color='b', linestyle='-')
ax.axhline(y=1, color='b', linestyle='-')
ax.axhline(y=-1, color='b', linestyle='-')

     
ax = pd.DataFrame({'US10s30': (data_gov['USD_0Y_30Y'] - data_gov['USD_0Y_10Y']).resample('5B').first(),
              'mean':pd.Series(kf_smooth_mean.squeeze(), index=d.index)}).tail(700).plot(secondary_y=['US10s30'])

ax.axhline(y=0, color='b', linestyle='-')
ax.axhline(y=1, color='b', linestyle='-')
ax.axhline(y=-1, color='b', linestyle='-')





d['USD_30sratioOn10s'] - pd.DataFrame(kf_filter_mean, columns=['filter_beta'], index=d.index)['filter_beta']

pd.DataFrame((data_gov['USD_0Y_30Y'] - data_gov['USD_0Y_10Y']).resample('w').last().tail(600), columns=['USD_30sratioOn10s']).ffill().dropna().plot()





pd.DataFrame(np.array([[1,2,3],[4,5,6]])).plot(kind='bar')