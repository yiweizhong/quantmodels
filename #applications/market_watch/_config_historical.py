import os, sys, inspect
import logging
import datetime
import copy
import collections
import pandas as pd

### common 
TEST_MODE = True
APP_NAME = r'market_watch'
CURRENT_PATH = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0])) 
APP_ROOT = r'c:/temp/test/' + APP_NAME
CORE_ROOT = r''
SENDER = 'YIWEI.ZHONG@ampcapital.com'
RECEIVERS = ['YIWEI.ZHONG@ampcapital.com']
CC = []
DTYPE_DATEIME64 = r'datetime64[ns]'
ODBC_STYLE_CSTR_PREFIX = r'mssql+pyodbc:///?odbc_connect=%s'
LOG_FORMAT = r'%(asctime)s - %(name)s - %(levelname)s - %(message)s'
TIMESTAMP_FMT = r'%Y-%m-%d %H:%M:%S.%f'
FI_PROD_DB_CONN_STRING = r'DRIVER={SQL Server Native Client 10.0};SERVER=frontofficesql;DATABASE=Fixed_income;Trusted_Connection=yes;Integrated Security=True; MultipleActiveResultSets=True; Connection Timeout=800'
LOG_FOLDER = ''
DEBUG_FOLDER = ''
OUTPUT_FOLDER = ''
ARCHIVE_FOLDER = ''
ARCHIVE_LOG_FOLDER = ''
ARCHIVE_DEBUG_FOLDER = ''
ARCHIVE_OUTPUT_FOLDER = ''
INPUT_FOLDER = ''
STAGING_FOLDER = ''
ARCHIVE_STAGING_FOLDER = ''

ARCHIVE_FILES = True

### app sepcific
COUNTRY_CONFIGS = None
MARKET_COMPONENTS = None


def set_components():
    global MARKET_COMPONENTS
    MARKET_COMPONENTS = {
        'IR_stirfut':None,
        'FF_stirfut':None,
        'L_stirfut':None,
        'BA_stirfut':None,
        'ED_stirfut':None
    }


    

### 
def config_path():

    _parent_path = os.path.abspath(os.path.join(CURRENT_PATH, os.pardir))
    _gparent_path = os.path.abspath(os.path.join(_parent_path, os.pardir))
    _ggparent_path = os.path.abspath(os.path.join(_gparent_path, os.pardir))

    #adding the path here
    if (TEST_MODE):        
        _core_path = _gparent_path
        if _core_path not in sys.path: sys.path.insert(0, _core_path)
    else:
        pass

    import core.dirs as dirs
    
    global INPUT_FOLDER, LOG_FOLDER, DEBUG_FOLDER, OUTPUT_FOLDER, ARCHIVE_FOLDER, ARCHIVE_LOG_FOLDER, ARCHIVE_DEBUG_FOLDER, ARCHIVE_OUTPUT_FOLDER, STAGING_FOLDER, ARCHIVE_STAGING_FOLDER

    INPUT_FOLDER = dirs.get_creat_sub_folder(folder_name='input', parent_dir=APP_ROOT) 
    LOG_FOLDER = dirs.get_creat_sub_folder(folder_name='log', parent_dir=APP_ROOT)
    DEBUG_FOLDER = dirs.get_creat_sub_folder(folder_name='debug', parent_dir=APP_ROOT)
    OUTPUT_FOLDER = dirs.get_creat_sub_folder(folder_name='output', parent_dir=APP_ROOT)
    ARCHIVE_FOLDER  = dirs.get_creat_sub_folder(folder_name='archive', parent_dir=APP_ROOT)
    ARCHIVE_LOG_FOLDER = dirs.get_creat_sub_folder(folder_name="log", parent_dir=ARCHIVE_FOLDER)
    ARCHIVE_DEBUG_FOLDER = dirs.get_creat_sub_folder(folder_name="debug", parent_dir=ARCHIVE_FOLDER)
    ARCHIVE_OUTPUT_FOLDER = dirs.get_creat_sub_folder(folder_name="Output", parent_dir=ARCHIVE_FOLDER)
    STAGING_FOLDER = dirs.get_creat_sub_folder(folder_name='staging', parent_dir=APP_ROOT)
    ARCHIVE_STAGING_FOLDER = dirs.get_creat_sub_folder(folder_name="staging", parent_dir=ARCHIVE_FOLDER)

    ###
    _log_path = os.path.join(LOG_FOLDER + '/' + APP_NAME + '_' + datetime.datetime.now().strftime("%Y%m%d_%H")+'.log')
    logging.basicConfig(filename=_log_path, level=logging.DEBUG, format = '%(asctime)s - %(levelname)s - %(message)s')
    
    logging.info("sys.path:")
    for p in sys.path: logging.info(p)
    #for p in sys.path: print p
    return

###
config_path()
set_components()

INPUT_FOLDER = STAGING_FOLDER

