import pandas as pd
import numpy as np
import os, sys, inspect
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from cycler import cycler
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations
from pandas.tseries.offsets import DateOffset


_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper
from statistics.derivative import bs as bs


from scipy import stats
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, fcluster
import scipy.cluster.hierarchy as cl
from scipy.spatial.distance import pdist, squareform



pd.set_option('display.expand_frame_repr', False)



def convert_atmf_vol_to_yield(option, vols):

    base = 1/np.sqrt(2*3.14*12)
    if option.upper() == '1M':
        return base * np.sqrt(1) * vols
    elif option.upper() == '2M':
        return base * np.sqrt(2) * vols     
    elif option.upper() == '3M':
        return base * np.sqrt(3) * vols
    elif option.upper() == '6M':
        return base * np.sqrt(6)* vols
    elif option.upper() == '1Y':
        return base * np.sqrt(12)* vols
    elif option.upper() == '2Y':
        return base * np.sqrt(24)* vols
    elif option.upper() == '3Y':
        return base * np.sqrt(36)* vols
    elif option.upper() == '4Y':
        return base * np.sqrt(48)* vols
    elif option.upper() == '5Y':
        return base * np.sqrt(60)* vols
    elif option.upper() == '6Y':
        return base * np.sqrt(72)* vols
    elif option.upper() == '7Y':
        return base * np.sqrt(84)* vols
    elif option.upper() == '8Y':
        return base * np.sqrt(96)* vols
    elif option.upper() == '9Y':
        return base * np.sqrt(108)* vols
    elif option.upper() == '10Y':
        return base * np.sqrt(120)* vols    
    
    else:
        raise ValueError("unknown optin tenor {0}".format(option))


def plot_scatter_main(ax, xs, ys, colors, cmaps, ticklabelsize=8):
    mapable = ax.scatter(xs, ys, c=colors, cmap=cmaps, marker='o', s = 9)
        
    ax.set_ylabel(ys.name, fontsize=8)
    ax.set_xlabel(xs.name, fontsize=8)
    ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
    ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
    
    for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    
    for label,tick in zip(ax.get_xticklabels(), ax.get_xticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    return mapable


def plot_scatter_endpoints(ax, xs, ys, colors, sizes, labels, markers, legend_loc=None,ncol=1, fontsize=8, lw =3):
    ds = [ax.scatter(xs[i], ys[i], c=colors[i], s=sizes[i], label=labels[i], marker=markers[i], linewidth= lw) for i in np.arange(len(xs))]
    if legend_loc is not None:
        lg =ax.legend(ds, labels, scatterpoints=1, fancybox=True, framealpha=0, loc=legend_loc, ncol=ncol, fontsize=fontsize)
        ax.add_artist(lg)
    return ds
    


def plot_scatter_fits(ax, xss, yss, label_prefixes, colors, legend_loc=None, ncol=1, fontsize=8):
    #ploting multiple fits
    pcs = []
    for (xs, ys, label_prefix, color) in zip(xss, yss, label_prefixes, colors): 
        (beta_x, beta_0), (xs, fitted_ys, residual_ys, residual_ys_std, residual_ys_p_1std, residual_ys_m_1std) = get_linear_betas(xs, ys)
        label = r'{1} = {3:.2f} + {2:.2f} * {0} with stdev {4:.2f} '.format(xs.name, ys.name, beta_x, beta_0, residual_ys_std)
        label = label_prefix + ', ' + label if label_prefix is not None else label
        pcs = pcs + ax.plot(xs, fitted_ys, lw=1, c=color, ls = '-', marker = '.', ms = 0, label=label)
        ax.plot(xs, residual_ys_p_1std, lw=0.5, c=color, ls = ':', ms = 0)
        ax.plot(xs, residual_ys_m_1std, lw=0.5, c=color, ls = ':', ms = 0)

    if legend_loc is not None:
        lgd = ax.legend(handles=pcs, loc=legend_loc, fancybox=True, framealpha=0, ncol=ncol, fontsize=fontsize)
        ax.add_artist(lgd)
    return pcs






def get_linear_betas(xs, ys):
    betas, _, _, _, _ = np.polyfit(list(xs), list(ys), deg = 1, full=True)
    (beta_x, beta_0) = betas
    #xs = df[df.columns[0]]
    #ys = df[df.columns[1]]
    #start_x = xs.min() 
    #last_x = xs.max()
    #step = (last_x - start_x)/100 
    #xs = np.arange(start_x,last_x, step)
    fitted_ys = [round(beta_0 + beta_x * x, 4) for x in list(xs)]
    residual_ys = [y - fitted_y for y, fitted_y in  zip(ys, fitted_ys)]
    residual_ys_std = np.std(residual_ys, ddof=1) 
    residual_ys_p_1std =  [fitted_y + residual_ys_std for fitted_y in  fitted_ys]
    residual_ys_m_1std =  [fitted_y - residual_ys_std for fitted_y in  fitted_ys]
    
    return betas, (xs, fitted_ys, residual_ys, residual_ys_std, residual_ys_p_1std, residual_ys_m_1std)
   


def bootstrap_ifs_fwd(data):
    nc =  { c: int( c[4:].replace('_SWIT','').replace('Y','') ) for c in data.columns}
    data = data.rename(columns=nc)
    a = data.columns.tolist()[1:]
    b = data.columns.tolist()[:-1]
    fwd = {}
    fwd['{0}Y'.format(data.columns[0])] = data[data.columns[0]]
    for (ca,cb) in zip(a, b):
        term = ca - cb
        forward = cb
        cca = (1 + data[ca]/100).pow(ca)
        ccb = (1 + data[cb]/100).pow(cb)
        fwd['{0}Y{1}Y'.format(forward, term)] = np.log(cca/ccb)/term * 100
    fwd = pd.DataFrame(fwd)
    return fwd


def plot_main(ax, xs, ys, xlegend, ylegend, colors, cmaps, ticklabelsize=8, mirror_limit_x = False, mirror_limit_y = False):
    
    mapable = ax.scatter(xs, ys, c=colors, cmap=cmaps, marker='o', s = 6)
        
    ax.set_ylabel(ys.name, fontsize=8)
    ax.set_xlabel(xs.name, fontsize=8)
    ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
    ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
    
    for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    
    #for label,tick in zip(ax.get_xticklabels(), ax.get_xticks()):
    #    label.set_fontsize(ticklabelsize)
    #    if tick < 0:
    #        label.set_color("red")
    #    else:
    #        label.set_color("black")
    
    ymin, ymax = ax.get_ylim()
    xmin, xmax = ax.get_xlim()    
    
    ymax_abs = max([abs(ymin), abs(ymax)])
    xmax_abs = max([abs(xmin), abs(xmax)])
    
    xymax = max([abs(ymin), abs(ymax), abs(xmin), abs(xmax)])
    
    if mirror_limit_x:
        ax.set_xlim(-1 * xmax_abs, xmax_abs)
    if mirror_limit_y:
        ax.set_ylim(-1 * ymax_abs, ymax_abs)
    
    # Move left y-axis and bottim x-axis to centre, passing through (0,0)
    # ax.spines['left'].set_position('center')
    
    
    #ax.spines['bottom'].set_position('center')
    # Eliminate upper and right axes
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    # Show ticks in the left and lower axes only
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    
    ax.xaxis.set_major_locator(MultipleLocator(0.5))
    ax.yaxis.set_major_locator(MultipleLocator(1))
    ax.xaxis.grid(True, which='major')
    #ax.xaxis.grid(True, which='minor')
    ax.yaxis.grid(True, which='major')
    
    for label in ax.get_xticklabels(minor =False):
        label.set_fontsize(7)

    for label in ax.get_yticklabels(minor =False):
        label.set_fontsize(7)
        
    ax.set_ylabel(ylegend, fontsize=9)
    ax.yaxis.set_label_coords(0, 0.12)

    ax.set_xlabel(xlegend, fontsize=9)
    ax.xaxis.set_label_coords(0.85, 0.045)
    
    # end points
    start_x = xs.loc[xs.first_valid_index()]
    start_y = ys.loc[ys.first_valid_index()]
    start_date = xs.first_valid_index().strftime(r'%Y-%m-%d')
    
    ds1 = ax.scatter(start_x, start_y, c='#f5d9c8', s=90, alpha=0.5, label=start_date, edgecolors='b')
    
    last_x = xs.loc[xs.last_valid_index()]
    last_y = ys.loc[ys.last_valid_index()]
    last_date = xs.last_valid_index().strftime(r'%Y-%m-%d')

    ds2 = ax.scatter(last_x, last_y, c='darkred', s=90, alpha=1, label=last_date)
    
    
    ax.legend([ds1, ds2], [start_date, last_date], scatterpoints=1, fancybox=True, framealpha=0, loc=1, ncol=1, fontsize=9)
    
    return mapable


def plot_multi_lines(data, cols=None, spacing=.1, **kwargs):

    from pandas import plotting

    # Get default color style from pandas - can be changed to any other color list
    if cols is None: cols = data.columns
    if len(cols) == 0: return
    colors = getattr(getattr(plotting, '_matplotlib').style, '_get_standard_colors')(num_colors=len(cols))

    # First axis
    ax = data.loc[:, cols[0]].plot(label=cols[0], color=colors[0], **kwargs)
    ax.set_ylabel(ylabel=cols[0])
    lines, labels = ax.get_legend_handles_labels()

    for n in range(1, len(cols)):
        # Multiple y-axes
        ax_new = ax.twinx()
        ax_new.spines['right'].set_position(('axes', 1 + spacing * (n - 1)))
        data.loc[:, cols[n]].plot(ax=ax_new, label=cols[n], color=colors[n % len(colors)], **kwargs)
        ax_new.set_ylabel(ylabel=cols[n])

        # Proper legend position
        line, label = ax_new.get_legend_handles_labels()
        lines += line
        labels += label

    ax.legend(lines, labels, loc=0)
    return ax


def rolling_apply(df, window_size, func, min_window_size=None):
    if min_window_size is None:
        min_window_size = window_size
    result = {}
    for i in range(1, len(df)+1):
        sub_df = df.iloc[max(i - window_size, 0):i, :] #I edited here

        if len(sub_df) >= min_window_size:            
            idx = sub_df.index[-1]
            result[idx] = func(sub_df)
    return result


def get_flattened_cov(df):
    covmatx = df.cov()
    nodes = set([comb for comb in combinations(covmatx.columns.tolist() + covmatx.columns.tolist(), 2)])
    result = pd.Series({col + '_vs_' + row : covmatx[col][row]  for (col, row) in nodes})
    return result


def get_flattened_corr(df):
    covmatx = df.corr()
    nodes = set([comb for comb in combinations(covmatx.columns.tolist() + covmatx.columns.tolist(), 2)])
    result = pd.Series({col + '_vs_' + row : covmatx[col][row]  for (col, row) in nodes})
    return result


def func(df): 
    return pd.DataFrame({
                      'Now':df.iloc[-2:].mean(),
                      #'2020-09-11':df[(df.index >= '2020-09-08') & (df.index <= '2020-09-12')].mean(),
                      #'2020-09-04':df[(df.index >= '2020-09-02') & (df.index <= '2020-09-05')].mean(),
                      #'2020-08-12':df[(df.index >= '2020-08-10') & (df.index <= '2020-08-13')].mean(),
                      #'Pre speech':df.iloc[-3],
                      '7d avg':df.iloc[-7:].mean(), 
                      '7d avg 7d ago':df.iloc[-14:-7].mean(), 
                      '7d avg 14d ago':df.iloc[-21:-14].mean(), 
                      '7d avg 21d ago':df.iloc[-28:-21].mean(), 
                      #'7d avg 28d ago':df.iloc[-35:-28].mean(), 
                      #'avg last 3 years':df.iloc[-97:-90].mean(),
                      #'7d avg 180 day ago':df.iloc[-187:-180].mean(),
                      #'7d avg 360 day ago':df.iloc[-367:-360].mean(),
                      #'2010-2011 avg': df[(df.index > '2010-01-01') & (df.index < '2011-12-31')].mean(),
                      #'2010-Oct-2011-Jul': df[(df.index > '2010-10-01') & (df.index < '2011-07-01')].mean(),
                      #'2012-2013 avg': df[(df.index > '2012-01-01') & (df.index < '2013-12-31')].mean(),
                      #'2016-2019 avg': df[(df.index > '2016-01-01') & (df.index < '2019-12-31')].mean(),
                      #'2016-08': df[(df.index > '2016-08-01') & (df.index < '2016-09-01')].mean(),
                      #'2016-09': df[(df.index > '2016-09-01') & (df.index < '2016-10-01')].mean(),
                      #'2016-10': df[(df.index > '2016-10-01') & (df.index < '2016-11-01')].mean()
                      }) 


def vol_snapshot(df,n=20):
    
    rtn = df - df.shift(1)    
    
    return pd.DataFrame({
                      'Now':rtn.iloc[-n:].std(),
                      '1m ago':rtn.iloc[-2*n:-n].std(), 
                      '2m ago':rtn.iloc[-3*n:-2*n].std(), 
                      '3m ago':rtn.iloc[-4*n:-3*n].std()                      
                      }) * np.sqrt(252)







rundate = '2021-03-12'



# data_fut10_pos = pd.read_excel('C:/Dev/Models/QuantModels/#applications/market_watch/data/FUT10_POS_' + rundate + '.xlsx', sheet_name='Data Table')  
# data_fut10_pos['Date'] = pd.to_datetime(data_fut10_pos['Date'])
# data_fut10_pos = data_fut10_pos.set_index('Date').ffill()



data_usd_all = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + '_USD_historical.csv')  
data_usd_all['date'] = pd.to_datetime(data_usd_all['date'])
data_usd_all = data_usd_all.set_index('date').ffill()



data_econ = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + '_ECON_historical.csv')  
data_econ['date'] = pd.to_datetime(data_econ['date'])
data_econ = data_econ.set_index('date').ffill()


data = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + '_BEIRR_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()


data_swap = pd.read_csv('C:/Temp/test/market_watch/staging/'  + rundate +  '_SWAP_ALL_historical.csv')  
data_swap['date'] = pd.to_datetime(data_swap['date'])
data_swap = data_swap.set_index('date').ffill()

data_swap2 = pd.read_csv('C:/Temp/test/market_watch/staging/'  + rundate +  '_SWAP_ALL_V2_historical.csv')  
data_swap2['date'] = pd.to_datetime(data_swap2['date'])
data_swap2 = data_swap2.set_index('date').ffill()


# data_bcs = pd.read_csv(r'C:/Dev/Models/QuantModels/#test/BCS_2020-07-29.csv')  
# data_bcs['date'] = pd.to_datetime(data_bcs['date'])
# data_bcs = data_bcs.set_index('date').ffill()
# data_bcs_rtn = (data_bcs - data_bcs.shift(1))/100

data_ifs = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_SWIT_historical.csv')  
data_ifs['date'] = pd.to_datetime(data_ifs['date'])
data_ifs = data_ifs.set_index('date').ffill()


data_ccy_basis = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_BS_ALL_historical.csv')  
data_ccy_basis['date'] = pd.to_datetime(data_ccy_basis['date'])
data_ccy_basis = data_ccy_basis.set_index('date').ffill()
data_ccy_basis = data_ccy_basis[[c for c in data_ccy_basis.columns if '3M' not in c]]



data_sovcds = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_SOVCDS_historical.csv')  
data_sovcds['date'] = pd.to_datetime(data_sovcds['date'])
data_sovcds = data_sovcds.set_index('date').ffill() #.drop(['IND','GRC'],axis=1)



data_gov = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_GOV_historical.csv')  
data_gov['date'] = pd.to_datetime(data_gov['date'])
data_gov = data_gov.set_index('date').ffill()

data_vol = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_VOL_historical.csv')  
data_vol['date'] = pd.to_datetime(data_vol['date'])
data_vol = data_vol.set_index('date').ffill()


data_ivol_in_yields_daily = pd.DataFrame({c: convert_atmf_vol_to_yield(c.split('_')[1], data_vol[c]) for c in data_vol.columns})

data_rvol_swap_1m_daily = (data_swap - data_swap.shift(1)).rolling(20).std() * 100
data_rvol_swap_3m_daily = (data_swap - data_swap.shift(1)).rolling(60).std() * 100

data_rvol_swap2_1m_daily = (data_swap2 - data_swap2.shift(1)).rolling(20).std() * 100
data_rvol_swap2_3m_daily = (data_swap2 - data_swap2.shift(1)).rolling(60).std() * 100




ifs_c = list(set([c[:3] for c in data_ifs.columns]))
ifs_cc = {cnty:[c for c in data_ifs.columns if cnty in c] for cnty in ifs_c}

data_fwd_ifs = {c: bootstrap_ifs_fwd(data_ifs[cc]) for (c,cc) in ifs_cc.items()}


##################
# needs to be changed
##################

US_cols =  [c for c in data.columns if 'US' in c]
US_cols.sort()
data_usd = data[US_cols].dropna()

data_usd['USD_NM_02Y'] = data_usd['USD_BE_02Y'] + data_usd['USD_RR_02Y'] 
data_usd['USD_NM_05Y'] = data_usd['USD_BE_05Y'] + data_usd['USD_RR_05Y']
data_usd['USD_NM_10Y'] = data_usd['USD_BE_10Y'] + data_usd['USD_RR_10Y']
data_usd['USD_NM_30Y'] = data_usd['USD_BE_30Y'] + data_usd['USD_RR_30Y']



data_usd['USD_NM_210'] = data_usd['USD_NM_10Y'] - data_usd['USD_NM_02Y']
data_usd['USD_NM_510'] = data_usd['USD_NM_10Y'] - data_usd['USD_NM_05Y']
data_usd['USD_NM_530'] = data_usd['USD_NM_30Y'] - data_usd['USD_NM_05Y']
data_usd['USD_NM_1030'] = data_usd['USD_NM_30Y'] - data_usd['USD_NM_10Y']
data_usd['USD_NM_020510'] = data_usd['USD_NM_10Y'] - 2 * data_usd['USD_NM_05Y'] + data_usd['USD_NM_02Y']
data_usd['USD_NM_051030'] = data_usd['USD_NM_30Y'] - 2 * data_usd['USD_NM_10Y'] + data_usd['USD_NM_05Y']


data_usd['USD_RR_210'] = data_usd['USD_RR_10Y'] - data_usd['USD_RR_02Y']
data_usd['USD_RR_510'] = data_usd['USD_RR_10Y'] - data_usd['USD_RR_05Y']
data_usd['USD_RR_530'] = data_usd['USD_RR_30Y'] - data_usd['USD_RR_05Y']
data_usd['USD_RR_230'] = data_usd['USD_RR_30Y'] - data_usd['USD_RR_02Y']
data_usd['USD_RR_1030'] = data_usd['USD_RR_30Y'] - data_usd['USD_RR_10Y']
data_usd['USD_RR_020510'] = data_usd['USD_RR_10Y'] - 2 * data_usd['USD_RR_05Y'] + data_usd['USD_RR_02Y']
data_usd['USD_RR_051030'] = data_usd['USD_RR_30Y'] - 2 * data_usd['USD_RR_10Y'] + data_usd['USD_RR_05Y']

data_usd['USD_BE_210'] = data_usd['USD_BE_10Y'] - data_usd['USD_BE_02Y']
data_usd['USD_BE_510'] = data_usd['USD_BE_10Y'] - data_usd['USD_BE_05Y']
data_usd['USD_BE_530'] = data_usd['USD_BE_30Y'] - data_usd['USD_BE_05Y']
data_usd['USD_BE_230'] = data_usd['USD_BE_30Y'] - data_usd['USD_BE_02Y']
data_usd['USD_BE_1030'] = data_usd['USD_BE_30Y'] - data_usd['USD_BE_10Y']
data_usd['USD_BE_020510'] = data_usd['USD_BE_10Y'] - 2 * data_usd['USD_BE_05Y'] + data_usd['USD_BE_02Y']
data_usd['USD_BE_051030'] = data_usd['USD_BE_30Y'] - 2 * data_usd['USD_BE_10Y'] + data_usd['USD_BE_05Y']

data_usd['USD_BE2NM30'] = data_usd['USD_NM_30Y'] - data_usd['USD_BE_02Y']

data_usd_rtn = data_usd - data_usd.shift(1)


##################
# needs to be changed
##################




#test11 ivol 

#### AUD
aud_ivol_cols = [c for c in data_ivol_in_yields_daily.columns if 'AUD' in c and ('_3M_' in c  or '_6M_' in c or '_9M_' in c  or '_1Y_' in c  or '_2Y_' in c or '_3Y_' in c)] 
aud_ivol_in_yields_daily = data_ivol_in_yields_daily[aud_ivol_cols].dropna(how='all', axis=1).ffill()
pca.PCA(aud_ivol_in_yields_daily.iloc[-250:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
####



usd_ivol_cols = [c for c in data_ivol_in_yields_daily.columns if 'USD' in c and ('_3M_' in c  or '_6m_' in c or '_9M_' in c  or '_1Y_' in c  or '_2Y_' in c or '_3Y_' in c)] 
usd_ivol_in_yields_daily = data_ivol_in_yields_daily[usd_ivol_cols].dropna(how='all', axis=1).ffill()
pca.PCA(usd_ivol_in_yields_daily.iloc[-2500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)






#test13 USD only

# swap
# ifs


usd_spot_swap_test13 = data_swap[['USD_0Y_1Y',
                         'USD_0Y_2Y',
                         'USD_0Y_3Y',
                         #'USD_0Y_4Y',
                         'USD_0Y_5Y',
                         #'USD_0Y_6Y',
                         #'USD_0Y_7Y',
                         #'USD_0Y_8Y',
                         #'USD_0Y_9Y',
                         'USD_0Y_10Y',
                         'USD_0Y_20Y',
                         'USD_0Y_30Y']].rename(lambda c: c.replace("_0Y_","_"), axis="columns")


usd_spot_gov_test13 = data_gov[[ 'USD_0Y_1Y',
 'USD_0Y_2Y',
 'USD_0Y_3Y',
 'USD_0Y_5Y',
 #'USD_0Y_7Y',
 'USD_0Y_10Y',
 #'USD_0Y_20Y',
 'USD_0Y_30Y']].rename(lambda c: c.replace("_0Y_","_"), axis="columns")




usd_spot_ifs_test13 = data_ifs[['USD_1Y_SWIT',
                         'USD_2Y_SWIT',
                         'USD_3Y_SWIT',
                         #'USD_4Y_SWIT',
                         'USD_5Y_SWIT',
                         #'USD_6Y_SWIT',
                         #'USD_7Y_SWIT',
                         #'USD_8Y_SWIT',
                         #'USD_9Y_SWIT',
                         'USD_10Y_SWIT',
                         'USD_20Y_SWIT',
                         'USD_30Y_SWIT']]




# eur_spot_ifs_test13 = data_ifs[['EUR_1Y_SWIT',
#                          'EUR_2Y_SWIT',
#                          'EUR_3Y_SWIT',
#                          #'EUR_4Y_SWIT',
#                          'EUR_5Y_SWIT',
#                          #'EUR_6Y_SWIT',
#                          #'EUR_7Y_SWIT',
#                          #'EUR_8Y_SWIT',
#                          #'EUR_9Y_SWIT',
#                          'EUR_10Y_SWIT',
#                          'EUR_20Y_SWIT',
#                          'EUR_30Y_SWIT']]




usd_data_rvol_daily_1m_test13 = data_rvol_swap_1m_daily[[
 #'USD_0Y_3M',
 'USD_0Y_1Y',
 'USD_0Y_2Y',
 'USD_0Y_3Y',
 #'USD_0Y_4Y',
 'USD_0Y_5Y',
 #'USD_0Y_6Y',
 #'USD_0Y_7Y',
 #'USD_0Y_8Y',
 #'USD_0Y_9Y',
 'USD_0Y_10Y',
 'USD_0Y_20Y',
 'USD_0Y_30Y']].rename(lambda c: c.replace('_0Y',''), axis=1) #* np.sqrt(256)



usd_data_rvol_daily_3m_test13 = data_rvol_swap_3m_daily[[
 #'USD_0Y_3M',
 'USD_0Y_1Y',
 'USD_0Y_2Y',
 'USD_0Y_3Y',
 #'USD_0Y_4Y',
 'USD_0Y_5Y',
 #'USD_0Y_6Y',
 #'USD_0Y_7Y',
 #'USD_0Y_8Y',
 #'USD_0Y_9Y',
 'USD_0Y_10Y',
 'USD_0Y_20Y',
 'USD_0Y_30Y']].rename(lambda c: c.replace('_0Y',''), axis=1)  #* np.sqrt(256)




usd_data_ivol_1m_in_yields_daily_test13 = data_ivol_in_yields_daily[[
 'USD_1M_1Y',
 'USD_1M_2Y',
 'USD_1M_3Y',
 'USD_1M_5Y',
 'USD_1M_10Y',
 'USD_1M_20Y',
 'USD_1M_30Y']].rename(lambda c: c.replace('_1M',''), axis=1)


usd_data_ivol_3m_in_yields_daily_test13 = data_ivol_in_yields_daily[[
 'USD_3M_1Y',
 'USD_3M_2Y',
 'USD_3M_3Y',
 'USD_3M_5Y',
 'USD_3M_10Y',
 'USD_3M_20Y',
 'USD_3M_30Y']].rename(lambda c: c.replace('_3M',''), axis=1)



usd_data_ivol_1y_in_yields_daily_test13 = data_ivol_in_yields_daily[[
 'USD_1Y_1Y',
 'USD_1Y_2Y',
 'USD_1Y_3Y',
 'USD_1Y_5Y',
 'USD_1Y_10Y',
 'USD_1Y_20Y',
 'USD_1Y_30Y']].rename(lambda c: c.replace('_1Y_',''), axis=1)




usd_data_ivol_rvol_1m_test13 = usd_data_ivol_1m_in_yields_daily_test13 / usd_data_rvol_daily_1m_test13

usd_data_ivol_rvol_3m_test13 = usd_data_ivol_3m_in_yields_daily_test13 / usd_data_rvol_daily_3m_test13




#inflation swap date seems to be always 1day lag. check

# use swap as the nominal rate
usd_spot_rr_test13 = usd_spot_swap_test13 - usd_spot_ifs_test13.rename(lambda c: c.replace('_SWIT',''), axis=1).reindex(usd_spot_swap_test13.index).ffill()


# use gov as the nominal rate
usd_spot_rr_test13 = usd_spot_gov_test13 - usd_spot_ifs_test13.rename(lambda c: c.replace('_SWIT',''), axis=1).reindex(usd_spot_gov_test13.index).ffill()


#
usd_spot_ss_test13 = usd_spot_swap_test13.reindex(usd_spot_gov_test13.index).ffill() - usd_spot_gov_test13 


usd_fwd_swap_test13 = bootstrap_ifs_fwd(usd_spot_swap_test13)
usd_fwd_gov_test13 = bootstrap_ifs_fwd(usd_spot_gov_test13)
usd_fwd_ifs_test13 =  bootstrap_ifs_fwd(usd_spot_ifs_test13)
usd_fwd_rr_test13 =  bootstrap_ifs_fwd(usd_spot_rr_test13)
usd_fwd_ss_test13 =  usd_fwd_swap_test13.reindex(usd_fwd_gov_test13.index).ffill() - usd_fwd_gov_test13


#eur_fwd_ifs_test13 =  bootstrap_ifs_fwd(eur_spot_ifs_test13)


### Output charts

#ivol/rvol

f0 = plt.figure(figsize = (15, 10))
gs0 = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
f0ax0 = plt.subplot(gs0[0])
f0ax1 = plt.subplot(gs0[1])
f0ax2 = plt.subplot(gs0[2])
f0ax3 = plt.subplot(gs0[3])


func(usd_data_ivol_rvol_1m_test13).rename(lambda c: c.split('_')[1]).plot(ax=f0ax0)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f0ax0.set_title('ivol/1 stdev ratio swap curves')


usd_data_ivol_rvol_1m_test13.iloc[-100:].plot(ax=f0ax1)
f0ax1.set_title('ivol/1 stdev ratio swap curves')

usd_data_ivol_rvol_1m_test13.iloc[-2000:-1000].plot(ax=f0ax2)
f0ax2.set_title('ivol/1 stdev ratio swap curves')

usd_data_ivol_rvol_1m_test13.iloc[-3000:-2000].plot(ax=f0ax3)
f0ax3.set_title('ivol/1 stdev ratio swap curves')


pca.PCA(usd_data_ivol_rvol_1m_test13.iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_data_ivol_rvol_1m_test13.iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_data_ivol_rvol_1m_test13.iloc[-3000:], rates_pca = True, momentum_size = None).plot_loadings(n=3)

usd_data_ivol_rvol_1m_long_cycle_pca_test13 = pca.PCA(usd_data_ivol_rvol_1m_test13[usd_data_ivol_rvol_1m_test13.index > '2008-01-01'], rates_pca = True, momentum_size = None)

#pick pc1 and pc2 as the input for market regime analysis

usd_rvol_1m_long_cycle_pcs_test13 = usd_data_ivol_rvol_1m_long_cycle_pca_test13.pcs[['PC1', 'PC2']].rename(lambda x: "vol_ratio_" + x, axis=1)


win = 40
data_rtns_d_1m_rolling_corrs = pd.DataFrame(rolling_apply(usd_data_ivol_rvol_1m_long_cycle_pca_test13.pcs[['PC1', 'PC2']], win, get_flattened_corr)).transpose()
regime_line1_test13 = (data_rtns_d_1m_rolling_corrs['PC1_vs_PC2'] * np.sign(usd_data_ivol_rvol_1m_long_cycle_pca_test13.pcs['PC1'].ewm(win).mean())).ewm(win).mean()


win = 20
data_rtns_d_1m_rolling_corrs = pd.DataFrame(rolling_apply(usd_data_ivol_rvol_1m_long_cycle_pca_test13.pcs[['PC1', 'PC2']], win, get_flattened_corr)).transpose()
regime_line2_test13 = (data_rtns_d_1m_rolling_corrs['PC1_vs_PC2'] * np.sign(usd_data_ivol_rvol_1m_long_cycle_pca_test13.pcs['PC1'].ewm(win).mean())).ewm(win).mean()

regime_test13 = pd.DataFrame({'1M implied': regime_line2_test13, '3M implied': regime_line1_test13})

regime_test13.plot(title='US interest rate trading regime')











############# clustering



data_ivol_in_yields_daily = pd.DataFrame({c: convert_atmf_vol_to_yield(c.split('_')[1], data_vol[c]) for c in data_vol.columns})

data_rvol_swap_1m_daily = (data_swap - data_swap.shift(1)).rolling(20).std() * 100



usd_spot_ifs_test13 = data_ifs[['USD_1Y_SWIT',
                         'USD_2Y_SWIT',
                         'USD_3Y_SWIT',
                         #'USD_4Y_SWIT',
                         'USD_5Y_SWIT',
                         #'USD_6Y_SWIT',
                         #'USD_7Y_SWIT',
                         #'USD_8Y_SWIT',
                         #'USD_9Y_SWIT',
                         'USD_10Y_SWIT',
                         'USD_20Y_SWIT',
                         'USD_30Y_SWIT']]



usd_data_ivol_1m_in_yields_daily_test13 = data_ivol_in_yields_daily[[
             'USD_1M_1Y',
             'USD_1M_2Y',
             'USD_1M_3Y',
             'USD_1M_5Y',
             'USD_1M_10Y',
             'USD_1M_20Y',
             'USD_1M_30Y']].rename(lambda c: c.replace('_1M',''), axis=1)



usd_data_rvol_daily_1m_test13 = data_rvol_swap_1m_daily[[
             #'USD_0Y_3M',
             'USD_0Y_1Y',
             'USD_0Y_2Y',
             'USD_0Y_3Y',
             #'USD_0Y_4Y',
             'USD_0Y_5Y',
             #'USD_0Y_6Y',
             #'USD_0Y_7Y',
             #'USD_0Y_8Y',
             #'USD_0Y_9Y',
             'USD_0Y_10Y',
             'USD_0Y_20Y',
             'USD_0Y_30Y']].rename(lambda c: c.replace('_0Y',''), axis=1) #* np.sqrt(256)




usd_data_ivol_rvol_1m_test13 = usd_data_ivol_1m_in_yields_daily_test13 / usd_data_rvol_daily_1m_test13



usd_spot_gov_test13 = data_gov[[ 'USD_0Y_1Y',
             'USD_0Y_2Y',
             'USD_0Y_3Y',
             'USD_0Y_5Y',
             #'USD_0Y_7Y',
             'USD_0Y_10Y',
             #'USD_0Y_20Y',
             'USD_0Y_30Y']].rename(lambda c: c.replace("_0Y_","_"), axis="columns")




def reformat_labelled_data(labelled_data, translation):
    labels = labelled_data[labelled_data.columns[1]].unique()
    labels.sort()
    output = {}
    for l in labels:
        output["{0}".format(translation[l])] = labelled_data[labelled_data.columns[0]][labelled_data[labelled_data.columns[1]] == l].reindex(labelled_data.index)
    return pd.DataFrame(output)


regime_training_start_date = '2008-01-01'
regime_training_end_date = '2021-02-10'


usd_ivol_rvol_1m_long_cycle_training_pca = pca.PCA(usd_data_ivol_rvol_1m_test13[(usd_data_ivol_rvol_1m_test13.index > regime_training_start_date) & (usd_data_ivol_rvol_1m_test13.index <= regime_training_end_date)], rates_pca = True, momentum_size = None)
usd_ifs_long_cycle_training_pca = pca.PCA(usd_spot_ifs_test13[(usd_spot_ifs_test13.index > regime_training_start_date) & (usd_spot_ifs_test13.index <= regime_training_end_date) ], rates_pca = True, momentum_size = None)
usd_ivol_1m_in_yields_training_pca = pca.PCA(usd_data_ivol_1m_in_yields_daily_test13[(usd_data_ivol_1m_in_yields_daily_test13.index > regime_training_start_date) & (usd_data_ivol_1m_in_yields_daily_test13.index <= regime_training_end_date)], 
                                                                                      rates_pca = True, momentum_size = None)


usd_data_ivol_rvol_1m_long_cycle_pca_test13 = pca.PCA(usd_data_ivol_rvol_1m_test13[usd_data_ivol_rvol_1m_test13.index > regime_training_start_date], rates_pca = True, momentum_size = None)
usd_spot_ifs_long_cycle_pca_test13 = pca.PCA(usd_spot_ifs_test13[usd_spot_ifs_test13.index > regime_training_start_date], rates_pca = True, momentum_size = None)
usd_data_ivol_1m_in_yields_daily_pca_test13 = pca.PCA(usd_data_ivol_1m_in_yields_daily_test13[usd_data_ivol_1m_in_yields_daily_test13.index > regime_training_start_date], rates_pca = True, momentum_size = None)


usd_data_ivol_rvol_1m_long_cycle_pcs = usd_data_ivol_rvol_1m_long_cycle_pca_test13.pcs.copy()
usd_spot_ifs_long_cycle_pcs = usd_spot_ifs_long_cycle_pca_test13.pcs.copy()
usd_data_ivol_1m_in_yields_daily_pcs = usd_data_ivol_1m_in_yields_daily_pca_test13.pcs.copy()


usd_data_ivol_rvol_1m_long_cycle_pcs.update(usd_ivol_rvol_1m_long_cycle_training_pca.pcs)
usd_spot_ifs_long_cycle_pcs.update(usd_ifs_long_cycle_training_pca.pcs)
usd_data_ivol_1m_in_yields_daily_pcs.update(usd_ivol_1m_in_yields_training_pca.pcs)


combined_pcs_predictors = pd.concat([usd_spot_ifs_long_cycle_pcs[['PC1','PC2']], 
                                     usd_data_ivol_rvol_1m_long_cycle_pcs[['PC1','PC2']],                                     
                                     usd_data_ivol_1m_in_yields_daily_pcs[['PC2']]/2,
                                     ], axis=1).ffill()


combined_pcs_predictors_linkage_matrix = linkage(combined_pcs_predictors, method='ward', optimal_ordering=False)
regime_labels = pd.Series(data=fcluster(combined_pcs_predictors_linkage_matrix, 4, criterion='maxclust'), index=combined_pcs_predictors.index)


clustered_data = {r: combined_pcs_predictors[regime_labels == r] for r in np.arange(4) + 1}
clustered_sse = {r: np.sum(np.square(pdist(cd) - np.mean(pdist(cd)))) for r, cd in clustered_data.items()}



df = combined_pcs_predictors #['2021-03-12':'2021-03-12']
clustered_data
sse_df_absolute = {}
sse_df_prob = {}
for index, row in df.iterrows():
    sse = {}
    for cd_id, cd in clustered_data.items():        
        cd_vals = np.vstack([cd.values, row.values])
        cd_dist = pdist(cd_vals)
        m_cd_dist = np.mean(cd_dist)
        sse1 = np.sum(np.square(cd_dist - m_cd_dist))
        sse[cd_id] = 0 if (sse1 - clustered_sse[cd_id]) < 0 else (sse1 - clustered_sse[cd_id]) / clustered_sse[cd_id]  
        #print(cd_id)
        #print(sse1)
        #print(sse[cd_id])        
    s = pd.Series(sse)
    s = (s.sum() - s) / s.sum()

    sse_df_absolute[index] = s
    #reweight
    x  = s / s.sum()
    #xx = (x - x.min()).apply(lambda i: np.nan if i == 0 else i).dropna() # take out the min
    #xxx = ((xx-xx.mean())/xx.std()).apply(lambda i:abs(i)) / ((xx-xx.mean())/xx.std()).apply(lambda i:abs(i)).sum() * (1-x.min()) #reweight zscore into % then account for the min
    #xxxx = x.copy()
    #xxxx.update(xxx)
    
    sse_df_prob[index] = x 
    #print(sse_df_absolute[index])
    #print(sse_df_prob[index])
    #print(index) 
    
    
sse_df_absolute = pd.DataFrame(sse_df_absolute).transpose()
sse_df_prob = pd.DataFrame(sse_df_prob).transpose()

sse_df_prob.plot.area(stacked=True)





translation = {1:'Late Cycle', 2:'Recession', 4: 'Reflation', 3: 'Mid Cycle'}
regime_colors = {'Late Cycle':'tab:blue', 'Recession': 'tab:orange', 'Reflation':'tab:green', 'Mid Cycle':'tab:red'}

#translation = {1:1, 2:2, 4:4, 3: 3}
#regime_colors1 = {'2':'tab:blue', '1': 'tab:orange', '3':'tab:green', '4':'tab:red'}
#regime_colors1 = {'4':'tab:blue', '1': 'tab:orange', '2':'tab:green', '3':'tab:red'}

labelled_us10 =pd.DataFrame({"US10":usd_spot_gov_test13['USD_10Y'].reindex(regime_labels.index),"Regime":regime_labels})
reformat_labelled_data(labelled_us10, translation).plot(color=regime_colors, title = 'Clustered with inf, i/r vol and ivol components')


labelled_us10s30s =pd.DataFrame({"US10S30":(usd_spot_gov_test13['USD_30Y'] - usd_spot_gov_test13['USD_10Y']).reindex(regime_labels.index),"Regime":regime_labels})
reformat_labelled_data(labelled_us10s30s, translation).plot()





regime_labels_backup = regime_labels










