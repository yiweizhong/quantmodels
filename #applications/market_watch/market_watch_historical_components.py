from __future__ import division
import _config as cfg
import logging
import os, sys, inspect
import time, datetime
import pandas as pd
from pandas.tseries.offsets import *
import numpy as np
from scipy import stats
from collections import namedtuple

from core import tools
from core import returns
import core.dirs as dirs



def calc_beta(xs, ydf, name, n=30):
    x_name = xs.name
    y_name = ydf.name

    result = {}

    for col in ydf.columns:
        ys = ydf[col]
        gradient, intercept, r_value, p_value, std_err = stats.linregress(xs.dropna()[-n:],ys.dropna()[-n:])
        rsqrd = r_value**2
        #logging.debug("{0} = {2} {1} + {3}, stder {4}, rsquared {5}". format(col, x_name, gradient, intercept, std_err, rsqrd))
        s = pd.Series({
            'beta': gradient,
            'intercept':intercept,
            'rsqrd': rsqrd,
            'std_err':std_err
        })
        s.name = col
        result[col] = s
    result = pd.DataFrame(result)
    result['x'] = x_name
    result = result[['x'] + ydf.columns.tolist()]
    result.name = name
    return result 
        

def unpivot(df, name = None, reverse_row = False):
    idx_name = df.index.name
    df_name = df.name if hasattr(df, 'name') else 'data'
    df_name = name if (name) else df_name #override
    df = df.sort_index(ascending= not reverse_row).reset_index().reset_index().melt(id_vars =['index', idx_name], var_name = 'security')
    df['data_type'] = df_name # add another level of filter
    df.name = df_name
    return df

def pivot(df, cols=['index','data_type'], name = None):
    df_name = df.name if hasattr(df, 'name') else 'data'
    df_name = name if (name) else df_name #override
    pvt_df = None
    for col in cols:
        if col in df.columns:
            pvt_df = df.drop(col, axis=1)
    pvt_df = pd.pivot_table(df,values='value', columns='security', index='date', aggfunc=np.mean)
    pvt_df.name = df_name
    return pvt_df

def px_to_yield(df,name,unpivot_result):
    result = (100 - df)
    result.name = name
    
    if (unpivot_result):
        return unpivot(result)
    else:
        return result


def ndays_move(df, n, name,unpivot_result):
    result = (df - df.shift(n)).reindex(df.index)
    result.name = name
    
    if (unpivot_result):
        return unpivot(result)
    else:
        return result


def weekly_move(df,name,unpivot_result):
    result =  df.resample('W', closed='right', label='left').sum()    
    result.name = name
    
    if (unpivot_result):
        return unpivot(result)
    else:
        return result

def monthly_move(df,name,unpivot_result):
    result = df.resample('BMS', closed='right').sum()
    result.name = name
    
    if (unpivot_result):
        return unpivot(result)
    else:
        return result    

def move_since(df,name,unpivot_result):
    result = (df.iloc[-1] - df).iloc[:-1]
    result.name = name
    
    if (unpivot_result):
        return unpivot(result)
    else:
        return result

def make_spread(df, n, name, unpivot_result):
    cols = {}
    col_names = []
    for i in np.arange(n, len(df.columns.tolist())):
        s = df.iloc[:,i] - df.iloc[:,i-n]
        s.name = "{}{}".format(df.iloc[:,i].name, df.iloc[:,i-n].name)
        cols[s.name] = s
        col_names.append(s.name)

    result = pd.DataFrame(cols)[col_names]
    #result = df - df.shift(n, axis=1)
    result.name = name
    
    if (unpivot_result):
        return unpivot(result)
    else:
        return result


def process_stirfut(curve_df, curve_name):

    Stirfut_Result = namedtuple('Stirfut_Result', 'curve_data_unpvt')

    curve_df.columns = [col.replace(curve_name, 'CT') for col in curve_df.columns] 

    yld = px_to_yield(curve_df.ffill(), 'yield', False)
    curve_data_unpvt = process_fwd_curve(yld, curve_name)
    curve_data_unpvt.name = curve_name +  "_stirfut"
    curve_data_unpvt['country'] = curve_name

    ydf = pivot(curve_data_unpvt[curve_data_unpvt['data_type'] == 'yield_1d_move'])
    xs = pivot(curve_data_unpvt[curve_data_unpvt['data_type'] == 'spreadtol3_1d_move']).iloc[:, 0]
    yield_1d_move_beta_sprd3_1d_move = calc_beta(xs, ydf, 'yield_1d_move_beta_sprd3_1d_move', n=100)

    
    ydf = pivot(curve_data_unpvt[curve_data_unpvt['data_type'] == 'yield'])
    xs = pivot(curve_data_unpvt[curve_data_unpvt['data_type'] == 'spreadtol3']).iloc[:, 0]
    yield_beta_sprd3 = calc_beta(xs, ydf, 'yield_beta_sprd3', n=1000)

    print yield_beta_sprd3.head()

        
    Stirfut_Result(curve_data_unpvt = curve_data_unpvt)
    
    return curve_data_unpvt
    
def process_fwd_curve(curve_df, curve_name):
    """ a generic process to analyse the historical move of the entire curve
    parameters
    ----------
    curve_df: dataframe 
        a BDH like of historical levels
    curve_name: string 
        i.e 'ED'
    israte: True
        is a curve of rate (otherwise futures)
    Returns
    -------
    dataframe 
        of what???
    """


    #yld = unpivot(curve_df, name = 'yield') if(israte) else px_to_yield(curve_df.ffill(),'yield',True)
    #
    yld_1d_move = ndays_move(curve_df, 1, 'yield_1d_move', False)
    yld_w_move = weekly_move(yld_1d_move, 'yield_weekly_move', False)
    yld_m_move = monthly_move(yld_1d_move, 'yield_monthly_move', False)
    #yld_move_since = move_since(curve_df,'yield_move_since', False)
     
    #
    sprd1 = make_spread(curve_df, 1, 'spreadtol1', False)
    sprd1_1d_move = ndays_move(sprd1, 1, 'spreadtol1_1d_move', False)
    sprd1_w_move = weekly_move(sprd1_1d_move, 'spreadtol1_weekly_move', False)
    sprd1_m_move = monthly_move(sprd1_1d_move, 'spreadtol1_monthly_move', False)
    #sprd1_move_since = move_since(sprd1,'spreadtol1_move_since', False)
    #

    sprd2 = make_spread(curve_df, 2, 'spreadtol2', False)
    sprd2_1d_move = ndays_move(sprd2, 1, 'spreadtol2_1d_move', False)
    sprd2_w_move = weekly_move(sprd2_1d_move, 'spreadtol2_weekly_move', False)
    sprd2_m_move = monthly_move(sprd2_1d_move, 'spreadtol2_monthly_move', False)
    #sprd2_move_since = move_since(sprd2,'spreadtol2_move_since', False)
    #
    sprd3 = make_spread(curve_df, 3, 'spreadtol3', False)
    sprd3_1d_move = ndays_move(sprd3, 1, 'spreadtol3_1d_move', False)
    sprd3_w_move = weekly_move(sprd3_1d_move, 'spreadtol3_weekly_move', False)
    sprd3_m_move = monthly_move(sprd3_1d_move, 'spreadtol3_monthly_move', False)
    #sprd3_move_since = move_since(sprd2,'spreadtol3_move_since', False)

    dfs = [
        curve_df, yld_1d_move, yld_w_move, yld_m_move,
        sprd1, sprd1_1d_move, sprd1_w_move, sprd1_m_move,
        sprd2, sprd2_1d_move, sprd2_w_move, sprd2_m_move,
        sprd3, sprd3_1d_move, sprd3_w_move, sprd3_m_move#,
        #yld_move_since, sprd1_move_since, sprd2_move_since, sprd3_move_since
    ]

    #beta calc

    #yield move vs first year spread
    #y = yld_1d_move
    #x = sprd3_1d_move.iloc[:, 0]
    #beta = calc_beta(x, y, '{0}_beta_{1}'.format(yld_1d_move.name, sprd3_1d_move.name), n=100)
    
    unpivoted_dfs = pd.concat([ unpivot(df, reverse_row = True) for df in dfs])

    return unpivoted_dfs


    

        

