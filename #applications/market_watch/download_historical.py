from __future__ import division
import argparse
import test_path
import historical_data_config as cfg
import datetime
import os, sys, inspect, time
import psutil
import logging
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from collections import Counter
import csv
from tia.bbg import LocalTerminal as bbgterminal
from StringIO import StringIO

#import tools.common.dirs as dirs
#from core import firedb as fdb
from core import dirs as dirs
from core import tools


def match_input_args(args, arg):
    if len(args) == 0:
        return True
    else:
        expected = sum(map(lambda x: arg.upper() in x.upper(), args))
        return True if expected > 0 else False


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-- Main
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Provide argument for bbg download input file')
    parser.add_argument('inputFiles', metavar='inputFiles', type=str, nargs='*', help='a list of the inputFileNames separated by a space. if none provided all input file will be used')
    args = parser.parse_args()
    print(args.inputFiles)

    _exit_code = 0

    try:
        logging.info("<<<< {0} starts >>>>".format(cfg.APP_NAME))
        
        print("Auto kill bbcomm is skipped as the process seems to have changed to be asynchonous")
        #bbcomm = 'bbcomm.exe'
        #if (bbcomm in (p.name() for p in psutil.process_iter())):
        #    os.system("taskkill /F /T /im bbcomm.exe")

        # output to staging folder
        if(cfg.ARCHIVE_FILES):
            logging.info(cfg.OUTPUT_FOLDER)
            dirs.archive_files(cfg.OUTPUT_FOLDER, cfg.ARCHIVE_OUTPUT_FOLDER, copy_only=True)



        today = datetime.datetime.today().strftime('%Y-%m-%d')
        logging.info(today)
        
        START_DATE = (datetime.datetime.today() + datetime.timedelta(days= -1 * cfg.START_DATE_OFFSET)).strftime('%Y-%m-%d')

        for file in os.listdir(cfg.INPUT_FOLDER):
            if file.endswith(cfg.FILE_SUFFIX) and match_input_args(args.inputFiles, file.replace('_historical.csv','')) :
                try:
                    logging.info(file)

                    fileName = file.replace(cfg.FILE_SUFFIX,'')

                    with open(os.path.join(cfg.INPUT_FOLDER, file), 'r') as myfile:
                        #content = myfile.read().replace('\n', ',')
                        tickers = []
                        names = {}
                        content = list(csv.reader(myfile))

                        for line in content:
                            if (len(line) == 2 and line[0].strip() != "" and line[0].strip()[0] != "#"):
                                tickers.append(line[0].strip())
                                if (line[0].strip() != line[1].strip()):
                                    names[line[0].strip()] = line[1].strip()


                        field = cfg.TICKER_FILE_FILED.get(file, "PX_LAST")
                        logging.info("{0} securities ({1}): ".format(len(tickers), field))
                        logging.info(tickers)
                        
                        
                        if(len(tickers) > 0):
                                                        
                            secdf = bbgterminal.get_historical(tickers, field, start=START_DATE, period='DAILY').as_frame()

                            #print secdf.columns.levels[0].index.name
                            secdf.columns = secdf.columns.levels[0]
                            #if "date" in secdf.columns.levels[0]:
                            #    secdf.columns = secdf.columns.levels[0]
                            #else:
                            #    secdf = pd.DataFrame(columns=["date"])                                                                    

                            if (set(tickers) == set(secdf.columns)):
                               secdf = secdf[tickers]

                            #rename
                            secdf.columns = [ col if not names.has_key(col) else names[col] for col in secdf.columns]

                            secdf.to_csv(os.path.join(cfg.OUTPUT_FOLDER, today + "_" +file ))

                            #save to database
                            
                            # try:
                            #     fire_database = fdb.FireDatabase() if len(cfg.SAVE_TO_FDB) > 0 else None 
                            #     if fileName in cfg.SAVE_TO_FDB:
                            #         logging.info("Save to firedb")
                            #         if fileName in cfg.OVERRIDE_EXISTING_DATA:
                            #             #logging.info(secdf.to_json())                                  
                            #             #fdb.persist(cfg.FDB_ROOT, fileName, secdf.to_json())
                            #             logging.info("Replace the existing data")
                            #             s = StringIO()
                            #             secdf.to_csv(s)
                            #             fire_database.persist(cfg.FDB_ROOT, fileName, s.getvalue())
                            #         else:
                            #             existing_data = fire_database.retrieve(cfg.FDB_ROOT + "/" + fileName + "/")
                            #             #logging.info(existing_data)
                            #             if existing_data != None:
                            #                 logging.info("Append to the existing data node")
                            #                 #existing_data = pd.read_json(existing_data)
                            #                 existing_data = pd.read_csv(StringIO(existing_data),sep=',')
                            #                 existing_data = existing_data.set_index("date")
                            #                 existing_data.index = pd.to_datetime(existing_data.index)                                            
                            #                 new_data = tools.merge_dataframess(existing_data, secdf)
                            #                 #logging.info(new_data)
                            #                 s = StringIO()
                            #                 new_data.to_csv(s)
                            #                 fire_database.persist(cfg.FDB_ROOT, fileName, s.getvalue())

                            #             else:
                            #                 #fdb.persist(cfg.FDB_ROOT, fileName, secdf.to_json())                                    
                            #                 logging.info("Create new data node")
                            #                 s = StringIO()
                            #                 secdf.to_csv(s)
                            #                 fire_database.persist(cfg.FDB_ROOT, fileName, s.getvalue())
                            # except:
                            #     logging.exception("!!! Error detected when updating the firedb for {0}. This file is skipped.".format(fileName))            
                            
                        else:
                            logging.info("Skip empty file")
                except:
                    logging.exception("!!! Error detected when downloading {0}. This file is skipped.".format(file))
                    
                    _exit_code = 1
        if _exit_code == 0:
            logging.info("<<<< {0} Ends SUCCESSFULLY >>>>".format(cfg.APP_NAME))
            #sys.exit(0)
        #sys.exit(1)
    except:
        logging.exception("!!! Error detected in {0}".format(cfg.APP_NAME))
        logging.info("<<<< {0} Ends UNSUCCESSFULLY >>>>".format(cfg.APP_NAME))
        _exit_code = 1
    finally:
        sys.exit(_exit_code)
