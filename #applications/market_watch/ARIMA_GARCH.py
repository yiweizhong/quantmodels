import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
import statsmodels.formula.api as smf
import statsmodels.tsa as tsa
from statsmodels.graphics.tsaplots import plot_acf
from pandas.plotting import autocorrelation_plot

import seaborn as sns
import numpy as np
import scipy


# def bartletts_formula(acf_array, n):
#     """
#     Computes the Standard Error of an acf with Bartlet's formula
#     Read more at: https://en.wikipedia.org/wiki/Correlogram
#     :param acf_array: (array) Containing autocorrelation factors
#     :param n: (int) Length of original time series sequence.
#     """
#     # The first value has autocorrelation with it self. So that values is skipped
#     se = np.zeros(len(acf_array) - 1)
#     se[0] = 1 / np.sqrt(n)
#     se[1:] = np.sqrt((1 + 2 * np.cumsum(acf_array[1:-1]**2)) / n )
#     return se


# def plot_acf(x, alpha=0.05, lag=40):
#     """
#     :param x: (array)
#     :param alpha: (flt) Statistical significance for confidence interval.
#     :parm lag: (int)
#     """
#     acf_val = tsa.stattools.acf(x, lag)
#     plt.figure(figsize=(16, 4))
#     plt.vlines(np.arange(lag), 0, acf_val)
#     plt.scatter(np.arange(lag), acf_val, marker='o')
#     plt.xlabel('lag')
#     plt.ylabel('autocorrelation')
    
#     # Determine confidence interval
#     ci = stats.norm.ppf(1 - alpha / 2.) * bartletts_formula(acf_val, len(x))
#     plt.fill_between(np.arange(1, ci.shape[0] + 1), -ci, ci, alpha=0.25)



#white noise

n = 500
eps = np.random.normal(size=n)

fig, ax = plt.subplots(1,2, figsize=(16, 6), gridspec_kw={'width_ratios':[3, 1]})
ax[0].plot(eps)
sns.distplot(eps, ax=ax[1])
plot_acf(eps)
autocorrelation_plot(eps)

#########################################################
#AR(1)
# https://www.quantstart.com/articles/Autoregressive-Moving-Average-ARMA-p-q-Models-for-Time-Series-Analysis-Part-1/
#
#########################################################

n = 500
eps = np.random.normal(loc=0, size=n)
ar_lag = 1
a1 = 0.6

ar1_eps = [eps[0]]

#bootstrap data
for i in (np.arange(n-ar_lag) + 1):
    ar1_eps = ar1_eps + [a1 * ar1_eps[i-1] + eps[i]]     


fig, ax = plt.subplots(1,2, figsize=(16, 6), gridspec_kw={'width_ratios':[3, 1]})
ax[0].plot(ar1_eps)
sns.distplot(ar1_eps, ax=ax[1])
plot_acf(np.array(ar1_eps)) #need to be numpy array
autocorrelation_plot(ar1_eps)

#supply the lag
mdl_AR = tsa.ar_model.AutoReg(ar1_eps, lags=[1], trend='n')
out = 'AIC: {0:0.3f}, HQIC: {1:0.3f}, BIC: {2:0.3f}'
res = mdl_AR.fit()
res.fittedvalues
res.params
print(out.format(res.aic, res.hqic, res.bic))

#let the model fit the lag
mdl_AR = tsa.ar_model.ar_select_order(ar1_eps, 5, trend='n') 

mdl_AR.ar_lags #get the lag
res = mdl_AR.model.fit() # get the fit


fig, ax = plt.subplots(1,2, figsize=(16, 6), gridspec_kw={'width_ratios':[3, 1]})
ax[0].plot(ar1_eps)
ax[0].plot(res.fittedvalues)
sns.distplot(ar1_eps, ax=ax[1])
sns.distplot(res.fittedvalues, ax=ax[1])

np.var(res.resid,ddof=1)
res.cov_params()

#########################################################
# MA(1)
# https://www.quantstart.com/articles/Autoregressive-Moving-Average-ARMA-p-q-Models-for-Time-Series-Analysis-Part-2/
#
#########################################################


n = 500
eps = np.random.normal(size=n)
ma_lag = 2
a1 = 0.9
a2 = 0.5


ma1_eps = [eps[0]]

#bootstrap data
for i in (np.arange(n-ma_lag) + ma_lag):
    print('{},{}'.format(i- ma_lag + 1, i- ma_lag))
    ma1_eps = ma1_eps + [a1 * eps[i- ma_lag + 1] + a2 * eps[i-ma_lag]  + eps[i]]     

#plot check

fig, ax = plt.subplots(1,2, figsize=(16, 6), gridspec_kw={'width_ratios':[3, 1]})
ax[0].plot(ma1_eps)
ax[0].plot(eps)
sns.distplot(ma1_eps, ax=ax[1])
plot_acf(np.array(ma1_eps)) #need to be numpy array
autocorrelation_plot(ma1_eps)


































