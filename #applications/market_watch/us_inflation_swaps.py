from __future__ import division
import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper# -*- coding: utf-8 -*-



def get_curves(data, country):
    spot_secs = [c for c in data.columns if country in c and '_0Y_' in c]
    f1y_secs = [c for c in data.columns if country in c and  '_1Y_' in c] 
    f2y_secs = [c for c in data.columns if country in c and '_2Y_' in c]
    spot_curve = data[spot_secs]
    f1y_curve = data[f1y_secs]
    f2y_curve = data[f2y_secs]
    spot_curve = spot_curve.rename(lambda col: col.replace(country + '_0Y_',''), axis='columns')
    f1y_curve = f1y_curve.rename(lambda col: col.replace(country + '_1Y_',''), axis='columns')
    f2y_curve = f2y_curve.rename(lambda col: col.replace(country + '_2Y_',''), axis='columns')
    spot_curve = spot_curve[f1y_curve.columns]
    spot_curve.name = 'spot'
    f1y_curve.name = 'f1y'
    f2y_curve.name = 'f2y'
    return (spot_curve, f1y_curve, f2y_curve)


def convert_spot_to_df_fwd(spot_df):
    # columns name looks like USD_1Y_SWIT
    df_df = {}
    
    for c in spot_df.columns:
        power = int(c.replace('USD_','').replace('Y_SWIT',''))
        df_df[power] = spot_df[c].apply(lambda x: np.power(1/(1 + x/100),power))
    df_df = pd.DataFrame(df_df)
    
    fwd_df = {}
    n = len(df_df.columns)
    for i in range(n):
        if i > 0: 
            t1 = df_df.columns[i-1]
            t2 = df_df.columns[i]
            t_delta = t2-t1
            fwd_df_s = df_df[t2]/df_df[t1] 
            fwd_s = fwd_df_s.apply(lambda x: (1 / np.power(x, 1/t_delta) - 1) * 100)
            name = 'USD_{0}Y{1}Y_SWIT'.format(t1,t_delta)
            fwd_s.name = name
            fwd_df[name] = fwd_s            
        else:
            fwd_df[spot_df.columns[0]] = spot_df[spot_df.columns[0]] 
    fwd_df = pd.DataFrame(fwd_df)
    
    return df_df, fwd_df

    

date = '2020-06-12'

spot = pd.read_csv('C:/Temp/test/market_watch/staging/' + date +'_SWIT_historical.csv')
spot['date'] = pd.to_datetime(spot['date'])
spot = spot.set_index('date').ffill()


spot_df, fwd = convert_spot_to_df_fwd(spot)


us_sec = pd.read_csv('C:/Temp/test/market_watch/staging/'+ date +'_USD_historical.csv')
us_sec['date'] = pd.to_datetime(us_sec['date'])
us_sec = us_sec.set_index('date').ffill()





#######################################################################################
#######################################################################################
###### spot
#######################################################################################
#######################################################################################

# there is a clear regime changes pre and post gfc. pre gfc the inflation factor 1 is upwa


tenors = ['USD_2Y_SWIT', 'USD_5Y_SWIT']
tenors = [['USD_1Y_SWIT','USD_2Y_SWIT', 'USD_5Y_SWIT', 'USD_10Y_SWIT', 'USD_30Y_SWIT']]

spot_sub = spot[spot.index > (spot.index[-1] - DateOffset(years = 12, months=0, days=0))]\
    [tenors]


spot_pca = pca.PCA(spot_sub, rates_pca = True, momentum_size = None)

spot_pca.plot_loadings(n=3, m=5)


spot_pca.plot_valuation_history(2000, n=[1,2], w=16, h=12, rows=5, cols=3, 
                                normalise_valuation_yaxis = False,  normalise_data_yaxis = False,
                                selected_series= spot_sub.columns.tolist())



spot_sub = spot[spot.index > (spot.index[-1] - DateOffset(years = 10, months=0, days=0))]\
        [tenors]


spot_pca = pca.PCA(spot_sub, rates_pca = True, momentum_size = None)

spot_pca.plot_loadings(n=2, m=5)

spot_pca.plot_valuation_history(2000, n=[1], w=16, h=12, rows=5, cols=3, 
                                normalise_valuation_yaxis = False,  normalise_data_yaxis = False,
                                selected_series= spot_sub.columns.tolist())




#######################################################################################
#######################################################################################
###### fwd
#######################################################################################
#######################################################################################

fwd_sub = fwd[fwd.index > (fwd.index[-1] - DateOffset(years = 12, months=0, days=0))]
fwd_pca = pca.PCA(fwd_sub, rates_pca = True, momentum_size = None)

fwd_pca.plot_loadings(n=3, m=5)

fwd_pca.plot_valuation_history(2000, n=[1,2], w=16, h=12, rows=5, cols=3, 
                                normalise_valuation_yaxis = False,  normalise_data_yaxis = False,
                                selected_series= fwd.columns.tolist())


fwd_sub = fwd[fwd.index > (fwd.index[-1] - DateOffset(years = 10, months=0, days=0))]
fwd_pca = pca.PCA(fwd_sub, rates_pca = True, momentum_size = None)

fwd_pca.plot_loadings(n=3, m=5)

fwd_pca.plot_valuation_history(2000, n=[1,2], w=16, h=12, rows=5, cols=3, 
                                normalise_valuation_yaxis = False,  normalise_data_yaxis = False,
                                selected_series= fwd.columns.tolist())



fwd_sub[['USD_1Y_SWIT','USD_1Y1Y_SWIT']].plot()

(-fwd_sub['USD_1Y_SWIT'] + fwd_sub['USD_1Y1Y_SWIT']).plot()







#######################################################################################
#######################################################################################
###### spot
#######################################################################################
#######################################################################################

# there is a clear regime changes pre and post gfc. pre gfc the inflation factor 1 is upwa


tenors = [['USD_1Y_SWIT','USD_2Y_SWIT', 'USD_5Y_SWIT', 'USD_10Y_SWIT', 'USD_30Y_SWIT']]


spot_sub = spot[spot.index > (spot.index[-1] - DateOffset(years = 12, months=0, days=0))]\


spot_sub_sub =spot_sub[['USD_1Y_SWIT', 'USD_30Y_SWIT']] 

spot_sub_sub['USD_2Y5YAVG_SWIT'] = spot_sub['USD_2Y_SWIT'] / 3 +  spot_sub['USD_5Y_SWIT'] * 2 / 3

spot_pca = pca.PCA(spot_sub_sub, rates_pca = True, momentum_size = None)

spot_pca.plot_loadings(n=3, m=5)


spot_pca.plot_valuation_history(2000, n=[1,2], w=16, h=12, rows=5, cols=3, 
                                normalise_valuation_yaxis = False,  normalise_data_yaxis = False,
                                selected_series= spot_sub.columns.tolist())















