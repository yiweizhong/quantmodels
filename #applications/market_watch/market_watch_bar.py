from __future__ import division
import _config_bar as cfg
import logging
import os, sys, inspect
import time, datetime
import pandas as pd
from pandas.tseries.offsets import *
import numpy as np
import argparse

from core import tools
from core import returns
import core.dirs as dirs

def load_tick_market_data_files(from_dir, country_filter, column_filter):
    
    def filter_generator():
        for subdir, dirs, files in os.walk(from_dir):
            country_bar_files = (f for f in files if '_bar' in f and country_filter in f)
            for f in country_bar_files:
                f_path = os.path.join(from_dir, f)
                df = tools.tframe(pd.read_csv(f_path),key ="time",utc=False)
                file_name_sections = f.split('_')
                if column_filter in df.columns and len(file_name_sections) == 4:    
                    yield (file_name_sections[2], file_name_sections[1], df[column_filter])
    
    dfs = {}
    
    for (sec, country, ds) in filter_generator():
        if sec not in dfs.keys():
            dfs[sec] = {country:ds}
        else:
            dfs[sec][country] = ds

    for k, v in dfs.iteritems():
        dfs[k] = pd.DataFrame(v)

    return dfs
  


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-- Main
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='---- market watch ----')
    parser.add_argument('countries', metavar='countries', type=str, nargs='+', 
        help='If there are more than 1 country seperate them by a space')
    args = parser.parse_args()
    print(args.countries)

    exit_code = 0

    sec_data = {}

    for country in args.countries:
        logging.info("Start to merge raw tick data for {0}".format(country))
        
        for sec, country_data in load_tick_market_data_files(cfg.INPUT_FOLDER, country, "close").iteritems():
            if sec not in sec_data.keys():
                sec_data[sec] = country_data
            else:
                sec_data[sec] = pd.concat([sec_data[sec], country_data], axis=1)

    #for sec, data in sec_data.iteritems():
    #    print sec
    #    print data.tail(30) 


