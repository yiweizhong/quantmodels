from __future__ import division
import pandas as pd 
import numpy as np
import _path
import os, sys, inspect
import logging
from collections import namedtuple
import common.dirs as dirs
import common.emails as emails
import data.database as db
import _config as cfg




if __name__ == '__main__':

    _exit_code = 9999

    try:
       
        logging.info("<<<< {0} starts >>>>".format(cfg.APP_NAME))
        dirs.archive_files(cfg.DEBUG_FOLDER, cfg.ARCHIVE_DEBUG_FOLDER, copy_only = False)
        dirs.archive_files(cfg.OUTPUT_FOLDER, cfg.ARCHIVE_OUTPUT_FOLDER, copy_only = False)
        #_db = db.Database(cfg.FI_PROD_DB_CONN_STRING, cfg.DEBUG_FOLDER)

        








        
        logging.info("<<<< {0} ends SUCCESSFULLY >>>>".format(cfg.APP_NAME))
        _exit_code = 0
    except:
        logging.exception("!!! Error detected in {0}".format(cfg.APP_NAME))
        logging.ERROR("<<<< {0} ends UNSUCCESSFULLY >>>>".format(cfg.APP_NAME))
        _exit_code = 9999
    finally:
        _result = dirs.check_log_file_result(cfg.LOG_PATH)
        _title = "{0} process - {1}".format(cfg.APP_NAME, _result)
        _email_body = dirs.htmlfy_log_file(cfg.LOG_PATH)
        emails.send_SMTP_mail(cfg.SENDER, cfg.RECEIVERS, _title, _email_body, cfg.CC, [cfg.LOG_PATH])
        sys.exit(_exit_code)

