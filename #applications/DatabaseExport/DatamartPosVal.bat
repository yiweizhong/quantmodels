setlocal

SET myPath=%~dp0
echo %mypath:~0,-1%

path=.\lib;%path%

set paddedtime=%TIME: =0%
set timestamp=%date:~10,4%%date:~7,2%%date:~4,2%%paddedtime:~0,2%%paddedtime:~3,2%%paddedtime:~6,2%
Set tempFileName=DatamartPosVal_temp
Set tempFileName2=DatamartPosVal_temp2
Set exportFileName=DatamartPosVal

rem clean up the data and log folders

rem grep -v "dummy"  ".\data\%exportFileName%.csv" > ".\archive\%exportFileName%_%timestamp%.csv"
rem grep -v "dummy"  "%myPath%data\%exportFileName%.csv" > "%myPath%archive\%exportFileName%_%timestamp%.csv"

rem xcopy /Y ".\data\%exportFileName%.csv" ".\archive\%exportFileName%_%timestamp%.csv"	
del /Q  "%myPath%data\%exportFileName%.csv"

rem extrat the latest data
sqlcmd  -x -b -r 0 -S CPMA-SQL\CPMAREPPRD -d DMPDatamart -i %myPath%DatamartPosVal.sql -s"," -W > %myPath%archive\%tempFileName%_%timestamp% 2>&1
rem get rid off the header divider line
grep -v "\-\-\-\-"  %myPath%archive\%tempFileName%_%timestamp% > %myPath%data\%tempFileName2%
rem remove the NULL string
sed "s/NULL//g"  %myPath%data\%tempFileName2% >  %myPath%data\%exportFileName%.csv

DEL  %myPath%data\%tempFileName%
DEL  %myPath%data\%tempFileName2%

rem copy the latest data
rem copy /y %myPath%archive\%exportFileName%_%timestamp%.csv  %myPath%data\%exportFileName%_%timestamp%.csv

rem archive the log file now
rem copy /y %myPath%%exportFileName%.log  %myPath%archive\%timestamp%_%exportFileName%.log


endlocal
rem pause