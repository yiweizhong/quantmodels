setlocal

SET myPath=%~dp0
echo %mypath:~0,-1%

path=.\lib;%path%

set paddedtime=%TIME: =0%
set timestamp=%date:~10,4%%date:~7,2%%date:~4,2%%paddedtime:~0,2%%paddedtime:~3,2%%paddedtime:~6,2%
Set tempFileName=EPI_M_temp
Set tempFileName2=EPI_M_temp2
Set exportFileName=EPI_M

rem clean up the data and log folders

rem grep -v "dummy"  ".\data\%exportFileName%.csv" > ".\archive\%exportFileName%_%timestamp%.csv"
grep -v "dummy"  "%myPath%data\%exportFileName%.csv" > "%myPath%archive\%exportFileName%_%timestamp%.csv"

rem xcopy /Y ".\data\%exportFileName%.csv" ".\archive\%exportFileName%_%timestamp%.csv"	
del /Q  "%myPath%data\%exportFileName%.csv"

rem extrat the latest data
sqlcmd  -x -b -r 0 -S frontofficesql -d FIXED_INCOME -i %myPath%EPI_M.sql -s"," -W > %myPath%data\%tempFileName% 2>&1
rem get rid off the header divider line
grep -v "\-\-\-\-"  %myPath%data\%tempFileName% > %myPath%data\%tempFileName2%
rem remove the NULL string
sed "s/NULL//g"  %myPath%data\%tempFileName2% >  %myPath%data\%exportFileName%.csv

DEL  %myPath%data\%tempFileName%
DEL  %myPath%data\%tempFileName2%

rem copy the latest data

if exist "\\capricorn\patriot 3\Macro_Markets_EPIs" (

	if exist "\\capricorn\patriot 3\Macro_Markets_EPIs\Archive" (
		
		grep -v "dummy" "\\capricorn\patriot 3\Macro_Markets_EPIs\%exportFileName%.csv" > "\\capricorn\patriot 3\Macro_Markets_EPIs\Archive\%exportFileName%_%timestamp%.csv"		
		del /Q "\\capricorn\patriot 3\Macro_Markets_EPIs\%exportFileName%.csv"
		
	) else (
		
		mkdir "\\capricorn\patriot 3\Macro_Markets_EPIs\Archive"	
	)
	
	xcopy /Y  %myPath%data\%exportFileName%.csv "\\capricorn\patriot 3\Macro_Markets_EPIs\" 
		
) else (
	
	mkdir "\\capricorn\patriot 3\Macro_Markets_EPIs"
	mkdir "\\capricorn\patriot 3\Macro_Markets_EPIs\Archive"	
	
	xcopy /Y %myPath%data\%exportFileName%.csv "\\capricorn\patriot 3\Macro_Markets_EPIs\" 
	
)

rem archive the log file now
copy /y %myPath%%exportFileName%.log  %myPath%archive\%timestamp%_%exportFileName%.log


endlocal
rem pause