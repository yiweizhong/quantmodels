% Homework 1

clear all; close all;

% subsamples = 1 will redo everything for subsamples, lots of graphs

subsamples = 0;


% Sample, 1952:6 to 2003:12
load bondprice.dat; 

mat=[1;2;3;4;5];
yields=-log(bondprice(:,2:end)/100)./(ones(length(bondprice),1)*mat');

% make nice dates for graphs

dates = bondprice(:,1); 
yr = floor(dates/10000);
mo = dates-10000*yr;
mo = floor(mo/100);
day = dates-10000*yr-100*mo;
dates = yr+mo/12;

% Fama & Bliss 1987 recommend using data only after 1964
% because there were not many long bonds trading before 1964 

beg = 140;    % beginning entry of the sample;

disp('check whether this is 1964');
disp(bondprice(beg,1)); 
 
dates=dates(beg:end);
yields=yields(beg:end,:);
normaldate=bondprice(beg:end,1);

plot(dates, yields);
axis tight;   % gets rid of extra space in the graph

% Sample

load FamaTbill16.dat;

shortyields=FamaTbill16;

beg = 169;

disp('check whether this is 1964');
disp(shortyields(beg,1));

shortyields=12*shortyields(beg:end,2:end);

yields=[shortyields yields];

yields=yields*100;    % percent for nice figures

% plot data
plot(dates, yields);
axis tight;

disp('check for zeros in this list');
disp(min(yields));

% fix the zeros by interpolation
indx=find(yields(:,6)==min(yields(:,6)));
yields(indx,6)=(yields(indx-1,6)+yields(indx+1,6))/2;

% plot again
plot(dates, yields);
axis tight;

% now for the handbook chapter
 figure; 
   plot(dates, yields(:,7), 'y', ...
        dates, yields(:,11),'g', ...
        dates, yields(:,3), 'k', 'Linewidth',1.3);
   axis([1964 2004 0 18]);
   legend('2-year', '5-year', '3 month', 0);
   ylabel('percent');
   title('Fama and Bliss data: 3-month, 2-year and 5-year yields');
   print -dpsc fbdata.ps;
   

if subsamples ==1;

subs = [1    length(yields);   % full sample
        1    175;              % pre 08-1979
        176  226;              % 08-1979 to 10-1982
        227 length(yields)];   % post 10-1982
          
else;
    
    subs = [1 length(yields)];
    %subs = [284 length(yields)];
        
end
    
        
mat = [1/12 2/12 3/12 4/12 5/12 6/12 1 2 3 4 5]';

T = size(yields,1);
N = size(yields,2);

    % initiate these moments & standard errors
    m1=zeros(N,size(subs,1)); se1 =m1;
    m2=m1;  se2=m1;
    m3=m1;  m4=m1; m3ch=m1; m4ch=m1; se3=m1; se4=m1; se3ch=m1; se4ch=m1;
    mauto=m1; seauto=m1;
    

for i=1:size(subs,1) 
    
    y = yields(subs(i,1):subs(i,2),:);
        
for j = 1 : size(y,2)

           [mj vj] = autoc(y(:,j),1,6);
            sej=sqrt(diag(vj));
            m1(j,i)    = mj(1);
            se1(j,i)   = sej(1);
            mauto(j,i) = mj(3);
            seauto(j,i)= sej(3);
           
           [mj vj]    = semom(y(:,j),6);
            sej=sqrt(diag(vj));
            m3(j,i)   = mj(3);
            m4(j,i)   = mj(4);
            se3(j,i)  = sej(3);
            se4(j,i)  = sej(4);
            
           [mj vj]    = semom(diff(y(:,j)),6);
            sej=sqrt(diag(vj));
            m2(j,i)    = mj(2);
            se2(j,i)   = sej(2);
            m3ch(j,i)  = mj(3);
            m4ch(j,i)  = mj(4);
            se3ch(j,i) = sej(3);
            se4ch(j,i) = sej(4);
            
            
end

% subset of yields to report statistics
only=[1;3;6;7;8;9;10;11]; 
 
for i=1:size(subs,1)
            disp([normaldate(subs(i,1)) normaldate(subs(i,2))]);
            
            disp('autocorrelation in yield levels');
            disp([mat(only) round(mauto(only,i)*1000)/1000 seauto(only,i)]);
            
            disp('levels');
            disp([mat(only) m3(only,i) m4(only,i) se3(only,i) se4(only,i)]);
            disp('changes');
            disp([mat(only) m3ch(only,i) m4ch(only,i) se3ch(only,i) se4ch(only,i)]);

end

% take autocorrelation of longest yield and round it, 3 decimals
rholong=mauto(end,i);
rholong=round(rholong*1000)/1000;

disp('Half-life of shocks to the longest yield');
disp([log(0.5)/log(rholong) log(0.5)/log(rholong)/12])

figure;

plot(mat,m1(:,i)+2*se1(:,i), 'r:', ...
     mat,m1(:,i),'k',...
     mat,m1(:,i)-2*se1(:,i),'r:');
title('Average Yield Curve');

if size(subs,1) == 1
figure;
   plot(mat,m1,'k', ...
        mat,m1+2*se1,'r:', ...
        mat,m1-2*se1,'r:',...
        'LineWidth',1.5);
   title('Average yield curve with 2 x standard error bounds');
   ylabel('percent');
   xlabel('maturity (in years)');
   
   print -dpsc yav.ps;
end

figure;
plot(mat,m2(:,i)+2*se2(:,i), 'r:', ...
     mat,m2(:,i),'k',...
     mat,m2(:,i)-2*se2(:,i),'r:');
title('Volatility');

figure;
plot(mat,mauto(:,i)+2*seauto(:,i),'r:', ...
     mat,mauto(:,i),'k', ...
     mat,mauto(:,i)-2*seauto(:,i),'r:');
title('Autocorrelation');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Vol curve for Greenspan era
  
  %load fedmonthly;
  %y1=fedmonthly(:,2);
  %y=[y1 y];
  %mats=[1/30;mats];

   start=284; %1987:8
   normaldate(284)
   
    for i=1:N

        dy=diff(yields(start:end,i));
        [m,c]=semom(dy,6);
        serr=sqrt(diag(c));
        yvol(i)=m(2);
        sevol(i)=serr(2);
    end;  
   
   figure;
   plot(mat,yvol','k', ...
        mat,yvol'+2*sevol','r:', ...
        mat,yvol'-2*sevol','r:',...
        'LineWidth',1.5);
   title('Volatility curve with 2x standard error bounds');
   ylabel('percent');
   xlabel('maturity (in years)');
   axis([0 max(mat) 0.18 0.55]); 
    
   print -dpsc vol.ps;

   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Principal components 

   % Eigenvalue decomposition for yields
   Vy = cov(yields); 
   [Q,L] = eig(Vy);
   % this produces Q*L*Q' = Vy. The eigenvalues are not necessarily ordered.  
   % reorder with largest eigenvalue on top 
   [D,Indx] = sort(-diag(L));
   D = -D; 
   Q2 = Q(:,Indx); 
   L2 = diag(D,0); 
   Q = Q2; 
   L = L2; 

   PC=(yields-ones(T,1)*mean(yields))*Q;  % principal components for levels
   level = PC(:,1);   
   slope = PC(:,2);
   curve = PC(:,3); 
      
   % for matrices x = Q'*yields
   % for time series: X = Y*Q
    
   N=size(D,1);
   lam=zeros(N,1);
   for i=1:N;
   lam(i)=sum(D(1:i))/sum(D);
   end

   QL=Q; % save for later
   
   % plot the loadings 
   figure;
   plot(mat,Q(:,1),'k', ...
        mat,Q(:,2),'g', ...
        mat,Q(:,3),'y',...
        'LineWidth',1.5);
   title('Loadings of yields on principal components');
   legend('level','slope','curvature');
   xlabel('maturity in months');
   
   print -dpsc loadings.ps;
   
   % principal components for yield changes
   Vy = cov(diff(yields)); 
   [Q,L] = eig(Vy);
  
   [D,Indx] = sort(-diag(L));
   D = -D; 
   Q2 = Q(:,Indx); 
   L2 = diag(D,0); 
   Q = Q2; 
   L = L2; 

   pc=diff(yields)*Q;  % principal components for levels
   levelch = pc(:,1);   
   slopech = pc(:,2);
   curvech = pc(:,3); 
      
   % for matrices x = Q'*yields
   % for time series: X = Y*Q
    
   N=size(D,1);
   lamch=zeros(N,1);
   for i=1:N;
   lamch(i)=sum(D(1:i))/sum(D);
   end
   disp('fraction of the variance explained - level and changes in yields');
   disp([(1:N)' lam lamch]);
   
   % plot the loadings 
   figure;
   plot(mat,Q(:,1),'k', ...
        mat,Q(:,2),'g', ...
        mat,Q(:,3),'y',...
        'LineWidth',1.5);
   title('Loadings of yields on principal components -- changes');
   legend('level','slope','curvature');
   xlabel('maturity in months');
   % the loadings are different
   % but same general pattern: 
   % level, slope and curvature
   
  
   % More properties of principal components
   
   for i=1:3
   ctemp=cov([PC(2:end,i) PC(1:end-1,i)])/var(PC(:,i)); 
   c(i)=ctemp(2,1);
   end

   disp('autocorrelations of 3 pc');
   disp(c);

   yfit=PC(:,1:3)*QL(:,1:3)'+ones(T,1)*mean(yields);
 
   fe=yields-yfit;

   disp('statistics of fitting errors');
   st=[mean(abs(fe))' std(abs(fe))' max(abs(fe))']; 
   disp([mat st]);
   