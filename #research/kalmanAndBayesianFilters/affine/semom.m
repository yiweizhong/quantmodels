% Procedure to calculate mean, variance, skewness and kurtosis, and
%  with Newey-West standard errors.  


function [ MOMENTS,COV] = semom(data,lags);

T=length(data);

m1=mean(data)';
m2=var(data);
m3=mean((data-m1).^3);
m4=mean((data-m1).^4); 
MEAN=m1';
STD=sqrt(m2);
VAR=m2;
SKEW=m3/m2^(3/2);
KURT=m4/m2^2;

% Moment Conditions 

h1=data-m1;
h2=h1.^2-m2;
h3=h1.^3-m3;
h4=h1.^4-m4;
h=[h1 h2 h3 h4];

nobs=T;
R=h'*h/nobs;
n_moving=lags;
i=1;
while i <= n_moving;
   R_temp=h(i+1:nobs,:)'*h(1:nobs-i,:)/nobs;    
   R=R+(R_temp'+R_temp)*(1-i/(n_moving+1));
   i=i+1;
end;
W=inv(R);

% Derivative matrix 

D = -eye(4);
D(3,1)=-3*m2;
D(4,1)=-4*m3;

% Covariance matrix for moment conditions 

COV=inv(D'*W*D);

% Standard errors for functions of interest 

D=eye(4);
D(2,2)=.5*m2^(-.5);
D(3,2)=-1.5*m3/m2^(5/2);    D(3,3)=1/m2^(3/2);     
D(4,2)=-2*m4/m2^3;          D(4,4)=1/m2^2;

COV = D*COV*D'/nobs;


MOMENTS=[MEAN;STD;SKEW;KURT];

        