import os,sys,inspect
import datetime as dt
import logging
import shutil

def get_cwd():
    #return os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0]))
    print(os.getcwd())
    return os.getcwd()

def get_parent_wd(path=None):
    p = (get_cwd() if path is None else path)
    return os.path.abspath(os.path.join(p, os.pardir))

def get_log_dt_fmt():
    return r"%(asctime)s - %(name)s - %(levelname)s - %(message)s"

def get_create_absolute_folder(dir):
    if not os.path.isdir(dir):
        os.makedirs(dir)
    return dir

def get_creat_sub_folder(folder_name= r"New_Folder", parent_dir = None):
    parent = parent_dir if (parent_dir is not None and os.path.isdir(parent_dir)) else get_cwd()
    dir = os.path.join(parent, folder_name)
    if not os.path.isdir(dir):
        os.makedirs(dir)
    return dir

def get_log_file_name(logfile_name_prefix= r"Default", log_folder=None):
    log_folder = get_creat_sub_folder(folder_name= r"log", parent_dir = None) if log_folder is None else log_folder
    f = os.path.join(log_folder, "%s_%s.%s" % (logfile_name_prefix, dt.datetime.now().strftime("%Y-%m-%d.%H-%M-%S"), "log"))
    return f

def archive_files(from_dir,to_dir, copy_only = False, file_mask = None):
    '''
    Parameters
    ----------
    from_dir : string
        path to the from folder.
    to_dir : string
        path to the to folder .
    copy_only : bool, optional
        flag whether to delete the file after archiving. The default is False.
    file_mask : list of string, optional
        a list of the file names or file name patterns to be archived. The default is None.

    Raises
    ------
    ValueError
        error if the dir doesnt exist.
    Returns
    -------
    a list of file names archived.

    '''
    if not os.path.isdir(from_dir): raise ValueError("Invalid from_dir for archive")
    if not os.path.isdir(to_dir): raise ValueError("Invalid to_dir for archive")
    tstamp = dt.datetime.now().strftime(r"%Y-%m-%d_%H%M%S")
    archived_files = {}
    for subdir, dirs, files in os.walk(from_dir):
        for file in files:
            if file_mask is None:
                
                fr_file_name_with_dir = os.path.join(subdir, file) 
                to_file_name = "{1}.{0}".format(file, tstamp)
                to_file_name_with_dir = os.path.join(to_dir, to_file_name)
                
                #print(f'{file}, {to_file_name}, {fr_file_name_with_dir}, {to_file_name_with_dir}')
                
                # this condition is to make sure file is not double archived
                # espeically when the archive folder sits within the from folder
                if (file not in archived_files.values()):
                    #print('not archived')
                
                    if copy_only:
                        shutil.copy(fr_file_name_with_dir, to_file_name_with_dir)
                    else:
                        os.rename(fr_file_name_with_dir, to_file_name_with_dir)
                        
                    archived_files[fr_file_name_with_dir] = to_file_name
                #logging.info("Archived {0} to {1}".format(fr_file_name, to_file_name))
            else:                
                if  len([f for f in file_mask if f in file]) > 0:
                    fr_file_name_with_dir = os.path.join(subdir, file) 
                    to_file_name = "{1}.{0}".format(file, tstamp)
                    to_file_name_with_dir = os.path.join(to_dir, to_file_name)
                    
                    # this condition is to make sure file is not double archived
                    # espeically when the archive folder sits within the from folder
                    if (file not in archived_files.values()):
                    
                        if copy_only:
                            shutil.copy(fr_file_name_with_dir, to_file_name_with_dir)
                        else:
                            os.rename(fr_file_name_with_dir, to_file_name_with_dir)
                        
                        archived_files[fr_file_name_with_dir] = to_file_name
                        
    return archived_files
                    


def check_log_file_result(file_path):
    result = ' - INFO'
    if (os.path.isfile(file_path)):
        logfile = open(file_path).read()
        if '- WARNING -' in logfile: result =  ' - WARNING'
        if '- ERROR -' in logfile: result =  ' - ERROR'
        if 'traceback' in logfile.lower() : result =  ' - ERROR'
    else:
        result = ' - No log file found'
    return result

def htmlfy_log_file(file_path):
    def highligher(line):
        line = line.replace(" INFO ", """<span style="color: #00008B; background: #98FB98"> &nbsp; INFO &nbsp;</span>""")
        line = line.replace(" WARNING ", """<span style="color: #00008B; background: #FF7F50">&nbsp; WARNING &nbsp;</span>""")
        line = line.replace(" DEBUG ", """<span style="color: #00008B; background: #87CEFA">&nbsp; DEBUG &nbsp;</span>""")
        line = line.replace(" ERROR ", """<span style="color: #00008B; background: #FF4500">&nbsp; ERROR &nbsp;</span>""")
        line = line.replace(" SUCCESSFULLY ", """<span style="color: #00008B; background: #98FB98"> &nbsp; SUCCESSFULLY &nbsp;</span>""")
        line = line.replace(" UNSUCCESSFULLY ", """<span style="color: #00008B; background: #FF4500">&nbsp; UNSUCCESSFULLY &nbsp;</span>""")

        return line

    result = ['']
    if (os.path.isfile(file_path)):
        lines = [highligher(line).rstrip('\n') for line in open(file_path)]
        #lines = [line.replace('\n',"</br>") for line in open(file_path)]
    else:
        lines = [' - No log file found']
    result = "<br>".join(lines)
    return result


def get_file_path(p,f):
    return os.path.join(p, f)


