import os
import smtplib
import logging
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import  encoders

def send_SMTP_mail(send_from, send_to, subject, email_body = "", cc_to = [], files=[], SMTP="ausmtp01.amp.com.au:25"):
    """
    This function will send email via the SMTP server
    :param send_from: the from email address
    :param send_to: the list of email addresses sent to
    :param subject: email subject
    :param email_body: email body
    :param cc_to: the list of email addresses cc to
    :param bcc_to: the list of email addresses bcc to
    :return: None
    """

    assert isinstance(send_to, list), "send_to must be a list of email address"
    assert len(send_to) > 0, "send_to must Not be empty"
    assert isinstance(files, list), "files must be a list of email address"

    msg = MIMEMultipart('alternative')
    msg['From'] = send_from
    if len(send_to) > 0: msg['To'] = COMMASPACE.join(send_to)
    if len(cc_to) > 0: msg['Cc'] = COMMASPACE.join(cc_to)

    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(email_body, 'html'))


    for f in files:
        part = MIMEBase('application', "octet-stream")
        part.set_payload( open(f,"rb").read() )
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(f))
        msg.attach(part)

    smtp = smtplib.SMTP(SMTP)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()
