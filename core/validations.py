import pandas as pd
import numpy as np
import logging

#------------------------------------------------------------------------------------------------
def validate_timeseries_object(ts, is_dataframe = False):
#------------------------------------------------------------------------------------------------
    """
    This function is used to validate whether the object is a valida pandas time series. In the Macro application context, a dataframe holds timeserieses as its column.
    This function will raise alerts if validation fails
    :param ts: the object to be validated
    :return: return a validated ts_obj and whose index column name is 'DATE'
    """
    # check 1
    if is_dataframe:
        if isinstance(ts, pd.DataFrame):
            logging.debug("Check1: The input obj is a instance of pd.Dataframe")
        else:
            logging.error("Check1: The input obj is NOT a valid pd.Dataframe")
            raise TypeError("Check1: The input obj is NOT a valid pd.Dataframe. Validation failed.")
    else:
        if isinstance(ts, pd.Series):
            logging.debug("Check1: The input obj is a instance of pd.Series" )
        else:
            logging.error("Check1: The input obj is NOT a valid pd.Series")
            raise TypeError("Check1: The input obj is NOT a valid pd.Series. Validation failed.")

    #check 2
    if isinstance(ts.index,pd.DatetimeIndex):
        logging.debug("Check2: The input obj index is a pd.DatetimeIndex")
    else:
        logging.error("Check2:The input series obj index is expected to be a pd.DatetimeIndex")
        raise TypeError("Check2:The input series obj index is expected to be a pd.DatetimeIndex")
    #check 3
    if not ts.empty:
        logging.debug("Check3: The input timeseries obj is not empty")
    else:
        logging.warn("Check3: The input timeseries obj is empty!")

    ts_out = ts.copy()
    ts_out.index.name = "DATE"
    return ts_out




