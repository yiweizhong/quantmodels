"""productivity toolbox
"""
from __future__ import division
import numpy as np
import pandas as pd
import datetime as dt
import collections
#from func import each as f_each
from pandas.tseries.offsets import *
from pytz import timezone
import logging


def ts(data, dates, key ="DATE"):
    """bootstrap ts
    Parameters
    ----------
    data: float list or set
        raw data
    dates: string list or set in yyyy-MM-dd format
        date index

    Returns
    -------
    float series indexed with dates
        
    """
    data = pd.Series(data=data, index=dates)
    data.index = pd.to_datetime(data.index)
    data.index.name = key
    return data

def tframe(data, key ="DATE", utc=False, local_tz='Australia/Sydney'):  
    """bootstrap date keyed dataframe
    Parameters
    ----------
    data: dataframe
        The raw dataframe.
    key: string
        The name of the column to be used as the key
    utc:bool
        A flag to suggest whether the datetime is utc
    local_tz:string
        A string indicates the local_tz name
    Returns
    -------
    float series indexed with dates
    """  
    
    data[key] = pd.to_datetime(data[key], utc=utc)
    data = data.set_index(key)    
    if utc == True:
        data.index.tz_localize('UTC').tz_convert(local_tz)        
    return data


def merge_dataframess(table_main, table_new):
    """This function merge any missing row from the table_new to table_main based
    on the dataframe key.
    Parameters
    ----------
    table_main: dataframe 
        The main dataframe.
    table_new:  dataframe
        The dataframe contains newer data.

    Returns
    -------
    dataframe: A new dataframe containing all the rows in the table_main and
    any row from table_new that is missing from table_main.
    """
    new_data_keys = list(set(table_new.index.tolist()) - set(table_main.index.tolist()))
    result = pd.concat([table_main, table_new.ix[new_data_keys, :]])
    return result


def validate_list(input, allow_empty=True):
    """ valid if the input is a valid list object
    Args:
        input (a list of something)
        allow_empty (bool): whether a completely empty is considered valid
    Returns"
        (bool): it will only return true or crash out
    """
    if not (isinstance(input, list)):
        raise TypeError("The input is expected to be a list object")
    if (not(allow_empty)) and len(input) <= 0:
        raise TypeError("The input is expected to be a non empty list object")
    
    return True


def validate_timeseries_data(ts, is_df=True):
    
    """ This function is used to validate the dataframe object used to hold time series. In the Macro application context, a dataframe holds timeserieses as its column.
    Args:    
        ts (pandas timeseries or pandas dataframe) data to be validated
    :param is_df: the input is a series if this flag is False
    :return:
    """

    if is_df:
        data_type = "dataframe"
         # check 1
        if isinstance(ts, pd.DataFrame):
            logging.debug("Check1: The input timeseries obj is a valid %s" % (data_type))
        else:
            raise TypeError("The input timeseries obj is NOT a valid %s" % (data_type))
    else:
        data_type = "series"
         # check 1
        if isinstance(ts, pd.Series):
            logging.debug("Check1: The input timeseries obj is a valid %s" % (data_type))
        else:
            raise TypeError("The input timeseries obj is NOT a valid %s" % (data_type))

    #check 2
    if not ts.empty:
        logging.debug("Check2: The input timeseries obj is not empty")
    else:
        logging.warn("Check2: The input timeseries obj is empty!")

    #check 3
    if ts.index.name == "DATE":  #breaking change
        logging.debug("Check3: The input timeseries obj index column name is DATE")
    else:
        logging.error("The input timeseries obj index column name is expected to be upper case %s but received %s" % ("DATE",ts.index.name))
        raise ValueError("The input timeseries obj index column name is expected to be  upper case %s but received %s" % ("DATE",ts.index.name))

    #check 4
    if ts.index.dtype == np.dtype(r"datetime64[ns]"): #breaking change
        logging.debug("Check4: The input timeseries obj index column datatype is %s" % r"datetime64[ns]")
    else:
        logging.error("The input timeseries obj index column datatype is expected to be %s but received %s" % (r"datetime64[ns]",ts.index.dtype))
        raise TypeError("The input timeseries obj index column datatype is expected to be %s but received %s" % (r"datetime64[ns]",ts.index.dtype))

    return True


#------------------------------------------------------------------------------------------------
def realign_ts_to_weekdays(ts, start_date=None, end_date=None):
#------------------------------------------------------------------------------------------------
    date_range = pd.date_range(ts.index.min(), ts.index.max(), freq=BDay()) if (start_date is None or end_date is None) else pd.date_range(start_date, end_date, freq=BDay())
    ts_out = ts.reindex(date_range).fillna(method="ffill")
    return ts_out


#------------------------------------------------------------------------------------------------
def create_empty_numeric_df(index, col_names):
#------------------------------------------------------------------------------------------------
    return pd.DataFrame(index=index, columns=col_names).astype("float64")

#------------------------------------------------------------------------------------------------
def create_empty_numeric_series(index, col_name):
#------------------------------------------------------------------------------------------------
    return pd.Series(index=index).astype("float64").rename(col_name)

#------------------------------------------------------------------------------------------------
def trim_series(ts):
#------------------------------------------------------------------------------------------------
    fi = ts.first_valid_index()
    li = ts.last_valid_index()
    return ts[fi:li+1]

#------------------------------------------------------------------------------------------------
def ltrim(pdobj):
#------------------------------------------------------------------------------------------------
    fi = pdobj.first_valid_index()
    return pdobj[fi:]

#------------------------------------------------------------------------------------------------
def rtrim(pdobj):
#------------------------------------------------------------------------------------------------
    li = pdobj.first_valid_index()
    return pdobj[:li+1]

#------------------------------------------------------------------------------------------------
def ltrim_n_from_series(ts, n, valid_dp_only=False):
#------------------------------------------------------------------------------------------------
    if valid_dp_only:
        ts = ltrim(ts)
        return ts [n:ts.size]
    else:
        return ts[n:ts.size]


#------------------------------------------------------------------------------------------------
def rolling_partition_df(df, window_size, **kwargs):
    """
    Partition a dataframe into certain window size. The apply() function will then be called on the return value of this function
    The apply() function will then take an function as input to process the data from the partitioned dataframe
    :param df dataframe: a dataframe containing the raw data
    :param window_size int: the window size into which the original dataframe will be partitioned in
    :return: a pandas groupby object that an apply function can be called.
    """
# ------------------------------------------------------------------------------------------------
    roll_array = np.dstack([df.values[i:i + window_size, :] for i in range(len(df.index) - window_size + 1)]).T
    panel = pd.Panel(roll_array,
                     items=df.index[window_size - 1:],
                     major_axis=df.columns,
                     minor_axis=pd.Index(range(window_size), name='roll'))
    return panel.to_frame(filter_observations=False).unstack().T.groupby(level=0, **kwargs)


#------------------------------------------------------------------------------------------------
def rolling_apply(df, window_size, func, min_window_size=None):
    """
    Partition a dataframe into certain window size and apply func to the partitioned dataframe
    :param df dataframe: a dataframe containing the raw data
    :param window_size int: the window size into which the original dataframe will be partitioned in
    :param func function: a function that will take the partioned dataframe
    :param min_window_size: the number of minimal number of data points must be available before
                            func result is valid
    :return: a series which is the result of func being applied to each partioned
             dataframe
    """
# ------------------------------------------------------------------------------------------------
    if min_window_size is None:
        min_window_size = window_size
    result = pd.Series(np.nan, index=df.index)
    for i in range(1, len(df)+1):
        sub_df = df.iloc[max(i - window_size, 0):i, :] #I edited here
        if len(sub_df) >= min_window_size:
            idx = sub_df.index[-1]
            result[idx] = func(sub_df)
    return result


#------------------------------------------------------------------------------------------------
def weight_df_by_selected_columns(df, selected_cols = None):
#------------------------------------------------------------------------------------------------
    if selected_cols is None:
        return df.apply(lambda r : r * 1.0/np.sum(r), axis = 1)
    else:
        df = recolumn_df_data(df.copy(), cols = selected_cols)
        return df.apply(lambda r : r * 1.0/np.sum(r), axis = 1)


#------------------------------------------------------------------------------------------------
def weight_df_by_selected_columns_as_ratio_to_a_col(df, based_col, selected_cols = None):
#------------------------------------------------------------------------------------------------
    if selected_cols is None:
        return df.apply(lambda r : r * 1.0/r[based_col], axis = 1)
    else:
        cols = selected_cols[:]
        if based_col not in selected_cols: selected_cols.append(based_col)
        df = recolumn_df_data(df.copy(), cols = cols)
        return df.apply(lambda r : r * 1.0/r[based_col], axis = 1)


#------------------------------------------------------------------------------------------------
def recolumn_df_data(df, cols=None):
#------------------------------------------------------------------------------------------------
    #validate_timeseries_data(df)
    if cols is None:
        return df
    else:
        df_new = df.copy()
        for c in cols:
            if c in df_new.columns.tolist():
                pass
            else:
                df_new[c] =np.nan
        return df_new[list(cols)]
