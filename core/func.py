from __future__ import division
import os
import zipfile
import collections
from functools import wraps
import logging

def each(fn, items):
    """
    A functional wrapper to allow iter like operation.
    It will iterate through a list of object and use its element(s) to call a certain function.
    This function has side affect
    :param fn: function to be applied
    :param items: the item from the list like object
    :return: None
    """
    for item in items:
        if isinstance(item, collections.Sequence):
            fn(*item)
        else:
            fn(item)
    return


def archive(source_dir,zip_file_name):
    """
    a function to archive a dir to a zip file
    :param source_dir: the (relative/absolute) dir whose content will be zipped and copied into an archive file
    :param zip_file_name: the FULL file path of the zipped file.
    :return: None
    """
    ziphandler = zipfile.ZipFile(zip_file_name,"w")
    for root,dirs,files in os.walk(source_dir):
        for file in files:
            ziphandler.write(os.path.join(root,file))
    return None


def trace(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        logging.info(">>>> Function %s(%r, %r) START >>>>" % func.__name__,args, kwargs)
        result = func(*args, **kwargs)
        logging.info("<<<< Function %s(%r, %r) END <<<<" % func.__name__,args, kwargs)
        return result

