import warnings
import numpy as np
import pandas as pd
import scipy.stats as st
import statsmodels.api as sm
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from cycler import cycler
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations
from pandas.tseries.offsets import DateOffset
from scipy import stats
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, fcluster
from scipy.spatial.distance import pdist, squareform

    
#matplotlib.rcParams['figure.figsize'] = (16.0, 12.0)
#matplotlib.style.use('ggplot')


# Create models from data
def best_fit_distribution(data, bins=200, ax=None):
    
    """Model data by finding best fit distribution to data"""
    
    # Get histogram of original data
    y, x = np.histogram(data, bins=bins, density=True)
    x = (x + np.roll(x, -1))[:-1] / 2.0

    # Distributions to check from scipy
    DISTRIBUTIONS = [        
        st.alpha,st.anglit,st.arcsine,st.beta,st.betaprime,st.bradford,st.burr,st.cauchy,st.chi,st.chi2,st.cosine,
        st.dgamma,st.dweibull,st.erlang,st.expon,st.exponnorm,st.exponweib,st.exponpow,st.f,st.fatiguelife,st.fisk,
        st.foldcauchy,st.foldnorm,st.genlogistic,st.genpareto,st.gennorm,st.genexpon,
        st.genextreme,st.gausshyper,st.gamma,st.gengamma,st.genhalflogistic,st.gilbrat,st.gompertz,st.gumbel_r,
        st.gumbel_l,st.halfcauchy,st.halflogistic,st.halfnorm,st.halfgennorm,st.hypsecant,st.invgamma,st.invgauss,
        st.invweibull,st.johnsonsb,st.johnsonsu,st.ksone,st.kstwobign,st.laplace,st.levy,st.levy_l,st.levy_stable,
        st.logistic,st.loggamma,st.loglaplace,st.lognorm,st.lomax,st.maxwell,st.mielke,st.nakagami,st.ncx2,st.ncf,
        st.nct,st.norm,st.pareto,st.pearson3,st.powerlaw,st.powerlognorm,st.powernorm,st.rdist,st.reciprocal,
        st.rayleigh,st.rice,st.recipinvgauss,st.semicircular,st.t,st.triang,st.truncexpon,st.truncnorm,st.tukeylambda,
        st.uniform,st.vonmises,st.vonmises_line,st.wald,st.weibull_min,st.weibull_max,st.wrapcauchy
    ]

    # Best holders
    best_distribution = st.norm
    best_params = (0.0, 1.0)
    best_sse = np.inf

    # Estimate distribution parameters from data
    for distribution in DISTRIBUTIONS:

        # Try to fit the distribution
        try:
            # Ignore warnings from data that can't be fit
            with warnings.catch_warnings():
                warnings.filterwarnings('ignore')

                # fit dist to data
                params = distribution.fit(data)

                # Separate parts of parameters
                arg = params[:-2]
                loc = params[-2]
                scale = params[-1]

                # Calculate fitted PDF and error with fit in distribution
                pdf = distribution.pdf(x, loc=loc, scale=scale, *arg)
                sse = np.sum(np.power(y - pdf, 2.0))

                # if axis pass in add to plot
                try:
                    if ax:
                        pd.Series(pdf, x).plot(ax=ax)
                    end
                except Exception:
                    pass

                # identify if this distribution is better
                if best_sse > sse > 0:
                    best_distribution = distribution
                    best_params = params
                    best_sse = sse

        except Exception:
            pass

    return (best_distribution.name, best_params)



def make_pdf(dist, params, size=10000):
    """Generate distributions's Probability Distribution Function """

    # Separate parts of parameters
    arg = params[:-2]
    loc = params[-2]
    scale = params[-1]

    # Get sane start and end points of distribution
    start = dist.ppf(0.01, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.01, loc=loc, scale=scale)
    end = dist.ppf(0.99, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.99, loc=loc, scale=scale)

    # Build PDF and turn into pandas Series
    x = np.linspace(start, end, size)
    y = dist.pdf(x, loc=loc, scale=scale, *arg)
    pdf = pd.Series(y, x)

    return pdf


def plot_best_fit_pdf(data, bin_size=20, fig_size=(12,8), title_prefix='', xaxis_labels = '', yaxis_labels='', ax=None):
    
    
    # Find best fit distribution
    best_fit_name, best_fit_params = best_fit_distribution(data, bin_size)
    
    #using the distribute name to get the distribution out scipy stats
    best_dist = getattr(st, best_fit_name)
    
    # Make PDF for the best fit with best params 
    pdf = make_pdf(best_dist, best_fit_params)
    
    
    param_names = (best_dist.shapes + ', loc, scale').split(', ') if best_dist.shapes else ['loc', 'scale']
    param_str = ', '.join(['{}={:0.2f}'.format(k,v) for k,v in zip(param_names, best_fit_params)])
    dist_str = '{}({})'.format(best_fit_name, param_str)
    
    pdf.name = dist_str
    
    # Display
    
    _ax = None
    
    if ax is None:
        plt.figure(figsize=fig_size)
        _ax = pdf.plot(lw=2, legend=True)    
    else:
        _ax = ax
        pdf.plot(lw=2, legend=True, ax=_ax)
    
    #_axlabel='{} PDF'.format(best_fit_name)
    
    data.plot(kind='hist', bins=bin_size, density=True, alpha=0.5, legend=True, ax=_ax)
    
    
    
    _ax.set_title( title_prefix)
    _ax.set_xlabel(xaxis_labels)
    _ax.set_ylabel(yaxis_labels)
    
    return _ax

def plot_fly(fly_data):
    
    _left = fly_data.iloc[:,:2]
    _right = fly_data.iloc[:,1:]
    
    _bve = pd.DataFrame({
        'WINGS':fly_data.iloc[:,[0,2]].sum(axis=1),
        'BELLY':fly_data.iloc[:,1] * 2
        })
    
    _edge = fly_data.iloc[:,[0,2]]
    
    
    _f = plt.figure(figsize = (30, 10))
    _gs = gridspec.GridSpec(3, 4, height_ratios=[1,1,1], width_ratios=[1,1,1,1])
    _ax0 = plt.subplot(_gs[0])
    _ax1 = plt.subplot(_gs[1])
    _ax2 = plt.subplot(_gs[2])
    _ax3 = plt.subplot(_gs[3])
    _ax4 = plt.subplot(_gs[4])
    _ax5 = plt.subplot(_gs[5])
    _ax6 = plt.subplot(_gs[6])
    _ax7 = plt.subplot(_gs[7])
    _ax8 = plt.subplot(_gs[8])
    _ax9 = plt.subplot(_gs[9])
    _ax10 = plt.subplot(_gs[10])
    _ax11 = plt.subplot(_gs[11])
        
    plot_curve(_left, axes=(_ax0, _ax4, _ax8))
    plot_curve(_right, axes=(_ax1,_ax5, _ax9))
    plot_curve(_edge, axes=(_ax2, _ax6, _ax10))
    plot_curve(_bve, axes=(_ax3, _ax7, _ax11), curve=False)
       
    
def plot_curve(curve_data, curve=True, axes = None):
    
    chart_type = 'CURVE' if curve == True else 'SPREAD'
    
    title = '{}/{} curve'.format(curve_data.columns.tolist()[1], curve_data.columns.tolist()[0]) if curve == True else \
    '{} spread {}'.format(curve_data.columns.tolist()[1], curve_data.columns.tolist()[0])
    
    s = 'S' if curve == True else 'W'
    f = 'F' if curve == True else 'N'
        
    data = curve_data.dropna()    
    data_g = data.iloc[-1,:] - data.iloc[0,:]
    
    
    data = data.rename(lambda x: '{}_{:.2f}/{:.2f}'.format(x, data.iloc[-1,:][x], data_g[x]), axis=1)
            
    curve = data.iloc[:,1] - data.iloc[:,0]
    curve_g = curve.iloc[-1] - curve.iloc[0]
    curve.name = '{}_{:.2f}/{:.2f}'.format(chart_type, curve.iloc[-1], curve_g)
    
    
    vol_curve =  data.diff().pow(2).apply(lambda x: x.iloc[1] - x.iloc[0], axis=1)
    vol_curve.name = 'VOL_{}'.format(chart_type)
    
    
    _ax0 = None
    _ax1 = None
    _ax2 = None
    
    if axes is None:        
        _f = plt.figure(figsize = (10, 10))
        _gs = gridspec.GridSpec(3, 1, height_ratios=[1,1,1], width_ratios=[1])
        _ax0 = plt.subplot(_gs[0])
        _ax1 = plt.subplot(_gs[1])
        _ax2 = plt.subplot(_gs[2])
    else:
        (_ax0, _ax1, _ax2) = axes

    
    #chart 1
    pd.concat([data, curve], axis=1).iloc[:,:2].plot(ax=_ax0, fontsize=6)    
    _ax0t = _ax0.twinx()
    _ax0t._get_lines.prop_cycler = _ax0._get_lines.prop_cycler
    
    pd.concat([data, curve], axis=1).iloc[:,2].plot(ax=_ax0t, fontsize=6)
    _lines, _labels = _ax0.get_legend_handles_labels()
    _line, _label = _ax0t.get_legend_handles_labels()
    
    _lines += _line
    _labels += ['{}(r)'.format(_label[0])]        
    _leg = _ax0.legend(_lines, _labels, fancybox=True, prop={'size': 6})           
    _leg.get_frame().set_facecolor('none')
    _leg.get_frame().set_linewidth(0.0)
    
            
    #chart 2
    #pd.concat([curve, vol_curve], axis=1).plot(ax=_ax1, fontsize=6)
    curve.plot(ax=_ax1, fontsize=6)
    _ax1t = _ax1.twinx()
    _ax1t._get_lines.prop_cycler = _ax1._get_lines.prop_cycler    
    vol_curve.plot(ax=_ax1t, fontsize=6)
    _lines, _labels = _ax1.get_legend_handles_labels()
    _line, _label = _ax1t.get_legend_handles_labels()
    _lines += _line
    _labels += ['{}(r)'.format(_label[0])]        
    _leg = _ax1.legend(_lines, _labels, fancybox=True, prop={'size': 6})           
    _leg.get_frame().set_facecolor('none')
    _leg.get_frame().set_linewidth(0.0)
    
    _ax1t.axhline(y=0, color="black", lw=0.2)
    
    #chart 3
    classifer = pd.DataFrame({'rtn': curve.diff(), 'vol': vol_curve}).dropna()
    
    bear_s = curve.reindex(classifer.index)[(classifer['rtn'] >= 0) & (classifer['vol'] >= 0)].reindex(classifer.index) 
    bull_f = curve.reindex(classifer.index)[(classifer['rtn'] <= 0) & (classifer['vol'] >= 0)].reindex(classifer.index)
    bear_f = curve.reindex(classifer.index)[(classifer['rtn'] < 0) & (classifer['vol'] < 0)].reindex(classifer.index) 
    bull_s = curve.reindex(classifer.index)[(classifer['rtn'] > 0) & (classifer['vol'] < 0)].reindex(classifer.index)
    
    bear_s_rtn = classifer[(classifer['rtn'] >= 0) & (classifer['vol'] >= 0)]['rtn'] 
    bull_f_rtn = classifer[(classifer['rtn'] <= 0) & (classifer['vol'] >= 0)]['rtn']
    bear_f_rtn = classifer[(classifer['rtn'] < 0) & (classifer['vol'] < 0)]['rtn'] 
    bull_s_rtn = classifer[(classifer['rtn'] > 0) & (classifer['vol'] < 0)]['rtn']
    
    
    classified_curve = pd.DataFrame({'BEAR_{}: {:.2f}'.format(s, bear_s_rtn.dropna().sum()):bear_s, 
                                     'BEAR_{}: {:.2f}'.format(f, bear_f_rtn.dropna().sum()):bear_f, 
                                     'BULL_{}: {:.2f}'.format(s, bull_s_rtn.dropna().sum()):bull_s, 
                                     'BULL_{}: {:.2f}'.format(f, bull_f_rtn.dropna().sum()):bull_f})
    
    data.plot(ax=_ax2, fontsize=6)
    _ax2t = _ax2.twinx()
    _ax2t._get_lines.prop_cycler = _ax2._get_lines.prop_cycler
    
    curve.plot(color='lightgrey', ax=_ax2t, fontsize=6)
    classified_curve.plot(style='.', ax=_ax2t, fontsize=6, legend=False)
   
    _lines, _labels = _ax2.get_legend_handles_labels()
    _lines2, _labels2 = _ax2t.get_legend_handles_labels()
    _lines += _lines2
    _labels += ['{}(r)'.format(_label) for _label in _labels2]        
    _leg = _ax2.legend(_lines, _labels, fancybox=True, prop={'size': 6})           
    _leg.get_frame().set_facecolor('none')
    _leg.get_frame().set_linewidth(0.0)    
   
    
    _ax0.xaxis.set_label_text("")
    _ax1.xaxis.set_label_text("")
    _ax2.xaxis.set_label_text("")
           
    _ax0.set_title(title, fontsize=8)
    plt.tight_layout()
    
  

def merge_dataframess(table_main, table_new, axis=1):
    """This function merge any missing row from the table_new to table_main based
    on the dataframe key.
    Parameters
    ----------
    table_main: dataframe 
        The main dataframe.
    table_new:  dataframe
        The dataframe contains newer data.
    axis: int
        1 means merge in new columns and 0 means merge in new rows

    Returns
    -------
    dataframe: A new dataframe containing all the columns rows in the table_main and
    any row from table_new that is missing from table_main.
    """
    
    
    if axis == 1:
        #when merge by columns the row index will be based on the existing table
        new_cols = [c for c in table_new.columns if c not in table_main.columns]
        return pd.concat([table_main, 
                          table_new[new_cols].reindex(table_main.index, axis=0)],
                         axis=1)
    elif axis == 0:
        #when merge by row the new and old tables are assumed to have the same 
        #table column formation
        if table_new.columns.tolist() != table_new.columns.tolist():
            raise TypeError("Can't merge the rows from the main and new table \
                            due to different column formations")
        new_rows = [r for r in table_new.index if r not in table_main.index]
        return pd.concat([table_main,
                          table_new.loc[new_rows,:]],
                          axis=0)
        
        
















