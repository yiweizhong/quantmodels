from __future__ import division
import math
import numpy as np
import pandas as pd
import statsmodels.tsa.stattools as ts
import functools
from core import tools



def calc_next_n_periods_moves(vals, n=1): 
    return (vals.shift(-1 * n) - vals).reindex(vals.index)

def calc_prev_n_periods_moves(vals, n=1):
    return (- vals.shift(1 * n) + vals).reindex(vals.index)

def find_neighbours_in_series(ds, x, number_of_neighbour):
    """ find n number of neighbours around x in ds
    """
    xs = ds.dropna().index.values # always in assending order
    ys = ds.dropna().values

    l = len(xs)
    n = math.ceil(number_of_neighbour/2)
    
    if n * 2 > l:
        raise RuntimeError("not enough data to locate {} neighbours".format(number_of_neighbour))

    left = []
    right = []

    # 
    if x <= xs[0]:
        return ds.loc[xs[0:int(n*2)]]
    #    
    if x >= xs[-1]:
        return ds.loc[xs[int(-1*n*2):]]

    #
    for i in np.arange(0, l, step=1):
        if xs[i-1] <= x and x <= xs[i]:
            left = xs[:i][int(-1 * n):]
            right = xs[i:][:int(n)]
            if len(left) < n:
                right =  xs[i:][:int(n + n - len(left))]
            elif len(right) < n:
                left = xs[:i][int(-1 * (n + n - len(right))):]
            break

    return ds.loc[np.append(left, right)]



def calc_momentum(vals):
    if np.count_nonzero(np.isnan(vals)) == 0:
        return (vals[-1] - vals[0])/len(vals)
    else:
        return 0

def create_hit_test(x0, level):
    if(level is None):
        return lambda val: (False, None)
    elif (level >= x0):
        return lambda val: (level <= val, level)
    elif(level < x0):
        return lambda val: (level >= val, level)





def calc_hurst_exp(ts):
	# Create the range of lag values
	lags = range(2, 100)
	# Calculate the array of the variances of the lagged differences
	tau = [np.sqrt(np.std(np.subtract(ts[lag:], ts[:-lag]))) for lag in lags]
	# Use a linear fit to estimate the Hurst Exponent
	poly = np.polyfit(np.log(lags), np.log(tau), 1)
	# Return the Hurst exponent from the polyfit output
	return poly[0]*2.0


def calc_weighted_risk_cix(data, risks):
    for risk, legs in risks.items():
        cix_components = {}
        #for sec, weight in legs.iteritems():
        for sec, weight in legs.items():
           cix_components[sec] = data[sec] * weight
           #print sec, weight
        cix = pd.DataFrame(cix_components).mean(axis=1)
        cix.name = risk
        yield (risk, cix)
