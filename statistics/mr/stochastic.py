"""A set of stochastic process models 
"""
from __future__ import division
import numpy as np
import scipy as sp
import pandas as pd
from pandas.tseries.offsets import BDay,Minute, DateOffset
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib.dates as mpd
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from matplotlib.patches import (Circle,Ellipse)
from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage, AnnotationBbox)
import matplotlib.cm as cm
import matplotlib.ticker as ticker
import statsmodels.tsa.stattools as ts
import helper 



def MLE_estimation_OU(s, t):
    n= len(s) - 1
    sx = np.sum(s[:-1])
    sy = np.sum(s[1:])
    sxx = np.sum(np.square(s[:-1]))
    sxy = np.sum(np.array(s[:-1])* np.array(s[1:]))
    syy = np.sum(np.square(s[1:]))
    a = (n * sxy - sx*sy) / (n * sxx - sx * sx)
    b = (sy - a * sx) / n
    sd = np.sqrt( (n * syy - sy * sy - a * (n *sxy - sx*sy)) /n / (n-2) )
    _lambda = - np.log(a)/t
    _mu = b/(1-a)
    _sigma = sd * np.sqrt(-2 * np.log(a)/t/(1-a*a))
    
    return n, sx, sy, sxx, sxy, syy, (_mu, _sigma, _lambda)

out_test_s = [3, 1.76, 1.2693, 1.196, 0.9468, 0.9532, 0.6252,
              0.8604, 1.0984, 1.4310, 1.3019, 1.4005, 1.2686, 0.7147,
              0.9237, 0.7297, 0.7105, 0.8683, 0.7406, 0.7314, 0.6232]

# =============================================================================
# #
# ou_test(out_test_s,1)
# #(_mu, _sigma, _lambda)
# # (0.9074878882833082, 0.291538037292631, 0.7821830445309641)
# #
# ou_test(out_test_s,0.25)
# #(_mu, _sigma, _lambda)
# #(0.9074878882833082, 0.583076074585262, 3.1287321781238564)
# =============================================================================


def simulate_GBM_SDE(x0, mu, sigma, dt, n):
    # dXt = Xt * mu * dt + xt * sigma * dWt 
    # X(t) = X0 * exp(mu - sigma * sigma/2) * t +  sigma * Wt
    
    _t_delta = dt
    _xs = [np.nan] * n
    _xs[0] = x0
    _steps = np.arange(1, n, step=1)

    for step in _steps:
        _xs[step] = _xs[step - 1] * np.exp((mu - sigma*sigma/2) * _t_delta + sigma * np.random.normal(loc=0.0, scale=np.sqrt(_t_delta)))  
        #_xs[step] = _xs[step - 1] * np.exp((mu - sigma*sigma/2) * _t_delta) 
    return _xs

def simulate_OU_SDE_simple(x0, theta, mu, sigma, dt, n):
    # discretised using Euler-Maruyama method into  
    # dXt = theta * (mu - Xt)dt + sigma * dWt
    # dXt = theta * (mu - Xt)dt + sigma * dWt
    # dXt here refers to Xt+1 - Xt

    _theta = float(theta)    
    _t_delta = dt

    _xs = [np.nan] * n
    _xs[0] = x0
    _steps = np.arange(1, n, step=1)
    for step in _steps:
        _xs[step] = (_xs[step-1] +  _theta * (mu - _xs[step-1]) * _t_delta + sigma * np.random.normal(loc=0.0, scale=np.sqrt(_t_delta)))
    return _xs

def simulate_OU_SDE(x0, theta, mu, sigma, dt, n, test_target, test_stop, mm_factor = 0, mm_window = 5):
    # a better discrete approximation of the OU simulation
    # Xt+1 = exp(- theta * detlat) * Xt + (1 - exp(- theta * deltat)) * mu + sigma * sqrt((1 - exp(- 2 * theta * deltat))/(2 * theta)) * dWt
    # sigma * np.sqrt((1 - np.exp(-2 * _theta_delta_t))/(2 * _theta)) * np.random.normal(loc=0.0, scale=1) is the std of the 
    # err from fitting data for a linear autocorrelation xt = a + b * xt-1 + err. It is the conditional vol
    # sigma is the unconditional vol    
    """ This function simulates the OU SDE
        parameters:
        ----------
        x0: float
            current value. it is the latest point on the historical data
        theta: float
            the speed of mean reversion
        sigma: float
            vol of the mean reverion
        dt: float
            time step size
        n: int
            how many time steps
        test_target: func(float) => (bool, float)
            a function return bool indicating whether hits the target and float for the target level 
        test_stop: func(float) => (bool, float)
            a function return bool indicating whether hits the stop and float for the stop level
        mm_factor: float
            at number between 0 to 1 indicating how much momentum influces do we want to factor into the simulation
        mm_window: int
             the number of days between which the momentum is calculated from
        downside_mm_only: bool
            when this flag is set to true, momentum is only factored if it makes the path away from the target. therefore only the downside momentum is 
            taken into consideration 
        returns:
        -------
        result:  an tuple of (float, int, int) => (xs, stop_hit_step, target_hit_step)
            xs is the path of the OU simulation. it may or may run the full size of n subject to the target and stop level
            stop_hit_step is the step at which the stop is hit. 0 by default
            target_hit_step is the step at which the stop is hit. 0 by default 
    """
    _theta = float(theta)   
    _t_delta = float(dt)
    #_dt = float(dt)
    _theta_delta_t = _theta * _t_delta 
    #_theta_dt = _theta * _t_delta 

    mm_history = [np.nan] * mm_window
    xs = [np.nan] * n
    stops = [0] * n
    targets = [0] * n

    x_prev = x0
    
    for i in np.arange(0, n, 1):
        mm_effect = helper.calc_momentum(mm_history) * mm_factor

        #
        xs[i] = np.exp(-1 * _theta_delta_t) * x_prev + \
                (1 - np.exp(-1 * _theta_delta_t)) * mu + \
                sigma * np.sqrt((1 - np.exp(-2 * _theta_delta_t))/(2 * _theta)) * np.random.normal(loc=0.0, scale=1) + \
                mm_effect
        
        x_prev = xs[i] 

        mm_history = mm_history[1:] + [x_prev]
        
        hit_stop, stop = test_stop(xs[i])
        if (hit_stop):
            xs[i] = stop
            stops[i] = 1
            #return xs, stops, targets
            break
        
        hit_target, target = test_target(xs[i])
        if (hit_target):
            xs[i] = target
            targets[i] = 1
            #return xs, stops, targets
            break
        
    #append the 1st point
    xs = [x0] + xs
    stops = [0] + stops
    targets = [0] + targets
    
    return xs, stops, targets


def linear_autogregession(data, n=1):
    """ This function takes in an array and do an linear autoregression on lag of n
        parameters
        ----------
        data: int
            an array of numbers
        n: int
            the lag. it is default to 1
        returns
        ----------
        a: float 
            the coefficeint for the constant
        b: float
            the coefficient for the first degree independant variable
        errs: an array of float
            the error term    
        xs: an array of float
            the independant variable sequence 
        ys:  an array of float
            the dependant variable sequence 
        y_hats: an array of float
            the estimated dependant variable sequence             
    """    
    
    #xs = data[n:]    
    #ys = data[:-n]
    
    ys = data[n:]    
    xs = data[:-n]
    
    
    (b, a) =  np.polyfit(xs, ys, 1) 
    f = lambda x: a + b * x
    y_hats = [f(x) for x in xs]
    errs = [ y - y_hat for y, y_hat in zip(ys, y_hats)]
    return a, b, errs, xs, ys, y_hats


# =============================================================================
# def test_estimate_unconditional_moments_with_linear_regression(data, dt = 1):
#     """ use the linear regression to estimate the parameters for the OU model
#     """
#     n=1
#     ys = data[n:]    
#     xs = data[:-n]
#     (b, a) =  np.polyfit(xs, ys, 1) 
#     f = lambda x: a + b * x
#     y_hats = [f(x) for x in xs]
#     errs = [ y - y_hat for y, y_hat in zip(ys, y_hats)]        
#     
#     _theta_hat = np.log(b)/(-1 * dt)
#     _mu_hat = a /(1 - b)
#     _sigma_hat = np.std(errs, ddof=1) * np.sqrt((2 * _theta_hat)/(1 - np.exp(-2 * _theta_hat * dt)))         
#     return _mu_hat, _sigma_hat, _theta_hat
# 
# 
# =============================================================================



class OrnsteinUhlenbeck(object):
    #https://en.wikipedia.org/wiki/Ornstein%E2%80%93Uhlenbeck_process
    
    def __init__(self, data, dt =None, resample_freq = 'B'):
        if resample_freq.upper() not in ['B', 'W'] : raise TypeError('resample_freq must be B - business daily, W - weekly')
        if not isinstance(data, pd.Series): raise TypeError("data arg needs to be a pandas time series")
        self._raw_data = data
        
        self._dt = dt if dt is not None else 1        
        
        
        self._n = None
        self._k = None
        self._simulated_paths = None
        self._resample_freq = resample_freq
        self._simulated_path_distribution = None
        self.estimate_unconditional_moments_with_linear_regression()
        #print(self.mu_hat)
        #print(self.data.values[-1] + np.sign(self.data.values[-1]) * 0.5 * abs(self.data.values[-1] - self.mu_hat))
                
        self.simulate_with_model_parameters()         
        self.calculate_conditional_moments()


    def estimate_unconditional_moments_with_linear_regression(self, dt = None):
        """ use the linear regression to estimate the parameters for the OU model
        """
        a, b, errs, xs, ys, y_hats = linear_autogregession(self._raw_data, n=1)
        _dt = self._dt if dt is None else dt
        self._theta_hat = np.log(b)/(-1 * _dt)
        self._mu_hat = a /(1 - b)
        #self._sigma_hat = np.std(errs, ddof=1) * np.sqrt((2 * self._theta_hat)/(1 - np.exp(-2 * self._theta_hat * _dt)))         
        self._sigma_hat = np.std(errs, ddof=1) * np.sqrt((2 * self._theta_hat)/(1 - b*b))                 
        self._dt = _dt

    
    def simulate_with_model_parameters(self, n=None, k=10, target=None, stop = None, momentum_factor = 0, momentum_lag = 5, update=True):
        """ Simulate the OU path based on the estimated model parameters
            parameters:
            -----------
            n:int
                The number of number random draws per simulation. This is the number of days for daily data 
            k: int
                The number of simulation to do done
            target: float
                at which level to stop the simulation as the target is hit
            stop: float 
                at which level to stop the simulation as the stoploss is hit
            include_history: bool
                Whether or not including the historical data used to estimate the model in the output
            momentum_factor: float
                a number between 0 to 1 indicates how much momentum is factored in
            momentum_lag: int
                the number of lag to use for the momentum calc
            returns:
            -----------
            a tuple of 2 dataframes 
            paths: dataframe
                A dataframe containing the simulted paths based on stop loss
            distribution: dataframe
                A dataframe of distribution of hitting target and being stopped out
                
        """

# =============================================================================
#         n = n 
# =============================================================================
        #freq = 'B'
        dt = self.dt

# =============================================================================
#         if self._resample_freq.upper() == 'W':
#          
#             n = int(round(n/5,ndigits=0))
#             #freq = 'W'
#             dt = self.dt
# =============================================================================
        
        #set _n to 3times the half life
        _n = n if n is not None else int(round(self.half_life * 3, 0))
        
        self._n = _n
        self._k = k
        

        if momentum_factor < 0 or momentum_factor > 1: raise  RuntimeError("The momentum factor must be between 0 and 1")
        
        path_ids = ['path_%s' % (i+1) for i in np.arange(k)]
        path_x = {}
        path_stop = {}
        path_target = {}
        x0 = self.data.values[-1]
        test_target = helper.create_hit_test(x0, target)
        test_stop = helper.create_hit_test(x0, stop)
        
        #sim_dates = pd.date_range(start=self.data.index[-1], periods=n + 1, freq=self._resample_freq.upper())[1:]
        
        #include the last historical date as the starting point for the forecast
        sim_dates = pd.date_range(start=self.data.index[-1], periods=_n + 1, freq=self._resample_freq.upper())
        
        
        for path_id in path_ids:
            xs, stops, targets = simulate_OU_SDE(x0, self.theta_hat, self.mu_hat, self.sigma_hat, dt, _n, test_target, test_stop)  
            path_x[path_id] = pd.Series(data=xs,index=sim_dates)
            path_stop[path_id] = pd.Series(data=stops,index=sim_dates)
            path_target[path_id] = pd.Series(data=targets,index=sim_dates)
            

        _simulated_paths = pd.DataFrame(path_x)

        _sim_distribution = pd.DataFrame(
            {
                'stop_hits': pd.DataFrame(path_stop).sum(axis=1),
                'target_hits': pd.DataFrame(path_target).sum(axis=1)
            }    
        )
        _sim_distribution['stop_hits_exp_pct'] =  _sim_distribution['stop_hits'].cumsum()/k
        _sim_distribution['target_hits_exp_pct'] = _sim_distribution['target_hits'].cumsum()/k 
        _sim_distribution['miss_hits_exp_pct'] =  1 - _sim_distribution['stop_hits_exp_pct'] - _sim_distribution['target_hits_exp_pct']
        _sim_distribution['x_mean'] = _simulated_paths.mean(axis=1)
        _sim_distribution['x_std'] = _simulated_paths.std(axis=1)
        
        
        if(update):
            self._simulated_paths = _simulated_paths        
            self._simulated_path_distribution = _sim_distribution
            # keep the conditional moment in sync
            self.calculate_conditional_moments()

        return _simulated_paths, _sim_distribution

               
        



    def eistimate_fractional_life(self, fraction):
        """ calcualte the time in days for the process's conditional expected value to revert to the long
            run mean from its current value based on the estimated parameters.                        
            returns:
            --------
            life in days: double
        """
        if fraction > 1 or fraction <= 0:
             raise TypeError("The fraction is a positive number that is > 0 and <= 1")         

        return np.log(1/fraction)/self.theta_hat        
    
    def estimate_conditional_decay_rate(self):
        """ calcualte the conditional and unconditional decay rate based on the estimated parameters.                        
            returns:
            --------
            decay_rates: an dictionary of doubles
                decay rates 
        """
        steps = self.n + 1
        f = lambda t: np.exp(-1 * self.theta_hat * t)
        decay_rates = {step:f(step) for step in np.arange(1, steps + 1, 1)}
        decay_rates['unconditional'] = 1
        return decay_rates        


    def calculate_conditional_moments(self, n = None):
        """ calcualte the conditional and unconditional sharpe ratios based on the estimated parameters            
            returns:
            --------
            : an dataframe containing conditional/unconditional mean, variance, sharpe ratio
        """
    
        #use property rather than private var
        steps = self.n if n is None else n
        x0 = self.data.values[-1]
        mu = self.mu_hat
        theta = self.theta_hat
        sigma = self.sigma_hat
        sim_dates = pd.date_range(start=self.data.index[-1], periods=steps + 1, freq=self._resample_freq.upper())[1:]

        fm = lambda t: mu + (x0 - mu) * np.exp(-1*theta*t)
        fv = lambda t: sigma * sigma * (1 - np.exp(-2 * theta * t)) / (2 * theta)

        means = [fm(step) for step in np.arange(1, steps + 1, 1)]
        unconditional_mean = [mu] * len(means)

        variances = [fv(step) for step in np.arange(1, steps + 1, 1)]
        unconditional_variances = [sigma * sigma / (2 * theta)]  * len(variances)

        result = pd.concat([
                            pd.Series(data = means, index = sim_dates, name = "conditional_mean"),
                            pd.Series(data = unconditional_mean, index = sim_dates, name = "unconditional_mean"),
                            pd.Series(data = variances, index = sim_dates, name = "conditional_variance"),
                            pd.Series(data = unconditional_variances, index = sim_dates, name = "unconditional_variance")], 
                            axis = 1)
        
        #dayoffsets = list(252/(result.index - self.data.index[-1]).days)
        #exp_day_factor = pd.Series(data=np.sqrt(dayoffsets), index=result.index)
        exp_day_factor = np.sqrt(252 / np.arange(1, len(result.index) + 1, 1))

        if self._resample_freq.upper() == 'W':
            exp_day_factor = np.sqrt(52 / np.arange(1, len(result.index) + 1, 1))


        result['conditional_stdev'] = result['conditional_variance'].apply(np.sqrt)
        
        if mu <= x0:
            result['conditional_sharpe_ratio'] = exp_day_factor * (x0 - result['conditional_mean'])/result['conditional_stdev']  
        else:
            result['conditional_sharpe_ratio'] = exp_day_factor * (result['conditional_mean'] - x0)/result['conditional_stdev']
        
        result['conditional_mean_u_2std'] = result['conditional_mean'] + 2 * result['conditional_stdev']
        result['conditional_mean_d_2std'] = result['conditional_mean'] - 2 * result['conditional_stdev']

        self._moments = result

        self._conditional_reversion_potential = abs(self.data[-1] - self.conditional_means[-1])

        return result


    @property
    def n(self):
        if self._n is None:
            raise  RuntimeError("The model currently has not done any simulation data yet.")
        return self._n


    @property
    def k(self):
        if self._k is None:
            raise  RuntimeError("The model currently has not done any simulation data yet.")
        return self._k

    @property
    def dt(self):
        return self._dt

    @property
    def data(self):
        return self._raw_data 

    @property
    def theta_hat(self):
        '''the reversion speed'''
        return self._theta_hat
    
    @property
    def mu_hat(self):
        ''' the mean'''
        return self._mu_hat

    @property
    def sigma_hat(self):
        '''the vol'''
        return self._sigma_hat

    @property
    def simulated_paths(self):
        if self._simulated_paths is None:
            raise  RuntimeError("The model currently has not done any simulation data yet.")
        return self._simulated_paths

    @property
    def simulation_distribution(self):
        if self._simulated_path_distribution is None:
            raise  RuntimeError("The model currently has not done any simulation data yet.")
        return self._simulated_path_distribution
        
    @property
    def conditional_means(self):
        return self._moments['conditional_mean']

    @property
    def conditional_reversion_potential(self):
        return self._conditional_reversion_potential 
    
    @property
    def conditional_variances(self):
        return self._moments['conditional_variance']
    
    @property
    def conditional_stdevs(self):
        return self._moments['conditional_stdev']

    @property
    def conditional_sharpe_ratios(self):
        return self._moments['conditional_sharpe_ratio']

    @property
    def moments(self):
        return self._moments
    
    @property
    def half_life(self):     
        return self.eistimate_fractional_life(1/2)

    @property
    def fractional_life(self):        
        lifes = { f : self.eistimate_fractional_life(1/f) for f in np.arange(1, 11, 1)}
        return pd.Series(data=lifes.values(),index=lifes.keys(),name ="fractional life")

    @property
    def hurst_exp(self):
        h = helper.calc_hurst_exp(self._raw_data.values)
        return h
    
    @property
    def adf(self, lag=1):
        test_stats, _, _, _, t_stats, _ = ts.adfuller(self._raw_data.values)
        return (test_stats, t_stats)

    
    def plot_hitstop_distribution(self, ax, xlim = None, ylim = None):
        
        
        _stop_level = self.data.values[-1] + np.sign(self.data.values[-1]) * 0.5 * abs(self.data.values[-1] - self.mu_hat)
        _sim_path, _sim_distribution = self.simulate_with_model_parameters(k=1000, target=self.mu_hat, 
                                                                           stop=_stop_level, update=False ) 
        

        sr = _sim_distribution[['stop_hits','target_hits']].rename(
            columns ={
                'stop_hits':'stopped out at ({})'.format(round(_stop_level, 2)),
                'target_hits' : 'hit target at ({})'.format(round(self.mu_hat, 2))                
            }
        )

        sr.plot(ax=ax, label= 'stop level half of the exp profit', lw=0.7)
        #ax_twinx.set_ylim((self.moments['conditional_sharpe_ratio'].min(),self.moments['conditional_sharpe_ratio'].max()))


        plt.setp(ax.xaxis.get_majorticklabels(), fontsize = 6)
        plt.setp(ax.xaxis.get_minorticklabels(), fontsize = 6)
        plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 6)

        #
        leg = ax.legend(fancybox=True, prop={'size': 7}, ncol=1)
        leg.get_frame().set_facecolor('none')
        leg.get_frame().set_linewidth(0.0)

        #
        if xlim is not None: ax.set_xlim(xlim)
        if ylim is not None: ax.set_ylim(ylim)
        ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
        ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')



    def plot_ou_sr(self, ax, xlim = None, ylim = None):

        sr = self.moments['conditional_sharpe_ratio']
        sr.plot(ax=ax, label= 'exp sharpe ratio', lw=0.7)
        #ax_twinx.set_ylim((self.moments['conditional_sharpe_ratio'].min(),self.moments['conditional_sharpe_ratio'].max()))


        plt.setp(ax.xaxis.get_majorticklabels(), fontsize = 6)
        plt.setp(ax.xaxis.get_minorticklabels(), fontsize = 6)
        plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 6)

        #
        leg = ax.legend(fancybox=True, prop={'size': 7}, ncol=2)
        leg.get_frame().set_facecolor('none')
        leg.get_frame().set_linewidth(0.0)

        #
        if xlim is not None: ax.set_xlim(xlim)
        if ylim is not None: ax.set_ylim(ylim)
        ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
        ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')

    
    def plot_ou_moments_ax(self, ax, xlim = None, ylim = None):
        
        moments = self.moments[[
            'conditional_mean', 
            'conditional_mean_u_2std', 
            'conditional_mean_d_2std']]

        a_single_sim_path = pd.DataFrame({'path_1' :self._simulated_paths.mean(axis=1)})
        existing_history = self.data

        chart_data = pd.concat([moments, a_single_sim_path, existing_history],axis=1).rename(
            columns ={
                'conditional_mean':'conditional expected value',
                'conditional_mean_u_2std' : '+2 $\sigma$', 
                'conditional_mean_d_2std' : '-2 $\sigma$',
                'path_1': 'a random simulation of asset price'
            }
        )

        chart_data.plot(ax= ax, lw=1)

        pd.Series(data = [self.mu_hat] * len(chart_data.index), index= chart_data.index).plot(
            ax= ax, lw=0.7, c='grey', ls = '--', label='unconditional mean')




        #
        plt.setp(ax.xaxis.get_minorticklabels(), fontsize = 6)
        plt.setp(ax.xaxis.get_majorticklabels(), fontsize = 6)
        plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 6)

        #
        ax.set_ylabel('Level', fontsize=8)
        #ax_twinx.set_ylabel('Sharpe ratio', fontsize=8)
        #
        leg = ax.legend(fancybox=True, prop={'size': 7}, ncol=2)
        leg.get_frame().set_facecolor('none')
        leg.get_frame().set_linewidth(0.0)

        #
        if xlim is not None: ax.set_xlim(xlim)
        if ylim is not None: ax.set_ylim(ylim)
        ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
        ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')

        #print chart_data
        #chart_data.plot()



if(1 > 2):
    
# =============================================================================
#     ou_test(out_test_s,1)
# #(_mu, _sigma, _lambda)
# # (0.9074878882833082, 0.291538037292631, 0.7821830445309641)
# #
# ou_test(out_test_s,0.25)
# #(_mu, _sigma, _lambda)
# #(0.9074878882833082, 0.583076074585262, 3.1287321781238564)
# 
#     
# =============================================================================
    

    def sim(x_prev, theta, mu, sigma, dt, random):
        
        x = np.exp(-1 * theta * dt) * x_prev + \
                (1 - np.exp(-1 * theta * dt)) * mu + \
                sigma * np.sqrt((1 - np.exp(-2 * theta * dt))/(2 * theta)) * random 
        return x
    
    
    sim(sim(3, 3, 1, 0.5, 0.25, -1.0268),  3, 1, 0.5, 0.25, -0.4985)
    
   
    
    #test simulate gbm
    x0 = 3
    mu = 1
    sigma = 0.5
    dt = 1
    theta = 3
    test_target = helper.create_hit_test(None, None)
    test_stop = helper.create_hit_test(None, None)
    simulation_steps = 50
    n = simulation_steps
    simulations = 5000
    #output = pd.DataFrame({s: simulate_GBM_SDE(x0, mu, sigma, dt/50,  simulation_steps) for s in np.arange(simulations)})
    #
    output = pd.DataFrame({s: simulate_OU_SDE(x0, theta, mu, sigma, dt, n, test_target, test_stop, mm_factor = 0, mm_window = 5)[0]
                   for s in np.arange(simulations)})
    output_summ = pd.DataFrame({'mean':output.mean(axis=1), 
                                '+1 std': output.mean(axis=1) + output.std(axis=1),
                                '-1 std': output.mean(axis=1) - output.std(axis=1),
                                'std': output.std(axis=1) / np.sqrt((1 - np.exp(-2 * theta * dt))/(2 * theta))
                                })
    output_summ.plot()
    