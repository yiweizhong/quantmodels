from __future__ import division
import numpy as np
import pandas as pd
from pandas.tseries.offsets import BDay,Minute, DateOffset
import statsmodels.tsa.stattools as ts
import functools
from core import tools
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib.dates as mpd
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from matplotlib.patches import (Circle,Ellipse)
from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage, AnnotationBbox)
import matplotlib.cm as cm
import matplotlib.ticker as ticker
from collections import namedtuple
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import helper as helper 
import stochastic as st
import numbers 


class sbin(object):
    
    def __init__(self, bin_id, left_edge, right_edge, last_bin=False):
        """    
        """
            
        if (isinstance(left_edge,numbers.Number) and isinstance(right_edge,numbers.Number) and left_edge <= right_edge):
            pass
        else:
            raise ValueError("The left_edge must be equal or less than the right_edge and both edges need to be numerical")
    

        self._id = bin_id
        self._edges = (left_edge, right_edge)
        self._last_bin = last_bin
        #self._content = []    
        self._content = None
        
    
    
    def isin(self, x):
        """ check if x is within the edges
            parameters:
            ----------- 
                x: num 
                    a number to be compared against the edges
            Returns:
            -------
                result: bool 
                    a bool indicating whether x is within the bin edges
        """
        success = False
        if(self._last_bin):
            if(x > self._edges[0] and x <= self._edges[1]):
                success = True
        else:
            if(x >= self._edges[0] and x < self._edges[1]):
                success = True
        return success

    
    # def add(self, x, val):
    #     """ Try to add val into the bin if x is within the edges
    #         parameters:
    #         ----------- 
    #             x: num 
    #                 a number to be compared against the edges
    #             val: obj
    #                 an object to be added into the bin content
    #         Returns:
    #         -------
    #             result: bool 
    #                 a bool indicating whether val is added
    #     """
    #     success = False
    #     if(self.isin(x)):
    #         self._content.append(val)
    #         success = True
    #     return success
    
    def populate(self, val):
         self._content = val
    
    
    def calc(self, col, func):
        if(self.isempty):
            #print('Not called')
            return None
        else:
            #print('called')
            return func(self.content[col])    
    
         
    @property
    def isempty(self):
        return self._content is None
         
    @property
    def content(self):
        return self._content

    @property
    def bin_id(self):
        return self._id
    
    @property
    def edges(self):        
        return self._edges
    
    @property
    def center(self):
        return np.mean(self._edges)
    
        
    
    
class sbins(object):
    
    # def __init__(self, bins):
    
    #     if ((isinstance(bins,list) or isinstance(bins,tuple)) and isinstance(bins[0],sbin)):
    #         pass
    #     else:
    #         raise ValueError("The left_edge must be equal or less than the right_edge and both edges need to be numerical")
    
    #     self._bins = bins
        
    def __init__(self, df, bin_col, n):
        """ parameters:
            ----------- 
                df: DataFrame 
                    containing the data                     
                bin_col: string
                    col name used to partition the bins
                n: int 
                    number of bins     
                include_right: bool                                               
        """
        
        self.create_equal_width_bins(df, bin_col, n)

    

    def create_equal_width_bins(self, df, bin_col, n):
        self._data = df
        self._min_bin_size = 0
        self._bin_col = bin_col

              
        bin_col_data = df[bin_col]
          
        max = bin_col_data.values.max()
        min = bin_col_data.values.min()
        width = (max - min)/n #width of bin is purely based on the value range of bin_col. it doesnt have to be integer
        
        # create the equal sized bins based on the levels
        edges = [ min + width * (i) for i in np.arange(n)] + [max]   
        l_edges = [ min + width * (i)  for i in np.arange(n)]
        r_edges = [ min + width * (i + 1)  for i in np.arange(n)]
        
        #next loop through the l, right edge pair to create the bins
        self._bin_ids = list(np.arange(len(r_edges))+1)
        self._bins = [sbin(i,l,r) for (l, r, i) in zip(l_edges, r_edges, self._bin_ids)]
        
        #        
        for b in self._bins:    
            (l, r) =(b.edges)            
            if b.bin_id != np.max(self._bin_ids):
                d_bin = df[(bin_col_data >=l) & (bin_col_data < r)] 
                b.populate(d_bin)
                self._min_bin_size = self._min_bin_size if self._min_bin_size  < len(d_bin) else len(d_bin)           
            else: #last bin needs to close right edge
                d_bin = df[(bin_col_data >=l) & (bin_col_data <= r)]
                b.populate(d_bin)
                self._min_bin_size = self._min_bin_size if self._min_bin_size  < len(d_bin) else len(d_bin)

                   
    def adjust_bins_with_high_evenness(self, min_size_limit=0.05, reduction_size = 1, min_num_of_bins = 2):
        """ auto reduce the number of the bin to increase the bin size evenness
        parameters
        ----------
        Parameters
        ----------

        min_size_limit: float
            the min bin size ratio can be
        reduction_size: int
            the number of bins to reduce during each reduction
        min_num_of_bins: int
            the minimal amount of bins
    
        Returns
        -------
        dataframe 
            the complete set of bins regardless whether it is empty
        """
        
        holder = [self._min_bin_size/len(self.data)]        
        
        n = self.bin_count
        
        while (holder[-1] < min_size_limit and self.bin_count > min_num_of_bins):
            n = n - reduction_size
            self.create_equal_width_bins(self.data, self._bin_col, n)
            holder.append(self._min_bin_size/len(self.data))     

    
    def isin(self, x):
        """ check if x is within the edges
            parameters:
            ----------- 
                x: num 
                    a number to be compared against the edges
            Returns:
            -------
                result:  
                    bin_id
        """
        _bin_id = None
        
        for _bin in self._bins:
            _success = _bin.isin(x)
            if(_success):
                _bin_id = _bin.id 
                break
       
        return _bin_id
    
    def calc(self, operations): 
        """ parameters:
            -----------                         
            operations: a dict of {name:(col, func)} 
                col - nominated col to be calced on
                func - to calc stats on the nominated col
                name - will be used in return df like avg, stdev
            name: string
            name of the result like avg or stdev
            
            Returns:
            -------
            a datarfame
                containing bin_center and calc result. each row is a bin 
            a dictionary of time series
                each representing the input calced by func               
        """
        result = {} 
        underline_content = {}
        for b in self._bins:
            r_result = {}
            r_result['bin_center'] = b.center
            r_result['bin_l'] = b.edges[0]
            r_result['bin_r'] = b.edges[1]
            r_result['bin_count'] = len(b.content) if b.content is not None else 0
            
            r_result_bin_content = {}
            
            
            for fn_name, (col, fn) in operations.items():
                #the calc using fn
                r_result['{}_{}'.format(col,fn_name)] = b.calc(col, fn)
            
                r_result_bin_content['{}_{}'.format(col,fn_name)] = b.content[col]
                
            result[b.bin_id] = pd.Series(r_result)
            #the actual content going into fn
            underline_content[b.bin_id] = r_result_bin_content                                                  
                  
        result = pd.DataFrame(result).transpose()
        return result, underline_content

        
    def plot_bin_mean_variance_for_col(self, mv_col, xlim = None, ylim = None, 
                                       bin_mid_col = 'bin_center', bin_l_edge = 'bin_l', 
                                       bin_r_edge = 'bin_r', bin_size='bin_count'):
        
        
        _f = plt.figure(figsize = (10, 6))
        _gs = gridspec.GridSpec(2, 1, height_ratios=[2,1], width_ratios=[1])
        _ax0 = plt.subplot(_gs[0])
        _ax1 = plt.subplot(_gs[1])
        
        # the underline data which is grouped and 
        mv_data = self.data[mv_col].values
        
        _mu_hat, _sigma_hat, _theta_hat = estimate_unconditional_moments_vasicek(mv_data, dt = 1)
        
        _fitted_unconditional_m_v_func = lambda s: (_theta_hat * (_mu_hat - s), _sigma_hat)
        

        #bin_content_inputs is the data going into the cal using specified col
        mv, bin_content_inputs = self.calc({'avg':(mv_col, np.mean), 'std':(mv_col, lambda x: np.std(x, ddof=1))})
        
        #first reorg the dict of dict. The bin content outter keys are bin ids
        #the inner dict's key is the calc method for example a_avg. avg of col a
        # the content of the inner dict is a df of segmented data which will go 
        #into the calc specified by the inner dict key
        # reorg the dict of dict means the inner keys will be the outer keys and  
        
        # from [1]['a_avg'] -> df to ['a_avg'][1] -> df
        
        reorg_bin_content_inputs = _reorg_double_dcit(bin_content_inputs)['{}_avg'.format(mv_col)] 
        
        plot_bin_mean_variance(mv, underline_bin_content=reorg_bin_content_inputs, ax=_ax0, 
                               xlim = None, ylim = None, dot_size = 0.1, fit_1std = True, fit_2ndd = True, 
                               ignore_bin = [], bin_mid_col = 'bin_center', bin_l_edge = 'bin_l', bin_r_edge = 'bin_r', 
                               bin_size='bin_count', y_label = '{}'.format(mv_col), bin_avg = '{}_avg'.format(mv_col), 
                               bin_std = '{}_std'.format(mv_col),
                               fitted_unconditional_m_v_func = None)
             
        plot_bin_bin_size(mv, ax=_ax1, xlim = None, ylim = None, bin_mid_col = 'bin_center', 
                          bin_l_edge = 'bin_l', bin_r_edge = 'bin_r', 
                          bin_size='bin_count', y_label=self._bin_col)
        
        
        
    
    @property
    def bins(self):
        return self._bins
    
    @property
    def bin_count(self):
        return len(self._bins)
        
    @property
    def data(self):
        return self._data




def estimate_unconditional_moments_vasicek(data, dt = 1):
    '''
    Estimate unconditional mean and variance using vasicek
    
    Parameters
    ----------
    data : list of num
        data being modelled.
    dt : float, optional
        fraction of time. The default is 1 meaning 1 day for daily data freq.

    Returns
    -------
    _mu_hat : float
        unconditional mean.
    _sigma_hat : float
        unconditional variance.
    _theta_hat : float
        speed of reversion.
    '''
    n=1
    ys = data[n:]    
    xs = data[:-n]
    (b, a) =  np.polyfit(xs, ys, 1) 
    
    print(b)
    print(a)
    
    f = lambda x: a + b * x
    y_hats = [f(x) for x in xs]
    errs = [ y - y_hat for y, y_hat in zip(ys, y_hats)]        
    
    _theta_hat = np.log(b)/(-1 * dt)
    _mu_hat = a /(1 - b)
    _sigma_hat = np.std(errs, ddof=1) * np.sqrt((2 * _theta_hat)/(1 - np.exp(-2 * _theta_hat * dt)))         

    return _mu_hat, _sigma_hat, _theta_hat





def plot_bin_bin_size(data, ax=None, xlim = None, ylim = None, 
                      bin_mid_col = 'bin_center', bin_l_edge = 'bin_l', 
                      bin_r_edge = 'bin_r', bin_size='bin_count', y_label=''):
       
    _ax = None 
    _f = None
    _gs = None
    
    if ax is not None:
        _ax = ax
    else:
        _f = plt.figure(figsize = (10, 3))
        _gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
        _ax = plt.subplot(_gs[0])
        
    
    if xlim is not None: ax.set_xlim(xlim)
    if ylim is not None: ax.set_ylim(ylim)
    
    _ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
    _ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
    _ax.xaxis.set_ticks_position('bottom')
    _ax.yaxis.set_ticks_position('left')

    #major_ticks = list(self.bin_diagnosis.bin_mid.values)
    major_ticks = list(data[bin_mid_col].values)


    _ax.set_xticks(major_ticks) 
    _ax.xaxis.set_major_locator(ticker.FixedLocator(major_ticks))
    
    #ax.set_xlim(self.bin_diagnosis.iloc[0,:].bin_l_edge, self.bin_diagnosis.iloc[-1,:].bin_r_edge)
    _ax.set_xlim(data[bin_l_edge].iloc[0], data[bin_r_edge].iloc[-1])

    
    #ax.xaxis.set_visible(False)
    
    #_ax.axis('off')
    _ax.spines['right'].set_visible(False)
    _ax.spines['top'].set_visible(False)
    _ax.spines['left'].set_visible(False)
    _ax.spines['bottom'].set_visible(True)
    #plt.box(False)
    
    #plot the sizes of the bins
    bar_width = 0.3 * (data[bin_r_edge].iloc[-1] - data[bin_l_edge].iloc[0]) / len(data)

    
    bars = _ax.bar(major_ticks, list(data[bin_size].values), width=bar_width, color='lightgrey')
    
    
    for bar in bars:
        height = bar.get_height()
        _ax.text(bar.get_x() + bar.get_width()/2., 1.01*height, '%d' % int(height), ha='center', va='bottom', color='dimgrey',fontsize=6)
    
    bin_width = abs(round(data[bin_l_edge].iloc[0] - data[bin_r_edge].iloc[0], 2))
    _ax.set_ylabel('bin({}, w: {}) size'.format(y_label, bin_width), fontsize=8)

    plt.setp(_ax.yaxis.get_majorticklabels(), fontsize = 6)
    #plt.setp(ax.xaxis.get_majorticklabels(), rotation=40, fontsize = 6)
    plt.setp(_ax.xaxis.get_majorticklabels(), rotation=40, fontsize = 6)
    
    return _ax
    


def plot_bin_mean_variance(data, underline_bin_content=None, ax=None, xlim = None, ylim = None, dot_size = 0.1, 
                           fit_1std = True, fit_2ndd = True, ignore_bin = [],
                           bin_mid_col = 'bin_center', bin_l_edge = 'bin_l', bin_r_edge = 'bin_r', 
                           bin_size='bin_count', y_label = '??',
                           bin_avg = '_avg', bin_std = '_std',
                           fitted_unconditional_m_v_func = None):
      
    
    _ax = None 
    _f = None
    _gs = None
    
    if ax is not None:
        _ax = ax
    else:
        _f = plt.figure(figsize = (10, 5))
        _gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
        _ax = plt.subplot(_gs[0])
    
    
    
    if xlim is not None: _ax.set_xlim(xlim)
    if ylim is not None: _ax.set_ylim(ylim)
    _ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
    _ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
    
    #flip up 
    _ax.xaxis.set_ticks_position('top')
    _ax.xaxis.set_label_position('top')
    
    #ax.yaxis.set_ticks_position('left')

    major_ticks = list(data[bin_mid_col].values)

    _ax.set_xticks(major_ticks) 
    _ax.xaxis.set_major_locator(ticker.FixedLocator(major_ticks))
    #_ax.set_xlim(self.bin_diagnosis.iloc[0,:].bin_l_edge * 0.99, self.bin_diagnosis.iloc[-1,:].bin_r_edge * 1.01)
    _ax.set_xlim(data[bin_l_edge].iloc[0] * 0.99, data[bin_r_edge].iloc[-1] * 1.01)


    #_ax.axhline(y=0, color="black", lw=0.5)
    plt.setp(_ax.xaxis.get_majorticklabels(), rotation=40, fontsize = 6)
    plt.setp(_ax.xaxis.get_majorticklabels(), fontsize = 6)
    plt.setp(_ax.yaxis.get_majorticklabels(), fontsize = 6)
    _ax.set_ylabel('Average {}'.format(y_label), fontsize=8)



    #plot the drift coefficients
    #for bin_id, row in self.bin_diagnosis.iterrows():
    for bin_id, row in data.iterrows():    
        
        #coefficient = np.nan if np.isnan(row.bin_avg) else row.bin_avg
        coefficient = np.nan if np.isnan(row[bin_avg]) else row[bin_avg]
        
        coefficient_u_1std = np.nan if np.isnan(coefficient) or np.isnan(row[bin_std]) else coefficient + row[bin_std]
        coefficient_l_1std = np.nan if np.isnan(coefficient) or np.isnan(row[bin_std]) else coefficient - row[bin_std]

        if(bin_id not in ignore_bin):
            point_alpha = 0 if np.isnan(row[bin_avg]) else 0.4
        else: 
            point_alpha = 0 if np.isnan(row[bin_avg]) else 0.1
        
        #coefficients
        _ax.scatter(row[bin_mid_col], coefficient, c='blue', alpha=point_alpha)
        
        #underline bin content if it is provided        
        if(underline_bin_content is not None and bin_id in underline_bin_content.keys()):
            for v in underline_bin_content[bin_id].values:
                _v = v if np.abs(v) < 2 * row[bin_std] else np.sign(v) * 2 * row[bin_std]
                _ax.scatter(row[bin_mid_col], _v, c='black', s=6, alpha=point_alpha * 0.4)
                    
        
        #coefficients +- 1std boundary
        _ax.plot([row[bin_mid_col], row[bin_mid_col]], [coefficient_l_1std, coefficient_u_1std], lw=8, c='blue', ls = '-', alpha=point_alpha * 0.2)
        
        _ax.annotate('%.2f' % (row[bin_avg]),
                    (row[bin_mid_col], row[bin_avg]), 
                    xytext=(-2, 5), xycoords='data', textcoords='offset points', fontsize=6)
    
    ########################################
    #current details are not available yet
    
    #plot the current data
    #(current_level, current_bin) = self.current_details
    #ax.scatter(current_bin.bin_mid, current_bin.bin_avg, c='red', alpha=0.2, s = 120)
    
      
    # debug =>
    #print (current_level)
    #print (current_bin.bin_mid, current_bin.bin_avg)
    #print(self.bin_diagnosis)
    # debug =<

    #ax.annotate('@%.4f' % (current_level), (current_bin.bin_mid, current_bin.bin_avg), 
    #            xytext=(-12, -12), xycoords='data', textcoords='offset points', fontsize=6)
    
    ########################################
    
    ds = data[[bin_mid_col, bin_avg]].dropna()
        
   
    #plot the linear fit of the bin's coefficients
    if fit_1std:
        (b, a) = np.polyfit(list(ds[bin_mid_col].values), list(ds[bin_avg].values), deg = 1)
        fitted_ys = [round(a + b * x, 4) for x in list(ds[bin_mid_col].values)]
        _ax.plot(list(ds[bin_mid_col].values), fitted_ys, lw=0.7, c='g', ls = ':')
    
    #plot the 2nd degree ploy fit of the bin's coefficients

    
    if fit_2ndd:
        (c, b, a) = np.polyfit(list(ds[bin_mid_col].values), list(ds[bin_avg].values), deg = 2)
        fitted_ys = [a + b * x + c * x * x  for x in list(ds[bin_mid_col].values)]
        _ax.plot(list(ds[bin_mid_col].values), fitted_ys, lw=0.5, c='darkgreen', ls = '--')
    
    
    if fitted_unconditional_m_v_func is not None:
        fitted_ys = [fitted_unconditional_m_v_func(x)[0] for x in list(ds[bin_mid_col].values)]
        _ax.plot(list(ds[bin_mid_col].values), fitted_ys, lw=0.5, c='lightcoral', ls = '--')
    
    
    _ax.spines['right'].set_visible(False)
    _ax.spines['top'].set_visible(True)
    _ax.spines['left'].set_visible(False)
    _ax.spines['bottom'].set_visible(False)
    
    '''
    ax_twinx = ax.twinx()
    ax_twinx.set_ylim(0, self.bin_diagnosis.bin_size.dropna().sum())
    
    #plot the sizes of the bins
    bar_width = 0.01
    bars = ax_twinx.bar(list(self.bin_diagnosis.bin_mid.values), list(self.bin_diagnosis.bin_size.values), bar_width, color='lightgrey')
    for bar in bars:
        height = bar.get_height()
        ax_twinx.text(bar.get_x() + bar.get_width()/2., 1.01*height, '%d' % int(height), ha='center', va='bottom', color='dimgrey',fontsize=6)
    
    ax_twinx.set_ylabel('bin sample size', fontsize=8)
    plt.setp(ax_twinx.yaxis.get_majorticklabels(), fontsize = 6)  
    
    
    #ax.set_xticks(major_ticks) 
    #ax.xaxis.set_major_locator(ticker.FixedLocator(major_ticks))
    #ax.set_xlim(self.bin_diagnosis.iloc[0,:].bin_l_edge, self.bin_diagnosis.iloc[-1,:].bin_r_edge)
    
    plt.setp(ax.get_xticklabels(), visible=True)
    '''



def _reorg_double_dcit(dd):
    
    dd_new = {}
    #_d = {}
    # k1 1,2,3, 4
    for k1 in dd.keys(): 
        #_d = {} 
        #k2: avg, std
        for k2 in dd[k1].keys():            
            #_d[k1] = dd[k1][k2]
                        
            if(k2 not in dd_new.keys()):                     
                dd_new[k2] = {}
                dd_new[k2][k1] = dd[k1][k2]
            else:
                if (k1 in dd_new[k2].keys()):
                    continue
                else:              
                    dd_new[k2][k1] = dd[k1][k2]
           
    return dd_new



#unit test here        

if( 2 > 3):
        
    df = pd.DataFrame({
            'a': pd.Series({1:-1,2:-2,3:-3,4:-4,5:-5,6:-6, 7:-7, 8:-8, 9:-9, 10:-11}),
            'b': pd.Series({1:10,2:20,3:30,4:40,5:50,6:60, 7:70, 8:80, 9:90, 10:100}),
            'c': pd.Series({1:11,2:21,3:31,4:41,5:51,6:61, 7:71, 8:81, 9:91, 10:101}),    
        })
    
    
    
    binobj = sbins(df, 'b', 4)
    #binobj.data    
    for b in binobj.bins:
        #print(b.bin_id)
        print(b.edges)
        print(b.content)
    
    binobj.plot_bin_mean_variance_for_col('b')
    
    
    output, content  = binobj.calc({'avg':('b', np.mean), 'std':('b',lambda x: np.std(x, ddof=1))})
    output, content  = binobj.calc({'avg':('a', np.mean)})
    
    
    
    print(output)
    
    binobj.adjust_bins_with_high_evenness(min_size_limit=0.2, min_num_of_bins = 3)
    
    
    output, content = binobj.calc({'avg':('a', np.mean),
                                   'std':('a',lambda x: np.std(x, ddof=1))})
    
    
    plot_bin_bin_size(output)
    
    plot_bin_mean_variance(output, y_label='next day return',  bin_avg = 'a_avg', bin_std = 'a_std')



