from __future__ import division
import _path
import numpy as np
import pandas as pd
import pandas.testing as pd_testing
from pandas.api.types import CategoricalDtype
from core import tools 
import unittest



class Test_mr(unittest.TestCase):

    def test_create_equal_width_bins(self):
        from statistics.mr import bins
        levels = tools.ts([20, 10, 5, 7, 12, 23, 12, 2, 5, 6],
              ['2017-02-01','2017-02-02','2017-02-03','2017-02-04','2017-02-05',
               '2017-02-06','2017-02-07','2017-02-08','2017-02-09','2017-02-10'])
        changes = (levels.shift(-1) - levels).reindex(levels.index)    
        
        data = pd.DataFrame({'level':levels, 'change':changes}).dropna()
        data.name = 'test_data'
    
        bins = bins.create_equal_width_bins(data, 4)
        
        expected_data_with_bins = tools.tframe(pd.DataFrame({
                "date":['2017-02-01','2017-02-02','2017-02-03','2017-02-04','2017-02-05',
               '2017-02-06','2017-02-07','2017-02-08','2017-02-09','2017-02-10'],
                'bin_id':[3,1,0,0,1,3,1,0,0,0],
                'level':[20,10,5,7,12,23,12,2,5,6],
                'change':changes}), key='date')[['bin_id','level','change']]
        
        expected_bins = pd.DataFrame({
                'bin_l_edge':[2.0,7.25,12.5,17.75],
                'bin_mid':[4.625,9.875,15.125,20.375],
                'bin_r_edge':[7.25,12.5,17.75,23.0],
                'bin_size':[4,3,0,2],
                'bin_avg':[2.75, -1.33333333333,np.nan, -10.5],
                'bin_vol':[1.70782512766, 10.9696551146, np.nan,0.707106781187],
                'bin_size_ratio':[0.444444444444, 0.333333333333, 0.0, 0.222222222222]
                })[['bin_l_edge','bin_mid','bin_r_edge','bin_size','bin_avg', 'bin_vol','bin_size_ratio']]
        
        expected_bins.index.name = 'bin_id'
        
# =============================================================================
#         print expected_data_with_bins
#         print data_with_bins
#         
# =============================================================================
        
# =============================================================================
#         pd_testing.assert_frame_equal(expected_data_with_bins.dropna(), data_with_bins)
#         pd_testing.assert_frame_equal(expected_bins, bins)
#         
# =============================================================================

        print expected_bins
        
        print bins
        
if __name__ == '__main__':
    unittest.main()