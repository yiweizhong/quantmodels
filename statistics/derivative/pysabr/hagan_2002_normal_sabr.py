from base_sabr import BaseNormalSABR
import numpy as np
import pandas as pd
from scipy.optimize import minimize
from scipy.optimize import least_squares




class Hagan2002NormalSABR(BaseNormalSABR):

    def alpha(self):
        """Implies alpha parameter from the ATM normal volatility."""
        f, s, t, v_atm_n = self.f, self.shift, self.t, self.v_atm_n
        beta, rho, volvol = self.beta, self.rho, self.volvol
        # Convert ATM normal vol to ATM shifted lognormal
        return alpha(f+s, t, v_atm_n, beta, rho, volvol)

    def normal_vol(self, k):
        """Return normal volatility for a given strike."""
        f, s, t = self.f, self.shift, self.t
        beta, rho, volvol = self.beta, self.rho, self.volvol
        alpha = self.alpha()
        v_n = normal_vol(k+s, f+s, t, alpha, beta, rho, volvol)
        return v_n

    def fit(self, k, v_sln):
        """
        Calibrate SABR parameters alpha, rho and volvol.

        Best fit a smile of shifted lognormal volatilities passed through
        arrays k and v. Returns a tuple of SABR params (alpha, rho,
        volvol)
        """
        f, s, t, beta = self.f, self.shift, self.t, self.beta
        #print(f"beta is {beta}")
                

        x0 = np.array([0.001, 0.00, 0.10])

# =============================================================================
#         
#         #scipy minimize wont work for normal vol. needs to change the logic 
#         # to use least square with 
#         # optimisatin Method ‘trf’ (Trust Region Reflective)
#         # also the function input needs to be producing residuals rather 
#         # than SSE        
#         
#         def vol_square_error(x):
#             #vols = [normal_vol(k_, f, t, x[0], beta, x[1], x[2]) * 100 for k_ in k]
#             vols = [normal_vol(k_, f, t, x[0], beta, x[1], x[2]) for k_ in k]
#             #print("vols:")
#             #print(vols)            
#             return sum((vols - v_sln)**2)
#         
#         bounds = [(0.0001, None), (-0.9999, 0.9999), (0.0001, None)]          
#         res = minimize(vol_square_error, x0, method='L-BFGS-B', bounds=bounds)
# =============================================================================
        
        
        def vol_residuals(x):
            #vols = [normal_vol(k_, f, t, x[0], beta, x[1], x[2]) * 100 for k_ in k]
            vols = [normal_vol(k_, f, t, x[0], beta, x[1], x[2]) for k_ in k]
            #print("vols:")
            #print(vols)            
            return (vols - v_sln)

        
        bounds = ([0.0001, -0.9999, 0.0001], [float('inf'), 0.9999, float('inf')])
        res = least_squares(vol_residuals, x0, bounds=bounds)
        
        
        alpha, self.rho, self.volvol = res.x
        return [alpha, self.rho, self.volvol]
    

#def lognormal_vol(k, f, t, alpha, beta, rho, volvol):
def normal_vol(k, f, t, alpha, beta, rho, volvol):
    """Hagan's 2002 SABR normal vol expansion - formula (B.67a)."""
    # We break down the complex formula into simpler sub-components
    eps = 1e-07
    if abs(beta) <= eps:
        #special case to allow negative rate
        if abs(f-k) > eps:
            #OTM
            
            C = (2 - 3 * rho**2) * volvol**2 / 24
            #ZXZ will be a special beta = 0 case 
            ZXZ = _zeta_over_x_of_zeta(k, f, t, alpha, beta, rho, volvol)
            v_n = alpha * (1 + C * t) * ZXZ
            
            #print((1 + C * t))            
            #print(f"f:{f}, k:{k}, alpha:{alpha}, beta:{beta},rho:{rho}, C:{C}, ZXZ:{ZXZ}")
            
            return v_n
        else: 
            #ATM
            
            C = (2 - 3 * rho**2) * volvol**2 / 24
            #ZXZ will be a special beta = 0 case 
            #ZXZ = _zeta_over_x_of_zeta(k, f, t, alpha, beta, rho, volvol)
            v_n = alpha * (1 + C * t)             
            #print(f"f:{f}, k:{k}, alpha:{alpha}, beta:{beta},rho:{rho}, C:{C}")
            return v_n                    
    else:
        f_av = np.sqrt(f * k)
        A = - beta * (2 - beta) * alpha**2 / (24 * f_av**(2 - 2 * beta))
        B = rho * alpha * volvol * beta / (4 * f_av**(1 - beta))
        C = (2 - 3 * rho**2) * volvol**2 / 24
        FMKR = _f_minus_k_ratio(f, k, beta)
        ZXZ = _zeta_over_x_of_zeta(k, f, t, alpha, beta, rho, volvol)
        #print(f"f:{f}, k{k}, f_av{f_av}, A{A}, B{B}, C{C}, FMKR{FMKR}, ZXZ{ZXZ} ")
        # Aggregate all components into actual formula (B.67a)
        v_n = alpha * FMKR * ZXZ * (1 + (A + B + C) * t)
        return v_n

def _f_minus_k_ratio(f, k, beta):
    """Hagan's 2002 f minus k ratio - formula (B.67a)."""
    eps = 1e-07  # Numerical tolerance for f-k and beta
    if abs(f-k) > eps:
        if abs(1-beta) > eps:
            return (1 - beta) * (f - k) / (f**(1-beta) - k**(1-beta))
        else:
            return (f - k) / np.log(f / k)
    else:
        return k**beta


def _zeta_over_x_of_zeta(k, f, t, alpha, beta, rho, volvol):
    """Hagan's 2002 zeta / x(zeta) function - formulas (B.67a)-(B.67b)."""
    eps = 1e-07  # Numerical tolerance for zeta and beta
    
    if abs(beta) <= eps:
        #special case beta is zero
        zeta = (volvol * (f - k)) / alpha 
        #print(zeta)
        #print(_x(rho, zeta))
        return zeta / _x(rho, zeta)        
    else:                
        f_av = np.sqrt(f * k)
        zeta = volvol * (f - k) / (alpha * f_av**beta)
        if abs(zeta) > eps:
            return zeta / _x(rho, zeta)
        else:
            # The ratio converges to 1 when zeta approaches 0
            return 1.


def _x(rho, z):
    """Hagan's 2002 x function - formula (B.67b)."""
    a = (1 - 2*rho*z + z**2)**.5 + z - rho
    b = 1 - rho
    return np.log(a / b)


def alpha(f, t, v_atm_n, beta, rho, volvol):
    """
    Compute SABR parameter alpha from an ATM normal volatility.

    Alpha is determined as the root of a 3rd degree polynomial. Return a single
    scalar alpha.
    """
    f_ = f ** (1 - beta)
    p = [
        - beta * (2 - beta) / (24 * f_**2) * t * f**beta,
        t * f**beta * rho * beta * volvol / (4 * f_),
        (1 + t * volvol**2 * (2 - 3*rho**2) / 24) * f**beta,
        -v_atm_n
    ]
    roots = np.roots(p)
    roots_real = np.extract(np.isreal(roots), np.real(roots))
    # Note: the double real roots case is not tested
    alpha_first_guess = v_atm_n * f**(-beta)
    i_min = np.argmin(np.abs(roots_real - alpha_first_guess))
    return roots_real[i_min]


def polynom(v_atm_n, f, t, alpha, beta, rho, volvol):
    """Debug function - to remove"""
    f_ = f ** (1 - beta)
    p = [
        - beta * (2 - beta) / (24 * f_**2) * t * f**beta,
        t * f**beta * rho * beta * volvol / (4 * f_),
        (1 + t * volvol**2 * (2 - 3*rho**2) / 24) * f**beta,
        -v_atm_n
    ]
    return p[0] * alpha**3 + p[1] * alpha**2 + p[2] * alpha + p[3]


def v_atm_n(f, t, alpha, beta, rho, volvol):
    """Debug function - to remove"""
    f_av = f
    A = - beta * (2 - beta) * alpha**2 / (24 * f_av**(2 - 2 * beta))
    B = rho * alpha * volvol * beta / (4 * f_av**(1 - beta))
    C = (2 - 3 * rho**2) * volvol**2 / 24
    v_atm_n = alpha * f**beta * (1 + (A + B + C) * t)
    return v_atm_n



############# Test

if 2 > 3: 
         
    v = np.array([20.58, 17.64, 16.93, 18.01, 20.46, 22.90, 26.11, 28.89, 31.91])/ 10000
    k = np.array([-0.0067, -0.0042, -0.0017, 0.0008, 0.0033, 0.0058, 0.0083, 0.0108, 0.0133])
    f = -0.0017
    t = 1
    s = 0
    beta = 0
    
    sabr_model = Hagan2002NormalSABR(f, s, t, beta=beta)
    sabr_model_params = sabr_model.fit(k, v)
    
    (alpha, rho, volvol) = sabr_model_params
    
    v_out = []    
    for i in np.arange(len(k)):
        v_out.append(normal_vol(k[i], f, t, alpha, beta, rho, volvol))    
    
    pd.DataFrame({'v':v, 'v_out':v_out}).plot()


