from matplotlib.dates import MonthLocator, YearLocator, DateFormatter
from matplotlib.ticker import FuncFormatter
import collections
from datetime import date
import pylab
import QuantLib as ql
import pandas as pd
import numpy as np

default_plot_size=(12,8)

Par_rate = collections.namedtuple('Par_rate', 'par risk carry roll')


def plot(figsize=None):
    f = pylab.figure(figsize=figsize or default_plot_size)
    ax = f.add_subplot(1,1,1)

    for side in ['top', 'right']:
        ax.spines[side].set_visible(False)
    ax.xaxis.grid(True, 'major', color=(0.9, 0.9, 0.9))
    ax.yaxis.grid(True, 'major', color=(0.9, 0.9, 0.9))

    return f, ax

def highlight_x_axis(ax):
    ax.axhline(0.0, linewidth=1, color=(0.5,0.5,0.5))

def to_datetime(d):
    return date(d.year(), d.month(), d.dayOfMonth())

def format_rate(r, digits=2):
    format = '%.' + str(digits) + 'f %%'
    return format % (r*100.0)

def rate_formatter(digits=2):
    return FuncFormatter(lambda r,pos: format_rate(r,digits))

def date_formatter():
    return DateFormatter("%b '%y")

def locator(span):
    if span < 400:
        return MonthLocator()
    elif 400 <= span < 800:
        return MonthLocator(bymonth=[1,4,7,10])
    elif 800 <= span < 3700:
        return YearLocator()
    else:
        return YearLocator(5)
    
def plot_curve(ax,dates,rates,ymin=None,ymax=None,digits=2,
               format_rates=False):
    span = dates[-1] - dates[0]
    dates = [ to_datetime(d) for d in dates ]
    for (rs, style, label) in rates:
        ax.plot_date(dates, rs, style, label=label)
    ax.set_xlim(min(dates),max(dates))
    ax.xaxis.set_major_locator(locator(span))
    ax.xaxis.set_major_formatter(date_formatter())
    ax.autoscale_view()
    ax.set_ylim(ymin,ymax)
    if format_rates:
        ax.yaxis.set_major_formatter(rate_formatter(digits))



def plot_1d1d_fwd(curve, length_in_yrs, calendar, day_count, title, ax=None, label = None):
    
    #from statistics.derivative import quantlibutils as utils    
    today = curve.referenceDate()
    end = today + ql.Period(length_in_yrs, ql.Years)
    dates = [ql.Date(serial) for serial in range(today.serialNumber(),end.serialNumber()+1)]     
    rates_c = [curve.forwardRate(d, calendar.advance(d,1,ql.Days), day_count, ql.Simple).rate() for d in dates]
    
    _ax = None
    if (ax is None):
        _, _ax = plot()
    else:
        _ax = ax
    highlight_x_axis(_ax)
    plot_curve(_ax, dates, [(rates_c,'-', label)], format_rates=True)
    _ax.set_title(title, fontsize=12)
    _ax.legend(frameon=False)

    return _ax

#aud_ois_curve = create_aud_ois_curve(bbg_ois_curve_data['aud'].iloc[-1,:], ql_cfg.aud_OIS_index)
#aud_ois_curve_20210201 = create_aud_ois_curve(bbg_ois_curve_data['aud'].loc['2021-02-01',:], ql_cfg.aud_OIS_index)
#_ax = plot_1d_fwd(aud_ois_curve, 10, ql.Australia(), ql.Actual365Fixed(), 'AUD OIS implied policy rate', label='Today')
#plot_1d_fwd(aud_ois_curve_20210201, 10, ql.Australia(), ql.Actual365Fixed(), 'AUD OIS implied policy rate', _ax, label='2021-02-01')



def derive_floating_rate_index(relinkable_floating_rate_curve_handle, tenor):
        """        

        Parameters
        ----------
        relinkable_floating_rate_curve_handle : RelinkableHandle< YieldTermStructure > *
            a RelinkableYieldTermStructureHandle to a curve object
        tenor : string
            for example '10Y'.

        Returns
        -------
        ql floating rate index like ql.Bbsw6M
            usdlibor3m_index = ql.USDLibor(ql.Period('3M'),  curve_handle)

        """
        if relinkable_floating_rate_curve_handle.calendar() == ql.Australia():
            if ql.Period(tenor) <= ql.Period('3Y'): 
                return  ql.Bbsw3M(relinkable_floating_rate_curve_handle)
            else:
                return  ql.Bbsw6M(relinkable_floating_rate_curve_handle)
        elif relinkable_floating_rate_curve_handle.calendar() == ql.UnitedStates():
            return ql.USDLibor(ql.Period('3M'),  relinkable_floating_rate_curve_handle)

# =============================================================================
# curve_handle = ql.RelinkableYieldTermStructureHandle(forward_curve)    
# curve_handle_base = ql.YieldTermStructureHandle(forward_curve)  
# derive_floating_rate_index(curve_handle, '10Y')
# =============================================================================




def calc_par_rate(forward_curve, discount_curve, forward, tenor, 
                              fixed_leg_day_count, 
                              floating_leg_day_count, carry_roll_period = '3M', 
                              bump_discount_curve=False):
    """    
    Parameters
    ----------
    forward_curve : boostrapped curve object 
        used to project the forward rate for the floating leg   
    discount_curve : bootstrapped curve object 
        used to discount the cashflow on both floating and fixed leg    
    forward : string 
        number and unit representing forward term. for example '10Y'
    tenor : string 
        number and unit representing forward term. for example '10Y'
    fixed_leg_day_count : ql day count object
        for example ql.Thirty360()
    floating_leg_day_count ql day count object
        for example ql.Thirty360()
    bump_discount_curve : bool, optional
        flag on whether bump discount curve too. The default is False.

    Returns
    -------
    float, float, float, float
        the par rate, analytic risk, carry and roll 

    """
    
    def bp(n):
        return ql.QuoteHandle(ql.SimpleQuote(n*0.0001))
    
            
    
    #relinkable handle to price swap
    curve_handle = ql.RelinkableYieldTermStructureHandle(forward_curve)    
    curve_handle_base = ql.YieldTermStructureHandle(forward_curve)    
    #default no curve shift
    curve_handle.linkTo(ql.ZeroSpreadedTermStructure(curve_handle_base,bp(0)))
        
    #disc pricing engine    
    discount_curve_handle = ql.RelinkableYieldTermStructureHandle(discount_curve)
    discount_curve_handle_base = ql.YieldTermStructureHandle(discount_curve)  
    #default no curve shift
    discount_curve_handle.linkTo(ql.ZeroSpreadedTermStructure(discount_curve_handle_base,bp(0)))
    
    #create the pricing engine with the discount curve
    #pricing_engine = ql.DiscountingSwapEngine(ql.YieldTermStructureHandle(discount_curve))
    pricing_engine = ql.DiscountingSwapEngine(discount_curve_handle)
 
    
    #usdlibor3m_index created with the forecast curve
    floating_rate_index = derive_floating_rate_index(curve_handle, tenor)
    
    #1st price to get the par rate
    par_swap = ql.MakeVanillaSwap(ql.Period(tenor), floating_rate_index, 
                              0.01, ql.Period(forward), 
                              pricingEngine=pricing_engine,
                              fixedLegDayCount=fixed_leg_day_count,
                              floatingLegDayCount=floating_leg_day_count)    
    
    par_rate = par_swap.fairRate()
    
    
    #going to do 3 price here
    ql_s_tenor = ql.Period('{}'.format(tenor))
    ql_f_tenor = ql.Period('{}'.format(forward))

    
    #rolled_swap
    ql_f_tenor_rolled = ql_f_tenor if forward.startswith('0') else ql_f_tenor - ql.Period(carry_roll_period)
    ql_s_tenor_rolled = ql_s_tenor- ql.Period(carry_roll_period) if forward.startswith('0') else ql_s_tenor
    
    par_swap_rolled = ql.MakeVanillaSwap(ql_s_tenor_rolled, floating_rate_index, 
                                         0.01, ql_f_tenor_rolled, 
                                         pricingEngine=pricing_engine, 
                                         fixedLegDayCount=fixed_leg_day_count,
                                         floatingLegDayCount=floating_leg_day_count)
    
    par_swap_rolled_rate = par_swap_rolled.fairRate()
            
    #aged_swap
    ql_f_tenor_aged = ql_f_tenor + ql.Period(carry_roll_period) if forward.startswith('0') else ql_f_tenor 
    ql_s_tenor_aged = ql_s_tenor - ql.Period(carry_roll_period) if forward.startswith('0') else ql_s_tenor
    
    par_swap_aged = ql.MakeVanillaSwap(ql_s_tenor_aged, floating_rate_index, 
                                         0.01, ql_f_tenor_aged, 
                                         pricingEngine=pricing_engine, 
                                         fixedLegDayCount=fixed_leg_day_count,
                                         floatingLegDayCount=floating_leg_day_count)
    
    par_swap_aged_rate = par_swap_aged.fairRate()
    
    # carry and roll running    
    carry = (par_swap_aged_rate - par_rate) 
    roll = (par_rate - par_swap_rolled_rate) 
    
    

    #create the par swap now
    par_swap = ql.MakeVanillaSwap(ql.Period(tenor), floating_rate_index, 
                              par_rate, ql.Period(forward), 
                              pricingEngine=pricing_engine,
                              fixedLegDayCount=fixed_leg_day_count,
                              floatingLegDayCount=floating_leg_day_count)
    
    #bump libor curve up by 1bp
    curve_handle.linkTo(ql.ZeroSpreadedTermStructure(curve_handle_base, bp(1)))
    if (bump_discount_curve):
        discount_curve_handle.linkTo(ql.ZeroSpreadedTermStructure(discount_curve_handle_base, bp(1)))
     
    a = par_swap.NPV()
    
    #bump libor curve down by 1bp
    curve_handle.linkTo(ql.ZeroSpreadedTermStructure(curve_handle_base, bp(-1)))
    if (bump_discount_curve):
        discount_curve_handle.linkTo(ql.ZeroSpreadedTermStructure(discount_curve_handle_base, bp(-1)))
      
    b = par_swap.NPV()
    
    analystical_risk = abs(a - b)/2 * 10000    
    
    return par_rate * 100, analystical_risk, carry * 100, roll * 100


# =============================================================================
# audswap5_risk = qlutils.calc_swap_analytical_risk(aud_bbsw_swap_curve, aud_ois_curve,
#                                                       '0D', '10Y', ql.Actual365Fixed(), 
#                                                       ql.Actual365Fixed(),
#                                                       bump_discount_curve = True)
#         
# usdswap5_risk = qlutils.calc_swap_analytical_risk(usd_3m_libor_swap_curve, usd_3m_libor_swap_curve,
#                                                   '0D', '5Y', ql.Thirty360(), ql.Actual360(),
#                                                   bump_discount_curve = False)
#     
# =============================================================================


def create_aud_ois_curve(data, bootstrap_f = ql.PiecewiseLogCubicDiscount):
    '''    

    Parameters
    ----------
    data : series
        a pd series of tenor and rate.
    bootstrap_f : TYPE, optional
        DESCRIPTION. The default is ql.PiecewiseLogCubicDiscount.
    dc : optional bootstrapped curve object 
        used to discount the cashflow on both floating and fixed leg. If none
        provided the fwd will be discounted by iteself
        
    Returns
    -------
    bootstrapped curve object for USD 3m libor

    '''
    aud_ois_index = ql.OvernightIndex('AUDOIS', 0, ql.AUDCurrency(), ql.Australia(),ql.Actual365Fixed())
    aud_ois_inputs = ql.RateHelperVector() 
    #adding the market quotes to the helper collection
    for t, r in data.items():
        if t == '1D':
            _d = ql.DepositRateHelper(ql.QuoteHandle(ql.SimpleQuote(r/100)), ql.Period(1,ql.Days), 0, 
                                      ql.Australia(), ql.ModifiedFollowing, False, ql.Actual365Fixed())
            aud_ois_inputs.append(_d)
            
        else:
            _s = ql.OISRateHelper(0, ql.Period(t), ql.QuoteHandle(ql.SimpleQuote(r/100)), aud_ois_index)
            aud_ois_inputs.append(_s)
    
    aud_ois_curve = bootstrap_f(0, ql.Australia(), aud_ois_inputs, ql.Actual365Fixed())
    aud_ois_curve.enableExtrapolation()        
    return aud_ois_curve 

def create_sofr_ois_curve(data, bootstrap_f = ql.PiecewiseLogCubicDiscount):
    '''    

    Parameters
    ----------
    data : series
        a pd series of tenor and rate.
    bootstrap_f : TYPE, optional
        DESCRIPTION. The default is ql.PiecewiseLogCubicDiscount.
    dc : optional bootstrapped curve object 
        used to discount the cashflow on both floating and fixed leg. If none
        provided the fwd will be discounted by iteself
        
    Returns
    -------
    bootstrapped curve object for USD 3m libor

    '''
    sofr_ois_index = ql.OvernightIndex('SOFROIS', 2, ql.USDCurrency(), ql.UnitedStates(), ql.Actual360())
    sofr_ois_inputs = ql.RateHelperVector()
    #adding the market quotes to the helper collection
    for t, r in data.items():
        if t == '1D':
            _d = ql.DepositRateHelper(ql.QuoteHandle(ql.SimpleQuote(r/100)), ql.Period(1,ql.Days), 0, 
                                      ql.UnitedStates(), ql.ModifiedFollowing, False, ql.Actual360())
            sofr_ois_inputs.append(_d)
            
        else:
            _s = ql.OISRateHelper(2, ql.Period(t), ql.QuoteHandle(ql.SimpleQuote(r/100)), sofr_ois_index)
            sofr_ois_inputs.append(_s)
    
    sofr_ois_curve = bootstrap_f(0, ql.UnitedStates(), sofr_ois_inputs, ql.Actual360())
    sofr_ois_curve.enableExtrapolation()        
    
    return sofr_ois_curve 
        


def create_aud_bbsw_swap_curve(data, bootstrap_f = ql.PiecewiseLogCubicDiscount, dc = None):
    '''    

    Parameters
    ----------
    data : series
        a pd series of tenor and rate.
    bootstrap_f : TYPE, optional
        DESCRIPTION. The default is ql.PiecewiseLogCubicDiscount.
    dc : optional bootstrapped curve object 
        used to discount the cashflow on both floating and fixed leg. If none
        provided the fwd will be discounted by iteself
        
    Returns
    -------
    bootstrapped curve object for USD 3m libor

    '''
    
    
    aud_swap_inputs = ql.RateHelperVector()
    discount_curve = ql.RelinkableYieldTermStructureHandle()
    if dc is not None:
        discount_curve.linkTo(dc)
    #adding the market quotes to the helper collection
    for t, r in data.items():
        if t == '1D':
            _d = ql.DepositRateHelper(ql.QuoteHandle(ql.SimpleQuote(r/100)), ql.Period(1,ql.Days), 0, ql.Australia(), ql.ModifiedFollowing, False, ql.Actual365Fixed())
            aud_swap_inputs.append(_d)
            
        elif ql.Period(t) <= ql.Period('3Y'):
            _s = ql.SwapRateHelper(ql.QuoteHandle(ql.SimpleQuote(r/100)), ql.Period(t), ql.Australia(), 
                                   ql.Quarterly,ql.ModifiedFollowing, ql.Actual365Fixed(), ql.Bbsw3M(),
                                   ql.QuoteHandle(), ql.Period(0,ql.Days), # these 2 positional parameters are required 
                                   discount_curve)
            aud_swap_inputs.append(_s)
        else:
            _s = ql.SwapRateHelper(ql.QuoteHandle(ql.SimpleQuote(r/100)), ql.Period(t), ql.Australia(), 
                                   ql.Semiannual, ql.ModifiedFollowing, ql.Actual365Fixed(), ql.Bbsw6M(),
                                   ql.QuoteHandle(), ql.Period(0,ql.Days),  # these 2 positional parameters are required 
                                   discount_curve)
            aud_swap_inputs.append(_s)
    
    aud_bbsw_swap_curve = bootstrap_f(0, ql.Australia(), aud_swap_inputs, ql.Actual365Fixed())
    aud_bbsw_swap_curve.enableExtrapolation()
    return aud_bbsw_swap_curve







def create_usd_3m_libor_swap_curve(data, bootstrap_f = ql.PiecewiseLogCubicDiscount, dc = None):
    '''    

    Parameters
    ----------
    data : series
        a pd series of tenor and rate.
    bootstrap_f : TYPE, optional
        DESCRIPTION. The default is ql.PiecewiseLogCubicDiscount.
    dc : optional bootstrapped curve object 
        used to discount the cashflow on both floating and fixed leg. If none
        provided the fwd will be discounted by iteself
        
    Returns
    -------
    bootstrapped curve object for USD 3m libor

    '''
    usd_swap_inputs = ql.RateHelperVector()
    discount_curve = ql.RelinkableYieldTermStructureHandle()
    if dc is not None:
        discount_curve.linkTo(dc)
    #adding the market quotes to the helper collection
    for t, r in data.items():
        if t == '3M' :
            _d = ql.DepositRateHelper(ql.QuoteHandle(ql.SimpleQuote(r/100)), ql.USDLibor(ql.Period('3M')))
            usd_swap_inputs.append(_d)
        else:
# =============================================================================
#             _s = ql.SwapRateHelper(ql.QuoteHandle(ql.SimpleQuote(r/100)), ql.UsdLiborSwapIsdaFixAm(ql.Period(t)),
#                                    ql.QuoteHandle(), ql.Period(0,ql.Days),  # these 2 positional parameters are required 
#                                    discount_curve)
#             
# =============================================================================
            _s = ql.SwapRateHelper(ql.QuoteHandle(ql.SimpleQuote(r/100)), ql.Period(t), ql.UnitedStates(),
                                   ql.Semiannual, ql.ModifiedFollowing, ql.Thirty360(),ql.USDLibor(ql.Period('3M')),
                                   ql.QuoteHandle(), ql.Period(0,ql.Days),  # these 2 positional parameters are required 
                                   discount_curve)
            
            
            usd_swap_inputs.append(_s)
    
    usd_3m_libor_swap_curve = bootstrap_f(0, ql.UnitedStates(), usd_swap_inputs, ql.Thirty360())
    usd_3m_libor_swap_curve.enableExtrapolation()
    return usd_3m_libor_swap_curve
       

# =============================================================================
# create_usd_3m_libor_swap_curve(bbg_swap_curve_data['usd'].iloc[-1,:], dc=sofr_ois_curve, 
#                                bootstrap_f = ql.PiecewiseFlatForward) 
# bbg_swap_curve_data['aud'].iloc[-1,:]
# Out[174]: 
# 1D     0.03000
# 1M     0.01910
# 2M     0.02000
# 3M     0.04030
# 4M     0.02000
# 6M     0.05500
# 9M     0.06500
# 1Y     0.07425
# 2Y     0.16075
# 3Y     0.32000
# 4Y     0.61375
# 5Y     0.85750
# 6Y     1.10625
# 7Y     1.32500
# 8Y     1.51375
# 9Y     1.66500
# 10Y    1.80650
# 20Y    2.31500
# 30Y    2.30125
# Name: 2021-04-13 00:00:00, dtype: float64
# =============================================================================

class CurveRiskEngine(object):
    
    def __init__(self, forecast_curve_input, discount_curve_input, country):
        '''    
        Parameters
        ----------
        forecast_curve_input : dataframe
            A dataframe of rate history to bootsrap the forecast curve object.
        discount_curve_input : dataframe
            A dataframe of rate history to bootstrap the discount curve object
        country : ql.country
            country flag
        Returns
        -------
        None.

        '''

        self._forecast_curve_input = forecast_curve_input
        self._discount_curve_input = discount_curve_input        
        self._country = country
        self._forecast_curves = {}
        self._discount_curves = {}
        self.bootsrap_curve()
        
    def day_count_convention(self):
        if self._country == ql.Australia():
            return (ql.Actual365Fixed(), ql.Actual365Fixed())
        elif self._country == ql.UnitedStates():
            return (ql.Thirty360(), ql.Actual360())
        else:
            raise Exception("Unrecognised country")

    def bootsrap_curve(self):        
        if self._country == ql.Australia():
            #print('bootstrap au curves')
            #discount curve first         
            for date in self._discount_curve_input.index:
                self._discount_curves[date] = create_aud_ois_curve(self._discount_curve_input.loc[date])
                self._forecast_curves[date] = create_aud_bbsw_swap_curve(self._forecast_curve_input.loc[date],
                                                                                  dc = self._discount_curves[date])
            
        elif self._country == ql.UnitedStates():
            #print('bootstrap us forecast curve')
            #discount curve first         
            for date in self._discount_curve_input.index:
                self._discount_curves[date] = create_sofr_ois_curve(self._discount_curve_input.loc[date])
                self._forecast_curves[date] = create_usd_3m_libor_swap_curve(self._forecast_curve_input.loc[date],
                                                                                     dc = self._discount_curves[date])
        else:
            raise Exception("Unrecognised country")
    
    
    def calc_par_rate_risk(self, date, forward, tenor):
        '''
        Parameters
        ----------
        date : string 'YYYY-MM-DD'
            a date string representing the date of the risk to be analysied
        forward : string 
            number and unit representing forward term. for example '10Y'
        tenor : string 
            number and unit representing forward term. for example '10Y'
        Returns
        -------
        named type of float, float, float, float
        the par rate, analytic risk, carry and roll 
        Par_rate = collections.namedtuple('Par_rate', 'par risk carry roll')

        '''
        # audswap5_risk = calc_swap_analytical_risk(aud_bbsw_swap_curve, aud_ois_curve,
#                                                       '0D', '10Y', ql.Actual365Fixed(), 
#                                                       ql.Actual365Fixed(),
#                                                       bump_discount_curve = True)
        
        _dt = pd.to_datetime(date)
        fixed_day_count, floating_day_count = self.day_count_convention()
        _par, _risk, _carry, _roll =  calc_par_rate(self._forecast_curves[_dt], 
                                                            self._discount_curves[_dt],
                                                            forward, tenor, fixed_day_count, floating_day_count,
                                                            bump_discount_curve = True)
        return Par_rate(_par, _risk, _carry, _roll)
                                  
    
    
    