import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations
_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)
from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper



def smooth_series(ds, lim=2, half_window=1):

    def smooth(row):

        if np.isnan(row['ds_prev']) or np.isnan(row['ds_next']):
            return row['ds']

        if abs(row['ds_prev'] - row['ds']) > lim and abs(row['ds_next'] - row['ds']) > lim and abs((row['ds_prev'] + row['ds_next'])/2 - row['ds']) > lim:
            return (row['ds_prev'] + row['ds_next'])/2
        else:
            return row['ds']

    data = pd.DataFrame({'ds':ds, 'ds_prev': ds.shift(half_window), 'ds_next': ds.shift(-1*half_window)})

    return data.apply(smooth, axis=1)
 
    

def smooth_rates(data, limit, half_window): 
    out = {}
    for col, ds in data.iteritems():
        out[col] = smooth_series(ds, lim=limit, half_window=half_window)
    
    return pd.DataFrame(out)

def smooth_fwd_rates(data, limit, half_window): 
    
    out = {}
    
    for col, ds in data.iteritems():
        
        if len(col.split('_')) > 1 and int(col.split('_')[1].replace('Y','')) != 0:
            out[col] = smooth_series(ds, lim=limit, half_window=half_window)
        else:
            out[col] = ds
    
    return pd.DataFrame(out)


def restructure(data, tenors, curve_names):
    df_data = {}
    curve_names.sort()    
    for curve in curve_names:
        curve_columns = [curve + '_' + tenor for tenor in tenors]
        curve_data = data[curve_columns].rename(lambda x: x.split('_')[2], axis='columns')
        ds = curve_data.iloc[0]
        df_data[curve] = ds 
    df = pd.DataFrame(df_data)
    df.index.name = 'Tenor'
    return df

#######################################################################
#load data
#######################################################################
filedate = '2019-05-29'
modeldate = filedate

data = pd.read_csv('C:/Temp/test/market_watch/staging/'+ filedate + '_SWAP_ALL_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()
data = smooth_fwd_rates(data, 0.2, 1)    


#take out the history for model period
data = data[data.index < modeldate]

#dates
dt_10yrs_ago = data.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_5yrs_ago = data.index[-1] - DateOffset(years = 5, months=0, days=0)
dt_3yrs_ago = data.index[-1] - DateOffset(years = 3, months=0, days=0)
dt_2yrs_ago = data.index[-1] - DateOffset(years = 2, months=0, days=0)
dt_1yrs_ago = data.index[-1] - DateOffset(years = 1, months=0, days=0)
dt_18mths_ago = data.index[-1] - DateOffset(years = 0, months=18, days=0)



#countries
countries = set(data.columns.map(lambda x: x[:3]))

#tickers

ticker_1y = [c for c in data.columns if '_0Y_1Y' in c and  'NOK' not in c]
ticker_2y = [c for c in data.columns if '_0Y_2Y' in c and  'NOK' not in c]
ticker_3y = [c for c in data.columns if '_0Y_3Y' in c and  'NOK' not in c]
ticker_4y = [c for c in data.columns if '_0Y_4Y' in c and  'NOK' not in c]
ticker_5y = [c for c in data.columns if '_0Y_5Y' in c and  'NOK' not in c]
ticker_7y = [c for c in data.columns if '_0Y_7Y' in c and  'NOK' not in c]
ticker_10y = [c for c in data.columns if '_0Y_10Y' in c and  'NOK' not in c]
ticker_1y1y = [c for c in data.columns if '_1Y_1Y' in c and  'NOK' not in c]
ticker_1y2y = [c for c in data.columns if '_1Y_2Y' in c and  'NOK' not in c]
ticker_1y5y = [c for c in data.columns if '_1Y_5Y' in c and  'NOK' not in c]
ticker_1y10y = [c for c in data.columns if '_1Y_10Y' in c and  'NOK' not in c]
ticker_2y1y = [c for c in data.columns if '_2Y_1Y' in c and  'NOK' not in c]
ticker_2y2y = [c for c in data.columns if '_2Y_2Y' in c and  'NOK' not in c]
ticker_2y5y = [c for c in data.columns if '_2Y_5Y' in c and  'NOK' not in c]
ticker_2y10y = [c for c in data.columns if '_2Y_10Y' in c and  'NOK' not in c]
ticker_3y2y = [c for c in data.columns if '_3Y_2Y' in c and  'NOK' not in c]
ticker_5y5y = [c for c in data.columns if '_5Y_5Y' in c and  'NOK' not in c]
ticker_2y2y = [c for c in data.columns if '_2Y_2Y' in c and  'NOK' not in c]
ticker_30y = [c for c in data.columns if '_0Y_30Y' in c and  'NOK' not in c and 'THB' not in c and 'HKD' not in c and 'NZD' not in c and 'SGD' not in c]


#derived data

data_2s10s = {}
if True:
    for i in np.arange(0, len(ticker_2y)):
         data_2s10s[ticker_10y[i][:3] + '2s10s'] =  data[ticker_10y[i]] - data[ticker_2y[i]]
    data_2s10s = pd.DataFrame(data_2s10s)

data_2s5s = {}
if True:
    for i in np.arange(0, len(ticker_2y)):
         data_2s5s[ticker_5y[i][:3] + '2s5s'] =  data[ticker_5y[i]] - data[ticker_2y[i]]
    data_2s5s = pd.DataFrame(data_2s5s)


data_2s7s = {}
if True:
    for i in np.arange(0, len(ticker_2y)):
         data_2s7s[ticker_2y[i][:3] + '2s7s'] =  data[ticker_7y[i]] - data[ticker_2y[i]]
    data_2s7s = pd.DataFrame(data_2s7s)


data_2s5y5s = {}
if True:
    for i in np.arange(0, len(ticker_2y)):
         data_2s5y5s[ticker_2y[i][:3] + '2s55s'] =  data[ticker_5y5y[i]] - data[ticker_2y[i]]
    data_2s5y5s = pd.DataFrame(data_2s5y5s)



data_2s2y5s = {}
if True:
    for i in np.arange(0, len(ticker_2y)):
         data_2s2y5s[ticker_2y[i][:3] + '2s25s'] =  data[ticker_2y5y[i]] - data[ticker_2y[i]]
    data_2s2y5s = pd.DataFrame(data_2s2y5s)


data_2s3y2s = {}
if True:
    for i in np.arange(0, len(ticker_2y)):
        data_2s3y2s[ticker_2y[i][:3] + '2s32s'] =  data[ticker_3y2y[i]] - data[ticker_2y[i]]
    data_2s3y2s = pd.DataFrame(data_2s3y2s)

data_5s10s = {}
if True:
    for i in np.arange(0, len(ticker_10y)):
         data_5s10s[ticker_5y[i][:3] + '5s10s'] =  data[ticker_10y[i]] - data[ticker_5y[i]]
    data_5s10s = pd.DataFrame(data_5s10s)

data_5s5y5s = {}
if True:
    for i in np.arange(0, len(ticker_5y5y)):
         data_5s5y5s[ticker_5y[i][:3] + '5s55s'] =  data[ticker_5y5y[i]] - data[ticker_5y[i]]
    data_5s5y5s = pd.DataFrame(data_5s5y5s)

data_5s30s = {}
ticker_temp = [c.replace('_0Y_30Y', '_0Y_5Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_5s30s[ticker_30y[i][:3] + '5s30s'] =  data[ticker_30y[i]] - data[ticker_temp[i]]
    data_5s30s = pd.DataFrame(data_5s30s)

data_10s30s = {}
ticker_temp = [c.replace('_0Y_30Y', '_0Y_10Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_10s30s[ticker_30y[i][:3] + '10s30s'] =  data[ticker_30y[i]] - data[ticker_temp[i]]
    data_10s30s = pd.DataFrame(data_10s30s)

data_5y5ys30s = {}
ticker_temp = [c.replace('_0Y_30Y', '_5Y_5Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_5y5ys30s[ticker_30y[i][:3] + '55s30s'] =  data[ticker_30y[i]] - data[ticker_temp[i]]
    data_5y5ys30s = pd.DataFrame(data_5y5ys30s)


data_2s30s = {}
ticker_temp = [c.replace('_0Y_30Y', '_0Y_2Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_2s30s[ticker_30y[i][:3] + '2s30s'] =  data[ticker_30y[i]] - data[ticker_temp[i]]
    data_2s30s = pd.DataFrame(data_2s30s)


data_2s5s10s = {}
if True:
    for i in np.arange(0, len(ticker_2y)):
         data_2s5s10s[ticker_2y[i][:3] + 'P2s5s10s'] =  2 * data[ticker_5y[i]] - data[ticker_2y[i]] - data[ticker_10y[i]]
    data_2s5s10s = pd.DataFrame(data_2s5s10s)


data_5s10s30s = {}
ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_5Y') for c in ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_5s10s30s[ticker_30y[i][:3] + 'P5s10s30s'] =  2 * data[ticker_temp2[i]] - data[ticker_temp1[i]] - data[ticker_30y[i]]
    data_5s10s30s = pd.DataFrame(data_5s10s30s)


data_2s10s30s = {}
ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_2Y') for c in ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_2s10s30s[ticker_30y[i][:3] + 'P2s10s30s'] =  2 * data[ticker_temp2[i]] - data[ticker_temp1[i]] - data[ticker_30y[i]]
    data_2s10s30s = pd.DataFrame(data_2s10s30s)


data_22s10s30s = {}
ticker_temp1 = [c.replace('_0Y_30Y', '_2Y_2Y') for c in ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_22s10s30s[ticker_30y[i][:3] + 'P22s10s30s'] =  2 * data[ticker_temp2[i]] - data[ticker_temp1[i]] - data[ticker_30y[i]]
    data_22s10s30s = pd.DataFrame(data_22s10s30s)





data_2s55s30s = {}
ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_2Y') for c in ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_5Y_5Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_2s55s30s[ticker_30y[i][:3] + 'P2s55s30s'] =  2 * data[ticker_temp2[i]] - data[ticker_temp1[i]] - data[ticker_30y[i]]
    data_2s55s30s = pd.DataFrame(data_2s55s30s)


data_5s55s30s = {}
ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_5Y') for c in ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_5Y_5Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_5s55s30s[ticker_30y[i][:3] + 'P5s55s30s'] =  2 * data[ticker_temp2[i]] - data[ticker_temp1[i]] - data[ticker_30y[i]]
    data_5s55s30s = pd.DataFrame(data_5s55s30s)


data_11s22s55s = {}
if True:
    for i in np.arange(0, len(ticker_1y1y)):
         data_11s22s55s[ticker_1y1y[i][:3] + 'P11s22s55s'] =  2 * data[ticker_2y2y[i]] - data[ticker_1y1y[i]] - data[ticker_5y5y[i]]
    data_11s22s55s = pd.DataFrame(data_11s22s55s)


data_22s25s = {}
if True:
    for i in np.arange(0, len(ticker_2y2y)):
         data_22s25s[ticker_2y2y[i][:3] + '22s25s'] =  data[ticker_2y5y[i]] - data[ticker_2y2y[i]]
    data_22s25s = pd.DataFrame(data_22s25s)



data_22s210s = {}
if True:
    for i in np.arange(0, len(ticker_2y2y)):
         data_22s210s[ticker_2y2y[i][:3] + '22s210s'] =  data[ticker_2y10y[i]] - data[ticker_2y2y[i]]
    data_22s210s = pd.DataFrame(data_22s210s)


data_25s210s = {}
if True:
    for i in np.arange(0, len(ticker_2y5y)):
         data_25s210s[ticker_2y5y[i][:3] + '25s210s'] =  data[ticker_2y10y[i]] - data[ticker_2y5y[i]]
    data_25s210s = pd.DataFrame(data_25s210s)



data_12s15s = {}
if True:
    for i in np.arange(0, len(ticker_1y2y)):
         data_12s15s[ticker_1y2y[i][:3] + '12s15s'] =  data[ticker_1y5y[i]] - data[ticker_1y2y[i]]
    data_12s15s = pd.DataFrame(data_12s15s)



data_12s110s = {}
if True:
    for i in np.arange(0, len(ticker_1y2y)):
         data_12s110s[ticker_1y2y[i][:3] + '12s110s'] =  data[ticker_1y10y[i]] - data[ticker_1y2y[i]]
    data_12s110s = pd.DataFrame(data_12s110s)


data_15s110s = {}
if True:
    for i in np.arange(0, len(ticker_1y5y)):
         data_15s110s[ticker_1y5y[i][:3] + '15s110s'] =  data[ticker_1y10y[i]] - data[ticker_1y5y[i]]
    data_15s110s = pd.DataFrame(data_15s110s)
    
    

data_12s15s110s = {}
if True:
    for i in np.arange(0, len(ticker_1y2y)):
         data_12s15s110s[ticker_1y2y[i][:3] + 'P12s15s110s'] =  2 * data[ticker_1y5y[i]] - data[ticker_1y2y[i]] - data[ticker_1y10y[i]]
    data_12s15s110s = pd.DataFrame(data_12s15s110s)

data_22s25s210s = {}
if True:
    for i in np.arange(0, len(ticker_2y2y)):
         data_22s25s210s[ticker_2y2y[i][:3] + 'P22s25s210s'] =  2 * data[ticker_2y5y[i]] - data[ticker_2y2y[i]] - data[ticker_2y10y[i]]
    data_22s25s210s = pd.DataFrame(data_22s25s210s)


data_22s55s = {}
if True:
    for i in np.arange(0, len(ticker_2y2y)):
         data_22s55s[ticker_2y2y[i][:3] + '22s55s'] =  data[ticker_5y5y[i]] - data[ticker_2y2y[i]]
    data_22s55s = pd.DataFrame(data_22s55s)

data_12s55s = {}
if True:
    for i in np.arange(0, len(ticker_1y2y)):
         data_12s55s[ticker_2y2y[i][:3] + '12s55s'] =  data[ticker_5y5y[i]] - data[ticker_1y2y[i]]
    data_12s55s = pd.DataFrame(data_12s55s)





###############################################################

data_per_country = {}

for i in np.arange(0, len(ticker_2y)):
        data_per_country[ticker_2y[i][:3] + 'PCW2s5s10s'] =  pd.DataFrame({ticker_2y[i]: data[ticker_2y[i]], ticker_5y[i]: data[ticker_5y[i]], ticker_10y[i]: data[ticker_10y[i]]})


USD2s5s10s = data_per_country['USD' + 'PCW2s5s10s']
USD2s5s10s = USD2s5s10s[USD2s5s10s.index > '2018-01-01']

USD2s5s10s_daily_rtn = USD2s5s10s - USD2s5s10s.shift(1)


USD2s5s10s_pca = pca.PCA(USD2s5s10s, rates_pca = True)
USD2s5s10s_daily_rtn_pca = pca.PCA(USD2s5s10s_daily_rtn, rates_pca = True)


weighted_fly, w, corr_test, description = USD2s5s10s_pca.hedge_pcs_on_existing_strat('PC1', np.array([-1,2,-1]), 1)


#USD2s5s10s_pca.eigvecs[0]
#strat_w/USD2s5s10s_pca.eigvecs[0]

w_anchor = 1
strat_w = np.array([-1,2,-1])

strat_w_hedged = strat_w/(USD2s5s10s_pca.loadings['Loading1']/USD2s5s10s_pca.loadings['Loading1'][w_anchor])
strat_w_hedged
strat_1 = USD2s5s10s_pca.data.dot(strat_w_hedged)

USD2s5s10s_pca.plot_loadings()

#test corr
np.corrcoef(USD2s5s10s_pca.pcs['PC1'], USD2s5s10s_pca.centered_data.dot(strat_w_hedged))
    
strat_1.plot()

strat_w_hedged2 = strat_w/(USD2s5s10s_daily_rtn_pca.loadings['Loading1']/USD2s5s10s_daily_rtn_pca.loadings['Loading1'][w_anchor])
strat_w_hedged2
strat_2 = USD2s5s10s_pca.data.dot(strat_w_hedged2)
strat_2.plot()




