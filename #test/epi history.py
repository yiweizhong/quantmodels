import pandas as pd

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper


data = pd.read_csv(r'C:/Dev/Models/QuantModels/#test/EPI history.csv')  
data['Date'] = pd.to_datetime(data['Date'])
data = data.set_index('Date').ffill()

#c.split('_')[1]

global_cols = [c for c in data.columns if 'GlobalGDP' in c]
US_cols =  [c for c in data.columns if 'US' in c]
AU_cols =  [c for c in data.columns if 'AU' in c]



data_us = data[US_cols].resample('W').last().dropna()
data_us_component = data_us.drop('EpiQ_Headline_US', axis=1)


