import pandas as pd
import os, sys, inspect
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns


_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper

def plot_main(ax, xs, ys, xlegend, ylegend, colors, cmaps, ticklabelsize=8):
    
    mapable = ax.scatter(xs, ys, c=colors, cmap=cmaps, marker='o', s = 6)
        
    ax.set_ylabel(ys.name, fontsize=8)
    ax.set_xlabel(xs.name, fontsize=8)
    ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
    ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
    
    for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    
    #for label,tick in zip(ax.get_xticklabels(), ax.get_xticks()):
    #    label.set_fontsize(ticklabelsize)
    #    if tick < 0:
    #        label.set_color("red")
    #    else:
    #        label.set_color("black")
    
    ymin, ymax = ax.get_ylim()
    xmin, xmax = ax.get_xlim()    
    xymax = max([abs(ymin), abs(ymax), abs(xmin), abs(xmax)])
    
    ax.set_xlim(-1 * xymax, xymax)
    ax.set_ylim(-1 * xymax, xymax)
    
    # Move left y-axis and bottim x-axis to centre, passing through (0,0)
    ax.spines['left'].set_position('center')
    ax.spines['bottom'].set_position('center')
    # Eliminate upper and right axes
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    # Show ticks in the left and lower axes only
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    
    ax.xaxis.set_major_locator(MultipleLocator(1))
    ax.yaxis.set_major_locator(MultipleLocator(1))
    ax.xaxis.grid(True, which='major')
    ax.yaxis.grid(True, which='major')
    
    for label in ax.get_xticklabels(minor =False):
        label.set_fontsize(7)

    for label in ax.get_yticklabels(minor =False):
        label.set_fontsize(7)
        
    ax.set_ylabel(ylegend, fontsize=9)
    ax.yaxis.set_label_coords(0, 0.12)

    ax.set_xlabel(xlegend, fontsize=9)
    ax.xaxis.set_label_coords(0.85, -0.025)
    
    # end points
    start_x = xs.loc[xs.first_valid_index()]
    start_y = ys.loc[ys.first_valid_index()]
    start_date = xs.first_valid_index().strftime(r'%Y-%m-%d')
    
    ds1 = ax.scatter(start_x, start_y, c='#f5d9c8', s=90, alpha=0.5, label=start_date, edgecolors='b')
    
    last_x = xs.loc[xs.last_valid_index()]
    last_y = ys.loc[ys.last_valid_index()]
    last_date = xs.last_valid_index().strftime(r'%Y-%m-%d')

    ds2 = ax.scatter(last_x, last_y, c='darkred', s=90, alpha=1, label=last_date)
    
    
    ax.legend([ds1, ds2], [start_date, last_date], scatterpoints=1, fancybox=True, framealpha=0, loc=1, ncol=1, fontsize=9)
    
    return mapable


data = pd.read_csv(r'C:/Temp/Test/market_watch/staging/2020-04-22_SWAP_SPREAD_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()

US_cols =  [c for c in data.columns if 'USD' in c]
US_cols.sort()
data_usd = data[US_cols].dropna()
data_usd = data_usd[[ 'USD_1Y_SS', 'USD_2Y_SS',
        'USD_3Y_SS', 'USD_4Y_SS', 'USD_5Y_SS', 'USD_6Y_SS',
       'USD_7Y_SS', 'USD_8Y_SS', 'USD_9Y_SS', 'USD_10Y_SS', 'USD_15Y_SS','USD_20Y_SS','USD_30Y_SS']]



data_usd_pca = pca.PCA(data_usd, rates_pca = True, momentum_size = None)

data_usd_pca.plot_loadings(n=3, m=5)



###########################

f1 = plt.figure(figsize = (20, 20))
gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
ax0 = plt.subplot(gs[0])

c1 = mpl.dates.date2num(data_usd_pca.pcs.index.to_pydatetime())
cmap1 = plt.cm.get_cmap('Reds')
mapable1 = plot_main(ax0, data_usd_pca.pcs['PC1'], data_usd_pca.pcs['PC2'], 'Infl bear steepen, RR Bull flatten', 'front end RR too high', c1, cmap1)

