import os, sys, inspect
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca

data = pd.read_csv('2018-02-14_USD_historical.csv') 
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date')   
data = data

for col in data.columns:
    if 'ED' in col:        
        data[col] = 1 - data[col]
     
data = data.dropna()
data['NOM_02_10'] = data.USGG10YR - data.USGG2YR
data['NOM_05_10'] = data.USGG10YR - data.USGG5YR
data['NOM_02_05'] = data.USGG5YR - data.USGG2YR
data['NOM_10_30'] = data.USGG30YR - data.USGG10YR

data['BE_02_10'] = data.USGGBE10 - data.USGGBE02
data['BE_05_10'] = data.USGGBE10 - data.USGGBE05
data['BE_02_05'] = data.USGGBE05 - data.USGGBE02
data['BE_10_30'] = data.USGGBE30 - data.USGGBE10


data['RR_02_10'] = data.USGGT10Y - data.USGGT02Y
data['RR_05_10'] = data.USGGT10Y - data.USGGT05Y
data['RR_02_05'] = data.USGGT05Y - data.USGGT02Y
data['RR_10_30'] = data.USGGT30Y - data.USGGT10Y

data['ED0812'] = data.ED12 - data.ED8
data['ED0711'] = data.ED11 - data.ED7
data['ED0610'] = data.ED10 - data.ED6
data['ED0509'] = data.ED9 - data.ED5

data = data.round(4)

level = data
move =  (data - data.shift(1)).dropna()


move_stats = move.describe().T  #.sort_values(by=['std'], ascending = False)
move_stats['skew'] = move.skew() 
move_stats['mode'] = move.mode().max()

#print move_stats.sort_values(by=['skew'], ascending = False)

#sns.kdeplot(move.USGG10YR, shade=True,rug=True);
#sns.distplot(move.USGG10YR, hist=False, rug=True);

ben_strat = move[['ED0812', 'USGG10YR']]



def func(s):
    if s > 2.4:
        return '>= 2.4'
    else:
        return '< 2.4'
    
ben_strat['FWISUS55'] = level['FWISUS55'].apply(func)

print ben_strat

#print ben_strat

sns.jointplot(x = 'ED0812', y = 'USGG10YR',  data = ben_strat, kind="reg", xlim=(-0.15,0.15), ylim=(-0.15,0.15))

g = sns.lmplot(x = 'ED0812', y = 'USGG10YR', hue = 'FWISUS55',  data = ben_strat, fit_reg=True)
g.set(xlim=(-0.15,0.15), ylim=(-0.15,0.15))


ben_strat['green_spreads vs 10'] = level['USGG10YR'] - level['ED0812']
ben_strat['FWISUS55'] = level['FWISUS55']

ben_strat['green_spreads vs 10 zscore'] = (ben_strat['green_spreads vs 10'] - ben_strat['green_spreads vs 10'].mean())/ben_strat['green_spreads vs 10'].std()
ben_strat['FWISUS55 zscore'] = (ben_strat['FWISUS55'] - ben_strat['FWISUS55'].mean())/ben_strat['FWISUS55'].std()


ben_strat[['green_spreads vs 10 zscore', 'FWISUS55 zscore']].plot()



g = sns.lmplot(x = 'green_spreads vs 10 zscore', y = 'FWISUS55 zscore',  data = ben_strat, fit_reg=True)
sns.jointplot(x = 'green_spreads vs 10 zscore', y = 'FWISUS55 zscore',  data = ben_strat, kind="reg")

g.set(xlim=(-0.15,0.15), ylim=(-0.15,0.15))



sns.jointplot(x = 'ED0812', y = 'USGG10YR', data = ben_strat.tail(40), kind="reg", xlim=(-0.15,0.15), ylim=(-0.15,0.15))


ben_strat3 = pd.DataFrame({'USGG10YR' :move['USGG10YR'],  'ED0812': move['ED0812']}).loc['2018-02-07':,:]

print ben_strat3


sns.jointplot(x = 'ED0812', y = 'USGG10YR', data = ben_strat3.tail(10), kind="reg", xlim=(-0.15,0.15), ylim=(-0.15,0.15))


me_strat = move[['CDFS0202', 'ED12', 'ED0812' ]].loc['2017-12-10':,:]
me_strat_stats = me_strat.describe().T
me_strat_stats['skew'] = me_strat.skew()
print me_strat_stats 
sns.jointplot(x = 'CDFS0202', y = 'ED12',  data = me_strat, kind="reg" , xlim=(-0.15,0.15), ylim=(-0.15,0.15))


gradient, intercept, r_value, p_value, std_err = stats.linregress(me_strat['CDFS0202'],me_strat['ED12'])
print gradient, intercept, r_value, p_value, std_err 


gradient, intercept, r_value, p_value, std_err = stats.linregress(me_strat['CDFS0202'],me_strat['ED0812'])
print gradient, intercept, r_value, p_value, std_err 


#PCA test

us_gov = ['USGG2YR','USGG5YR', 'USGG10YR','USGG30YR']
eds = ['ED9','ED10', 'ED11','ED12']
move_pca = pca.PCA(level[eds])

eigen_pct, eigvecs, eigvals = move_pca.get_eigens()

move_pca.loadings.plot(kind='bar')
move_pca.pcs['PC3'].plot()

print move_pca.eigvals


##############################################################################
# cross country correlation risks based PCA
##############################################################################

import os, sys, inspect
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca


data = pd.read_csv('2018-02-19_FWD_G10_historical.csv') 
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date')   
data = data.loc[data.index >'2017-09-01'].dropna()
print data
tenor = '1Y'
countries = set(data.columns.map(lambda x: x[:3]))



country_cols = map(lambda country: [col for col in data.columns if country in col and col.endswith(tenor)], countries)
country_pcas = map(lambda cols: pca.PCA(data[cols], rates_pca =True), country_cols)

country_pc1 = pd.DataFrame({country_pca.data.columns[0][:3] : country_pca.pcs['PC1'] for country_pca in country_pcas})
sns.heatmap(country_pc1.corr(), annot=True,linewidth=0.5,  cmap="YlGnBu")

country_pc2 = pd.DataFrame({country_pca.data.columns[0][:3] : country_pca.pcs['PC2'] for country_pca in country_pcas})
sns.heatmap(country_pc2.corr(), annot=True,linewidth=0.5,  cmap="YlGnBu")

country_pc3 = pd.DataFrame({country_pca.data.columns[0][:3] : country_pca.pcs['PC3'] for country_pca in country_pcas})
sns.heatmap(country_pc3.corr(), annot=True,linewidth=0.5,  cmap="YlGnBu")

country_eigval1 = pd.Series({country_pca.data.columns[0][:3] : country_pca.eigvals[0] for country_pca in country_pcas})
country_eigval2 = pd.Series({country_pca.data.columns[0][:3] : country_pca.eigvals[1] for country_pca in country_pcas})
country_eigval3 = pd.Series({country_pca.data.columns[0][:3] : country_pca.eigvals[2] for country_pca in country_pcas})

country_eigvals = pd.DataFrame({"Eig1":country_eigval1,"Eig2":country_eigval2,"Eig3":country_eigval3}).transpose()

print country_eigvals

sns.heatmap(country_eigvals, annot=True,linewidth=0.5,  cmap="YlGnBu")



################################################################################
# long term terminal rates using first 2 pcs
##############################################################################


import os, sys, inspect
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca

import itertools

data = pd.read_csv('2018-02-19_FWD_G10_historical.csv') 
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date')   
data = data.dropna()
print data.head()
tenor = '9Y_1Y'
countries = set(data.columns.map(lambda x: x[:3]))

country_cols = map(lambda country: [col for col in data.columns if country in col and col.endswith(tenor)], countries)
country_cols = list(itertools.chain.from_iterable(country_cols))
country_9y1y_pca = pca.PCA(data[country_cols], rates_pca =True)
print country_9y1y_pca.eigvals

country_9y1y_pca.loadings[['Loading1', 'Loading2', 'Loading3']].plot(kind='bar')
country_9y1y_pca.reconstruct_data_by_n_pc(n=[1, 2])

sns.heatmap((country_9y1y_pca.reconstructed_data + country_9y1y_pca.data_mean).tail(1), annot=True,linewidth=0.5,  cmap="YlGnBu")
sns.heatmap(country_9y1y_pca.reconstruction_data_gaps.tail(1), annot=True,linewidth=0.5,  cmap="YlGnBu")

################################################################################
# 
##############################################################################


import os, sys, inspect
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import itertools
from scipy import stats

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca


data = pd.read_csv('2018-02-19_FWD_G10_historical.csv') 
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date')   
data = data.loc[data.index>'2016-01-01'].dropna()



    
(data[['JPY_1Y_10Y','CAD_5Y_1Y']].loc[data.index>'2018-01-01'] - data[['JPY_1Y_10Y','CAD_1Y_1Y']].loc[data.index>'2018-01-01'].iloc[0]).plot()


tenor = '_1Y'
countries = set(data.columns.map(lambda x: x[:3]))
#countries = ['USD','EUR','JPY']
country_cols = map(lambda country: [col for col in data.columns if country in col and col.endswith(tenor)], countries)
country_cols = [ country_c[1:-2] for country_c in country_cols]
country_pcas = map(lambda cols: (cols[0][:3], pca.PCA(data[cols], rates_pca =True)), country_cols)
country_pcas = {country: country_pca for (country, country_pca) in country_pcas}

country_pcas['CAD'].reconstruct_data_by_n_pc(n=[1,2,3])

print country_pcas['CAD'].reconstruction_data_gaps.iloc[-1]

country_pcas['CAD'].reconstruction_data_gaps.tail(1).plot(kind='bar')

sns.heatmap(country_pcas['CAD'].reconstruction_data_gaps.tail(1), annot=True,linewidth=0.5,  cmap="YlGnBu")

CAD = country_pcas['CAD'].data.copy()
CAD.columns = [ col.replace('CAD_','') for col in CAD.columns]  
USD = country_pcas['USD'].data.copy()
USD.columns = [ col.replace('USD_','') for col in USD.columns]
CADUSD_pca = pca.PCA(CAD-USD, rates_pca=True) 
CADUSD_pca.reconstruct_data_by_n_pc(n=[1,2,3])
CADUSD_pca.reconstruction_data_gaps.tail(1).plot(kind='bar')



country_pcas['USD'].loadings[['Loading1', 'Loading2', 'Loading3']].plot(kind='bar')
country_pcas['USD'].reconstruct_data_by_n_pc(n=[1,2])
print country_pcas['USD'].eigvals 
country_pcas['USD'].reconstruction_data_gaps.tail(1).plot(kind='bar')




i = 331

fig = plt.figure()
for c, pca in country_pcas.iteritems():
    print c
    print pca.eigvals
    ax = plt.subplot(i)
    pca.loadings[['Loading1', 'Loading2', 'Loading3', 'Loading4']].plot(kind='bar', ax=ax, ylim = (-0.8,0.8))
    ax.xaxis.set_tick_params(labelsize=6, direction='in')
    ax.yaxis.set_tick_params(labelsize=6)
    i = i + 1

plt.tight_layout(pad=1, w_pad=0, h_pad=0)

i = 331
fig = plt.figure()
for c, pca in country_pcas.iteritems():
    print c
    print pca.eigvals
    ax = plt.subplot(i)
    ax.set_title(c)
    pca.pcs[['PC1', 'PC2', 'PC3','PC4']].tail(100).plot(ax=ax,secondary_y=['PC3','PC4'], ylim = (-0.8,0.8))
    ax.xaxis.set_tick_params(labelsize=6)
    ax.yaxis.set_tick_params(labelsize=6)
    i = i + 1

plt.tight_layout()



country_pcas['CAD'].loadings[['Loading1', 'Loading2', 'Loading3', 'Loading4']].plot(kind='bar', fontsize = 6)
plt.tight_layout()

country_pcas['CAD'].pcs[['PC1', 'PC2', 'PC3','PC4']].tail(100).plot(secondary_y=['PC3','PC4'], fontsize = 6)
plt.tight_layout()


i = 331

for c, pca in country_pcas.iteritems():
    print c
    print pca.eigvals
    _, _ = pca.reconstruct_data_by_n_pc(n=[1,2, 3])
    ax = plt.subplot(i)
    pca.reconstruction_data_gaps.tail(1).plot(kind='bar', ax=ax)
    i = i + 1



##########################################



################################################################################
# 
##############################################################################

import os, sys, inspect
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import itertools
from scipy import stats

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca


data = pd.read_csv('C:/Temp/test/market_watch/staging/2018-02-25_SOVCDS5YR_historical.csv') 
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date')

data = data.loc[data.index>'2016-01-01'].ffill()
EUR = ['GBR','FRA','DEU','AUT','SWE','ITA','NLD','SPA']

data[EUR].cov()
EURCDS_PCA = pca.PCA(data[EUR],rates_pca=True)

EURCDS_PCA.loadings[['Loading1', 'Loading2', 'Loading3', 'Loading4']].plot(kind='bar', fontsize = 6)



##########################################

cols = [col for col in data.columns if 'CAD' in col and col.endswith(tenor)][2:11]
cad_1y_fwd_pca = pca.PCA(data[cols], rates_pca =True)

cols = [col for col in data.columns if 'USD' in col and col.endswith(tenor)][2:11]
usd_1y_fwd_pca = pca.PCA(data[cols], rates_pca =True)

cols = [col for col in data.columns if 'EUR' in col and col.endswith(tenor)]
eur_1y_fwd_pca = pca.PCA(data[cols], rates_pca =True)

pd.DataFrame({'cad':cad_1y_fwd_pca.pcs['PC1'], 'usd':usd_1y_fwd_pca.pcs['PC1'], 'eur':  eur_1y_fwd_pca.pcs['PC1']}).tail(200).plot()


pd.DataFrame({'cad':cad_1y_fwd_pca.pcs['PC2'], 'usd':usd_1y_fwd_pca.pcs['PC2'], 'eur':  eur_1y_fwd_pca.pcs['PC2']}).tail(200).plot()


pd.DataFrame({'cad':cad_1y_fwd_pca.pcs['PC3'], 'usd':usd_1y_fwd_pca.pcs['PC3'], 'eur':  eur_1y_fwd_pca.pcs['PC3']}).tail(200).plot()


cad_1y_fwd_pca.loadings[['Loading1', 'Loading2', 'Loading3']].plot(kind='bar')

usd_1y_fwd_pca.loadings[['Loading1', 'Loading2', 'Loading3']].plot(kind='bar')

eur_1y_fwd_pca.loadings[['Loading1', 'Loading2', 'Loading3','Loading4' ]].plot(kind='bar')


eur_1y_fwd_pca.data[['EUR_8Y_1Y', 'EUR_9Y_1Y']].plot()

print cad_1y_fwd_pca.eigvals

print cad_1y_fwd_pca.loadings

print cad_1y_fwd_pca.pcs.head()


plt.bar(np.arange(0, len(eur_1y_fwd_pca.eigvals)), eur_1y_fwd_pca.eigvals)


vec = cad_1y_fwd_pca.eigvecs[0]
print cad_1y_fwd_pca.eigvecs[0]
vec = vec * -1 if len(np.extract(vec > 0, vec))/len(vec) < 0.5 else vec
cad_1y_fwd_pca.eigvecs[0] = vec
print cad_1y_fwd_pca.eigvecs[0]

vec = cad_1y_fwd_pca.eigvecs[2] 
print vec


len(vec)
chop = int((len(vec) + 1)/4.0)

vec[chop + 1: len(vec) - chop + 1]



eur_1y_fwd_pca.reconstruct_data_by_n_pc()
eur_1y_fwd_pca.reconstruction_data_gaps.dropna().tail(1).plot(kind='bar')

print eur_1y_fwd_pca.eigvals

















