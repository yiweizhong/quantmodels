##############################################################################
# 
##############################################################################
import os, sys, inspect
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats
from pytz import UTC
from pytz import timezone
import datetime as dt

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins

data = pd.read_csv('C:/Temp/test/market_watch/staging/2018-03-14_USGG10YR_RATES_bar.csv') 
data['time'] = pd.to_datetime(data['time']).apply(lambda t: t.tz_localize(UTC).astimezone(timezone('Australia/NSW')))
data = data.set_index('time')
data = pd.DataFrame({'level':data['open'], 'change':data['close'] - data['open']})
us10_tick = data #  data[data['change'] <> 0 ]
us10_tick.name = 'USGG10'


us10_tick_last_night = us10_tick[us10_tick.index > '2018-03-13 20:00']
us10_tick_before_last_night = us10_tick[us10_tick.index <= '2018-03-13 20:00']

bin_obj = bins.create_equal_width_bins_with_high_evenness(us10_tick,20)

g = sns.kdeplot(us10_tick_before_last_night.change,  label = 'us10', bw=0.0005)
sns.kdeplot(us10_tick_last_night.change,  label = 'last night',  bw=0.0005, ax=g)



data = pd.read_csv('C:/Temp/test/market_watch/staging/2018-03-14_BAZ8_RATES_bar.csv') 
data['time'] = pd.to_datetime(data['time']).apply(lambda t: t.tz_localize(UTC).astimezone(timezone('Australia/NSW')))
data = data.set_index('time')
data = pd.DataFrame({'level':data['open'], 'change':data['close'] - data['open']})
cd10_tick = data #  data[data['change'] <> 0 ]
cd10_tick.name = 'BAZ8'


cd10_tick_last_night = cd10_tick[cd10_tick.index > '2018-03-13 20:00']
cd10_tick_before_last_night = cd10_tick[cd10_tick.index <= '2018-03-13 20:00']

g = sns.kdeplot(cd10_tick_before_last_night.change,  label = 'cd10', bw=0.0005)
sns.kdeplot(cd10_tick_last_night.change,  label = 'last night',  bw=0.0005, ax=g)

g = sns.kdeplot(us10_tick_before_last_night.change,  label = 'us10 last night',  bw=0.0005)
sns.kdeplot(cd10_tick_before_last_night.change,  label = 'BAZ8',  bw=0.0005, ax=g)



data = pd.read_csv('C:/Temp/test/market_watch/staging/2018-03-02_USGG5YR_RATES_bar.csv')  
data['time'] = pd.to_datetime(data['time'])
data = data.set_index('time')
data = pd.DataFrame({'level':data['open'], 'change':data['close'] - data['open']})
us5_downtime = data # data[data['change'] <> 0 ]
us5_downtime.name = 'USGG5'


data = pd.read_csv('C:/Temp/test/market_watch/staging/2018-03-02_EDZ9_RATES_bar.csv')  
data['time'] = pd.to_datetime(data['time'])
data = data.set_index('time')
data = 100 - data
data = pd.DataFrame({'level':data['open'], 'change':data['close'] - data['open']})
edz9_excluding_downtime = data # data[data['change'] <> 0 ]
edz9_excluding_downtime.name = 'EDZ9'



data = pd.read_csv('C:/Temp/test/market_watch/staging/2018-03-02_EDZ8_RATES_bar.csv')  
data['time'] = pd.to_datetime(data['time'])
data = data.set_index('time')
data = 100 - data
data = pd.DataFrame({'level':data['open'], 'change':data['close'] - data['open']})
edz8_excluding_downtime = data # data[data['change'] <> 0 ]
edz8_excluding_downtime.name = 'EDZ8'





data_excluding_downtime



g =sns.kdeplot(us5_downtime.change,  label = 'us5')
sns.kdeplot(us10_downtime.change,  label = 'us10', ax=g)
#sns.kdeplot(us10_downtime.change,  bw=.0005, label = 'us10', ls = ':')
sns.kdeplot(edz8_excluding_downtime.change,  label = 'edz8', ax=g)
sns.kdeplot(edz9_excluding_downtime.change,  label = 'edz9', ls = ':', ax=g)
plt.legend()
g.set(xlim=(-0.02, 0.02))


