from __future__ import division
import os, sys, inspect
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import *

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca# -*- coding: utf-8 -*-


data = pd.read_csv('C:/Temp/test/market_watch/staging/2018-03-05_FX_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date')
dt_10yrs_ago = data.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_2yrs_ago = data.index[-1] - DateOffset(years = 2, months=0, days=0)
def rename_loading_idx(loadings): return pd.Series(loadings.values, [i[4:] for i in loadings.index]) 


dual = data[['EUR','CAD']]
dual = dual[dual.index > dt_2yrs_ago]
dual.loc[:,'EUR'] = 1 / trio.loc[:,'EUR']
dual_pca = pca.PCA(dual)
dual_pca.loadings.plot(kind='bar')
dual_pca.pcs.plot()
dual_pca.loadings_lambda



trio_return = (trio - trio.shift(1))/trio.shift(1)
trio_return_pca = pca.PCA(trio_return)


trio_return_pca.loadings.plot(kind='bar')
trio_return_pca.loadings_lambda
trio_return_pca.pcs.plot()

trio_return.plot(secondary_y)
