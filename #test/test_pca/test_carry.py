
from __future__ import division
import os, sys, inspect
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import *
from itertools import permutations, combinations

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca


data = pd.read_csv('C:/Temp/test/market_watch/staging/2018-03-07_FWD_G10_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date')
def rename_loading_idx(loadings): return pd.Series(loadings.values, [i[4:] for i in loadings.index]) 


rates = [d for d in data.columns if '_3M_' not in d and '_0Y_3M' not in d and '_0Y_9M' not in d and '_0Y_6M' not in d  and '_0Y_25Y' not in d]


for rate in rates[:100]:

    country, fwd, tenor = rate.split('_')
    fwd_term = int(fwd.replace('Y',''))
    tenor_term = int(tenor.replace('Y',''))
    carry_proxy = None if fwd <> '0Y' else '%s_%s_%s' % (country, '3M', tenor)
    
    
    rolldown_proxy = ''
    
    if fwd_term == 15:
        rolldown_proxy = '%s_10Y_%s' % (country, tenor)
    elif fwd_term > 0:
        rolldown_proxy = '%s_%s_%s' % (country, '%sY' % (fwd_term -1), tenor)
    elif tenor_term == 30:
        rolldown_proxy = '%s_0Y_25Y' % (country)
    elif tenor_term > 1:
        rolldown_proxy = '%s_%s_%s' % (country, fwd, '%sY' % (tenor_term -1))
    elif country == 'JPY_0Y_1Y':
        rolldown_proxy = 'JPY_0Y_6M'
    else:
        rolldown_proxy = '%s_0Y_9M' % (country)
        
    
    print (rate, carry_proxy, rolldown_proxy)





