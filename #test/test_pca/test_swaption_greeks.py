from __future__ import division
import os, sys, inspect
import pandas as pd
import numpy as np
from scipy.stats import norm
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)


from statistics.factor import pca

data = pd.read_csv('2018-02-16_VOL_historical.csv') 
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date')   
data = data



def price_swap_norm(fwd, strike, expiry, vol, option_type = 'P'):
    vol_t = vol * np.power(expiry, 0.5)/100
    d = (fwd - strike)/vol_t
    n_payer_premium = vol_t * (norm.pdf(d) + d*norm.cdf(d))
    if option_type.upper() == 'P':
        return n_payer_premium
    else:
        return  n_payer_premium - (fwd - strike)   


def delta(fwd, strike, expiry, vol, option_type):
    dl = price_swap_norm(fwd-0.005, strike, expiry, vol, option_type)
    dh = price_swap_norm(fwd+0.005, strike, expiry, vol, option_type)
    return (dh-dl) * 100

def gamma(fwd, strike, expiry, vol, option_type):
    dl = delta(fwd-0.005, strike, expiry, vol, option_type)
    dh = delta(fwd+0.005, strike, expiry, vol, option_type)
    return (dh-dl)

def theta(fwd, strike, expiry, vol, option_type):
    dl = price_swap_norm(fwd, strike, expiry, vol, option_type)
    dh = price_swap_norm(fwd, strike, expiry-1/365, vol, option_type)
    return (dh-dl)
    
_premium = price_swap_norm(2.305229, 2.305229, 28/365, 35.44, option_type ='P')         
_delta = delta(2.305229, 2.305229, 28/365, 35.44, option_type ='P')
_gamma = gamma(2.305229, 2.305229, 28/365, 35.44, option_type ='P')
_theta = theta(2.305229, 2.305229, 28/365, 35.44, option_type ='P')
print _premium
print _delta
print _gamma * 984
print _theta * 100 * 984



    

