from __future__ import division
import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations
_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)
from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper



def smooth_series(ds, lim=2, half_window=1):

    def smooth(row):

        if np.isnan(row['ds_prev']) or np.isnan(row['ds_next']):
            return row['ds']

        if abs(row['ds_prev'] - row['ds']) > lim and abs(row['ds_next'] - row['ds']) > lim and abs((row['ds_prev'] + row['ds_next'])/2 - row['ds']) > lim:
            return (row['ds_prev'] + row['ds_next'])/2
        else:
            return row['ds']

    data = pd.DataFrame({'ds':ds, 'ds_prev': ds.shift(half_window), 'ds_next': ds.shift(-1*half_window)})

    return data.apply(smooth, axis=1)


def smooth_fwd_rates(data, limit, half_window): 
    
    out = {}
    
    for col, ds in data.iteritems():
        
        out[col] = smooth_series(ds, lim=limit, half_window=half_window)
        
        if int(col.split('_')[1].replace('Y','')) != 0:
            out[col] = smooth_series(ds, lim=limit, half_window=half_window)
        else:
            out[col] = ds
    
    return pd.DataFrame(out)
    




# data
data = pd.read_csv('C:/Temp/test/market_watch/staging/2018-08-22_SWAP_ALL_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()

#dates
dt_10yrs_ago = data.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_5yrs_ago = data.index[-1] - DateOffset(years = 5, months=0, days=0)
dt_3yrs_ago = data.index[-1] - DateOffset(years = 3, months=0, days=0)
dt_2yrs_ago = data.index[-1] - DateOffset(years = 2, months=0, days=0)
dt_1yrs_ago = data.index[-1] - DateOffset(years = 1, months=0, days=0)
dt_18mths_ago = data.index[-1] - DateOffset(years = 0, months=18, days=0)
dt_6mths_ago = data.index[-1] - DateOffset(years = 0, months=6, days=0)

#countries
countries = set(data.columns.map(lambda x: x[:3]))




#######################################
#beta
#######################################

#ideally this beta should not change but we are in a regime change right now. hence rolling 5years

# yield effect 10s
global_10yr_tickers = [c for c in data.columns if '_0Y_10Y' in c and  'NOK' not in c]
global_10yr_data_sub = data[data.index> dt_5yrs_ago][global_10yr_tickers]    
global_10yr_beta_pca = pca.PCA(global_10yr_data_sub, rates_pca = True)
synth_global_10yr, synth_global_10yr_w, synth_global_10yr_corr, synth_global_10yr_desc = global_10yr_beta_pca.risk_in_a_pc('PC1')
synth_global_10y = synth_global_10yr/synth_global_10yr_w.sum()


# curve effect 2s10s
global_2yr_tickers = [c for c in data.columns if '_0Y_2Y' in c and  'NOK' not in c]
global_10yr_tickers = [c for c in data.columns if '_0Y_10Y' in c and  'NOK' not in c]
global_2s10s_data = {}
for i in np.arange(0, len(global_2yr_tickers)):
     global_2s10s_data[global_10yr_tickers[i][:3]] =  data[global_10yr_tickers[i]] - data[global_2yr_tickers[i]]
global_2s10s_data = pd.DataFrame(global_2s10s_data)
global_2s10s_data_sub = global_2s10s_data[global_2s10s_data.index>dt_5yrs_ago]  
global_2s10s_beta_pca = pca.PCA(global_2s10s_data_sub, rates_pca = True)
synth_global_2s10s, synth_global_2s10s_w, ssynth_global_2s10s_corr, synth_global_2s10s_desc = global_2s10s_beta_pca.risk_in_a_pc('PC1')
synth_global_2s10s = synth_global_2s10s/synth_global_2s10s_w.sum()

global_betas = pd.DataFrame({'global_rates_level':synth_global_10y, 'global_2s10s_spread':synth_global_2s10s})


#######################################
# current risk
#######################################

existing_risks = {
                   'Fly -1 * AU2 + 3.154 * AU5 -1.965 * AU10': {'AUD_0Y_2Y': -1,'AUD_0Y_5Y': 3.154, 'AUD_0Y_10Y': -1.965},
                   'Fly -1 * EU2 + 2.525 * EU5 -1.44 * EU10': {'EUR_0Y_2Y': -1,'EUR_0Y_5Y': 2.525, 'EUR_0Y_10Y': -1.44}
                 }
existing_risks = pd.DataFrame({risk: data for (risk, data) in helper.calc_weighted_risk_cix(data, existing_risks)})
existing_risks = existing_risks[existing_risks.index > dt_5yrs_ago]







#######################################
# 5y5y Single security cross country spread
#########################################
data_tenor = [d for d in data.columns if '_5Y_5Y' in d] 
data_tenor = [ dt for dt in data_tenor if 'NOK' not in dt]


data_sub = data[data.index> dt_2yrs_ago].ffill()
data_sub_narrow = data_sub[data_tenor]
#data_sub_narrow.columns = [c.replace('_', '') for c in data_sub_narrow.columns]
data_sub_narrow_pca = pca.PCA(data_sub_narrow, rates_pca = True)
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)


gaps = data_sub_narrow_pca.data_gaps_from_reconstruction(n=[1, 2]).iloc[-1,:]
combos = [((a,b), abs(gaps[a] - gaps[b])) for (a,b) in list(combinations(gaps.index, 2))]
combos.sort(key = lambda a: a[1], reverse=True)

print(combos)


i = 1
weighted_data = {}
mr = {}
for (combo,_) in combos[:]: 
    if ('GBP' in combo[0] or 'GBP' in combo[1]):
        data_sub_narrow_pair = data[data.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    else:
        data_sub_narrow_pair = data[data.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    
    combo_type = 'spread' if combo[0][0:3] != combo[1][0:3] and combo[0][3:] == combo[1][3:] else 'N/A'    
    combo_type = 'curve' if combo[0][0:3] == combo[1][0:3] and combo[0][3:] != combo[1][3:]  else combo_type
    
    if combo_type != 'N/A':
        data_sub_narrow_pair.columns = [c.replace('_', '') for c in data_sub_narrow_pair.columns]
        data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
        #hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
        hedge_weighted_data, _, _, hedge_description = data_sub_narrow_pair_pca.pc_hedge
        weighted_data[hedge_description] = hedge_weighted_data
        mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20)
        mr[hedge_description] = mr_obj
        tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')
        
        fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
        plt.savefig('C:/Temp/test\market_watch/output/5y5y/%s_mr_%s_%s_%s.png' % (combo_type, combo[0], combo[1], tstmp), bbox_inches='tight')
        plt.close()
        
        i = i + 1


#########################################


#######################################
#Multi legs trade
#######################################

data_tenor_multi_legs = [d for d in data.columns if '_5Y_5Y' in d and ('USD' in d or 'SGD' in d or 'HKD' in d)]
data_sub_narrow = data[data_tenor_multi_legs].ffill()[data.index> dt_3yrs_ago]

combo_type = 'usd_sgd_hkd_5y5y'
data_sub_narrow_pca = pca.PCA(data_sub_narrow, rates_pca = True)
hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pca.pc_hedge
mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20)
mr[hedge_description] = mr_obj
tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')
fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pca)
plt.savefig('C:/Temp/test\market_watch/output/%s_%s.png' % (tstmp, combo_type), bbox_inches='tight')
plt.close()






#######################################
#box 2s10s
#######################################
        
def debug(c):
    return True
    if 'CAD' in c or 'USD' in c:
        return True
    else: 
        return False

data_tenor_2y = [d for d in data.columns if '_0Y_2Y' in d and 'NOK' not in d and debug(d)]
data_tenor_10y = [d for d in data.columns if '_0Y_10Y' in d and 'NOK' not in d and debug(d)]

data_sub = data.ffill()
data_sub_2y = data_sub[data_tenor_2y]
data_sub_2y.columns = [c[:3]  + '2s10s' for c in data_sub_2y.columns]

data_sub_10y = data_sub[data_tenor_10y]
data_sub_10y.columns = [c[:3]  + '2s10s' for c in data_sub_10y.columns]

data_sub_narrow = data_sub_10y - data_sub_2y
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)



data_sub_narrow_pca = pca.PCA(data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago], rates_pca = True)

gaps = data_sub_narrow_pca.data_gaps_from_reconstruction(n=[1, 2]).iloc[-1,:]
combos = [((a,b), abs(gaps[a] - gaps[b])) for (a,b) in list(combinations(gaps.index, 2))]
combos.sort(key = lambda a: a[1], reverse=True)

print(combos)


i = 1
mr = {}
for (combo,_) in combos[:]: 
    if ('GBP' in combo[0] or 'GBP' in combo[1]):
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    else:
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    
    combo_type = '2s10sbox'  
    
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20)
    mr[hedge_description] = mr_obj
    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')
    #fig = data_sub_narrow_pair_pca.create_hedged_pcs_fig()
    #plt.savefig('C:/Temp/test\market_watch/output/%s_%s_pca_hedge_rank_%s_%s_%s.png' % (tstmp, combo_type, i, combo[0], combo[1]), bbox_inches='tight')
    #plt.close()
    fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
    plt.savefig('C:/Temp/test\market_watch/output/2y10y/%s_%s_mr_%s_%s.png' % (tstmp, combo_type, combo[0], combo[1]), bbox_inches='tight')
    plt.close()
    
    i = i + 1




#######################################
#box 1y fwd 2s10s
#######################################
    
        
def debug(c):
    return True
    if 'CAD' in c or 'USD' in c:
        return True
    else: 
        return True

data_tenor_2y = [d for d in data.columns if '_1Y_2Y' in d and 'NOK' not in d and debug(d)]
data_tenor_10y = [d for d in data.columns if '_1Y_10Y' in d and 'NOK' not in d and debug(d)]

data_sub = data.ffill()
data_sub_2y = data_sub[data_tenor_2y]
data_sub_2y.columns = [c[:3]  + '1y2s10s' for c in data_sub_2y.columns]

data_sub_10y = data_sub[data_tenor_10y]
data_sub_10y.columns = [c[:3]  + '1y2s10s' for c in data_sub_10y.columns]

data_sub_narrow = data_sub_10y - data_sub_2y
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)


data_sub_narrow_pca = pca.PCA(data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago], rates_pca = True)

gaps = data_sub_narrow_pca.data_gaps_from_reconstruction(n=[1, 2]).iloc[-1,:]
combos = [((a,b), abs(gaps[a] - gaps[b])) for (a,b) in list(combinations(gaps.index, 2))]
combos.sort(key = lambda a: a[1], reverse=True)

print(combos)


i = 1
mr = {}
for (combo,_) in combos[:]: 
    if ('GBP' in combo[0] or 'GBP' in combo[1]):
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    else:
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    
    combo_type = '1y2s10sbox'  
    
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20)
    mr[hedge_description] = mr_obj
    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')
    #fig = data_sub_narrow_pair_pca.create_hedged_pcs_fig()
    #plt.savefig('C:/Temp/test\market_watch/output/%s_%s_pca_hedge_rank_%s_%s_%s.png' % (tstmp, combo_type, i, combo[0], combo[1]), bbox_inches='tight')
    #plt.close()
    fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
    plt.savefig('C:/Temp/test\market_watch/output/2y10y/%s_%s_mr_%s_%s.png' % (tstmp, combo_type, combo[0], combo[1]), bbox_inches='tight')
    plt.close()
    
    i = i + 1




#######################################
#box 2y fwd 2s10s
#######################################
    
        
def debug(c):
    return True
    if 'CAD' in c or 'USD' in c:
        return True
    else: 
        return True

data_tenor_2y = [d for d in data.columns if '_2Y_2Y' in d and 'NOK' not in d and debug(d)]
data_tenor_10y = [d for d in data.columns if '_2Y_10Y' in d and 'NOK' not in d and debug(d)]

data_sub = data.ffill()
data_sub_2y = data_sub[data_tenor_2y]
data_sub_2y.columns = [c[:3]  + '2y2s10s' for c in data_sub_2y.columns]

data_sub_10y = data_sub[data_tenor_10y]
data_sub_10y.columns = [c[:3]  + '2y2s10s' for c in data_sub_10y.columns]

data_sub_narrow = data_sub_10y - data_sub_2y
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)


data_sub_narrow_pca = pca.PCA(data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago], rates_pca = True)

gaps = data_sub_narrow_pca.data_gaps_from_reconstruction(n=[1, 2]).iloc[-1,:]
combos = [((a,b), abs(gaps[a] - gaps[b])) for (a,b) in list(combinations(gaps.index, 2))]
combos.sort(key = lambda a: a[1], reverse=True)

print(combos)


i = 1
mr = {}
for (combo,_) in combos[:]: 
    if ('GBP' in combo[0] or 'GBP' in combo[1]):
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    else:
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    
    combo_type = '2y2s10sbox'  
    
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20)
    mr[hedge_description] = mr_obj
    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')
    #fig = data_sub_narrow_pair_pca.create_hedged_pcs_fig()
    #plt.savefig('C:/Temp/test\market_watch/output/%s_%s_pca_hedge_rank_%s_%s_%s.png' % (tstmp, combo_type, i, combo[0], combo[1]), bbox_inches='tight')
    #plt.close()
    fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
    plt.savefig('C:/Temp/test\market_watch/output/2y10y/%s_%s_mr_%s_%s.png' % (tstmp, combo_type, combo[0], combo[1]), bbox_inches='tight')
    plt.close()
    
    i = i + 1



#######################################
#box 2s5s
#######################################
def debug(c):
    return True
    if 'CAD' in c or 'USD' in c:
        return True
    else: 
        return True

data_tenor_2y = [d for d in data.columns if '_0Y_2Y' in d and 'NOK' not in d and debug(d)]
data_tenor_5y = [d for d in data.columns if '_0Y_5Y' in d and 'NOK' not in d and debug(d)]

data_sub = data.ffill()
data_sub_2y = data_sub[data_tenor_2y]
data_sub_2y.columns = [c[:3]  + '2s5s' for c in data_sub_2y.columns]

data_sub_5y = data_sub[data_tenor_5y]
data_sub_5y.columns = [c[:3]  + '2s5s' for c in data_sub_5y.columns]

data_sub_narrow = data_sub_5y - data_sub_2y
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)


data_sub_narrow_pca = pca.PCA(data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago], rates_pca = True)

gaps = data_sub_narrow_pca.data_gaps_from_reconstruction(n=[1, 2]).iloc[-1,:]
combos = [((a,b), abs(gaps[a] - gaps[b])) for (a,b) in list(combinations(gaps.index, 2))]
combos.sort(key = lambda a: a[1], reverse=True)

print(combos)


i = 1
mr = {}
for (combo,_) in combos[:]: 
    if ('GBP' in combo[0] or 'GBP' in combo[1]):
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    else:
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    
    combo_type = '2s5sbox'  
    
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20)
    mr[hedge_description] = mr_obj
    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')
    #fig = data_sub_narrow_pair_pca.create_hedged_pcs_fig()
    #plt.savefig('C:/Temp/test\market_watch/output/%s_%s_pca_hedge_rank_%s_%s_%s.png' % (tstmp, combo_type, i, combo[0], combo[1]), bbox_inches='tight')
    #plt.close()
    fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
    plt.savefig('C:/Temp/test\market_watch/output/2s5s/%s_%s_mr_%s_%s.png' % (tstmp, combo_type, combo[0], combo[1]), bbox_inches='tight')
    plt.close()
    
    i = i + 1


#######################################
#box 1y fwd 2s5s
#######################################
def debug(c):
    return True
    if 'CAD' in c or 'USD' in c:
        return True
    else: 
        return True
    
data_tenor_2y = [d for d in data.columns if '_1Y_2Y' in d and 'NOK' not in d and debug(d)]
data_tenor_5y = [d for d in data.columns if '_1Y_5Y' in d and 'NOK' not in d and debug(d)]

data_sub = data.ffill()
data_sub_2y = data_sub[data_tenor_2y]
data_sub_2y.columns = [c[:3]  + '1y2s5s' for c in data_sub_2y.columns]

data_sub_5y = data_sub[data_tenor_5y]
data_sub_5y.columns = [c[:3]  + '1y2s5s' for c in data_sub_5y.columns]

data_sub_narrow = data_sub_5y - data_sub_2y
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)


data_sub_narrow_pca = pca.PCA(data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago], rates_pca = True)

gaps = data_sub_narrow_pca.data_gaps_from_reconstruction(n=[1, 2]).iloc[-1,:]
combos = [((a,b), abs(gaps[a] - gaps[b])) for (a,b) in list(combinations(gaps.index, 2))]
combos.sort(key = lambda a: a[1], reverse=True)

print(combos)


i = 1
mr = {}
for (combo,_) in combos[:]: 
    if ('GBP' in combo[0] or 'GBP' in combo[1]):
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    else:
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    
    combo_type = '1y2s5sbox'  
    
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20)
    mr[hedge_description] = mr_obj
    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')
    #fig = data_sub_narrow_pair_pca.create_hedged_pcs_fig()
    #plt.savefig('C:/Temp/test\market_watch/output/%s_%s_pca_hedge_rank_%s_%s_%s.png' % (tstmp, combo_type, i, combo[0], combo[1]), bbox_inches='tight')
    #plt.close()
    fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
    plt.savefig('C:/Temp/test\market_watch/output/2s5s/%s_%s_mr_%s_%s.png' % (tstmp, combo_type, combo[0], combo[1]), bbox_inches='tight')
    plt.close()
    
    i = i + 1



#######################################
#box 2y fwd 2s5s
#######################################
def debug(c):
    return True
    if 'CAD' in c or 'USD' in c:
        return True
    else: 
        return True


data_tenor_2y = [d for d in data.columns if '_2Y_2Y' in d and 'NOK' not in d and debug(d)]
data_tenor_5y = [d for d in data.columns if '_2Y_5Y' in d and 'NOK' not in d and debug(d)]

data_sub = data.ffill()
data_sub_2y = data_sub[data_tenor_2y]
data_sub_2y.columns = [c[:3]  + '2y2s5s' for c in data_sub_2y.columns]

data_sub_5y = data_sub[data_tenor_5y]
data_sub_5y.columns = [c[:3]  + '2y2s5s' for c in data_sub_5y.columns]

data_sub_narrow = data_sub_5y - data_sub_2y
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)


data_sub_narrow_pca = pca.PCA(data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago], rates_pca = True)

gaps = data_sub_narrow_pca.data_gaps_from_reconstruction(n=[1, 2]).iloc[-1,:]
combos = [((a,b), abs(gaps[a] - gaps[b])) for (a,b) in list(combinations(gaps.index, 2))]
combos.sort(key = lambda a: a[1], reverse=True)

print(combos)


i = 1
mr = {}
for (combo,_) in combos[:]: 
    if ('GBP' in combo[0] or 'GBP' in combo[1]):
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_18mths_ago].ffill()[[combo[0],combo[1]]]
    else:
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_3yrs_ago].ffill()[[combo[0],combo[1]]]
    
    combo_type = '2y2s5sbox'  
    
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20)
    mr[hedge_description] = mr_obj
    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')
    #fig = data_sub_narrow_pair_pca.create_hedged_pcs_fig()
    #plt.savefig('C:/Temp/test\market_watch/output/%s_%s_pca_hedge_rank_%s_%s_%s.png' % (tstmp, combo_type, i, combo[0], combo[1]), bbox_inches='tight')
    #plt.close()
    fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
    plt.savefig('C:/Temp/test\market_watch/output/2s5s/%s_%s_mr_%s_%s.png' % (tstmp, combo_type, combo[0], combo[1]), bbox_inches='tight')
    plt.close()
    
    i = i + 1



#######################################
#box 5s5y5
#######################################
def debug(c):
    return True
    if 'KRW' in c or 'USD' in c or 'SGD' in c or 'THB' in c:
        return True
    else: 
        return True
    
data_tenor_5y = [d for d in data.columns if '_0Y_5Y' in d and 'NOK' not in d and debug(d)]
data_tenor_5y5 = [d for d in data.columns if '_5Y_5Y' in d and 'NOK' not in d and debug(d)]

data_sub = data.ffill()

data_sub_5y = data_sub[data_tenor_5y]
data_sub_5y.columns = [c[:3]  + '5s5y5' for c in data_sub_5y.columns]

data_sub_5y5 = data_sub[data_tenor_5y5]
data_sub_5y5.columns = [c[:3]  + '5s5y5' for c in data_sub_5y5.columns]

data_sub_narrow = data_sub_5y5 - data_sub_5y
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)


data_sub_narrow_pca = pca.PCA(data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago], rates_pca = True)

gaps = data_sub_narrow_pca.data_gaps_from_reconstruction(n=[1, 2]).iloc[-1,:]
combos = [((a,b), abs(gaps[a] - gaps[b])) for (a,b) in list(combinations(gaps.index, 2))]
combos.sort(key = lambda a: a[1], reverse=True)

print(combos)


i = 1
mr = {}
resample_freq = 'W'

for (combo,_) in combos[:]: 
    if ('GBP' in combo[0] or 'GBP' in combo[1]):
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    else:
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    
    combo_type = '5s5y5'  
    
    data_sub_narrow_pair = data_sub_narrow_pair.resample(resample_freq).last()
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq=resample_freq)
    mr[hedge_description] = mr_obj
    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')
    #fig = data_sub_narrow_pair_pca.create_hedged_pcs_fig()
    #plt.savefig('C:/Temp/test\market_watch/output/%s_%s_pca_hedge_rank_%s_%s_%s.png' % (tstmp, combo_type, i, combo[0], combo[1]), bbox_inches='tight')
    #plt.close()
    fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
    plt.savefig('C:/Temp/test\market_watch/output/%s/%s_%s_mr_%s_%s.png' % (combo_type, tstmp, combo_type, combo[0], combo[1]), bbox_inches='tight')
    plt.close()
    
    i = i + 1

#########################################




#######################################
#box 5s10s
#######################################
def debug(c):
    return True
    if 'KRW' in c or 'USD' in c or 'SGD' in c or 'THB' in c:
        return True
    else: 
        return True
    
data_tenor_5y = [d for d in data.columns if '_0Y_5Y' in d and 'NOK' not in d and 'JPY' not in d and debug(d)]
data_tenor_10y = [d for d in data.columns if '_0Y_10Y' in d and 'NOK' not in d and 'JPY' not in d and debug(d)]

data_sub = data.ffill()

data_sub_5y = data_sub[data_tenor_5y]
data_sub_5y.columns = [c[:3]  + '5s10s' for c in data_sub_5y.columns]

data_sub_10y = data_sub[data_tenor_10y]
data_sub_10y.columns = [c[:3]  + '5s10s' for c in data_sub_10y.columns]

data_sub_narrow = data_sub_10y - data_sub_5y
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)


data_sub_narrow_pca = pca.PCA(data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago], rates_pca = True)

gaps = data_sub_narrow_pca.data_gaps_from_reconstruction(n=[1, 2]).iloc[-1,:]
combos = [((a,b), abs(gaps[a] - gaps[b])) for (a,b) in list(combinations(gaps.index, 2))]
combos.sort(key = lambda a: a[1], reverse=True)

print(combos)


i = 1
mr = {}
resample_freq = 'W'

for (combo,_) in combos[:]: 
    if ('GBP' in combo[0] or 'GBP' in combo[1]):
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    else:
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    
    combo_type = '5s10sbox'  
    
    data_sub_narrow_pair = data_sub_narrow_pair.resample(resample_freq).last()
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq=resample_freq)
    mr[hedge_description] = mr_obj
    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')
    #fig = data_sub_narrow_pair_pca.create_hedged_pcs_fig()
    #plt.savefig('C:/Temp/test\market_watch/output/%s_%s_pca_hedge_rank_%s_%s_%s.png' % (tstmp, combo_type, i, combo[0], combo[1]), bbox_inches='tight')
    #plt.close()
    fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
    plt.savefig('C:/Temp/test\market_watch/output/5s10s/%s_%s_mr_%s_%s.png' % (tstmp, combo_type, combo[0], combo[1]), bbox_inches='tight')
    plt.close()
    
    i = i + 1

#########################################


#######################################
#box 1y fwd 5s10s
#########################################
def debug(c):
    return True
    if 'KRW' in c or 'USD' in c or 'SGD' in c or 'THB' in c:
        return True
    else: 
        return True
    
data_tenor_5y = [d for d in data.columns if '_1Y_5Y' in d and 'NOK' not in d and 'JPY' not in d and debug(d)]
data_tenor_10y = [d for d in data.columns if '_1Y_10Y' in d and 'NOK' not in d and 'JPY' not in d and debug(d)]

data_sub = data.ffill()

data_sub_5y = data_sub[data_tenor_5y]
data_sub_5y.columns = [c[:3]  + '1y5s10s' for c in data_sub_5y.columns]

data_sub_10y = data_sub[data_tenor_10y]
data_sub_10y.columns = [c[:3]  + '1y5s10s' for c in data_sub_10y.columns]

data_sub_narrow = data_sub_10y - data_sub_5y
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)


data_sub_narrow_pca = pca.PCA(data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago], rates_pca = True)

gaps = data_sub_narrow_pca.data_gaps_from_reconstruction(n=[1, 2]).iloc[-1,:]
combos = [((a,b), abs(gaps[a] - gaps[b])) for (a,b) in list(combinations(gaps.index, 2))]
combos.sort(key = lambda a: a[1], reverse=True)

print(combos)


i = 1
mr = {}
resample_freq = 'W'

for (combo,_) in combos[:]: 
    if ('GBP' in combo[0] or 'GBP' in combo[1]):
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    else:
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    
    combo_type = '1y5s10sbox'  
    
    data_sub_narrow_pair = data_sub_narrow_pair.resample(resample_freq).last()
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq=resample_freq)
    mr[hedge_description] = mr_obj
    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')
    #fig = data_sub_narrow_pair_pca.create_hedged_pcs_fig()
    #plt.savefig('C:/Temp/test\market_watch/output/%s_%s_pca_hedge_rank_%s_%s_%s.png' % (tstmp, combo_type, i, combo[0], combo[1]), bbox_inches='tight')
    #plt.close()
    fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
    plt.savefig('C:/Temp/test\market_watch/output/5s10s/%s_%s_mr_%s_%s.png' % (tstmp, combo_type, combo[0], combo[1]), bbox_inches='tight')
    plt.close()
    
    i = i + 1

#########################################


#######################################
#box 2y fwd 5s10s
#########################################
def debug(c):
    return True
    if 'KRW' in c or 'USD' in c or 'SGD' in c or 'THB' in c:
        return True
    else: 
        return True
    
data_tenor_5y = [d for d in data.columns if '_2Y_5Y' in d and 'NOK' not in d and 'JPY' not in d and debug(d) ]
data_tenor_10y = [d for d in data.columns if '_2Y_10Y' in d and 'NOK' not in d and 'JPY' not in d and debug(d)]

data_sub = data.ffill()

data_sub_5y = data_sub[data_tenor_5y]
data_sub_5y.columns = [c[:3]  + '2y5s10s' for c in data_sub_5y.columns]

data_sub_10y = data_sub[data_tenor_10y]
data_sub_10y.columns = [c[:3]  + '2y5s10s' for c in data_sub_10y.columns]

data_sub_narrow = data_sub_10y - data_sub_5y
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)


data_sub_narrow_pca = pca.PCA(data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago], rates_pca = True)

gaps = data_sub_narrow_pca.data_gaps_from_reconstruction(n=[1, 2]).iloc[-1,:]
combos = [((a,b), abs(gaps[a] - gaps[b])) for (a,b) in list(combinations(gaps.index, 2))]
combos.sort(key = lambda a: a[1], reverse=True)

print(combos)


i = 1
mr = {}
resample_freq = 'W'

for (combo,_) in combos[:]: 
    if ('GBP' in combo[0] or 'GBP' in combo[1]):
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    else:
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    
    combo_type = '2y5s10sbox'  
    
    #data_sub_narrow_pair = data_sub_narrow_pair[data_sub_narrow_pair.index.dayofweek == 4]
    
    data_sub_narrow_pair = data_sub_narrow_pair.resample(resample_freq).last()
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq=resample_freq)
    mr[hedge_description] = mr_obj
    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')
    #fig = data_sub_narrow_pair_pca.create_hedged_pcs_fig()
    #plt.savefig('C:/Temp/test\market_watch/output/%s_%s_pca_hedge_rank_%s_%s_%s.png' % (tstmp, combo_type, i, combo[0], combo[1]), bbox_inches='tight')
    #plt.close()
    fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
    plt.savefig('C:/Temp/test\market_watch/output/5s10s/%s_%s_mr_%s_%s.png' % (tstmp, combo_type, combo[0], combo[1]), bbox_inches='tight')
    plt.close()
    
    i = i + 1

#########################################


#######################################
#flys 1y1ys2y2ys5y5ys
#########################################
data_tenor = [d for d in data.columns if '_1Y_1Y' in d and 'NOK' not in d] + \
             [d for d in data.columns if '_2Y_2Y' in d and 'NOK' not in d] + \
             [d for d in data.columns if '_5Y_5Y' in d and 'NOK' not in d]


data_sub = data[data.index> dt_5yrs_ago]
data_sub_narrow = data_sub[data_tenor]
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)


combos = [(a,b,c) for (a,b,c) in list(combinations(data_sub_narrow.columns, 3)) if (a[:3] == b[:3]) and (a[:3] == c[:3])]

print(combos)

i = 1
fly2s5s10s_weighted_data = {}
fly2s5s10s_mr = {}
resample_freq = 'W'



for combo in combos[:]: 
    if ('GBP' in combo[0]):
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1], combo[2]]]
    else:
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1], combo[2]]]
    
    combo_type = '1y1y2y2y5y5y'  

    data_sub_narrow_pair = data_sub_narrow_pair.resample(resample_freq).last()
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    fly2s5s10s_weighted_data[hedge_description] = hedge_weighted_data
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
    fly2s5s10s_mr[hedge_description] = mr_obj
    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')

    fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
    plt.savefig('C:/Temp/test\market_watch/output/%s/%s_mr_%s_%s_%s_%s.png' % (combo_type, combo_type, combo[0], combo[1], combo[2], tstmp), bbox_inches='tight')
    plt.close()
    
    i = i + 1

#########################################






#######################################
#flys 2s5s10s
#########################################
data_tenor = [d for d in data.columns if '_0Y_2Y' in d and 'NOK' not in d] + \
             [d for d in data.columns if '_0Y_5Y' in d and 'NOK' not in d] + \
             [d for d in data.columns if '_0Y_10Y' in d and 'NOK' not in d]


data_sub = data[data.index> dt_5yrs_ago]
data_sub_narrow = data_sub[data_tenor]
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)


combos = [(a,b,c) for (a,b,c) in list(combinations(data_sub_narrow.columns, 3)) if (a[:3] == b[:3]) and (a[:3] == c[:3])]

print(combos)

i = 1
fly2s5s10s_weighted_data = {}
fly2s5s10s_mr = {}
resample_freq = 'W'



for combo in combos[:]: 
    if ('GBP' in combo[0]):
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1], combo[2]]]
    else:
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1], combo[2]]]
    
    combo_type = '2s5s10s'  

    data_sub_narrow_pair = data_sub_narrow_pair.resample(resample_freq).last()
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    fly2s5s10s_weighted_data[hedge_description] = hedge_weighted_data
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
    fly2s5s10s_mr[hedge_description] = mr_obj
    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')

    fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
    plt.savefig('C:/Temp/test\market_watch/output/2s5s10s/%s_mr_%s_%s_%s_%s.png' % (combo_type, combo[0], combo[1], combo[2], tstmp), bbox_inches='tight')
    plt.close()
    
    i = i + 1

#########################################


#######################################
#flys 1y fwd 2s5s10s
#########################################
data_tenor = [d for d in data.columns if '_1Y_2Y' in d and 'NOK' not in d] + \
             [d for d in data.columns if '_1Y_5Y' in d and 'NOK' not in d] + \
             [d for d in data.columns if '_1Y_10Y' in d and 'NOK' not in d]


data_tenor

data_sub = data[data.index> dt_5yrs_ago]
data_sub_narrow = data_sub[data_tenor]
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)


combos = [(a,b,c) for (a,b,c) in list(combinations(data_sub_narrow.columns, 3)) if (a[:3] == b[:3]) and (a[:3] == c[:3])]

print(combos)

i = 1
fly2s5s10s_weighted_data = {}
fly2s5s10s_mr = {}
resample_freq = 'W'


for combo in combos[:]: 
    if ('GBP' in combo[0]):
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1], combo[2]]]
    else:
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1], combo[2]]]
    
    combo_type = '1y2s5s10s'  
    
    data_sub_narrow_pair = data_sub_narrow_pair.resample(resample_freq).last()
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    fly2s5s10s_weighted_data[hedge_description] = hedge_weighted_data
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
    fly2s5s10s_mr[hedge_description] = mr_obj
    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')

    fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
    plt.savefig('C:/Temp/test\market_watch/output/2s5s10s/%s_mr_%s_%s_%s_%s.png' % (combo_type, combo[0], combo[1], combo[2], tstmp), bbox_inches='tight')
    plt.close()
    
    i = i + 1

#########################################


#######################################
#flys 2y fwd 2s5s10s
#########################################
data_tenor = [d for d in data.columns if '_2Y_2Y' in d and 'NOK' not in d] + \
             [d for d in data.columns if '_2Y_5Y' in d and 'NOK' not in d] + \
             [d for d in data.columns if '_2Y_10Y' in d and 'NOK' not in d]


data_tenor

data_sub = data[data.index> dt_5yrs_ago]
data_sub_narrow = data_sub[data_tenor]
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)


combos = [(a,b,c) for (a,b,c) in list(combinations(data_sub_narrow.columns, 3)) if (a[:3] == b[:3]) and (a[:3] == c[:3])]

print(combos)

i = 1
fly2s5s10s_weighted_data = {}
fly2s5s10s_mr = {}
resample_freq = 'W'


for combo in combos[:]: 
    if ('GBP' in combo[0]):
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1], combo[2]]]
    else:
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1], combo[2]]]
    
    combo_type = '2y2s5s10s'  
    
    data_sub_narrow_pair = data_sub_narrow_pair.resample(resample_freq).last()
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    fly2s5s10s_weighted_data[hedge_description] = hedge_weighted_data
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
    fly2s5s10s_mr[hedge_description] = mr_obj
    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')

    fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
    plt.savefig('C:/Temp/test\market_watch/output/2s5s10s/%s_mr_%s_%s_%s_%s.png' % (combo_type, combo[0], combo[1], combo[2], tstmp), bbox_inches='tight')
    plt.close()
    
    i = i + 1

#########################################   
    


#######################################
#flys 5s10s30s
#########################################
_data_tenor = [d for d in data.columns if '_0Y_30Y' in d and 'NOK' not in d and 'HKD' not in d and 'THB' not in d]
data_tenor = [d.replace('_30Y','_5Y') for d in _data_tenor ] + \
             [d.replace('_30Y','_10Y') for d in _data_tenor ] + \
             _data_tenor 


data_sub = data[data.index> dt_5yrs_ago]
data_sub_narrow = data_sub[data_tenor]
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)


combos = [(a,b,c) for (a,b,c) in list(combinations(data_sub_narrow.columns, 3)) if (a[:3] == b[:3]) and (a[:3] == c[:3])]

print(combos)

i = 1
fly5s10s30s_weighted_data = {}
fly5s10s30s_mr = {}
resample_freq = 'W'



for combo in combos[:]: 
    if ('GBP' in combo[0]):
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1], combo[2]]]
    else:
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1], combo[2]]]
    
    combo_type = '5s10s30s'  

    data_sub_narrow_pair = data_sub_narrow_pair.resample(resample_freq).last()
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    fly5s10s30s_weighted_data[hedge_description] = hedge_weighted_data
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
    fly5s10s30s_mr[hedge_description] = mr_obj
    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')

    fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
    plt.savefig('C:/Temp/test\market_watch/output/5s10s30s/%s_mr_%s_%s_%s_%s.png' % (combo_type, combo[0], combo[1], combo[2], tstmp), bbox_inches='tight')
    plt.close()
    
    i = i + 1

#########################################


#######################################
#flys 2y2ys5y5y30s
#########################################
_data_tenor = [d for d in data.columns if '_0Y_30Y' in d and 'NOK' not in d and 'HKD' not in d and 'THB' not in d]
data_tenor = [d.replace('_0Y_30Y','_2Y_2Y') for d in _data_tenor ] + \
             [d.replace('_0Y_30Y','_5Y_5Y') for d in _data_tenor ] + \
             _data_tenor 


data_sub = data[data.index> dt_5yrs_ago]
data_sub_narrow = data_sub[data_tenor]
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)


combos = [(a,b,c) for (a,b,c) in list(combinations(data_sub_narrow.columns, 3)) if (a[:3] == b[:3]) and (a[:3] == c[:3])]

print(combos)

i = 1
fly5s10s30s_weighted_data = {}
fly5s10s30s_mr = {}
resample_freq = 'W'



for combo in combos[:]: 
    if ('GBP' in combo[0]):
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1], combo[2]]]
    else:
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1], combo[2]]]
    
    combo_type = '2y2ys5y5y30s'  

    data_sub_narrow_pair = data_sub_narrow_pair.resample(resample_freq).last()
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    fly5s10s30s_weighted_data[hedge_description] = hedge_weighted_data
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
    fly5s10s30s_mr[hedge_description] = mr_obj
    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')

    fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
    plt.savefig('C:/Temp/test\market_watch/output/5s10s30s/%s_mr_%s_%s_%s_%s.png' % (combo_type, combo[0], combo[1], combo[2], tstmp), bbox_inches='tight')
    plt.close()
    
    i = i + 1

#########################################






    
    

#######################################
#box 10s30s
#######################################
def debug(c):
    return True
    if 'KRW' in c or 'USD' in c or 'SGD' in c or 'THB' in c:
        return True
    else: 
        return True
    
data_tenor_10y = [d for d in data.columns if '_0Y_10Y' in d and 'KRW' not in d and 'NOK' not in d and 'JPY' not in d and 'HKD' not in d and 'THB' not in d and 'NZD' not in d and debug(d)]
data_tenor_30y = [d for d in data.columns if '_0Y_30Y' in d and 'KRW' not in d and 'NOK' not in d and 'JPY' not in d and 'HKD' not in d and 'THB' not in d and 'NZD' not in d and debug(d)]

data_sub = data.ffill()

data_sub_10y = data_sub[data_tenor_10y]
data_sub_10y.columns = [c[:3]  + '10s30s' for c in data_sub_10y.columns]

data_sub_30y = data_sub[data_tenor_30y]
data_sub_30y.columns = [c[:3]  + '10s30s' for c in data_sub_30y.columns]

data_sub_narrow = data_sub_30y - data_sub_10y
data_sub_narrow = smooth_fwd_rates(data_sub_narrow, 0.2, 1)


data_sub_narrow_pca = pca.PCA(data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago], rates_pca = True)

gaps = data_sub_narrow_pca.data_gaps_from_reconstruction(n=[1, 2]).iloc[-1,:]
combos = [((a,b), abs(gaps[a] - gaps[b])) for (a,b) in list(combinations(gaps.index, 2))]
combos.sort(key = lambda a: a[1], reverse=True)

print(combos)


i = 1
mr = {}
for (combo,_) in combos[:]: 
    if ('GBP' in combo[0] or 'GBP' in combo[1]):
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    else:
        data_sub_narrow_pair = data_sub_narrow[data_sub_narrow.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]
    
    combo_type = '10s30sbox'  
    
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20)
    mr[hedge_description] = mr_obj
    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')
    #fig = data_sub_narrow_pair_pca.create_hedged_pcs_fig()
    #plt.savefig('C:/Temp/test\market_watch/output/%s_%s_pca_hedge_rank_%s_%s_%s.png' % (tstmp, combo_type, i, combo[0], combo[1]), bbox_inches='tight')
    #plt.close()
    fig2 = mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pair_pca)
    plt.savefig('C:/Temp/test\market_watch/output/10s30s/%s_%s_mr_%s_%s.png' % (tstmp, combo_type, combo[0], combo[1]), bbox_inches='tight')
    plt.close()
    
    i = i + 1

#########################################





#############################################
#debug
############################################

