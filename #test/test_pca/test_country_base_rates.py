from __future__ import division
import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations


_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper


def restructure(data, tenors, curve_names):
    df_data = {}
    curve_names.sort()    
    for curve in curve_names:
        curve_columns = [curve + '_' + tenor for tenor in tenors]
        curve_data = data[curve_columns].rename(lambda x: x.split('_')[2], axis='columns')
        ds = curve_data.iloc[0]
        df_data[curve] = ds 
    df = pd.DataFrame(df_data)
    df.index.name = 'Tenor'
    return df



# data
    
filedate = '2018-08-17'

data = pd.read_csv('C:/Temp/test/market_watch/staging/'+ filedate + '_SWAP_ALL_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()


def create_pca(data, date_index):
    data_sub = data[date_index]    
    return pca.PCA(data_sub, rates_pca = True)



#dates
dt_10yrs_ago = data.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_5yrs_ago = data.index[-1] - DateOffset(years = 5, months=0, days=0)
dt_3yrs_ago = data.index[-1] - DateOffset(years = 3, months=0, days=0)
dt_2yrs_ago = data.index[-1] - DateOffset(years = 2, months=0, days=0)
dt_1yrs_ago = data.index[-1] - DateOffset(years = 1, months=0, days=0)
dt_18mths_ago = data.index[-1] - DateOffset(years = 0, months=18, days=0)

#countries
countries = set(data.columns.map(lambda x: x[:3]))

#tickers



#######################################
# countries
#######################################



filtered_countries = [
         #'AUD',
         #'CAD',
         #'EUR',
         #'GBP',
         #'HKD',
         #'JPY',
         #'KRW',
         #'NOK',
         #'NZD',
         #'SEK',
         'SGD',
         #'THB',
         'USD',
        ]

filtered_fwds = [
        '_0Y_',
        #'_1Y_',
        #'_2Y_',
        ]

filtered_tenors = [
        #'_3M',
        #'_1Y',
        '_2Y',
        #'_3Y',
        '_5Y',
        #'_6Y',
        #'_7Y',
        #'_8Y',
        #'_9Y',
        #'_10Y',
        #'_15Y',
        #'_30Y',
        ]

#######################################
# spot
#######################################


data_sub = data[data.index> dt_1yrs_ago]

spot_tickers = [ticker for ticker in data_sub.columns if any([filtered_country in ticker for filtered_country in filtered_countries])]               
spot_tickers = [ticker for ticker in spot_tickers if any([filtered_fwd in ticker for filtered_fwd in filtered_fwds])]
spot_tickers = [ticker for ticker in spot_tickers if any([ticker.endswith(filtered_tenor)  for filtered_tenor in filtered_tenors])]


  
data_sub_narrow = data_sub[spot_tickers]
data_sub_narrow.columns = [c.replace('_', '') for c in data_sub_narrow.columns]
data_sub_narrow_pca = pca.PCA(data_sub_narrow, rates_pca = True)

#data_sub_narrow_pca.plot_loadings_relationship()
data_sub_narrow_pca.plot_loadings()
data_sub_narrow_pca.create_hedged_pcs_fig()
data_sub_narrow_pca.plot_residules()
 

##
resample_freq = 'B' #'W'
hedge_weighted_data, weights, _, hedge_description = data_sub_narrow_pca.pc_hedge
mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
mr_obj.create_mr_info_fig((18, 7), pca_obj = data_sub_narrow_pca)


data_sub_narrow_pca.eigvecs.T[1]

x = np.polyfit(np.arange(len(data_sub_narrow_pca.eigvecs.T[1])), data_sub_narrow_pca.eigvecs.T[1], deg = 1, full=False)
x
#######################################
# fwd
#######################################


data_sub = data[data.index> dt_1yrs_ago]
fwd_tickers = [ticker for ticker in data_sub.columns \
                if fitlered_country in ticker \
                and ticker.endswith('_1Y') 
                ]
  
data_sub_narrow = data_sub[fwd_tickers]
data_sub_narrow.columns = [c.replace('_', '') for c in data_sub_narrow.columns]
data_sub_narrow_pca = pca.PCA(data_sub_narrow, rates_pca = True)

data_sub_narrow_pca.plot_loadings_relationship()
data_sub_narrow_pca.plot_loadings()














