import pandas as pd
import numpy as np


data = pd.read_csv(r'test.csv')  

#data
#Out[3]: 
#    key     val
#0   aaa    445
#1  bbb   5546
#2  ccc  ssgdf


#you can access your data in a panda dataframe like a dictionary, if you set the first column as the index

data = data.set_index(['key'])

#use the data as a dictionary like this 

data.loc['aaa']

#Out[14]: 
#val    445
#Name: aaa, dtype: object


#if you really like dictionary you can convert the dataframe to dict. it will use the row index as the dict key
data_dict = data.to_dict('index')
#access it like
data_dict['aaa']['val']

#data_dict['aaa']['val']
#Out[26]: '445'




df = pd.DataFrame({'a':[1,2,3], 'B':[np.NaN,np.NaN,np.NaN]})