import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations
import xlwings as xw

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper


#################################################################################################################
#################################################################################################################
#functions
#################################################################################################################
#################################################################################################################

def create_pca(data, date_index, momentum_size=None, resample_freq= 'W'):

    data_sub = data[date_index].resample(resample_freq).last() if resample_freq is not None else data[date_index]    
    return pca.PCA(data_sub, rates_pca = True, momentum_size = momentum_size)





#################################################################################################################
#################################################################################################################
# data
#################################################################################################################
#################################################################################################################



data = pd.read_csv('C:/Temp/test/market_watch/staging/2020-01-28_STIRFUT_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()


#dates
dt_10yrs_ago = data.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_5yrs_ago = data.index[-1] - DateOffset(years = 5, months=0, days=0)
dt_3yrs_ago = data.index[-1] - DateOffset(years = 3, months=0, days=0)
dt_2yrs_ago = data.index[-1] - DateOffset(years = 2, months=0, days=0)
dt_1yrs_ago = data.index[-1] - DateOffset(years = 1, months=0, days=0)
dt_18mths_ago = data.index[-1] - DateOffset(years = 0, months=18, days=0)

cross_countries_pca_history = dt_5yrs_ago






#tickers

ticker_3m = [c for c in data.columns if '_0Y_3M' in c and  'NOK' not in c]
ticker_1y = [c for c in data.columns if '_0Y_1Y' in c and  'NOK' not in c]
ticker_2y = [c for c in data.columns if '_0Y_2Y' in c and  'NOK' not in c]
ticker_3y = [c for c in data.columns if '_0Y_3Y' in c and  'NOK' not in c]
ticker_4y = [c for c in data.columns if '_0Y_4Y' in c and  'NOK' not in c]
ticker_5y = [c for c in data.columns if '_0Y_5Y' in c and  'NOK' not in c]
ticker_7y = [c for c in data.columns if '_0Y_7Y' in c and  'NOK' not in c]
ticker_10y = [c for c in data.columns if '_0Y_10Y' in c and  'NOK' not in c]
ticker_1y1y = [c for c in data.columns if '_1Y_1Y' in c and  'NOK' not in c]
ticker_1y2y = [c for c in data.columns if '_1Y_2Y' in c and  'NOK' not in c]
ticker_1y5y = [c for c in data.columns if '_1Y_5Y' in c and  'NOK' not in c]
ticker_1y10y = [c for c in data.columns if '_1Y_10Y' in c and  'NOK' not in c]
ticker_2y1y = [c for c in data.columns if '_2Y_1Y' in c and  'NOK' not in c]
ticker_2y2y = [c for c in data.columns if '_2Y_2Y' in c and  'NOK' not in c]
ticker_2y5y = [c for c in data.columns if '_2Y_5Y' in c and  'NOK' not in c]
ticker_2y10y = [c for c in data.columns if '_2Y_10Y' in c and  'NOK' not in c]
ticker_3y2y = [c for c in data.columns if '_3Y_2Y' in c and  'NOK' not in c]
ticker_5y5y = [c for c in data.columns if '_5Y_5Y' in c and  'NOK' not in c]
ticker_2y2y = [c for c in data.columns if '_2Y_2Y' in c and  'NOK' not in c]
ticker_30y = [c for c in data.columns if '_0Y_30Y' in c and  'NOK' not in c and 'THB' not in c and 'HKD' not in c and 'NZD' not in c and 'SGD' not in c]



#derive data


data_3ms2s = {}
if True:
    for i in np.arange(0, len(ticker_2y)):
         data_3ms2s[ticker_2y[i][:3] + '3ms2s'] =  data[ticker_2y[i]] - data[ticker_3m[i]]
    data_3ms2s = pd.DataFrame(data_3ms2s)

data_2s10s = {}
if True:
    for i in np.arange(0, len(ticker_2y)):
         data_2s10s[ticker_10y[i][:3] + '2s10s'] =  data[ticker_10y[i]] - data[ticker_2y[i]]
    data_2s10s = pd.DataFrame(data_2s10s)

data_2s5s = {}
if True:
    for i in np.arange(0, len(ticker_2y)):
         data_2s5s[ticker_5y[i][:3] + '2s5s'] =  data[ticker_5y[i]] - data[ticker_2y[i]]
    data_2s5s = pd.DataFrame(data_2s5s)

data_5s10s = {}
if True:
    for i in np.arange(0, len(ticker_10y)):
         data_5s10s[ticker_5y[i][:3] + '5s10s'] =  data[ticker_10y[i]] - data[ticker_5y[i]]
    data_5s10s = pd.DataFrame(data_5s10s)
    
data_10s30s = {}
ticker_temp = [c.replace('_0Y_30Y', '_0Y_10Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_10s30s[ticker_30y[i][:3] + '10s30s'] =  data[ticker_30y[i]] - data[ticker_temp[i]]
    data_10s30s = pd.DataFrame(data_10s30s)
    

data_2s30s = {}
ticker_temp = [c.replace('_0Y_30Y', '_0Y_2Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_2s30s[ticker_30y[i][:3] + '2s30s'] =  data[ticker_30y[i]] - data[ticker_temp[i]]
    data_2s30s = pd.DataFrame(data_2s30s)


data_2s5s10s = {}
if True:
    for i in np.arange(0, len(ticker_2y)):
         data_2s5s10s[ticker_2y[i][:3] + 'P2s5s10s'] =  2 * data[ticker_5y[i]] - data[ticker_2y[i]] - data[ticker_10y[i]]
    data_2s5s10s = pd.DataFrame(data_2s5s10s)
    

data_5s30s = {}
ticker_temp = [c.replace('_0Y_30Y', '_0Y_5Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_5s30s[ticker_30y[i][:3] + '5s30s'] =  data[ticker_30y[i]] - data[ticker_temp[i]]
    data_5s30s = pd.DataFrame(data_5s30s)



data_5s10s30s = {}
ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_5Y') for c in ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_5s10s30s[ticker_30y[i][:3] + 'P5s10s30s'] =  2 * data[ticker_temp2[i]] - data[ticker_temp1[i]] - data[ticker_30y[i]]
    data_5s10s30s = pd.DataFrame(data_5s10s30s)
    

data_2s10s30s = {}
ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_2Y') for c in ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_2s10s30s[ticker_30y[i][:3] + 'P2s10s30s'] =  2 * data[ticker_temp2[i]] - data[ticker_temp1[i]] - data[ticker_30y[i]]
    data_2s10s30s = pd.DataFrame(data_2s10s30s)



#i have to remove the data[ticker_3m] as the matrix is too big and introducing floating number calculation error





pcas = {}
pcas['global_2s5s10s_pca'] = create_pca(data_2s5s10s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
pcas['global_5s30s_pca'] = create_pca(data_5s30s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
pcas['global_10s30s_pca'] = create_pca(data_10s30s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
pcas['global_1y1y_pca'] = create_pca(data[ticker_1y1y], data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
pcas['global_5y5y_pca'] = create_pca(data[ticker_5y5y], data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
pcas['global_2y2y_pca'] = create_pca(data[ticker_2y2y], data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
pcas['global_2y5y_pca'] = create_pca(data[ticker_2y5y], data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
pcas['global_2s10s_pca'] = create_pca(data_2s10s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
pcas['global_2s5s_pca'] = create_pca(data_2s5s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
pcas['global_5s10s_pca'] = create_pca(data_5s10s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
pcas['global_10y_pca'] = create_pca(data[ticker_10y], data.index> dt_10yrs_ago, resample_freq= 'B', momentum_size=None)
pcas['global_3m_pca'] = create_pca(data[ticker_3m], data.index> dt_10yrs_ago, resample_freq= 'B', momentum_size=None)
pcas['global_3ms2s_pca'] = create_pca(data_3ms2s, data.index> dt_10yrs_ago, resample_freq= 'B', momentum_size=None)



pcas = {}
rates_curvs_frontend_pricing = pd.concat([data[ticker_10y], data_2s10s, data_3ms2s], join='outer', axis=1)
pcas['global_curvs_frontend_pricing_pca'] = create_pca(rates_curvs_frontend_pricing, data.index> dt_10yrs_ago, resample_freq= 'B', momentum_size=None)
pcas['global_curvs_frontend_pricing_pca'].plot_loadings(n=5, m=7)
pcas['global_curvs_frontend_pricing_pca'].plot_valuation_history(2300, n=[1, 2, 3], w=30, h=16, rows=5, cols=5, normalise_valuation_yaxis = False,  normalise_data_yaxis = True, exp_standardise_val = True )


rates_curvs_lower_bound = pd.concat([data[ticker_10y], data_2s10s, data[ticker_3m]], join='outer', axis=1)
pcas['global_rates_curvs_lower_bound_pca'] = create_pca(rates_curvs_lower_bound, data.index> dt_10yrs_ago, resample_freq= 'B', momentum_size=None)
pcas['global_rates_curvs_lower_bound_pca'].plot_loadings(n=5, m=7)
pcas['global_rates_curvs_lower_bound_pca'].plot_valuation_history(2300, n=[1, 2, 3], w=30, h=16, rows=5, cols=5, normalise_valuation_yaxis = False,  normalise_data_yaxis = True, exp_standardise_val = True )


rates_lower_bound_frontend_pricing = pd.concat([data[ticker_10y], data_3ms2s, data[ticker_3m]], join='outer', axis=1)
pcas['global_rates_lower_bound_frontend_pricing_pca'] = create_pca(rates_lower_bound_frontend_pricing, data.index> dt_10yrs_ago, resample_freq= 'B', momentum_size=None)
pcas['global_rates_lower_bound_frontend_pricing_pca'].plot_loadings(n=5, m=7)
pcas['global_rates_lower_bound_frontend_pricing_pca'].plot_valuation_history(2300, n=[1, 2, 3], w=30, h=16, rows=5, cols=5, normalise_valuation_yaxis = False,  normalise_data_yaxis = True, exp_standardise_val = True )



curves_lower_bound_frontend_pricing = pd.concat([data_2s10s, data_3ms2s, data[ticker_3m]], join='outer', axis=1)
pcas['global_curves_lower_bound_frontend_pricing_pca'] = create_pca(curves_lower_bound_frontend_pricing, data.index> dt_10yrs_ago, resample_freq= 'B', momentum_size=None)
pcas['global_curves_lower_bound_frontend_pricing_pca'].plot_loadings(n=5, m=7)
pcas['global_curves_lower_bound_frontend_pricing_pca'].plot_valuation_history(2300, n=[1, 2, 3], w=30, h=16, rows=5, cols=5, normalise_valuation_yaxis = False,  normalise_data_yaxis = True, exp_standardise_val = True )



