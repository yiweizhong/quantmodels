import xlwings as xw
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

xw.__version__

data = np.random.rand(2,2)

#always starts a new workbook
xw.view(data) 

#use the active workbook
xw.view(data,xw.sheets.active) 

# open a new workbook
wb1 = xw.Book()

#connect to a UNSAVED workbook by name. If the workbook is saved the workbook name needs to be full path
wb1 = xw.Book('Book3')

#add a sheet
wb2_sht = wb1.sheets.add('test_sheet')
xw.view(data,wb1.sheets('test_sheet')) 

#add 2 rows; 3 cols
wb2_sht.range('a10').value = [[1,'a',2],[5,6,7]]



#get value from the sheet
a = wb2_sht.range('a1').value
a
#get the range from the sheet
#2 rows and 3 cols
a = wb2_sht.range('a1').expand().value

#get the range from the sheet into a dataframe
a = wb2_sht.range('a1').options(pd.DataFrame, expand='table').value
a
#above code wont work properly as xlwings will treat the left col as the index and first row the column name
#need to use the index and header false option
a = wb2_sht.range('a1').options(pd.DataFrame, expand='table', index=False, header=False).value


#plot
fig = plt.figure()



#sheet obj
sht1 = wb1.sheets('Sheet1')



#update a range 
sht1.range('A1').value = 'test'
sht1.range('A1:B2').value = data
sht1.range('C1').formula = '=sum(A1:B2)'
sht1.range((3,1), (3,3)).value = 'test2'
#dsiplay value of a range as a list of list row major
sht1.range((1,1), (3,3)).value

# same as selecting the top left cell and control+A to select the adjacent cells into a list of list
sht1.range((1,1)).expand('table').value

# same as selecting the top left cell and control+A to select the adjacent cells into a Range object
sht1.range((1,1)).expand()
sht1.range((1,1)).expand().clear_contents()

#the range is a row major list of list. 

# copy array into a row
sht1.range((1,1)).value = [1,2,3]

# copy array into a col
sht1.range((1,1)).value = [[4],[5],[6]]
#or
sht1.range((1,1)).options(transpose=True).value = [7,8,9]



#read a range by row or col
sht1.range((1,1)).value = [[40,41],[50,51],[60,61]]
#read a range by row
sht1.range((1,1)).options(ndim=2, expand='right').value

#read a range by col
sht1.range((1,1)).options(ndim=2, expand='down').value


#slicing the range
sht1.range((1,1)).expand()[1:,1:].value


#list all the excel instance PIDs
xw.apps.keys()



