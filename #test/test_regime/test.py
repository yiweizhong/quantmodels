import pandas as pd# -*- coding: utf-8 -*-


def load_df(df):
    df[df.columns[0]] = pd.to_datetime(df[df.columns[0]])
    df = df.set_index(df.columns[0])
    df.index.name = 'Date'
    return df

##############################################################################################################################
#
# from Angus
#
##############################################################################################################################
PXLast = load_df(pd.read_csv('USPXLastData.csv'))
BestEPS = load_df(pd.read_csv('USBestEPSData.csv'))
BestPE = load_df(pd.read_csv('USBestPEData3.csv'))
BankNIMs = load_df(pd.read_csv('USNIMsData.csv'))
USEPIEx = load_df(pd.read_excel('US EPI.xlsx'))


PXLast['SPX Index BEPS'] = BestEPS['SPX Index'].copy() # EPS Growth
PXLast['SPX Index PE'] = BestPE['SPX Index'].copy() # PE Ratio
PXLast['JPM US Equity'] = BankNIMs['JPM US Equity'].copy()
PXLast['BAC US Equity'] = BankNIMs['BAC US Equity'].copy()
PXLast['C US Equity'] = BankNIMs['C US Equity'].copy()
PXLast['WFC US Equity'] = BankNIMs['WFC US Equity'].copy()
PXLast['USB US Equity'] = BankNIMs['USB US Equity'].copy()
PXLast['EpiA_Headline_US'] = USEPIEx['EpiA_Headline_US'].copy()
PXLast['EpiA_Business_US'] = USEPIEx['EpiA_Business_US'].copy()
PXLast['EpiA_Consumer_US'] = USEPIEx['EpiA_Consumer_US'].copy()
PXLast['EpiA_Employment_US'] = USEPIEx['EpiA_Employment_US'].copy()
PXLast['EpiA_Growth_US'] = USEPIEx['EpiA_Growth_US'].copy()
PXLast['EpiA_Inflation_US'] = USEPIEx['EpiA_Inflation_US'].copy()

#YOY%
PXLast['ALCBLOAN Index'] = (PXLast['ALCBLOAN Index']/PXLast['ALCBLOAN Index'].shift(12) -1)
PXLast['ALCBC&IL Index'] = (PXLast['ALCBC&IL Index']/PXLast['ALCBC&IL Index'].shift(12) -1)
PXLast['ALCBLLCN Index'] = (PXLast['ALCBLLCN Index']/PXLast['ALCBLLCN Index'].shift(12) -1)
PXLast['ALCBRLES Index'] = (PXLast['ALCBRLES Index']/PXLast['ALCBRLES Index'].shift(12) -1)
PXLast['USTBEXP Index'] = (PXLast['USTBEXP Index']/PXLast['USTBEXP Index'].shift(12) -1)
PXLast['USTBIMP Index'] = (PXLast['USTBIMP Index']/PXLast['USTBIMP Index'].shift(12) -1)
PXLast['AHE TOTL Index'] = (PXLast['AHE TOTL Index']/PXLast['AHE TOTL Index'].shift(12) -1)
PXLast['AWH TOTL Index'] = (PXLast['AWH TOTL Index']/PXLast['AWH TOTL Index'].shift(12) -1)
PXLast['ETSLTOTL Index'] = (PXLast['ETSLTOTL Index']/PXLast['ETSLTOTL Index'].shift(12) -1)

# Unemployment vs NAIRU estimate of 4.5% for Australia
PXLast['USURTOT Index'] = (PXLast['USURTOT Index'] - PXLast['CBOPNRUE Index'])
PXLast['USUDMAER Index'] = (PXLast['USUDMAER Index'] - PXLast['CBOPNRUE Index'])

# Inverse for unemployment and debt
PXLast['USURTOT Index'] = -1*PXLast['USURTOT Index']
PXLast['USUDMAER Index'] = -1*PXLast['USUDMAER Index']
PXLast['CPNFUSHG Index'] = -1*PXLast['CPNFUSHG Index']
PXLast['CPNFUSNG Index'] = -1*PXLast['CPNFUSNG Index']
PXLast['CPNFUSOG Index'] = -1*PXLast['CPNFUSOG Index']

# 12M SUm for trade balance
PXLast['USTBTOT Index'] = PXLast['USTBTOT Index'].rolling(12).sum()

# Household debt as % of income
#PXLast['CPNFAUA3 Index'] = (PXLast['CPNFAUA3 Index']/(PXLast['AUNATGI Index']/1000)) #HH income adj for blns

# Central Bank Balance sheet, convert all CB B/S to USD and Sum
PXLast['FARBCRED Index'] = ((PXLast['FARBCRED Index']/1000)+(PXLast['EBBSTOTA Index']*PXLast['EURUSD Curncy'])
                            +(PXLast['BJACTOTL Index']*PXLast['JPYUSD Curncy'])
                            +(PXLast['CNBMTTAS Index']*PXLast['CNYUSD Curncy']))                            
                            
# Current Account Balance G4 Sums
PXLast['USCABAL Index'] = PXLast['USCABAL Index']+PXLast['ECOCEAS Index']+PXLast['ECOCJPN Index']+PXLast['ECOCCNN Index']

# Real Rate Calcs policy and long end, and vs neutral rate for policy
PXLast['USGG10YR Index'] = PXLast['USGGBE10 Index'] - PXLast['USGG10YR Index'] #Real long end
PXLast['FDTR Index'] = ((PXLast['FDTR Index'] - (PXLast['G0169 5Y5Y BLC2 Curncy']+PXLast['PCE DEFY Index'])/2)
                        -(0.5*PXLast['USNRUS Index']+0.5*PXLast['USNREUAR Index'])) #Real policy rate

# Real currency v Terms of trade
PXLast['111.028 Index'] = PXLast['CTOTUSD Index']/PXLast['111.028 Index']


# Z-scoring at different time frames

# 0.5Y
PXLast05Yzs = (PXLast - PXLast.rolling(6).mean())/PXLast.rolling(6).std()

#1y
PXLast1Yzs = (PXLast - PXLast.rolling(12).mean())/PXLast.rolling(12).std()

#3Y
PXLast3Yzs = (PXLast - PXLast.rolling(3*12).mean())/PXLast.rolling(3*12).std()

#5y
PXLast5Yzs = (PXLast - PXLast.rolling(5*12).mean())/PXLast.rolling(5*12).std()

#7Y
PXLast7Yzs = (PXLast - PXLast.rolling(7*12).mean())/PXLast.rolling(7*12).std()

#10Y
PXLast10Yzs = (PXLast - PXLast.rolling(10*12).mean())/PXLast.rolling(10*12).std()



#Growth
growth = pd.DataFrame()
growth['EPI ex inflation M'] = PXLast05Yzs['EpiA_Headline_US']
growth['EPI ex inflation L'] = PXLast5Yzs['EpiA_Headline_US']
growth['EPI Synchronised Sectors or Regions M'] = (PXLast05Yzs['EpiA_Business_US']+PXLast05Yzs['EpiA_Consumer_US']+
                                                    PXLast05Yzs['EpiA_Employment_US']+PXLast05Yzs['EpiA_Growth_US']+
                                                    PXLast05Yzs['EpiA_Inflation_US'])/5
growth['EPI Synchronised Sectors or Regions L'] = (PXLast5Yzs['EpiA_Business_US']+PXLast5Yzs['EpiA_Consumer_US']+
                                                    PXLast5Yzs['EpiA_Employment_US']+PXLast5Yzs['EpiA_Growth_US']+
                                                    PXLast5Yzs['EpiA_Inflation_US'])/5
growth['Earnings Growth M'] = PXLast05Yzs['SPX Index BEPS']
growth['Earnings Growth L'] = PXLast7Yzs['SPX Index BEPS']
growth['Investment/Capex M'] = ((PXLast05Yzs['IP YOY Index']+PXLast5Yzs['GPDITOC% Index']
                                  +PXLast05Yzs['DGNOYOY Index']+PXLast05Yzs['CGNOXAY% Index'])/4)
growth['Investment/Capex L'] = ((PXLast5Yzs['IP YOY Index']+PXLast10Yzs['GPDITOC% Index']
                                  +PXLast5Yzs['DGNOYOY Index']+PXLast5Yzs['CGNOXAY% Index'])/4)
growth['Credit Growth M'] = ((PXLast1Yzs['ALCBLOAN Index']+PXLast1Yzs['ALCBC&IL Index']
                                  +PXLast1Yzs['ALCBLLCN Index']+PXLast1Yzs['ALCBRLES Index'])/4)
growth['Credit Growth L'] = ((PXLast7Yzs['ALCBLOAN Index']+PXLast7Yzs['ALCBC&IL Index']
                                  +PXLast7Yzs['ALCBLLCN Index']+PXLast7Yzs['ALCBRLES Index'])/4)
growth['Inventory Cycle M'] = ((PXLast05Yzs['MGT2TB Index']+PXLast05Yzs['MGT2MA Index']
                                  +PXLast05Yzs['MGT2WHDU Index'])/3)
growth['Inventory Cycle L'] = ((PXLast3Yzs['MGT2TB Index']+PXLast3Yzs['MGT2MA Index']
                                  +PXLast3Yzs['MGT2WHDU Index'])/3)
growth['Global Trade M'] = (PXLast1Yzs['USTBTOT Index']+PXLast1Yzs['USTBEXP Index']+PXLast1Yzs['USTBIMP Index'])/3
growth['Global Trade L'] = (PXLast7Yzs['USTBTOT Index']+PXLast7Yzs['USTBEXP Index']+PXLast7Yzs['USTBIMP Index'])/3
growth['Fiscal Impulse M'] = (PXLast5Yzs['FDDSGDP Index']+PXLast5Yzs['GPGSTOC% Index'])/2
growth['Fiscal Impulse L'] = (PXLast10Yzs['FDDSGDP Index']+PXLast10Yzs['GPGSTOC% Index'])/2


#Output Gap inflation
output_gap = pd.DataFrame()
output_gap['EPI Inflation M'] = PXLast05Yzs['EpiA_Inflation_US']
output_gap['EPI Inflation L'] = PXLast5Yzs['EpiA_Inflation_US']
output_gap['Wage Inflation M'] = ((PXLast05Yzs['AHE TOTL Index']+PXLast05Yzs['AWH TOTL Index']
                                +PXLast05Yzs['AWH TOTL Index'])/3)
output_gap['Wage Inflation L'] = ((PXLast7Yzs['AHE TOTL Index']+PXLast7Yzs['AWH TOTL Index']
                                +PXLast7Yzs['AWH TOTL Index'])/3)
output_gap['Core (Persistence/Breadth) M'] = ((PXLast05Yzs['CPI XYOY Index']+PXLast05Yzs['PCE CYOY Index']
                                +PXLast05Yzs['CPUPXCHG Index']+PXLast05Yzs['FDIUSGYO Index'])/4)
output_gap['Core (Persistence/Breadth) L'] = ((PXLast7Yzs['CPI XYOY Index']+PXLast7Yzs['PCE CYOY Index']
                                +PXLast7Yzs['CPUPXCHG Index']+PXLast7Yzs['FDIUSGYO Index'])/4)
output_gap['OECD Output Gap M'] = PXLast5Yzs['CBOPGAPP Index']
output_gap['OECD Output Gap L'] = PXLast10Yzs['CBOPGAPP Index']
output_gap['Employment EPI M'] = PXLast5Yzs['EpiA_Employment_US']
output_gap['Employment EPI L'] = PXLast10Yzs['EpiA_Employment_US']
output_gap['Unemployment level vs NAIRU M'] = ((PXLast5Yzs['USURTOT Index']+PXLast5Yzs['USUDMAER Index'])/2)
output_gap['Unemployment level vs NAIRU L'] = ((PXLast10Yzs['USURTOT Index']+PXLast10Yzs['USUDMAER Index'])/2)
output_gap['Capacity Utilisation M'] = PXLast5Yzs['CPTICHNG Index']
output_gap['Capacity Utilisation L'] = PXLast10Yzs['CPTICHNG Index']



#### Financial Conditions ###
# Capacity to borrow
fc_capcity_to_borrow = pd.DataFrame()
fc_capcity_to_borrow['Property (1y momentum) M'] = ((PXLast1Yzs['ETSLTOTL Index']+PXLast1Yzs['USHBMIDX Index']
                                +PXLast1Yzs['SPCS20Y% Index']+PXLast1Yzs['HPIMYOY% Index'])/4)
fc_capcity_to_borrow['Property (level) L'] = ((PXLast7Yzs['ETSLTOTL Index']+PXLast7Yzs['USHBMIDX Index']
                                +PXLast7Yzs['SPCS20Y% Index']+PXLast7Yzs['HPIMYOY% Index'])/4)
fc_capcity_to_borrow['Equities M'] = PXLast05Yzs['SPX Index PE']
fc_capcity_to_borrow['Equities L'] = PXLast7Yzs['SPX Index PE']
fc_capcity_to_borrow['Household Leverage M'] = PXLast5Yzs['CPNFUSHG Index']
fc_capcity_to_borrow['Household Leverage L'] = PXLast10Yzs['CPNFUSHG Index']
fc_capcity_to_borrow['Corporate Leverage M'] = PXLast5Yzs['CPNFUSNG Index']
fc_capcity_to_borrow['Corporate Leverage L'] = PXLast10Yzs['CPNFUSNG Index']
fc_capcity_to_borrow['Government Leverage M'] = PXLast5Yzs['CPNFUSOG Index']
fc_capcity_to_borrow['Government Leverage L'] = PXLast10Yzs['CPNFUSOG Index']



# Liquidity
fc_liquidity = pd.DataFrame()
fc_liquidity['Public Sector Liquidity ==> Reserves, Central Bank BS M'] = PXLast05Yzs['FARBCRED Index']
fc_liquidity['Public Sector Liquidity ==> Reserves, Central Bank BS L'] = PXLast3Yzs['FARBCRED Index']
fc_liquidity['Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) M'] = (PXLast1Yzs['ARDIMONY Index']+PXLast1Yzs['M2 Index']+PXLast1Yzs['M1 Index'])/3
fc_liquidity['Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) L'] = (PXLast1Yzs['ARDIMONY Index']+PXLast1Yzs['M2 Index']+PXLast1Yzs['M1 Index'])/3
fc_liquidity['Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc M'] = PXLast5Yzs['USCABAL Index']
fc_liquidity['Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc L'] = PXLast10Yzs['USCABAL Index']

# Funding Costs
fc_funding_cost = pd.DataFrame()
fc_funding_cost['Real Policy Rate vs real neutral rate M'] = PXLast05Yzs['FDTR Index']
fc_funding_cost['Real Policy Rate vs real neutral rate L'] = PXLast5Yzs['FDTR Index']
fc_funding_cost['Real Long End Yields M'] = PXLast05Yzs['USGG10YR Index']
fc_funding_cost['Real Long End Yield L'] = PXLast5Yzs['USGG10YR Index']
fc_funding_cost['Real Ccy vs ToT M'] = PXLast1Yzs['111.028 Index']
fc_funding_cost['Real Ccy vs ToT L'] = PXLast7Yzs['111.028 Index']
fc_funding_cost['Financial Sector Health M'] = ((PXLast1Yzs['JPM US Equity']+PXLast1Yzs['BAC US Equity']
                                         +PXLast1Yzs['C US Equity']+PXLast1Yzs['WFC US Equity']
                                        +PXLast1Yzs['USB US Equity'])/5)
fc_funding_cost['Financial Sector Health L'] = ((PXLast7Yzs['JPM US Equity']+PXLast7Yzs['BAC US Equity']
                                         +PXLast7Yzs['C US Equity']+PXLast7Yzs['WFC US Equity']
                                        +PXLast7Yzs['USB US Equity'])/5)
fc_funding_cost['Credit Spreads M'] = PXLast05Yzs['LUCROAS Index']
fc_funding_cost['Credit Spreads L'] = PXLast3Yzs['LUCROAS Index']

financial_condition = pd.concat([fc_capcity_to_borrow, fc_liquidity, fc_funding_cost], axis='columns', join='outer')





financial_condition



