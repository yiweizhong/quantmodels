from __future__ import division
import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper# -*- coding: utf-8 -*-



def get_curves(data, country):
    spot_secs = [c for c in data.columns if country in c and '_0Y_' in c]
    f1y_secs = [c for c in data.columns if country in c and  '_1Y_' in c] 
    f2y_secs = [c for c in data.columns if country in c and '_2Y_' in c]
    spot_curve = data[spot_secs]
    f1y_curve = data[f1y_secs]
    f2y_curve = data[f2y_secs]
    spot_curve = spot_curve.rename(lambda col: col.replace(country + '_0Y_',''), axis='columns')
    f1y_curve = f1y_curve.rename(lambda col: col.replace(country + '_1Y_',''), axis='columns')
    f2y_curve = f2y_curve.rename(lambda col: col.replace(country + '_2Y_',''), axis='columns')
    spot_curve = spot_curve[f1y_curve.columns]
    spot_curve.name = 'spot'
    f1y_curve.name = 'f1y'
    f2y_curve.name = 'f2y'
    return (spot_curve, f1y_curve, f2y_curve)




#data = pd.read_csv('C:/Temp/test/market_watch/staging/2019-10-08_USD_historical.csv')
#data['date'] = pd.to_datetime(data['date'])
#data = data.set_index('date').ffill()


swap = pd.read_csv('C:/Temp/test/market_watch/staging/2019-10-28_SWAP_ALL_historical.csv')
us_sec = pd.read_csv('C:/Temp/test/market_watch/staging/2019-10-28_USD_historical.csv')
swap['date'] = pd.to_datetime(swap['date'])
swap = swap.set_index('date').ffill()

us_sec['date'] = pd.to_datetime(us_sec['date'])
us_sec = us_sec.set_index('date').ffill()


(spot_curve, fwd1y_curve, fwd2y_curve) = get_curves(swap, 'USD')



i = -1
curve_set = pd.DataFrame({spot_curve.iloc[i].name:spot_curve.iloc[i], 
                          fwd1y_curve.name:fwd1y_curve.iloc[i],
                          fwd2y_curve.name:fwd2y_curve.iloc[i]})

i = -100
curve_set2 = pd.DataFrame({spot_curve.iloc[i].name:spot_curve.iloc[i], 
                          fwd1y_curve.name:fwd1y_curve.iloc[i],
                          fwd2y_curve.name:fwd2y_curve.iloc[i]})


i = -200
curve_set3 = pd.DataFrame({spot_curve.iloc[i].name:spot_curve.iloc[i], 
                          fwd1y_curve.name:fwd1y_curve.iloc[i],
                          fwd2y_curve.name:fwd2y_curve.iloc[i]})

i = -400
curve_set4 = pd.DataFrame({spot_curve.iloc[i].name:spot_curve.iloc[i], 
                          fwd1y_curve.name:fwd1y_curve.iloc[i],
                          fwd2y_curve.name:fwd2y_curve.iloc[i]})
    
    


f = plt.figure(figsize = (20, 20))
gs = gridspec.GridSpec(2, 2, width_ratios=[1,1], height_ratios=[1,1])
gss = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=gs[0])
ax0 = plt.subplot(gss[0])
gss = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=gs[1])
ax1 = plt.subplot(gss[0])
gss = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=gs[2])
ax2 = plt.subplot(gss[0])
gss = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=gs[3])
ax3 = plt.subplot(gss[0])

ax0.set_ylim(1,3)
ax1.set_ylim(1,3)
ax2.set_ylim(1,3)
ax3.set_ylim(1,3)

curve_set.plot(ax = ax0)
curve_set2.plot(ax = ax1)
curve_set3.plot(ax = ax2)
curve_set4.plot(ax = ax3)







def get_linear_betas(xs, ys):
    betas, _, _, _, _ = np.polyfit(list(xs), list(ys), deg = 1, full=True)
    (beta_x, beta_0) = betas
    #xs = df[df.columns[0]]
    #ys = df[df.columns[1]]
    #start_x = xs.min() 
    #last_x = xs.max()
    #step = (last_x - start_x)/100 
    #xs = np.arange(start_x,last_x, step)
    fitted_ys = [round(beta_0 + beta_x * x, 4) for x in list(xs)]
    residual_ys = [y - fitted_y for y, fitted_y in  zip(ys, fitted_ys)]
    residual_ys_std = np.std(residual_ys, ddof=1) 
    residual_ys_p_1std =  [fitted_y + residual_ys_std for fitted_y in  fitted_ys]
    residual_ys_m_1std =  [fitted_y - residual_ys_std for fitted_y in  fitted_ys]
    
    return betas, (xs, fitted_ys, residual_ys, residual_ys_std, residual_ys_p_1std, residual_ys_m_1std)
    

######################### curve vs rate ####################################


def plot_main(ax, xs, ys, colors, cmaps, ticklabelsize=8):
    mapable = ax.scatter(xs, ys, c=colors, cmap=cmaps, marker='o', s = 9)
        
    ax.set_ylabel(ys.name, fontsize=8)
    ax.set_xlabel(xs.name, fontsize=8)
    ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
    ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
    
    for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    
    for label,tick in zip(ax.get_xticklabels(), ax.get_xticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    return mapable


def plot_ts_endpoints(ax, xs, ys, colors, sizes, labels, markers, legend_loc=None,ncol=1, fontsize=8, lw =3):
    ds = [ax.scatter(xs[i], ys[i], c=colors[i], s=sizes[i], label=labels[i], marker=markers[i], linewidth= lw) for i in np.arange(len(xs))]
    if legend_loc is not None:
        lg =ax.legend(ds, labels, scatterpoints=1, fancybox=True, framealpha=0, loc=legend_loc, ncol=ncol, fontsize=fontsize)
        ax.add_artist(lg)
    return ds
    

def plot_fits(ax, xss, yss, label_prefixes, colors, legend_loc=None, ncol=1, fontsize=8):
    #ploting multiple fits
    pcs = []
    for (xs, ys, label_prefix, color) in zip(xss, yss, label_prefixes, colors): 
        (beta_x, beta_0), (xs, fitted_ys, residual_ys, residual_ys_std, residual_ys_p_1std, residual_ys_m_1std) = get_linear_betas(xs, ys)
        label = r'{1} = {3:.2f} + {2:.2f} * {0} '.format(xs.name, ys.name, beta_x, beta_0)
        label = label_prefix + ', ' + label if label_prefix is not None else label
        pcs = pcs + ax.plot(xs, fitted_ys, lw=1, c=color, ls = '-', marker = '.', ms = 0, label=label)
        ax.plot(xs, residual_ys_p_1std, lw=0.5, c=color, ls = ':', ms = 0)
        ax.plot(xs, residual_ys_m_1std, lw=0.5, c=color, ls = ':', ms = 0)

    if legend_loc is not None:
        lgd = ax.legend(handles=pcs, loc=legend_loc, fancybox=True, framealpha=0, ncol=ncol, fontsize=fontsize)
        ax.add_artist(lgd)
    return pcs
# data

us_sec = pd.read_csv('C:/Temp/test/market_watch/staging/2019-10-28_USD_historical.csv')
us_sec['date'] = pd.to_datetime(us_sec['date'])
us_sec = us_sec.set_index('date').ffill()


#source_data = pd.DataFrame({'US10': us_sec.USGG10YR, 'ED1ED4':(us_sec.ED1 - us_sec.ED4), 'TMRAET1Y':us_sec.US1YTMRATE})
source_data = pd.DataFrame({'US10': us_sec.USGG10YR, 'ABFS':us_sec.ABFS, 'US2S10':us_sec.USGG10YR - us_sec.USGG2YR })
source_data = pd.DataFrame({'US10': us_sec.USGG10YR, 'US2S10':us_sec.USGG10YR - us_sec.USGG2YR, 'ABFS':us_sec.ABFS })



source_data = source_data[source_data.index > '2007-01-01']

xs = source_data[source_data.columns[0]]
ys = source_data[source_data.columns[1]]

start_x = xs.loc[xs.first_valid_index()]
start_y = ys.loc[ys.first_valid_index()]
start_dt = xs.first_valid_index().strftime('%Y-%m-%d')

last_x = xs.loc[xs.last_valid_index()]
last_y = ys.loc[ys.last_valid_index()]
last_dt = xs.last_valid_index().strftime('%Y-%m-%d')


source_data_sub1 = source_data.loc[source_data[source_data.columns[0]] > 2.5]
source_data_sub2 = source_data.loc[source_data[source_data.columns[0]] < 2.5]
dt_12mths_ago = source_data.index[-1] - DateOffset(years = 0, months=12, days=0)
source_data_sub3 = source_data[source_data.index >dt_12mths_ago]

source_data_sub1_xs = source_data_sub1[source_data_sub1.columns[0]]
source_data_sub1_ys = source_data_sub1[source_data_sub1.columns[1]]

source_data_sub2_xs = source_data_sub2[source_data_sub2.columns[0]]
source_data_sub2_ys = source_data_sub2[source_data_sub2.columns[1]]

source_data_sub3_xs = source_data_sub3[source_data_sub3.columns[0]]
source_data_sub3_ys = source_data_sub3[source_data_sub3.columns[1]]
source_data_sub3_trates = source_data_sub3[source_data_sub3.columns[2]]


source_data_sub3_start_x = source_data_sub3_xs.loc[source_data_sub3_xs.first_valid_index()]
source_data_sub3_start_y = source_data_sub3_ys.loc[source_data_sub3_ys.first_valid_index()]
source_data_sub3_start_trate = source_data_sub3_trates.loc[source_data_sub3_trates.first_valid_index()]

source_data_sub3_end_x = source_data_sub3_xs.loc[source_data_sub3_xs.last_valid_index()]
source_data_sub3_end_y = source_data_sub3_ys.loc[source_data_sub3_ys.last_valid_index()]
source_data_sub3_end_trate = source_data_sub3_trates.loc[source_data_sub3_trates.last_valid_index()]



#################################charts


f2 = plt.figure(figsize = (40, 20))
gs = gridspec.GridSpec(1, 2, height_ratios=[1], width_ratios=[1,1])
ax0 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[1])

colors0 = mpl.dates.date2num(source_data.index.to_pydatetime())
colors1 = 'lightgrey'
colors_overlay1 = source_data_sub3[source_data_sub3.columns[2]]

cmap0 = plt.cm.get_cmap('binary')
cmap1 = plt.cm.get_cmap('Reds')


#plot main
main_pc0 = plot_main(ax0, xs, ys, colors0, cmap0)
main_pc1 = plot_main(ax1, xs, ys, colors1, None)

#plot end point
endpt_pc0 = plot_ts_endpoints(ax0, [start_x, last_x], [start_y, last_y], ['lightgrey','black'], [80,80], [start_dt, last_dt], ['v','^'], legend_loc=1)


#plot fit
fit_pcs0 = plot_fits(ax0, 
                    [source_data_sub1_xs,source_data_sub2_xs,source_data_sub3_xs], 
                    [source_data_sub1_ys,source_data_sub2_ys,source_data_sub3_ys],
                    ['US10 > 2.5', 'US10 < 2.5','last 12 months'], 
                    ['black','green','red'], 
                    legend_loc = 2)


fit_pcs1 = plot_fits(ax1, 
                    [source_data_sub3_xs], 
                    [source_data_sub3_ys],
                    ['last 12 months'], 
                    ['red'], 
                    legend_loc = 2)










#plot terminal overlay
overlay_pc1 = plot_main(ax1, source_data_sub3_xs, source_data_sub3_ys, colors_overlay1, cmap1)
endpt_pc1 = plot_ts_endpoints(ax1, 
                              [source_data_sub3_start_x, source_data_sub3_end_x], 
                              [source_data_sub3_start_y, source_data_sub3_end_y], 
                              ['grey', 'grey'], 
                              [80,80], ['1y terminal rate: {0:.2f}'.format(source_data_sub3_start_trate), '1y terminal rate: {0:.2f}'.format(source_data_sub3_end_trate)], 
                              ['v','^'], 
                              legend_loc=1)



#plt.colorbar(overlay_pc1)



colors =  mpl.dates.date2num(source_data.index.to_pydatetime())
cmap = plt.cm.get_cmap('binary')
x = source_data[source_data.columns[0]]
y = source_data[source_data.columns[1]]
mapable = ax0.scatter(x, y, c=colors, cmap=cmap, marker='o', s = 9)

start_x = x.loc[x.first_valid_index()]
start_y = y.loc[y.first_valid_index()]
start_dt = x.first_valid_index().strftime('%Y-%m-%d')

last_x = x.loc[x.last_valid_index()]
last_y = y.loc[y.last_valid_index()]
last_dt = x.last_valid_index().strftime('%Y-%m-%d')


ds = [ax0.scatter([start_x, last_x][i], [start_y, last_y][i], c=['lightgrey','black'][i], s=[80,80], label=[start_dt, last_dt], marker= ['s','s'][i]) for i in np.arange(len([start_x, last_x]))]

#ax0.scatter([start_x], [start_y], c='Red', s=90, label='start')
#ax0.scatter([last_x], [last_y], c='Blue', s=90, label='last')
legend1 =ax0.legend(ds, [start_dt, last_dt], scatterpoints=1, fancybox=True, framealpha=0, loc=1, ncol=1, fontsize=8)

ax0.add_artist(legend1)

ax0.set_ylabel(y.name, fontsize=8)
ax0.set_xlabel(x.name, fontsize=8)
ax0.xaxis.grid(True, linestyle='-', linewidth='0.2')
ax0.yaxis.grid(True, linestyle='-', linewidth='0.2')

for label,tick in zip(ax0.get_yticklabels(), ax0.get_yticks()):
    label.set_fontsize(8)
    if tick < 0:
        label.set_color("red")
    else:
        label.set_color("black")


for label,tick in zip(ax0.get_xticklabels(), ax0.get_xticks()):
    label.set_fontsize(8)
    if tick < 0:
        label.set_color("red")
    else:
        label.set_color("black")


#########################################################################

source_data_sub1 = source_data.loc[source_data[source_data.columns[0]] > 2.5]
source_data_sub2 = source_data.loc[source_data[source_data.columns[0]] < 2.5]
dt_12mths_ago = source_data.index[-1] - DateOffset(years = 0, months=12, days=0)
source_data_sub3 = source_data[source_data.index >dt_12mths_ago]


#overlay terminal rate
#mapable_terminal_overlay = ax0.scatter(source_data_sub3[source_data_sub3.columns[0]], source_data_sub3[source_data_sub3.columns[1]], c=source_data_sub3[source_data_sub3.columns[2]], cmap=plt.cm.get_cmap('Reds'), marker='X', s = 9)



(beta_x, beta_0), (xs, fitted_ys, residual_ys, residual_ys, residual_ys_p_1std, residual_ys_m_1std) = get_linear_betas(source_data_sub1)
label = r'{0}>2.5, {1} = {3:.2f} + {2:.2f} * {0} '.format(source_data.columns[0], source_data.columns[1], beta_x, beta_0)
 
ls1 = ax0.plot(xs, fitted_ys, lw=1, c='black', ls = '-', marker = '.', ms = 0, label=label)
ax0.plot(xs, residual_ys_p_1std, lw=0.5, c='black', ls = ':', ms = 0)
ax0.plot(xs, residual_ys_m_1std, lw=0.5, c='black', ls = ':', ms = 0)


(beta_x, beta_0), (xs, fitted_ys, residual_ys, residual_ys, residual_ys_p_1std, residual_ys_m_1std) = get_linear_betas(source_data_sub2)
label = r'{0}<2.5, {1} = {3:.2f} + {2:.2f} * {0} '.format(source_data.columns[0], source_data.columns[1], beta_x, beta_0)

ls2 = ax0.plot(xs, fitted_ys, lw=1, c='green', ls = '-', marker = '.', ms = 0, label=label)
ax0.plot(xs, residual_ys_p_1std, lw=0.5, c='green', ls = ':', ms = 0)
ax0.plot(xs, residual_ys_m_1std, lw=0.5, c='green', ls = ':', ms = 0)


(beta_x, beta_0), (xs, fitted_ys, residual_ys, residual_ys, residual_ys_p_1std, residual_ys_m_1std) = get_linear_betas(source_data_sub3)
label = r'last 12m, {1} = {3:.2f} + {2:.2f} * {0} '.format(source_data.columns[0], source_data.columns[1], beta_x, beta_0)

ls3 = ax0.plot(xs, fitted_ys, lw=1, c='red', ls = '-', marker = '.', ms = 0, label=label)
ax0.plot(xs, residual_ys_p_1std, lw=0.5, c='red', ls = ':', ms = 0)
ax0.plot(xs, residual_ys_m_1std, lw=0.5, c='red', ls = ':', ms = 0)

lss = ls1 + ls2 + ls3
legend2 = plt.legend(handles=lss, loc=2, fancybox=True, framealpha=0, ncol=1, fontsize=8)


ax0.add_artist(legend2)

########################################################################



ussp = us_sec[['USSP2','USSP5','USSP10']]
dt_24mths_ago = ussp.index[-1] - DateOffset(years = 0, months=24, days=0)
ussp24m = ussp[ussp.index >dt_24mths_ago]

dt_60mths_ago = ussp.index[-1] - DateOffset(years = 0, months=60, days=0)
ussp60m = ussp[ussp.index >dt_60mths_ago]



ussp24m_pca = pca.PCA(ussp24m, rates_pca = True)
ussp24m_pca.plot_loadings()



ussp60m_pca = pca.PCA(ussp60m, rates_pca = True)
ussp60m_pca.plot_loadings()





usr = us_sec[['USGG2YR','USGG5YR','USGG10YR']]
dt_24mths_ago = ussp.index[-1] - DateOffset(years = 0, months=24, days=0)
usgg24m = usr[usr.index >dt_24mths_ago]

dt_60mths_ago = ussp.index[-1] - DateOffset(years = 0, months=60, days=0)
usgg60m = usr[usr.index >dt_60mths_ago]



ussp24m_pca = pca.PCA(ussp24m, rates_pca = True)
ussp24m_pca.plot_loadings()



ussp60m_pca = pca.PCA(ussp60m, rates_pca = True)
ussp60m_pca.plot_loadings()


usgg60m_pca = pca.PCA(usgg60m, rates_pca = True)
usgg60m_pca.plot_loadings()


proxy = pd.DataFrame({'SP': ussp60m_pca.pcs['PC1'], 'CURVE':usgg60m_pca.pcs['PC2']})


fig = plt.figure()
ax1 = fig.add_subplot(111)
proxy.SP.plot(ax=ax1, color='blue')
ax1.set_ylabel('Swap spread')
ax2 = ax1.twinx()
proxy.CURVE.plot(ax=ax2, color='red')
ax2.set_ylabel('curve')
ax1.spines['left'].set_color('red')
ax1.yaxis.label.set_color('red')
ax2.yaxis.label.set_color('blue')



ax2.plot(x, y2, 'r-')
ax2.set_ylabel('y2', color='r')
for tl in ax2.get_yticklabels():
    tl.set_color('r')








