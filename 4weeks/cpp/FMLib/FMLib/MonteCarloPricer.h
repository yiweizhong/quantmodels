#pragma once
#include "stdafx.h"
//#include "CallOption.h"
//#include "PathIndependentOption.h"
#include "ContinuousTimeOption.h"
#include "BlackScholesModel.h"

class MonteCarloPricer {
public:
	/*Constrcutor*/
	MonteCarloPricer();
	/*Number of scenarios*/
	int nScenarios;
	/*Price a call option*/
	double price(const ContinuousTimeOption & continuousTimeOption,
		const BlackScholesModel & model, 
		int nSimulationSteps = 250);

};

void testMonteCarloPricer();


