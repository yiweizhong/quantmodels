#pragma once
#include "stdafx.h"
#include "BlackScholesModel.h"

using std::vector;

class ContinuousTimeOption {
public:
	virtual ~ContinuousTimeOption() {}
	virtual double getMaturity() const = 0;
	virtual double payoff(vector<double>& stockPrices) const = 0;
	virtual bool isPathDependent() const = 0;
	//virtual double price(const BlackScholesModel& bsm) const = 0;

};
