#include "Portfolio.h"
#include "CallOption.h"
#include "PutOption.h"
#include <algorithm>

using std::shared_ptr;
using std::pair;
using std::make_pair;
using std::make_shared;
using std::for_each;

typedef pair<shared_ptr<Priceable>, double> position;

/*
 *  By using an abstract interface class with a factory constructor
 *  and only having the implementation in the C++ file we increase information
 *  hiding. Nobody knows about the PortfolioImpl class outside of the C++ file,
 *  so we can change it without any impact on anything else.
 */

/*PortfolioImpl interface*/
class PorfolioImpl : public Portfolio {
public:
	/*Returns the number of items in the porfolio*/
	int size() const;
	/*  Add a new security to the portfolio,
		returns the index at which it was added */
	int add(shared_ptr<Priceable> security, double quantity);
	/*  Update the quantity at a given index */
	void setQuantity(int index, double quantity);
	/*  Compute the current price */
	double price(const BlackScholesModel& bsm) const;	
	vector<position> positions;
};

int PorfolioImpl::size() const
{
	return this->positions.size();
}

int PorfolioImpl::add(shared_ptr<Priceable> security, double quantity)
{
	this->positions.push_back(make_pair(security, quantity));
	return this->size();
}

void PorfolioImpl::setQuantity(int index, double quantity)
{
	this->positions[index].second = quantity;
}

double PorfolioImpl::price(const BlackScholesModel & bsm) const
{
	auto price = 0.;
	for (auto const& p : this->positions) {
		price = price + p.first->price(bsm) * p.second;
	}

	return price;
}

std::shared_ptr<Portfolio> Portfolio::newInstance()
{
	return  make_shared<PorfolioImpl>();
}

/////////////////////////////
//  Tests
/////////////////////////////


static void testCreatePortfolio() {
	shared_ptr<Portfolio> portfolio = Portfolio::newInstance();


	//BSM
	auto bsm = BlackScholesModel{};
	bsm.volatility = 0.1;
	bsm.stockPrice = 100;


	//position 1
	shared_ptr<CallOption> c = make_shared<CallOption>();
	c->strike = 100.;
	c->maturity = 1.0;
	
	//add poition to the portfolio
	portfolio->add(c, 100);

	//price
	double unitPrice = c->price(bsm);
	double portfolioPrice = portfolio->price(bsm);

	ASSERT_APPROX_EQUAL(100 * unitPrice, portfolioPrice, 0.0001);

}

static void testPutCallParity() {
	//buy a call and sell a put will have the value
	// max[s - k, 0] - max[k - s, 0] = S - K at expiry
	// therefore the a portfolio of 1 long call and 1 short put's value
	//today will be (S-K) * df

	shared_ptr<Portfolio> portfolio = Portfolio::newInstance();

	//BSM
	auto bsm = BlackScholesModel{};
	bsm.volatility = 0.1;
	bsm.stockPrice = 100;
	bsm.riskFreeRate = 0.;

	//position 1
	shared_ptr<CallOption> c = make_shared<CallOption>();
	c->strike = 110.;
	c->maturity = 1.0;

	//position 1
	shared_ptr<PutOption> p = make_shared<PutOption>();
	p->strike = 110.;
	p->maturity = 1.0;

	portfolio->add(c, 1);
	portfolio->add(p, -1);

	//price
	double portfolioPrice = portfolio->price(bsm);
	double expected = bsm.stockPrice - c->strike;
	


	ASSERT_APPROX_EQUAL(expected, portfolioPrice, 0.0001);


}

void testPortfolio()
{
	TEST(testCreatePortfolio);
	TEST(testPutCallParity);
}
