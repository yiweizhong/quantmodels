#pragma once
#include "stdafx.h"

using std::vector;


/**
 *  Find the sum of the elements in an array
 */
double sum(const vector<double>& v);


/*  Compute the mean of a vector */
double mean(const vector<double>& v);

/*  Find the minimum of a vector */
double min(const vector<double>& v);

/*  Find the maximum of a vector */
double max(const vector<double>& v);

/*  Create a linearly spaced vector */
vector<double> linspace(double from, double to, int numPoints);

/*  Compute the standard deviation of a vector */
double standardDeviation(const vector<double>& v, bool population);

/*
Computes the cumulative dirstribution function of the norm distribution
*/
double normcdf(double x);
/*
Computes the inverse of the normcdf
*/
double norminv(double x);

/*
generate n normally distributed normal random variables
*/
vector<double> randNorm(int n, bool staticSeed = false);
//vector<double> randn(int n);

/*test harness interface*/
void testMatlib();

namespace base
{
	class NotImplementedException : public std::exception
	{
		public:
			NotImplementedException();
		
	};
};
