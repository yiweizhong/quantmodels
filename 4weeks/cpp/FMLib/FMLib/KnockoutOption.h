#pragma once
#include "stdafx.h"
#include "Priceable.h"
#include "ContinuousTimeOption.h"
#include "BlackScholesModel.h"

class KnockoutOption : public ContinuousTimeOption, public Priceable {

public:
	enum barrierType { upAndOut, downAndOut };
	double strike;
	double maturity;
	double barrier;
	barrierType bType;
	KnockoutOption(double strike, double barrier, double maturity, barrierType bType);
	double getMaturity() const;
	double payoff(vector<double>& stockPrices) const;
	bool isPathDependent() const;
	double price(const BlackScholesModel& bsm) const;


};

void testKnockoutOption();