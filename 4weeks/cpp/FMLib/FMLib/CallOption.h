#pragma once
#include "stdafx.h"
#include "BlackScholesModel.h"
//#include "PathIndependentOption.h"
#include "ContinuousTimeOption.h"
#include "Priceable.h"

/*
	The default inheritance type of a class in C++ is private, so any public and protected members 
	from the base class are limited to private. 
	Therefore the public keyword is needed on the interface. 
	Struct inheritance on the other hand is public by default.
*/
class CallOption: public ContinuousTimeOption, public Priceable {
public:
	CallOption();
	
	double strike;
	double maturity;

	double payoff(double stockAtMaturity) const;
	double payoff(vector<double>& stockPrices) const;
	double price(const BlackScholesModel& bsm) const;
	double getMaturity() const;
	bool isPathDependent() const;

};

void testCallOption();
