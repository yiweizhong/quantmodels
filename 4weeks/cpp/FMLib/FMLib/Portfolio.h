#pragma once
#include "stdafx.h"
#include "Priceable.h"

class Portfolio : public Priceable {

public:
	/*Virtual destructor*/
	virtual ~Portfolio() {};

	/*Returns the number of items in the porfolio*/
	virtual int size() const = 0;

	/*  Add a new security to the portfolio, returns the index at which it was added */
	virtual int add(std::shared_ptr<Priceable> security, double quantity) = 0;

	/*set the quantity for a security*/
	virtual void setQuantity(int index, double quantity) = 0;

	/*price the current portfolio*/
	virtual double price(const BlackScholesModel& model) const = 0;

	/*factory methods*/
	static std::shared_ptr<Portfolio> newInstance();

};

void testPortfolio();
