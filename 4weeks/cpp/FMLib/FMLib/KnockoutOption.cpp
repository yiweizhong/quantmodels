#include <algorithm>
#include <stdexcept>
#include "CallOption.h"
#include "Priceable.h"
#include "KnockoutOption.h"
#include "MonteCarloPricer.h"


using std::find_if;
using std::invalid_argument;

KnockoutOption::KnockoutOption(double strike, double barrier, double maturity, KnockoutOption::barrierType bType): 
	strike(strike), barrier(barrier), maturity(maturity), bType(bType)
{

}

double KnockoutOption::getMaturity() const
{
	return maturity;
}


double KnockoutOption::payoff(vector<double>& stockPrices) const
{
	switch (this->bType) {
		case KnockoutOption::upAndOut:
		{
			auto result = find_if(stockPrices.begin(), stockPrices.end(),
				[this](auto x) {return (x > this->barrier);});

			if (result != stockPrices.end()) {
				return 0.0; //hit the barrier
			}
			else if (stockPrices.back() > this->strike) {
				return stockPrices.back() - this->strike;
			}
			else {
				return 0.0;
			}
		}
		case KnockoutOption::downAndOut:
		{
			auto result = find_if(stockPrices.begin(), stockPrices.end(),
				[this](auto x) {return (x < this->barrier); });

			if (result != stockPrices.end()) {
				return 0.0; //hit the barrier
			}
			else if (stockPrices.back() < this->strike) {
				return this->strike - stockPrices.back();
			}
			else {
				return 0.0;
			}
		}
		default:
			throw invalid_argument("Unkknow barrier type");
	}

}

bool KnockoutOption::isPathDependent() const
{
	return true;
}

double KnockoutOption::price(const BlackScholesModel & bsm) const
{
	MonteCarloPricer pricer;
	return pricer.price(*this, bsm);

}


/////////////////////////////////////
//
//   TESTS
//
/////////////////////////////////////


void testDownAndOutBarrierOption() {
	auto strike = 70.0;
	auto barrier = 50.0;
	auto maturity = 1.0;

	KnockoutOption o(strike, barrier, maturity, KnockoutOption::downAndOut);
	vector<double> prices;
	prices.push_back(120.0);
	prices.push_back(80.0);
	ASSERT_APPROX_EQUAL(o.payoff(prices), 0.0, 0.001); //out of money
	prices[1] = 60.0;
	ASSERT_APPROX_EQUAL(o.payoff(prices), 10.0, 0.001); // in the money
	prices[1] = 40.0;
	ASSERT_APPROX_EQUAL(o.payoff(prices), 0.0, 0.001); //hit the barrier
}

void testUpAndOutBarrierOption() {
	auto strike = 70.0;
	auto barrier = 90.0;
	auto maturity = 1.0;

	KnockoutOption o(strike, barrier, maturity, KnockoutOption::upAndOut);
	vector<double> prices;
	prices.push_back(120.0);
	prices.push_back(80.0);
	ASSERT_APPROX_EQUAL(o.payoff(prices), 0.0, 0.001); //out of money
	prices[0] = 70.0;
	ASSERT_APPROX_EQUAL(o.payoff(prices), 10.0, 0.001); // in the money
	prices[0] = 40.0;
	prices[1] = 60.0;
	ASSERT_APPROX_EQUAL(o.payoff(prices), 0.0, 0.001); //hit the barrier
}


void testPriceKnockoutOption() {
	auto stockPrice = 100.0;
	auto volatility = 0.1;
	auto riskFreeRate = 0.05;
	auto strike = 100.0;
	auto barrier = 1000.0;
	auto maturity = 1.0;

	BlackScholesModel bsm;
	bsm.volatility = volatility;
	bsm.stockPrice = stockPrice;

	CallOption callOption;
	callOption.strike = strike;
	callOption.maturity = maturity;

	KnockoutOption upAndOutOption(strike, barrier, maturity, KnockoutOption::upAndOut);

	Priceable& o1 = callOption;
	Priceable& o2 = upAndOutOption;

	double p1 = o1.price(bsm);
	double p2 = o2.price(bsm);

	ASSERT_APPROX_EQUAL(p1, p2, 0.2);

}

void testKnockoutOption()
{
	TEST(testDownAndOutBarrierOption);
	TEST(testUpAndOutBarrierOption);
	TEST(testPriceKnockoutOption);
}

