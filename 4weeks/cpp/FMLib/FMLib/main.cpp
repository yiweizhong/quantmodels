#include <iostream>
#include "matlib.h"
#include "exercises.h"
#include "BlackScholesModel.h"
#include "CallOption.h"
#include "PutOption.h"
#include "KnockoutOption.h"
#include "MonteCarloPricer.h"
#include "Portfolio.h"


using namespace std;

int main()
{
	testPortfolio();
	//testKnockoutOption();
	//testPutOption();
	//testMonteCarloPricer();
	//testCallOption();
	//testBlackScholesModel();
	//testMatlib();
	//testChapter7Excercise();

	return 0;
}
