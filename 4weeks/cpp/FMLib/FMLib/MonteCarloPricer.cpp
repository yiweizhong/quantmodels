#include "MonteCarloPricer.h"
#include "CallOption.h"


//default constructor has 10000 simulation
MonteCarloPricer::MonteCarloPricer():nScenarios(10000)
{

}

double MonteCarloPricer::price(const ContinuousTimeOption & continuousTimeOption,
	const BlackScholesModel & model, int nSimulationSteps)
{
	double total = 0.0;
	for(int i = 0; i < nScenarios; i++) {
		vector <double > pricePath = model.generateRiskNeutralPricePath(continuousTimeOption.getMaturity(), nSimulationSteps);
		//double stockPrice = path.back();
		double payoff = continuousTimeOption.payoff(pricePath);
		total += payoff;
	}
	double mean = total / nScenarios;
	double r = model.riskFreeRate;
	double T = continuousTimeOption.getMaturity() - model.date;
	double price = exp(-r * T) * mean; //discounted average of the terminal payoff values based on the simulated stock prices

	return price;
}

void testMonteCarloPricer()
{
	CallOption c;
	c.strike = 110;
	c.maturity = 2;

	BlackScholesModel m;
	m.volatility = 0.1;
	m.riskFreeRate = 0.05;
	m.stockPrice = 100.0;
	m.drift = 0.1;
	m.date = 1;
	
	MonteCarloPricer pricer;
	double price = pricer.price(c, m);
	double expected = c.price(m);

	ASSERT_APPROX_EQUAL(price, expected, 0.1);
}
