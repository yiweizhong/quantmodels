#include "PutOption.h"
#include "matlib.h"


PutOption::PutOption(): strike(0.0), maturity(0.0)
{
}

double PutOption::payoff(double stockAtMaturity) const{
	if (stockAtMaturity >= this->strike){
		return 0.0;
	}
	else {
		return strike - stockAtMaturity;
	}
}

double PutOption::payoff(vector<double>& stockPrices) const{
	//there is copy via value happening here
	return this->payoff(stockPrices.back());
}

double PutOption::price(const BlackScholesModel & bsm) const{
	double S = bsm.stockPrice;
	double K = strike;
	double sigma = bsm.volatility;
	double r = bsm.riskFreeRate;
	double T = maturity - bsm.date;
	double numerator = log(S / K) + (r + sigma * sigma*0.5)*T;
	double denominator = sigma * sqrt(T);	
	double d1 = numerator / denominator;
	double d2 = d1 - denominator;
	return - S * normcdf(-d1) + exp(-r * T)*K*normcdf(-d2);
}

double PutOption::getMaturity() const
{
	return this->maturity;
}

bool PutOption::isPathDependent() const
{
	return false;
}



//////////////////////////
//
//  Test the put option class
//  
//
//////////////////////////

static void testPutOptionPrice() {
	PutOption putOption;
	putOption.strike = 105.0;
	putOption.maturity = 2.0;

	BlackScholesModel bsm;
	bsm.date = 1.0;
	bsm.volatility = 0.1;
	bsm.riskFreeRate = 0.05;
	bsm.stockPrice = 100.0;

	double price = putOption.price(bsm);
	ASSERT_APPROX_EQUAL(price, 3.925, 0.01);
}


void testPutOption()
{
	TEST(testPutOptionPrice);
}
