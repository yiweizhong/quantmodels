#include "matlib.h"
#include "testing.h"
#include <random>
#include <cstdlib>
#include <exception>
#include <numeric>


using std::vector;
using std::accumulate;

namespace base
{

	NotImplementedException::NotImplementedException() : std::exception{ "Function not yet implemented." } {}

}

const double PI = 3.14159265359;
const double SQRT_2PI = sqrt(2 * PI);


/**
 *  Find the sum of the elements in an array
 */
double sum(const vector<double>& v) {
	double total = 0.0;
	int n = v.size();
	for (int i = 0; i < n; i++) {
		total += v[i];
	}
	return total;
}


/*  Compute the mean of a vector */
double mean(const vector<double>& v) {
	int n = v.size();
	ASSERT(n > 0);
	return sum(v) / n;
}

/*  Compute the standard deviation of a vector */
double standardDeviation(const vector<double>& v, bool population) {
	int n = v.size();
	double total = 0.0;
	double totalSq = 0.0;
	for (int i = 0; i < n; i++) {
		total += v[i];
		totalSq += v[i] * v[i];
	}
	if (population) {
		ASSERT(n > 0);
		return sqrt((totalSq - total * total / n) / n);
	}
	else {
		ASSERT(n > 1);
		return sqrt((totalSq - total * total / n) / (n - 1));
	}
}

/*  Find the minimum of a vector */
double min(const vector<double>& v) {
	int n = v.size();
	ASSERT(n > 0);
	double min = v[0];
	for (int i = 1; i < n; i++) {
		if (v[i] < min) {
			min = v[i];
		}
	}
	return min;
}

/*  Find the maximum of a vector */
double max(const vector<double>& v) {
	int n = v.size();
	ASSERT(n > 0);
	double max = v[0];
	for (int i = 1; i < n; i++) {
		if (v[i] > max) {
			max = v[i];
		}
	}
	return max;
}


/*  Create a linearly spaced vector */
vector<double> linspace(double from, double to, int numPoints) {
	ASSERT(numPoints >= 2);
	vector<double> ret(numPoints, 0.0);
	double step = (to - from) / (numPoints - 1);
	double current = from;
	for (int i = 0; i < numPoints; i++) {
		ret[i] = current;
		current += step;
	}
	return ret;
}

double normcdf(double x) {

	double a = 0.319381530;
	double b = -0.356563782;
	double c = 1.781477937;
	double d = -1.821255978;
	double e = 1.330274429;

	auto k = [](double x) { return 1 / (1 + 0.2316419 * x); };
	auto N = [a, b, c, d, e, k](double x) { return 1 - 1 / SQRT_2PI * exp(-x * x / 2) * k(x)*(a + k(x)*(b + k(x)*(c + k(x)*(d + e * k(x))))); };

	return (x >= 0) ? N(x) : 1 - N(-x);
}


static inline double hornerFunction(
	double x,
	double a0,
	double a1) {
	return a0 + x * a1;
}

static inline double hornerFunction(double x, double a0, double a1, double a2) {
	return a0 + x * hornerFunction(x, a1, a2);
}

static inline double hornerFunction(double x, double a0, double a1, double a2, double a3) {
	return a0 + x * hornerFunction(x, a1, a2, a3);
}

static inline double hornerFunction(double x, double a0, double a1, double a2, double a3, double a4) {
	return a0 + x * hornerFunction(x, a1, a2, a3, a4);
}

static inline double hornerFunction(double x, double a0, double a1, double a2, double a3, double a4,
	double a5) {
	return a0 + x * hornerFunction(x, a1, a2, a3, a4, a5);
}

static inline double hornerFunction(double x, double a0, double a1, double a2, double a3, double a4,
	double a5, double a6) {
	return a0 + x * hornerFunction(x, a1, a2, a3, a4, a5, a6);
}

static inline double hornerFunction(double x, double a0, double a1, double a2, double a3, double a4,
	double a5, double a6, double a7) {
	return a0 + x * hornerFunction(x, a1, a2, a3, a4, a5, a6, a7);
}

static inline double hornerFunction(double x, double a0, double a1, double a2, double a3, double a4,
	double a5, double a6, double a7, double a8) {
	return a0 + x * hornerFunction(x, a1, a2, a3, a4, a5, a6, a7, a8);
}

static const double a0 = 2.50662823884;
static const double a1 = -18.61500062529;
static const double a2 = 41.39119773534;
static const double a3 = -25.44106049637;
static const double b1 = -8.47351093090;
static const double b2 = 23.08336743743;
static const double b3 = -21.06224101826;
static const double b4 = 3.13082909833;
static const double c0 = 0.3374754822726147;
static const double c1 = 0.9761690190917186;
static const double c2 = 0.1607979714918209;
static const double c3 = 0.0276438810333863;
static const double c4 = 0.0038405729373609;
static const double c5 = 0.0003951896511919;
static const double c6 = 0.0000321767881768;
static const double c7 = 0.0000002888167364;
static const double c8 = 0.0000003960315187;

double norminv(double x) {
	// We use Moro's algorithm
	double y = x - 0.5;
	if (y<0.42 && y>-0.42) {
		double r = y * y;
		return y * hornerFunction(r, a0, a1, a2, a3) / hornerFunction(r, 1.0, b1, b2, b3, b4);
	}
	else {
		double r;
		if (y < 0.0) {
			r = x;
		}
		else {
			r = 1.0 - x;
		}
		double s = log(-log(r));
		double t = hornerFunction(s, c0, c1, c2, c3, c4, c5, c6, c7, c8);
		if (x > 0.5) {
			return t;
		}
		else {
			return -t;
		}
	}
}

static std::mt19937 mersenneTwister;

/*  Create uniformly distributed random numbers*/

vector<double> randUniform(int n, bool staticSeed = false) {

	vector<double> ranNum{};

	
	//std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
	
	if (staticSeed) {
		mersenneTwister.seed(std::mt19937::default_seed);
	}
	else {
		std::random_device rd;  //Will be used to obtain a seed for the random number engine
		mersenneTwister.seed(rd());
	}

	std::uniform_real_distribution<> uniformDitribution0_1(0.0, 1);

	for (int i = 0; i < n; i++) {
		auto r = uniformDitribution0_1(mersenneTwister);
		ranNum.push_back(r);
	}
	return ranNum;
}

/*  Create normally distributed random numbers*/

vector<double> randNorm(int n, bool staticSeed) {
	vector<double> ranNum{};
	auto ranUniformedNum = randUniform(n, staticSeed = staticSeed);

	for(const auto r: ranUniformedNum) {
		ranNum.push_back(norminv(r));
	}
	return ranNum;
}

///*  MersenneTwister random number generator */
//static std::mt19937 mersenneTwister;
//
//
///*  Create uniformly distributed random numbers using
//	the Mersenne Twister algorithm. See the code above for the answer
//	to the homework excercise which should familiarize you with the C API*/
//vector<double> randuniform(int n) {
//	
//	vector<double> ret(n, 0.0);
//	for (int i = 0; i < n; i++) {
//		ret[i] = (mersenneTwister() + 0.5) /
//			(mersenneTwister.max() + 1.0);
//	}
//	return ret;
//}
//
///*  Create normally distributed random numbers */
//vector<double> randn(int n) {
//	vector<double> v = randuniform(n);
//	for (int i = 0; i < n; i++) {
//		v[i] = norminv(v[i]);
//	}
//	return v;
//}
//






vector<double> mva(int p, vector<double> data) {
	vector<double> _mva{};
	double _mvSum = 0;
	vector<double> _mvSumData{}; 

	for (int i = 0; i < (int) data.size(); ++i) {
		_mvSum += data[i];
		_mvSumData.push_back(data[i]);
		if (i >= p) {
			_mva.push_back(_mvSum / (double)p);
			_mvSum -= data[i - p];
			_mvSumData.clear();
		}
	}
	return _mva;
}

/*
Better MVA. iterator cant be ++ over its boundary!
*/
//vector<double> mva2(int p, const vector<double> & data) {
//	vector<double> _mva{};
//	auto _left = data.cbegin();
//	auto _right = data.cbegin() + p;
//
//	for (int i = 0; i < data.size() - (p-1); i++) {
//		if (i != 0) {
//			_left++;
//			_right++;
//		}
//		//auto _subset =  vector(_left, _right);
//		_mva.push_back(accumulate(_left, _right, 0)/(double)p);		
//	}
//	return _mva;
//}


///////////////////////////////////////////////;
//
//   TESTS
//
///////////////////////////////////////////////



static void testMva() {

	auto result = mva(5, vector<double> {10, 11, 22, 12, 13, 23, 12, 32, 12, 3, 2, 22, 32});

	assert(1 == 1);
}

static void testRandUniform() {

	auto ranNums = randUniform(1000);
	for (const auto & r: ranNums) {
		ASSERT(r >= 0);
		ASSERT(r <= 1);
	}
	assert(ranNums.size() == 1000);

}

static void testRandNormal() {

	auto ranNums = randUniform(1000);
	assert(ranNums.size() == 1000);

}


static void testNormcdf() {
	//test tests
	ASSERT(3 > 2);
	// test bounds
	ASSERT(normcdf(0.3) > 0);
	ASSERT(normcdf(0.3) < 1);
	// test extreme values
	ASSERT_APPROX_EQUAL(normcdf(-1e10), 0, 0.001);
	ASSERT_APPROX_EQUAL(normcdf(1e10), 1.0, 0.001);
	// test increasing
	ASSERT(normcdf(0.3) < normcdf(0.5));
	// test symmetry
	ASSERT_APPROX_EQUAL(normcdf(0.3),
		1 - normcdf(-0.3), 0.0001);
	ASSERT_APPROX_EQUAL(normcdf(0.0), 0.5, 0.0001);
	// test inverse
	ASSERT_APPROX_EQUAL(normcdf(norminv(0.3)),
		0.3, 0.0001);
	// test well known value
	ASSERT_APPROX_EQUAL(normcdf(1.96), 0.975, 0.001);
}

static void testNormInv() {
	ASSERT_APPROX_EQUAL(norminv(0.975), 1.96, 0.01);
}

void testMatlib() {



	setDebugEnabled(true);
	TEST(testMva);
	setDebugEnabled(false);

	TEST(testRandUniform);
	TEST(testRandNormal);	
	TEST(testNormcdf);
	TEST(testNormInv);

	
}