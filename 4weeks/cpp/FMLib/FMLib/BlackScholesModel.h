#pragma once
#include "stdafx.h"


using std::vector;

//Test handler 
void testBlackScholesModel();


class BlackScholesModel {
public:
	
	double drift;
	double stockPrice;
	double volatility;
	double riskFreeRate;
	double date;

	BlackScholesModel();
	BlackScholesModel(double riskFreeRate,  double volatility, double stockPrice, double date);

	vector<double> generatePricePath(
		double toDate,
		int nSteps,
		bool staticSeed =false) const;

	vector<double> generateRiskNeutralPricePath(
		double toDate,
		int nSteps,
		bool staticSeed =false) const;
private:
	vector<double> generatePricePath(
		double toDate,
		int nSteps,
		double drift,
		bool staticSeed = false) const;


};
