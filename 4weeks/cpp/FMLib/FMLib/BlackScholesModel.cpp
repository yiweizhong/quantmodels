#include <cmath>
#include "BlackScholesModel.h"
#include "matlib.h"
#include "LineChart.h"


using std::vector;


BlackScholesModel::BlackScholesModel(): drift(0.0),stockPrice(0.0),volatility(0.0),riskFreeRate(0.0),date(0.0) {};
BlackScholesModel::BlackScholesModel(double riskFreeRate, double volatility, double stockPrice, double date) :
	stockPrice(stockPrice), volatility(volatility), riskFreeRate(riskFreeRate), date(date) {};

vector<double> BlackScholesModel::generatePricePath(double toDate, int nSteps, bool staticSeed) const
{
	return this->generatePricePath(toDate, nSteps, this->drift, staticSeed = staticSeed);
}

vector<double> BlackScholesModel::generateRiskNeutralPricePath(double toDate, int nSteps, bool staticSeed) const
{
	double _riskNeutralDrift = this->riskFreeRate;
	return this->generatePricePath(toDate, nSteps, _riskNeutralDrift);
}

vector<double> BlackScholesModel::generatePricePath(double toDate, int nSteps, double drift, bool staticSeed) const
{
	vector<double> _pricePath(nSteps, nan(""));
	//vector<double> epsilon = randn(nSteps);
	vector<double> epsilon = randNorm(nSteps, staticSeed = staticSeed);
	
	double dt = (toDate - this->date) / (double) nSteps;
	double a = (drift - this->volatility * this->volatility * 0.5) * dt;
	double b = this->volatility * sqrt(dt);
	double lp = log(this->stockPrice);

	for (int i = 0; i < nSteps; i++) {
		double dlp = a + b * epsilon[i];
		double lpNext = lp + dlp;
		_pricePath[i] = exp(lpNext);
		lp = lpNext;
	}
	return _pricePath;
}






///////////////////////////////////////////////;
//
//   TESTS
//
///////////////////////////////////////////////

static void testGeneratePricePath() {

	auto riskFreeRate = 0.05;
	auto volatility = 0.1;
	auto stockPrice = 100.0;
	auto date = 2.0;
	auto nSteps = 1000;
	auto maturity = 4.0;

	BlackScholesModel bsm { riskFreeRate, volatility, stockPrice, date};

	vector<double> pricePath = bsm.generateRiskNeutralPricePath(maturity, nSteps);

	double dt = (maturity - bsm.date) / (double) nSteps;
	vector<double> times = linspace(dt, maturity, nSteps);

	LineChart lineChart;
	lineChart.setTitle("A Stock Price Path");
	lineChart.setSeries(times, pricePath);
	lineChart.writeAsHTML("examplePricePath.html ");
}

static void testRiskNeutralPricePath() {
	/*If risk neutral price pathes are correct the discounted value of the mean path
	terminal values should be equal to the initial price*/

	auto riskFreeRate = 0.05;
	auto volatility = 0.1;
	auto stockPrice = 100.0;
	auto date = 2.0;
	
	BlackScholesModel bsm{ riskFreeRate, volatility, stockPrice, date };

	auto nPaths = 10000; //generate 10000 simulation
	auto nSteps = 50; // each simulation has 50 pricing iterations
	auto maturity = 4.0; 
	vector<double> finalPrices(nPaths, 0.0);

	for (int i = 0; i < nPaths; i++) {
		vector<double> _singlePath = bsm.generateRiskNeutralPricePath(maturity, nSteps);
		finalPrices[i] = _singlePath.back(); //get the terminal price of the simulation
	}

	auto _meanPrice = mean(finalPrices);
	auto _expectedPrice = bsm.stockPrice * exp(bsm.riskFreeRate * 2);

	ASSERT_APPROX_EQUAL(_meanPrice, _expectedPrice, 0.5);
}

void testBlackScholesModel() {

	setDebugEnabled(true);
	TEST(testGeneratePricePath);
	TEST(testRiskNeutralPricePath);
	setDebugEnabled(false);

}